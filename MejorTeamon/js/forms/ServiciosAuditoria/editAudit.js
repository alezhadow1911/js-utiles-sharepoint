﻿var auditForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			dateTimeFormat:"DD/MM/YYYY hh:mm a",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		internalNameFields:{			
			nombrePersona:"NombrePersona1",
			pais:"Pais",
			workcity:"WorkCity",
			descriptionCity:"help-block-WorkCity",
			ciudades:"CiudadesSeleccion",
			estado:"EstadoAuditoria",
			comentarios:"ComentariosGerente",
			auditor:"Auditor"
		},
		formsParameters:{			
			description:"",
			descriptionApprove:"Revisa la información de la solicitud, para aprobarla o rechazarla",
			descriptionAudit:"Revisa la información de la solicitud, para asignar la fecha de auditoría",
			buttons:{
				send:{text:"Enviar",id:"enviar"},				
				cancel:{text:"Cancelar",id:"cancelar"},				
				aprove:{text:"Aprobar",id:"aprobar"},				
				reject:{text:"Rechazar", id:"rechazar"},
				assing:{text:"Asignar Auditoria", id:"asignar"}
			},
			contentType:"Servicios Auditoria",
			containerId:"container-form"			
		},
		global:{
			createdBy:null,
			created:null,
			formName:null
		},		
		queryCities:{
			query:"?$select=*,Pais/Title&$expand=Pais&$filter=Pais eq {0}",
			listName:"Ciudades"
		},
		errorRequiered:"Esto no puede estar en blanco",		
		errorInvalid:"Los comentarios son obligatorios",		
		invalid:"sp-invalid",
		forms:{
			"AprobarSolicitud":"auditForm.loadApprove()",
			"AsignarAuditoria":"auditForm.loadAudit()"
		},
		states:{
			"NuevaSolicitud":0,
			"AprobarSolicitud":1,
			"AsignarAuditoria":2,
			"Finalizada":3,
			"Rechazada":4			
		},
		bodyItem:"<div class='form-group row'><label>Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior</label></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
		containerForm:".form-content",
		queryPerson:{
			query:"?$select=Usuario/Id&$expand=Usuario&$filter=Title eq 'Gerente Auditoria'",
			listName:"Responsables Auditoria"
		},
		flowParameters:{
			urlFlow:"https://prod-54.westus.logic.azure.com:443/workflows/cf0ea0eb956a4dddafd9a7a40ef7505a/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=tftF1NhZQ_F8wdqsJM8jbC47KJ-MHf1Mww2BLWJ_gPU",
	    	approveUrl:"",
	   		auditUrl:"",	   		
	 	    approveManager:"",
	    	userAuditor:"",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:1,
	    	subjectTitle:"Solicitud de Servicios"
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
	
	    auditForm.parameters.flowParameters.approveUrl = String.format(auditForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(auditForm.parameters.forms)[0],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    auditForm.parameters.flowParameters.auditUrl = String.format(auditForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(auditForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":auditForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "fechaCreacion":auditForm.parameters.global.created,
		        "comentarios":spForms.loadedFields.ComentariosGerente.value,
		        "empresa":spForms.loadedFields.Empresa.value,
		        "descripcion":spForms.loadedFields.DescripcionSituacion.value,
		        "fechaAuditoria":moment(new Date(spForms.loadedFields.FechaAuditoria.value)).format(auditForm.parameters.formats.dateTimeFormat)
		    },
		    "state":spForms.loadedFields.EstadoAuditoria.value,
		    "approveManager":auditForm.parameters.flowParameters.approveManager,
		    "userAuditor":auditForm.parameters.flowParameters.userAuditor,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":auditForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":auditForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,		    
		    "approveUrl":auditForm.parameters.flowParameters.approveUrl,
		    "auditUrl":auditForm.parameters.flowParameters.auditUrl,
		    "subjectTitle":	auditForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:auditForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){			
		var partsOfUrl = window.location.href.split('?')[0].split('/');
		auditForm.parameters.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];
		eval(auditForm.parameters.forms[auditForm.parameters.global.formName]);
	},
	loadApprove:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:auditForm.parameters.formsParameters.buttons.aprove.text,
				id:auditForm.parameters.formsParameters.buttons.aprove.id,
				success:function (){				
					auditForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){				
					return auditForm.validateApprove();
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:auditForm.parameters.formsParameters.buttons.reject.text,
				id:auditForm.parameters.formsParameters.buttons.reject.id,
				success:function (){					
					auditForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return auditForm.validateRejected();
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:auditForm.parameters.formsParameters.buttons.cancel.text,
				id:auditForm.parameters.formsParameters.buttons.cancel.id,
				callback:""
			}
		];
		auditForm.parameters.formsParameters.description = auditForm.parameters.formsParameters.descriptionApprove;
		auditForm.loadEdit(actions,auditForm.personInCharge);


	},
	loadAudit:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:auditForm.parameters.formsParameters.buttons.assing.text,
				id:auditForm.parameters.formsParameters.buttons.assing.id,
				success:function (){				
					auditForm.callFlowService();
					return false;				
				},
				error:function (){},
				preSave: function (){				
					return auditForm.validateApprove();				}
			},			
			{
				type:spForms.parameters.cancelAction,
				name:auditForm.parameters.formsParameters.buttons.cancel.text,
				id:auditForm.parameters.formsParameters.buttons.cancel.id,
				callback:""
			}
		];
		auditForm.parameters.formsParameters.description = auditForm.parameters.formsParameters.descriptionAudit;
		auditForm.loadEdit(actions,auditForm.personInCharge);


	},
	loadEdit:function(actions,sucessFunction){
		spForms.options={	
			actions:actions,			
			description:auditForm.parameters.formsParameters.description,
			contentTypeName:auditForm.parameters.formsParameters.contentType,
			containerId:auditForm.parameters.formsParameters.containerId,
			allowAttachments:true,
		  	requiredAttachments:false,
		  	maxAttachments:5,
		  	minAttachments:0,
		  	maxSizeInMB:3,
			successLoadEditForm:function(){	
				auditForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
				auditForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(auditForm.parameters.formats.dateFormat);			
				if(typeof sucessFunction== "function"){
					sucessFunction();
				}
			} 
		};
		spForms.loadEditForm();
	},
	personInCharge:function(){
	
		try
		{
			var userCurrent = _spPageContextInfo.userId
			var state = document.getElementById(auditForm.parameters.internalNameFields.estado);
			if(state != null)
			{
				if(state.value == auditForm.parameters.states[auditForm.parameters.global.formName])
				{

					if(state.value == auditForm.parameters.states.AprobarSolicitud)
					{
						sputilities.getItems(auditForm.parameters.queryPerson.listName,auditForm.parameters.queryPerson.query,function(data){
							var results = utilities.convertToJSON(data).d.results;	
							if(results.length > 0)
							{
								var items = results[0].Usuario.results;
								var find = false;
								for(var i=0;i<items.length;i++)
								{
									if(items[i].Id == userCurrent){
										find = true;
									 	break;
									}
								}
								
								if(!find)
								{
									auditForm.hideNotAllowed();
								}
							}
							else
							{
								auditForm.hideNotAllowed();
							}

						},function(xhr){ console.log(xhr)});

					}
					else
					{
						if(state.value == auditForm.parameters.states.AsignarAuditoria)
						{
							var userAudit = spForms.loadedFields.Auditor.value;
							if(userCurrent != userAudit)
							{
								auditForm.hideNotAllowed();
							}
						}	
					}
				}
				else
				{
					auditForm.hideNotAllowed();
				}
			}
			
		}
		catch(e)
		{
			console.log(e);
		}
	},
	hideNotAllowed:function(){
		var html = auditForm.parameters.bodyItem;
		var formContent = document.querySelector(auditForm.parameters.containerForm);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	},
	validateRejected:function(){
		
		var state = document.getElementById(auditForm.parameters.internalNameFields.estado);		
		var row = document.getElementById(auditForm.parameters.internalNameFields.comentarios);	
		var auditor = document.getElementById(auditForm.parameters.internalNameFields.auditor);	
	
		var isValid = true;
		
		if(row != null && state != null && auditor != null)
		{			
			if(row.value != "")
			{
				row.parentNode.parentNode.parentNode.removeAttribute(auditForm.parameters.invalid);
				auditor.parentNode.parentNode.parentNode.removeAttribute(auditForm.parameters.invalid);			
				SPClientPeoplePicker.SPClientPeoplePickerDict.Auditor_TopSpan.DeleteProcessedUser();			
				state.value = auditForm.parameters.states.Rechazada;							
			}
			else
			{				
				row.parentNode.parentNode.parentNode.setAttribute(auditForm.parameters.invalid,auditForm.parameters.errorInvalid);	
				auditor.parentNode.parentNode.parentNode.removeAttribute(auditForm.parameters.invalid);		
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;

	},
	validateApprove:function(){
		
		var state = document.getElementById(auditForm.parameters.internalNameFields.estado);	
		var isValid = true;

		if(state != null)
		{
			if(state.value == auditForm.parameters.states.AprobarSolicitud)
			{
				var auditor = document.getElementById(auditForm.parameters.internalNameFields.auditor);	
				var row = document.getElementById(auditForm.parameters.internalNameFields.comentarios);		

				if(auditor != null && row != null)
				{
					if(SPClientPeoplePicker.SPClientPeoplePickerDict.Auditor_TopSpan.IsEmpty())
					{	
						row.parentNode.parentNode.parentNode.removeAttribute(auditForm.parameters.invalid);
						auditor.parentNode.parentNode.parentNode.setAttribute(auditForm.parameters.invalid,auditForm.parameters.errorRequiered);		 
					}
					else
					{
						auditor.parentNode.parentNode.parentNode.removeAttribute(auditForm.parameters.invalid);
						row.parentNode.parentNode.parentNode.removeAttribute(auditForm.parameters.invalid);
						auditForm.parameters.flowParameters.userAuditor = SPClientPeoplePicker.SPClientPeoplePickerDict.Auditor_TopSpan.GetAllUserInfo()[0].EntityData.Email;
						state.value = auditForm.parameters.states.AsignarAuditoria;
					}
				}	
			}
			else
			{
				if(state.value == auditForm.parameters.states.AsignarAuditoria)
				{
					state.value = auditForm.parameters.states.Finalizada;
				}
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;

	}	
}
auditForm.initialize();