﻿var auditForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		optionInitial:"Seleccione",
		loadUserInfo:"auditForm.loadUserInfo",
		formsIsLoaded:false,
		internalNameFields:{			
			nombrePersona:"NombrePersona1",
			pais:"Pais",
			workcity:"WorkCity",
			descriptionCity:"help-block-WorkCity",
			ciudades:"CiudadesSeleccion"
		},
		formsParameters:{			
			description:"Completa la información correspondiente",
			buttons:{
				send:{text:"Enviar",id:"enviar"},				
				cancel:{text:"Cancelar",id:"cancelar"},				
				aprove:{text:"Aprobar",id:"aprobar"},				
				reject:{text:"Rechazar", id:"rechazar"},
				assing:{text:"Asignar Auditoria", id:"asignar"}
			},
			contentType:"Servicios Auditoria",
			containerId:"container-form"			
		},
		global:{
			createdBy:null,
			created:null
		},		
		queryCities:{
			query:"?$select=*,Pais/Title&$expand=Pais&$filter=Pais eq {0}",
			listName:"Ciudades"
		},
		forms:{
			"AprobarSolicitud":"Aprobar Solicitud",
			"AsignarAuditoria":"Asignar Auditoria"
		},
		selectedCountry:"#Pais option:checked",
		selectedCity:"#CiudadesSeleccion option:checked",
		htmlOption:"<option id='{0}-Ciudades' value='{0}'>{1}</option>",	
		htmlCities:"<div class='form-group row' sp-static-name='CiudadesSeleccion'>"+
						   "<label class='col-sm-4 control-label'>Ciudad<i>*</i></label>"+
						   "<div class='col-sm-8'>"+
						      "<div class='form-control-help'>"+
						         "<select id='CiudadesSeleccion' class='form-control input-lg' sp-required='true' sp-type='lookup'></select>"+
						         "<a class='glyphicon glyphicon-question-sign ico-help' id='ico-help-CiudadesSeleccion' aria-hidden='true' onclick='javascript:spForms.showHelpDescription(\"CiudadesSeleccion\",this)'></a>"+
						      "</div>"+
						      "<span id='help-block-CiudadesSeleccion' class='help-block forms-hide-element'>{0}</span>"+
						      "<span id='help-block-error-message-CiudadesSeleccion' class='help-block forms-hide-element error-message'></span>"+
						   "</div>"+
					"</div>",
		invalid:"sp-invalid",
		errorInvalid:"La ciudad seleccionada no es válida",
		queryPerson:{
			query:"?$select=Usuario/EMail&$expand=Usuario&$filter=Title eq 'Gerente Auditoria'",
			listName:"Responsables Auditoria"
		},
		flowParameters:{
			urlFlow:"https://prod-54.westus.logic.azure.com:443/workflows/cf0ea0eb956a4dddafd9a7a40ef7505a/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=tftF1NhZQ_F8wdqsJM8jbC47KJ-MHf1Mww2BLWJ_gPU",
	    	approveUrl:"",
	   		auditUrl:"",	   		
	 	    approveManager:"",
	    	userAuditor:"",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:1,
	    	subjectTitle:"Solicitud de Servicios"
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
	
	    auditForm.parameters.flowParameters.approveUrl = String.format(auditForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(auditForm.parameters.forms)[0],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    auditForm.parameters.flowParameters.auditUrl = String.format(auditForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(auditForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":auditForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "fechaCreacion":auditForm.parameters.global.created,
		        "comentarios":spForms.loadedFields.ComentariosGerente.value,
		        "empresa":spForms.loadedFields.Empresa.value,
		        "descripcion":spForms.loadedFields.DescripcionSituacion.value,
		        "fechaAuditoria":spForms.loadedFields.FechaAuditoria.value
		    },
		    "state":spForms.loadedFields.EstadoAuditoria.value,
		    "approveManager":auditForm.parameters.flowParameters.approveManager,
		    "userAuditor":auditForm.parameters.flowParameters.userAuditor,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":auditForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":auditForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,		    
		    "approveUrl":auditForm.parameters.flowParameters.approveUrl,
		    "auditUrl":auditForm.parameters.flowParameters.auditUrl,
		    "subjectTitle":	auditForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:auditForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){
		sputilities.callFunctionOnLoadBody(auditForm.parameters.loadUserInfo);
		spForms.loadNewForm();
	},	
	loadUserInfo:function(){
		try {
			//running the query with parameters
			sputilities.getMyUserProfileProperties("",function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d;
				var ctrlInterval = setInterval(function(){
					if(auditForm.parameters.formsIsLoaded)
					{
						var name = document.getElementById(auditForm.parameters.internalNameFields.nombrePersona);
						if(name != null)
						{
							name.value = resultItems.DisplayName ? resultItems.DisplayName: "";
						}
						clearInterval(ctrlInterval);
					}
				},500);
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	onChangeCountry:function(){
	
		var selectedCountry = document.querySelector(auditForm.parameters.selectedCountry);
		var selection = document.getElementById(auditForm.parameters.internalNameFields.ciudades);
		
		if(selection != null)
		{
			if(selection.options.length > 0)
			{
				selection.innerHTML = "";
			}
			try
			{
				if(selectedCountry != null)
				{
					var query = String.format(auditForm.parameters.queryCities.query,selectedCountry.value);
					sputilities.getItems(auditForm.parameters.queryCities.listName,query,function(data){
						var results = utilities.convertToJSON(data).d.results;						
						
						var optionInitial = String.format(auditForm.parameters.htmlOption,0,auditForm.parameters.optionInitial);
						selection.insertAdjacentHTML("beforeend",optionInitial);
						
						for(var i=0;i < results.length; i++)
						{
							var option = String.format(auditForm.parameters.htmlOption,results[i].ID,results[i].Ciudad);
							selection.insertAdjacentHTML("beforeend",option);
						}
						
					},function(xhr){ console.log(xhr)});
				}
			}
			catch(e){
				console.log(e);
			}
		}
	},
	insertHTMLCities:function(){
		var row = document.getElementById(auditForm.parameters.internalNameFields.workcity);
		var description = document.getElementById(auditForm.parameters.internalNameFields.descriptionCity)			
		if(row != null && description != null)
		{
			var position = row.parentNode.parentNode.parentNode;
			var html = String.format(auditForm.parameters.htmlCities,description.innerHTML);
			position.insertAdjacentHTML("beforebegin", html);
		}
	},
	addOnChange:function(){
		var selectCountries = document.getElementById(auditForm.parameters.internalNameFields.pais);
		
		if(selectCountries != null)
		{
			selectCountries.onchange = function() {
				auditForm.onChangeCountry();
			}

		}
	},
	saveCity:function(){
		var selectedCity = document.querySelector(auditForm.parameters.selectedCity);
		var row = document.getElementById(auditForm.parameters.internalNameFields.ciudades);
		var city = document.getElementById(auditForm.parameters.internalNameFields.workcity);
		if(selectedCity != null && row != null && city != null)
		{
			if(selectedCity.value != 0)
			{
				row.parentNode.parentNode.parentNode.removeAttribute(auditForm.parameters.invalid);
				city.value = selectedCity.text; 
			}
			else
			{	
				city.value = ""; 
				row.parentNode.parentNode.parentNode.setAttribute(auditForm.parameters.invalid, auditForm.parameters.errorInvalid);

			}
		}
		else
		{
			return false;
		}
		
		return true;		
	},
	loadManager:function(){
		try
		{
			sputilities.getItems(auditForm.parameters.queryPerson.listName,auditForm.parameters.queryPerson.query,function(data){
				var results = utilities.convertToJSON(data).d.results;
				var items = results[0].Usuario.results;
				if(items.length > 0)
				{
					var manager = "";
					for(var i=0;i < items.length; i++)
					{
						manager = manager + String.format('{0};',items[i].EMail);
					}
					
					auditForm.parameters.flowParameters.approveManager = manager;
				}
					
			},function(xhr){ console.log(xhr)});
		}
		catch(e){
			console.log(e);
		}
	}
}

spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:auditForm.parameters.formsParameters.buttons.send.text,
			id:auditForm.parameters.formsParameters.buttons.send.id,
			success:function (){				
				auditForm.callFlowService();
				return false;
			},
			error:function (){},
			preSave: function (){
				return auditForm.saveCity();				
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:auditForm.parameters.formsParameters.buttons.cancel.text,
			id:auditForm.parameters.formsParameters.buttons.cancel.id,
			callback:""
		}
	],		
	description:auditForm.parameters.formsParameters.description,
	contentTypeName:auditForm.parameters.formsParameters.contentType,
	containerId:auditForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:false,
  	maxAttachments:5,
  	minAttachments:0,
  	maxSizeInMB:3,
	successLoadNewForm:function(){
		auditForm.parameters.global.createdBy = sputilities.contextInfo().userEmail;
		auditForm.parameters.global.created = moment.utc(new Date()).format(auditForm.parameters.formats.dateFormat);
		auditForm.parameters.formsIsLoaded = true;
		auditForm.insertHTMLCities();
		auditForm.addOnChange();
		auditForm.onChangeCountry();
		auditForm.loadManager();
	} 
};
auditForm.initialize();