﻿spForms.options={
	actions:[
		{
			type:spForms.parameters.cancelAction,
			name:"Cancelar",
			id:"cancelar",
			callback:""
		},
	],
	description:"",
	contentTypeName:"PQRS",
	containerId:"spForm",
	successLoadEditForm:function(){
		document.getElementById('label-ComentariosRespuestaPqrs').innerHTML += "<i>*</i>";
		var select = document.getElementById('TipoPQRS');
		if(select != null){
			var option = select.selectedOptions[0];
			if(option != null){
				document.querySelector('[sp-static-name="AsuntoPqrs"]').style.display = "block";
				if(option.innerText == "Preguntas"){
					var inputPqrs = document.getElementById('AsuntoPqrs');
					if(inputPqrs != null){
						if(inputPqrs.value == "Otro"){
							document.querySelector('[sp-static-name="MensajePqrs"]').style.display = "block";		
						}
					}
				}else{
					document.querySelector('[sp-static-name="MensajePqrs"]').style.display = "block";
				}
			}
		}
	}
};
spForms.loadEditForm();
var pqrsForm = {
	parameters:{
	},
	changeState:function(isRejected){
		document.getElementById('EstadoPqrs').selectedIndex = isRejected ? 2 : 1; 
	}
};
