﻿var pqrsForm = {
	parameters:{
		workPhone:"WorkPhone",
		bodyItemStart:"<div class='contact'><div class='detail-contact'><p>Solicita información o radica una queja sobre algún proceso y/o servicio de nuestras Vicepresidencias diligenciando el siguiente formulario. </br>Ten en cuenta que este canal de atención <strong>no reemplaza</strong> a la Mesa de Ayuda. </br> </br> <strong>¡Espera de vuelta el correo con la atención a tu solicitud!</strong></p></div></div><style>#s4-ribbonrow{display:none;} .detail-contact{ width:100%; max-width:800px; height:auto;}.contact{text-align:justify;}#content-page-detail{height:auto !important;} #dialogTitleSpan{width:100%!important; max-width:718px;}</style>",
		titlePopUp:"Buzón de sugerencias y PQR",
		inputsId: [{idInput :"NombrePersona"},{idInput:"CorreoPersona"},{idInput:"ExtensionPersona"}, {idInput:"CargoPersona"}],
		inputsStaticNames: [{staticName: "ka9a0dc9075e40b68498b7acd43885f1"}, {staticName: "Title"} , {staticName: "AceptadoPqrs"}, {staticName: "EstadoPqrs"}, {staticName: "IdAsuntoPqrs"}, {staticName: "ComentariosRespuestaPqrs"}, {staticName: "EsPreguntaPqrs"}],
		inputSubject: "AsuntoPqrs",
		inputMenssage:"MensajePqrs",
		querySelector: "div[sp-static-name='{0}']",
		typeRequest: "div[sp-static-name='TipoPQRS'] > div > div > select",
		camlVp: "<View><Query><Where><Eq><FieldRef Name='VicepresidenciaAQ'/><Value Type='TaxonomyFieldType'>{0}</Value></Eq></Where></Query></View>",
		listNameQuestions:"PreguntasRespuestasPQRS",
		subjectQuestion:"div[sp-static-name='PreguntasRespuestas'] > div > div > select",
		typePQR:"div[sp-static-name='TipoPQRS'] > div > div > select",
		htmlOption:"<option id='{0}-Preguntas' value='{0}'>{1}</option>",
		htmlTypePqr:"<div class='form-group row' sp-static-name='PreguntasRespuestas'>"+
						   "<label class='col-sm-4 control-label'>Asunto<i>*</i></label>"+
						   "<div class='col-sm-8'>"+
						      "<div class='form-control-help'>"+
						         "<select class='form-control input-lg' sp-required='true' sp-type='lookup'>{0}</select>"+
						         "<a class='glyphicon glyphicon-question-sign ico-help' aria-hidden='true'></a>"+
						      "</div>"+
						      "<span id='help-block-Asunto' class='help-block forms-hide-element'></span>"+
						      "<span id='help-block-error-message-Asunto' class='help-block forms-hide-element error-message'></span>"+
						   "</div>"+
					"</div>",
		bodyItemFinish:"<div class='contact' style='text-align: justify;line-height: 1.5em;'><p>Su solicitud ha sido enviada exitosamente.</p></div>",			
		resultItems:null,
		resultItemsQuestions:null,
		queryStringParameter:"Source",
		internalNameFields:{
			NombrePersona:"NombrePersona",
			CorreoElectronico:"CorreoPersona",
			PreguntasRespuestas:"PreguntasRespuestas",
			Vicepresidencia:"Vicepresidencia",
			AsuntoPqrs:"AsuntoPqrs",
			MensajePqrs:"MensajePqrs",
			TipoPQRS:"TipoPQRS",
			preguntas:"preguntas",
			spTerm:"sp-nameterm",
			EsPreguntaPqrs:"EsPreguntaPqrs"
		},
		messagesInvalid:{
		 CorreoElectronico:"El correo electrónico no tiene el formato correcto"
		},
		errorClass:"has-error",
		selectorError:"help-block-error-message-{0}",
		buttonsNames:{
			btnSend:"Enviar",
			btnCancel:"Cancelar"
		},
		buttonsId:{
			btnSend:"enviar",
			btnCancel:"cancelar"
		},
		descriptionForm:"Completa la información correspondiente",
		loadUserInfo:"pqrsForm.loadUserInfo",
		loadInitPopUp:"pqrsForm.initPopUp(pqrsForm.parameters.bodyItemStart,null,pqrsForm.parameters.titlePopUp)",
		contentTypeNameForm:"PQRS",
		containerIdForm:"spForm",
		label:"label-{0}",
		checkLabel:"label input",
		checkedLabel:"label input:checked",
		requiredText:"<i>*</i>",
		regexEmail:"^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$",
		errorInvalidNotText:"Esto no puede estar en blanco",
		formsIsLoaded:false,
		formsParameters:{
			title:"Buzón de sugerencias y PQR",
			description:"Completa la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
		},
		flowParameters:{
			urlFlow:"https://prod-26.westus.logic.azure.com:443/workflows/6c93bb144d774f8da0ec59c259f7e5e0/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=Ig384tmUBeHk5kcJijteVOlKr_jprrRN18Ylf5UCopo",
			editUrl:"",
	    	approveUrl:"",
	   		listNameAssing:"ResponsablesPQRS",
	    	subjectTitle:"Buzón de sugerencias y PQR"
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud",
			"VerSolicitud":"Ver Solicitud"
		},
		global:{
			createdBy:null,
			created:null
		},
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity = 1;
		document.querySelector('.loader-container').style.height = "auto";
		
		var objectSelect = document.querySelector(pqrsForm.parameters.typeRequest);
		var textSelected = objectSelect.options[objectSelect.selectedIndex].text;
			
	    pqrsForm.parameters.flowParameters.approveUrl = String.format(pqrsForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(pqrsForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    pqrsForm.parameters.flowParameters.editUrl = String.format(pqrsForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(pqrsForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	   	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":pqrsForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona.value,
		    	"extension":spForms.loadedFields.ExtensionPersona.value,
		    	"cargo":spForms.loadedFields.CargoPersona.value,
		    	"vicepresidencia":spForms.loadedFields.Vicepresidencia.value.label,
		        "tipo":textSelected,
		        "asuntoSolicitud":spForms.loadedFields.AsuntoPqrs.value,
		        "mensajeSolicitud":spForms.loadedFields.MensajePqrs.value,
		        "esPregunta":spForms.loadedFields.EsPreguntaPqrs.value,
		        "comentariosSolicitud":spForms.loadedFields.ComentariosRespuestaPqrs.value,
		        "idAsuntoSolicitud":spForms.loadedFields.IdAsuntoPqrs.value,
		        "aceptado":spForms.loadedFields.AceptadoPqrs.value
		        
		    },
		    "state":spForms.loadedFields.EstadoPqrs.value,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "entityType":spForms.listInfo.entityItem,
		    "editUrl":pqrsForm.parameters.flowParameters.editUrl,
		    "approveUrl":pqrsForm.parameters.flowParameters.approveUrl,
		    "subjectTitle":	pqrsForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:pqrsForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity = 0;
				document.querySelector('.form-content').style.opacity = 1;
				document.querySelector('.loader-container').style.height = 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){
		sputilities.callFunctionOnLoadBody(pqrsForm.parameters.loadUserInfo);
		sputilities.callFunctionOnLoadBody(pqrsForm.parameters.loadInitPopUp);
		spForms.loadNewForm();
	},
	loadUserInfo:function(){
		try {
			//running the query with parameters
			sputilities.getMyUserProfileProperties("",function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d;
				pqrsForm.parameters.resultItems = resultItems;
				var ctrlInterval = setInterval(function(){
					if(pqrsForm.parameters.formsIsLoaded)
					{
						pqrsForm.setValuesInputs();
						pqrsForm.addOnChangeVp();
						pqrsForm.addOnChangeTypeRequest();
						clearInterval(ctrlInterval);
					}
				},500);
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	setValuesInputs: function() {
		
		pqrsForm.hiddenInputs();
		
		var displayName = document.getElementById(pqrsForm.parameters.inputsId[0].idInput);
		var email = document.getElementById(pqrsForm.parameters.inputsId[1].idInput);
		var title = document.getElementById(pqrsForm.parameters.inputsId[3].idInput);
		var ext = document.getElementById(pqrsForm.parameters.inputsId[2].idInput);
		var lblMessage = document.getElementById(String.format(pqrsForm.parameters.label,pqrsForm.parameters.internalNameFields.MensajePqrs));
		
		if (displayName != null && email != null && title != null && ext != null)
		{
			displayName.value = pqrsForm.parameters.resultItems.DisplayName ? pqrsForm.parameters.resultItems.DisplayName: "";
			email.value = pqrsForm.parameters.resultItems.Email ? pqrsForm.parameters.resultItems.Email: "";
			title.value = pqrsForm.parameters.resultItems.Title ? pqrsForm.parameters.resultItems.Title: "";
			ext.value = pqrsForm.parameters.resultItems.UserProfileProperties.results.filter(pqrsForm.findPropertiesUser)[0].Value ? pqrsForm.parameters.resultItems.UserProfileProperties.results.filter(pqrsForm.findPropertiesUser)[0].Value: "";		
			lblMessage.innerHTML += pqrsForm.parameters.requiredText;
			
			displayName.disabled = true;
			pqrsForm.onChangeEmail();
			
			
		}		
	},
	findPropertiesUser:function(properties){
		return properties.Key == pqrsForm.parameters.workPhone;
	},
	onChangeEmail: function (){
		document.querySelector(String.format(pqrsForm.parameters.querySelector ,pqrsForm.parameters.internalNameFields.CorreoElectronico)).onchange = function(){
			var emailPerson = document.getElementById(pqrsForm.parameters.internalNameFields.CorreoElectronico).value;
				
			if(emailPerson != ""){
				if (!pqrsForm.validateEmail(emailPerson)){
					document.querySelector(String.format(pqrsForm.parameters.querySelector ,pqrsForm.parameters.internalNameFields.CorreoElectronico)).classList.add(pqrsForm.parameters.errorClass);
					document.getElementById(String.format(pqrsForm.parameters.selectorError,pqrsForm.parameters.internalNameFields.CorreoElectronico)).textContent = pqrsForm.parameters.messagesInvalid.CorreoElectronico;
				}else{
					document.querySelector(String.format(pqrsForm.parameters.querySelector ,pqrsForm.parameters.internalNameFields.CorreoElectronico)).classList.remove(pqrsForm.parameters.errorClass);
					document.getElementById(pqrsForm.parameters.selectorError,pqrsForm.parameters.internalNameFields.CorreoElectronico).textContent ="";
				}
			}
		}
	},
	hiddenInputs: function(){
		for(var i=0; i < pqrsForm.parameters.inputsStaticNames.length; i++){
			pqrsForm.disableInputs(pqrsForm.parameters.inputsStaticNames[i].staticName,'block');
	
			var objectHidden = String.format(pqrsForm.parameters.querySelector, pqrsForm.parameters.inputsStaticNames[i].staticName);
			document.querySelector(objectHidden).style.display = 'none';
		}
	},
	addOnChangeVp:function(){
		var row = document.querySelector(String.format(pqrsForm.parameters.querySelector,pqrsForm.parameters.internalNameFields.Vicepresidencia));
		if(row != null)
		{
			var checks = row.querySelectorAll(pqrsForm.parameters.checkLabel);
			
			if(checks != null)
			{
				for(var j= 0; j < checks.length; j++){
					var check = checks[j];
					check.addEventListener('change',function(){
						var row = document.querySelector(String.format(pqrsForm.parameters.querySelector,pqrsForm.parameters.internalNameFields.Vicepresidencia));						var checks_checked = row.querySelectorAll(pqrsForm.parameters.checkedLabel);
						var rows = document.querySelectorAll('[sp-static-name="TipoPQRS"],[sp-static-name="AsuntoPqrs"],[sp-static-name="MensajePqrs"]');
						var display = checks_checked.length == 0 ? "none":"block";
						for(var i = 0; i < rows.length; i++){
							var row = rows[i];
							row.style.display = display;
						}
						document.getElementById(pqrsForm.parameters.internalNameFields.TipoPQRS).selectedIndex = 0;
						var ctrl = document.getElementById(pqrsForm.parameters.internalNameFields.preguntas);
						if(ctrl != null){
							ctrl.parentNode.removeChild(ctrl);
						}
					});
				}
			}
		}
	},
	addOnChangeTypeRequest: function(){
				
		 document.querySelector(pqrsForm.parameters.typeRequest).onchange = function() {

			var objectSelect = document.querySelector(pqrsForm.parameters.typeRequest);
			var textSelected = objectSelect.options[objectSelect.selectedIndex].text;
			var inputVp = document.querySelectorAll("input[sp-title='Vicepresidencia']:checked");

			if (inputVp.length > 0)
			{
				if(textSelected == "Preguntas"){
					var titleVp = inputVp[0].getAttribute(pqrsForm.parameters.internalNameFields.spTerm);
					var calmQuery = String.format(pqrsForm.parameters.camlVp,inputVp[0].getAttribute(pqrsForm.parameters.internalNameFields.spTerm));
					
					sputilities.getItemsByCaml({caml:calmQuery,listName:pqrsForm.parameters.listNameQuestions,
						success: function(dataContent) {
							var hmtlOption = "";
							pqrsForm.parameters.resultItemsQuestions= JSON.parse(dataContent).d.results;
							for(var i = 0 ; i < pqrsForm.parameters.resultItemsQuestions.length; i++){
								hmtlOption += String.format(pqrsForm.parameters.htmlOption,pqrsForm.parameters.resultItemsQuestions[i].Id,pqrsForm.parameters.resultItemsQuestions[i].PreguntaAQ);							
							}
							hmtlOption += String.format(pqrsForm.parameters.htmlOption,0, "Otro");
							pqrsForm.insertQuestionsHTML(hmtlOption);
							pqrsForm.disableInputs(pqrsForm.parameters.inputSubject,'none');
							pqrsForm.disableInputs(pqrsForm.parameters.inputMenssage,'none');

							document.querySelector(pqrsForm.parameters.subjectQuestion).onchange = function() {
								var objectSelectSubject = document.querySelector(pqrsForm.parameters.subjectQuestion);
								var textSelectedSubject = objectSelectSubject.options[objectSelectSubject.selectedIndex].text;
								
								if (textSelectedSubject == "Otro") {
									pqrsForm.disableInputs(pqrsForm.parameters.inputMenssage,'block');
								}
								else {
									pqrsForm.disableInputs(pqrsForm.parameters.inputMenssage,'none');
								}
							}
							var event = new Event('change');
							document.querySelector(pqrsForm.parameters.subjectQuestion).dispatchEvent(event);
						}
					});
				}
				else
				{
					pqrsForm.disableInputs(pqrsForm.parameters.inputSubject,'block');
					pqrsForm.disableInputs(pqrsForm.parameters.inputMenssage,'block');
					
					var ctrl = document.getElementById(pqrsForm.parameters.internalNameFields.preguntas);	
					if(ctrl != null){
						ctrl.parentNode.removeChild(ctrl);
					}
				}
			}
		 }
	},
	insertQuestionsHTML: function(hmtlOption){

		var htmlSelect = String.format(pqrsForm.parameters.htmlTypePqr,hmtlOption);
		var element = document.createElement('div');
		element.id = pqrsForm.parameters.internalNameFields.preguntas;
		element.innerHTML = htmlSelect;
		var typeElement = document.querySelector(String.format(pqrsForm.parameters.querySelector,pqrsForm.parameters.internalNameFields.TipoPQRS));
		typeElement.parentNode.insertBefore(element, typeElement.nextSibling);

	},
	disableInputs: function(name,display){
		var object = String.format(pqrsForm.parameters.querySelector, name);
		document.querySelector(object).style.display = display;
	},
	initPopUp:function(body,url,title){
		try {
			
			sputilities.showModalDialogForms(title,null,url,body,
				function(data){	
				 return true;			
				}
			);
		}
		catch (e){
			console.log(e);
		}
	},
	setValuesForm:function(){
			var isValid = true;
			var objectSelect = document.querySelector(pqrsForm.parameters.typeRequest);
			var textSelected = objectSelect.options[objectSelect.selectedIndex].text;

			if (textSelected == "Preguntas")
			{
				var questionIdSelected = document.querySelector("div[sp-static-name='PreguntasRespuestas'] select");
				var questionTextSelected = questionIdSelected.options[questionIdSelected.selectedIndex].text;
				var subjectId = document.querySelector("div[sp-static-name='IdAsuntoPqrs'] input").value = questionIdSelected.value; 
			 	var subjectTitle = document.querySelector("div[sp-static-name='AsuntoPqrs'] input").value = questionTextSelected;
			 	document.querySelector("div[sp-static-name='EsPreguntaPqrs'] input").checked = true;
			 	
			 	if (document.querySelector("div[sp-static-name='PreguntasRespuestas'] select").value == "0"){
			 		if (document.querySelector("div[sp-static-name='MensajePqrs'] textarea").value == ""){
					document.querySelector("div[sp-static-name='MensajePqrs']").setAttribute('sp-invalid', "Este campo no debe estar en blanco");
					}else{
						document.querySelector("div[sp-static-name='MensajePqrs']").removeAttribute('sp-invalid');
					}
			 	}
			}
			else
			{
				if (document.querySelector("div[sp-static-name='MensajePqrs'] textarea").value == ""){
					document.querySelector("div[sp-static-name='MensajePqrs']").setAttribute('sp-invalid', "Este campo no debe estar en blanco");
				}else{
					document.querySelector("div[sp-static-name='MensajePqrs']").removeAttribute('sp-invalid');
				}
			}
		return isValid;				
	},
	validateEmail: function (email){
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	return re.test(email);
  	},
};
spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:pqrsForm.parameters.buttonsNames.btnSend,
			id:pqrsForm.parameters.buttonsId.btnSend,
			success:function (){
				pqrsForm.callFlowService();
				return false;
			},
			error:function (){},
			preSave: function (){
				return pqrsForm.setValuesForm()
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:pqrsForm.parameters.buttonsNames.btnCancel,
			id:pqrsForm.parameters.buttonsId.btnCancel,
			callback:""
		},
	],
	description:pqrsForm.parameters.descriptionForm,
	contentTypeName:pqrsForm.parameters.contentTypeNameForm,
	containerId:pqrsForm.parameters.containerIdForm,
	allowAttachments:false,
  	requiredAttachments:false,
  	maxAttachments:20,
  	minAttachments:0,
  	maxSizeInMB:3,
	successLoadNewForm:function(){
		pqrsForm.parameters.global.createdBy = sputilities.contextInfo().userEmail;
		pqrsForm.parameters.global.created = moment.utc(new Date()).format(pqrsForm.parameters.formats.dateFormat);
		pqrsForm.parameters.formsIsLoaded = true;
	} 
};
pqrsForm.initialize();