﻿var pqrsForm = {
	parameters:{
	internalNameFields:{
			NombrePersona:"NombrePersona",
			CorreoElectronico:"CorreoPersona",
			PreguntasRespuestas:"PreguntasRespuestas",
			Vicepresidencia:"Vicepresidencia",
			AsuntoPqrs:"AsuntoPqrs",
			MensajePqrs:"MensajePqrs",
			TipoPQRS:"TipoPQRS",
			preguntas:"preguntas",
			spTerm:"sp-nameterm",
			EsPreguntaPqrs:"EsPreguntaPqrs"
		},
		errorClass:"has-error",
		selectorError:"help-block-error-message-{0}",
		buttonsNames:{
			btnSend:"Enviar",
			btnCancel:"Cancelar"
		},
		buttonsId:{
			btnSend:"enviar",
			btnCancel:"cancelar"
		},
		descriptionForm:"Completa la información correspondiente, para aprobar o rechazar la solicitud",
		contentTypeNameForm:"PQRS",
		containerIdForm:"spForm",
		loadInfoRequest:"pqrsForm.loadInfoRequest",
		label:"label-{0}",
		checkLabel:"label input",
		checkedLabel:"label input:checked",
		requiredText:"<i>*</i>",
		regexEmail:"^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$",
		errorInvalidNotText:"Esto no puede estar en blanco",
		formsIsLoaded:false,
		formsParameters:{
			title:"Buzón de sugerencias y PQR",
			description:"Completa la información correspondiente",
			buttons:{
				Send:{Text:"Responder",Id:"responder"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
		},
		flowParameters:{
			urlFlow:"https://prod-26.westus.logic.azure.com:443/workflows/6c93bb144d774f8da0ec59c259f7e5e0/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=Ig384tmUBeHk5kcJijteVOlKr_jprrRN18Ylf5UCopo",
			editUrl:"",
	    	approveUrl:"",
	    	listNameAssing:"ResponsablesPQRS",
	    	subjectTitle:"Buzón de sugerencias y PQR"
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud",
			"VerSolicitud":"Ver Solicitud"
		},
		global:{
			createdBy:null,
			created:null
		},
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity = 1;
		document.querySelector('.loader-container').style.height = "auto";
			
	    pqrsForm.parameters.flowParameters.approveUrl = String.format(pqrsForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(pqrsForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    pqrsForm.parameters.flowParameters.editUrl = String.format(pqrsForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(pqrsForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	   	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":spForms.loadedFields.CorreoPersona.value,
		    	"nombres":spForms.loadedFields.NombrePersona.value,
		    	"extension":spForms.loadedFields.ExtensionPersona.value,
		    	"cargo":spForms.loadedFields.CargoPersona.value,
		    	"vicepresidencia":spForms.loadedFields.Vicepresidencia.value.label,
		        "tipo":spForms.loadedFields.TipoPQRS.value,
		        "asuntoSolicitud":spForms.loadedFields.AsuntoPqrs.value,
		        "mensajeSolicitud":spForms.loadedFields.MensajePqrs.value,
		        "esPregunta":spForms.loadedFields.EsPreguntaPqrs.value,
		        "comentariosSolicitud":spForms.loadedFields.ComentariosRespuestaPqrs.value,
		        "idAsuntoSolicitud":spForms.loadedFields.IdAsuntoPqrs.value,
		        "aceptado":spForms.loadedFields.AceptadoPqrs.value
		        
		    },
		    "state":spForms.loadedFields.EstadoPqrs.value,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "entityType":spForms.listInfo.entityItem,
		    "editUrl":pqrsForm.parameters.flowParameters.editUrl,
		    "approveUrl":pqrsForm.parameters.flowParameters.approveUrl,
		    "subjectTitle":	pqrsForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:pqrsForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity = 0;
				document.querySelector('.form-content').style.opacity = 1;
				document.querySelector('.loader-container').style.height = 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){
		sputilities.callFunctionOnLoadBody(pqrsForm.parameters.loadInfoRequest);
		spForms.loadEditForm();
	},
	validatePreSaveFormEdit:function(){
		var ctrl = document.getElementById('ComentariosRespuestaPqrs');
		if(ctrl.value.trim() == ''){
			document.querySelector('[sp-static-name="ComentariosRespuestaPqrs"]').setAttribute('sp-invalid',spForms.parameters.formats.requiredError);
			return false;
		}
		else{
			document.querySelector('[sp-static-name="ComentariosRespuestaPqrs"]').removeAttribute('sp-invalid');
			return true;
		}
	},
	changeState:function(isRejected){
		document.getElementById('EstadoPqrs').selectedIndex = isRejected ? 2 : 1; 
	},
	loadInfoRequest: function(){
		var ctrlInterval = setInterval(function(){
			if(pqrsForm.parameters.formsIsLoaded)
			{
				document.getElementById('label-ComentariosRespuestaPqrs').innerHTML += "<i>*</i>";
				document.getElementById('label-MensajePqrs').innerHTML += "<i>*</i>";
		
				var select = document.getElementById('TipoPQRS');
				if(select != null)
				{
					var option = select.options[select.selectedIndex];
					if(option != null)
					{
						document.querySelector('[sp-static-name="AsuntoPqrs"]').style.display = "block";
						if(option.innerText == "Preguntas"){
							var inputPqrs = document.getElementById('AsuntoPqrs');
							if(inputPqrs != null)
							{
								if(inputPqrs.value == "Otro")
								{
									document.querySelector('[sp-static-name="MensajePqrs"]').style.display = "block";
								}
							}
						}else
						{
							document.querySelector('[sp-static-name="MensajePqrs"]').style.display = "block";
						}
					}
				}			
				clearInterval(ctrlInterval);
			}
		},500);
	}
};
spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:pqrsForm.parameters.formsParameters.buttons.Send.Text,
			id:pqrsForm.parameters.formsParameters.buttons.Send.Id,
			preSave:function(){
				if(pqrsForm.validatePreSaveFormEdit()){
					pqrsForm.changeState(false);
				}
				return true;

			},
			success:function(){
				pqrsForm.callFlowService();
				return false;

			},
			error:function(){return true}
		},
		{
			type:spForms.parameters.saveAction,
			name:pqrsForm.parameters.formsParameters.buttons.Reject.Text,
			id:pqrsForm.parameters.formsParameters.buttons.Reject.Id,
			preSave:function(){
				if(pqrsForm.validatePreSaveFormEdit()){
					 pqrsForm.changeState(true);
				}
				return true;

			},
			success:function(){
				pqrsForm.callFlowService();
				return false;

			},
			error:function(){return true}
		},
		{
			type:spForms.parameters.saveAction,
			name:pqrsForm.parameters.formsParameters.buttons.Cancel.Text,
			id:pqrsForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		},
	],
	description:pqrsForm.parameters.descriptionForm,
	contentTypeName:pqrsForm.parameters.contentTypeNameForm,
	containerId:pqrsForm.parameters.containerIdForm,
	allowAttachments:false,
  	requiredAttachments:false,
  	maxAttachments:20,
  	minAttachments:0,
  	maxSizeInMB:3,
	successLoadEditForm:function(){
		pqrsForm.parameters.formsIsLoaded = true		
	}
};
pqrsForm.initialize();