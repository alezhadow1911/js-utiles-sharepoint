﻿var ctrlForm = {
	parameters:{
		formats:{
			"queryStaticNameRow":"[sp-static-name='{0}']",
			"htmlNotAllowed":"<div><h4>{0}</h4></div>",
			"querySelectRadio":'input[type="radio"][value="{0}"]:checked',
			"queryRadio":'input[type="radio"][value="{0}"]',
			"emailUsers":"{0};",
			"dateFormat":"DD-MM-YYYY",
			"dateCustomFormat":"{1}{0}{2}{0}{3}",
			"formFormat":"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			"requiredForm":"<i>*</i>",
			"labelRowId":"label-{0}",
			"queryCheckBox":"input[type='checkbox'][sp-title='{0}']:checked",
			"message":"El número de centro de costo es: {0} "					
		},
		forms:{
			"NewForm":"ctrlForm.redirectCustomForm('NuevaSolicitud')",
			"EditForm":"ctrlForm.redirectCustomForm('CorregirSolicitud')",
			"DispForm":"ctrlForm.redirectCustomForm('VerSolicitud')",
			"NuevaSolicitud":"ctrlForm.loadNew()",
			"AprobarBP":"ctrlForm.loadApprove()",
			"ValidarAD":"ctrlForm.loadApprove()",
			"AprobarGerente":"ctrlForm.loadApprove()",
			"AplicarSolicitud":"ctrlForm.loadApply()",
			"CorregirSolicitud":"ctrlForm.loadEditApplicant()",
			"VerSolicitud":"ctrlForm.loadReview()"
		},
		formsStates:{
			"AprobarBP":"AprobarBP",
			"ValidarAD":"ValidarAD",
			"AprobarGerente":"AprobarGerente"
		},
		messages:{
			"idSaveForm":"enviarSolicitud",
			"idCancelForm":"cancelar",
			"idEditForm":"enviarCorreccion",
			"idApproveForm":"aprobar",
			"idRejectForm":"solicitarCorreccion",
			"idApplyForm":"aplicarNovedad",
			"textSaveForm":"Enviar",
			"textCancelForm":"Cancelar",
			"textEditForm":"Enviar",
			"textApproveForm":"Aprobar",
			"textRejectForm":"Enviar a Corregir",
			"textApplyForm":"Novedad Aplicada",
			"textNotAllowed":"Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior"
		},
		columns:{			
			"displayName":{
				"staticName":"FullName"
			},
			"comments":{
				"staticName":"ComentariosFZ"
			},
			"email":{
				"staticName":"EMail"
			},
			"approveBP":{
				"staticName":"BPFinanzas",
				"inputId":"BPFinanzas_TopSpan_HiddenInput"
			},
			"centroDeCostos":{
				"staticName":"CentroDeCostos"
			},
			"tipoAccion":{
				"staticName":"TipoAccion",
				"values":{
					"crear":"Crear",
					"habilitar":"Habilitar",
					"modificar":"Modificar",
					"inhabilitar":"Inhabilitar"
				}
			},
			"justification":{
				"staticName":"JustificacionSolicitud"
			},
			"ResponsibleDevolution":{
				"staticName":"ResponsableDevolucion"
				
			},
			"BPEMail":{
				"staticName":"BPEMail"
			},
			"responsible":{
				"staticName":"Responsable",
				"inputId":"Responsable_TopSpan_HiddenInput"
			},
			"requestDate":{
				"staticName":"FechaSolicitud"
			},
			"lastState":{
				"staticName":"EstadoAnterior"
			},
			"vicepresidency":{
				"attributeSelector":"Vicepresidencia",
				"attributeValue":"sp-nameterm"
			}
		},
		states:[
			"Solicitado",
			"Aprobacion BP",
			"Validacion AD",
			"Aprobacion Gerente AD",
			"Asignado CERAT",
			"Corregir",
			"Novedad Aplicada"
		],
		parametersHTML:{
			"spHidden":"sp-hidden",
			"spDisable":"sp-disable"
		},
		selectorFormContent:".form-content",
		cellPhone:"CellPhone",
		functionLoadMyProperties:"ctrlForm.loadMyProperties",
		containerId:"container-form",
		title:"Solicitud centro de costos",
		titleEdit:"Corregir solicitud",
		titleApply:"Aplicar solicitud",
		titleApprove:"Aprobar/Corregir solicitud centro de costos",
		titleReview:"Solicitud centro de costos",
		description:"Complete la información para la solicitud",
		descriptionEdit:"Corrija la información de la solicitud",
		descriptionApply:"Revise la información y aplique la novedad si corresponde",
		descriptionApprove:"Revise la información y apruebe o envíe a corrección según corresponda",
		descriptionReview:"Resultado de la solicitud",
		contentTypeName:"Centro de costos",
		formIsLoaded:false,
		allowAttachments:false,
		requiredAttachments:false,
		maxAttachments:20,
		minAttachments:0,
		maxSizeInMB:3,
		listNameConsecutive:"ControlRadicados",
		idConsecutive:1,
		listUsersFlow:"ResponsablesFZ",
		viewUrl:"",
	    editUrl:"",
	    approveBPUrl:"",
	    approveADUrl:"",
	    approveGADUrl:"",
	    applyUrl:"",
	    listViewApply:"",
	    listViewApprove:"",
	    queryUsers:"?$filter=FlujoAsociadoId eq 1&$select=ResponsablesAbonos/EMail,EtapaResponsable&$expand=ResponsablesAbonos",
	    flowUrl:"https://prod-41.westus.logic.azure.com:443/workflows/dc4f3121684d4173be7374722a459fb3/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=691q9FdBD5jd6_W0mui5lbJTkozl3AW0SlNeJqrf3XQ"
	},
	global:{
		formName:null,
		createdBy:null,
		created:null,
		requestDate:null,
		lastState:"",
		approveBPUsers:"",
		approveADUsers:"",
		approveGADUsers:"",		
		applyUsers:"",
		bpFinanzas:"",
		responsable:"",
		vicepresidency:"",
		message:""
	},
	initialize:function(){
		var partsOfUrl = window.location.href.split('?')[0].split('/');
		ctrlForm.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];		
		eval(ctrlForm.parameters.forms[ctrlForm.global.formName]);
	},
	callFlowService:function(){			
		
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
		ctrlForm.parameters.viewUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[9],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.editUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[8],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.approveBPUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.approveADUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[5],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.approveGADUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[6],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);	    	    
	    ctrlForm.parameters.applyUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[7],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":ctrlForm.global.createdBy,
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		    	"nombres":spForms.loadedFields.FullName.value,
		    	"email":spForms.loadedFields.EMail.value,
		    	"areaSolicitante":spForms.loadedFields.AreaSolicitante.value,
		    	"fechaSolicitud":ctrlForm.global.requestDate,
		        "fechaCreacion":ctrlForm.global.created,
		        "tipoAccion":spForms.loadedFields.TipoAccion.value,
		        "c_level":spForms.loadedFields.C_level.value,
		        "vicepresidency":spForms.loadedFields.Vicepresidencia.value.label,
		        "direccion":spForms.loadedFields.Direccion.value,
		        "agrupadorHyperion":spForms.loadedFields.AgrupadorHyperion.value,
		        "gerencia":spForms.loadedFields.Gerencia.value,
		        "funcion":spForms.loadedFields.Funcion.value,
		        "oracle11":spForms.loadedFields.Oracle11.value,
		        "nombreOracle11":spForms.loadedFields.NombreOracle11.value,
		        "BPFinanzas":ctrlForm.global.bpFinanzas,
		        "responsable":ctrlForm.global.responsable,
		        "excepcionesDeFunciones":spForms.loadedFields.ExcepcionesDeFunciones.value,
		        "justificacionSolicitud":spForms.loadedFields.JustificacionSolicitud.value,
		        "comentarios":spForms.loadedFields.ComentariosFZ.value,
		        "centroDeCostos":spForms.loadedFields.CentroDeCostos.value,	        
		        "responsibleDevolution":spForms.loadedFields.ResponsableDevolucion.value,
		        "bpEmail":spForms.loadedFields.BPEMail.value		        
		    },
		    "state":spForms.loadedFields.EstadoSolicitudCentroCostos.value,
		    "approveBPUsers":ctrlForm.global.approveBPUsers,
		    "approveADUsers":ctrlForm.global.approveADUsers,
		    "approveGADUsers":ctrlForm.global.approveGADUsers,
		    "applyUsers":ctrlForm.global.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":ctrlForm.parameters.listNameConsecutive,
		    "idConsecutive":ctrlForm.parameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
		    "viewUrl":ctrlForm.parameters.viewUrl,
		    "editUrl":ctrlForm.parameters.editUrl,
		    "approveBPUrl":ctrlForm.parameters.approveBPUrl,
		    "approveADUrl":ctrlForm.parameters.approveADUrl,
		    "approveGADUrl":ctrlForm.parameters.approveGADUrl,		    
		    "applyUrl":ctrlForm.parameters.applyUrl,
		    "mensaje":ctrlForm.global.message
		}
		utilities.http.callRestService({
			url:ctrlForm.parameters.flowUrl,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	getUsers:function(){
		sputilities.getItems(ctrlForm.parameters.listUsersFlow,ctrlForm.parameters.queryUsers,
			function(data){				
				var data = utilities.convertToJSON(data);				
				if(data != null){
					var items = data.d.results;
					for(var i = 0; i < items.length ;i++){
						var item = items[i];											
						for(var j= 0; j < item.ResponsablesAbonos.results.length;j++){
							var user = item.ResponsablesAbonos.results[j];	
													
							switch(item.EtapaResponsable){
								case(ctrlForm.parameters.states[1]):
									ctrlForm.global.approveBPUsers += String.format(ctrlForm.parameters.formats.emailUsers,user.EMail);
								break;
								case(ctrlForm.parameters.states[2]):
									ctrlForm.global.approveADUsers += String.format(ctrlForm.parameters.formats.emailUsers,user.EMail);
								break;
								case(ctrlForm.parameters.states[3]):
									ctrlForm.global.approveGADUsers += String.format(ctrlForm.parameters.formats.emailUsers,user.EMail);
								break;
								case(ctrlForm.parameters.states[4]):
									ctrlForm.global.applyUsers += String.format(ctrlForm.parameters.formats.emailUsers,user.EMail);
								break;
							}
						}
					}	
				}				
			},
			function(xhr){
				console.log(xhr);
			}
		);
	},
	changeState:function(state){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio,state));
		if(radio != null){
			radio.click();
		}
	},
	hideNotAllowed:function(){
		var html = String.format(ctrlForm.parameters.formats.htmlNotAllowed,ctrlForm.parameters.messages.textNotAllowed);
		var formContent = document.querySelector(ctrlForm.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	},
	validateAllUsers:function(){
		if(spForms.currentItem.AuthorId != sputilities.contextInfo().userId || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
	},
	validateEditUser:function(){		
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[5])) == null;
		if(spForms.currentItem.AuthorId != sputilities.contextInfo().userId || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
		return true;
	},
	validateApplyUser:function(){			
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[4])) == null;
		if(ctrlForm.global.applyUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}		
		
	},
	validateApproveUser:function(){		
		var radio = false;
		var validUser = true;	
		switch(ctrlForm.global.formName){
			case(ctrlForm.parameters.formsStates.AprobarBP):
				radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[1])) == null;				
				validUser = spForms.loadedFields.BPEMail.value != sputilities.contextInfo().userEmail;
			break;
			case(ctrlForm.parameters.formsStates.ValidarAD):
				radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[2])) == null;				
				validUser = ctrlForm.global.approveADUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1;
			break;
			case(ctrlForm.parameters.formsStates.AprobarGerente):
				radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[3])) == null;
				validUser = ctrlForm.global.approveGADUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1;
			break;
		}
		if(validUser || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}	
	},
	hideComments:function(){
		var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.comments.staticName));
		if(ctrlComments != null){
			ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,true);
		}
	},
	loadReview:function(){
		var actions = [
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionReview;
		ctrlForm.loadEdit(actions,ctrlForm.validateAllUsers);
	},
	loadApply:function(){
		var actions = [
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textApplyForm,
				id:ctrlForm.parameters.messages.idApplyForm,
				success:function (){					
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[4])) != null;				
					ctrlForm.validateRequiredFields();						
					if(radio){
						ctrlForm.global.message = String.format(ctrlForm.parameters.formats.message,document.getElementById(ctrlForm.parameters.columns.centroDeCostos.staticName).value);
					}			
					ctrlForm.changeState(ctrlForm.parameters.states[6]);
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionApply;
		ctrlForm.loadEdit(actions,ctrlForm.validateApplyUser);

	},
	loadApprove:function(){
		var actions = [
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textApproveForm,
				id:ctrlForm.parameters.messages.idApproveForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					ctrlForm.validateRequiredFields();
					ctrlForm.getComplementaryFields();					
					ctrlForm.saverResponsibleEMail();														
					spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = false;				
					switch(spForms.loadedFields.EstadoSolicitudCentroCostos.value){
						case(ctrlForm.parameters.states[1]):
							ctrlForm.changeState(ctrlForm.parameters.states[2]);
						break;
						case(ctrlForm.parameters.states[2]):
							ctrlForm.changeState(ctrlForm.parameters.states[3]);
						break;
						case(ctrlForm.parameters.states[3]):
							ctrlForm.changeState(ctrlForm.parameters.states[4]);
						break;
					}
					
					return true;
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textRejectForm,
				id:ctrlForm.parameters.messages.idRejectForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					ctrlForm.validateRequiredFields();
					ctrlForm.saverResponsibleEMail();	
					spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
					document.getElementById(ctrlForm.parameters.columns.lastState.staticName).value = spForms.loadedFields.EstadoSolicitudCentroCostos.value;
					ctrlForm.changeState(ctrlForm.parameters.states[5]);
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.title = ctrlForm.parameters.titleApprove;
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
		ctrlForm.loadEdit(actions,ctrlForm.validateApproveUser);
	},
	loadEditApplicant:function(){
		var actions = [
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textEditForm,
				id:ctrlForm.parameters.messages.idEditForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					ctrlForm.validateRequiredFields(); 	
										
					switch (ctrlForm.global.lastState){
						case(ctrlForm.parameters.states[2]):
							ctrlForm.changeState(ctrlForm.parameters.states[2]);
							ctrlForm.global.approveADUsers = spForms.loadedFields.ResponsableDevolucion.value;							
						break;
						default:							
							ctrlForm.changeState(ctrlForm.parameters.states[1]);
						break;						
					}
					return true;					
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionEdit;
		ctrlForm.loadEdit(actions,function(){
			if(ctrlForm.validateEditUser()){
				ctrlForm.hideComments();				
				var ctrlComments = document.getElementById(ctrlForm.parameters.columns.comments.staticName);
				if(ctrlComments != null){
					ctrlComments.value = "";
				}
			}
		});
	},
	loadEdit:function(actions,callBack){
		ctrlForm.getUsers();
		spForms.options={
			actions:actions,
			title:ctrlForm.parameters.title,
			description:ctrlForm.parameters.description,
			contentTypeName:ctrlForm.parameters.contentTypeName,
			containerId:ctrlForm.parameters.containerId,
			allowAttachments:ctrlForm.parameters.allowAttachments,
		  	requiredAttachments:ctrlForm.parameters.requiredAttachments,
		  	maxAttachments:ctrlForm.parameters.maxAttachments,
		  	minAttachments:ctrlForm.parameters.minAttachments,
		  	maxSizeInMB:ctrlForm.parameters.maxSizeInMB,
			successLoadEditForm:function(){	
				ctrlForm.insertRequiredSymbol();
				ctrlForm.assignEventsToFields();
				ctrlForm.hideDependentsFields();
				ctrlForm.saveGlobalParameters();																
				ctrlForm.formIsLoaded = true;				
				if(typeof callBack== "function"){
					callBack();
				}
			} 
		};
		spForms.loadEditForm();	
	},
	loadNew:function(){	
		ctrlForm.getUsers();
		ctrlForm.global.createdBy = sputilities.contextInfo().userEmail;
		ctrlForm.global.created = moment.utc(new Date()).format(ctrlForm.parameters.formats.dateFormat);
		sputilities.callFunctionOnLoadBody(ctrlForm.parameters.functionLoadMyProperties);
		ctrlForm.loadNewForm();
	},
	loadNewForm:function(){
		spForms.options={
			actions:[
				{
					type:spForms.parameters.saveAction,
					name:ctrlForm.parameters.messages.textSaveForm,
					id:ctrlForm.parameters.messages.idSaveForm,
					success:function (){
						ctrlForm.callFlowService();
						return false;
					},
					error:function (){return true;},
					preSave: function (){
						ctrlForm.validateRequiredFields();
						ctrlForm.changeState(ctrlForm.parameters.states[0]);
						return true;
					}
				},
				{
					type:spForms.parameters.cancelAction,
					name:ctrlForm.parameters.messages.textCancelForm,
					id:ctrlForm.parameters.messages.idCancelForm,
					callback:null
				},
			],
			title:ctrlForm.parameters.title,
			description:ctrlForm.parameters.description,
			contentTypeName:ctrlForm.parameters.contentTypeName,
			containerId:ctrlForm.parameters.containerId,
			allowAttachments:ctrlForm.parameters.allowAttachments,
		  	requiredAttachments:ctrlForm.parameters.requiredAttachments,
		  	maxAttachments:ctrlForm.parameters.maxAttachments,
		  	minAttachments:ctrlForm.parameters.minAttachments,
		  	maxSizeInMB:ctrlForm.parameters.maxSizeInMB,
			successLoadNewForm:function(){
				ctrlForm.formIsLoaded = true;
				//ctrlForm.saveGlobalParameters();
				ctrlForm.insertRequiredSymbol();
				ctrlForm.assignEventsToFields();
				ctrlForm.hideDependentsFields();
				ctrlForm.hideComments();
			} 
		};
		spForms.loadNewForm();
	},
	loadMyProperties:function(){
		sputilities.getUserProfileProperties(sputilities.contextInfo().userEmail,
			function(userInfo){
				var objUser = utilities.convertToJSON(userInfo);
				if(objUser != null){
					var displayName = objUser.d.DisplayName;
					var email = objUser.d.Email;
					var cellPhone = ctrlForm.findProperty(objUser,ctrlForm.parameters.cellPhone);
					var validateFormIsLoaded = setInterval(function(){
						if(ctrlForm.formIsLoaded){
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.displayName.staticName,displayName);
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.email.staticName,email)
							clearInterval(validateFormIsLoaded);
						}
					},500);
				}
			},
			function(xhr){
				console.log(xhr)
			}
		);
	},
	setValueToControl:function(staticName,value){
		if(value != null){
			var ctrl = document.getElementById(staticName);
			if(ctrl != null){
				ctrl.value = value;
			}
		}
	},
	findProperty:function(objUser,name){
		var properties = objUser.d.UserProfileProperties.results;
		for(var i = 0; i < properties.length; i++){
			var property = properties[i];
			if(property.Key == name){
				return property.Value;
			}
		}
	},
	redirectCustomForm:function(name){
		window.location.href = window.location.href.replace(ctrlForm.global.formName,name);
	},	
	validateJustification:function(){
		var justification = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.justification.staticName));
		var actionCreate = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio,ctrlForm.parameters.columns.tipoAccion.values.crear));
		var actionModify = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio,ctrlForm.parameters.columns.tipoAccion.values.modificar));
		
		if(actionCreate.checked || actionModify.checked){
			spForms.loadedFields[ctrlForm.parameters.columns.justification.staticName].required = true;									
		}else{			
			spForms.loadedFields[ctrlForm.parameters.columns.justification.staticName].required = false;
			justification.classList.remove('has-error');
			justification.removeAttribute('sp-invalid','Esto no puede estar en blanco');
			justification.removeAttribute('sp-required','true');
			justification.setAttribute('sp-required','false');		
		}	
	},
	validateCostCenter:function(){
		var costCenter = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.centroDeCostos.staticName));
		var actionCreate = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio,ctrlForm.parameters.columns.tipoAccion.values.crear));		
		console.log(actionCreate.checked && ctrlForm.parameters.states[4] != spForms.loadedFields.EstadoSolicitudCentroCostos.value)
		
		
		if(actionCreate.checked && ctrlForm.parameters.states[4] != spForms.loadedFields.EstadoSolicitudCentroCostos.value){			
			spForms.loadedFields[ctrlForm.parameters.columns.centroDeCostos.staticName].required = false;
			costCenter.classList.remove('has-error');
			costCenter.removeAttribute('sp-invalid','Esto no puede estar en blanco');
			costCenter.removeAttribute('sp-required','true');
			costCenter.setAttribute('sp-required','false');
		}else{
			spForms.loadedFields[ctrlForm.parameters.columns.centroDeCostos.staticName].required = true;	
		}
	},
	validateBPUser:function(){
		var BPUserField = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.approveBP.staticName));
		var BPUserParameters = document.getElementById(ctrlForm.parameters.columns.approveBP.inputId).value;
				
		if(BPUserParameters.length > 2){
			var BPUserEmail = JSON.parse(BPUserParameters)[0].EntityData.Email;
			if(ctrlForm.global.approveBPUsers.split(';').indexOf(BPUserEmail) == -1){
				BPUserField.classList.add('has-error');
				BPUserField.removeAttribute('sp-invalid');
				BPUserField.setAttribute('sp-invalid','Este usuario no esta autorizado para la aprobación');										
			}else{	
				ctrlForm.setValueToControl(ctrlForm.parameters.columns.BPEMail.staticName,BPUserEmail);				
				BPUserField.classList.remove('has-error');
				BPUserField.removeAttribute('sp-invalid');			
			}
		}else{
			BPUserField.classList.add('has-error');
			BPUserField.removeAttribute('sp-invalid');
			BPUserField.setAttribute('sp-invalid','Esto no puede estar en blanco');			
		}
	},
	validateRequiredFields:function(){
		ctrlForm.validateJustification();
		ctrlForm.validateCostCenter();
		ctrlForm.validateBPUser();		
	},
	hideDependentsFields:function(){
		var costCenter = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.centroDeCostos.staticName));
		var justification = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.justification.staticName));
		var actionCreate = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio,ctrlForm.parameters.columns.tipoAccion.values.crear));
		var actionModify = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio,ctrlForm.parameters.columns.tipoAccion.values.modificar));	
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[4])) == null;
		
		
		switch(true){
			case(actionCreate.checked && radio):
				costCenter.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,true);	
				justification.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,false);
			break;
			case(actionModify.checked):
				justification.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,false);
				costCenter.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,false);
			break;			
			default:
				justification.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,true);	
				costCenter.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,false);	
				costCenter.style.
			break;	
		}		
	},
	assignEventsToFields:function(){
		var actionCreate = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio,ctrlForm.parameters.columns.tipoAccion.values.crear));
		var actionEnable = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio,ctrlForm.parameters.columns.tipoAccion.values.habilitar));
		var actionModify = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio,ctrlForm.parameters.columns.tipoAccion.values.modificar));
		var actionDisable = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio,ctrlForm.parameters.columns.tipoAccion.values.inhabilitar));
		actionCreate.setAttribute('onchange','ctrlForm.hideDependentsFields()');
		actionEnable.setAttribute('onchange','ctrlForm.hideDependentsFields()');
		actionModify.setAttribute('onchange','ctrlForm.hideDependentsFields()');
		actionDisable.setAttribute('onchange','ctrlForm.hideDependentsFields()');
	},
	insertRequiredSymbol:function(){
		var approveBP = document.getElementById(String.format(ctrlForm.parameters.formats.labelRowId,ctrlForm.parameters.columns.approveBP.staticName));
		approveBP.insertAdjacentHTML('beforeend', ctrlForm.parameters.formats.requiredForm);
	},
	saveGlobalParameters:function(){
		ctrlForm.global.createdBy = spForms.currentItem.Author.EMail;
		ctrlForm.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(ctrlForm.parameters.formats.dateFormat);
		ctrlForm.global.lastState = spForms.currentItem.EstadoAnterior;		
	},
	saverResponsibleEMail:function(){
		var responsibleUserField = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.responsible.staticName));
		var responsibleUserParameters = document.getElementById(ctrlForm.parameters.columns.responsible.inputId).value;
		var responsibleEMail = document.getElementById(ctrlForm.parameters.columns.ResponsibleDevolution.staticName);		
		var state = spForms.loadedFields.EstadoSolicitudCentroCostos.value;
		switch(state){
			case(ctrlForm.parameters.states[4] && responsibleUserParameters.length > 2):
				var value = JSON.parse(responsibleUserParameters)[0].EntityData.Email;
			break;
			case(ctrlForm.parameters.states[2]):
				var value = sputilities.contextInfo().userEmail; 
			break;
		}		
		ctrlForm.setValueToControl(ctrlForm.parameters.columns.ResponsibleDevolution.staticName,value);
	},
	getComplementaryFields:function(){
		var BPUserParameters = document.getElementById(ctrlForm.parameters.columns.approveBP.inputId).value;
		var responsibleUserParameters = document.getElementById(ctrlForm.parameters.columns.responsible.inputId).value;
		var requestDate = document.getElementById(ctrlForm.parameters.columns.requestDate.staticName).value
		
		ctrlForm.global.bpFinanzas = JSON.parse(BPUserParameters)[0].DisplayText;
		ctrlForm.global.responsable = JSON.parse(responsibleUserParameters)[0].DisplayText;
		ctrlForm.global.requestDate = requestDate;		
	}
	
}
ctrlForm.initialize();