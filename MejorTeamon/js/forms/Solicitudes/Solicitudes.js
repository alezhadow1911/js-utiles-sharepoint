﻿var Solicitudes = {
	//create parameters
	parameters:{
			IdEnviar: "Enviar",
			IdCancelar:"Cancelar",
			bodyItem:"<div class='contact'><p>Usted no ha aprobado el proceso para inscribirse a las capacitaciones de voluntarios.</p></div>",
			TitlePopUp:"Usuario No aprobado",
			listName: "Voluntarios",
			query:"?$select*&$filter=IDCreado eq {0}",
			resultItems:null,
	
	htmlRequiered:{
						StaticNameCorreo:'[sp-static-name="CorreoV"]',
						StaticNameCorreoTrabajo:'[sp-static-name="CorreoTrabajoV"]',
						input: 'input',
															
									
										
		},
			Style:{
		Block: "block",
		None: "none",
		//Atributtes
		Invalid:"sp-invalid",
		Message:"Formato de correo invalido"	
	},

			ID:{
		Correo: "CorreoV",	
		CorreoWork:"CorreoTrabajoV"
	},

		
		},

	
	Initialize:function(){
		try {
		Solicitudes.loadNewForm();
		}
		catch (e){
			console.log(e);
		}
	},
	
	loadNewForm:function(){
	spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:Solicitudes.parameters.IdEnviar,
			id:Solicitudes.parameters.IdEnviar,
			success:function(){return true;},
			error:function(){},
			preSave:function(){return true;}

		},
		{
			type:spForms.parameters.cancelAction,
			name:Solicitudes.parameters.IdCancelar,
			id:Solicitudes.parameters.IdCancelar,
			callback:""
		},
	],
	description:"Completa la información correspondiente",
	contentTypeName:"Voluntarios",
	containerId:"spForm",
			allowAttachments:true,
		  	requiredAttachments:true,
		  	maxAttachments:5,
		  	minAttachments:1,
		  			maxSizeInMB:3,
	successLoadNewForm:function(){
	Solicitudes.InitForm()
	} 
};
spForms.loadNewForm();

	},


	Message:function(){
		try {
		var SectionTwoPanel=document.getElementsByClassName('form-group row')[14];
		var DivTitle = document.createElement('div');
		DivTitle.setAttribute('class','form-header');
		var HTitle = document.createElement('h2');
		HTitle.textContent = "Información de emergencia";
		DivTitle.append(HTitle);
		SectionTwoPanel.insertBefore(DivTitle, SectionTwoPanel.childNodes[0])
		var Correo = document.querySelector(Solicitudes.parameters.htmlRequiered.StaticNameCorreo).querySelector(Solicitudes.parameters.htmlRequiered.input);
		var CorreoT = document.querySelector(Solicitudes.parameters.htmlRequiered.StaticNameCorreoTrabajo).querySelector(Solicitudes.parameters.htmlRequiered.input);
		//Correo.setAttribute('onchange','javascript:Solicitudes.validateEmailForm(this)');
		//CorreoT.setAttribute('onchange','javascript:Solicitudes.validateEmailForm(this)');

		}
		catch (e){
			console.log(e);
		}
	},
		InitForm:function(){
		try {
		Solicitudes.Message();
		Solicitudes.validateEmailForm();
		Solicitudes.validateEmailWork();

		}
		catch (e){
			console.log(e);
		}
	},
		validateEmailForm:function(){
		var input = document.getElementById(Solicitudes.parameters.ID.Correo);
			if(input != null){
				input.addEventListener('change',function(event){
				var email = this.value;
		  			if (Solicitudes.validateEmail(email)) {
		  				var staticName = "[sp-static-name="+Solicitudes.parameters.ID.Correo+"]";
		  				var InputSelect = document.querySelector(staticName);
   							InputSelect.removeAttribute(Solicitudes.parameters.Style.Invalid)
   			  				} 	
   			  		else{
		  				var staticName = "[sp-static-name="+Solicitudes.parameters.ID.Correo+"]";
		  				var InputSelect = document.querySelector(staticName);
   						InputSelect.setAttribute(Solicitudes.parameters.Style.Invalid,Solicitudes.parameters.Style.Message)
   			  			}
  				return false;
  					});
					}
	},
	validateEmailWork:function(){
		var input = document.getElementById(Solicitudes.parameters.ID.CorreoWork);
			if(input != null){
				input.addEventListener('change',function(event){
				var email = this.value;
		  			if (Solicitudes.validateEmail(email)) {
		  				var staticName = "[sp-static-name="+Solicitudes.parameters.ID.CorreoWork+"]";
		  				var InputSelect = document.querySelector(staticName);
   							InputSelect.removeAttribute(Solicitudes.parameters.Style.Invalid)
   			  				} 	
   			  		else{
		  				var staticName = "[sp-static-name="+Solicitudes.parameters.ID.CorreoWork+"]";
		  				var InputSelect = document.querySelector(staticName);
   						InputSelect.setAttribute(Solicitudes.parameters.Style.Invalid,Solicitudes.parameters.Style.Message)
   			  			}
  				return false;
  					});
					}
	},



		validateEmail:function(email){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 		return re.test(email);
 	 	},

	
}

Solicitudes.Initialize();