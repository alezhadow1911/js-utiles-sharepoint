﻿spForms.options={
	actions:[
		{
			type:spForms.parameters.updateAction,
			name:"Aprobar",
			id:"Aprobar",
			preSave:function(){
			var CurrentEstado = document.getElementById("Estado");
			if(CurrentEstado.value == "0"){
			CurrentEstado.value = "1";
			}else{
			if(CurrentEstado.value == "1"){
			CurrentEstado.value = "3";
			}
			}	
			return true},
			success:function(){return true},
			error:function(){return true}
		},
		{
			type:spForms.parameters.updateAction,
			name:"Rechazar",
			id:"enviar",
			preSave:function(){
			var CurrentEstado = document.getElementById("Estado");
			if(CurrentEstado.value == "1"){
			CurrentEstado.value = "4";
			}
			return true},
			success:function(){return true},
			error:function(){return true}
		},
		{
			type:spForms.parameters.cancelAction,
			name:"Cancelar",
			id:"cancelar",
			callback:""
		},
	],
	description:"Completa la información correspondiente",
	contentTypeName:"Voluntarios",
	containerId:"spForm",
	allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:5,
  	minAttachments:1,

	successLoadEditForm:function(){
		Solicitudes.initialize();
	} 
};
spForms.loadEditForm();

var Solicitudes = {
	//create parameters
	
	parameters:{
	query:"?$select*&$filter=ID eq 1",
	listName: "ParametrosSolicitudes",
	listNameJefe: "Solicitudes de Voluntarios",
	bodyItem:"<div class='contact'><p>Usted no tiene permisos para aprobar este formulario</p></div>",
	TitlePopUp:"Usuario Sin Permisos",

	},	
	fieldType:{
  	"0":{
    	Actualizar:function(){
    	Solicitudes.userJefeForm();
      	console.log("ingreso a Actualizar")
     	var ObservacionesJefe = document.querySelector('div[sp-static-name="ObservacionesJefe"]');
		ObservacionesJefe.style.display = "block";
		var TextObservacionesJefe = document.getElementById("ObservacionesJefe")
		TextObservacionesJefe.style.pointerEvents = "initial";
		TextObservacionesJefe.style.color = "#444";
		

      }
    },
    	"1":{
    	Actualizar:function(){
    	console.log("ingreso a aprobado jefe");
    	Solicitudes.userTHForm();
      	var ObservacionesTH = document.querySelector('div[sp-static-name="ObservacionesTH"]');
		ObservacionesTH.style.display = "block";
		var ObservacionesTH = document.querySelector('div[sp-static-name="ObservacionesJefe"]');
		ObservacionesTH.style.display = "block";

		var TextObservacionesTH = document.getElementById("ObservacionesTH")
		TextObservacionesTH.style.pointerEvents = "initial"
		TextObservacionesTH.style.color = "#444"
		

      }
    },	
    "2":{
    	Actualizar:function(){
      	var ObservacionesTH = document.querySelector('div[sp-static-name="ObservacionesJefe"]');
		ObservacionesTH.style.display = "block";
		var ObservacionesTH = document.querySelector('div[sp-static-name="ObservacionesTH"]');
		ObservacionesTH.style.display = "block";
		document.getElementById("btn-Aprobar").style.display ="none";
		document.getElementById("btn-enviar").style.display ="none";		
		document.getElementById("btn-cancelar").style.display ="none";
		

      }
    },
	
    	"5":{
    	Actualizar:function(){
      	var ObservacionesTH = document.querySelector('div[sp-static-name="ObservacionesJefe"]');
		ObservacionesTH.style.display = "block";
		var ObservacionesTH = document.querySelector('div[sp-static-name="ObservacionesTH"]');
		ObservacionesTH.style.display = "block";
		document.getElementById("btn-Aprobar").style.display ="none";
		document.getElementById("btn-enviar").style.display ="none";		
		document.getElementById("btn-cancelar").style.display ="none";
		

      }
    },

    	"3":{
    	Actualizar:function(){
    	Solicitudes.userTHForm()
      	console.log("ingreso a estado aprobado TH")
      	var ObservacionesJefe = document.querySelector('div[sp-static-name="ObservacionesJefe"]');
		ObservacionesJefe.style.display = "block";
		var ObservacionesTH = document.querySelector('div[sp-static-name="ObservacionesTH"]');
		ObservacionesTH.style.display = "block";
		
		document.getElementById("btn-Aprobar").style.display ="none";
		document.getElementById("btn-enviar").style.display ="none";		
		document.getElementById("btn-cancelar").style.display ="none";	
      }
    },


	},
	initialize:function(){
		try {
		var SectionTwoPanel=document.getElementsByClassName('form-group row')[14];
		var DivTitle = document.createElement('div');
		DivTitle.setAttribute('class','form-header');
		var HTitle = document.createElement('h2');
		HTitle.textContent = "Información de emergencia";
		DivTitle.appendChild(HTitle);
		SectionTwoPanel.insertBefore(DivTitle, SectionTwoPanel.childNodes[0])
		document.getElementById("VacunaV").disabled=true;
		console.log("ingreso a editado")
		var SelectEstado = document.getElementById("Estado").value;
		var spFormField = Solicitudes.fieldType[SelectEstado].Actualizar()
		}
		catch (e){
			console.log(e);
		}
	},
	userTHForm:function(){
		try {
		var UserEmailCurrent = _spPageContextInfo.userEmail
			//adding parameters to query
		var linkQuery = String.format(Solicitudes.parameters.query);
		
			sputilities.getItems(Solicitudes.parameters.listName,linkQuery,function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				for (var i=0; i<resultItems.length; i++){
				var CurrrentItem = resultItems[i];
				var Correo = CurrrentItem.Correo;
				var UserEmailCurrent = _spPageContextInfo.userEmail;
				if(Correo == UserEmailCurrent){
				document.getElementById('spForm').style.display="block";
				document.getElementById('Mensaje').style.display="block";
				console.log("usuario con permisos")
				}
				else{
				console.log("usuario sin permisos")
				document.getElementById('spForm').style.display="none";
				document.getElementById('Mensaje').style.display="none";	
				Solicitudes.InitPopUp();			
				}
				}
			});


		}
		catch (e){
			console.log(e);
		}
	},
	userJefeForm:function(){
		try {
			var idUserCreated = spForms.currentItem.AuthorId;
			//adding parameters to query
			var linkQuery = String.format(Solicitudes.parameters.query);
			sputilities.getUserInformation(idUserCreated,function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d;
				var Email = resultItems.EMail;
				console.log(resultItems)
			sputilities.getUserProfileProperty('Manager',Email,function(data){
				var resultProperty = JSON.parse(data).d;
				var EmailJefe = resultProperty.GetUserProfilePropertyFor;
				var CurrentJefe = EmailJefe.split("|")[2];
				var CurrentUser =_spPageContextInfo.userEmail;
				
				if(CurrentJefe == CurrentUser ){
				document.getElementById('spForm').style.display="block";
				document.getElementById('Mensaje').style.display="block";
				}else{
				document.getElementById('spForm').style.display="none";
				document.getElementById('Mensaje').style.display="none";
				Solicitudes.InitPopUp();
				}
				console.log(resultProperty)
	
				
			});

			});


		}
		catch (e){
			console.log(e);
		}
	},

		InitPopUp:function(){
		try {
		
				sputilities.showModalDialog(Solicitudes.parameters.TitlePopUp,null,null,Solicitudes.parameters.bodyItem,function(data){
				console.log("ingresa al pop up")				
			});
		}
		catch (e){
			console.log(e);
		}
	},


	
}