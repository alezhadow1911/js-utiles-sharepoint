﻿spForms.options={
	actions:[
		{
			type:spForms.parameters.cancelAction,
			name:"Cancelar",
			id:"cancelar",
			callback:""
		},
	],
	description:"Completa la información correspondiente",
	contentTypeName:"Voluntarios",
	containerId:"spForm",
			allowAttachments:true,
		  	requiredAttachments:true,
		  	maxAttachments:5,
		  	minAttachments:1,
		  			maxSizeInMB:3,

	successLoadEditForm:function(){
		Solicitudes.initialize();
	} 
};
spForms.loadEditForm();

var Solicitudes = {
	//create parameters
	parameters:{
	
	},	
	initialize:function(){
		try {
		var SectionTwoPanel=document.getElementsByClassName('form-group row')[14];
		var DivTitle = document.createElement('div');
		DivTitle.setAttribute('class','form-header');
		var HTitle = document.createElement('h2');
		HTitle.textContent = "Información de emergencia";
		DivTitle.append(HTitle);
		SectionTwoPanel.insertBefore(DivTitle, SectionTwoPanel.childNodes[0])
		document.getElementById("VacunaV").disabled=true;
		}
		catch (e){
			console.log(e);
		}
	},
	
}