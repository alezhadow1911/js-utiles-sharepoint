﻿var ctrlForm = {
	parameters:{
		formats:{
			"queryStaticNameRow":"[sp-static-name='{0}']",
			"htmlNotAllowed":"<div><h4>{0}</h4></div> <div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
			"querySelectRadio":'input[type="radio"][value="{0}"]:checked',
			"queryRadio":'input[type="radio"][value="{0}"]',
			"emailUsers":"{0};",
			"dateFormat":"DD-MM-YYYY",
			"formFormat":"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}"
		},
		forms:{
			"NewForm":"ctrlForm.redirectCustomForm('NuevaSolicitud')",
			"EditForm":"ctrlForm.redirectCustomForm('CorregirSolicitud')",
			"DispForm":"ctrlForm.redirectCustomForm('VerSolicitud')",
			"NuevaSolicitud":"ctrlForm.loadNew()",
			"Jefe":"ctrlForm.loadApprove()",
			"CorregirSolicitud":"ctrlForm.loadEditApplicant()",
			"TH":"ctrlForm.loadApply()",
			"VerSolicitud":"ctrlForm.loadReview()",
			"PendientesAprobar":"",
			"Aprobadas":""
		},
		messages:{
			"idSaveForm":"enviarSolicitud",
			"idCancelForm":"cancelar",
			"idEditForm":"enviarCorreccion",
			"idApproveForm":"aprobar",
			"idRejectForm":"solicitarCorreccion",
			"idApplyForm":"aplicarNovedad",
			"textSaveForm":"Enviar",
			"textCancelForm":"Cancelar",
			"textEditForm":"Enviar",
			"textApproveForm":"Aprobar",
			"textRejectForm":"Rechazar",
			"textApplyForm":"Novedad Aplicada",
			"textNotAllowed":"Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior"
		},
		columns:{
			"comments":{
				"staticName":"ObservacionesJefe"
			},
			"commentsTH":{
				"staticName":"ObservacionesTH"
			},

			"displayName":{
				"staticName":"NombrePersona1"
			},
			"idDoc":{
				"staticName":"NombrePersona1"
			},
			"cellPhone":{
				"staticName":"NumeroCelular"
			},
			"email":{
				"staticName":"CorreoElectronico"
			}
		},
		states:[
			"Solicitado",
			"Jefe",
			"Aprobado Jefe",
			"Rechazado Jefe",
			"Aprobado TH",
			"Rechazado TH"
			
			


		]
		,
		parametersHTML:{
			"spHidden":"sp-hidden",
			"spDisable":"sp-disable"
		},
		ID:{
			ContentClose: ".col-md-6.col-xs-6.delete-doc",
			ContentAdd: "a#sp-forms-add-attachment",
			Attachment:"div[sp-static-name='sp-forms-attachment']",
			Correo: "CorreoV",
			Comments:"div[sp-static-name='ObservacionesJefe']",
			CorreoWork:"CorreoTrabajoV"	

		},
		style:{
			None: "none",
			Opacity: "0.7",
			Block: "block",
			//Atributtes
			Invalid:"sp-invalid",
			Message:"Formato de correo invalido"
		},
		selectorFormContent:".form-content",
		cellPhone:"CellPhone",
		functionLoadMyProperties:"ctrlForm.loadMyProperties",
		containerId:"container-form",
		description:"Complete la información para la solicitud",
		descriptionEdit:"Corrija la información de la solicitud",
		descriptionApply:"Revise la información y aplique la novedad si corresponde",
		descriptionApprove:"Revise la información y apruebe o envíe a corrección según corresponda",
		descriptionReview:"Resultado de la solicitud",
		contentTypeName:"Voluntarios",
		formIsLoaded:false,
		allowAttachments:true,
		requiredAttachments:true,
		maxAttachments:20,
		minAttachments:0,
		maxSizeInMB:3,
		listNameConsecutive:"ControlRadicados",
		idConsecutive:12,
		listUsersFlow:"ResponsablesTH",
		listNameTH:"ParametrosSolicitudes",
		queryTH:"?$filter=ID eq 1",
		viewUrl:"",
	    editUrl:"",
	    approveUrl:"",
	    applyUrl:"",
	    listViewApply:"",
	    listViewApprove:"",
	    queryUsers:"?$filter=FlujoAsociadoId eq 12&$select=ResponsablesAbonos/EMail,EtapaResponsable&$expand=ResponsablesAbonos",
	    flowUrl:"https://prod-46.westus.logic.azure.com:443/workflows/d3e766a1fc88402b94becf043662d2ab/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=11TjmiTLtdcEpWd2JNivFaIXlk34S70tdXyFg2hDMHo"

	},
	global:{
		formName:null,
		createdBy:null,
		created:null,
		approveUsers:"",
		applyUsers:"",
		applyManager:"",
		applyTH:""
	},
	initialize:function(){
		var partsOfUrl = window.location.href.split('?')[0].split('/');
		ctrlForm.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];
		eval(ctrlForm.parameters.forms[ctrlForm.global.formName]);
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
		ctrlForm.parameters.viewUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[7],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.editUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[5],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.approveUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.applyUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[6],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.listViewApply = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[9]);
	    ctrlForm.parameters.listViewApprove= String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[8]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":ctrlForm.global.createdBy,
		    	"nombres":spForms.loadedFields.PrimerNombreV.value,
		    	"apellidos":spForms.loadedFields.PrimerApellidoV.value,
		    	"nombreCompleto":spForms.loadedFields.PrimerApellidoV.value+" "+spForms.loadedFields.PrimerNombreV.value,
		    	"cedula":spForms.loadedFields.CedulaV.value,
		    	"telefono":spForms.loadedFields.NumeroTelefonoV.value,
		    	"estacion":spForms.loadedFields.EstacionTrabajoV.value,
		    	"area":spForms.loadedFields.AreaV.value,
		    	
		    	
		    	
		    	"ItemID":spForms.currentItemID,
		        "comentarios":spForms.loadedFields.ObservacionesJefe.value,
    		    "comentariosTH":spForms.loadedFields.ObservacionesTH.value,
		        
		        "area":spForms.loadedFields.AreaV.value,
				"cedula":Math.round(spForms.loadedFields.CedulaV.value),
				"correo":spForms.loadedFields.CorreoTrabajoV.value,
				"correop":spForms.loadedFields.CorreoV.value,
				"estacion":spForms.loadedFields.EstacionTrabajoV.value,
				"fechap":spForms.loadedFields.FechaPasaporteV.value,
				"fechaa":spForms.loadedFields.FechaVisaV.value,
				"nombreEmergencia":spForms.loadedFields.NombreContactoEmergencia.value,
				"numeroTelefono":Math.round(spForms.loadedFields.NumeroTelefonoV.value),
				"parentesco":spForms.loadedFields.Parentesco.value,
				"vacunaV":spForms.loadedFields.VacunaV.value,
				"primerApellido":spForms.loadedFields.PrimerApellidoV.value,
				"primerNombre":spForms.loadedFields.PrimerNombreV.value,
				"segundoApellido":spForms.loadedFields.SegundoApellidoV.value,
				"SegundoNombre":spForms.loadedFields.SegundoNombreV.value,
				"nombreCompleto":spForms.loadedFields.PrimerNombreV.value+" "+spForms.loadedFields.SegundoNombreV.value+" "+spForms.loadedFields.PrimerApellidoV.value+" "+spForms.loadedFields.SegundoApellidoV.value,
				"telefonoEmergencia":Math.round(spForms.loadedFields.TelefonocontactoEmergencia.value),
				"telefonoOficina":Math.round(spForms.loadedFields.TelefonoOficinaV.value),
		    	//"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        //"base":spForms.loadedFields.BasePersona.value.label,
		        //"fechaCreacion":ctrlForm.global.created,
		        //"comentarios":spForms.loadedFields.ComentariosTH.value,
		        //"Correo":spForms.listInfo.title
		    },
		    "state":spForms.loadedFields.Estado	.value,
		    "titleCorreo":spForms.listInfo.title,  	
		    "approveUsers":ctrlForm.global.approveUsers,
		    "applyUsers":ctrlForm.global.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":ctrlForm.parameters.listNameConsecutive,
		    "idConsecutive":ctrlForm.parameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
		    "viewUrl":ctrlForm.parameters.viewUrl,
		    "editUrl":ctrlForm.parameters.editUrl,
		    "approveUrl":ctrlForm.parameters.approveUrl,
		    "applyUrl":ctrlForm.parameters.applyUrl,
		    "listViewApply":ctrlForm.parameters.listViewApply,
		    "listViewApprove":ctrlForm.parameters.listViewApprove	
		}
		utilities.http.callRestService({
			url:ctrlForm.parameters.flowUrl,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	getUsers:function(){
		sputilities.getItems(ctrlForm.parameters.listUsersFlow,ctrlForm.parameters.queryUsers,
			function(data){
				var data = utilities.convertToJSON(data);
				if(data != null){
					var items = data.d.results;
					for(var i = 0; i < items.length ;i++){
						var item = items[i];
						for(var j= 0; j < item.ResponsablesAbonos.results.length;j++){
							var user = item.ResponsablesAbonos.results[j];
							if(item.EtapaResponsable == ctrlForm.parameters.states[2]){
								ctrlForm.global.applyUsers += String.format(ctrlForm.parameters.formats.emailUsers,user.EMail);
							}else{
								ctrlForm.global.approveUsers += String.format(ctrlForm.parameters.formats.emailUsers,user.EMail);
							}
						}
					}	
				}
			},
			function(xhr){
				console.log(xhr);
			}
		);
	},
	validateEmailForm:function(){
		var input = document.getElementById(ctrlForm.parameters.ID.Correo);
			if(input != null){
				input.addEventListener('change',function(event){
				var email = this.value;
		  			if (ctrlForm.validateEmail(email)) {
		  				var staticName = "[sp-static-name="+ctrlForm.parameters.ID.Correo+"]";
		  				var InputSelect = document.querySelector(staticName);
   							InputSelect.removeAttribute(ctrlForm.parameters.style.Invalid)
   			  				} 	
   			  		else{
		  				var staticName = "[sp-static-name="+ctrlForm.parameters.ID.Correo+"]";
		  				var InputSelect = document.querySelector(staticName);
   						InputSelect.setAttribute(ctrlForm.parameters.style.Invalid,ctrlForm.parameters.style.Message)
   			  			}
  				return false;
  					});
					}
	},
	validateEmailWork:function(){
		var input = document.getElementById(ctrlForm.parameters.ID.CorreoWork);
			if(input != null){
				input.addEventListener('change',function(event){
				var email = this.value;
		  			if (ctrlForm.validateEmail(email)) {
		  				var staticName = "[sp-static-name="+ctrlForm.parameters.ID.CorreoWork+"]";
		  				var InputSelect = document.querySelector(staticName);
   							InputSelect.removeAttribute(ctrlForm.parameters.style.Invalid)
   			  				} 	
   			  		else{
		  				var staticName = "[sp-static-name="+ctrlForm.parameters.ID.CorreoWork+"]";
		  				var InputSelect = document.querySelector(staticName);
   						InputSelect.setAttribute(ctrlForm.parameters.style.Invalid,ctrlForm.parameters.style.Message)
   			  			}
  				return false;
  					});
					}
	},
	validateEmail:function(email){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 			return re.test(email);
 	 	},
	changeState:function(state){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio,state));
		if(radio != null){
			radio.click();
		}
	},
	hideNotAllowed:function(){
		var html = String.format(ctrlForm.parameters.formats.htmlNotAllowed,ctrlForm.parameters.messages.textNotAllowed);
		var formContent = document.querySelector(ctrlForm.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	},
	validateUserCreated:function(){
		if(spForms.currentItem.AuthorId != sputilities.contextInfo().userId){
			ctrlForm.hideNotAllowed();
			return false;
		}
		ctrlForm.hideAttachments();
	},
	validateAllUsers:function(){
		if(spForms.currentItem.AuthorId != sputilities.contextInfo().userId || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
	},
	validateEditUser:function(){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[4])) == null;
		if(spForms.currentItem.AuthorId != sputilities.contextInfo().userId || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
		return true;
	},
	//Novedades
	validateApplyUser:function(){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[2])) == null;
		if(ctrlForm.global.applyUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
		ctrlForm.hideAttachments();
	},
	//Ventanilla TH
	validateApproveUser:function(){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[1])) == null;
		if(ctrlForm.global.approveUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}	
		ctrlForm.hideAttachments();
	},
	validateApplyManager:function(){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[1])) == null;
		if(ctrlForm.global.applyManager != sputilities.contextInfo().userEmail || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
		ctrlForm.hideAttachments();
		//ctrlForm.hideDate();
	},
		validateApplyTH:function(){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[2])) == null;
		if(ctrlForm.global.applyTH != sputilities.contextInfo().userEmail || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
		ctrlForm.hideAttachments();
		//ctrlForm.hideDate();
	},

	hideAttachments:function(){
		var i;
		var CloaseAttachments = document.querySelectorAll(ctrlForm.parameters.ID.ContentClose);
			for(i=0; i<CloaseAttachments.length; i++){
			CloaseAttachments[i].style.display = ctrlForm.parameters.style.None;
			}
		document.querySelector(ctrlForm.parameters.ID.ContentAdd).style.display = ctrlForm.parameters.style.None;
		document.querySelector(ctrlForm.parameters.ID.Attachment).style.opacity = ctrlForm.parameters.style.Opacity;
	},

	hideComments:function(){
		var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.comments.staticName));
		if(ctrlComments != null){
			ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,true);
		}
	},
	blockedComments:function(){
		var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.comments.staticName));
		if(ctrlComments != null){
			ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,true);
		}
	},

	loadReview:function(){
		var actions = [
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionReview;
		ctrlForm.loadEdit(actions,ctrlForm.validateUserCreated);
	},
	loadApply:function(){
		var actions = [
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textApproveForm,
				id:ctrlForm.parameters.messages.idApproveForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					ctrlForm.changeState(ctrlForm.parameters.states[4]);
					return true;
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textRejectForm,
				id:ctrlForm.parameters.messages.idRejectForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					spForms.loadedFields[ctrlForm.parameters.columns.commentsTH.staticName].required = true;
					ctrlForm.changeState(ctrlForm.parameters.states[5]);
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionApply;
		ctrlForm.loadEdit(actions,null);//revisar

	},
	loadApprove:function(){
		var actions = [
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textApproveForm,
				id:ctrlForm.parameters.messages.idApproveForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
				ctrlForm.UnlockedComments();
					spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = false;
					ctrlForm.changeState(ctrlForm.parameters.states[2]);
					return true;
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textRejectForm,
				id:ctrlForm.parameters.messages.idRejectForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
					ctrlForm.changeState(ctrlForm.parameters.states[3]);
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
		ctrlForm.loadEdit(actions,null);

	},
	loadEditApplicant:function(){
		var actions = [
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textEditForm,
				id:ctrlForm.parameters.messages.idEditForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					ctrlForm.changeState(ctrlForm.parameters.states[1]);
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionEdit;
		ctrlForm.loadEdit(actions,function(){
			if(ctrlForm.validateEditUser()){
			ctrlForm.hideComments();
				var ctrlComments = document.getElementById(ctrlForm.parameters.columns.comments.staticName);
				if(ctrlComments != null){
					ctrlComments.value = "";
				}
			}
		});
	},
	loadEdit:function(actions,callBack){
		ctrlForm.getUsers();
		spForms.options={
			actions:actions,
			description:ctrlForm.parameters.description,
			contentTypeName:ctrlForm.parameters.contentTypeName,
			containerId:ctrlForm.parameters.containerId,
			allowAttachments:ctrlForm.parameters.allowAttachments,
		  	requiredAttachments:ctrlForm.parameters.requiredAttachments,
		  	maxAttachments:ctrlForm.parameters.maxAttachments,
		  	minAttachments:ctrlForm.parameters.minAttachments,
		  	maxSizeInMB:ctrlForm.parameters.maxSizeInMB,
			successLoadEditForm:function(){	
				ctrlForm.global.createdBy = spForms.currentItem.Author.EMail;
				ctrlForm.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(ctrlForm.parameters.formats.dateFormat);
				ctrlForm.formIsLoaded = true;
				ctrlForm.Message();
				if(ctrlForm.global.formName == "Jefe"){
				ctrlForm.getManagerTotal();
				}
				if(ctrlForm.global.formName == "TH"){
				ctrlForm.GetTH();
				}
				
				if(typeof callBack== "function"){
					callBack();
				}
			} 
		};
		spForms.loadEditForm();

	},
	loadNew:function(){
		ctrlForm.getUsers();
		ctrlForm.global.createdBy = sputilities.contextInfo().userEmail;
		ctrlForm.global.created = moment.utc(new Date()).format(ctrlForm.parameters.formats.dateFormat);
		sputilities.callFunctionOnLoadBody(ctrlForm.parameters.functionLoadMyProperties);
		ctrlForm.loadNewForm();
	},
	loadNewForm:function(){
		spForms.options={
			actions:[
				{
					type:spForms.parameters.saveAction,
					name:ctrlForm.parameters.messages.textSaveForm,
					id:ctrlForm.parameters.messages.idSaveForm,
					success:function (){
						ctrlForm.callFlowService();
						return false;
					},
					error:function (){return true;},
					preSave: function (){
						ctrlForm.changeState(ctrlForm.parameters.states[0]);
						return true;
					}
				},
				{
					type:spForms.parameters.cancelAction,
					name:ctrlForm.parameters.messages.textCancelForm,
					id:ctrlForm.parameters.messages.idCancelForm,
					callback:null
				},
			],
			description:ctrlForm.parameters.description,
			contentTypeName:ctrlForm.parameters.contentTypeName,
			containerId:ctrlForm.parameters.containerId,
			allowAttachments:ctrlForm.parameters.allowAttachments,
		  	requiredAttachments:ctrlForm.parameters.requiredAttachments,
		  	maxAttachments:ctrlForm.parameters.maxAttachments,
		  	minAttachments:ctrlForm.parameters.minAttachments,
		  	maxSizeInMB:ctrlForm.parameters.maxSizeInMB,
			successLoadNewForm:function(){
				ctrlForm.formIsLoaded = true;
				ctrlForm.hideComments();
				ctrlForm.Message();
				ctrlForm.validateEmailForm();
				ctrlForm.validateEmailWork();

			} 
		};
		spForms.loadNewForm();
	},
	loadMyProperties:function(){
		sputilities.getUserProfileProperties(sputilities.contextInfo().userEmail,
			function(userInfo){
				var objUser = utilities.convertToJSON(userInfo);
				if(objUser != null){
					var displayName = objUser.d.DisplayName;
					var email = objUser.d.Email;
					var cellPhone = ctrlForm.findProperty(objUser,ctrlForm.parameters.cellPhone);
					var validateFormIsLoaded = setInterval(function(){
						if(ctrlForm.formIsLoaded){
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.displayName.staticName,displayName);
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.email.staticName,email);
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.cellPhone.staticName,cellPhone);
							clearInterval(validateFormIsLoaded);
						}
					},500);
				}
			},
			function(xhr){
				console.log(xhr)
			}
		);
	},
	setValueToControl:function(staticName,value){
		if(value != null){
			var ctrl = document.getElementById(staticName);
			if(ctrl != null){
				ctrl.value = value;
			}
		}
	},
		Message:function(){
		try {
		var SectionTwoPanel=document.getElementsByClassName('form-group row')[14];
		var DivTitle = document.createElement('div');
		DivTitle.setAttribute('class','form-header');
		var HTitle = document.createElement('h2');
		HTitle.textContent = "Información de emergencia";
		DivTitle.append(HTitle);
		SectionTwoPanel.insertBefore(DivTitle, SectionTwoPanel.childNodes[0])
		var Correo = document.querySelector(Solicitudes.parameters.htmlRequiered.StaticNameCorreo).querySelector(Solicitudes.parameters.htmlRequiered.input);
		var CorreoT = document.querySelector(Solicitudes.parameters.htmlRequiered.StaticNameCorreoTrabajo).querySelector(Solicitudes.parameters.htmlRequiered.input);
		//Correo.setAttribute('onchange','javascript:Solicitudes.validateEmailForm(this)');
		//CorreoT.setAttribute('onchange','javascript:Solicitudes.validateEmailForm(this)');

		}
		catch (e){
			console.log(e);
		}
	},

	findProperty:function(objUser,name){
		var properties = objUser.d.UserProfileProperties.results;
		for(var i = 0; i < properties.length; i++){
			var property = properties[i];
			if(property.Key == name){
				return property.Value;
			}
		}
	},
		GetTH:function(){
		sputilities.getItems(ctrlForm.parameters.listNameTH,ctrlForm.parameters.queryTH,function(data){
			
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				
				for (var i=0; i< resultItems.length; i++){
					
					var correo = resultItems[i].Correo;
					ctrlForm.global.applyTH = correo;
					ctrlForm.validateApplyTH();

					}
					});

		},

	getManagerTotal:function(){
			var idUserCreated = spForms.currentItem.AuthorId;
			sputilities.getUserInformation(idUserCreated,function(data){
				var resultItems = JSON.parse(data).d;
				var Email = resultItems.EMail;
			sputilities.getUserProfileProperty('Manager',Email,
			function(data){
				var resultProperty = JSON.parse(data).d;
				var EmailJefe = resultProperty.GetUserProfilePropertyFor;
				var CurrentJefe = EmailJefe.split("|")[2];
				ctrlForm.global.applyManager = CurrentJefe;
				ctrlForm.validateApplyManager();
			});	
			});	
			},
				UnlockedComments:function(){
	spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = false;
					var uno = document.querySelector(ctrlForm.parameters.ID.Comments);
					console.log(uno)
					uno.removeAttribute(ctrlForm.parameters.style.Invalid);
	
	},

				


	redirectCustomForm:function(name){
		window.location.href = window.location.href.replace(ctrlForm.global.formName,name);
	}
}
ctrlForm.initialize();