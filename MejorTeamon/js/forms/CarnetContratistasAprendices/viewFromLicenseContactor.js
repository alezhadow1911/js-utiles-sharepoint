﻿var licenseFrom = {
	parameters:{
		buttonsNames:{
			btnCancel:"Cancelar"
		},
		buttonsId:{
			btnCancel:"cancelar"
		},
		titleForm:"Solicitudes Carnet Contratistas y Aprendices",
		contentTypeNameForm:"Carnet Contratistas y Aprendices",
		containerIdForm:"spForm"
	},
	initialize: function(){
		spForms.loadEditForm();
	}
};

spForms.options = {
	actions:[		
		{
			type:spForms.parameters.cancelAction,
			name:licenseFrom.parameters.buttonsNames.btnCancel,
			id:licenseFrom.parameters.buttonsId.btnCancel,
			callback:""
		}
	],	
	title:licenseFrom.parameters.titleForm,
	contentTypeName:licenseFrom.parameters.contentTypeNameForm,
	containerId:licenseFrom.parameters.containerIdForm,
	allowAttachments:true,
  	requiredAttachments:false,
	successLoadEditForm:function(){
	} 
};
licenseFrom.initialize();