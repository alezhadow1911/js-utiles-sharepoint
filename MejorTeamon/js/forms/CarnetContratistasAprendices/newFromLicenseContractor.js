﻿var licenseForm = {
	parameters:{
		fields:[{field:"NombrePersona1"},{field:"NumeroCelular"},{field:"CorreoElectronico"},{field:"Cargo"}],
		internalNameFields:{
			NombrePersona:"NombrePersona1",
			NumeroCelular:"NumeroCelular",
			CorreoElectronico:"CorreoElectronico",
			Cargo:"Cargo"
		},
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		selectorHTML:"div[sp-static-name='{0}']",
		messagesInvalid:{
		 CorreoElectronico:"El correo electrónico no tiene el formato correcto"
		},
		errorClass:"has-error",
		selectorError:"help-block-error-message-{0}",
		buttonsNames:{
			btnSend:"Enviar",
			btnCancel:"Cancelar"
		},
		buttonsId:{
			btnSend:"enviar",
			btnCancel:"cancelar"
		},
		flowParameters:{
			urlFlow:"https://prod-55.westus.logic.azure.com:443/workflows/9322cd0b2c44415e9c8d468211ca1dd6/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=wJ9WkKJxu3ITfNRzV656-ZhMTheB1KIbDsF9mA5SVyA",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",
	   		approveBossUrl:"",
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Carnet Contratistas y Aprendices Ventanilla TH",
	    	applyUsers:"Carnet Contratistas y Aprendices Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:6,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Carnet Contratistas y Aprendices"
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud Ventanilla TH",
			"AplicarSolicitud":"Aplicar Novedad",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Solicitudes pendientes Novedades":"Vista Novedades",
			"Solicitudes pendientes Ventanilla TH":"Vista Ventanilla TH"
		},
		global:{
			createdBy:null,
			created:null
		},
		descriptionForm:"Completa la información correspondiente",
		contentTypeNameForm:"Carnet Contratistas y Aprendices",
		containerIdForm:"spForm",
		loadUserInfo:"licenseForm.loadUserInfo",
		regexEmail:"/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/",
		resultItems:null
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
			
	    licenseForm.parameters.flowParameters.approveUrl = String.format(licenseForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(licenseForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    licenseForm.parameters.flowParameters.applyUrl = String.format(licenseForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(licenseForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    licenseForm.parameters.flowParameters.editUrl = String.format(licenseForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(licenseForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    licenseForm.parameters.flowParameters.listViewApply = String.format(licenseForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(licenseForm.parameters.forms)[5]);
	    licenseForm.parameters.flowParameters.listViewApprove= String.format(licenseForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(licenseForm.parameters.forms)[6]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":licenseForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"cargo":spForms.loadedFields.Cargo.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.Radicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":licenseForm.parameters.global.created,
		        "comentarios":spForms.loadedFields.ComentariosTH.value,
		     },
		    "state":spForms.loadedFields.EstadoCarnet.value,
		    "approveUsers":licenseForm.parameters.flowParameters.approveUsers,
		    "applyUsers":licenseForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":licenseForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":licenseForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":licenseForm.parameters.flowParameters.listNameAssing,
		    "editUrl":licenseForm.parameters.flowParameters.editUrl,
		    "approveUrl":licenseForm.parameters.flowParameters.approveUrl,
		    "applyUrl":licenseForm.parameters.flowParameters.applyUrl,
		    "listViewApply":licenseForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":licenseForm.parameters.flowParameters.listViewApprove
		}
		utilities.http.callRestService({
			url:licenseForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize: function(){
		sputilities.callFunctionOnLoadBody(licenseForm.parameters.loadUserInfo);
		spForms.loadNewForm();
	},
	loadUserInfo:function(){
		try {
			//running the query with parameters
			sputilities.getMyUserProfileProperties("",function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d;
				licenseForm.parameters.resultItems = resultItems;
				var ctrlInterval = setInterval(function(){
					if(licenseForm.parameters.formsIsLoaded)
					{
						licenseForm.setValuesUserInfo();						
						clearInterval(ctrlInterval);
					}
				},500);
				
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	setValuesUserInfo:function(){
		var displayName = document.getElementById(licenseForm.parameters.fields[0].field);
		var cellPhone = document.getElementById(licenseForm.parameters.fields[1].field);
		var email = document.getElementById(licenseForm.parameters.fields[2].field);
		var title = document.getElementById(licenseForm.parameters.fields[3].field);
		
		if (displayName != null && cellPhone!= null && email != null && title != null){
		  	displayName.value = licenseForm.parameters.resultItems.DisplayName ? licenseForm.parameters.resultItems.DisplayName: "";
		  	cellPhone.value = licenseForm.parameters.resultItems.CellPhone ? licenseForm.parameters.resultItems.CellPhone: "";
		  	email.value = licenseForm.parameters.resultItems.Email ? (licenseForm.parameters.resultItems.Email).toLowerCase(): "";
		  	title.value = licenseForm.parameters.resultItems.Title ? licenseForm.parameters.resultItems.Title: "";
		  	
		  	document.querySelector(String.format(licenseForm.parameters.selectorHTML,licenseForm.parameters.internalNameFields.CorreoElectronico)).onchange = function(){
				
				var emailPerson = email.value;
					
				if(emailPerson != ""){
					if (!licenseForm.validateEmail(emailPerson)){
					
						document.querySelector(String.format(licenseForm.parameters.selectorHTML,licenseForm.parameters.internalNameFields.CorreoElectronico)).classList.add(licenseForm.parameters.errorClass);
						document.getElementById(String.format(licenseForm.parameters.selectorError,licenseForm.parameters.internalNameFields.CorreoElectronico)).textContent = licenseForm.parameters.messagesInvalid.CorreoElectronico;
						
					}else{
					
						document.querySelector(String.format(licenseForm.parameters.selectorHTML,licenseForm.parameters.internalNameFields.CorreoElectronico)).classList.remove(licenseForm.parameters.errorClass);
						document.getElementById(licenseForm.parameters.selectorError,licenseForm.parameters.internalNameFields.CorreoElectronico).textContent ="";
					}
				}
			}
		}
		displayName.disabled = true;
	},
	validateEmail: function (email){
		 var re = new RegExp(licenseForm.parameters.regexEmail);
  		return re.test(email);
	}
};
spForms.options = {
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:licenseForm.parameters.buttonsNames.btnSend,
			id:licenseForm.parameters.buttonsId.btnSend,
			success:function (){
				licenseForm.callFlowService();
				return false;
			},
			error:function (){},
			preSave: function (){
				return true;
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:licenseForm.parameters.buttonsNames.btnCancel,
			id:licenseForm.parameters.buttonsId.btnCancel,
			callback:""
		},
	],
	description:licenseForm.parameters.descriptionForm,
	contentTypeName:licenseForm.parameters.contentTypeNameForm,
	containerId:licenseForm.parameters.containerIdForm,
	allowAttachments:true,
  	requiredAttachments:false,
  	maxAttachments:20,
  	minAttachments:0,
  	maxSizeInMB:3,
	successLoadNewForm:function(){
		licenseForm.parameters.global.createdBy = sputilities.contextInfo().userEmail;
		licenseForm.parameters.global.created = moment.utc(new Date()).format(licenseForm.parameters.formats.dateFormat);
		licenseForm.parameters.formsIsLoaded = true;
	} 
};
licenseForm.initialize();
