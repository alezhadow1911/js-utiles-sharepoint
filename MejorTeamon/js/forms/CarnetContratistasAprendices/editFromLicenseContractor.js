﻿var licenseForm = {
	parameters:{
		listName:"ResponsablesTH",
		query:"?$Select=EtapaResponsable,FlujoAsociado/Title,ResponsablesAbonos/EMail,ID&$expand=FlujoAsociado,ResponsablesAbonos&$filter=Title eq 'Carnet Contratistas y Aprendices Ventanilla TH'", 
		resultItems:null,
		bodyItem:"Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior.",
		queryStringParameter:"Source",
		titlePopUp:"Carnet contratistas y/o aprendices - Acceso denegado",
		internalNameFields:{
			EstadoCarnet:"EstadoCarnet",
			ComentariosTH:"ComentariosTH"
		},
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx",
			"htmlNotAllowed":"<div class='form-group row'><label>{0}</label></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>"	
		},
		flowParameters:{
			urlFlow:"https://prod-55.westus.logic.azure.com:443/workflows/9322cd0b2c44415e9c8d468211ca1dd6/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=wJ9WkKJxu3ITfNRzV656-ZhMTheB1KIbDsF9mA5SVyA",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",
	   		approveBossUrl:"",
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Carnet Contratistas y Aprendices Ventanilla TH",
	    	applyUsers:"Carnet Contratistas y Aprendices Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:6,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Carnet Contratistas y Aprendices"
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud Ventanilla TH",
			"AplicarSolicitud":"Aplicar Novedad",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Solicitudes pendientes Novedades":"Vista Novedades",
			"Solicitudes pendientes Ventanilla TH":"Vista Ventanilla TH"
		},
		global:{
			createdBy:null,
			created:null
		},
		selectorHTML:"div[sp-static-name='{0}']",
		invalidClass:"sp-invalid",
		attachmentId:"sp-forms-add-attachment",
		colorDisabled:"#B1B1B1",
		borderDisabled:"#E1E1E1",
		display:"none",
		buttonsNames:{
			btnSend:"Enviar a corregir",
			btnCancel:"Cancelar",
			btnApprove:"Aprobar"
		},
		buttonsId:{
			btnSend:"enviar",
			btnCancel:"cancelar",
			btnApprove:"aprobar"
		},
		descriptionForm:"Completa la información correspondiente, para aprobar o enviar a corregir la solicitud",
		contentTypeNameForm:"Carnet Contratistas y Aprendices",
		containerIdForm:"spForm",
		selectorFormContent:".form-content",
		label:"label-{0}",
		button:"btn-{0}",
		requiredText:"<i>*</i>",
		btnClassDelete:".delete-doc"
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
			
	    licenseForm.parameters.flowParameters.approveUrl = String.format(licenseForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(licenseForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    licenseForm.parameters.flowParameters.applyUrl = String.format(licenseForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(licenseForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    licenseForm.parameters.flowParameters.editUrl = String.format(licenseForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(licenseForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    licenseForm.parameters.flowParameters.listViewApply = String.format(licenseForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(licenseForm.parameters.forms)[5]);
	    licenseForm.parameters.flowParameters.listViewApprove= String.format(licenseForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(licenseForm.parameters.forms)[6]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":spForms.loadedFields.CorreoElectronico.value,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"cargo":spForms.loadedFields.Cargo.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.Radicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":licenseForm.parameters.global.created,
		        "comentarios":spForms.loadedFields.ComentariosTH.value,
		     },
		    "state":spForms.loadedFields.EstadoCarnet.value,
		    "approveUsers":licenseForm.parameters.flowParameters.approveUsers,
		    "applyUsers":licenseForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":licenseForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":licenseForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":licenseForm.parameters.flowParameters.listNameAssing,
		    "editUrl":licenseForm.parameters.flowParameters.editUrl,
		    "approveUrl":licenseForm.parameters.flowParameters.approveUrl,
		    "applyUrl":licenseForm.parameters.flowParameters.applyUrl,
		    "listViewApply":licenseForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":licenseForm.parameters.flowParameters.listViewApprove
		}
		utilities.http.callRestService({
			url:licenseForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity = 0;
				document.querySelector('.form-content').style.opacity = 1;
				document.querySelector('.loader-container').style.height = 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},	
	initialize: function(){
		spForms.loadEditForm();
	},
	blockUsersForPermissions:function(){
		var currentUser =_spPageContextInfo.userEmail;		
		sputilities.getItems(licenseForm.parameters.listName,licenseForm.parameters.query,function(data){
				
			var resultItems = JSON.parse(data).d.results;
			if(resultItems.length > 0)
			{
				var items = resultItems[0].ResponsablesAbonos.results
				var find = false;
				for(i=0; i < items.length; i++)
				{
					if(items[i].EMail == currentUser){
						find = true;
						break;
					}
				}
				if(!find || document.getElementById(licenseForm.parameters.internalNameFields.EstadoCarnet).value != "1"){
					licenseForm.initPopUp(licenseForm.parameters.bodyItemPermissions,null,licenseForm.parameters.titlePopUp);
					licenseForm.enableInputs();
				}
			}				
		});
	},
	validatePreSaveFormEdit:function(){
		var ctrl = document.getElementById(licenseForm.parameters.internalNameFields.ComentariosTH);
		if(ctrl.value.trim() == ''){
			document.querySelector(String.format(licenseForm.parameters.selectorHTML,licenseForm.parameters.internalNameFields.ComentariosTH)).setAttribute(licenseForm.parameters.invalidClass,spForms.parameters.formats.requiredError);
		}
		else{
			document.querySelector(String.format(licenseForm.parameters.selectorHTML,licenseForm.parameters.internalNameFields.ComentariosTH)).removeAttribute(licenseForm.parameters.invalidClass);
		}
	},
	initPopUp:function(){
		var html = String.format(licenseForm.parameters.formats.htmlNotAllowed,licenseForm.parameters.bodyItem);
		var formContent = document.querySelector(licenseForm.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}	
	},
	enableInputs: function(){
		var deleted = document.querySelectorAll(licenseForm.parameters.btnClassDelete);
		var comments = document.getElementById(licenseForm.parameters.internalNameFields.ComentariosTH);
		var attachment = document.getElementById(licenseForm.parameters.attachmentId);
		var btnApprove = document.getElementById(String.format(licenseForm.parameters.button,licenseForm.parameters.buttonsId.btnApprove));
		var btnSend = document.getElementById(String.format(licenseForm.parameters.button,licenseForm.parameters.buttonsId.btnSend));

		if (deleted.length > 0)
		{				
			for(i=0; i < deleted.length; i++)
			{
				deleted[i].style.pointerEvents = licenseForm.parameters.display;
				deleted[i].style.color = licenseForm.parameters.colorDisabled;
				deleted[i].style.borderColor = licenseForm.parameters.borderDisabled;
			}
		}
		if(comments != null)
		{
			comments.style.pointerEvents = licenseForm.parameters.display;
			comments.style.color = licenseForm.parameters.colorDisabled;
			comments.style.borderColor = licenseForm.parameters.borderDisabled;
			
		}
		if(attachment != null)
		{
			attachment.style.pointerEvents = licenseForm.parameters.display;
			attachment.style.color = licenseForm.parameters.colorDisabled;
			attachment.style.borderColor = licenseForm.parameters.borderDisabled;
		}
		
		if(btnApprove != null || btnSend != null)
		{		
			btnApprove.style.display = licenseForm.parameters.display;
			btnSend.style.display = licenseForm.parameters.display;
		}
	}
};

spForms.options = {
	actions:[
		{
			type:spForms.parameters.updateAction,
			name:licenseForm.parameters.buttonsNames.btnApprove,
			id:licenseForm.parameters.buttonsId.btnApprove,
			preSave:function(){
				document.getElementById(licenseForm.parameters.internalNameFields.EstadoCarnet).value = "2";
				return true;
			},
			success:function(){
				licenseForm.callFlowService();
				return false;
			},
			error:function(){return true}
		},
		{
			type:spForms.parameters.updateAction,
			name:licenseForm.parameters.buttonsNames.btnSend,
			id:licenseForm.parameters.buttonsId.btnSend,
			preSave:function(){
				document.getElementById(licenseForm.parameters.internalNameFields.EstadoCarnet).value = "4";
				licenseForm.validatePreSaveFormEdit();
				return true;
			},
			success:function(){
				licenseForm.callFlowService();
				return false;
			},
			error:function(){return true}
		},
		{
			type:spForms.parameters.cancelAction,
			name:licenseForm.parameters.buttonsNames.btnCancel,
			id:licenseForm.parameters.buttonsId.btnCancel,
			callback:""
		},
	],
	description:licenseForm.parameters.descriptionForm,
	contentTypeName:licenseForm.parameters.contentTypeNameForm,
	containerId:licenseForm.parameters.containerIdForm,
	allowAttachments:true,
  	requiredAttachments:false,
  	maxAttachments:20,
  	minAttachments:0,
  	maxSizeInMB:3,
	successLoadEditForm:function(){
		licenseForm.blockUsersForPermissions();
	}
};
licenseForm.initialize();