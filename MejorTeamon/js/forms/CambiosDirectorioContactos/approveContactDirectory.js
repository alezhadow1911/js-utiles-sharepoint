﻿var contactDirectoryForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx",
			"htmlNotAllowed":"<div class='form-group row'><label>{0}</label></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>"
		},
		query:"?$Select=Responsable/EMail&$expand=Responsable",
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			Extension:"[sp-static-name='Extension']",
			NumeroTelefono:"[sp-static-name='NumeroTelefono']",
			CorreoElectronicoAlterno:"[sp-static-name='CorreoElectronicoAlterno']",
			DescripcionSolicitudCambio:"[sp-static-name='DescripcionSolicitudCambio']",
			ObservacionesResponsable:"[sp-static-name='ObservacionesResponsable']"
		},
		internalNameFields:{			
			NombrePersona:"NombrePersona1",
			Extension:"Extension",
			NumeroTelefono:"NumeroTelefono",
			CorreoElectronicoAlterno:"CorreoElectronicoAlterno",
			EstadoCambiosDirectorio:"EstadoCambiosDirectorio",
			DescripcionSolicitudCambio:"DescripcionSolicitudCambio",
			ObservacionesResponsable:"ObservacionesResponsable"
		},
		controlsHtml:{
			Attachment:"sp-forms-add-attachment",
			Aprobar:"btn-aprobar",
			Corregir:"btn-corregir",
			Aplicar:"btn-aplicar",
			Rechazar:"btn-rechazar",
			classDelete:".delete-doc",
			classGroup:".form-group.row",
			colorDisable:"#B1B1B1",
			borderDisable:"#E1E1E1",
			iconTH:"ico-help-ObservacionesResponsable"
		},		
		errorInvalid:{text:"Esto no puede estar en blanco",area:"input", areaComments:"textarea"},
		htmlRequiered:{label:"label",block:"block",none:"none"},		
		invalid:"sp-invalid",
		errorInvalidCorreo:"El correo electrónico no tiene el formato correcto",
		hidden:"sp-hidden",
		listaNameDL:"Responsable Directorio Contactos",	
		resultItems:null,
		formsIsLoaded:false,
		codeStates:{ Solicitado:"0",ConfirmacionResponsable:"1",Corregir:"2",Finalizada:"3"},	
		bodyItem:"Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior.",
		selectorFormContent:".form-content",
		queryCount:"({0})",
		formsParameters:{
			title:"",
			description:"",
			titleApprove:"Aprobar solicitud de cambio al directorio",
			descriptionApprove:"Completa la información correspondiente, para aprobar o rechazar la solicitud",
			titleToCorrect:"Corregir solicitud de cambio al directorio",
			descriptionToCorrect:"Complete la información correspondiente para corregir la solicitud",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Reject:{Text:"Enviar a corregir", Id:"enviar-corregir"},
				ToCorrect:{Text:"Corregir", Id:"corregir"}
			},
			contentType:"Cambios Directorio Contactos",
			containerId:"container-form"			
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarCambio":"contactDirectoryForm.loadApprove()",
			"Corregir":"contactDirectoryForm.loadToCorrect()",
			"VerSolicitud":"Ver Solicitud"
		},
		flowParameters:{
			urlFlow:"https://prod-43.westus.logic.azure.com:443/workflows/ab6e7b0cc5a149a38676647231beb772/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=lc7P3l5TnuGj2DPBJNm5yZt43JcpqQG9Zyge5bfjRE8",
			confirmUrl:"",
			approveUrl:"",
	   		toCorrectUrl:"",
	    	listNameAssing:"Responsable Directorio Contactos",
	    	subjectTitle:"Directorio de contactos"
		},
		formsState:{
			AprobarCambio:"1",
			Corregir:"2"
		},
		global:{
			formName:null,
			createdBy:null,
			created:null
		},
		initialState:null
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
		
	    contactDirectoryForm.parameters.flowParameters.approveUrl = String.format(contactDirectoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(contactDirectoryForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    contactDirectoryForm.parameters.flowParameters.toCorrectUrl = String.format(contactDirectoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(contactDirectoryForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    	   		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":contactDirectoryForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
			    "numeroTelefono":spForms.loadedFields.NumeroTelefono.value,
			    "descripcionCambio":spForms.loadedFields.DescripcionSolicitudCambio.value,
			    "observacionesResponsable":spForms.loadedFields.ObservacionesResponsable.value
		    },
		    "state":spForms.loadedFields.EstadoCambiosDirectorio.value,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":contactDirectoryForm.parameters.flowParameters.listNameAssing,
		    "viewUrl":contactDirectoryForm.parameters.flowParameters.viewUrl,
		    "approveUrl":contactDirectoryForm.parameters.flowParameters.approveUrl,
		   	"toCorrectUrl":contactDirectoryForm.parameters.flowParameters.toCorrectUrl
		}
		utilities.http.callRestService({
		url:contactDirectoryForm.parameters.flowParameters.urlFlow,
		method:utilities.http.methods.POST,
		data: JSON.stringify(objService),
		headers:{
    	  	"accept":utilities.http.types.jsonOData,
		  	"content-type": utilities.http.types.jsonOData
		},
		success:function(data){
			document.querySelector('.loader-container').style.opacity = 0;
			document.querySelector('.form-content').style.opacity = 1;
			document.querySelector('.loader-container').style.height = 0;

			spForms.cancelForm(null);
		},
		error:function(xhr){
			console.log(xhr);
		}
		});

	},
	initialize:function(){			
		var partsOfUrl = window.location.href.split('?')[0].split('/');
		contactDirectoryForm.parameters.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];
		eval(contactDirectoryForm.parameters.forms[contactDirectoryForm.parameters.global.formName]);
	},
	loadApprove:function(){	
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:contactDirectoryForm.parameters.formsParameters.buttons.Aprove.Text,
				id:contactDirectoryForm.parameters.formsParameters.buttons.Aprove.Id,
				success:function (){
					contactDirectoryForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return contactDirectoryForm.validateApprove();
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:contactDirectoryForm.parameters.formsParameters.buttons.Reject.Text,
				id:contactDirectoryForm.parameters.formsParameters.buttons.Reject.Id,
				success:function (){
					contactDirectoryForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return contactDirectoryForm.validateReject()
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:contactDirectoryForm.parameters.formsParameters.buttons.Cancel.Text,
				id:contactDirectoryForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		contactDirectoryForm.parameters.formsParameters.description = contactDirectoryForm.parameters.formsParameters.descriptionApprove;
		contactDirectoryForm.parameters.formsParameters.title = contactDirectoryForm.parameters.formsParameters.titleApprove;
		contactDirectoryForm.loadEdit(actions,contactDirectoryForm.loadResponsableState);
	},
	loadToCorrect:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:contactDirectoryForm.parameters.formsParameters.buttons.ToCorrect.Text,
				id:contactDirectoryForm.parameters.formsParameters.buttons.ToCorrect.Id,
				success:function (){
					contactDirectoryForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					if (contactDirectoryForm.validateFormat()){
						return contactDirectoryForm.validateApprove();
					}
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:contactDirectoryForm.parameters.formsParameters.buttons.Cancel.Text,
				id:contactDirectoryForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		contactDirectoryForm.parameters.formsParameters.description = contactDirectoryForm.parameters.formsParameters.descriptionToCorrect;
		contactDirectoryForm.parameters.formsParameters.title = contactDirectoryForm.parameters.formsParameters.titleToCorrect;
        contactDirectoryForm.loadEdit(actions,contactDirectoryForm.loadResponsableState);

	},
	loadEdit:function(actions,sucessFunction){
		spForms.options = {	
			actions:actions,
			title:contactDirectoryForm.parameters.formsParameters.title,
			description:contactDirectoryForm.parameters.formsParameters.description,
			contentTypeName:contactDirectoryForm.parameters.formsParameters.contentType,
			containerId:contactDirectoryForm.parameters.formsParameters.containerId,
			allowAttachments:true,
		  	requiredAttachments:false,
		  	maxAttachments:20,
		  	minAttachments:0,
		  	maxSizeInMB:3,
			successLoadEditForm:function(){	
				contactDirectoryForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
				contactDirectoryForm.parameters.formsIsLoaded = true;
				contactDirectoryForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(contactDirectoryForm.parameters.formats.dateFormat);
				if(typeof sucessFunction== "function"){
					sucessFunction();
				}
			} 
		};
		spForms.loadEditForm();
	},
	validateRequired:function(){
		var comentariosRow = document.querySelector(contactDirectoryForm.parameters.fieldsHTML.ObservacionesResponsable);
		var isValid = true;
		
		if(comentariosRow != null)
		{
			var comentarios = comentariosRow.querySelector(contactDirectoryForm.parameters.errorInvalid.areaComments);
	
			if(comentarios.value != "")
			{
				comentariosRow.removeAttribute(contactDirectoryForm.parameters.invalid);
			}
			else
			{
				isValid = false;
				comentariosRow.setAttribute(contactDirectoryForm.parameters.invalid,contactDirectoryForm.parameters.errorInvalid.text);			
			}	
		}
		else
		{
			isValid = false;
		}
		return isValid;
	},
	validateApprove:function(){
		var state = document.getElementById(contactDirectoryForm.parameters.internalNameFields.EstadoCambiosDirectorio);
		var comentariosRow = document.querySelector(contactDirectoryForm.parameters.fieldsHTML.ObservacionesResponsable);
		var isValid = true;
		if(contactDirectoryForm.parameters.initialState == contactDirectoryForm.parameters.codeStates.ConfirmacionResponsable)
		{			
			if(state!= null)
			{
				comentariosRow.removeAttribute(contactDirectoryForm.parameters.invalid);
				state.value = contactDirectoryForm.parameters.codeStates.Finalizada;
			}
			else
			{
				isValid = false;
			}
		}
		if(contactDirectoryForm.parameters.initialState == contactDirectoryForm.parameters.codeStates.Corregir)
		{			
			if(state!= null)
			{
				state.value = contactDirectoryForm.parameters.codeStates.ConfirmacionResponsable;
			}
			else
			{
				isValid = false;
			}
		}

				return isValid;		
	},
	validateReject:function(){
		var row = document.querySelector(contactDirectoryForm.parameters.fieldsHTML.ObservacionesResponsable);
		var state = document.getElementById(contactDirectoryForm.parameters.internalNameFields.EstadoCambiosDirectorio);		
		var isValid = true;
		
		if(row != null && state != null)
		{
			var comments = row.querySelector(contactDirectoryForm.parameters.errorInvalid.areaComments);
			if(comments.value != "")
			{
				row.removeAttribute(contactDirectoryForm.parameters.invalid);
				if(contactDirectoryForm.parameters.initialState == contactDirectoryForm.parameters.codeStates.ConfirmacionResponsable)
				{
					state.value = contactDirectoryForm.parameters.codeStates.Corregir;
				}
				else
				{
					state.value = contactDirectoryForm.parameters.codeStates.Finalizada;
				}			
			}
			else
			{
				row.setAttribute(contactDirectoryForm.parameters.invalid,contactDirectoryForm.parameters.errorInvalid.text);			
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;

	},
	loadResponsableState:function(){	
		try
		{	
			var userCurrent = _spPageContextInfo.userEmail;
			var state = document.getElementById(contactDirectoryForm.parameters.internalNameFields.EstadoCambiosDirectorio);
			if(state != null)
			{
				if(state.value == contactDirectoryForm.parameters.formsState[contactDirectoryForm.parameters.global.formName])
				{
					if(state.value == contactDirectoryForm.parameters.codeStates.ConfirmacionResponsable)
					{
						var query = contactDirectoryForm.parameters.query;
						sputilities.getItems(contactDirectoryForm.parameters.listaNameDL,query,function(data){
							
							var resultItems = JSON.parse(data).d.results;
							if(resultItems.length > 0)
							{
								var items = resultItems[0].Responsable.EMail;
								var find = false;
								if(items == userCurrent){
									find = true;
								}
								
								contactDirectoryForm.parameters.initialState = state.value;
								
								if(state.value == contactDirectoryForm.parameters.codeStates.ConfirmacionResponsable)
								{				
									if(find)
									{
										contactDirectoryForm.showComments(state.value);
									}
									else
									{
										contactDirectoryForm.showComments(state.value);
										contactDirectoryForm.hideButtons(state.value);
										contactDirectoryForm.showPopUp();
									}
								}
							}						
						});
					}
					else
					if (state.value == contactDirectoryForm.parameters.codeStates.Corregir)
					{	
						userCurrent = _spPageContextInfo.userId;
						var find = false;
						if(userCurrent == spForms.currentItem.AuthorId)
						{
							find = true;
						}
						
						contactDirectoryForm.parameters.initialState = state.value;
						
						if(find)
						{
							contactDirectoryForm.showComments(state.value);
						}
						else
						{
							contactDirectoryForm.showComments(state.value);
							contactDirectoryForm.hideButtons(state.value);
							contactDirectoryForm.showPopUp();
						}
					}
				}
				else
				{
					contactDirectoryForm.showComments(state.value);
					contactDirectoryForm.hideButtons(state.value);
					contactDirectoryForm.showPopUp();
				}
			}
		}
		catch(e){
			console.log(e);
		}	
	},
	hideButtons:function(state){
	
		if(state == contactDirectoryForm.parameters.codeStates.ConfirmacionResponsable)
		{
			var bottonApp = document.getElementById(contactDirectoryForm.parameters.controlsHtml.Enviar);
			if(bottonApp != null)
			{
				bottonApp.style.display = contactDirectoryForm.parameters.htmlRequiered.none;
			}
		}
		else if(state == contactDirectoryForm.parameters.codeStates.Corregir)
		{
			var bottonApp = document.getElementById(contactDirectoryForm.parameters.controlsHtml.Enviar);
			if(bottonApp != null)
			{
				bottonApp.style.display = contactDirectoryForm.parameters.htmlRequiered.none;
			}		
		}
		
	},
	showPopUp:function(){
		var html = String.format(contactDirectoryForm.parameters.formats.htmlNotAllowed,contactDirectoryForm.parameters.bodyItem);
		var formContent = document.querySelector(contactDirectoryForm.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}	
	},
	validateEmail: function (email){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);
	},
	validateFormat:function(){
		var rowEmail = document.querySelector(contactDirectoryForm.parameters.fieldsHTML.CorreoElectronicoAlterno);
		var email = document.getElementById(contactDirectoryForm.parameters.internalNameFields.CorreoElectronicoAlterno);
		var isValid = true;		
		if(email != null && rowEmail!= null)
		{			
			if(contactDirectoryForm.validateEmail(email.value) || email.value == "")
			{
				rowEmail.removeAttribute(contactDirectoryForm.parameters.invalid);
			}
			else
			{
				rowEmail.setAttribute(contactDirectoryForm.parameters.invalid,contactDirectoryForm.parameters.errorInvalidCorreo);
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;
	},
	showComments:function(state){
		if(state == contactDirectoryForm.parameters.codeStates.ConfirmacionEntrenamiento)
		{
			var comments = document.querySelector(contactDirectoryForm.parameters.fieldsHTML.ObservacionesResponsable);	
			if(comments != null)
			{
				comments.setAttribute(contactDirectoryForm.parameters.hidden,false);
				comments.querySelector(contactDirectoryForm.parameters.errorInvalid.areaComments).value="";
	
			}
		}
		else if(state == contactDirectoryForm.parameters.codeStates.ConfirmarFecha)
		{
			var comments = document.querySelector(contactDirectoryForm.parameters.fieldsHTML.ObservacionesResponsable);	
			if(comments != null)
			{
				comments.setAttribute(contactDirectoryForm.parameters.hidden,false);
			}
		}
		else if(state == contactDirectoryForm.parameters.codeStates.Corregir)
		{
			var comments = document.querySelector(contactDirectoryForm.parameters.fieldsHTML.ObservacionesResponsable);	
			if(comments != null)
			{
				comments.setAttribute(contactDirectoryForm.parameters.hidden,false);
			}
		}
		else if(state == contactDirectoryForm.parameters.codeStates.AsignarSala)
		{
			var comments = document.querySelector(contactDirectoryForm.parameters.fieldsHTML.ObservacionesResponsable);	
			if(comments != null)
			{
				comments.setAttribute(contactDirectoryForm.parameters.hidden,false);
			}
		}
	}
}
contactDirectoryForm.initialize();