﻿var regulatoryTrainingForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			Extension:"[sp-static-name='Extension']",
			NumeroTelefono:"[sp-static-name='NumeroTelefono']",
			CorreoElectronicoAlterno:"[sp-static-name='CorreoElectronicoAlterno']",
			Estacion:"[sp-static-name='Estacion0']> div > div > select",
			Area:"[sp-static-name='Area0'] > div > div > select",
			COASelect:"[sp-static-name='COA'] > div > div > select",
			COA:"[sp-static-name='COA']",
			Manual:"[sp-static-name='Manual']",
			TipoCurso:"[sp-static-name='TipoCurso']",
			CantidadColaboradores:"[sp-static-name='CantidadColaboradores']",
			FechaVencimiento:"[sp-static-name='FechaVencimiento']",
			FechaPropuestaEntrenamieto:"[sp-static-name='FechaPropuestaEntrenamieto']",
			EstadoEntrenamientoRegulatorio:"[sp-static-name='EstadoEntrenamientoRegulatorio']",
			ManualSelect:"[sp-static-name='ManualSelect'] > div > div > select",
			CursosSelect:"[sp-static-name='TipoCursosSelect'] > div > div > select"
		},
		internalNameFields:{			
			NombrePersona:"NombrePersona1",
			Extension:"Extension",
			NumeroTelefono:"NumeroTelefono",
			CorreoElectronicoAlterno:"CorreoElectronicoAlterno",
			Estacion:"Estacion0",
			Area:"Area0",
			COA:"COA",
			Manual:"Manual",
			TipoCurso:"TipoCurso",
			CantidadColaboradores:"CantidadColaboradores",
			FechaVencimiento:"FechaVencimiento",
			FechaPropuestaEntrenamieto:"FechaPropuestaEntrenamieto",
			EstadoEntrenamientoRegulatorio:"EstadoEntrenamientoRegulatorio",
			TipoCursoSelect:"TipoCursosElement",
			ManualSelect:"ManualElement"
		},
		resultItems:null,
		resultManual:null,
		resultCurso:null,
		invalid:"sp-invalid",
		requiredTag:"<i>*</i>",
		workPhone:"WorkPhone",
		htmlCurso:"<div class='form-group row' sp-static-name='TipoCursosSelect'>"+
						   "<label class='col-sm-4 control-label'>Tipo Cursos<i>*</i></label>"+
						   "<div class='col-sm-8'>"+
						      "<div class='form-control-help'>"+
						         "<select class='form-control input-lg' sp-required='true' sp-type='lookup'>{0}</select>"+
						         "<a class='glyphicon glyphicon-question-sign ico-help' aria-hidden='true'></a>"+
						      "</div>"+
						      "<span id='help-block-TipoCursosSelect' class='help-block forms-hide-element'></span>"+
						      "<span id='help-block-error-message-TipoCursosSelect' class='help-block forms-hide-element error-message'></span>"+
						   "</div>"+
					"</div>",
		htmlManual:"<div class='form-group row' sp-static-name='ManualSelect'>"+
						   "<label class='col-sm-4 control-label'>Manual<i>*</i></label>"+
						   "<div class='col-sm-8'>"+
						      "<div class='form-control-help'>"+
						         "<select class='form-control input-lg' sp-required='true' sp-type='lookup'>{0}</select>"+
						         "<a class='glyphicon glyphicon-question-sign ico-help' aria-hidden='true'></a>"+
						      "</div>"+
						      "<span id='help-block-ManualSelect' class='help-block forms-hide-element'></span>"+
						      "<span id='help-block-error-message-ManualSelect' class='help-block forms-hide-element error-message'></span>"+
						   "</div>"+
					"</div>",
		errorInvalid:"El correo electrónico no tiene el formato correcto",
		errorInvalidNotText:"Esto no puede estar en blanco",
		loadUserInfo:"regulatoryTrainingForm.loadUserInfo",
		formsIsLoaded:false,
		listNameManual:"Manual",
		listNameCurso:"TipoCurso",
		htmlOptionCursos:"<option id='{0}-TipoCurso' value='{0}'>{1}</option>",
		htmlOptionManual:"<option id='{0}-Manual' value='{0}'>{1}</option>",
		COAId:"COAId",
		formsParameters:{
			title:"Solicitudes de entrenamiento regulatorio",
			description:"Completa la información correspondiente",
			buttons:{
				Send:{Text:"Corregir",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Reject:{Text:"Enviar a corregir", Id:"corregir"}
			},
			contentType:"Entrenamientos Regulatorios",
			containerId:"container-form"			
		},
		flowParameters:{
			urlFlow:"https://prod-62.westus.logic.azure.com:443/workflows/3f2410a9add94059aa6f885775e57055/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=Bn7ciXBuBeUPCz4PiP8vMqOXnspWPwZoYfvaipJiBKs",
			confirmUrl:"",
	    	approveUrl:"",
	   		assingUrl:"",
	   		toCorrectUrl:"",
	    	listNameAssing:"ResponsablesEntrenamiento",
	    	subjectTitle:"Entrenamiento Regulatorio"
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobacionEntrenamiento":"Aprobacion Entrenamiento",
			"AsignarSala":"Asignar Sala",
			"ConfirmarFecha":"Confirmar Fecha",
			"Corregir":"Corregir"
		},
		global:{
			createdBy:null,
			created:null
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
						
	    regulatoryTrainingForm.parameters.flowParameters.approveUrl = String.format(regulatoryTrainingForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(regulatoryTrainingForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    regulatoryTrainingForm.parameters.flowParameters.editUrl = String.format(regulatoryTrainingForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(regulatoryTrainingForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	   	regulatoryTrainingForm.parameters.flowParameters.assingUrl = String.format(regulatoryTrainingForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(regulatoryTrainingForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    regulatoryTrainingForm.parameters.flowParameters.toCorrectUrl = String.format(regulatoryTrainingForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(regulatoryTrainingForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    regulatoryTrainingForm.parameters.flowParameters.confirmUrl = String.format(regulatoryTrainingForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(regulatoryTrainingForm.parameters.forms)[5],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	   		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":regulatoryTrainingForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
			    "numeroTelefono":spForms.loadedFields.NumeroTelefono.value,
			    "manual": spForms.loadedFields.Manual.value,
			    "cantidadColaboradores": spForms.loadedFields.CantidadColaboradores.value,
			    "fechaPropuesta": spForms.loadedFields.FechaPropuestaEntrenamieto.value,
			    "fechaVencimiento": spForms.loadedFields.FechaVencimiento.value,
			    "area":spForms.loadedFields.Area0.value,
			    "estacion":spForms.loadedFields.Estacion0.value
		    },
		    "state":spForms.loadedFields.EstadoEntrenamientoRegulatorio.value,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":regulatoryTrainingForm.parameters.flowParameters.listNameAssing,
		    "viewUrl":regulatoryTrainingForm.parameters.flowParameters.viewUrl,
		    "approveUrl":regulatoryTrainingForm.parameters.flowParameters.approveUrl,
		    "editUrl":regulatoryTrainingForm.parameters.flowParameters.editUrl,
		    "assingUrl":regulatoryTrainingForm.parameters.flowParameters.assingUrl,
		    "toCorrectUrl":regulatoryTrainingForm.parameters.flowParameters.assingScore,
		   	"subjectTitle":	regulatoryTrainingForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:regulatoryTrainingForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity = 0;
				document.querySelector('.form-content').style.opacity = 1;
				document.querySelector('.loader-container').style.height = 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){
		$("#"+ regulatoryTrainingForm.parameters.internalNameFields.FechaVencimiento).data("DateTimePicker").minDate(new Date());
		$("#"+ regulatoryTrainingForm.parameters.internalNameFields.FechaPropuestaEntrenamieto).data("DateTimePicker").minDate(new Date());
		spForms.loadEditForm();
	}
}
spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:regulatoryTrainingForm.parameters.formsParameters.buttons.Send.Text,
			id:regulatoryTrainingForm.parameters.formsParameters.buttons.Send.Id,
			success:function (){
				regulatoryTrainingForm.callFlowService();
				return false;

			},
			error:function (){},
			preSave: function (){
				return regulatoryTrainingForm.setValuesForm()

			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:regulatoryTrainingForm.parameters.formsParameters.buttons.Cancel.Text,
			id:regulatoryTrainingForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:regulatoryTrainingForm.parameters.formsParameters.title,
	description:regulatoryTrainingForm.parameters.formsParameters.description,
	contentTypeName:regulatoryTrainingForm.parameters.formsParameters.contentType,
	containerId:regulatoryTrainingForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:false,
  	maxAttachments:20,
  	minAttachments:0,
  	maxSizeInMB:3,
	successLoadNewForm:function(){
		regulatoryTrainingForm.parameters.global.createdBy = sputilities.contextInfo().userEmail;
		regulatoryTrainingForm.parameters.global.created = moment.utc(new Date()).format(regulatoryTrainingForm.parameters.formats.dateFormat);
		regulatoryTrainingForm.parameters.formsIsLoaded = true;
	} 
};
regulatoryTrainingForm.initialize();