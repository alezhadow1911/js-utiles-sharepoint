﻿var contactDirectoryForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			Extension:"[sp-static-name='Extension']",
			NumeroTelefono:"[sp-static-name='NumeroTelefono']",
			CorreoElectronicoAlterno:"[sp-static-name='CorreoElectronicoAlterno']",
			DescripcionSolicitudCambio:"[sp-static-name='DescripcionSolicitudCambio']"
		},
		internalNameFields:{			
			NombrePersona:"NombrePersona1",
			Extension:"Extension",
			NumeroTelefono:"NumeroTelefono",
			CorreoElectronicoAlterno:"CorreoElectronicoAlterno",
			EstadoCambiosDirectorio:"EstadoCambiosDirectorio",
			DescripcionSolicitudCambio:"DescripcionSolicitudCambio"
		},
		resultItems:null,
		resultDoc:null,
		invalid:"sp-invalid",
		requiredTag:"<i>*</i>",
		htmlDoc:"<div class='form-group row'>"+
					"<label class='col-sm-4 control-label'>Plantilla de descarga</label>"+
						"<div class='col-sm-8'><a class='doc-template' href='{0}' target='_blank'>{1}</a></div></div>",
		workPhone:"WorkPhone",
		errorInvalid:"El correo electrónico no tiene el formato correcto",
		errorInvalidNotText:"Esto no puede estar en blanco",
		loadUserInfo:"contactDirectoryForm.loadUserInfo",
		listNameTemplate:"Plantilla Directorio de Contactos",
		query:"?$select=ID,ServerRedirectedEmbedUri,Title,FileRef&$orderby=Title&$top=1",
		formsIsLoaded:false,
		formsParameters:{
			title:"Solicitudes de cambio en directorio de contactos",
			description:"Completa la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Reject:{Text:"Enviar a corregir", Id:"corregir"}
			},
			contentType:"Cambios Directorio Contactos",
			containerId:"container-form"			
		},
		flowParameters:{
			urlFlow:"https://prod-43.westus.logic.azure.com:443/workflows/ab6e7b0cc5a149a38676647231beb772/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=lc7P3l5TnuGj2DPBJNm5yZt43JcpqQG9Zyge5bfjRE8",
			confirmUrl:"",
	    	approveUrl:"",
	   		assingUrl:"",
	   		toCorrectUrl:"",
	    	listNameAssing:"Responsable Directorio Contactos",
	    	subjectTitle:"Directorio de contactos"
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarCambio":"Aprobar Cambio",
			"Corregir":"Corregir"
		},
		global:{
			createdBy:null,
			created:null
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
				
	    contactDirectoryForm.parameters.flowParameters.approveUrl = String.format(contactDirectoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(contactDirectoryForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    contactDirectoryForm.parameters.flowParameters.editUrl = String.format(contactDirectoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(contactDirectoryForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	   	contactDirectoryForm.parameters.flowParameters.assingUrl = String.format(contactDirectoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(contactDirectoryForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    contactDirectoryForm.parameters.flowParameters.toCorrectUrl = String.format(contactDirectoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(contactDirectoryForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    contactDirectoryForm.parameters.flowParameters.confirmUrl = String.format(contactDirectoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(contactDirectoryForm.parameters.forms)[5],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	   		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":contactDirectoryForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
			    "numeroTelefono":spForms.loadedFields.NumeroTelefono.value,
			    "descripcionCambio":spForms.loadedFields.DescripcionSolicitudCambio.value
		    },
		    "state":spForms.loadedFields.EstadoCambiosDirectorio.value,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":contactDirectoryForm.parameters.flowParameters.listNameAssing,
		    "viewUrl":contactDirectoryForm.parameters.flowParameters.viewUrl,
		    "approveUrl":contactDirectoryForm.parameters.flowParameters.approveUrl,
		   	"toCorrectUrl":contactDirectoryForm.parameters.flowParameters.toCorrectUrl
		}
		utilities.http.callRestService({
			url:contactDirectoryForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity = 0;
				document.querySelector('.form-content').style.opacity = 1;
				document.querySelector('.loader-container').style.height = 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){
		sputilities.callFunctionOnLoadBody(contactDirectoryForm.parameters.loadUserInfo);
		spForms.loadNewForm();
	},
	loadUserInfo:function(){
		try {
			//running the query with parameters
			sputilities.getMyUserProfileProperties("",function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d;
				contactDirectoryForm.parameters.resultItems = resultItems;
				var ctrlInterval = setInterval(function(){
					if(contactDirectoryForm.parameters.formsIsLoaded)
					{
						contactDirectoryForm.setValuesInputs();
						contactDirectoryForm.getTemplateDoc();
						clearInterval(ctrlInterval);
					}
				},500);
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	setValuesInputs: function(){
		var nombres = document.getElementById(contactDirectoryForm.parameters.internalNameFields.NombrePersona);
		var extension = document.getElementById(contactDirectoryForm.parameters.internalNameFields.Extension);

		if (nombres != null && extension!= null)
		{
			nombres.value = contactDirectoryForm.parameters.resultItems.DisplayName;
			extension.value = contactDirectoryForm.parameters.resultItems.UserProfileProperties.results.filter(contactDirectoryForm.findPropertiesUser)[0].Value ? contactDirectoryForm.parameters.resultItems.UserProfileProperties.results.filter(contactDirectoryForm.findPropertiesUser)[0].Value: "";
		
		}
	},
	findPropertiesUser:function(properties){
		return properties.Key == contactDirectoryForm.parameters.workPhone;
	},
	getTemplateDoc: function(){
		try {
			//running the query with parameters
			sputilities.getItems(contactDirectoryForm.parameters.listNameTemplate,contactDirectoryForm.parameters.query,function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				contactDirectoryForm.parameters.resultDoc = resultItems;
				var ctrlInterval = setInterval(function(){
					if(contactDirectoryForm.parameters.formsIsLoaded)
					{
						contactDirectoryForm.insertHTMLDoc();
						clearInterval(ctrlInterval);
					}
				},500);
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}

	},
	insertHTMLDoc: function(){
		var doc="";
		
		for (var i=0; i < contactDirectoryForm.parameters.resultDoc.length; i++)
		{
			doc += String.format(contactDirectoryForm.parameters.htmlDoc,contactDirectoryForm.parameters.resultDoc[i].FileRef,contactDirectoryForm.parameters.resultDoc[i].Title);
		}
		var element = document.createElement('div');
		element.id = "doc-template";
		element.innerHTML = doc;
		var typeElement = document.querySelector(contactDirectoryForm.parameters.fieldsHTML.DescripcionSolicitudCambio);
		typeElement.parentNode.insertBefore(element,typeElement.nextSibling);
	},
	validateFormat:function(){
		var rowEmail = document.querySelector(contactDirectoryForm.parameters.fieldsHTML.CorreoElectronicoAlterno);
		var email = document.getElementById(contactDirectoryForm.parameters.internalNameFields.CorreoElectronicoAlterno);
		var isValid = true;		
		if(email != null && rowEmail!= null)
		{			
			if(contactDirectoryForm.validateEmail(email.value) || email.value == "")
			{
				rowEmail.removeAttribute(contactDirectoryForm.parameters.invalid);
			}
			else
			{
				rowEmail.setAttribute(contactDirectoryForm.parameters.invalid,contactDirectoryForm.parameters.errorInvalid);
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;
		
	},
	validateEmail: function (email){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);
	}
}
spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:contactDirectoryForm.parameters.formsParameters.buttons.Send.Text,
			id:contactDirectoryForm.parameters.formsParameters.buttons.Send.Id,
			success:function (){
				contactDirectoryForm.callFlowService();
				return false;

			},
			error:function (){},
			preSave: function (){
				return contactDirectoryForm.validateFormat();

			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:contactDirectoryForm.parameters.formsParameters.buttons.Cancel.Text,
			id:contactDirectoryForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:contactDirectoryForm.parameters.formsParameters.title,
	description:contactDirectoryForm.parameters.formsParameters.description,
	contentTypeName:contactDirectoryForm.parameters.formsParameters.contentType,
	containerId:contactDirectoryForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:20,
  	minAttachments:1,
  	maxSizeInMB:3,
	successLoadNewForm:function(){
		contactDirectoryForm.parameters.global.createdBy = sputilities.contextInfo().userEmail;
		contactDirectoryForm.parameters.global.created = moment.utc(new Date()).format(contactDirectoryForm.parameters.formats.dateFormat);
		contactDirectoryForm.parameters.formsIsLoaded = true;
	} 
};
contactDirectoryForm.initialize();