﻿var Cesantias = {
	//create parameters
	parameters:{
		query:"?$Select=FlujoAsociado/Title,ResponsablesAbonos/EMail,ID&$expand=FlujoAsociado,ResponsablesAbonos&$filter=ID eq 11",
		queryNovedades:"?$Select=FlujoAsociado/Title,ResponsablesAbonos/EMail,ID&$expand=FlujoAsociado,ResponsablesAbonos&$filter=ID eq 12",
		listName: "ResponsablesTH",
		bodyItem:"<div class='contact'><p>Usted no tiene permisos sobre este formulario.</p></div>",
		TitlePopUp:"Retiro de Cesantias - Acceso Denegado ",
		textNotAllowed: "Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior",
		selectorFormContent:".form-content",
		htmlNotAllowed:"<div><h4>{0}</h4></div>"
	},	
	fieldType:{
	 "0":{
	    	Actualizar:function(){
	       	}
    },
     "1":{
	    	Actualizar:function(){
	    	Cesantias.userResponsablesVentanillaForm();
	    	var ObservacionesJefe = document.querySelector('div[sp-static-name="ComentariosTH"]');
			ObservacionesJefe.style.display = "block";
	    	var TextObservacionesTH = document.getElementById("ComentariosTH");
			TextObservacionesTH.style.pointerEvents = "initial";
			TextObservacionesTH.style.color = "#444";
			document.getElementById("btn-Enviar").style.display ="none";
			document.getElementById("btn-ANovedad").style.display ="none";
      	}
    },
     "2":{
	    	Actualizar:function(){
	    	Cesantias.userResponsablesNovedadesForm();
	    	 var ObservacionesJefe = document.querySelector('div[sp-static-name="ComentariosTH"]');
			ObservacionesJefe.style.display = "block";
	    	document.getElementById("btn-Aprobar").style.display ="none";
			document.getElementById("btn-Corregir").style.display ="none";	
			document.getElementById("btn-Enviar").style.display ="none";	
     	 }
    },

   	"3":{
	    	Actualizar:function(){
	    	var ObservacionesJefe = document.querySelector('div[sp-static-name="ComentariosTH"]');
			ObservacionesJefe.style.display = "block";
	    	document.getElementById("btn-Aprobar").style.display ="none";
			document.getElementById("btn-Corregir").style.display ="none";
			document.getElementById("btn-ANovedad").style.display ="none";		
			document.getElementById('NombrePersona1').style.pointerEvents = "initial"
			document.getElementById('DocumentoIdentidad').style.pointerEvents = "initial"
			document.getElementById('NumeroCelular').style.pointerEvents = "initial"
			document.getElementById('CorreoElectronico').style.pointerEvents = "initial"
			document.getElementById('NumeroRadicado').style.pointerEvents = "initial"
			document.getElementById('EstadosCajaCompensacion').style.pointerEvents = "initial"
			document.getElementById('MotivoRetiro').style.pointerEvents = "initial";
			document.getElementById('Fondo').style.pointerEvents = "initial";
			document.getElementById('MontoSolicitado').style.pointerEvents = "initial";
			document.getElementById('ComentariosTH').style.pointerEvents = "initial";
			document.getElementById('NombrePersona1').style.color  = "#444"
			document.getElementById('DocumentoIdentidad').style.color  = "#444"
			document.getElementById('NumeroCelular').style.color  = "#444"
			document.getElementById('CorreoElectronico').style.color  = "#444"
			document.getElementById('NumeroRadicado').style.color  = "#444"
			document.getElementById('EstadosCajaCompensacion').style.color  = "#444";
			document.getElementById('MotivoRetiro').style.color  = "#444";
			document.getElementById('Fondo').style.color  = "#444";
			document.getElementById('MontoSolicitado').style.color  = "#444";
			document.getElementById('ComentariosTH').style.color  = "#444";
			document.getElementsByClassName('col-md-6 col-xs-6 delete-doc')[0].style.display = "block";
			document.getElementById('sp-forms-add-attachment').style.display = "block";
			var Base = document.getElementById('BasePersona')
			var BaseInputs =Base.getElementsByTagName('input')
			for(var i=0; i<BaseInputs.length;i++){
			var input=BaseInputs[i];
			input.disabled=false;
			}	
      	}
    },
     "4":{
	    	Actualizar:function(){
	    	document.getElementById("btn-Aprobar").style.display ="none";
			document.getElementById("btn-Corregir").style.display ="none";	
			document.getElementById("btn-Enviar").style.display ="none";
			document.getElementById("btn-cancelar").style.display ="none";
			document.getElementById("btn-ANovedad").style.display ="none";
     	 }
    },
    },

	CtrlForm:function(){
		try {
			var Base = document.getElementById('BasePersona')
			var BaseInputs =Base.getElementsByTagName('input')
			for(var i=0; i<BaseInputs.length;i++){
				var input=BaseInputs[i];
				input.disabled=true;
				}	
			var CurrentEstado = document.getElementById("EstadosCajaCompensacion").value;
			var spFormField = Cesantias.fieldType[CurrentEstado].Actualizar();		
			}
		catch (e){
			console.log(e);
		}
	},
	userResponsablesVentanillaForm:function(){
		try {
			var UserEmailCurrent = _spPageContextInfo.userEmail
			var linkQuery = String.format(Cesantias.parameters.query);
			sputilities.getItems(Cesantias.parameters.listName,linkQuery,function(data){
				var resultItems = JSON.parse(data).d.results;
				var Items = resultItems[0].ResponsablesAbonos.results
				var find = false; 
				for(var i=0;i<Items.length;i++){
					var nextItem = Items[i+1];
					var prevItem = Items[i-1];
					var Email = Items[i].EMail
						if(Email == UserEmailCurrent){
							find = true;
							}
						}
				if(find){
					document.getElementById('spForm').style.display="block";
				}else{			
					Cesantias.hideNotAllowed();					
					}			
			},
			);
		}
		catch (e){
			console.log(e);
		}
	},
	userResponsablesNovedadesForm:function(){
		try {
		var UserEmailCurrent = _spPageContextInfo.userEmail
		var linkQuery = String.format(Cesantias.parameters.queryNovedades);
		sputilities.getItems(Cesantias.parameters.listName,linkQuery,function(data){
				var resultItems = JSON.parse(data).d.results;
				var Items = resultItems[0].ResponsablesAbonos.results
				var find = false; 
				for(var i=0;i<Items.length;i++){
					var nextItem = Items[i+1];
					var prevItem = Items[i-1];
					var Email = Items[i].EMail
					console.log(Email);
						if(Email == UserEmailCurrent){
							find = true;
						}
						}
				if(find){
					document.getElementById('spForm').style.display="block";
				}else{
					Cesantias.hideNotAllowed();					
					}		
			},
			);


		}
		catch (e){
			console.log(e);
		}
	},
	validateReject:function(){
	
		var row = document.querySelector("[sp-static-name='ComentariosTH']");
		var comments = row.querySelector("textarea");
		var isValid = true;
		if(comments.value != "")
		{
			row.removeAttribute('sp-invalid');			
		}
		else
		{
			row.setAttribute('sp-invalid',"Los comentarios son obligatorios");			
		}
		return isValid;
	
	},
	
	hideNotAllowed:function(){
		var html = String.format(Cesantias.parameters.htmlNotAllowed,Cesantias.parameters.textNotAllowed);
		var formContent = document.querySelector(Cesantias.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	},

	Initialize:function(){
		try {
		Cesantias.loadEditForm();
		}
		catch (e){
			console.log(e);
		}
	},
	loadEditForm:function(){
	spForms.options={
		actions:[
			{
			type:spForms.parameters.updateAction,
			name:"Aprobar",
			id:"Aprobar",
			preSave:function(){
			var CurrentEstado = document.getElementById("EstadosCajaCompensacion");
			if(CurrentEstado.value == "0"){
			CurrentEstado.value = "2";
			}
			return true},
			success:function(){return true},
			error:function(){return true}
		},
				{
			type:spForms.parameters.updateAction,
			name:"Enviar a Corregir",
			id:"Corregir",
			preSave:function(){
			var CurrentEstado = document.getElementById("EstadosCajaCompensacion");
			if(CurrentEstado.value == "1"){
			CurrentEstado.value = "3";
			}
			return Cesantias.validateReject();
			return true},
			success:function(){return true},
			error:function(){return true}
		},
		{
			type:spForms.parameters.updateAction,
			name:"Enviar",
			id:"Enviar",
			preSave:function(){
			var CurrentEstado = document.getElementById("EstadosCajaCompensacion");
			if(CurrentEstado.value == "3"){
			CurrentEstado.value = "1";
			}
			return true},
			success:function(){return true},
			error:function(){return true}
		},
		{
			type:spForms.parameters.updateAction,
			name:"Aplicar Novedad",
			id:"ANovedad",
			preSave:function(){
			var CurrentEstado = document.getElementById("EstadosCajaCompensacion");
			if(CurrentEstado.value == "2"){
			CurrentEstado.value = "4";
			}

			return true},
			success:function(){return true},
			error:function(){return true}
		},


		{
			type:spForms.parameters.cancelAction,
			name:"Cancelar",
			id:"cancelar",
			callback:""
		},
	],
	title:"Formulario de Cesantias Edición",
	description:"Completa la información correspondiente",
	contentTypeName:"RetiroCesantias",
	containerId:"spForm",
		allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:5,
  	minAttachments:1,

	successLoadEditForm:function(){
		Cesantias.CtrlForm();
	} 
};
spForms.loadEditForm();
	},	
}
Cesantias.Initialize();







