﻿var Cesantias = {
	//create parameters
	parameters:{
			IdEnviar: "Enviar",
			IdCancelar:"Cancelar",
			ContentType:"RetiroCesantias",
			IdDiv:"spForm",
			TitleForm:"Ver Retiro de Cesantias",
	},	
	Initialize:function(){
		try {
		Cesantias.loadEditForm();
		}
		catch (e){
			console.log(e);
		}
	},
	loadEditForm:function(){
	spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:Cesantias.parameters.IdEnviar,
			id:Cesantias.parameters.IdEnviar,
			success:function(){return true;},
			error:function(){},
			preSave:function(){
			return true;
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:Cesantias.parameters.IdCancelar,
			id:Cesantias.parameters.IdCancelar,
			callback:""
		},
	],
	title:Cesantias.parameters.TitleForm,
	contentTypeName:Cesantias.parameters.ContentType,
	containerId:Cesantias.parameters.IdDiv,
	allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:5,
  	minAttachments:1,

	successLoadEditForm:function(){
	
	} 
	};
	spForms.loadEditForm();
	},	
}
Cesantias.Initialize();


