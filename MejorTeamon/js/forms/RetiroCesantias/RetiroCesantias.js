﻿var Cesantias = {
	//create parameters
	parameters:{
			bodyItem:"<div class='contact'><p>Te invitamos a resolver tus inquietudes y enviarnos tus comentarios y sugerencias sobre nuestra Intranet corporativa diligenciando este formulario. Si deseas solicitar información sobre otros servicios o canales de comunicación, por favor ponte en contacto directamente con el área encargada</p></div>",
			TitleCesantias:"Contactenos",
			IdEnviar: "Enviar",
			IdCancelar:"Cancelar",
			ContentType:"RetiroCesantias",
			IdDiv:"spForm",
			TitleForm:"Nuevo Retiro de Cesantias",
			fields:[{field:"NombrePersona1"},{field:"NumeroCelular"},{field:"CorreoElectronico"}]
	},	
	InitCesantias:function(){
		try {
			sputilities.showModalDialog(Cesantias.parameters.TitleCesantias,null,null,Cesantias.parameters.bodyItem,function(data){
			},);
		}catch (e){
			console.log(e);
		}
	},
	loadUserInfo:function(){
		try {
			//running the query with parameters
			sputilities.getMyUserProfileProperties("",function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d;
				Cesantias.parameters.resultItems = resultItems;
				Cesantias.setValuesUserInfo();
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	setValuesUserInfo:function(){
	  	document.getElementById(Cesantias.parameters.fields[0].field).value = Cesantias.parameters.resultItems.DisplayName ? Cesantias.parameters.resultItems.DisplayName: "";
	  	document.getElementById(Cesantias.parameters.fields[1].field).value = Cesantias.parameters.resultItems.CellPhone ? Cesantias.parameters.resultItems.CellPhone: "";
	  	document.getElementById(Cesantias.parameters.fields[2].field).value = Cesantias.parameters.resultItems.Email ? (Cesantias.parameters.resultItems.Email).toLowerCase(): "";
	},

	Initialize:function(){
		try {
		Cesantias.loadNewForm();
		}
		catch (e){
			console.log(e);
		}
	},
	loadNewForm:function(){
	spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:Cesantias.parameters.IdEnviar,
			id:Cesantias.parameters.IdEnviar,
			success:function(){return true;},
			error:function(){},
			preSave:function(){
			//Cesantias.fieldNumber();
			return true;
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:Cesantias.parameters.IdCancelar,
			id:Cesantias.parameters.IdCancelar,
			callback:""
		},
	],
	title:Cesantias.parameters.TitleForm,
	contentTypeName:Cesantias.parameters.ContentType,
	containerId:Cesantias.parameters.IdDiv,
	allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:5,
  	minAttachments:1,

	successLoadNewForm:function(){
		//Cesantias.InitCesantias();
		Cesantias.validate();
		Cesantias.loadUserInfo();
		
	} 
	};
	spForms.loadNewForm();
	},
	validate:function(input){
			//var uno =document.getElementById('MontoSolicitado')
	//uno.setAttribute('onchange','Cesantias.FormatNumero(this)')
	//uno.setAttribute('onkeyup','Cesantias.FormatNumero(this)')

		var input = document.getElementById('CorreoElectronico');
			if(input != null){
				input.addEventListener('change',function(event){
				var email = this.value;
		  			if (Cesantias.validateEmail(email)) {
		  				var uno=document.getElementsByClassName('form-group row')[3]
   							uno.removeAttribute('sp-invalid')
   			  				} 	
   			  		else{
   						var uno=document.getElementsByClassName('form-group row')[3]
   						uno.setAttribute('sp-invalid','Formato de correo invalido.')
   			  			}
  				return false;
  					});
					}
	},
	fieldNumber:function(input){
	var uno =document.getElementById('MontoSolicitado')
	var dos =uno.value;
	var todo = "";
	var tres = dos.split('$')[1].split(',');
	for(var i=0; i<tres.length; i++){
	var todoo = tres[i];
	todo = todo+todoo 
	console.log(todo)
	} 
	document.getElementById('MontoSolicitado').value = todo;
	console.log("el numero es"+todo);

		},
	
	
	validateEmail:function(email){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 		return re.test(email);
 	 	},
 	 	FormatNumero:function(input){
		try {
				var e = "$"
				var num = input.value.replace(/\,/g,'');
				var num = num.replace(/\$/g,'');
				if(!isNaN(num)){
				num = num.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
				num = num.split('').reverse().join('').replace(/^[\,]/,'');
				input.value =  e+num;
				var numero2 = input.value.replace(/\,/g,'');
				var numero3 = numero2.replace(/\$/g,'');//Variable que se debe guardar, variable sin puntos de miles.
				}
				 else{ alert('Solo se permiten numeros');
				input.value = input.value.replace(/[^\d\.]*/g,'');
				}		
			
		}
		catch (e){
			console.log(e);
		}
	},
}
Cesantias.Initialize();


