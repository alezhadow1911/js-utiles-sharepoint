﻿var ctrlForm = {
    parameters: {
        formats: {
            "queryStaticNameRow": "[sp-static-name='{0}']",
            "htmlNotAllowed": "<div><h4>{0}</h4></div>",
            "htmlNotQuotas": "<div><h4>{0}</h4></div> <div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
            "querySelectRadio": 'input[type="radio"][value="{0}"]:checked',
            "queryRadio": 'input[type="radio"][value="{0}"]',
            "emailUsers": "{0};",
            "dateFormat": "DD-MM-YYYY",
            "dateFormatJson": "dd/MM/yyyy hh:mm tt",
            "formFormat": "{0}//{1}{2}/{3}.aspx?ID={4}&source={5}"
        },
        forms: {
            "NewForm": "ctrlForm.redirectCustomForm('NuevaSolicitud')",
            "EditForm": "ctrlForm.redirectCustomForm('CorregirSolicitud')",
            "DispForm": "ctrlForm.redirectCustomForm('VerSolicitud')",
            "NuevaSolicitud": "ctrlForm.loadNew()",
            "Aprobacion": "ctrlForm.loadApprove()",
            "CorregirSolicitud": "ctrlForm.loadEditApplicant()",
            "TH": "ctrlForm.loadApply()",
            "VerSolicitud": "ctrlForm.loadReview()",
            "PendientesAprobar": "",
            "Aprobadas": ""
        },
        messages: {
            "idSaveForm": "enviarSolicitud",
            "idCancelForm": "cancelar",
            "idEditForm": "enviarCorreccion",
            "idApproveForm": "aprobar",
            "idRejectForm": "solicitarCorreccion",
            "idApplyForm": "aplicarNovedad",
            "textSaveForm": "Enviar",
            "textCancelForm": "Cancelar",
            "textEditForm": "Enviar",
            "textApproveForm": "Aprobar",
            "textRejectForm": "Rechazar",
            "textApplyForm": "Novedad Aplicada",
            "textNotAllowed": "Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior",
            "textNotQuotas": "En estos momentos no se cuentan con cupos disponibles para ninguna capacitacion."
        },
        columns: {
            "comments": {
                "staticName": "ObservacionesRechazoInscripcion"
            },
            "commentsTH": {
                "staticName": "ObservacionesTH"
            },

            "displayName": {
                "staticName": "NombrePersona1"
            },
            "idDoc": {
                "staticName": "NombrePersona1"
            },
            "cellPhone": {
                "staticName": "NumeroCelular"
            },
            "email": {
                "staticName": "CorreoElectronico"
            }
        },
        states: [
            "Solicitado",
            "Aprobado",
            "Rechazado"
        ],
        parametersHTML: {
            "spHidden": "sp-hidden",
            "spDisable": "sp-disable",
            formrow:"form-group row",
        },
        ID: {
            ContentClose: ".col-md-6.col-xs-6.delete-doc",
            ContentAdd: "a#sp-forms-add-attachment",
            Attachment: "div[sp-static-name='sp-forms-attachment']",
            Correo: "CorreoElectronico",
            CorreoWork: "CorreoTrabajoV",
            staticCorreo: "[sp-static-name='CorreoElectronico']",
            Observaciones:"div[sp-static-name='ObservacionesRechazoInscripcion']",
            IdNombreCapacitacion:"NombreCapacitacion",
            IdCapacitacion:"IDCapacitacion",
            IdFechacapacitacion:"FechaCapacitacion",
            dataCupos:"data-Cupos",
            dataId:"data-ID",
            helpFechaCapacitacion:"help-block-error-message-FechaCapacitacion",
            table:"table",
            labelCapacitacion:"CapacitacionLabel",
            icoHelpFechaCapacitacion:"ico-help-FechaCapacitacion",
            input:"input",
            eleccionRadio:"div[sp-static-name='eleccion'] .radio",
            id:"id",
            textFecha:"Seleccione una fecha",
            textOption:"Seleccione una opcion",
			textSelect:"Seleccione..",
			radio:"radio",
			selectName:"SelectName",           
            icoHelpCapacitacion:"ico-help-Capacitacion",
            eventOnclick:"onclick",
            functionjs:"javascript:spForms.showHelpDescription('NombreCapacitaciones',this)",
            icoHelpDateCapacitacion:"ico-help-DateCapacitacion",
            functionjsDate:"javascript:spForms.showHelpDescription('FechaCapacitaciones',this)",
            div:"div",
            clase:"class",
            formCheckbox:"form-group row form-checkbo",
            spStaticName:"sp-static-name",
            spType:"sp-type",
            choice:"choice",
            eleccion:"eleccion",
			formHorizontal:"form-body form-horizontal",
			IdNameCapacitacion:"Capacitaciones"

        },
        style: {
            None: "none",
            Opacity: "0.7",
            Block: "block",
            //Atributtes
            Invalid: "sp-invalid",
            Message: "Formato de correo invalido"
        },
        selectorFormContent: ".form-content",
        query: "?$select*&$filter=IDCreado eq {0}",
        listName: "Voluntarios",
        listNameCapacitaciones: "Capacitaciones",
        querycapacitaciones: "?$select=*,NombreCapacitacion,CuposCapacitaciones&$filter=(CuposCapacitaciones gt 0)&$orderby=NombreCapacitacion",
        Divcontent: "<label class='col-sm-4 control-label' >Nombre Capacitación</label><div class='col-sm-8'><div class='form-control-help'><Select class='form-control input-lg' id='SelectName' onchange='javascript:ctrlForm.click(this);'><option value='{0}'>{0}</option>",
        DivHour: "<label class='col-sm-4 control-label' style='display:none;' id='CapacitacionLabel'>Fecha Capacitación<i>*</i></label><div class='col-sm-8'><div class='form-control-help'><div class='list-checkbox-radio'><div class='radio' id={1} style='display:none;'>",
        DivHourSecond: "<div class='radio' id={1} style='display:none;'>",
        DivSelectInput: "<label><input type='radio' id={1} name='Fecha' onchange='javascript:ctrlForm.clickInput(this);' value='{0}' data-ID='{2}' data-Cupos='{3}'>{0}</label><br>",
        DivOption: "<option  value='{0}'>{0}</option>",

        cellPhone: "CellPhone",
        functionLoadMyProperties: "ctrlForm.loadMyProperties",
        containerId: "container-form",
        description: "Complete la información para la solicitud",
        descriptionEdit: "Corrija la información de la solicitud",
        descriptionApply: "Revise la información y aplique la novedad si corresponde",
        descriptionApprove: "Revise la información y apruebe o envíe a corrección según corresponda",
        descriptionReview: "Resultado de la solicitud",
        contentTypeName: "Inscripcion",
        formIsLoaded: false,
        allowAttachments: false,
        requiredAttachments: false,
        maxAttachments: 20,
        minAttachments: 0,
        maxSizeInMB: 3,
        listNameConsecutive: "ControlRadicados",
        idConsecutive: 12,
        listUsersFlow: "ResponsablesTH",
        listNameTH: "ParametrosSolicitudes",
        queryTH: "?$filter=ID eq 3",
        viewUrl: "",
        editUrl: "",
        approveUrl: "",
        applyUrl: "",
        listViewApply: "",
        listViewApprove: "",
        queryUsers: "?$filter=FlujoAsociadoId eq 12&$select=ResponsablesAbonos/EMail,EtapaResponsable&$expand=ResponsablesAbonos",
        flowUrl: "https://prod-54.westus.logic.azure.com:443/workflows/b741acec24ad4e5a8c5215d00e05b557/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=eX1lOc59gYM8hhhHCBgfYBi8lxgVhTXodnIkqvfDhCE"


    },
    global: {
        formName: null,
        createdBy: null,
        created: null,
        approveUsers: "",
        applyUsers: "",
        applyManager: "",
        applyTH: ""
    },
    initialize: function() {
        var partsOfUrl = window.location.href.split('?')[0].split('/');
        ctrlForm.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];
        eval(ctrlForm.parameters.forms[ctrlForm.global.formName]);
    },
    callFlowService: function() {
        document.getElementById('s4-workspace').scrollTop = 0;
        document.querySelector('.form-content').style.opacity = 0;
        document.querySelector('.loader-container').style.opacity = 1;
        document.querySelector('.loader-container').style.height = "auto";


        ctrlForm.parameters.viewUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[7], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.editUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[5], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.approveUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[4], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.applyUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[6], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.listViewApply = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[9]);
        ctrlForm.parameters.listViewApprove = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[8]);

        var objService = {
            "currentItemID": spForms.currentItemID,
            "currentItem": {
                "createdBy": ctrlForm.global.createdBy,
                "nombres": spForms.loadedFields.NombreVoluntario.value,
                "ciudad": spForms.loadedFields.CiudadInscripcion.value,
                "nombreCapacitacion": spForms.loadedFields.NombreCapacitacion.value,
                "ItemID": spForms.currentItemID,
                "fechaCapacitacion": spForms.loadedFields.FechaCapacitacion.value,
                "estacion": spForms.loadedFields.Estacion.value,
                "observaciones": spForms.loadedFields.ObservacionesRechazoInscripcion.value,
                "idCapacitacion": Math.round(spForms.loadedFields.IDCapacitacion.value),


                //"consecutivo":spForms.loadedFields.NumeroRadicado.value,
                //"base":spForms.loadedFields.BasePersona.value.label,
                //"fechaCreacion":ctrlForm.global.created,
                //"comentarios":spForms.loadedFields.ComentariosTH.value,
                //"Correo":spForms.listInfo.title
            },
            "state": spForms.loadedFields.EstadoInscripcion.value,
            "titleCorreo": spForms.listInfo.title,
            "approveUsers": ctrlForm.global.approveUsers,
            "applyUsers": ctrlForm.global.applyUsers,
            "siteUrl": sputilities.contextInfo().webAbsoluteUrl,
            "listName": sputilities.contextInfo().listTitle,
            "listNameConsecutive": ctrlForm.parameters.listNameConsecutive,
            "idConsecutive": ctrlForm.parameters.idConsecutive,
            "entityType": spForms.listInfo.entityItem,
            "viewUrl": ctrlForm.parameters.viewUrl,
            "editUrl": ctrlForm.parameters.editUrl,
            "approveUrl": ctrlForm.parameters.approveUrl,
            "applyUrl": ctrlForm.parameters.applyUrl,
            "listViewApply": ctrlForm.parameters.listViewApply,
            "listViewApprove": ctrlForm.parameters.listViewApprove
        }
        utilities.http.callRestService({
            url: ctrlForm.parameters.flowUrl,
            method: utilities.http.methods.POST,
            data: JSON.stringify(objService),
            headers: {
                "accept": utilities.http.types.jsonOData,
                "content-type": utilities.http.types.jsonOData
            },
            success: function(data) {
                document.querySelector('.loader-container').style.opacity = 0;
                document.querySelector('.form-content').style.opacity = 1;
                document.querySelector('.loader-container').style.height = 0;

                spForms.cancelForm(null);
            },
            error: function(xhr) {
                console.log(xhr);
            }
        });

    },
    getUsers: function() {
        sputilities.getItems(ctrlForm.parameters.listUsersFlow, ctrlForm.parameters.queryUsers,
            function(data) {
                var data = utilities.convertToJSON(data);
                if (data != null) {
                    var items = data.d.results;
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        for (var j = 0; j < item.ResponsablesAbonos.results.length; j++) {
                            var user = item.ResponsablesAbonos.results[j];
                            if (item.EtapaResponsable == ctrlForm.parameters.states[2]) {
                                ctrlForm.global.applyUsers += String.format(ctrlForm.parameters.formats.emailUsers, user.EMail);
                            } else {
                                ctrlForm.global.approveUsers += String.format(ctrlForm.parameters.formats.emailUsers, user.EMail);
                            }
                        }
                    }
                }
            },
            function(xhr) {
                console.log(xhr);
            }
        );
    },
    changeState: function(state) {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio, state));
        if (radio != null) {
            radio.click();
        }
    },
    hideNotAllowed: function() {
        var html = String.format(ctrlForm.parameters.formats.htmlNotAllowed, ctrlForm.parameters.messages.textNotAllowed);
        var formContent = document.querySelector(ctrlForm.parameters.selectorFormContent);
        if (formContent != null) {
            formContent.innerHTML = html;
        }
    },
    hideNotQuotas: function() {
        var html = String.format(ctrlForm.parameters.formats.htmlNotQuotas, ctrlForm.parameters.messages.textNotQuotas);
        var formContent = document.querySelector(ctrlForm.parameters.selectorFormContent);
        if (formContent != null) {
            formContent.innerHTML = html;
        }
    },

    validateUserCreated: function() {
        if (spForms.currentItem.AuthorId != sputilities.contextInfo().userId) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        ctrlForm.hideAttachments();
    },
    validateAllUsers: function() {
        if (spForms.currentItem.AuthorId != sputilities.contextInfo().userId || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
    },
    validateEditUser: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[0])) == null;
        if (ctrlForm.global.applyTH != sputilities.contextInfo().userEmail || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        return true;
    },
    //Novedades
    validateApplyUser: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[2])) == null;
        if (ctrlForm.global.applyUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        ctrlForm.hideAttachments();
    },
    //Ventanilla TH
    validateApproveUser: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[1])) == null;
        if (ctrlForm.global.approveUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        ctrlForm.hideAttachments();
    },
    validateApplyManager: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[1])) == null;
        if (ctrlForm.global.applyManager != sputilities.contextInfo().userEmail || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        ctrlForm.hideAttachments();
        //ctrlForm.hideDate();
    },
    validateApplyTH: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[0])) == null;
        if (ctrlForm.global.applyTH != sputilities.contextInfo().userEmail || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        //ctrlForm.hideAttachments();
        //ctrlForm.hideDate();
    },

    hideAttachments: function() {
        var i;
        var CloaseAttachments = document.querySelectorAll(ctrlForm.parameters.ID.ContentClose);
        for (i = 0; i < CloaseAttachments.length; i++) {
            CloaseAttachments[i].style.display = ctrlForm.parameters.style.None;
        }
        document.querySelector(ctrlForm.parameters.ID.ContentAdd).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.Attachment).style.opacity = ctrlForm.parameters.style.Opacity;
    },

    hideComments: function() {
        var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow, ctrlForm.parameters.columns.comments.staticName));
        if (ctrlComments != null) {
            ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden, true);
        }
    },
    blockedComments: function() {
        var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow, ctrlForm.parameters.columns.comments.staticName));
        if (ctrlComments != null) {
            ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden, true);
        }
    },

    loadReview: function() {
        var actions = [{
            type: spForms.parameters.cancelAction,
            name: ctrlForm.parameters.messages.textCancelForm,
            id: ctrlForm.parameters.messages.idCancelForm,
            callback: null
        }, ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionReview;
        ctrlForm.loadEdit(actions, ctrlForm.validateUserCreated);
    },
    loadApply: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textApproveForm,
                id: ctrlForm.parameters.messages.idApproveForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.changeState(ctrlForm.parameters.states[4]);
                    return true;
                }
            },
            {
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textRejectForm,
                id: ctrlForm.parameters.messages.idRejectForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    spForms.loadedFields[ctrlForm.parameters.columns.commentsTH.staticName].required = true;
                    ctrlForm.changeState(ctrlForm.parameters.states[5]);
                    return true;
                }
            },
            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionApply;
        ctrlForm.loadEdit(actions, null); //revisar

    },
    loadApprove: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textApproveForm,
                id: ctrlForm.parameters.messages.idApproveForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.Fields();
                    ctrlForm.changeState(ctrlForm.parameters.states[1]);
                    return true;
                }
            },
            {
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textRejectForm,
                id: ctrlForm.parameters.messages.idRejectForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
                    ctrlForm.changeState(ctrlForm.parameters.states[2]);
                    return true;
                }
            },
            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
        ctrlForm.loadEdit(actions, null);

    },
    loadEditApplicant: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textEditForm,
                id: ctrlForm.parameters.messages.idEditForm,
                success: function() {
                    //ctrlForm.callFlowService();
                    return true;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.changeState(ctrlForm.parameters.states[0]);
                    return true;
                }
            },
            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionEdit;
        ctrlForm.loadEdit(actions, null);
    },
    loadEdit: function(actions, callBack) {
        ctrlForm.getUsers();
        spForms.options = {
            actions: actions,
            description: ctrlForm.parameters.description,
            contentTypeName: ctrlForm.parameters.contentTypeName,
            containerId: ctrlForm.parameters.containerId,
            allowAttachments: ctrlForm.parameters.allowAttachments,
            requiredAttachments: ctrlForm.parameters.requiredAttachments,
            maxAttachments: ctrlForm.parameters.maxAttachments,
            minAttachments: ctrlForm.parameters.minAttachments,
            maxSizeInMB: ctrlForm.parameters.maxSizeInMB,
            successLoadEditForm: function() {
                ctrlForm.global.createdBy = spForms.currentItem.Author.EMail;
                ctrlForm.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(ctrlForm.parameters.formats.dateFormat);
                ctrlForm.formIsLoaded = true;
                if (ctrlForm.global.formName == "CorregirSolicitud") {
                    //ctrlForm.FormInit();
                    ctrlForm.GetTH();
                }
                if (ctrlForm.global.formName == "Aprobacion") {
                    ctrlForm.GetTH();
                }

                if (typeof callBack == "function") {
                    callBack();
                }
            }
        };
        spForms.loadEditForm();

    },
    loadNew: function() {
        ctrlForm.getUsers();
        ctrlForm.global.createdBy = sputilities.contextInfo().userEmail;
        ctrlForm.global.created = moment.utc(new Date()).format(ctrlForm.parameters.formats.dateFormat);
        sputilities.callFunctionOnLoadBody(ctrlForm.parameters.functionLoadMyProperties);
        ctrlForm.loadNewForm();
    },
    loadNewForm: function() {
        spForms.options = {
            actions: [{
                    type: spForms.parameters.saveAction,
                    name: ctrlForm.parameters.messages.textSaveForm,
                    id: ctrlForm.parameters.messages.idSaveForm,
                    success: function() {
                        ctrlForm.callFlowService();
                        return false;
                    },
                    error: function() {
                        return true;
                    },
                    preSave: function() {
                        ctrlForm.changeState(ctrlForm.parameters.states[0]);
                        return true;
                    }
                },
                {
                    type: spForms.parameters.cancelAction,
                    name: ctrlForm.parameters.messages.textCancelForm,
                    id: ctrlForm.parameters.messages.idCancelForm,
                    callback: null
                },
            ],
            description: ctrlForm.parameters.description,
            contentTypeName: ctrlForm.parameters.contentTypeName,
            containerId: ctrlForm.parameters.containerId,
            allowAttachments: ctrlForm.parameters.allowAttachments,
            requiredAttachments: ctrlForm.parameters.requiredAttachments,
            maxAttachments: ctrlForm.parameters.maxAttachments,
            minAttachments: ctrlForm.parameters.minAttachments,
            maxSizeInMB: ctrlForm.parameters.maxSizeInMB,
            successLoadNewForm: function() {
                ctrlForm.formIsLoaded = true;
                ctrlForm.hideComments();
                ctrlForm.FormInit();


            }
        };
        spForms.loadNewForm();
    },
    loadMyProperties: function() {
        sputilities.getUserProfileProperties(sputilities.contextInfo().userEmail,
            function(userInfo) {
                var objUser = utilities.convertToJSON(userInfo);
                if (objUser != null) {
                    var displayName = objUser.d.DisplayName;
                    var email = objUser.d.Email;
                    var cellPhone = ctrlForm.findProperty(objUser, ctrlForm.parameters.cellPhone);
                    var validateFormIsLoaded = setInterval(function() {
                        if (ctrlForm.formIsLoaded) {
                            ctrlForm.setValueToControl(ctrlForm.parameters.columns.displayName.staticName, displayName);
                            ctrlForm.setValueToControl(ctrlForm.parameters.columns.email.staticName, email);
                            ctrlForm.setValueToControl(ctrlForm.parameters.columns.cellPhone.staticName, cellPhone);
                            clearInterval(validateFormIsLoaded);
                        }
                    }, 500);
                }
            },
            function(xhr) {
                console.log(xhr)
            }
        );
    },
    setValueToControl: function(staticName, value) {
        if (value != null) {
            var ctrl = document.getElementById(staticName);
            if (ctrl != null) {
                ctrl.value = value;
            }
        }
    },
    FormInit: function() {
        try {
            var DivCapacitaciones = document.createElement(ctrlForm.parameters.ID.div);
            DivCapacitaciones.setAttribute(ctrlForm.parameters.ID.clase, ctrlForm.parameters.parametersHTML.formrow);
            DivCapacitaciones.setAttribute(ctrlForm.parameters.ID.id, ctrlForm.parameters.ID.IdNameCapacitacion);
            var uno = document.getElementsByClassName(ctrlForm.parameters.ID.formHorizontal);
            uno[0].insertBefore(DivCapacitaciones, uno[0].childNodes[7]);



            var UserIdCurrent = _spPageContextInfo.userId
            //adding parameters to query
            var linkQuery = String.format(ctrlForm.parameters.query, UserIdCurrent);

            //running the query with parameters
            sputilities.getItems(ctrlForm.parameters.listName, linkQuery, function(data) {
                //save the results to an array
                var resultItems = JSON.parse(data).d.results;
                if (resultItems.length > 0) {
                    ctrlForm.PrintSelectDate()

                }
                if (resultItems.length == 0) {
                    ctrlForm.hideNotAllowed();
                }
                for (var i = 0; i < resultItems.length; i++) {
                    var currentItem = resultItems[i];
                }
            });
        } catch (e) {
            console.log(e);
        }
    },
    PrintSelectDate: function() {
        try {

            var htmll = "";
            var html = "";
            var htmla = "";

            sputilities.getItems(ctrlForm.parameters.listNameCapacitaciones, ctrlForm.parameters.querycapacitaciones, function(data) {
                var resultItems = JSON.parse(data).d.results;
                if (resultItems.length == 0) {
                    ctrlForm.hideNotQuotas();
                } else {
                    for (var i = 0; i < resultItems.length; i++) {
                        var currentItem = resultItems[i];
                        var previousItem = resultItems[i - 1];
                        var nextItem = resultItems[i + 1];
                        var Nombre = currentItem.NombreCapacitacion;
                        var Cupos = currentItem.CuposCapacitaciones;
                        var ID = currentItem.ID;
                        var Cupos = currentItem.CuposCapacitaciones;
                        var FechaCurrent = currentItem.FechaCapacitacion;
                        var CurrentDate = new Date(FechaCurrent);
                        var Fecha = CurrentDate.format(ctrlForm.parameters.formats.dateFormatJson)

                        if (i == 0) {
                            //more of one
                            htmla += String.format(ctrlForm.parameters.Divcontent, Nombre);
                            //only one
                            html += String.format(ctrlForm.parameters.Divcontent, Nombre);
                            html += "</select><a class='glyphicon glyphicon-question-sign ico-help' id='ico-help-Capacitacion' aria-hidden='true' aria-hidden='true'></a></div><span id='help-block-NombreCapacitaciones' class='help-block forms-hide-element'>Campo para seleccionar la capacitación</span><span id='help-block-error-message-NombreCapacitaciones' class='help-block forms-hide-element error-message'></span></div></div>";
                            document.getElementById(ctrlForm.parameters.ID.IdNameCapacitacion).innerHTML = html;
                        } else {
                            if (previousItem != undefined && Nombre != previousItem.NombreCapacitacion && nextItem != undefined) {
                                htmla += String.format(ctrlForm.parameters.DivOption, Nombre);
                            } else {
                                if (resultItems[i - 1].NombreCapacitacion != Nombre) {
                                    htmla += String.format(ctrlForm.parameters.DivOption, Nombre);
                                    document.getElementById(ctrlForm.parameters.ID.IdNameCapacitacion).innerHTML = htmla;

                                }

                            }
                            if (i == resultItems.length - 1) {
                                htmla += "</select><a class='glyphicon glyphicon-question-sign ico-help' id='ico-help-Capacitacion' aria-hidden='true' aria-hidden='true'></a></div><span id='help-block-NombreCapacitaciones' class='help-block forms-hide-element'>Campo para seleccionar la capacitación</span><span id='help-block-error-message-NombreCapacitaciones' class='help-block forms-hide-element error-message'></span></div></div>";
                                document.getElementById(ctrlForm.parameters.ID.IdNameCapacitacion).innerHTML = htmla;
                            }
                        }

                        if (i == 0) {
                            var IdDiv = Nombre.split(" ")[0];
                            htmll += String.format(ctrlForm.parameters.DivHour, Fecha, IdDiv);
                            //document.getElementById(Content.parameters.divID).innerHTML = htmll;
                        } else {
                            if (previousItem != undefined && Nombre != previousItem.NombreCapacitacion && nextItem != undefined) {
                                htmll += "</div>" + String.format(ctrlForm.parameters.DivHourSecond, Fecha, Nombre);
                            }
                            if (i == resultItems.length - 1) {
                                if (resultItems[i - 1].NombreCapacitacion != Nombre) {
                                    htmll += "</div>" + String.format(ctrlForm.parameters.DivHourSecond, Fecha, Nombre);


                                }
                            }

                        }



                        if (i == resultItems.length) {

                        } else {

                            if (i == 0) {
                                htmll += String.format(ctrlForm.parameters.DivSelectInput, Fecha, Nombre, ID, Cupos);
                            } else {
                                htmll += String.format(ctrlForm.parameters.DivSelectInput, Fecha, Nombre, ID, Cupos);
                            }
                            if (i == resultItems.length - 1) {
                                htmll += "</div></div><a class='glyphicon glyphicon-question-sign ico-help'  id='ico-help-DateCapacitacion' aria-hidden='true' ></a></div><span id='help-block-FechaCapacitaciones' class='help-block forms-hide-element'>Campo para seleccionar fecha	</span><span id='help-block-error-message-FechaCapacitacion' class='help-block forms-hide-element error-message'></span></div>";
                            }
                        }



                    }
                }



                //<div class='form-group row form-checkbox' sp-static-name='Eleccion' sp-invalid='Seleccion una fecha' sp-required='true' sp-type='choice'> 
                var Divtitle = document.createElement(ctrlForm.parameters.ID.div);
                Divtitle.setAttribute(ctrlForm.parameters.ID.clase, ctrlForm.parameters.ID.formCheckbox);
                Divtitle.setAttribute(ctrlForm.parameters.ID.spStaticName, ctrlForm.parameters.ID.eleccion);
                Divtitle.setAttribute(ctrlForm.parameters.style.Invalid,  ctrlForm.parameters.ID.textFecha);
                Divtitle.setAttribute(ctrlForm.parameters.ID.spType, ctrlForm.parameters.ID.choice);
                var uno = document.getElementsByClassName(ctrlForm.parameters.ID.formHorizontal);
                uno[0].insertBefore(Divtitle, uno[0].childNodes[8]);
                Divtitle.innerHTML = htmll;


                //document.getElementById(ctrlForm.parameters.divinput).innerHTML = htmll;
                var OnclickNameCapacitacion = document.getElementById(ctrlForm.parameters.ID.icoHelpCapacitacion);
                OnclickNameCapacitacion.setAttribute(ctrlForm.parameters.ID.eventOnclick, ctrlForm.parameters.ID.functionjsDate);
                var OnclickDateCapacitacion = document.getElementById(ctrlForm.parameters.ID.icoHelpDateCapacitacion);
                OnclickDateCapacitacion.setAttribute(ctrlForm.parameters.ID.eventOnclick, ctrlForm.parameters.ID.functionjs);

                var SelectCapacitacion = document.getElementById(ctrlForm.parameters.ID.selectName).value;
                document.getElementById(ctrlForm.parameters.ID.IdNombreCapacitacion).value = SelectCapacitacion;
                var Name = SelectCapacitacion.split(" ")[0];
                document.getElementById(Name).style.display = ctrlForm.parameters.style.Block;
                document.getElementById(ctrlForm.parameters.ID.labelCapacitacion).style.display = ctrlForm.parameters.style.Block;


                document.getElementById(ctrlForm.parameters.ID.selectName).addEventListener('change', function() {
                 document.getElementById(ctrlForm.parameters.ID.IdNombreCapacitacion).value = this.selectedOptions[0].innerHTML;

                });
            });
        } catch (e) {
            console.log(e);
        }
    },
    click: function(ctrl) {

        var SelectCapacitacion = document.getElementById(ctrlForm.parameters.ID.selectName).value;
        var FechasForm = document.getElementsByClassName(ctrlForm.parameters.ID.radio);
        if (SelectCapacitacion == ctrlForm.parameters.ID.textSelect) {
            var CurrentSelect = document.getElementsByClassName(ctrlForm.parameters.parametersHTML.formrow)[3]
            CurrentSelect.setAttribute(ctrlForm.parameters.style.Invalid, ctrlForm.parameters.ID.textOption)
            document.getElementById(ctrlForm.parameters.ID.icoHelpFechaCapacitacion).style.display = ctrlForm.parameters.style.None;
            document.getElementById(ctrlForm.parameters.ID.labelCapacitacion).style.display = ctrlForm.parameters.style.None;
            document.getElementById(ctrlForm.parameters.ID.helpFechaCapacitacion).style.display = ctrlForm.parameters.style.Block;
            for (var i = 0; i < FechasForm.length; i++) {
                var ID = FechasForm[i].getAttribute(ctrlForm.parameters.ID.id);
                document.getElementById(ID).style.display = ctrlForm.parameters.style.None;

            }

        } else {
            var CurrentSelect = document.getElementsByClassName(ctrlForm.parameters.parametersHTML.formrow)[3]
            CurrentSelect.removeAttribute(ctrlForm.parameters.style.Invalid)
            var FechasForm = document.querySelectorAll(ctrlForm.parameters.ID.eleccionRadio)
            for (var i = 0; i < FechasForm.length; i++) {
                var ID = FechasForm[i].getAttribute(ctrlForm.parameters.ID.id);
                document.getElementById(ID).style.display = ctrlForm.parameters.style.None;

            }
            for (var i = 0; i < FechasForm.length; i++) {
                var input = FechasForm[i].querySelectorAll(ctrlForm.parameters.ID.input);
                for (var j = 0; j < input.length; j++) {
                    var CurrentSelect = document.getElementsByClassName(ctrlForm.parameters.parametersHTML.formrow)[5]
                    CurrentSelect.setAttribute(ctrlForm.parameters.style.Invalid, ctrlForm.parameters.ID.textFecha)
                }
            }
            var Name = SelectCapacitacion.split(" ")[0];
            document.getElementById(Name).style.display = ctrlForm.parameters.style.Block;
            document.getElementById(ctrlForm.parameters.ID.icoHelpFechaCapacitacion).style.display = ctrlForm.parameters.ID.table;
            document.getElementById(ctrlForm.parameters.ID.labelCapacitacion).style.display = ctrlForm.parameters.ID.table;
            document.getElementById(ctrlForm.parameters.ID.helpFechaCapacitacion).style.display = ctrlForm.parameters.style.Block;

        }


    },



    clickInput: function(ctrl) {
        var ID = ctrl.getAttribute(ctrlForm.parameters.ID.data-ID)
        var Cupos = ctrl.getAttribute(ctrlForm.parameters.ID.data-Cupos)

        var CurrentDate = ctrl.value;
        var CurrentInput = document.getElementsByClassName(ctrlForm.parameters.parametersHTML.formrow)[5]
        CurrentInput.removeAttribute(ctrlForm.parameters.style.Invalid)

        document.getElementById(ctrlForm.parameters.ID.IdFechacapacitacion).value = CurrentDate;
        document.getElementById(ctrlForm.parameters.ID.IdCapacitacion).value = ID;
    },

    valueCapacitacion: function() {
        document.getElementById(ctrlForm.parameters.ID.IdNombreCapacitacion).value = spForms.loadedFields.NombreCapacitacion.value
    },
    findProperty: function(objUser, name) {
        var properties = objUser.d.UserProfileProperties.results;
        for (var i = 0; i < properties.length; i++) {
            var property = properties[i];
            if (property.Key == name) {
                return property.Value;
            }
        }
    },
    GetTH: function() {
        sputilities.getItems(ctrlForm.parameters.listNameTH, ctrlForm.parameters.queryTH, function(data) {

            //save the results to an array
            var resultItems = JSON.parse(data).d.results;

            for (var i = 0; i < resultItems.length; i++) {

                var correo = resultItems[i].Correo;
                ctrlForm.global.applyTH = correo;
                ctrlForm.validateApplyTH();

            }
        });

    },

    getManagerTotal: function() {
        var idUserCreated = spForms.currentItem.AuthorId;
        sputilities.getUserInformation(idUserCreated, function(data) {
            var resultItems = JSON.parse(data).d;
            var Email = resultItems.EMail;
            sputilities.getUserProfileProperty('Manager', Email,
                function(data) {
                    var resultProperty = JSON.parse(data).d;
                    var EmailJefe = resultProperty.GetUserProfilePropertyFor;
                    var CurrentJefe = EmailJefe.split("|")[2];
                    ctrlForm.global.applyManager = CurrentJefe;
                    ctrlForm.validateApplyManager();
                });
        });
    },

    Fields: function() {
        spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = false;
        var uno = document.querySelector(ctrlForm.parameters.ID.Observaciones);
        uno.removeAttribute(ctrlForm.parameters.style.Invalid);

    },




    redirectCustomForm: function(name) {
        window.location.href = window.location.href.replace(ctrlForm.global.formName, name);
    }
}
ctrlForm.initialize();