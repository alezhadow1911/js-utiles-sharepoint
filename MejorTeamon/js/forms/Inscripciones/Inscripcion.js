﻿spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:"Enviar",
			id:"enviar",
			success:function(){
			UserApproved.FieldSucess();
			return false;	
			

			},
			error:function(){},
			preSave:function(){
		
			return true;
			
			}

		},
		{
			type:spForms.parameters.cancelAction,
			name:"Cancelar",
			id:"cancelar",
			callback:""
		},
	],
	description:"Completa la información correspondiente",
	contentTypeName:"Inscripcion",
	containerId:"spForm",
	successLoadNewForm:function(){
		UserApproved.initialize();
		//pqrsForm.addOnChangeTypeRequest();
	} 
};
spForms.loadNewForm();

var UserApproved = {
	//create parameters
	parameters:{
				/*component PopUp*/
			bodyItem:"<div class='contact'><p>Usted no ha aprobado el proceso para inscribirse a las capacitaciones de voluantarios.</p></div>",
			TitlePopUp:"Usuario No aprobado",
			listName: "Voluntarios",
			query:"?$select*&$filter=IDCreado eq {0}",
			listNameCapacitaciones: "Capacitaciones",
			divselect:"Capacitaciones",
			divinput:"Fechas",			
			NameField:"NombreCapacitaciones",
			contentType: "Todos",
			Divcontent:"<label class='col-sm-4 control-label' >Nombre Capacitación</label><div class='col-sm-8'><div class='form-control-help'><Select class='form-control input-lg' id='SelectName' onchange='javascript:UserApproved.click(this);'><option value='{0}'>{0}</option>",
			DivHour:"<label class='col-sm-4 control-label' style='display:none;' id='CapacitacionLabel'>Fecha Capacitación<i>*</i></label><div class='col-sm-8'><div class='form-control-help'><div class='list-checkbox-radio'><div class='radio' id={1} style='display:none;'>",
			DivHourSecond:"<div class='radio' id={1} style='display:none;'>",
			DivSelectInput:"<label><input type='radio' id={1} name='Fecha' onchange='javascript:UserApproved.clickInput(this);' value='{0}' data-ID='{2}' data-Cupos='{3}'>{0}</label><br>",
			DivOption:"<option data-ID='{1}' value='{0}'>{0}</option>",
			querycapacitaciones:"?$select=*,NombreCapacitacion,CuposCapacitaciones&$filter=(CuposCapacitaciones gt 0)&$orderby=NombreCapacitacion",
			resultItems:null,
			textNotAllowed: "Su solicitud ha sido enviada exitosamente",
			selectorFormContent:".form-content",
			htmlNotAllowed:"<div><h4>{0}</h4><div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-enviar' value='Ok' onclick='UserApproved.Redirect()'></div></div>",

		
					
	},	
	initialize:function(){
		try {
					var DivCapacitaciones = document.createElement('div');
					DivCapacitaciones.setAttribute('class','form-group row');
					DivCapacitaciones.setAttribute('id','Capacitaciones');
					var uno=document.getElementsByClassName('form-body form-horizontal');
	    			 uno[0].insertBefore(DivCapacitaciones, uno[0].childNodes[7]);
			


			var UserIdCurrent = _spPageContextInfo.userId
			//adding parameters to query
			var linkQuery = String.format(UserApproved.parameters.query,UserIdCurrent);
			
			//running the query with parameters
				sputilities.getItems(UserApproved.parameters.listName,linkQuery,function(data){
				console.log("ingresa")
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				if(resultItems.length > 0){
				document.getElementById('spForm').style.display="block";
				console.log("usuario aprobado")
				UserApproved.PrintSelectDate()
				
				}
				if(resultItems.length == 0){
				document.getElementById('spForm').style.display="none";
				UserApproved.InitPopUp();
				console.log("Usuario no aprobado")
				}
				
				console.log(resultItems); 
				
				for (var i=0; i< resultItems.length; i++){
				var currentItem = resultItems[i];
							}
			});
		}
		catch (e){
			console.log(e);
		}
	},
	
	
	FieldSucess:function(){
		try {
			var html = String.format(UserApproved .parameters.htmlNotAllowed,UserApproved.parameters.textNotAllowed);
			var formContent = document.querySelector(UserApproved.parameters.selectorFormContent);
			formContent.innerHTML = html;
				}
		catch (e){
			console.log(e);
		}
	},
		Redirect:function(){
		try {
			window.location.href = window.location.href.replace("Nueva-Inscripcion","AllItems");		
				}
		catch (e){
			console.log(e);
		}
	},

	InitPopUp:function(){
		try {
		
				sputilities.showModalDialog(UserApproved.parameters.TitlePopUp,null,null,UserApproved.parameters.bodyItem,function(data){
				console.log("ingresa al pop up")				
			}
			);
			//UserApproved.direct();
		}
		catch (e){
			console.log(e);
		}
	},

	PrintSelectDate:function(){
		try {
		
		var htmll="";
		var html="";
		var htmla="";

			sputilities.getItems(UserApproved.parameters.listNameCapacitaciones,UserApproved.parameters.querycapacitaciones,function(data){
				console.log("ingresa")
				var resultItems = JSON.parse(data).d.results;
				
				console.log(resultItems); 
				
				for (var i=0; i< resultItems.length; i++){
				var currentItem = resultItems[i];
				var previousItem = resultItems[i-1];
				var nextItem = resultItems[i+1];
				var Nombre = currentItem.NombreCapacitacion;
				var Cupos = currentItem.CuposCapacitaciones;
				var ID = currentItem.ID;
				var Cupos = currentItem.CuposCapacitaciones;
				var FechaCurrent= currentItem.FechaCapacitacion;
				var CurrentDate = new Date(FechaCurrent);
			    var Fecha = CurrentDate.format("dd/MM/yyyy hh:mm tt")
				
					if (i== 0)
					{
						//more of one
						htmla += String.format(UserApproved.parameters.Divcontent,Nombre);
                        //only one
                        html += String.format(UserApproved.parameters.Divcontent,Nombre);
                        html += "</select><a class='glyphicon glyphicon-question-sign ico-help' id='ico-help-Capacitacion' aria-hidden='true' aria-hidden='true'></a></div><span id='help-block-NombreCapacitaciones' class='help-block forms-hide-element'>Campo para seleccionar la capacitación</span><span id='help-block-error-message-NombreCapacitaciones' class='help-block forms-hide-element error-message'></span></div></div>";
                        document.getElementById('Capacitaciones').innerHTML = html;
					}
					else
					{
						if(previousItem != undefined && Nombre != previousItem.NombreCapacitacion && nextItem != undefined){ 
							htmla += String.format(UserApproved.parameters.DivOption,Nombre,ID);
						}
						else{
						if(resultItems[i-1].NombreCapacitacion != Nombre )
						{
								htmla += String.format(UserApproved.parameters.DivOption,Nombre,ID);
								document.getElementById('Capacitaciones').innerHTML = htmla;

						}
						
						}
						if(i == resultItems.length - 1){
						htmla += "</select><a class='glyphicon glyphicon-question-sign ico-help' id='ico-help-Capacitacion' aria-hidden='true' aria-hidden='true'></a></div><span id='help-block-NombreCapacitaciones' class='help-block forms-hide-element'>Campo para seleccionar la capacitación</span><span id='help-block-error-message-NombreCapacitaciones' class='help-block forms-hide-element error-message'></span></div></div>";
						document.getElementById('Capacitaciones').innerHTML = htmla;
							}
								}
						
					if (i== 0)
					{
						var IdDiv = Nombre.split(" ")[0];
                        htmll += String.format(UserApproved.parameters.DivHour,Fecha,IdDiv);
                        //document.getElementById(Content.parameters.divID).innerHTML = htmll;
					}
					else
					{
						if(previousItem != undefined && Nombre != previousItem.NombreCapacitacion && nextItem != undefined){ 
                        htmll += "</div>" + String.format(UserApproved.parameters.DivHourSecond,Fecha,Nombre);
						}
							if(i == resultItems.length - 1){
								if(resultItems[i-1].NombreCapacitacion != Nombre )
						{
                        htmll += "</div>" + String.format(UserApproved.parameters.DivHourSecond,Fecha,Nombre);


						}
							}

						}
					


					if(i == resultItems.length ){
											
					}
					else
					{ 
				
                        if(i==0){
                     htmll += String.format(UserApproved.parameters.DivSelectInput,Fecha,Nombre,ID,Cupos);
                    }
                    else{
					  htmll += String.format(UserApproved.parameters.DivSelectInput,Fecha,Nombre,ID,Cupos);
					}
					if(i == resultItems.length - 1){
					htmll += "</div></div><a class='glyphicon glyphicon-question-sign ico-help'  id='ico-help-DateCapacitacion' aria-hidden='true' ></a></div><span id='help-block-FechaCapacitaciones' class='help-block forms-hide-element'>Campo para seleccionar fecha	</span><span id='help-block-error-message-FechaCapacitacion' class='help-block forms-hide-element error-message'></span></div>";
						}
                    }				
					
					
				
				}
					
					 
					 
					 //<div class='form-group row form-checkbox' sp-static-name='Eleccion' sp-invalid='Seleccion una fecha' sp-required='true' sp-type='choice'> 
					 var Divtitle = document.createElement('div');
					Divtitle.setAttribute('class','form-group row form-checkbo');
					Divtitle.setAttribute('sp-static-name','eleccion');
					Divtitle.setAttribute('sp-invalid','Seleccione una fecha');
					Divtitle.setAttribute('sp-type','choice');																				
					var uno=document.getElementsByClassName('form-body form-horizontal');
	    			 uno[0].insertBefore(Divtitle, uno[0].childNodes[8]);
					 Divtitle.innerHTML = htmll;


				//document.getElementById(UserApproved.parameters.divinput).innerHTML = htmll;
				var OnclickNameCapacitacion =document.getElementById('ico-help-Capacitacion');
    			OnclickNameCapacitacion.setAttribute("onclick","javascript:spForms.showHelpDescription('NombreCapacitaciones',this)");
    			var OnclickDateCapacitacion =document.getElementById('ico-help-DateCapacitacion');
    			OnclickDateCapacitacion.setAttribute("onclick","javascript:spForms.showHelpDescription('FechaCapacitaciones',this)");
    			
    			var SelectCapacitacion = document.getElementById("SelectName").value;
    			document.getElementById('NombreCapacitacion').value = SelectCapacitacion;
    			var Name = SelectCapacitacion .split(" ")[0];
				document.getElementById(Name).style.display = "block";
				document.getElementById('CapacitacionLabel').style.display = "block";

    			
				document.getElementById('SelectName').addEventListener('change',function(){
    			document.getElementById('NombreCapacitacion').value = this.selectedOptions[0].innerHTML;
    		
			});
				});	
			}
		catch (e){
			console.log(e);
		}
	},
	click:function(ctrl){
	
	 	var SelectCapacitacion = document.getElementById("SelectName").value;
	 	var FechasForm =document.getElementsByClassName("radio");
    			if(SelectCapacitacion == "Seleccione.."){
    			var CurrentSelect = document.getElementsByClassName('form-group row')[3]
				CurrentSelect.setAttribute('sp-invalid','Seleccione una opcion')
				document.getElementById('ico-help-FechaCapacitacion').style.display = "none";
				document.getElementById('CapacitacionLabel').style.display = "none";	
				document.getElementById('help-block-error-message-FechaCapacitacion').style.display = "block";					
				
    			console.log("esta en seleccione..")
    			for (var i=0; i< FechasForm.length; i++){
			var ID = FechasForm[i].getAttribute('id');
			document.getElementById(ID).style.display = "none";

		}

    			}
    	else{
    	var CurrentSelect = document.getElementsByClassName('form-group row')[3]
		CurrentSelect.removeAttribute('sp-invalid')
		var FechasForm =document.getElementsByClassName("radio");
		for (var i=0; i< FechasForm.length; i++){
			var ID = FechasForm[i].getAttribute('id');
			document.getElementById(ID).style.display = "none";

		}
			for(var i=0; i<FechasForm.length; i++)
			{
			console.log(FechasForm[i].querySelectorAll('input'))
			var input = FechasForm[i].querySelectorAll('input');
		for(var j=0; j<input.length; j++)
			{
			console.log(input[j].checked = false)
				var CurrentSelect = document.getElementsByClassName('form-group row')[5]
				CurrentSelect.setAttribute('sp-invalid','Seleccione una fecha')
			}
		}
		var Name = SelectCapacitacion .split(" ")[0];
		document.getElementById(Name).style.display = "block";
		document.getElementById('ico-help-FechaCapacitacion').style.display = "table";
		document.getElementById('CapacitacionLabel').style.display = "table";
		document.getElementById('help-block-error-message-FechaCapacitacion').style.display = "block";								
		
		}
		
		
	},
		clickInput:function(ctrl){
		var ID =ctrl.getAttribute('data-ID')
		var Cupos =ctrl.getAttribute('data-Cupos')
		
		var CurrentDate = ctrl.value;
		var CurrentInput = document.getElementsByClassName('form-group row')[5]
		CurrentInput.removeAttribute('sp-invalid')

    	document.getElementById('FechaCapacitacion').value = CurrentDate; 
    	document.getElementById('IDCapacitacion').value = ID;   	
	},



	
}