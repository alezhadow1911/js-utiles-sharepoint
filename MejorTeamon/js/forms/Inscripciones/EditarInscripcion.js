﻿spForms.options={
	actions:[
	{
			type:spForms.parameters.updateAction,
			name:"Aprobar",
			id:"Aprobar",
			preSave:function(){
			var CurrentEstado = document.getElementById("EstadoInscripcion");
			if(CurrentEstado.value == "0"){
			CurrentEstado.value = "1";
			}

			
			return true},
			success:function(){return true},
			error:function(){return true}
		},
		{
			type:spForms.parameters.updateAction,
			name:"Rechazar",
			id:"enviar",
			preSave:function(){
			var CurrentEstado = document.getElementById("EstadoInscripcion");
			//alert(CurrentEstado.value);
			if(CurrentEstado.value == "0"){
			CurrentEstado.value = "2";
			}
			return Inscripciones.validateReject();
			return true},
			success:function(){return true},
			error:function(){return true}
		},
		
		{
			type:spForms.parameters.cancelAction,
			name:"Cancelar",
			id:"cancelar",
			callback:""
		},
	],
	description:"Completa la información correspondiente",
	contentTypeName:"Inscripcion",
	containerId:"spForm",
	successLoadEditForm:function(){
		Inscripciones.initialize();
		//pqrsForm.addOnChangeTypeRequest();
	} 
};
spForms.loadEditForm();

var Inscripciones = {
	//create parameters
	parameters:{
	query:"?$select*&$filter=ID eq 3",
	listName: "ParametrosSolicitudes",
	bodyItem:"<div class='contact'><p>Usted no tiene permisos para aprobar este formulario</p></div>",
	TitlePopUp:"Usuario Sin Permisos",


	},	
	fieldType:{
	 "0":{
    	Actualizar:function(){
   		Inscripciones.userPAEForm();
   		var TextObservacionesTH = document.getElementById("ObservacionesRechazoInscripcion");
		TextObservacionesTH.style.pointerEvents = "initial";
		TextObservacionesTH.style.color = "#444";
		


      }
    },

	 "1":{
    	Actualizar:function(){
      	console.log("ingreso a Actualizar")
    	document.getElementById("btn-Aprobar").style.display ="none";
		document.getElementById("btn-enviar").style.display ="none";		
		document.getElementById("btn-cancelar").style.display ="none";
		

      }
    },

  	"2":{
    	Actualizar:function(){
      	console.log("ingreso a Actualizar")
    	document.getElementById("btn-Aprobar").style.display ="none";
		document.getElementById("btn-enviar").style.display ="none";		
		document.getElementById("btn-cancelar").style.display ="none";
		

      }
    },
    },

	initialize:function(){
		try {
		var CurrentEstado = document.getElementById("EstadoInscripcion").value;
		var spFormField = Inscripciones.fieldType[CurrentEstado].Actualizar()

				console.log("ingreso a ver")
		}
		catch (e){
			console.log(e);
		}
	},
	userPAEForm:function(){
		try {
		var UserEmailCurrent = _spPageContextInfo.userEmail
			//adding parameters to query
		var linkQuery = String.format(Inscripciones.parameters.query);
		
			sputilities.getItems(Inscripciones.parameters.listName,linkQuery,function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				for (var i=0; i<resultItems.length; i++){
				var CurrrentItem = resultItems[i];
				var Correo = CurrrentItem.Correo;
				var UserEmailCurrent = _spPageContextInfo.userEmail;
				if(Correo == UserEmailCurrent){
				console.log("usuario con permisos")
				}
				else{
				console.log("usuario sin permisos")
				document.getElementById('spForm').style.display="none";	
				Inscripciones.InitPopUp();
				}
				}
			},
			);


		}
		catch (e){
			console.log(e);
		}
	},
	InitPopUp:function(){
		try {
		
				sputilities.showModalDialog(Inscripciones.parameters.TitlePopUp,null,null,Inscripciones.parameters.bodyItem,function(data){
				console.log("ingresa al pop up")				
			},
			);
		}
		catch (e){
			console.log(e);
		}
	},
	validateReject:function(){
	
		var row = document.querySelector("[sp-static-name='ObservacionesRechazoInscripcion']");
		var comments = row.querySelector("textarea");
		var isValid = true;
		if(comments.value != "")
		{
			row.removeAttribute('sp-invalid');			
		}
		else
		{
			row.setAttribute('sp-invalid',"Los comentarios son obligatorios");			
		}
		return isValid;
	
	},



	
}