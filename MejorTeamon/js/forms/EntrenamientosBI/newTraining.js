﻿var trainingForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		

		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']",
			entrenamientoSolicitado:"div[sp-static-name='Entrenamiento'] > div > div > select",
			Justificacion:"Justificacion",
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			Gerencia:'Gerencia',
			CorreoElectronico:'CorreoElectronico',
			Cargo:"Cargo"		
		},
		resultItems:null,
		invalid:"sp-invalid",
		requiredTag:"<i>*</i>",
		Max:"maxlength",
		MaxNum:"500",
		userProperties:"Department",
		errorInvalid:"El correo electrónico no tiene el formato correcto",
		errorInvalidNotText:"Esto no puede estar en blanco",
		loadUserInfo:"trainingForm.loadUserInfo",
		formsIsLoaded:false,
		formsParameters:{
			title:"Solicitudes de entrenamiento",
			description:"Completa la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Tipo de Contenido TI",
			containerId:"container-form"			
		},
		flowParameters:{
			urlFlow:"https://prod-41.westus.logic.azure.com:443/workflows/1c9d381d7c1748c895fa70560d594fe0/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=explMNDZxpVdkndWe0gM8IXcAexRr33OITp42_VPAqU",
			viewUrl:"",
	    	approveUrl:"",
	   		assingUrl:"",
	   		assingScore:"",
	    	listViewCoordination:"",
	    	listViewReschedule:"",
	    	listNameAssing:"DataStewards",
	    	subjectTitle:"Entreamientos"
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud DataSteward",
			"AsignarSoliciutd":"Asignar Solicitud",
			"AsociarNotaSolicitud":"Asociar Nota Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Solicitudes pendientes en Coordinacion":"Vista en Coordinación",
			"Solicitudes pendientes por Reprogramar":"Vista por Reprogramar"
		},
		global:{
			createdBy:null,
			created:null
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
		
		var objectSelect = document.querySelector(trainingForm.parameters.fieldsHTML.entrenamientoSolicitado);
		var textSelected = objectSelect.options[objectSelect.selectedIndex].text;
							
	    trainingForm.parameters.flowParameters.approveUrl = String.format(trainingForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(trainingForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    trainingForm.parameters.flowParameters.assingUrl = String.format(trainingForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(trainingForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    trainingForm.parameters.flowParameters.assingScore = String.format(trainingForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(trainingForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    trainingForm.parameters.flowParameters.viewUrl = String.format(trainingForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(trainingForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    trainingForm.parameters.flowParameters.listViewCoordination = String.format(trainingForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(trainingForm.parameters.forms)[6]);
	    trainingForm.parameters.flowParameters.listViewReschedule = String.format(trainingForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(trainingForm.parameters.forms)[7]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":trainingForm.parameters.global.createdBy,
		    	"emailApplicant":spForms.loadedFields.CorreoElectronico.value,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"dataSteward": textSelected
		    },
		    "state":spForms.loadedFields.EstadoEntrenamiento.value,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":trainingForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":trainingForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":trainingForm.parameters.flowParameters.listNameAssing,
		    "viewUrl":trainingForm.parameters.flowParameters.viewUrl,
		    "approveUrl":trainingForm.parameters.flowParameters.approveUrl,
		    "assingUrl":trainingForm.parameters.flowParameters.assingUrl,
		    "assingScore":trainingForm.parameters.flowParameters.assingScore,
		    "listViewCoordination":trainingForm.parameters.flowParameters.listViewCoordination,
		    "listViewReschedule":trainingForm.parameters.flowParameters.listViewReschedule,
		    "subjectTitle":	trainingForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:trainingForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity = 0;
				document.querySelector('.form-content').style.opacity = 1;
				document.querySelector('.loader-container').style.height = 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){
		sputilities.callFunctionOnLoadBody(trainingForm.parameters.loadUserInfo);
		spForms.loadNewForm();
		
	},
	loadUserInfo:function(){
		try {
			//running the query with parameters
			sputilities.getMyUserProfileProperties("",function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d;
				trainingForm.parameters.resultItems = resultItems;
				var ctrlInterval = setInterval(function(){
					if(trainingForm.parameters.formsIsLoaded)
					{
						trainingForm.setValuesUserInfo();
						clearInterval(ctrlInterval);
					}
				},500);
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	setValuesUserInfo:function(){
	
		if(trainingForm.parameters.resultItems != null)
		{
		  	var name = document.getElementById(trainingForm.parameters.internalNameFields.NombrePersona);
		  	var email = document.getElementById(trainingForm.parameters.internalNameFields.CorreoElectronico);
		  	var department = document.getElementById(trainingForm.parameters.internalNameFields.Gerencia);
		  	var title = document.getElementById(trainingForm.parameters.internalNameFields.Cargo);
		  	
		  	if(name != null){
		  		name.value = trainingForm.parameters.resultItems.DisplayName ? trainingForm.parameters.resultItems.DisplayName: "";
		  	}
		  	
		  	if(email != null){
		  		email.value = trainingForm.parameters.resultItems.Email ? (trainingForm.parameters.resultItems.Email).toLowerCase(): "";
		  	}
		  	
		  	if(department != null){
		  	
		  		var userProperties = trainingForm.parameters.resultItems.UserProfileProperties.results;
		  	
			  	for(var i = 0; i < userProperties.length; i++)
			  	{
			  		if(userProperties[i].Key == trainingForm.parameters.userProperties)
			  		{
			  		 	department.value =  userProperties[i].Value;
			  		 	break;
			  		}
			  	}
		  	}
			
			if(title != null){
		  		title.value = trainingForm.parameters.resultItems.Title ? trainingForm.parameters.resultItems.Title: "";
		  	}
		  	
		}
	},
	validateFormat:function(){
		var rowEmail = document.querySelector(trainingForm.parameters.fieldsHTML.CorreoElectronico);
		var email = document.getElementById(trainingForm.parameters.internalNameFields.CorreoElectronico);
		var isValid = true;		
		if(email != null && rowEmail!= null)
		{			
			if(trainingForm.validateEmail(email.value) || email.value == "")
			{
				rowEmail.removeAttribute(trainingForm.parameters.invalid);
			}
			else
			{
				rowEmail.setAttribute(trainingForm.parameters.invalid,trainingForm.parameters.errorInvalid);
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;
		
	},
	validateEmail: function (email){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);
	}
}
spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:trainingForm.parameters.formsParameters.buttons.Send.Text,
			id:trainingForm.parameters.formsParameters.buttons.Send.Id,
			success:function (){
				trainingForm.callFlowService();
				return false;

			},
			error:function (){},
			preSave: function (){
				return trainingForm.validateFormat();
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:trainingForm.parameters.formsParameters.buttons.Cancel.Text,
			id:trainingForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:trainingForm.parameters.formsParameters.title,
	description:trainingForm.parameters.formsParameters.description,
	contentTypeName:trainingForm.parameters.formsParameters.contentType,
	containerId:trainingForm.parameters.formsParameters.containerId,
	allowAttachments:false,
  	requiredAttachments:false,
  	maxAttachments:20,
  	minAttachments:0,
  	maxSizeInMB:3,
	successLoadNewForm:function(){
		trainingForm.parameters.global.createdBy = sputilities.contextInfo().userEmail;
		trainingForm.parameters.global.created = moment.utc(new Date()).format(trainingForm.parameters.formats.dateFormat);
		trainingForm.parameters.formsIsLoaded = true;
		document.getElementById(trainingForm.parameters.fieldsHTML.Justificacion).setAttribute(trainingForm.parameters.Max,trainingForm.parameters.MaxNum)
	} 
};
trainingForm.initialize();