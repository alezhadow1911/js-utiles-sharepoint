﻿var trainingForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx",
			"htmlNotAllowed":"<div class='form-group row'><label>{0}</label></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>"
		},
		query:"?$Select=Responsable/EMail,ID&$expand=Responsable",
		queryDS:"?$Select=Responsable/EMail,Entrenamiento/Title&$expand=Entrenamiento,Responsable&$Filter=Entrenamiento/Title eq '{0}'",
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']",
			Comentarios:"[sp-static-name='ComentarioTI']",
			entrenamientoSolicitado:"div[sp-static-name='Entrenamiento'] > div > div > select",
			FechaEntrenamiento:"[sp-static-name='FechaEntrenamiento']",
			LugarCapacitacion:"[sp-static-name='LugarCapacitacion']",
			ResponsableCapacitacion:"[sp-static-name='ResponsableCapacitacion']",
			URLDocumentacion:"[sp-static-name='URLDocumentacion0']",
			notaEntrenamiento:"[sp-static-name='NotaEntrenamiento']"
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico',	
			Comentarios:"ComentariosTI",
			Estado:"EstadoEntrenamiento",
			NotaEntrenamiento:"NotaEntrenamiento",
			FechaAprobacion:"FechaAprobacion",
			VoBoSteward:"VoBoSteward",
			UsuarioAprobador:"UsuarioAprobador",
			FechaReasignacion:"FechaReasignacion",
			FechaEntrenamiento:"FechaEntrenamiento"			
		},		
		controlsHtml:{
			Attachment:"sp-forms-add-attachment",
			Aprobar:"btn-aprobar",
			Corregir:"btn-corregir",
			Aplicar:"btn-aplicar",
			Rechazar:"btn-rechazar",
			classDelete:".delete-doc",
			classGroup:".form-group.row",
			colorDisable:"#B1B1B1",
			borderDisable:"#E1E1E1",
			iconTH:"ico-help-ComentariosTH"
		},
		errorInvalid:{text:"Esto no puede estar en blanco",area:"input", areaComments:"textarea"},
		htmlRequiered:{label:"label",block:"block",none:"none"},		
		invalid:"sp-invalid",
		hidden:"sp-hidden",		
		resultItems:null,	
		codeStates:{ Abierta:"0",Aprobada:"1",EnCoordinacion:"3",Asignado:"4",Reprogramar:"5",Cancelada:"6",Rechazado:"2",Finalizada:"7" },	
		userProperties:"Department",
		bodyItem:"Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior.",
		selectorFormContent:".form-content",
		queryCount:"({0})",
		listName:"AdministradoresBI",
		listNameDS:"DataStewards",
		formsParameters:{
			title:"",
			description:"",
			titleApprove:"Aprobar Solicitud de Entrenamiento",
			titleAssing:"Asignar Solicitud de Entrenamiento",
			titleScore:"Asociar Nota Solicitud de Entrenamiento",
			descriptionApprove:"Completa la información correspondiente, para aprobar o rechazar la solicitud",
			descriptionAssing:"Completa la información correspondiente para asignar la solicitud",
			descriptionScore:"Completa la información correspondiente para asociar la nota del entrenamiento",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Tipo de Contenido TI",
			containerId:"container-form"			
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"trainingForm.loadApprove()",
			"AsignarSolicitud":"trainingForm.loadAssing()",
			"AsociarNotaSolicitud":"trainingForm.loadAssingScrore()",
			"VerSolicitud":"Ver Solicitud",
			"Solicitudes pendientes en Coordinacion":"Vista en Coordinación",
			"Solicitudes pendientes por Reprogramar":"Vista por Reprogramar"
		},
		flowParameters:{
			urlFlow:"https://prod-41.westus.logic.azure.com:443/workflows/1c9d381d7c1748c895fa70560d594fe0/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=explMNDZxpVdkndWe0gM8IXcAexRr33OITp42_VPAqU",
			viewUrl:"",
	    	approveUrl:"",
	   		assingUrl:"",
	   		assingScore:"",
	    	listViewCoordination:"",
	    	listViewReschedule:"",
	    	listNameAssing:"DataStewards",
	    	subjectTitle:"Entreamientos"
		},
		formsState:{
			AprobarSolicitud:"0",
			AsignarSolicitud:"3",
			AsociarNotaSolicitud:"4"
			
		},
		global:{
			formName:null,
			createdBy:null,
			created:null

		},
		initialState:null

	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
		
		var objectSelect = document.querySelector(trainingForm.parameters.fieldsHTML.entrenamientoSolicitado);
		var textSelected = objectSelect.options[objectSelect.selectedIndex].text;
							
	    trainingForm.parameters.flowParameters.approveUrl = String.format(trainingForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(trainingForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    trainingForm.parameters.flowParameters.assingUrl = String.format(trainingForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(trainingForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    trainingForm.parameters.flowParameters.assingScore = String.format(trainingForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(trainingForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    trainingForm.parameters.flowParameters.viewUrl = String.format(trainingForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(trainingForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    trainingForm.parameters.flowParameters.listViewCoordination = String.format(trainingForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(trainingForm.parameters.forms)[6]);
	    trainingForm.parameters.flowParameters.listViewReschedule = String.format(trainingForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(trainingForm.parameters.forms)[7]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":trainingForm.parameters.global.createdBy,
		    	"emailApplicant":spForms.loadedFields.CorreoElectronico.value,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"dataSteward": textSelected,
		    	"nota":Math.round(spForms.loadedFields.NotaEntrenamiento.value)
		    },
		    "state":spForms.loadedFields.EstadoEntrenamiento.value,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":trainingForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":trainingForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":trainingForm.parameters.flowParameters.listNameAssing,
		    "viewUrl":trainingForm.parameters.flowParameters.viewUrl,
		    "approveUrl":trainingForm.parameters.flowParameters.approveUrl,
		    "assingUrl":trainingForm.parameters.flowParameters.assingUrl,
		    "assingScore":trainingForm.parameters.flowParameters.assingScore,
		    "listViewCoordination":trainingForm.parameters.flowParameters.listViewCoordination,
		    "listViewReschedule":trainingForm.parameters.flowParameters.listViewReschedule,
		    "subjectTitle":	trainingForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:trainingForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity = 0;
				document.querySelector('.form-content').style.opacity = 1;
				document.querySelector('.loader-container').style.height = 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){			
		var partsOfUrl = window.location.href.split('?')[0].split('/');
		trainingForm.parameters.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];
		eval(trainingForm.parameters.forms[trainingForm.parameters.global.formName]);

	},
	loadApprove:function(){	
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:trainingForm.parameters.formsParameters.buttons.Aprove.Text,
				id:trainingForm.parameters.formsParameters.buttons.Aprove.Id,
				success:function (){
					trainingForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return trainingForm.validateApprove()
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:trainingForm.parameters.formsParameters.buttons.Reject.Text,
				id:trainingForm.parameters.formsParameters.buttons.Reject.Id,
				success:function (){
					trainingForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return trainingForm.validateReject()
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:trainingForm.parameters.formsParameters.buttons.Cancel.Text,
				id:trainingForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		trainingForm.parameters.formsParameters.description = trainingForm.parameters.formsParameters.descriptionApprove;
		trainingForm.parameters.formsParameters.title = trainingForm.parameters.formsParameters.titleApprove;
		trainingForm.loadEdit(actions,trainingForm.loadResponsableState);
	},
	loadAssing:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:trainingForm.parameters.formsParameters.buttons.Send.Text,
				id:trainingForm.parameters.formsParameters.buttons.Send.Id,
				success:function (){
					trainingForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					if(trainingForm.validateRequired()){
						return trainingForm.validateApprove()
					}
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:trainingForm.parameters.formsParameters.buttons.Cancel.Text,
				id:trainingForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		trainingForm.parameters.formsParameters.description = trainingForm.parameters.formsParameters.descriptionAssing;
		trainingForm.parameters.formsParameters.title = trainingForm.parameters.formsParameters.titleAssing;
        trainingForm.loadEdit(actions,trainingForm.loadResponsableState);
	},
	loadAssingScrore:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:trainingForm.parameters.formsParameters.buttons.Send.Text,
				id:trainingForm.parameters.formsParameters.buttons.Send.Id,
				success:function (){
					trainingForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					if (trainingForm.validateScore()){
						return trainingForm.validateApprove()
					}
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:trainingForm.parameters.formsParameters.buttons.Cancel.Text,
				id:trainingForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		trainingForm.parameters.formsParameters.description = trainingForm.parameters.formsParameters.descriptionScore;
		trainingForm.parameters.formsParameters.title = trainingForm.parameters.formsParameters.titleScore;
        trainingForm.loadEdit(actions,trainingForm.loadResponsableState);

	},
	loadEdit:function(actions,sucessFunction){
		spForms.options={	
			actions:actions,
			title:trainingForm.parameters.formsParameters.title,
			description:trainingForm.parameters.formsParameters.description,
			contentTypeName:trainingForm.parameters.formsParameters.contentType,
			containerId:trainingForm.parameters.formsParameters.containerId,
			allowAttachments:false,
		  	requiredAttachments:false,
		  	maxAttachments:20,
		  	minAttachments:0,
		  	maxSizeInMB:3,
			successLoadEditForm:function(){	
				trainingForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
				trainingForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(trainingForm.parameters.formats.dateFormat);
				if(typeof sucessFunction== "function"){
					sucessFunction();
				}
			} 
		};
		spForms.loadEditForm();
	},
	validateScore: function(){
		var notaRow = document.querySelector(trainingForm.parameters.fieldsHTML.notaEntrenamiento);
		var isValid = true;
		
		if(notaRow != null)
		{
			var nota = notaRow.querySelector(trainingForm.parameters.errorInvalid.area);
			
			if(nota.value != "")
			{
				notaRow.removeAttribute(trainingForm.parameters.invalid);
			}
			else
			{
				isValid = false;
				notaRow.setAttribute(trainingForm.parameters.invalid,trainingForm.parameters.errorInvalid.text);			
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;
	},
	validateRequired:function(){
		var fechaEntrenamientoRow = document.querySelector(trainingForm.parameters.fieldsHTML.FechaEntrenamiento);
		var lugarCapacitacionRow = document.querySelector(trainingForm.parameters.fieldsHTML.LugarCapacitacion);
		var ResponsableCapacitacionRow = document.querySelector(trainingForm.parameters.fieldsHTML.ResponsableCapacitacion);
		var URLDocumentacionRow = document.querySelector(trainingForm.parameters.fieldsHTML.URLDocumentacion);
		var isValid = true;
		
		if(fechaEntrenamientoRow != null && lugarCapacitacionRow != null && ResponsableCapacitacionRow != null && URLDocumentacionRow != null)
		{
		
			var fechaEntrenamiento = fechaEntrenamientoRow.querySelector(trainingForm.parameters.errorInvalid.area);
			var lugarCapacitacion = lugarCapacitacionRow.querySelector(trainingForm.parameters.errorInvalid.area);
			var ResponsableCapacitacion = ResponsableCapacitacionRow.querySelector(trainingForm.parameters.errorInvalid.area);
			var URLDocumentacion = URLDocumentacionRow.querySelector(trainingForm.parameters.errorInvalid.area);
						
			if(fechaEntrenamiento.value != "")
			{
				fechaEntrenamientoRow.removeAttribute(trainingForm.parameters.invalid);
			}
			else
			{
				isValid = false;
				fechaEntrenamientoRow.setAttribute(trainingForm.parameters.invalid,trainingForm.parameters.errorInvalid.text);			
			}
			
			if(lugarCapacitacion.value != "")
			{
				lugarCapacitacionRow.removeAttribute(trainingForm.parameters.invalid);
			}
			else
			{
				isValid = false;
				lugarCapacitacionRow.setAttribute(trainingForm.parameters.invalid,trainingForm.parameters.errorInvalid.text);			
			}
			
			if(ResponsableCapacitacion.value != "")
			{
				ResponsableCapacitacionRow.removeAttribute(trainingForm.parameters.invalid);
			}
			else
			{
				isValid = false;
				ResponsableCapacitacionRow.setAttribute(trainingForm.parameters.invalid,trainingForm.parameters.errorInvalid.text);			
			}
			
			if(URLDocumentacion.value != "")
			{
				URLDocumentacionRow.removeAttribute(trainingForm.parameters.invalid);
			}
			else
			{
				isValid = false;
				URLDocumentacionRow.setAttribute(trainingForm.parameters.invalid,trainingForm.parameters.errorInvalid.text);			
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;
		
	},
	validateApprove:function(){
		var state = document.getElementById(trainingForm.parameters.internalNameFields.Estado);
		var dateApprove = document.getElementById(trainingForm.parameters.internalNameFields.FechaAprobacion);
		var userApprove  = document.getElementById(trainingForm.parameters.internalNameFields.UsuarioAprobador);
		var dateAssing = document.getElementById(trainingForm.parameters.internalNameFields.FechaReasignacion);
		
		
		var isValid = true;
		if(trainingForm.parameters.initialState == trainingForm.parameters.codeStates.Abierta)
		{			
			if(state!= null)
			{
				state.value = trainingForm.parameters.codeStates.Aprobada;
				dateApprove.value = moment.utc(new Date()).format(trainingForm.parameters.formats.dateFormat);
				userApprove.value = sputilities.contextInfo().userId;
			}
			else
			{
				isValid = false;
			}
		}
		if(trainingForm.parameters.initialState == trainingForm.parameters.codeStates.EnCoordinacion)
		{			
			if(state!= null){
				state.value = trainingForm.parameters.codeStates.Asignado;
			}
			else
			{
				isValid = false;
			}
		}
		if(trainingForm.parameters.initialState == trainingForm.parameters.codeStates.Asignado)
		{		
			var nota = document.getElementById(trainingForm.parameters.internalNameFields.NotaEntrenamiento).value	
			if(state!= null)
			{
				if(parseInt(nota)<= 6)
				{
					state.value = trainingForm.parameters.codeStates.Reprogramar;
				}
				else
				{
					state.value = trainingForm.parameters.codeStates.Finalizada;
				}
			}
			else
			{
				isValid = false;
			}
		}
		if(trainingForm.parameters.initialState == trainingForm.parameters.codeStates.Reprogramar)
		{			
			if(state!= null){
				state.value = trainingForm.parameters.codeStates.Asignado;
				dateAssing.value = moment.utc(new Date()).format(trainingForm.parameters.formats.dateFormat);
			}
			else
			{
				isValid = false;
			}
		}	
		return isValid;		
	},
	validateReject:function(){
		var row = document.querySelector(trainingForm.parameters.fieldsHTML.Comentarios);
		var state = document.getElementById(trainingForm.parameters.internalNameFields.Estado);		
		var isValid = true;
		
		if(row != null && state != null)
		{
			var comments = row.querySelector(trainingForm.parameters.errorInvalid.areaComments);
			if(comments.value != "")
			{
				row.removeAttribute(trainingForm.parameters.invalid);
				if(trainingForm.parameters.initialState == trainingForm.parameters.codeStates.Abierta)
				{
					state.value = trainingForm.parameters.codeStates.Rechazado;
				}
				else
				{
					state.value = trainingForm.parameters.codeStates.EnCoordinacion;

				}			
			}
			else
			{
				row.setAttribute(trainingForm.parameters.invalid,trainingForm.parameters.errorInvalid.text);			
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;

	},
	loadResponsableState:function(){	
		try
		{			
		var userCurrent = _spPageContextInfo.userEmail
		var state = document.getElementById(trainingForm.parameters.internalNameFields.Estado);
		$("#"+ trainingForm.parameters.internalNameFields.FechaEntrenamiento).data("DateTimePicker").minDate(new Date());
		
		if(state != null)
		{
				if(state.value == trainingForm.parameters.codeStates.EnCoordinacion || state.value == trainingForm.parameters.codeStates.Asignado || state.value == trainingForm.parameters.codeStates.Reprogramar)
				{
					sputilities.getItems(trainingForm.parameters.listName,trainingForm.parameters.query,function(data){
						
						var resultItems = JSON.parse(data).d.results;
						if(resultItems.length > 0)
						{
							var items = resultItems[0].Responsable.EMail;
							var find = false;
							if(items == userCurrent){
								find = true;
							}
							
							trainingForm.parameters.initialState = state.value;
							
							if(state.value == trainingForm.parameters.codeStates.EnCoordinacion)
							{				
								if(find)
								{
									trainingForm.showComments(state.value);
								}
								else
								{
									trainingForm.showComments(state.value);
									trainingForm.hideButtons(state.value);
									trainingForm.showPopUp();
								}
							}
							else
								if(state.value == trainingForm.parameters.codeStates.Asignado)
								{
									if(find)
									{
										trainingForm.showComments(state.value);
									}
									else
									{
										trainingForm.showComments(state.value);
										trainingForm.hideButtons(state.value);
										trainingForm.showPopUp();
									}

								}
							}						
					});
				}
				else 
				if(state.value == trainingForm.parameters.codeStates.Abierta)
				{
					var objectSelect = document.querySelector(trainingForm.parameters.fieldsHTML.entrenamientoSolicitado);
					var textSelected = objectSelect.options[objectSelect.selectedIndex].text;
					
					var query = String.format(trainingForm.parameters.queryDS,textSelected);
					sputilities.getItems(trainingForm.parameters.listNameDS,query,function(data){
						
						var resultItems = JSON.parse(data).d.results;
						if(resultItems.length > 0)
						{
							var items = resultItems[0].Responsable.EMail;
							var find = false;
							if(items == userCurrent){
								find = true;
							}
							
							trainingForm.parameters.initialState = state.value;
							
							if(state.value == trainingForm.parameters.codeStates.Abierta)
							{				
								if(find)
								{
									trainingForm.showComments(state.value);
								}
								else
								{
									trainingForm.showComments(state.value);
									trainingForm.hideButtons(state.value);
									trainingForm.showPopUp();
								}
							}
						}						
					});
				}
				else
				{
					trainingForm.showComments(state.value);
					trainingForm.hideButtons(state.value);
					trainingForm.showPopUp();
				}
			}
		}
		catch(e){
			console.log(e);
		}	
	},
	hideButtons:function(state){
	
		if(state == trainingForm.parameters.codeStates.Abierta)
		{
			var bottonApp = document.getElementById(trainingForm.parameters.controlsHtml.Enviar);
			if(bottonApp != null)
			{
				bottonApp.style.display = trainingForm.parameters.htmlRequiered.none;
			}
		}
		else if(state == trainingForm.parameters.codeStates.EnCoordinacion)
		{
			var bottonApp = document.getElementById(trainingForm.parameters.controlsHtml.Enviar);
			var comments  = document.getElementById(trainingForm.parameters.internalNameFields.Comentarios);
			var icon = document.getElementById(trainingForm.parameters.controlsHtml.iconTH);
			
			if(bottonApp != null && bottonRej != null && comments != null && icon != null)
			{
				comments.style.pointerEvents = trainingForm.parameters.htmlRequiered.none;
				icon.style.pointerEvents = trainingForm.parameters.htmlRequiered.none;
				comments.style.color = trainingForm.parameters.controlsHtml.colorDisable;
				comments.style.borderColor = trainingForm.parameters.controlsHtml.borderDisable;	
				bottonRej.style.display = trainingForm.parameters.htmlRequiered.none;	
			}
		
		}
		else if(state == trainingForm.parameters.codeStates.Abierto)
		{
			var bottonApp = document.getElementById(trainingForm.parameters.controlsHtml.Enviar);
			var comments  = document.getElementById(trainingForm.parameters.internalNameFields.Comentarios);
			var icon = document.getElementById(trainingForm.parameters.controlsHtml.iconTH);
			
			if(bottonApp != null && bottonRej != null && comments != null && icon != null)
			{
				comments.style.pointerEvents = trainingForm.parameters.htmlRequiered.none;
				icon.style.pointerEvents = trainingForm.parameters.htmlRequiered.none;
				comments.style.color = trainingForm.parameters.controlsHtml.colorDisable;
				comments.style.borderColor = trainingForm.parameters.controlsHtml.borderDisable;	
				bottonRej.style.display = trainingForm.parameters.htmlRequiered.none;	
			}
		
		}
	},
	showPopUp:function(){
		var html = String.format(trainingForm.parameters.formats.htmlNotAllowed,trainingForm.parameters.bodyItem);
		var formContent = document.querySelector(trainingForm.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}	
	},
	showComments:function(state){
		if(state == trainingForm.parameters.codeStates.Abierta)
		{
			var comments = document.querySelector(trainingForm.parameters.fieldsHTML.Comentarios);	
			if(comments != null)
			{
				comments.setAttribute(trainingForm.parameters.hidden,false);
				comments.querySelector(trainingForm.parameters.errorInvalid.areaComments).value="";
	
			}
		}
		else if(state == trainingForm.parameters.codeStates.EnCoordinacion)
		{
			var comments = document.querySelector(trainingForm.parameters.fieldsHTML.Comentarios);	
			if(comments != null)
			{
				comments.setAttribute(trainingForm.parameters.hidden,false);
			}
		}
		
		else if(state == trainingForm.parameters.codeStates.Asignado)
		{
			var comments = document.querySelector(trainingForm.parameters.fieldsHTML.Comentarios);	
			if(comments != null)
			{
				comments.setAttribute(trainingForm.parameters.hidden,false);
			}
		}

	}
}
trainingForm.initialize();