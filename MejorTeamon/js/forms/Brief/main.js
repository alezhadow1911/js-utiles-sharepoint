﻿var ctrlForm = {
    parameters: {
        formats: {
            "queryStaticNameRow": "[sp-static-name='{0}']",
            "htmlNotAllowed": "<div><h4>{0}</h4></div> <div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
            "querySelectRadio": 'input[type="radio"][value="{0}"]:checked',
            "htmlTable": "<div id='header-contacts'><div><h3>Usuario Aprobador</h3></div><div><h3>Estado</h3></div><div><h3>Fecha</h3></div><div><h3>Otros</h3> </div></div>{0}",
            "HtmlUser": "<div class='data-row-contact'> <div><h3>Persona</h3>   </div>   <section>{0}</section>   <div>      <h3>Teléfonos</h3>   </div>   <section>{1}</section>   <div>      <h3>Celular</h3>   </div>   <section>{2}</section>   <div>      <h3>Email</h3>   </div>   <section>{3}</section></div>",
            "HtmlOneDrive": "<p>Haga clic <a href='{0}' target='_blank'>aquí</a> Para abrir el documento de brief</p>",
            "queryRadio": 'input[type="radio"][value="{0}"]',
            "emailUsers": "{0};",
            "emailUsersRevisores": "{0};",
            "dateFormat": "DD-MM-YYYY",
            "dateFormatJson": "dd/MM/yyyy hh:mm tt",
            "formFormat": "{0}//{1}{2}/{3}.aspx?ID={4}&source={5}"
        },
        forms: {
            "NewForm": "ctrlForm.redirectCustomForm('NuevaSolicitud')",
            "EditForm": "ctrlForm.redirectCustomForm('CorregirSolicitud')",
            "DispForm": "ctrlForm.redirectCustomForm('VerSolicitud')",
            "NuevaSolicitud": "ctrlForm.loadNew()",
            "AsignarFecha": "ctrlForm.loadApprove()",
            "CorregirComunicaciones": "ctrlForm.loadEditApplicant()",            
            "Revision": "ctrlForm.loadRevisionArea()",
            "VerSolicitud": "ctrlForm.loadReview()",
            "PendientesAprobar": "",
            "Aprobadas": "",
            "ContenidoBrief": "ctrlForm.loadRevision()",
            "Finalizado": "ctrlForm.loadFinalizado()",
            "CorregirSolicitud": "ctrlForm.loadEditCorregir()",
        },
        messages: {
            "idSaveForm": "enviarSolicitud",
            "idCancelForm": "cancelar",
            "idEditForm": "enviarCorreccion",
            "idApproveForm": "aprobar",
            "idRejectForm": "solicitarCorreccion",
            "idApplyForm": "NuevaReunion",
            "textSaveForm": "Enviar",
            "textCancelForm": "Cancelar",
            "textEditForm": "Enviar",
            "textApproveForm": "Aprobar",
            "textRejectForm": "Corregir",
            "textApplyForm": "Solicitar nueva reunión",
            "textNotAllowed": "Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior",
            "textNotAllowedRevisores": "Usted no tiene permisos sobre este formulario o la solicitud ya fue aprobada por usted"
        },
        columns: {
            "comments": {
                "staticName": "ComentariosRE"
            },
            
            "commentsSolicitante": {
                "staticName": "ComentariosSolicitante"
            },
            
            "Onedrive": {
                "staticName": "UrlOnedrive"
            },

            "displayName": {
                "staticName": "NombrePersona1"
            },
            "idDoc": {
                "staticName": "NombrePersona1"
            },
            "cellPhone": {
                "staticName": "NumeroCelular"
            },
            "email": {
                "staticName": "CorreoElectronico"
            }
        },
        states: [
            "Solicitado",
            "Asignar Reunion",
            "Contenido Brief",
            "Revision Solicitante",
            "Corregir Comunicaciones",
            "Solicitar Reunion",
            "Terminado",            
        ],
        parametersHTML: {
            "spHidden": "sp-hidden",
            "spDisable": "sp-disable",
            div: "div",
            clase: "class",
            form: "form-group row",
            id: "id",
            idCapacitaciones: "Capacitaciones",
            formBody: "form-body form-horizontal",
            formHorizontal:".form-horizontal .form-header",
            formrow:"form-group row",
            formHeader:"form-header",
            textFicha:"Sección Ficha Tecnica",
            h2:"h2",
            formHeaderQuerySelector:".form-header",
        },
        Functions: {
            Onchange: "onchange",
            functionOnchange: "javascript:ctrlForm.clickSelect(this);"
        },

        ID: {
            ContentClose: ".col-md-6.col-xs-6.delete-doc",
            ContentAdd: "a#sp-forms-add-attachment",
            Attachment: "div[sp-static-name='sp-forms-attachment']",
            Comments: "div[sp-static-name='ComentariosRE']",
            CommentsSolicitante: "div[sp-static-name='ComentariosSolicitante']",
            Correo: "CorreoElectronico",
            DiasF:"div[sp-static-name='FechaLanzamiento']",
            TipoComunicacion: "TipoComunicacion",
            Estado: "[sp-static-name='EstadoComunicados']",
            IdComentarios: "ComentariosSolicitante",
            IdComentariosComunicados: "ComentariosRE",
            IdRevisoresaprobados:"Auditoria",

        },
        style: {
            None: "none",
            Opacity: "0.7",
            Block: "block",
            //Atributtes
            Invalid: "sp-invalid",
            Message: "Formato de correo invalido",
            opacity: "0.7",
            MessageDate:"Fecha aprobacion menor a fecha actual",
            MessageDateSolicitud:"Fecha solicitud menor a fecha actual",
            MessageSame:"La fecha no debe ser inferior a dos semanas",
            MessageDateInicial:"Fecha inicio menor a hoy",
            MessageDateFinal:"Fecha final menor a fecha inicio"


        },
        selectorFormContent: ".form-content",
        cellPhone: "CellPhone",
        functionLoadMyProperties: "ctrlForm.loadMyProperties",
        containerId: "container-form",
        description: "Complete la información para la solicitud",
        descriptionEdit: "Corrija la información de la solicitud",
        descriptionApply: "Revise la información y aplique la novedad si corresponde",
        descriptionApprove: "Revise la información y apruebe o envíe a corrección según corresponda",
        descriptionFinalizado: "Finalizacion del comunicado",
        descriptionReview: "Resultado de la solicitud",
        contentTypeName: "Solicitudes BRIEF",
        formIsLoaded: false,
        allowAttachments: true,
        requiredAttachments: false,
        maxAttachments: 20,
        minAttachments: 0,
        maxSizeInMB: 3,
        listNameConsecutive: "ControlRadicados",
        idConsecutive: 2,
        listUsersFlow: "ResponsablesRE",
        listParametros: "ParametrosSEC",
        ListName: sputilities.contextInfo().listTitle,
        viewUrl: "",
        editUrl: "",
        approveUrl: "",
        applyUrl: "",
        UrlBrief: "",
        StateFinish: "",
        UrlFinalizado: "",
        FechaAprobacion: "",
        Cantidad: "",
        RevisionInicial: true,
        Contador: "",
        ObjectPerson: "",
        successQuery: false,
        listViewApply: "",
        listViewApprove: "",
        queryList: "?$filter=ID eq {0}",
        queryUsersComunicaciones: "?$filter=ID eq 3&$select=ResponsablesAbonos/EMail&$expand=ResponsablesAbonos",
        queryUrlOnedrive: "?$filter=ID eq 1&$select=UrlArchivo",
        flowUrl: "https://prod-36.westus.logic.azure.com:443/workflows/a65ec3e3a0674573a0d651d92bcab71d/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=_50VIb8eOzwiFyyGIaYpz82LJmS-N6cDks1HcQEAUq8"

    },
    global: {
        formName: null,
        createdBy: null,
        created: null,
        approveUsers: "",
        applyUsers: "",
        Revisores: "",
        IdUsers: ""

    },
       fieldState: {
        "Solicitado": {
            JsonAuditoriaApproved: function() {
                var nombre = _spPageContextInfo.userDisplayName;
                var IdUser = _spPageContextInfo.userId;
                var today = new Date();
                var Fecha = today.format(ctrlForm.parameters.formats.dateFormatJson);
                document.getElementById(ctrlForm.parameters.ID.IdComentarios).value = "Registro Inicial";
                var Comentarios = document.getElementById(ctrlForm.parameters.ID.IdComentarios).value;
                //save json
                var estado = spForms.loadedFields.EstadoBrief.value;
                var JsonPersonas = {
                    "Personas": []
                }
                JsonPersonas.Personas.push({
                    'Aprobador': '' + nombre + '',
                    'Estado': '' + estado + '',
                    'fecha': '' + Fecha + '',
                    'Comentarios': '' + Comentarios + '',
                    'IdUser': '' + IdUser + ''
                });
                var jsonRevisoresComplete = JSON.stringify(JsonPersonas);
                document.getElementById(ctrlForm.parameters.ID.IdRevisoresaprobados).value = jsonRevisoresComplete;
            }
        },
        "Asignar Reunion": {
            JsonAuditoriaApproved: function() {
                var nombre = _spPageContextInfo.userDisplayName;
                var today = new Date();
                var Fecha = today.format(ctrlForm.parameters.formats.dateFormatJson);
                var IdUser = _spPageContextInfo.userId;
                var Comentarios = document.getElementById(ctrlForm.parameters.ID.IdComentariosComunicados).value;
                var estado = "Asignacion de reunion";
                var ObjectPerson = spForms.loadedFields.Auditoria.value;
                var JsonPersonas = JSON.parse(ObjectPerson)
                JsonPersonas.Personas.push({
                    'Aprobador': '' + nombre + '',
                    'Estado': '' + estado + '',
                    'fecha': '' + Fecha + '',
                    'Comentarios': '' + Comentarios + '',
                    'IdUser': '' + IdUser + ''
                });
                var jsonRevisoresComplete = JSON.stringify(JsonPersonas)
                document.getElementById(ctrlForm.parameters.ID.IdRevisoresaprobados).value = jsonRevisoresComplete
            }
        },
        "Corregir Comunicaciones": {
            JsonAuditoriaApproved: function() {
                var nombre = _spPageContextInfo.userDisplayName;
                var today = new Date();
                var Fecha = today.format(ctrlForm.parameters.formats.dateFormatJson);
                var IdUser = _spPageContextInfo.userId;
                var Comentarios = document.getElementById(ctrlForm.parameters.ID.IdComentariosComunicados).value;
                var estado = "Correcion Realizada";
                var ObjectPerson = spForms.loadedFields.Auditoria.value;
                var JsonPersonas = JSON.parse(ObjectPerson)
                JsonPersonas.Personas.push({
                    'Aprobador': '' + nombre + '',
                    'Estado': '' + estado + '',
                    'fecha': '' + Fecha + '',
                    'Comentarios': '' + Comentarios + '',
                    'IdUser': '' + IdUser + ''
                });
                var jsonRevisoresComplete = JSON.stringify(JsonPersonas)
                document.getElementById(ctrlForm.parameters.ID.IdRevisoresaprobados).value = jsonRevisoresComplete
            }
        },
        "Solicitar Reunion": {
            JsonAuditoriaApproved: function() {
                var nombre = _spPageContextInfo.userDisplayName;
                var today = new Date();
                var Fecha = today.format(ctrlForm.parameters.formats.dateFormatJson);
                var IdUser = _spPageContextInfo.userId;
                var Comentarios = document.getElementById(ctrlForm.parameters.ID.IdComentariosComunicados).value;
                var estado = "Reunion solicitada";
                var ObjectPerson = spForms.loadedFields.Auditoria.value;
                var JsonPersonas = JSON.parse(ObjectPerson)
                JsonPersonas.Personas.push({
                    'Aprobador': '' + nombre + '',
                    'Estado': '' + estado + '',
                    'fecha': '' + Fecha + '',
                    'Comentarios': '' + Comentarios + '',
                    'IdUser': '' + IdUser + ''
                });
                var jsonRevisoresComplete = JSON.stringify(JsonPersonas)
                document.getElementById(ctrlForm.parameters.ID.IdRevisoresaprobados).value = jsonRevisoresComplete
            }
        },
        "Revision Solicitante": {
            JsonAuditoriaApproved: function() {
                var nombre = _spPageContextInfo.userDisplayName;
                var today = new Date();
                var Fecha = today.format(ctrlForm.parameters.formats.dateFormatJson);
                var IdUser = _spPageContextInfo.userId;
                var Comentarios = document.getElementById(ctrlForm.parameters.ID.IdComentariosComunicados).value;
                var estado = "Contenido de brief aprobado";
                var ObjectPerson = spForms.loadedFields.Auditoria.value;
                var JsonPersonas = JSON.parse(ObjectPerson)
                JsonPersonas.Personas.push({
                    'Aprobador': '' + nombre + '',
                    'Estado': '' + estado + '',
                    'fecha': '' + Fecha + '',
                    'Comentarios': '' + Comentarios + '',
                    'IdUser': '' + IdUser + ''
                });
                var jsonRevisoresComplete = JSON.stringify(JsonPersonas)
                document.getElementById(ctrlForm.parameters.ID.IdRevisoresaprobados).value = jsonRevisoresComplete
            }
        },

        "Contenido Brief": {
            JsonAuditoriaApproved: function() {
                var nombre = _spPageContextInfo.userDisplayName;
                var today = new Date();
                var Fecha = today.format(ctrlForm.parameters.formats.dateFormatJson);
                var IdUser = _spPageContextInfo.userId;
                var Comentarios = document.getElementById(ctrlForm.parameters.ID.IdComentariosComunicados).value;
                var estado = "Asignacion Url Brief";
                var ObjectPerson = spForms.loadedFields.Auditoria.value;
                var JsonPersonas = JSON.parse(ObjectPerson)
                JsonPersonas.Personas.push({
                    'Aprobador': '' + nombre + '',
                    'Estado': '' + estado + '',
                    'fecha': '' + Fecha + '',
                    'Comentarios': '' + Comentarios + '',
                    'IdUser': '' + IdUser + ''
                });
                var jsonRevisoresComplete = JSON.stringify(JsonPersonas)
                document.getElementById(ctrlForm.parameters.ID.IdRevisoresaprobados).value = jsonRevisoresComplete
            }
        },
    },

    fieldStateReject: {
         "Revision Solicitante": {
            JsonAuditoriaReject: function() {
                var nombre = _spPageContextInfo.userDisplayName;
                var today = new Date();
                var Fecha = today.format(ctrlForm.parameters.formats.dateFormatJson);
                var IdUser = _spPageContextInfo.userId;
                var Comentarios = document.getElementById(ctrlForm.parameters.ID.IdComentarios).value;
                var estado = "Rechazo Solicitante";
                var ObjectPerson = spForms.loadedFields.Auditoria.value;
                var JsonPersonas = JSON.parse(ObjectPerson)
                JsonPersonas.Personas.push({
                    'Aprobador': '' + nombre + '',
                    'Estado': '' + estado + '',
                    'fecha': '' + Fecha + '',
                    'Comentarios': '' + Comentarios + '',
                    'IdUser': '' + IdUser + ''
                });
                var jsonRevisoresComplete = JSON.stringify(JsonPersonas)
                document.getElementById(ctrlForm.parameters.ID.IdRevisoresaprobados).value = jsonRevisoresComplete
            }
        },
     },
    initialize: function() {
        var partsOfUrl = window.location.href.split('?')[0].split('/');
        ctrlForm.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];
        eval(ctrlForm.parameters.forms[ctrlForm.global.formName]);
    },
    callFlowService: function() {
        document.getElementById('s4-workspace').scrollTop = 0;
        document.querySelector('.form-content').style.opacity = 0;
        document.querySelector('.loader-container').style.opacity = 1;
        document.querySelector('.loader-container').style.height = "auto";


        ctrlForm.parameters.viewUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[7], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.editUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[5], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.approveUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[4], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.applyUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[6], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.UrlBrief = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[10], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.UrlFinalizado = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[11], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.listViewApply = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[9]);
        ctrlForm.parameters.listViewApprove = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[8]);

        var objService = {
            "currentItemID": spForms.currentItemID,
            "currentItem": {
                "createdBy": ctrlForm.global.createdBy,
                "nombres":spForms.loadedFields.NombrePersona1.value,
                "correo": spForms.loadedFields.CorreoElectronico.value,
                "campana": spForms.loadedFields.NombreCampana.value,
                "consecutivo": spForms.loadedFields.NumeroRadicado.value,
                "area": spForms.loadedFields.Area.value,
                "comentariosSolicitante": spForms.loadedFields.ComentariosSolicitante.value,
                "comentariosRE": spForms.loadedFields.ComentariosRE.value,
                "fechaReunion": spForms.loadedFields.FechaContextualizacion.value,                                
                "fechaCreacion": ctrlForm.global.created,
            },
            "state": spForms.loadedFields.EstadoBrief.value,
            "titleCorreo": spForms.listInfo.title,
            "approveUsers": ctrlForm.global.approveUsers,
            "applyUsers": ctrlForm.global.applyUsers,
            "applyUsersRevisores": ctrlForm.global.Revisores,
            "siteUrl": sputilities.contextInfo().webAbsoluteUrl,
            "listName": sputilities.contextInfo().listTitle,
            "listNameConsecutive": ctrlForm.parameters.listNameConsecutive,
            "idConsecutive": ctrlForm.parameters.idConsecutive,
            "entityType": spForms.listInfo.entityItem,
            "viewUrl": ctrlForm.parameters.viewUrl,
            "editUrl": ctrlForm.parameters.editUrl,
            "approveUrl": ctrlForm.parameters.approveUrl,
            "applyUrl": ctrlForm.parameters.applyUrl,
            "UrlBrief": ctrlForm.parameters.UrlBrief,
            "UrlFinalizado": ctrlForm.parameters.UrlFinalizado,
            "FechaAprobacion": ctrlForm.parameters.FechaAprobacion,
            "Cantidad": ctrlForm.parameters.Cantidad,
            "EmailInicialRevision": ctrlForm.parameters.RevisionInicial,
            "Contador": ctrlForm.parameters.Contador,
            "JsonPerson": ctrlForm.parameters.ObjectPerson,
            "StateFinish": ctrlForm.parameters.StateFinish,
            "listViewApply": ctrlForm.parameters.listViewApply,
            "listViewApprove": ctrlForm.parameters.listViewApprove
        }
        utilities.http.callRestService({
            url: ctrlForm.parameters.flowUrl,
            method: utilities.http.methods.POST,
            data: JSON.stringify(objService),
            headers: {
                "accept": utilities.http.types.jsonOData,
                "content-type": utilities.http.types.jsonOData
            },
            success: function(data) {
                document.querySelector('.loader-container').style.opacity = 0;
                document.querySelector('.form-content').style.opacity = 1;
                document.querySelector('.loader-container').style.height = 0;

                spForms.cancelForm(null);
            },
            error: function(xhr) {
                console.log(xhr);
            }
        });

    },
    getUsersComunicaciones: function() {
        sputilities.getItems(ctrlForm.parameters.listUsersFlow, ctrlForm.parameters.queryUsersComunicaciones,
            function(data) {
                var data = utilities.convertToJSON(data);
                if (data != null) {
                    var items = data.d.results;
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        for (var j = 0; j < item.ResponsablesAbonos.results.length; j++) {
                            var user = item.ResponsablesAbonos.results[j];
                            ctrlForm.global.approveUsers += String.format(ctrlForm.parameters.formats.emailUsers, user.EMail);
                        }
                    }
                }
            },
            function(xhr) {
                console.log(xhr);
            }
        );
    },
    getUrlOneDrieve: function() {
      var item = spForms.loadedFields.UrlBrief.value;
      var doc  = document.querySelector('.name-doc')
      var container =document.querySelector('#sp-forms-attachment-container');
      if(container.childElementCount != 0){
     	 var linebreak = document.createElement("br");
	        var link = document.createElement('a');
			var linkText = document.createTextNode("Url Brief");
			link.appendChild(linkText);
			link.href = item;
			doc.appendChild(linebreak);
			doc.appendChild(link);			
      }else{
       var itemPrincipal = document.createElement('div');
      itemPrincipal.className = 'item-doc';
      var col = document.createElement('div');
      col.className = 'col-md-6 col-xs-12 name-doc';
      itemPrincipal.appendChild(col)
      var link = document.createElement('a');
		var linkText = document.createTextNode("Url Brief");
			link.appendChild(linkText);
			link.href = item;
			col.appendChild(link);	
			container.appendChild(itemPrincipal);	
					

      }
    },
        changeState: function(state) {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio, state));
        if (radio != null) {
            radio.click();
        }
    },
    hideNotAllowed: function() {
        var html = String.format(ctrlForm.parameters.formats.htmlNotAllowed, ctrlForm.parameters.messages.textNotAllowed);
        var formContent = document.querySelector(ctrlForm.parameters.selectorFormContent);
        if (formContent != null) {
            formContent.innerHTML = html;
        }
    },
	validateUserCreated:function(){
	  var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[3])) == null;
		if(spForms.currentItem.AuthorId != sputilities.contextInfo().userId || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
		ctrlForm.hideAttachments();
	},
    validateAllUsers: function() {
        if (spForms.currentItem.AuthorId != sputilities.contextInfo().userId) {
            ctrlForm.hideNotAllowed();
            return false;
        }
    },
    validateEditUser: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[2])) == null;
        if (spForms.currentItem.AuthorId != sputilities.contextInfo().userId || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        return true;
    },
    //Asignar reunion
        validateUserDate: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[1])) == null;
        if (ctrlForm.global.approveUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        ctrlForm.hideAttachments();
    },

    //Correcion comunicaciones
    validateUserComunicaciones: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[4])) == null;
        if (ctrlForm.global.approveUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        ctrlForm.hideAttachments();
    },
    //Notificacion para revision
    validateUserRevision: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[2])) == null;
       		if (ctrlForm.global.approveUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
         ctrlForm.hideAttachments();
    },
    hideAttachments: function() {
        var i;
        var CloaseAttachments = document.querySelectorAll(ctrlForm.parameters.ID.ContentClose);
        for (i = 0; i < CloaseAttachments.length; i++) {
            CloaseAttachments[i].style.display = ctrlForm.parameters.style.None;
        }
        document.querySelector(ctrlForm.parameters.ID.ContentAdd).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.Attachment).style.opacity = ctrlForm.parameters.style.Opacity;
    },

    hideComments: function() {
        var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow, ctrlForm.parameters.columns.comments.staticName));
        if (ctrlComments != null) {
            ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden, true);
        }
    },
    blockedComments: function() {
        var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow, ctrlForm.parameters.columns.comments.staticName));
        if (ctrlComments != null) {
            ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden, true);
        }
    },

    loadReview: function() {
        var actions = [{
            type: spForms.parameters.cancelAction,
            name: ctrlForm.parameters.messages.textCancelForm,
            id: ctrlForm.parameters.messages.idCancelForm,
            callback: null
        }, ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionReview;
        ctrlForm.loadEdit(actions, null);
    },
    loadApprove: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textSaveForm,
                id: ctrlForm.parameters.messages.idApproveForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    //ctrlForm.UnlockedComments();
                    ctrlForm.changeState(ctrlForm.parameters.states[2]);
                    spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
                    ctrlForm.JsonRevisoresApprove();
                    return true;
                }
            },
              {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
        ctrlForm.loadEdit(actions, ctrlForm.validateUserDate);

    },
    loadRevision: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textEditForm,
                id: ctrlForm.parameters.messages.idApproveForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;

                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.changeState(ctrlForm.parameters.states[3]);                    
                    ctrlForm.JsonRevisoresApprove();
                    return true;
                }
            },
            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
        ctrlForm.loadEdit(actions, ctrlForm.validateUserRevision);

    },
    loadRevisionArea: function() {
        var actions = [
        {
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textApproveForm,
                id: ctrlForm.parameters.messages.idApproveForm,
                success: function() {
					      ctrlForm.callFlowService();              
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.changeState(ctrlForm.parameters.states[6]);  
                    ctrlForm.parameters.StateFinish = ctrlForm.parameters.states[3];
                    ctrlForm.JsonRevisoresApprove(ctrlForm.parameters.StateFinish);
                    return true;
                }
            },
              {
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textRejectForm,
                id: ctrlForm.parameters.messages.idRejectForm,
                success: function() {
					       ctrlForm.callFlowService();        
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                ctrlForm.correcionSolicitante();
                    return true;
                }
            },
             {
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textApplyForm,
                id: ctrlForm.parameters.messages.idApplyForm,
                success: function() {
					ctrlForm.callFlowService();           
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
               		ctrlForm.changeState(ctrlForm.parameters.states[6]);  
					ctrlForm.parameters.StateFinish = ctrlForm.parameters.states[5];
					ctrlForm.JsonRevisoresApprove(ctrlForm.parameters.StateFinish);
                    return true;
                }
            },  
  
            ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
        ctrlForm.loadEdit(actions, ctrlForm.validateUserCreated);

    },

    loadFinalizado: function() {
        var actions = [{
            type: spForms.parameters.cancelAction,
            name: ctrlForm.parameters.messages.textCancelForm,
            id: ctrlForm.parameters.messages.idCancelForm,
            callback: null
        }, ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionFinalizado;
        ctrlForm.loadEdit(actions, null);

    },


    loadEditApplicant: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textEditForm,
                id: ctrlForm.parameters.messages.idEditForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.correcionComunicaciones();
                    return true;
                }
            },
            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionEdit;
        ctrlForm.loadEdit(actions, ctrlForm.validateUserComunicaciones);
        
    },
    loadEditCorregir: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textEditForm,
                id: ctrlForm.parameters.messages.idEditForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    //ctrlForm.correcionComunicaciones();
                    return true;
                }
            },
            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionEdit;
        ctrlForm.loadEdit(actions, ctrlForm.validateAllUsers);
        
    },

    loadEdit: function(actions, callBack) {
        ctrlForm.getUsersComunicaciones();
        spForms.options = {
            actions: actions,
            description: ctrlForm.parameters.description,
            contentTypeName: ctrlForm.parameters.contentTypeName,
            containerId: ctrlForm.parameters.containerId,
            allowAttachments: ctrlForm.parameters.allowAttachments,
            requiredAttachments: ctrlForm.parameters.requiredAttachments,
            maxAttachments: ctrlForm.parameters.maxAttachments,
            minAttachments: ctrlForm.parameters.minAttachments,
            maxSizeInMB: ctrlForm.parameters.maxSizeInMB,
            successLoadEditForm: function() {
                ctrlForm.global.createdBy = spForms.currentItem.Author.EMail;
                ctrlForm.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(ctrlForm.parameters.formats.dateFormat);
                ctrlForm.formIsLoaded = true;
                if (ctrlForm.global.formName == "CorregirComunicaciones") {
                ctrlForm.commentsEmpty();
                }
                if (ctrlForm.global.formName == "VerSolicitud") {
                    ctrlForm.commentsEmpty();
                    ctrlForm.Auditoria();
                    ctrlForm.validateUserCreated();
                }
                if (ctrlForm.global.formName == "Revision") {
                    ctrlForm.getUrlOneDrieve(); 
                    ctrlForm.commentsEmpty();    
				    ctrlForm.RevisionCorrecion();
           }
                
                if (typeof callBack == "function") {
                    callBack();
                }
            }
        };
        spForms.loadEditForm();

    },
    loadNew: function() {
        ctrlForm.getUsersComunicaciones();
        ctrlForm.global.createdBy = sputilities.contextInfo().userEmail;
        ctrlForm.global.created = moment.utc(new Date()).format(ctrlForm.parameters.formats.dateFormat);
        sputilities.callFunctionOnLoadBody(ctrlForm.parameters.functionLoadMyProperties);
        ctrlForm.loadNewForm();
    },
    loadNewForm: function() {
        spForms.options = {
            actions: [{
                    type: spForms.parameters.saveAction,
                    name: ctrlForm.parameters.messages.textSaveForm,
                    id: ctrlForm.parameters.messages.idSaveForm,
                    success: function() {
                        ctrlForm.callFlowService();

                        return false;
                    },
                    error: function() {
                        return true;
                    },
                    preSave: function() {
                        ctrlForm.JsonRevisoresApprove();
                        ctrlForm.changeState(ctrlForm.parameters.states[0]);
                        return true;
                    }
                },
                {
                    type: spForms.parameters.cancelAction,
                    name: ctrlForm.parameters.messages.textCancelForm,
                    id: ctrlForm.parameters.messages.idCancelForm,
                    callback: null
                },
            ],
            description: ctrlForm.parameters.description,
            contentTypeName: ctrlForm.parameters.contentTypeName,
            containerId: ctrlForm.parameters.containerId,
            allowAttachments: ctrlForm.parameters.allowAttachments,
            requiredAttachments: ctrlForm.parameters.requiredAttachments,
            maxAttachments: ctrlForm.parameters.maxAttachments,
            minAttachments: ctrlForm.parameters.minAttachments,
            maxSizeInMB: ctrlForm.parameters.maxSizeInMB,
            successLoadNewForm: function() {
                ctrlForm.formIsLoaded = true;
                ctrlForm.hideComments();
                ctrlForm.stateChange();
                ctrlForm.validateEmailForm();
				ctrlForm.validateDateInicial();
            }
        };
        spForms.loadNewForm();
    },
    loadMyProperties: function() {
        sputilities.getUserProfileProperties(sputilities.contextInfo().userEmail,
            function(userInfo) {
                var objUser = utilities.convertToJSON(userInfo);
                if (objUser != null) {
                    var displayName = objUser.d.DisplayName;
                    var validateFormIsLoaded = setInterval(function() {
                        if (ctrlForm.formIsLoaded) {
                            ctrlForm.setValueToControl(ctrlForm.parameters.columns.displayName.staticName, displayName);
                            clearInterval(validateFormIsLoaded);
                        }
                    }, 500);
                }
            },
            function(xhr) {
                console.log(xhr)
            }
        );
    },
    	setValueToControl:function(staticName,value){
		if(value != null){
			var ctrl = document.getElementById(staticName);
			if(ctrl != null){
				ctrl.value = value;
			}
		}
	},

    Auditoria: function() {
        var html = "";
        var htmla = "";
        var i = 0;
        var uno = spForms.loadedFields.Auditoria.value;
        var Json = utilities.convertToJSON(uno);
        var DivCapacitaciones = document.createElement(ctrlForm.parameters.parametersHTML.div);
        DivCapacitaciones.setAttribute(ctrlForm.parameters.parametersHTML.clase, ctrlForm.parameters.parametersHTML.form);
        DivCapacitaciones.setAttribute(ctrlForm.parameters.parametersHTML.id, ctrlForm.parameters.parametersHTML.idCapacitaciones);
        var uno = document.getElementsByClassName(ctrlForm.parameters.parametersHTML.formBody);
        uno[0].insertBefore(DivCapacitaciones, uno[0].childNodes[55]);
        for (i; i < Json.Personas.length; i++) {
            var Nombre = Json.Personas[i].Aprobador;
            var Estado = Json.Personas[i].Estado;
            var Fecha = Json.Personas[i].fecha;
            var Comentarios = Json.Personas[i].Comentarios;
            html += String.format(ctrlForm.parameters.formats.HtmlUser, Nombre, Estado, Fecha, Comentarios);
        }
        htmla = String.format(ctrlForm.parameters.formats.htmlTable, html);
        document.getElementById(ctrlForm.parameters.parametersHTML.idCapacitaciones).innerHTML = htmla;

    },
    UnlockedComments: function() {
        spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = false;
        var uno = document.querySelector(ctrlForm.parameters.ID.Comments);
        uno.removeAttribute(ctrlForm.parameters.style.Invalid);
        spForms.loadedFields[ctrlForm.parameters.columns.commentsSolicitante.staticName].required = false;
        var dos = document.querySelector(ctrlForm.parameters.ID.CommentsSolicitante);
        dos.removeAttribute(ctrlForm.parameters.style.Invalid);

    },
    JsonRevisoresApprove: function(StateFinish) {
        var CurrentState = spForms.loadedFields.EstadoBrief.value;
        if(CurrentState == "Revision Solicitante"){
        var state = StateFinish;
        var spFormField = ctrlForm.fieldState[state].JsonAuditoriaApproved();        
        }else{
        var spFormField = ctrlForm.fieldState[CurrentState].JsonAuditoriaApproved();
        }
        
    },
    JsonRevisoresReject: function() {
        var CurrentState = spForms.loadedFields.EstadoBrief.value;
        var spFormField = ctrlForm.fieldStateReject[CurrentState].JsonAuditoriaReject();
    },
    stateChange: function() {
        spForms.loadedFields.EstadoBrief.value = ctrlForm.parameters.states[0];
    },
    commentsEmpty: function() {
        document.getElementById(ctrlForm.parameters.ID.IdComentarios).value = "";
        document.getElementById(ctrlForm.parameters.ID.IdComentariosComunicados).value = "";
    },
    correcionSolicitante: function() {
        if(document.getElementById(ctrlForm.parameters.ID.IdComentarios).value == ""){
        spForms.loadedFields[ctrlForm.parameters.columns.commentsSolicitante.staticName].required = true;
        }else{
				ctrlForm.UnlockedComments();
                document.getElementById('ContadorCorregir').value = 1
                ctrlForm.JsonRevisoresReject();
                ctrlForm.changeState(ctrlForm.parameters.states[4]); 
                
        }
    },
    correcionComunicaciones: function() {
        if(document.getElementById(ctrlForm.parameters.ID.IdComentariosComunicados).value == ""){
        spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
        }else{
				ctrlForm.UnlockedComments();
                ctrlForm.JsonRevisoresApprove();
                ctrlForm.changeState(ctrlForm.parameters.states[3]); 
        }
    },
    RevisionCorrecion: function() {
        if(document.getElementById('ContadorCorregir').value >= 1){
		 document.getElementById('btn-solicitarCorreccion').style.display = "none"
        }
 
    },
    	validateDateInicial:function(){
    	var now = moment().add(14,'days').format('MM/DD/YYYY');
    	var Final = document.getElementById('FechaLanzamiento');
		if(Final != null){
				Final.addEventListener('blur',function(event){
				var FinalEvent = moment(this.value,'DD/MM/YYYY').format('MM/DD/YYYY');
				var finalDate = document.querySelector(ctrlForm.parameters.ID.DiasF)
				if(FinalEvent < now){
				finalDate.setAttribute(ctrlForm.parameters.style.Invalid,ctrlForm.parameters.style.MessageSame)	
				}else{
				finalDate.removeAttribute(ctrlForm.parameters.style.Invalid)
				}
  					});
					}
			},
	validateEmailForm:function(){
		var input = document.getElementById(ctrlForm.parameters.ID.Correo);
			if(input != null){
				input.addEventListener('change',function(event){
				var email = this.value;
		  			if (ctrlForm.validateEmail(email)) {
		  				var staticName = "[sp-static-name="+ctrlForm.parameters.ID.Correo+"]";
		  				var InputSelect = document.querySelector(staticName);
   							InputSelect.removeAttribute(ctrlForm.parameters.style.Invalid)
   			  				} 	
   			  		else{
		  				var staticName = "[sp-static-name="+ctrlForm.parameters.ID.Correo+"]";
		  				var InputSelect = document.querySelector(staticName);
   						InputSelect.setAttribute(ctrlForm.parameters.style.Invalid,ctrlForm.parameters.style.Message)
   			  			}
  				return false;
  					});
					}
	},
	validateEmail:function(email){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 			return re.test(email);
 	 	},

    redirectCustomForm: function(name) {
        window.location.href = window.location.href.replace(ctrlForm.global.formName, name);
    }

}
ctrlForm.initialize();