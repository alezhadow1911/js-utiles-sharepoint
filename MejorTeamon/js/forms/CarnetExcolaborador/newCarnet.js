﻿var carnetForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		fields:[{field:"NombrePersona1"},{field:"NumeroCelular"},{field:"CorreoElectronico"}],
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']"
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico'			
		},
		resultItems:null,
		invalid:"sp-invalid",
		userProperties:"CellPhone", 
		errorInvalid:"El correo electrónico no tiene el formato correcto",
		loadUserInfo:"carnetForm.loadUserInfo",
		formsIsLoaded:false,
		formsParameters:{
			title:"Solicitud Carnet ExColaborador (Jubilados)",
			description:"Completa la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"}
			},
			contentType:"Carnet ExColaborador",
			containerId:"container-form"			
		},
		flowParameters:{
			urlFlow:"https://prod-46.westus.logic.azure.com:443/workflows/e8aa1f118e9143949ae64d1e580e4b6a/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=0-YSE8iAu2SJ7TnbRZoJWN2mljJu75VMypD2_PbDu1s",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",
	   		approveBossUrl:"",
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Carnet ExColaborador Ventanilla TH",
	    	applyUsers:"Carnet ExColaborador Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:8,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Carnet Ex Colaborador"
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud Ventanilla TH",			
			"AplicarSolicitud":"Aplicar Novedad",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Novedades":"Vista Novedades",
			"Ventanilla":"Vista Ventanilla"
		},
		global:{
			createdBy:null,
			created:null
		}

	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
	    carnetForm.parameters.flowParameters.approveUrl = String.format(carnetForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(carnetForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);			    
	    carnetForm.parameters.flowParameters.applyUrl = String.format(carnetForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(carnetForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    carnetForm.parameters.flowParameters.editUrl = String.format(carnetForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(carnetForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    carnetForm.parameters.flowParameters.listViewApply = String.format(carnetForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(carnetForm.parameters.forms)[5]);
	    carnetForm.parameters.flowParameters.listViewApprove= String.format(carnetForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(carnetForm.parameters.forms)[6]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":carnetForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":carnetForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosTH.value		       
		    },
		    "state":spForms.loadedFields.EstadoCarnetExcolaborador.value,
		    "approveUsers":carnetForm.parameters.flowParameters.approveUsers,
		    "applyUsers":carnetForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":carnetForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":carnetForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":carnetForm.parameters.flowParameters.listNameAssing,
		    "editUrl":carnetForm.parameters.flowParameters.editUrl,
		    "approveUrl":carnetForm.parameters.flowParameters.approveUrl,
		    "applyUrl":carnetForm.parameters.flowParameters.applyUrl,		    
		    "listViewApply":carnetForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":carnetForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	carnetForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:carnetForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},	
	initialize:function(){
		sputilities.callFunctionOnLoadBody(carnetForm.parameters.loadUserInfo);
		spForms.loadNewForm();
	},
	loadUserInfo:function(){
		try {
			//running the query with parameters
			sputilities.getMyUserProfileProperties("",function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d;
				carnetForm.parameters.resultItems = resultItems;
				var ctrlInterval = setInterval(function(){
					if(carnetForm.parameters.formsIsLoaded)
					{
						carnetForm.setValuesUserInfo();
						clearInterval(ctrlInterval);
					}
				},500);
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	setValuesUserInfo:function(){
	
		if(carnetForm.parameters.resultItems != null)
		{
		  	var name = document.getElementById(carnetForm.parameters.internalNameFields.NombrePersona);
		  	var email = document.getElementById(carnetForm.parameters.internalNameFields.CorreoElectronico);
		  	var cellPhone = document.getElementById(carnetForm.parameters.internalNameFields.NumeroCelular);
		  	if(name != null){
		  		name.value = carnetForm.parameters.resultItems.DisplayName ? carnetForm.parameters.resultItems.DisplayName: "";
		  	}
		  	
		  	if(email != null){
		  		email.value = carnetForm.parameters.resultItems.Email ? (carnetForm.parameters.resultItems.Email).toLowerCase(): "";
		  	}
		  	
		  	if(cellPhone != null){
		  	
		  		var userProperties = carnetForm.parameters.resultItems.UserProfileProperties.results;
		  	
			  	for(var i=0;i<userProperties.length;i++)
			  	{
			  		if(userProperties[i].Key == carnetForm.parameters.userProperties)
			  		{
			  		 	cellPhone.value =  userProperties[i].Value;
			  		 	break;
			  		}
			  	}
			}
	  	}
	},
	validateFormat:function(){
		var rowEmail = document.querySelector(carnetForm.parameters.fieldsHTML.CorreoElectronico);
		var email = document.getElementById(carnetForm.parameters.internalNameFields.CorreoElectronico);
		var isValid = true;		
		if(email != null && rowEmail!= null)
		{			
			if(carnetForm.validateEmail(email.value) || email.value == "")
			{
				rowEmail.removeAttribute(carnetForm.parameters.invalid);
			}
			else
			{
				rowEmail.setAttribute(carnetForm.parameters.invalid,carnetForm.parameters.errorInvalid);
			}
		}
		else
		{
			isValid=false;
		}
		return isValid;
		
	},
	validateEmail: function (email){
		 var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);
	
	}

}
spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:carnetForm.parameters.formsParameters.buttons.Send.Text,
			id:carnetForm.parameters.formsParameters.buttons.Send.Id,
			success:function (){
				carnetForm.callFlowService();
				return false;
			},
			error:function (){},
			preSave: function (){
				return carnetForm.validateFormat()
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:carnetForm.parameters.formsParameters.buttons.Cancel.Text,
			id:carnetForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:carnetForm.parameters.formsParameters.title,
	description:carnetForm.parameters.formsParameters.description,
	contentTypeName:carnetForm.parameters.formsParameters.contentType,
	containerId:carnetForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:false,
  	maxAttachments:10,
  	minAttachments:0,
  	maxSizeInMB:5,
	successLoadNewForm:function(){
		carnetForm.parameters.global.createdBy = sputilities.contextInfo().userEmail;
		carnetForm.parameters.global.created = moment.utc(new Date()).format(carnetForm.parameters.formats.dateFormat);
		carnetForm.parameters.formsIsLoaded = true;
	} 
};
carnetForm.initialize();