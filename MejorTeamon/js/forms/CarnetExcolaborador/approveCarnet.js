﻿var carnetForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		query:"?$Select=FlujoAsociado/Title,ResponsablesAbonos/EMail,ID&$expand=FlujoAsociado,ResponsablesAbonos&$filter=Title eq 'Carnet ExColaborador Ventanilla TH'",
		listName:"ResponsablesTH",
		fieldsHtml:{
			Comentarios:"[sp-static-name='ComentariosTH']"				
		},		
		controlsHtml:{
			Attachment:"sp-forms-add-attachment",
			Aprobar:"btn-aprobar",
			Corregir:"btn-corregir",
			classDelete:".delete-doc",
			classGroup:".form-group.row",
			colorDisable:"#B1B1B1",
			borderDisable:"#E1E1E1",
			iconAttachment:"ico-help-ComentariosTH"
		},
		internalNameFields:{
			Comentarios:"ComentariosTH",
			Estado:"EstadoCarnetExcolaborador"
		},	
		errorInvalid:{text:"Los comentarios son obligatorios",area:"textarea"},
		htmlRequiered:{position:"beforeend",html:"<i>*</i>",label:"label",block:"block",none:"none"},		
		invalid:"sp-invalid",
		hidden:"sp-hidden",		
		resultItems:null,	
		codeStates:{Solicitado:"0",Ventanilla:"1",Novedades:"2",Aplicada:"3",Corregir:"4"},	
		userProperties:"CellPhone",
		bodyItem:"<div class='form-group row'><label>Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior</label></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
		containerForm:".form-content",
		TitlePopUp:"Carnet ExColaborador(Jubilados) - Acceso Denegado",
		formsParameters:{
			title:"Aprobar/ Corregir Solicitud Carnet ExColaborador (Jubilados)",
			description:"Completa la información correspondiente, para aprobar o rechazar la solicitud",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"}
				},
			contentType:"Carnet ExColaborador",
			containerId:"container-form"			
		},
		flowParameters:{
			urlFlow:"https://prod-46.westus.logic.azure.com:443/workflows/e8aa1f118e9143949ae64d1e580e4b6a/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=0-YSE8iAu2SJ7TnbRZoJWN2mljJu75VMypD2_PbDu1s",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",
	   		approveBossUrl:"",
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Carnet ExColaborador Ventanilla TH",
	    	applyUsers:"Carnet ExColaborador Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:8,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Carnet Ex Colaborador"
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud Ventanilla TH",			
			"AplicarSolicitud":"Aplicar Novedad",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Novedades":"Vista Novedades",
			"Ventanilla":"Vista Ventanilla"
		},
		global:{
			createdBy:null,
			created:null
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
	    carnetForm.parameters.flowParameters.approveUrl = String.format(carnetForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(carnetForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);			    
	    carnetForm.parameters.flowParameters.applyUrl = String.format(carnetForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(carnetForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    carnetForm.parameters.flowParameters.editUrl = String.format(carnetForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(carnetForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    carnetForm.parameters.flowParameters.listViewApply = String.format(carnetForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(carnetForm.parameters.forms)[5]);
	    carnetForm.parameters.flowParameters.listViewApprove= String.format(carnetForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(carnetForm.parameters.forms)[6]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":carnetForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":carnetForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosTH.value		       
		    },
		    "state":spForms.loadedFields.EstadoCarnetExcolaborador.value,
		    "approveUsers":carnetForm.parameters.flowParameters.approveUsers,
		    "applyUsers":carnetForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":carnetForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":carnetForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":carnetForm.parameters.flowParameters.listNameAssing,
		    "editUrl":carnetForm.parameters.flowParameters.editUrl,
		    "approveUrl":carnetForm.parameters.flowParameters.approveUrl,
		    "applyUrl":carnetForm.parameters.flowParameters.applyUrl,		    
		    "listViewApply":carnetForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":carnetForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	carnetForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:carnetForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	validateReject:function(){
	
		var row = document.querySelector(carnetForm.parameters.fieldsHtml.Comentarios);
		var state = document.getElementById(carnetForm.parameters.internalNameFields.Estado);
		var comments = row.querySelector(carnetForm.parameters.errorInvalid.area);
		var isValid = true;
		if(row != null && state != null)
		{
			if(comments.value != "")
			{
				row.removeAttribute(carnetForm.parameters.invalid);
				state.value = carnetForm.parameters.codeStates.Corregir;			
			}
			else
			{
				row.setAttribute(carnetForm.parameters.invalid,carnetForm.parameters.errorInvalid.text);			
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;
	
	},
	validateApproved:function(){
		
		var row = document.querySelector(carnetForm.parameters.fieldsHtml.Comentarios);
		var state = document.getElementById(carnetForm.parameters.internalNameFields.Estado);
		var isValid=true;
		if(row != null && state!= null){
			row.removeAttribute(carnetForm.parameters.invalid);
			state.value = carnetForm.parameters.codeStates.Novedades;
		}
		else
		{
			isValid = false;
		}
		
		return isValid;
	},
	showComments:function(){
		var comments = document.querySelector(carnetForm.parameters.fieldsHtml.Comentarios);	
		if(comments != null)
		{
			comments.setAttribute(carnetForm.parameters.hidden,false);
			comments.querySelector(carnetForm.parameters.errorInvalid.area).value="";

		}
	},	
	loadResponsableState:function(){	
		try
		{
			var userCurrent = _spPageContextInfo.userEmail

			sputilities.getItems(carnetForm.parameters.listName,carnetForm.parameters.query,function(data){
				
				var resultItems = JSON.parse(data).d.results;
				var items = resultItems[0].ResponsablesAbonos.results
				var find = false;
				for(var i=0;i<items.length;i++)
				{
					if(items[i].EMail == userCurrent){
						find = true;
					 	break;
					}
				}
				var state = document.getElementById(carnetForm.parameters.internalNameFields.Estado);
				if(state != null)
				{
					if(find && state.value == carnetForm.parameters.codeStates.Ventanilla)
					{				
						carnetForm.showComments();
					}
					else
					{						
						carnetForm.hideNotAllowed();						
					}
				}
			});
		}
		catch(e){
			console.log(e);
		}	
	},
	hideNotAllowed:function(){
		var html = carnetForm.parameters.bodyItem;
		var formContent = document.querySelector(carnetForm.parameters.containerForm);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	}
}
spForms.options={
	actions:[
		{
			type:spForms.parameters.updateAction,
			name:carnetForm.parameters.formsParameters.buttons.Aprove.Text,
			id:carnetForm.parameters.formsParameters.buttons.Aprove.Id,
			success:function (){
				carnetForm.callFlowService();
				return false;
			},
			error:function (){},
			preSave: function (){
				return carnetForm.validateApproved()
			}
		},
		{
			type:spForms.parameters.updateAction,
			name:carnetForm.parameters.formsParameters.buttons.Revise.Text,
			id:carnetForm.parameters.formsParameters.buttons.Revise.Id,
			success:function (){
				carnetForm.callFlowService();
				return false;
			},
			error:function (){},
			preSave: function (){
				return carnetForm.validateReject()
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:carnetForm.parameters.formsParameters.buttons.Cancel.Text,
			id:carnetForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:carnetForm.parameters.formsParameters.title,
	description:carnetForm.parameters.formsParameters.description,
	contentTypeName:carnetForm.parameters.formsParameters.contentType,
	containerId:carnetForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:false,
  	maxAttachments:10,
  	minAttachments:0,
  	maxSizeInMB:5,
	successLoadEditForm:function(){
		carnetForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
		carnetForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(carnetForm.parameters.formats.dateFormat);			
		carnetForm.loadResponsableState()
	} 
};
spForms.loadEditForm();
