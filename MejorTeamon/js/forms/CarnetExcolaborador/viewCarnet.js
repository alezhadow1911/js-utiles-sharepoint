﻿var carnetForm = {
	parameters:{
		fields:[{field:"NombrePersona1"},{field:"NumeroCelular"},{field:"CorreoElectronico"}],
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']"
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico'			
		},
		resultItems:null,
		invalid:"sp-invalid",
		userProperties:"CellPhone", 
		errorInvalid:"El correo electrónico no tiene el formato correcto",
		loadUserInfo:"carnetForm.loadUserInfo",
		formsIsLoaded:false,
		formsParameters:{
			title:"Solicitud Carnet ExColaborador (Jubilados)",
			description:"",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"}
			},
			contentType:"Carnet ExColaborador",
			containerId:"container-form"			
		}
	},	
	initialize:function(){		
		spForms.loadEditForm();
	}

}
spForms.options={
	actions:[		
		{
			type:spForms.parameters.cancelAction,
			name:carnetForm.parameters.formsParameters.buttons.Cancel.Text,
			id:carnetForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:carnetForm.parameters.formsParameters.title,
	description:carnetForm.parameters.formsParameters.description,
	contentTypeName:carnetForm.parameters.formsParameters.contentType,
	containerId:carnetForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:false,
  	maxAttachments:10,
  	minAttachments:0,
  	maxSizeInMB:5,
	successLoadEditForm:function(){
		
	} 
};
carnetForm.initialize();