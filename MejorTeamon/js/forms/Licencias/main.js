﻿var ctrlForm = {
	parameters:{
		formats:{
			"queryStaticNameRow":"[sp-static-name='{0}']",
			"htmlNotAllowed":"<div><h4>{0}</h4></div>",
			"querySelectRadio":'input[type="radio"][value="{0}"]:checked',
			"queryRadio":'input[type="radio"][value="{0}"]',
			"emailUsers":"{0};",
			"dateFormat":"DD-MM-YYYY",
			"dateCustomFormat":"{1}{0}{2}{0}{3}",
			"formFormat":"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}"
		},
		forms:{
			"NewForm":"ctrlForm.redirectCustomForm('NuevaSolicitud')",
			"EditForm":"ctrlForm.redirectCustomForm('CorregirSolicitud')",
			"DispForm":"ctrlForm.redirectCustomForm('VerSolicitud')",
			"NuevaSolicitud":"ctrlForm.loadNew()",
			"RevisionVentanillaTH":"ctrlForm.loadApprove()",
			"CorregirSolicitud":"ctrlForm.loadEditApplicant()",
			"AplicarNovedad":"ctrlForm.loadApply()",
			"VerSolicitud":"ctrlForm.loadReview()",
			"PendientesAprobar":"",
			"Aprobadas":""
		},
		messages:{
			"idSaveForm":"enviarSolicitud",
			"idCancelForm":"cancelar",
			"idEditForm":"enviarCorreccion",
			"idApproveForm":"aprobar",
			"idRejectForm":"solicitarCorreccion",
			"idApplyForm":"aplicarNovedad",
			"textSaveForm":"Enviar",
			"textCancelForm":"Cancelar",
			"textEditForm":"Enviar",
			"textApproveForm":"Aprobar",
			"textRejectForm":"Enviar a Corregir",
			"textApplyForm":"Novedad Aplicada",
			"textNotAllowed":"Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior"
		},
		columns:{
			"comments":{
				"staticName":"ComentariosTH"
			},
			"displayName":{
				"staticName":"NombrePersona1"
			},
			"idDoc":{
				"staticName":"DocumentoIdentidad"
			},
			"cellPhone":{
				"staticName":"NumeroCelular"
			},
			"email":{
				"staticName":"CorreoElectronico"
			},
			"relationship":{
				"staticName":"ParentescoLicenciaLuto"
			},
			"licenseType":{
				"staticName":"TipoLicencia",
				"states":{
					"Matrimonio":"0",
					"Luto":"1",
					"Calamidad":"2"
				}
			},
			"startDate":{
				"staticName":"FechaInicio"
			},
			"endDate":{
				"staticName":"FechaFinal"
			},
			"backToWork":{
				"staticName":"FechaReintegro"
			}
		},
		states:[
			"Solicitado",
			"Ventanilla TH",
			"Novedades",
			"Novedad Aplicada",
			"Corregir"
		]
		,
		parametersHTML:{
			"spHidden":"sp-hidden",
			"spDisable":"sp-disable"
		},
		selectorFormContent:".form-content",
		cellPhone:"CellPhone",
		functionLoadMyProperties:"ctrlForm.loadMyProperties",
		containerId:"container-form",
		description:"Complete la información para la solicitud",
		descriptionEdit:"Corrija la información de la solicitud",
		descriptionApply:"Revise la información y aplique la novedad si corresponde",
		descriptionApprove:"Revise la información y apruebe o envíe a corrección según corresponda",
		descriptionReview:"Resultado de la solicitud",
		contentTypeName:"Licencias",
		formIsLoaded:false,
		allowAttachments:true,
		requiredAttachments:true,
		maxAttachments:20,
		minAttachments:1,
		maxSizeInMB:3,
		listNameConsecutive:"ControlRadicados",
		idConsecutive:14,
		listUsersFlow:"ResponsablesTH",
		viewUrl:"",
	    editUrl:"",
	    approveUrl:"",
	    applyUrl:"",
	    listViewApply:"",
	    listViewApprove:"",
	    queryUsers:"?$filter=FlujoAsociadoId eq 14&$select=ResponsablesAbonos/EMail,EtapaResponsable&$expand=ResponsablesAbonos",
	    flowUrl:"https://prod-59.westus.logic.azure.com:443/workflows/4e938735d8b2427e832ba5bc2fc19402/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=E2MCq8yEdUILfbhaaUiRvYxOHw0zLGdyLhHdjh6gNcM"
	},
	global:{
		formName:null,
		createdBy:null,
		created:null,
		approveUsers:"",
		applyUsers:""
	},
	initialize:function(){
		var partsOfUrl = window.location.href.split('?')[0].split('/');
		ctrlForm.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];		
		eval(ctrlForm.parameters.forms[ctrlForm.global.formName]);
	},
	callFlowService:function(){
	
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
		ctrlForm.parameters.viewUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[7],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.editUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[5],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.approveUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.applyUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[6],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.listViewApply = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[9]);
	    ctrlForm.parameters.listViewApprove= String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[8]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":ctrlForm.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":ctrlForm.global.created,
		        "comentarios":spForms.loadedFields.ComentariosTH.value
		    },
		    "state":spForms.loadedFields.EstadoSolicitudLicencia.value,
		    "approveUsers":ctrlForm.global.approveUsers,
		    "applyUsers":ctrlForm.global.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":ctrlForm.parameters.listNameConsecutive,
		    "idConsecutive":ctrlForm.parameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
		    "viewUrl":ctrlForm.parameters.viewUrl,
		    "editUrl":ctrlForm.parameters.editUrl,
		    "approveUrl":ctrlForm.parameters.approveUrl,
		    "applyUrl":ctrlForm.parameters.applyUrl,
		    "listViewApply":ctrlForm.parameters.listViewApply,
		    "listViewApprove":ctrlForm.parameters.listViewApprove	
		}
		utilities.http.callRestService({
			url:ctrlForm.parameters.flowUrl,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	getUsers:function(){
		sputilities.getItems(ctrlForm.parameters.listUsersFlow,ctrlForm.parameters.queryUsers,
			function(data){
				var data = utilities.convertToJSON(data);
				if(data != null){
					var items = data.d.results;
					for(var i = 0; i < items.length ;i++){
						var item = items[i];
						for(var j= 0; j < item.ResponsablesAbonos.results.length;j++){
							var user = item.ResponsablesAbonos.results[j];
							if(item.EtapaResponsable == ctrlForm.parameters.states[2]){
								ctrlForm.global.applyUsers += String.format(ctrlForm.parameters.formats.emailUsers,user.EMail);
							}else{
								ctrlForm.global.approveUsers += String.format(ctrlForm.parameters.formats.emailUsers,user.EMail);
							}
						}
					}	
				}
			},
			function(xhr){
				console.log(xhr);
			}
		);
	},
	changeState:function(state){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio,state));
		if(radio != null){
			radio.click();
		}
	},
	hideNotAllowed:function(){
		var html = String.format(ctrlForm.parameters.formats.htmlNotAllowed,ctrlForm.parameters.messages.textNotAllowed);
		var formContent = document.querySelector(ctrlForm.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	},
	validateAllUsers:function(){
		if(spForms.currentItem.AuthorId != sputilities.contextInfo().userId || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
	},
	validateEditUser:function(){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[4])) == null;
		if(spForms.currentItem.AuthorId != sputilities.contextInfo().userId || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
		return true;
	},
	validateApplyUser:function(){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[2])) == null;
		if(ctrlForm.global.applyUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}

	},
	validateApproveUser:function(){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[1])) == null;
		if(ctrlForm.global.approveUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
	},
	hideComments:function(){
		var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.comments.staticName));
		if(ctrlComments != null){
			ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,true);
		}
	},
	loadReview:function(){
		var actions = [
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionReview;
		ctrlForm.loadEdit(actions,ctrlForm.validateAllUsers);
	},
	loadApply:function(){
		var actions = [
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textApplyForm,
				id:ctrlForm.parameters.messages.idApplyForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					ctrlForm.validateRequiredFields();
					ctrlForm.changeState(ctrlForm.parameters.states[3]);
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionApply;
		ctrlForm.loadEdit(actions,ctrlForm.validateApplyUser);

	},
	loadApprove:function(){
		var actions = [
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textApproveForm,
				id:ctrlForm.parameters.messages.idApproveForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					ctrlForm.validateRequiredFields();					
					spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = false;
					ctrlForm.changeState(ctrlForm.parameters.states[2]);
					return true;
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textRejectForm,
				id:ctrlForm.parameters.messages.idRejectForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					ctrlForm.validateRequiredFields();
					spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
					ctrlForm.changeState(ctrlForm.parameters.states[4]);
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
		ctrlForm.loadEdit(actions,ctrlForm.validateApproveUser);

	},
	loadEditApplicant:function(){
		var actions = [
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textEditForm,
				id:ctrlForm.parameters.messages.idEditForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					ctrlForm.validateRequiredFields();
					ctrlForm.changeState(ctrlForm.parameters.states[1]);
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionEdit;
		ctrlForm.loadEdit(actions,function(){
			if(ctrlForm.validateEditUser()){
				ctrlForm.hideComments();
				var ctrlComments = document.getElementById(ctrlForm.parameters.columns.comments.staticName);
				if(ctrlComments != null){
					ctrlComments.value = "";
				}
			}
		});
	},
	loadEdit:function(actions,callBack){
		ctrlForm.getUsers();
		spForms.options={
			actions:actions,
			description:ctrlForm.parameters.description,
			contentTypeName:ctrlForm.parameters.contentTypeName,
			containerId:ctrlForm.parameters.containerId,
			allowAttachments:ctrlForm.parameters.allowAttachments,
		  	requiredAttachments:ctrlForm.parameters.requiredAttachments,
		  	maxAttachments:ctrlForm.parameters.maxAttachments,
		  	minAttachments:ctrlForm.parameters.minAttachments,
		  	maxSizeInMB:ctrlForm.parameters.maxSizeInMB,
			successLoadEditForm:function(){	
				ctrlForm.hideRelationship();
				ctrlForm.global.createdBy = spForms.currentItem.Author.EMail;
				ctrlForm.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(ctrlForm.parameters.formats.dateFormat);
				ctrlForm.formIsLoaded = true;
				
				if(typeof callBack== "function"){
					callBack();
				}
			} 
		};
		spForms.loadEditForm();

	},
	loadNew:function(){
	
		ctrlForm.getUsers();
		ctrlForm.global.createdBy = sputilities.contextInfo().userEmail;
		ctrlForm.global.created = moment.utc(new Date()).format(ctrlForm.parameters.formats.dateFormat);
		sputilities.callFunctionOnLoadBody(ctrlForm.parameters.functionLoadMyProperties);
		ctrlForm.loadNewForm();
	},
	loadNewForm:function(){
		spForms.options={
			actions:[
				{
					type:spForms.parameters.saveAction,
					name:ctrlForm.parameters.messages.textSaveForm,
					id:ctrlForm.parameters.messages.idSaveForm,
					success:function (){
						ctrlForm.callFlowService();
						return false;
					},
					error:function (){return true;},
					preSave: function (){
						ctrlForm.validateRequiredFields();
						ctrlForm.changeState(ctrlForm.parameters.states[0]);
						return true;
					}
				},
				{
					type:spForms.parameters.cancelAction,
					name:ctrlForm.parameters.messages.textCancelForm,
					id:ctrlForm.parameters.messages.idCancelForm,
					callback:null
				},
			],
			description:ctrlForm.parameters.description,
			contentTypeName:ctrlForm.parameters.contentTypeName,
			containerId:ctrlForm.parameters.containerId,
			allowAttachments:ctrlForm.parameters.allowAttachments,
		  	requiredAttachments:ctrlForm.parameters.requiredAttachments,
		  	maxAttachments:ctrlForm.parameters.maxAttachments,
		  	minAttachments:ctrlForm.parameters.minAttachments,
		  	maxSizeInMB:ctrlForm.parameters.maxSizeInMB,
			successLoadNewForm:function(){
				ctrlForm.formIsLoaded = true;
				ctrlForm.hideRelationship();
				ctrlForm.hideComments();
			} 
		};
		spForms.loadNewForm();
	},
	loadMyProperties:function(){
		sputilities.getUserProfileProperties(sputilities.contextInfo().userEmail,
			function(userInfo){
				var objUser = utilities.convertToJSON(userInfo);
				if(objUser != null){
					var displayName = objUser.d.DisplayName;
					var email = objUser.d.Email;
					var cellPhone = ctrlForm.findProperty(objUser,ctrlForm.parameters.cellPhone);
					var validateFormIsLoaded = setInterval(function(){
						if(ctrlForm.formIsLoaded){
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.displayName.staticName,displayName);
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.email.staticName,email);
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.cellPhone.staticName,cellPhone);
							clearInterval(validateFormIsLoaded);
						}
					},500);
				}
			},
			function(xhr){
				console.log(xhr)
			}
		);
	},
	setValueToControl:function(staticName,value){
		if(value != null){
			var ctrl = document.getElementById(staticName);
			if(ctrl != null){
				ctrl.value = value;
			}
		}
	},
	findProperty:function(objUser,name){
		var properties = objUser.d.UserProfileProperties.results;
		for(var i = 0; i < properties.length; i++){
			var property = properties[i];
			if(property.Key == name){
				return property.Value;
			}
		}
	},
	redirectCustomForm:function(name){
		window.location.href = window.location.href.replace(ctrlForm.global.formName,name);
	},
	validateLicenseField:function(){
		var licenseType = document.getElementById(ctrlForm.parameters.columns.licenseType.staticName).value;
		var relationship = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.relationship.staticName))		
		
		if(licenseType != ctrlForm.parameters.columns.licenseType.states.Luto){
			spForms.loadedFields[ctrlForm.parameters.columns.relationship.staticName].required = false;
			relationship.classList.remove('has-error');
			relationship.removeAttribute('sp-invalid','Esto no puede estar en blanco');
			relationship.removeAttribute('sp-required','true');
			relationship.setAttribute('sp-required','false');			
		}else{
			spForms.loadedFields[ctrlForm.parameters.columns.relationship.staticName].required = true;			
		}
	},
	validateEmailField:function(){
		var emailRow = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.email.staticName));
		var email = document.getElementById(ctrlForm.parameters.columns.email.staticName).value;		
		
		if(ctrlForm.validateEmail(email)){
			spForms.loadedFields[ctrlForm.parameters.columns.email.staticName].required = false;
			emailRow.classList.remove('has-error');
			emailRow.removeAttribute('sp-invalid','Esto no puede estar en blanco');
			emailRow.removeAttribute('sp-required','true');
			emailRow.setAttribute('sp-required','false');			
		}else{
			spForms.loadedFields[ctrlForm.parameters.columns.email.staticName].required = true;
			emailRow.setAttribute('sp-invalid','El formato de correo no es valido');
			
		}
	},
	validateDatesLicense:function(){
		var startDateArray = document.getElementById(ctrlForm.parameters.columns.startDate.staticName).value.split('/');
		var endDateArray = document.getElementById(ctrlForm.parameters.columns.endDate.staticName).value.split('/');
		var backToWorkArray = document.getElementById(ctrlForm.parameters.columns.backToWork.staticName).value.split('/');
		var endDateRow = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.endDate.staticName));
		var backToWorkRow = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.backToWork.staticName));
		
		
		if(startDateArray.length == 3 && endDateArray.length == 3 && backToWorkArray.length == 3){			
			var startDate = new Date(String.format(ctrlForm.parameters.formats.dateCustomFormat,'/',startDateArray[1],startDateArray[0],startDateArray[2]));
			var endDate = new Date(String.format(ctrlForm.parameters.formats.dateCustomFormat,'/',endDateArray[1],endDateArray[0],endDateArray[2]));
			var backToWork = new Date(String.format(ctrlForm.parameters.formats.dateCustomFormat,'/',backToWorkArray[1],backToWorkArray[0],backToWorkArray[2]));
			var ready = false;			
			
			spForms.loadedFields[ctrlForm.parameters.columns.endDate.staticName].required = false;
			endDateRow.classList.remove('has-error');
			endDateRow.removeAttribute('sp-invalid','Esto no puede estar en blanco');
			endDateRow.removeAttribute('sp-required','true');
			endDateRow.setAttribute('sp-required','false');
					
			spForms.loadedFields[ctrlForm.parameters.columns.backToWork.staticName].required = false;
			backToWorkRow.classList.remove('has-error');
			backToWorkRow.removeAttribute('sp-invalid','Esto no puede estar en blanco');
			backToWorkRow.removeAttribute('sp-required','true');
			backToWorkRow.setAttribute('sp-required','false');
			
			switch(true){
				case (startDate > endDate):
					spForms.loadedFields[ctrlForm.parameters.columns.endDate.staticName].required = true;
					endDateRow.setAttribute('sp-invalid','La fecha debe ser superior a la fecha de inicio');
				case (endDate > backToWork):
					spForms.loadedFields[ctrlForm.parameters.columns.backToWork.staticName].required = true;
					backToWorkRow.setAttribute('sp-invalid','La fecha debe ser superior a la fecha final');				
			}					
		}

	
	},
	validateRequiredFields:function(){
		ctrlForm.validateLicenseField();
		ctrlForm.validateEmailField();
		ctrlForm.validateDatesLicense();
	},
	hideRelationship:function(){
		var licenseType = document.getElementById(ctrlForm.parameters.columns.licenseType.staticName);
		licenseType.setAttribute('onchange','ctrlForm.hideRelationship()')
		var relationship = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.relationship.staticName));	
		if(licenseType.value != ctrlForm.parameters.columns.licenseType.states.Luto){
			relationship.classList.add('hidden');
		}else{
			relationship.classList.remove('hidden');
		}
	},
	validateEmail: function (email){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);
	
	}
}
ctrlForm.initialize();