﻿var loanForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx",
			"htmlNotAllowed":"<div class='form-group row'><label>{0}</label></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>"
		},
		query:"?$Select=FlujoAsociado/Title,ResponsablesAbonos/EMail,ID&$expand=FlujoAsociado,ResponsablesAbonos&$filter=Title eq '{0}'",
		listName:"ResponsablesTH",
		assingTo:{
			Ventanilla:"Préstamos Ventanilla TH",
			Novedades:"Préstamos Novedades"
		},
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']",
			Comentarios:"[sp-static-name='ComentariosTH']",
			ComentariosCodeudor:"[sp-static-name='ComentariosCodeudor']",
			TipoPrestamo:"[sp-static-name='TipoPrestamo']",
			MontoSolicitado:"[sp-static-name='MontoSolicitado']",
			DescuentosPrestamos:"[sp-static-name='DescuentosPrestamos']",
			PrimerCodeudor:"[sp-static-name='PrimerCodeudor']",
			CedulaPrimerCodeudor:"[sp-static-name='CedulaPrimerCodeudor']",
			SegundoCodeudor:"[sp-static-name='SegundoCodeudor']",
			CedulaSegundoCodeudor:"[sp-static-name='CedulaSegundoCodeudor']",
			sysContadorAprobaciones:"[sp-static-name='sysContadorAprobaciones']",
			PrimerCodeudorSpan:"PrimerCodeudor_TopSpan",
			SegundoCodeudorSpan:"SegundoCodeudor_TopSpan"
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico',	
			Comentarios:"ComentariosTH",
			ComentariosCodeudor:"ComentariosCodeudor",
			Estado:"EstadoPrestamo",
			sysContadorAprobaciones:"sysContadorAprobaciones"
		
		},		
		controlsHtml:{
			Attachment:"sp-forms-add-attachment",
			Aprobar:"btn-aprobar",
			Corregir:"btn-corregir",
			Aplicar:"btn-aplicar",
			Rechazar:"btn-rechazar",
			classDelete:".delete-doc",
			classGroup:".form-group.row",
			colorDisable:"#B1B1B1",
			borderDisable:"#E1E1E1",
			iconTH:"ico-help-ComentariosTH",
			iconCodeudor:"ico-help-ComentariosCodeudor"
		},
		errorInvalid:{text:"Los comentarios son obligatorios",area:"textarea"},
		htmlRequiered:{label:"label",block:"block",none:"none"},		
		invalid:"sp-invalid",
		hidden:"sp-hidden",		
		resultItems:null,	
		codeStates:{Solicitado:"0",Ventanilla:"1",Novedades:"2",Aplicada:"3",Corregir:"4",Codeudor:"5",Rechazado:"6"},	
		userProperties:"CellPhone",
		bodyItem:"Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior.",
		selectorFormContent:".form-content",
		queryCount:"({0})",
		formsParameters:{
			title:"",
			description:"",
			titleApprove:"Aprobar/Corregir Solicitud de Préstamo",
			titleAplicate:"Aplicar Solicitud de Préstamo",
			descriptionApprove:"Completa la información correspondiente, para aprobar o rechazar la solicitud",
			descriptionAplicate:"Revisa la información de la solicitud, para aplicar la novedad",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Prestamos",
			containerId:"container-form"			
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"loanForm.loadApprove()",
			"AprobarCodeudor":"loanForm.loadApproveCoSigner()",
			"AplicarSolicitud":"loanForm.loadApplicate()",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Solicitudes pendientes Novedades":"Vista Novedades",
			"Solicitudes pendientes Ventanilla TH":"Vista Ventanilla"
		},
		flowParameters:{
			urlFlow:"https://prod-14.westus.logic.azure.com:443/workflows/bbf39bd67b134c8c8994329371c581c9/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=LEYe_gBEg-ql-3npmsGsaadLZ78qMKlczkd16LA8DZM",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",
	   		approveCoSignerUrl:"",
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Préstamos Ventanilla TH",
	    	applyUsers:"Préstamos Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:20,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Préstamos"
		},
		formsState:{
			AprobarSolicitud:"1",
			AprobarCodeudor:"5",
			AplicarSolicitud:"2"
		},
		global:{
			formName:null,
			createdBy:null,
			created:null

		},
		initialState:null

	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
		
		var firstCosigner = document.getElementById(loanForm.parameters.fieldsHTML.PrimerCodeudorSpan);
		var secondCosigner = document.getElementById(loanForm.parameters.fieldsHTML.SegundoCodeudorSpan);
        var peoplePickerFC = SPClientPeoplePicker.SPClientPeoplePickerDict[firstCosigner.id];
        var peoplePickerSC = SPClientPeoplePicker.SPClientPeoplePickerDict[secondCosigner.id];

	    loanForm.parameters.flowParameters.approveUrl = String.format(loanForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(loanForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    loanForm.parameters.flowParameters.approveCoSignerUrl = String.format(loanForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(loanForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    loanForm.parameters.flowParameters.applyUrl = String.format(loanForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(loanForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    loanForm.parameters.flowParameters.editUrl = String.format(loanForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(loanForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    loanForm.parameters.flowParameters.listViewApply = String.format(loanForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(loanForm.parameters.forms)[6]);
	    loanForm.parameters.flowParameters.listViewApprove = String.format(loanForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(loanForm.parameters.forms)[7]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":loanForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		    	"celular":spForms.loadedFields.NumeroCelular.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":loanForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosTH.value,
		        "comentariosCodeudor":spForms.loadedFields.ComentariosCodeudor.value,
			    "primerCodeudor": loanForm.getDataPeople(peoplePickerFC),
			    "segundoCodeudor": loanForm.getDataPeople(peoplePickerSC),
			    "montoSolicitado":Math.round(spForms.loadedFields.MontoSolicitado.value),
			    "descuento":spForms.loadedFields.DescuentosPrestamos.value,
			    "tipoPrestamo":spForms.loadedFields.TipoPrestamo.value,
			    "formAprobacion":"Si"
		    },
		    "state":spForms.loadedFields.EstadoPrestamo.value,
		    "approveUsers":loanForm.parameters.flowParameters.approveUsers,
		    "applyUsers":loanForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":loanForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":loanForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":loanForm.parameters.flowParameters.listNameAssing,
		    "editUrl":loanForm.parameters.flowParameters.editUrl,
		    "approveUrl":loanForm.parameters.flowParameters.approveUrl,
		    "applyUrl":loanForm.parameters.flowParameters.applyUrl,
		    "approveCoSignerUrl":loanForm.parameters.flowParameters.approveCoSignerUrl,
		    "listViewApply":loanForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":loanForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	loanForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:loanForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){			
		var partsOfUrl = window.location.href.split('?')[0].split('/');
		loanForm.parameters.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];
		eval(loanForm.parameters.forms[loanForm.parameters.global.formName]);

	},
	loadApprove:function(){	
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:loanForm.parameters.formsParameters.buttons.Aprove.Text,
				id:loanForm.parameters.formsParameters.buttons.Aprove.Id,
				success:function (){
					loanForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return loanForm.validateApprove()
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:loanForm.parameters.formsParameters.buttons.Revise.Text,
				id:loanForm.parameters.formsParameters.buttons.Revise.Id,
				success:function (){
					loanForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return loanForm.validateReject()
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:loanForm.parameters.formsParameters.buttons.Cancel.Text,
				id:loanForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		loanForm.parameters.formsParameters.description = loanForm.parameters.formsParameters.descriptionApprove;
		loanForm.parameters.formsParameters.title = loanForm.parameters.formsParameters.titleApprove;
		var query = String.format(loanForm.parameters.query,encodeURIComponent(loanForm.parameters.assingTo.Ventanilla));
		loanForm.parameters.query = query;
		loanForm.loadEdit(actions,loanForm.loadResponsableState);

		
	},
	loadApproveCoSigner:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:loanForm.parameters.formsParameters.buttons.Aprove.Text,
				id:loanForm.parameters.formsParameters.buttons.Aprove.Id,
				success:function (){
					loanForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return loanForm.validateApprove()
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:loanForm.parameters.formsParameters.buttons.Reject.Text,
				id:loanForm.parameters.formsParameters.buttons.Reject.Id,
				success:function (){
					loanForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return loanForm.validateReject()
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:loanForm.parameters.formsParameters.buttons.Cancel.Text,
				id:loanForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		loanForm.parameters.formsParameters.description = loanForm.parameters.formsParameters.descriptionApprove;
		loanForm.parameters.formsParameters.title = loanForm.parameters.formsParameters.titleApprove;
        loanForm.loadEdit(actions,loanForm.loadResponsableState);
		var ctrlInterval = setInterval(function(){
				loanForm.setValueSysCount();
		},2000);
	},
	loadApplicate:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:loanForm.parameters.formsParameters.buttons.Aplicate.Text,
				id:loanForm.parameters.formsParameters.buttons.Aplicate.Id,
				success:function (){
					loanForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return loanForm.validateApprove()
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:loanForm.parameters.formsParameters.buttons.Cancel.Text,
				id:loanForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		loanForm.parameters.formsParameters.description = loanForm.parameters.formsParameters.descriptionAplicate;
		loanForm.parameters.formsParameters.title = loanForm.parameters.formsParameters.titleAplicate;
		var query = String.format(loanForm.parameters.query,encodeURIComponent(loanForm.parameters.assingTo.Novedades));
		loanForm.parameters.query = query;
		loanForm.loadEdit(actions,loanForm.loadResponsableState);

	},
	loadEdit:function(actions,sucessFunction){
		spForms.options={	
			actions:actions,
			title:loanForm.parameters.formsParameters.title,
			description:loanForm.parameters.formsParameters.description,
			contentTypeName:loanForm.parameters.formsParameters.contentType,
			containerId:loanForm.parameters.formsParameters.containerId,
			allowAttachments:true,
		  	requiredAttachments:false,
		  	maxAttachments:20,
		  	minAttachments:0,
		  	maxSizeInMB:3,
			successLoadEditForm:function(){	
				loanForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
				loanForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(loanForm.parameters.formats.dateFormat);	
				if(typeof sucessFunction== "function"){
					sucessFunction();
				}
			} 
		};
		spForms.loadEditForm();
	},
	setValueSysCount: function(){
		sputilities.getItems(sputilities.contextInfo().listTitle,String.format(loanForm.parameters.queryCount,parseInt(spForms.currentItemID)),function(data){
			//save the results to an array
			var resultItems = JSON.parse(data).d;
			loanForm.parameters.resultItems = resultItems;
			document.getElementById(loanForm.parameters.internalNameFields.sysContadorAprobaciones).value = loanForm.parameters.resultItems.sysContadorAprobaciones;
		},
		function(xhr){
			console.log(xhr);
		});	
	},
	validateApprove:function(){
		var row = loanForm.parameters.initialState == loanForm.parameters.codeStates.Ventanilla ? document.querySelector(loanForm.parameters.fieldsHTML.Comentarios) : document.querySelector(loanForm.parameters.fieldsHTML.ComentariosCodeudor);
		var state = document.getElementById(loanForm.parameters.internalNameFields.Estado);
		loanForm.parameters.formsIsLoaded = false;
		var isValid = true;
		if(loanForm.parameters.initialState == loanForm.parameters.codeStates.Ventanilla)
		{			
			if(row != null && state!= null){
				row.removeAttribute(loanForm.parameters.invalid);
				state.value = loanForm.parameters.codeStates.Novedades;
			}
			else
			{
				isValid = false;
			}
		}
		if(loanForm.parameters.initialState == loanForm.parameters.codeStates.Novedades)
		{	
			if(state!= null){				
				state.value = loanForm.parameters.codeStates.Aplicada;
			}
			else
			{
				isValid = false;
			}
		}
		
		return isValid;		
	},
	validateReject:function(){
		var row = loanForm.parameters.initialState == loanForm.parameters.codeStates.Ventanilla ? document.querySelector(loanForm.parameters.fieldsHTML.Comentarios): document.querySelector(loanForm.parameters.fieldsHTML.ComentariosCodeudor);
		var state = document.getElementById(loanForm.parameters.internalNameFields.Estado);		
		var isValid = true;
		
		if(row != null && state != null)
		{
			var comments = row.querySelector(loanForm.parameters.errorInvalid.area);
			if(comments.value != "")
			{
				row.removeAttribute(loanForm.parameters.invalid);
				if(loanForm.parameters.initialState == loanForm.parameters.codeStates.Codeudor)
				{
					state.value = loanForm.parameters.codeStates.Rechazado;
				}
				else
				{
					state.value = loanForm.parameters.codeStates.Corregir;

				}			
			}
			else
			{
				row.setAttribute(loanForm.parameters.invalid,loanForm.parameters.errorInvalid.text);			
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;

	},
	loadResponsableState:function(){	
		try
		{
			var userCurrent = _spPageContextInfo.userEmail
			var state = document.getElementById(loanForm.parameters.internalNameFields.Estado);
			if(state != null)
			{
				if(state.value == loanForm.parameters.formsState[loanForm.parameters.global.formName])
				{
					if(state.value == loanForm.parameters.codeStates.Novedades || state.value == loanForm.parameters.codeStates.Ventanilla)
					{
						sputilities.getItems(loanForm.parameters.listName,loanForm.parameters.query,function(data){
							
							var resultItems = JSON.parse(data).d.results;
							if(resultItems.length > 0)
							{
								var items = resultItems[0].ResponsablesAbonos.results
								var find = false;
								for(var i=0;i<items.length;i++)
								{
									if(items[i].EMail == userCurrent){
										find = true;
									 	break;
									}
								}
								
								loanForm.parameters.initialState = state.value;
								
								if(state.value == loanForm.parameters.codeStates.Novedades)
								{				
									if(find)
									{
										loanForm.showComments(state.value);
									}
									else
									{
										loanForm.showComments(state.value);
										loanForm.hideButtons(state.value);
										loanForm.showPopUp();
									}
								}
								else if(state.value == loanForm.parameters.codeStates.Ventanilla)
								{						
									if(find)
									{
										loanForm.showComments(state.value);
									}
									else
									{
										loanForm.showComments(state.value);
										loanForm.hideButtons(state.value);
										loanForm.showPopUp();
									}
								}
							}						
						});
					}
					else
					{
						var find = false;
						var currentUserId = _spPageContextInfo.userId;
						
						if (currentUserId == spForms.loadedFields.PrimerCodeudor.value || currentUserId == spForms.loadedFields.SegundoCodeudor.value){
								find = true;
						}
						loanForm.parameters.initialState = state.value;
						
						if(state.value == loanForm.parameters.codeStates.Codeudor)
						{				
							if(find)
							{
								loanForm.showComments(state.value);
							}
							else
							{
								loanForm.showComments(state.value);
								loanForm.hideButtons(state.value);
								loanForm.showPopUp();
							}
					   }
					}
                }
                else
                {
                    loanForm.showComments(loanForm.parameters.formsState[loanForm.parameters.global.formName]);
                    loanForm.hideButtons(loanForm.parameters.formsState[loanForm.parameters.global.formName]);
                    loanForm.showPopUp();
                }
		    }
		}
		catch(e){
			console.log(e);
		}	
	},
	hideButtons:function(state){
	
		if(state == loanForm.parameters.codeStates.Novedades)
		{
			var bottonApp = document.getElementById(loanForm.parameters.controlsHtml.Aplicar);
			if(bottonApp != null)
			{
				bottonApp.style.display = loanForm.parameters.htmlRequiered.none;
			}
		}
		else if(state == loanForm.parameters.codeStates.Ventanilla)
		{
			var bottonApp = document.getElementById(loanForm.parameters.controlsHtml.Aprobar);
			var bottonRej = document.getElementById(loanForm.parameters.controlsHtml.Corregir);
			var comments  = document.getElementById(loanForm.parameters.internalNameFields.Comentarios);
			var icon = document.getElementById(loanForm.parameters.controlsHtml.iconTH);
			
			if(bottonApp != null && bottonRej != null && comments != null && icon != null)
			{
				comments.style.pointerEvents = loanForm.parameters.htmlRequiered.none;
				icon.style.pointerEvents = loanForm.parameters.htmlRequiered.none;
				comments.style.color = loanForm.parameters.controlsHtml.colorDisable;
				comments.style.borderColor = loanForm.parameters.controlsHtml.borderDisable;	
				bottonApp.style.display = loanForm.parameters.htmlRequiered.none;
				bottonRej.style.display = loanForm.parameters.htmlRequiered.none;	

			}
		
		}
		else if(state == loanForm.parameters.codeStates.Codeudor)
		{
			var bottonApp = document.getElementById(loanForm.parameters.controlsHtml.Aprobar);
			var bottonRej = document.getElementById(loanForm.parameters.controlsHtml.Rechazar);
			var comments  = document.getElementById(loanForm.parameters.internalNameFields.ComentariosCodeudor);
			var icon = document.getElementById(loanForm.parameters.controlsHtml.iconCodeudor);
			
			if(bottonApp != null && bottonRej != null && comments != null && icon != null)
			{
				comments.style.pointerEvents = loanForm.parameters.htmlRequiered.none;
				icon.style.pointerEvents = loanForm.parameters.htmlRequiered.none;
				comments.style.color = loanForm.parameters.controlsHtml.colorDisable;
				comments.style.borderColor = loanForm.parameters.controlsHtml.borderDisable;	
				bottonApp.style.display = loanForm.parameters.htmlRequiered.none;
				bottonRej.style.display = loanForm.parameters.htmlRequiered.none;	
			}
		}	
	},
	showPopUp:function(){
		var html = String.format(loanForm.parameters.formats.htmlNotAllowed,loanForm.parameters.bodyItem);
		var formContent = document.querySelector(loanForm.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}	
	},
	showComments:function(state){
		if(state == loanForm.parameters.codeStates.Codeudor)
		{
			var comments = document.querySelector(loanForm.parameters.fieldsHTML.ComentariosCodeudor);	
			if(comments != null)
			{
				comments.setAttribute(loanForm.parameters.hidden,false);
				comments.querySelector(loanForm.parameters.errorInvalid.area).value="";
	
			}
		}
		else if(state == loanForm.parameters.codeStates.Ventanilla)
		{
			var commentsCodeudor = document.querySelector(loanForm.parameters.fieldsHTML.ComentariosCodeudor);	
			var comments = document.querySelector(loanForm.parameters.fieldsHTML.Comentarios);	
			if(comments != null && commentsCodeudor != null)
			{
				comments.setAttribute(loanForm.parameters.hidden,false);
				commentsCodeudor.setAttribute(loanForm.parameters.hidden,false);
				comments.querySelector(loanForm.parameters.errorInvalid.area).value="";
	
			}
		}
		else if(state == loanForm.parameters.codeStates.Novedades)
		{
			var commentsCodeudor = document.querySelector(loanForm.parameters.fieldsHTML.ComentariosCodeudor);	
			var comments = document.querySelector(loanForm.parameters.fieldsHTML.Comentarios);	
			if(comments != null && commentsCodeudor != null)
			{
				comments.setAttribute(loanForm.parameters.hidden,false);
				commentsCodeudor.setAttribute(loanForm.parameters.hidden,false);
			}
		}
	},
	getDataPeople: function (peoplePicker){
	   	var userList = peoplePicker.GetAllUserInfo();
	    var userInfo = userList[0];
	    var userData = null;
	 
	    if(userInfo != null)
	    {
	   		userData = {
		    "displayName":userInfo.DisplayText,
			"email": userInfo.EntityData.Email
			}
	    }
	   return userData;
	}
}
loanForm.initialize();