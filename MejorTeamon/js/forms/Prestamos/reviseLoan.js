﻿var loanForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx",
			"htmlNotAllowed":"<div class='form-group row'><label>{0}</label></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>"
		},
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']",
			Comentarios:"[sp-static-name='ComentariosTH']",
			ComentariosCodeudor:"[sp-static-name='ComentariosCodeudor']",
			TipoPrestamo:"[sp-static-name='TipoPrestamo']",
			MontoSolicitado:"[sp-static-name='MontoSolicitado']",
			DescuentosPrestamos:"[sp-static-name='DescuentosPrestamos']",
			PrimerCodeudor:"[sp-static-name='PrimerCodeudor']",
			CedulaPrimerCodeudor:"[sp-static-name='CedulaPrimerCodeudor']",
			SegundoCodeudor:"[sp-static-name='SegundoCodeudor']",
			CedulaSegundoCodeudor:"[sp-static-name='CedulaSegundoCodeudor']",
			sysContadorAprobaciones:"[sp-static-name='sysContadorAprobaciones']",
			PrimerCodeudorSpan:"PrimerCodeudor_TopSpan",
			SegundoCodeudorSpan:"SegundoCodeudor_TopSpan"
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico',	
			Comentarios:"ComentariosTH",
			ComentariosCodeudor:"ComentariosCodeudor",
			Estado:"EstadoPrestamo",
			sysContadorAprobaciones:"sysContadorAprobaciones"
		},		
		controlsHtml:{
            Attachment:"sp-forms-add-attachment",
            Enviar:"btn-enviar",            
			Aprobar:"btn-aprobar",
			Corregir:"btn-corregir",
			Aplicar:"btn-aplicar",
			Rechazar:"btn-rechazar",
			classDelete:".delete-doc",
			classGroup:".form-group.row",
			colorDisable:"#B1B1B1",
			borderDisable:"#E1E1E1",
			iconTH:"ico-help-ComentariosTH",
			iconJefe:"ico-help-ComentariosCodeudor"
		},
		bodyItem:"Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior.",
		TitlePopUp:"Acceso Denegado",
    	htmlRequiered:{block:"block",none:"none"},	            
		invalid:"sp-invalid",
		selectorFormContent:".form-content",
		userProperties:"CellPhone", 
		errorInvalid:"El correo electrónico no tiene el formato correcto",
        codeStates:{Solicitado:"0",Ventanilla:"1",Novedades:"2",Aplicada:"3",Corregir:"4",Codeudor:"5",Rechazado:"6"},		
		formsParameters:{
			title:"Corregir Solicitud de Préstamo",
			description:"Completa la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			}		
		},
		formsParameters:{
			title:"Solicitud préstamo",
			description:"Corrige la información correspondiente",
			titleApprove:"Aprobar / Corregir solicitud de préstamo",
			titleAplicate:"Aplicar solicitud préstamo",
			descriptionApprove:"Completa la información correspondiente, para aprobar o rechazar la solicitud",
			descriptionAplicate:"Revisa la información de la solicitud, para aplicar la novedad",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Prestamos",
			containerId:"container-form"			
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"loanForm.loadApprove()",
			"AprobarCodeudor":"loanForm.loadApproveCoSigner()",
			"AplicarSolicitud":"loanForm.loadApplicate()",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Solicitudes pendientes Novedades":"Vista Novedades",
			"Solicitudes pendientes Ventanilla TH":"Vista Ventanilla"
		},
		flowParameters:{
			urlFlow:"https://prod-14.westus.logic.azure.com:443/workflows/bbf39bd67b134c8c8994329371c581c9/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=LEYe_gBEg-ql-3npmsGsaadLZ78qMKlczkd16LA8DZM",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",
	   		approveCoSignerUrl:"",
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Préstamos Ventanilla TH",
	    	applyUsers:"Préstamos Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:20,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Préstamos"
		},
		global:{		
			createdBy:null,
			created:null
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
		
		var firstCosigner = document.getElementById(loanForm.parameters.fieldsHTML.PrimerCodeudorSpan);
		var secondCosigner = document.getElementById(loanForm.parameters.fieldsHTML.SegundoCodeudorSpan);
        var peoplePickerFC = SPClientPeoplePicker.SPClientPeoplePickerDict[firstCosigner.id];
        var peoplePickerSC = SPClientPeoplePicker.SPClientPeoplePickerDict[secondCosigner.id];

	    loanForm.parameters.flowParameters.approveUrl = String.format(loanForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(loanForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    loanForm.parameters.flowParameters.approveCoSignerUrl = String.format(loanForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(loanForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    loanForm.parameters.flowParameters.applyUrl = String.format(loanForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(loanForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    loanForm.parameters.flowParameters.editUrl = String.format(loanForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(loanForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    loanForm.parameters.flowParameters.listViewApply = String.format(loanForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(loanForm.parameters.forms)[6]);
	    loanForm.parameters.flowParameters.listViewApprove = String.format(loanForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(loanForm.parameters.forms)[7]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":loanForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		    	"celular":spForms.loadedFields.NumeroCelular.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":loanForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosTH.value,
		        "comentariosCodeudor":spForms.loadedFields.ComentariosCodeudor.value,
			    "primerCodeudor": loanForm.getDataPeople(peoplePickerFC),
			    "segundoCodeudor": loanForm.getDataPeople(peoplePickerSC),
			    "montoSolicitado":Math.round(spForms.loadedFields.MontoSolicitado.value),
			    "descuento":spForms.loadedFields.DescuentosPrestamos.value,
			    "tipoPrestamo":spForms.loadedFields.TipoPrestamo.value,
			    "syscontador":spForms.loadedFields.sysContadorAprobaciones.value,
			    "formAprobacion":"No"
		    },
		    "state":spForms.loadedFields.EstadoPrestamo.value,
		    "approveUsers":loanForm.parameters.flowParameters.approveUsers,
		    "applyUsers":loanForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":loanForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":loanForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":loanForm.parameters.flowParameters.listNameAssing,
		    "editUrl":loanForm.parameters.flowParameters.editUrl,
		    "approveUrl":loanForm.parameters.flowParameters.approveUrl,
		    "applyUrl":loanForm.parameters.flowParameters.applyUrl,
		    "approveCoSignerUrl":loanForm.parameters.flowParameters.approveCoSignerUrl,
		    "listViewApply":loanForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":loanForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	loanForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:loanForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){
		spForms.loadEditForm();
	},
	validateFormat:function(){
		var rowEmail = document.querySelector(loanForm.parameters.fieldsHTML.CorreoElectronico);
		var email = document.getElementById(loanForm.parameters.internalNameFields.CorreoElectronico);
		var isValid = true;		
		if(email != null && rowEmail!= null)
		{			
			if(loanForm.validateEmail(email.value) || email.value == "")
			{
				rowEmail.removeAttribute(loanForm.parameters.invalid);
			}
			else
			{
				rowEmail.setAttribute(loanForm.parameters.invalid,loanForm.parameters.errorInvalid);
			}
		}
		else
		{
			isValid=false;
		}
		
		var state = document.getElementById(loanForm.parameters.internalNameFields.Estado);
		if(state != null)
		{
			state.value = loanForm.parameters.codeStates.Ventanilla;
		}
		else
		{
			isValid = false;
		}
		return isValid;

	},
	validateEmail: function (email){
		 var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);
	
	},
	loadResponsableState:function(){
		try
		{
			var currentUserId =_spPageContextInfo.userId;		
		
			var find = false;
			var state = document.getElementById(loanForm.parameters.internalNameFields.Estado);
			if(state != null)
			{
				if(spForms.currentItem.AuthorId == currentUserId){
					find = true;
				}
								
				if(state.value != loanForm.parameters.codeStates.Corregir)
				{				
					loanForm.showPermissions();
				}
				else
				{
					if(!find)
					{
						loanForm.showPermissions();
					}
				}
			}
		}
		catch(e){
			console.log(e);
		}	
	},
	showPermissions:function(){
		var html = String.format(loanForm.parameters.formats.htmlNotAllowed,loanForm.parameters.bodyItem);
		var formContent = document.querySelector(loanForm.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}						
	},
	disableInputs:function(){
	
		var rows = document.querySelectorAll(loanForm.parameters.controlsHtml.classGroup);
		
		if(rows != null)
		{
			for(var i=0;i<rows.length;i++)
			{
				rows[i].style.pointerEvents = loanForm.parameters.htmlRequiered.none;
			}
		}
	},
	getDataPeople: function (peoplePicker){
	   	var userList = peoplePicker.GetAllUserInfo();
	    var userInfo = userList[0];
	    var userData = null;
	 
	    if(userInfo != null)
	    {
	   		userData = {
		    "displayName":userInfo.DisplayText,
			"email": userInfo.EntityData.Email
			}
	    }
	   return userData;
	}
}
spForms.options={
	actions:[
		{
			type:spForms.parameters.updateAction,
			name:loanForm.parameters.formsParameters.buttons.Send.Text,
			id:loanForm.parameters.formsParameters.buttons.Send.Id,
			success:function (){
				loanForm.callFlowService();
				return false;
			},
			error:function (){},
			preSave: function (){
				return loanForm.validateFormat();
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:loanForm.parameters.formsParameters.buttons.Cancel.Text,
			id:loanForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:loanForm.parameters.formsParameters.title,
	description:loanForm.parameters.formsParameters.description,
	contentTypeName:loanForm.parameters.formsParameters.contentType,
	containerId:loanForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:false,
  	maxAttachments:20,
  	minAttachments:0,
  	maxSizeInMB:3,
	successLoadEditForm:function(){
		loanForm.loadResponsableState();
		loanForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
		loanForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(loanForm.parameters.formats.dateFormat);
	} 
};
loanForm.initialize();