﻿var loanForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']",
			PrimerCodeudor:"PrimerCodeudor_TopSpan",
			SegundoCodeudor:"SegundoCodeudor_TopSpan"
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico',		
		},
		resultItems:null,
		invalid:"sp-invalid",
		requiredTag:"<i>*</i>",
		userProperties:"CellPhone", 
		errorInvalid:"El correo electrónico no tiene el formato correcto",
		errorInvalidNotText:"Esto no puede estar en blanco",
		loadUserInfo:"loanForm.loadUserInfo",
		formsIsLoaded:false,
		formsParameters:{
			title:"Solicitudes para préstamos",
			description:"Completa la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Prestamos",
			containerId:"container-form"			
		},
		flowParameters:{
			urlFlow:"https://prod-14.westus.logic.azure.com:443/workflows/bbf39bd67b134c8c8994329371c581c9/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=LEYe_gBEg-ql-3npmsGsaadLZ78qMKlczkd16LA8DZM",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",
	   		approveCoSignerUrl:"",
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Préstamos Ventanilla TH",
	    	applyUsers:"Préstamos Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:20,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Préstamos"
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud Ventanilla TH",
			"AprobarCodeudor":"Aprobar Codeudor",
			"AplicarSolicitud":"Aplicar Novedad",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Solicitudes pendientes Novedades":"Vista Novedades",
			"Solicitudes pendientes Ventanilla TH":"Vista Ventanilla TH"
		},
		global:{
			createdBy:null,
			created:null
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
		
		var firstCosigner = document.getElementById(loanForm.parameters.fieldsHTML.PrimerCodeudor);
		var secondCosigner = document.getElementById(loanForm.parameters.fieldsHTML.SegundoCodeudor);
        var peoplePickerFC = SPClientPeoplePicker.SPClientPeoplePickerDict[firstCosigner.id];
        var peoplePickerSC = SPClientPeoplePicker.SPClientPeoplePickerDict[secondCosigner.id];
					
	    loanForm.parameters.flowParameters.approveUrl = String.format(loanForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(loanForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    loanForm.parameters.flowParameters.approveCoSignerUrl = String.format(loanForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(loanForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    loanForm.parameters.flowParameters.applyUrl = String.format(loanForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(loanForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    loanForm.parameters.flowParameters.editUrl = String.format(loanForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(loanForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    loanForm.parameters.flowParameters.listViewApply = String.format(loanForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(loanForm.parameters.forms)[6]);
	    loanForm.parameters.flowParameters.listViewApprove= String.format(loanForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(loanForm.parameters.forms)[7]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":loanForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		    	"celular":spForms.loadedFields.NumeroCelular.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":loanForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosTH.value,
		        "comentariosCodeudor":spForms.loadedFields.ComentariosCodeudor.value,
			    "primerCodeudor": loanForm.getDataPeople(peoplePickerFC),
			    "segundoCodeudor": loanForm.getDataPeople(peoplePickerSC),
			    "montoSolicitado":Math.round(spForms.loadedFields.MontoSolicitado.value),
			    "descuento":spForms.loadedFields.DescuentosPrestamos.value,
			    "tipoPrestamo":spForms.loadedFields.TipoPrestamo.value,
			    "syscontador":0,
			    "formAprobacion":"No"
		    },
		    "state":spForms.loadedFields.EstadoPrestamo.value,
		    "approveUsers":loanForm.parameters.flowParameters.approveUsers,
		    "applyUsers":loanForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":loanForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":loanForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":loanForm.parameters.flowParameters.listNameAssing,
		    "editUrl":loanForm.parameters.flowParameters.editUrl,
		    "approveUrl":loanForm.parameters.flowParameters.approveUrl,
		    "applyUrl":loanForm.parameters.flowParameters.applyUrl,
		    "approveCoSignerUrl":loanForm.parameters.flowParameters.approveCoSignerUrl,
		    "listViewApply":loanForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":loanForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	loanForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:loanForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity = 0;
				document.querySelector('.form-content').style.opacity = 1;
				document.querySelector('.loader-container').style.height = 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){
		sputilities.callFunctionOnLoadBody(loanForm.parameters.loadUserInfo);
		spForms.loadNewForm();
	},
	loadUserInfo:function(){
		try {
			//running the query with parameters
			sputilities.getMyUserProfileProperties("",function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d;
				loanForm.parameters.resultItems = resultItems;
				var ctrlInterval = setInterval(function(){
					if(loanForm.parameters.formsIsLoaded)
					{
						loanForm.setValuesUserInfo();
						clearInterval(ctrlInterval);
					}
				},500);
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	setValuesUserInfo:function(){
	
		if(loanForm.parameters.resultItems != null)
		{
		  	var name = document.getElementById(loanForm.parameters.internalNameFields.NombrePersona);
		  	var email = document.getElementById(loanForm.parameters.internalNameFields.CorreoElectronico);
		  	var cellPhone = document.getElementById(loanForm.parameters.internalNameFields.NumeroCelular);
		  	if(name != null){
		  		name.value = loanForm.parameters.resultItems.DisplayName ? loanForm.parameters.resultItems.DisplayName: "";
		  	}
		  	
		  	if(email != null){
		  		email.value = loanForm.parameters.resultItems.Email ? (loanForm.parameters.resultItems.Email).toLowerCase(): "";
		  	}
		  	
		  	if(cellPhone != null){
		  	
		  		var userProperties = loanForm.parameters.resultItems.UserProfileProperties.results;
		  	
			  	for(var i=0;i<userProperties.length;i++)
			  	{
			  		if(userProperties[i].Key == loanForm.parameters.userProperties)
			  		{
			  		 	cellPhone.value =  userProperties[i].Value;
			  		 	break;
			  		}
			  	}
			}
	  	}
	},
	validateFormat:function(){
		var rowEmail = document.querySelector(loanForm.parameters.fieldsHTML.CorreoElectronico);
		var email = document.getElementById(loanForm.parameters.internalNameFields.CorreoElectronico);
		var isValid = true;		
		if(email != null && rowEmail!= null)
		{			
			if(loanForm.validateEmail(email.value) || email.value == "")
			{
				rowEmail.removeAttribute(loanForm.parameters.invalid);
			}
			else
			{
				rowEmail.setAttribute(loanForm.parameters.invalid,loanForm.parameters.errorInvalid);
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;
		
	},
	validateEmail: function (email){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);
	},
	getDataPeople: function (peoplePicker){
	   	var userList = peoplePicker.GetAllUserInfo();
	    var userInfo = userList[0];
	    var userData = null;
	 
	    if(userInfo != null)
	    {
	   		userData = {
		    "displayName":userInfo.DisplayText,
			"email": userInfo.EntityData.Email
			}
	    }
	   return userData;
	}
}

spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:loanForm.parameters.formsParameters.buttons.Send.Text,
			id:loanForm.parameters.formsParameters.buttons.Send.Id,
			success:function (){
				loanForm.callFlowService();
				return false;

			},
			error:function (){},
			preSave: function (){
				spForms.loadedFields.sysContadorAprobaciones.value = 0;
				return loanForm.validateFormat();
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:loanForm.parameters.formsParameters.buttons.Cancel.Text,
			id:loanForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:loanForm.parameters.formsParameters.title,
	description:loanForm.parameters.formsParameters.description,
	contentTypeName:loanForm.parameters.formsParameters.contentType,
	containerId:loanForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:false,
  	maxAttachments:20,
  	minAttachments:0,
  	maxSizeInMB:3,
	successLoadNewForm:function(){
		loanForm.parameters.global.createdBy = sputilities.contextInfo().userEmail;
		loanForm.parameters.global.created = moment.utc(new Date()).format(loanForm.parameters.formats.dateFormat);
		loanForm.parameters.formsIsLoaded = true;
	} 
};
loanForm.initialize();