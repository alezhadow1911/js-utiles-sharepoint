﻿var certificationForm = {
	parameters:{
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']"
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico'			
		},
		resultItems:null,
		invalid:"sp-invalid",
		userProperties:"CellPhone", 
		errorInvalid:"El correo electrónico no tiene el formato correcto",
		loadUserInfo:"certificationForm.loadUserInfo",
		formsIsLoaded:false,
		formsParameters:{
			title:"Solicitud Certificaciones",
			description:"",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Certificaciones",
			containerId:"container-form"			
		}

	},	
	initialize:function(){
		
		spForms.loadEditForm();
	}
	
}

spForms.options={
	actions:[
		{
			type:spForms.parameters.cancelAction,
			name:certificationForm.parameters.formsParameters.buttons.Cancel.Text,
			id:certificationForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:certificationForm.parameters.formsParameters.title,
	description:certificationForm.parameters.formsParameters.description,
	contentTypeName:certificationForm.parameters.formsParameters.contentType,
	containerId:certificationForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:false,
  	maxSizeInMB:3,
	successLoadEditForm:function(){		
	} 
};
certificationForm.initialize();