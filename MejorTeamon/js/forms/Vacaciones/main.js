﻿var ctrlForm = {
	parameters:{
		formats:{
			"queryStaticNameRow":"[sp-static-name='{0}']",
			"htmlNotAllowed":"<div><h4>{0}</h4></div> <div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
			"querySelectRadio":'input[type="radio"][value="{0}"]:checked',
			"queryRadio":'input[type="radio"][value="{0}"]',
			"emailUsers":"{0};",
			"dateFormat":"DD-MM-YYYY",
			"formFormat":"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}"
		},
		forms:{
			"NewForm":"ctrlForm.redirectCustomForm('NuevaSolicitud')",
			"EditForm":"ctrlForm.redirectCustomForm('CorregirSolicitud')",
			"DispForm":"ctrlForm.redirectCustomForm('VerSolicitud')",
			"NuevaSolicitud":"ctrlForm.loadNew()",
			"RevisionVentanillaTH":"ctrlForm.loadApprove()",
			"CorregirSolicitud":"ctrlForm.loadEditApplicant()",
			"AplicarNovedad":"ctrlForm.loadApply()",
			"VerSolicitud":"ctrlForm.loadReview()",
			"PendientesAprobar":"",
			"Aprobadas":"",
			"Jefe":"ctrlForm.loadJefe()"
		},
		messages:{
			"idSaveForm":"enviarSolicitud",
			"idCancelForm":"cancelar",
			"idEditForm":"enviarCorreccion",
			"idApproveForm":"aprobar",
			"idRejectForm":"RechazadoTH",
			"idApplyForm":"aplicarNovedad",
			"idRejectManager":"RechazoJefe",
			"textSaveForm":"Enviar",
			"textCancelForm":"Cancelar",
			"textEditForm":"Enviar",
			"textApproveForm":"Aprobar",
			"textRejectForm":"Rechazar",
			"textApplyForm":"Aplicar Novedad",
			"textRejectManager":"Rechazar",
			"textNotAllowed":"Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior"
		},
		columns:{
			"comments":{
				"staticName":"ComentariosTH"
			},
			"commentsManager":{
				"staticName":"ComentariosJefe1"
			},
			
			"displayName":{
				"staticName":"NombrePersona1"
			},
			"idDoc":{
				"staticName":"NombrePersona1"
			},
			"cellPhone":{
				"staticName":"NumeroCelular"
			},
			"email":{
				"staticName":"CorreoElectronico"
			}
		},
		states:[
			"Solicitado",
			"Ventanilla TH",
			"Novedades",
			"Novedad Aplicada",
			"Corregir",
			"Jefe",
			"Rechazado Jefe",
			"Rechazado TH"			
		]
		,
		parametersHTML:{
			"spHidden":"sp-hidden",
			"spDisable":"sp-disable"
		},
		ID:{
			ContentClose: ".col-md-6.col-xs-6.delete-doc",
			ContentAdd: "a#sp-forms-add-attachment",
			Attachment:"div[sp-static-name='sp-forms-attachment']",
			Modalidad:"div[sp-static-name='Modalidad_x0020_Vacaciones'] select",
			DiasI:"div[sp-static-name='FechaInicioDisfrute']",
			DiasF:"div[sp-static-name='FechaFinDisfrute']",
			ReintegroStatic:"div[sp-static-name='FechadeReintegro']",
			Correo: "CorreoElectronico",
			FechaI: "FechaInicioDisfrute",
			FechaF: "FechaFinDisfrute",
			Reintegro: "FechadeReintegro"

			
		},
		style:{
			None: "none",
			Block: "block",
			Opacity: "0.7",
			Invalid:"sp-invalid",
			Message:"Formato de correo invalido",
			MessageDate:"Fecha final menor a fecha inicial",
			MessageSame:"Fechas inicial y final iguales",
			MessageReintegro:"Fecha reintegro menor a fechas de vacaciones",
			MessageReintegroSame:"Fecha reintegro igual a fecha inicial o final",	

		},


		selectorFormContent:".form-content",
		cellPhone:"CellPhone",
		functionLoadMyProperties:"ctrlForm.loadMyProperties",
		containerId:"container-form",
		description:"Complete la información para la solicitud",
		descriptionEdit:"Corrija la información de la solicitud",
		descriptionApply:"Revise la información y aplique la novedad si corresponde",
		descriptionApprove:"Revise la información y apruebe o envíe a corrección según corresponda",
		descriptionReview:"Resultado de la solicitud",
		contentTypeName:"Vacaciones",
		formIsLoaded:false,
		allowAttachments:true,
		requiredAttachments:true,
		maxAttachments:20,
		minAttachments:0,
		maxSizeInMB:3,
		listNameConsecutive:"ControlRadicados",
		idConsecutive:16,
		listUsersFlow:"ResponsablesTH",
		viewUrl:"",
	    editUrl:"",
	    approveUrl:"",
	    applyUrl:"",
	    applyManagerUrl:"",
	    listViewApply:"",
	    listViewApprove:"",
	    queryUsers:"?$filter=FlujoAsociadoId eq 16&$select=ResponsablesAbonos/EMail,EtapaResponsable&$expand=ResponsablesAbonos",
	    flowUrl:"https://prod-62.westus.logic.azure.com:443/workflows/32609321b2d04568a4c605226ed90830/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=I1N2ApD-t7-2HEzgM0UrV2qZp-xFML7EMN2qKfOavTg"},
	global:{
		formName:null,
		createdBy:null,
		created:null,
		approveUsers:"",
		applyUsers:"",
		applyManager:"",
		displayManager:""
	},
	initialize:function(){
		var partsOfUrl = window.location.href.split('?')[0].split('/');
		ctrlForm.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];
		eval(ctrlForm.parameters.forms[ctrlForm.global.formName]);
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
		ctrlForm.parameters.viewUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[7],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.editUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[5],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.approveUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.applyUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[6],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.applyManagerUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[10],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.listViewApply = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[9]);
	    ctrlForm.parameters.listViewApprove= String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[8]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":ctrlForm.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":ctrlForm.global.created,
		        "comentarios":spForms.loadedFields.ComentariosTH.value,
		        "comentariosJefe":spForms.loadedFields.ComentariosJefe1.value,		        
   		        "diasSolicitados":Math.round(spForms.loadedFields.DiasSolicitados1.value),
   		        		        
		        "Correo":spForms.listInfo.title
		    },
		    "state":spForms.loadedFields.EstadosCajaCompensacion.value,
		    "titleCorreo":spForms.listInfo.title,  	
		    "approveUsers":ctrlForm.global.approveUsers,
		    "applyUsers":ctrlForm.global.applyUsers,
		    "applyManager":ctrlForm.global.applyManager,
   		    "displayManager":ctrlForm.global.displayManager,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":ctrlForm.parameters.listNameConsecutive,
		    "idConsecutive":ctrlForm.parameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
		    "viewUrl":ctrlForm.parameters.viewUrl,
		    "editUrl":ctrlForm.parameters.editUrl,
		    "approveUrl":ctrlForm.parameters.approveUrl,
		    "applyUrl":ctrlForm.parameters.applyUrl,
		    "applyManagerUrl":ctrlForm.parameters.applyManagerUrl,
		    "listViewApply":ctrlForm.parameters.listViewApply,
		    "listViewApprove":ctrlForm.parameters.listViewApprove	
		}
		utilities.http.callRestService({
			url:ctrlForm.parameters.flowUrl,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	getUsers:function(){
		sputilities.getItems(ctrlForm.parameters.listUsersFlow,ctrlForm.parameters.queryUsers,
			function(data){
				var data = utilities.convertToJSON(data);
				if(data != null){
					var items = data.d.results;
					for(var i = 0; i < items.length ;i++){
						var item = items[i];
						for(var j= 0; j < item.ResponsablesAbonos.results.length;j++){
							var user = item.ResponsablesAbonos.results[j];
							if(item.EtapaResponsable == ctrlForm.parameters.states[2]){
								ctrlForm.global.applyUsers += String.format(ctrlForm.parameters.formats.emailUsers,user.EMail);
							}else{
								ctrlForm.global.approveUsers += String.format(ctrlForm.parameters.formats.emailUsers,user.EMail);
							}
						}
					}	
				}
			},
			function(xhr){
				console.log(xhr);
			}
		);
	},
		getManager:function(){
		var idUserCreated = _spPageContextInfo.userId;
			sputilities.getUserInformation(idUserCreated,function(data){
				var resultItems = JSON.parse(data).d;
				var Email = resultItems.EMail;
			sputilities.getUserProfileProperty('Manager',Email,
			function(data){
				var resultProperty = JSON.parse(data).d;
				var EmailJefe = resultProperty.GetUserProfilePropertyFor;
				if(EmailJefe == ""){
				ctrlForm.global.applyManager = "";
				}
				else{
				var CurrentJefe = EmailJefe.split("|")[2];
				ctrlForm.global.applyManager = CurrentJefe;
				ctrlForm.loadMyPropertiesManager();
				}
			});	
			});
			},
	getManagerTotal:function(){
			var idUserCreated = spForms.currentItem.AuthorId;
			sputilities.getUserInformation(idUserCreated,function(data){
				var resultItems = JSON.parse(data).d;
				var Email = resultItems.EMail;
			sputilities.getUserProfileProperty('Manager',Email,
			function(data){
				var resultProperty = JSON.parse(data).d;
				var EmailJefe = resultProperty.GetUserProfilePropertyFor;
				var CurrentJefe = EmailJefe.split("|")[2];
				ctrlForm.global.applyManager = CurrentJefe;
				ctrlForm.validateApplyManager();
				ctrlForm.loadMyPropertiesManager();
			});	
			});	
			},


	changeState:function(state){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio,state));
		if(radio != null){
			radio.click();
		}
	},
	hideNotAllowed:function(){
		var html = String.format(ctrlForm.parameters.formats.htmlNotAllowed,ctrlForm.parameters.messages.textNotAllowed);
		var formContent = document.querySelector(ctrlForm.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	},
	validateUserCreated:function(){
		if(spForms.currentItem.AuthorId != sputilities.contextInfo().userId){
			ctrlForm.hideNotAllowed();
			return false;
		}
		ctrlForm.hideAttachments();
		ctrlForm.hideDate();
	},
	validateAllUsers:function(){
		if(spForms.currentItem.AuthorId != sputilities.contextInfo().userId || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
		ctrlForm.hideDate();
	},
	validateEditUser:function(){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[4])) == null;
		if(spForms.currentItem.AuthorId != sputilities.contextInfo().userId || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
		ctrlForm.hideDate();
		return true;
	},
	//Novedades
	validateApplyUser:function(){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[2])) == null;
		if(ctrlForm.global.applyUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
		ctrlForm.hideAttachments();
		ctrlForm.hideDate();
	},
	validateApplyManager:function(){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[5])) == null;
		if(ctrlForm.global.applyManager != sputilities.contextInfo().userEmail || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
		ctrlForm.hideAttachments();
		ctrlForm.hideDate();
	},

	//Ventanilla TH
	validateApproveUser:function(){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[1])) == null;
		if(ctrlForm.global.approveUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}	
		ctrlForm.hideAttachments();
		ctrlForm.hideDate();
	},
	hideAttachments:function(){
		var i;
		var CloaseAttachments = document.querySelectorAll(ctrlForm.parameters.ID.ContentClose);
			for(i=0; i<CloaseAttachments.length; i++){
			CloaseAttachments[i].style.display = ctrlForm.parameters.style.None;
			}
		document.querySelector(ctrlForm.parameters.ID.ContentAdd).style.display = ctrlForm.parameters.style.None;
		document.querySelector(ctrlForm.parameters.ID.Attachment).style.opacity = ctrlForm.parameters.style.Opacity;
	},

	hideComments:function(){
		var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.comments.staticName));
		if(ctrlComments != null){
			ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,true);
		}
	},
	hideDate:function(){
	if(document.querySelector(ctrlForm.parameters.ID.Modalidad).value == "1"){
	document.querySelector(ctrlForm.parameters.ID.DiasI).style.display = ctrlForm.parameters.style.None;
	document.querySelector(ctrlForm.parameters.ID.DiasF).style.display = ctrlForm.parameters.style.None;
	document.querySelector(ctrlForm.parameters.ID.Reitegro).style.display = ctrlForm.parameters.style.None;
	}
	},

	blockedComments:function(){
		var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.comments.staticName));
		if(ctrlComments != null){
			ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,true);
		}
	},

	loadReview:function(){
		var actions = [
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionReview;
		ctrlForm.loadEdit(actions,ctrlForm.validateUserCreated);
	},
	loadApply:function(){
		var actions = [
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textApplyForm,
				id:ctrlForm.parameters.messages.idApplyForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					ctrlForm.changeState(ctrlForm.parameters.states[3]);
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionApply;
		ctrlForm.loadEdit(actions,ctrlForm.validateApplyUser);

	},
	loadApprove:function(){
		var actions = [
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textApproveForm,
				id:ctrlForm.parameters.messages.idApproveForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = false;
					ctrlForm.changeState(ctrlForm.parameters.states[2]);
					return true;
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textRejectForm,
				id:ctrlForm.parameters.messages.idRejectForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
					ctrlForm.changeState(ctrlForm.parameters.states[7]);
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
		ctrlForm.loadEdit(actions,ctrlForm.validateApproveUser);

	},
	loadJefe:function(){
		var actions = [
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textApproveForm,
				id:ctrlForm.parameters.messages.idApproveForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = false;
					ctrlForm.changeState(ctrlForm.parameters.states[1]);
					return true;
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textRejectManager,
				id:ctrlForm.parameters.messages.idRejectManager,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					spForms.loadedFields[ctrlForm.parameters.columns.commentsManager.staticName].required = true;
					ctrlForm.changeState(ctrlForm.parameters.states[6]);
					return true;
				}
			},
			];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
		
		ctrlForm.loadEdit(actions,null);

	},

	loadEditApplicant:function(){
		var actions = [
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textEditForm,
				id:ctrlForm.parameters.messages.idEditForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					ctrlForm.changeState(ctrlForm.parameters.states[1]);
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionEdit;
		ctrlForm.loadEdit(actions,function(){
			if(ctrlForm.validateEditUser()){
			ctrlForm.hideComments();
				var ctrlComments = document.getElementById(ctrlForm.parameters.columns.comments.staticName);
				if(ctrlComments != null){
					ctrlComments.value = "";
				}
			}
		});
	},
	loadEdit:function(actions,callBack){
		ctrlForm.getUsers();
		spForms.options={
			actions:actions,
			description:ctrlForm.parameters.description,
			contentTypeName:ctrlForm.parameters.contentTypeName,
			containerId:ctrlForm.parameters.containerId,
			allowAttachments:ctrlForm.parameters.allowAttachments,
		  	requiredAttachments:ctrlForm.parameters.requiredAttachments,
		  	maxAttachments:ctrlForm.parameters.maxAttachments,
		  	minAttachments:ctrlForm.parameters.minAttachments,
		  	maxSizeInMB:ctrlForm.parameters.maxSizeInMB,
			successLoadEditForm:function(){	
				ctrlForm.global.createdBy = spForms.currentItem.Author.EMail;
				ctrlForm.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(ctrlForm.parameters.formats.dateFormat);
				ctrlForm.formIsLoaded = true;
				if(ctrlForm.global.formName == "Jefe"){
				ctrlForm.getManagerTotal();
				}
				
				if(typeof callBack== "function"){
					callBack();
				}
			} 
		};
		spForms.loadEditForm();

	},
	loadNew:function(){
		ctrlForm.getUsers();
		ctrlForm.getManager();
		ctrlForm.global.createdBy = sputilities.contextInfo().userEmail;
		ctrlForm.global.created = moment.utc(new Date()).format(ctrlForm.parameters.formats.dateFormat);
		sputilities.callFunctionOnLoadBody(ctrlForm.parameters.functionLoadMyProperties);
		ctrlForm.loadNewForm();
	},
	loadNewForm:function(){
		spForms.options={
			actions:[
				{
					type:spForms.parameters.saveAction,
					name:ctrlForm.parameters.messages.textSaveForm,
					id:ctrlForm.parameters.messages.idSaveForm,
					success:function (){
						ctrlForm.callFlowService();
						return false;
					},
					error:function (){return true;},
					preSave: function (){
					var uno = spForms.loadedFields.DocumentoIdentidad.value
						console.log("ingreso a pre");
						console.log(uno);
						ctrlForm.changeState(ctrlForm.parameters.states[0]);
						return true;
					}
				},
				{
					type:spForms.parameters.cancelAction,
					name:ctrlForm.parameters.messages.textCancelForm,
					id:ctrlForm.parameters.messages.idCancelForm,
					callback:null
				},
			],
			description:ctrlForm.parameters.description,
			contentTypeName:ctrlForm.parameters.contentTypeName,
			containerId:ctrlForm.parameters.containerId,
			allowAttachments:ctrlForm.parameters.allowAttachments,
		  	requiredAttachments:ctrlForm.parameters.requiredAttachments,
		  	maxAttachments:ctrlForm.parameters.maxAttachments,
		  	minAttachments:ctrlForm.parameters.minAttachments,
		  	maxSizeInMB:ctrlForm.parameters.maxSizeInMB,
			successLoadNewForm:function(){
				ctrlForm.formIsLoaded = true;
				ctrlForm.hideComments();
				ctrlForm.AddEvent();
				ctrlForm.validateEmailForm();
				ctrlForm.validateDateInicial();
			} 
		};
		spForms.loadNewForm();
	},
	loadMyPropertiesManager:function(){
		sputilities.getUserProfileProperties(ctrlForm.global.applyManager,
			function(userInfo){
				var objUser = utilities.convertToJSON(userInfo);
				if(objUser != null){
					var displayName = objUser.d.DisplayName;
					ctrlForm.global.displayManager = displayName;
					}
			},
			function(xhr){
				console.log(xhr)
			}
		);
	},

	loadMyProperties:function(){
		sputilities.getUserProfileProperties(sputilities.contextInfo().userEmail,
			function(userInfo){
				var objUser = utilities.convertToJSON(userInfo);
				if(objUser != null){
					var displayName = objUser.d.DisplayName;
					var email = objUser.d.Email;
					var cellPhone = ctrlForm.findProperty(objUser,ctrlForm.parameters.cellPhone);
					var validateFormIsLoaded = setInterval(function(){
						if(ctrlForm.formIsLoaded){
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.displayName.staticName,displayName);
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.email.staticName,email);
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.cellPhone.staticName,cellPhone);
							clearInterval(validateFormIsLoaded);
						}
					},500);
				}
			},
			function(xhr){
				console.log(xhr)
			}
		);
	},
	setValueToControl:function(staticName,value){
		if(value != null){
			var ctrl = document.getElementById(staticName);
			if(ctrl != null){
				ctrl.value = value;
			}
		}
	},
	findProperty:function(objUser,name){
		var properties = objUser.d.UserProfileProperties.results;
		for(var i = 0; i < properties.length; i++){
			var property = properties[i];
			if(property.Key == name){
				return property.Value;
			}
		}
	},
		AddEvent:function(){
		document.querySelector(ctrlForm.parameters.ID.Modalidad).setAttribute("onchange","javascript:ctrlForm.click(this)")
		console.log("ingreso al click")
	},


			
	click:function(ctrl){
	if(ctrl.value == "1"){
	document.querySelector(ctrlForm.parameters.ID.DiasI).style.display = ctrlForm.parameters.style.None;
	spForms.loadedFields.FechaInicioDisfrute.required = false	
	document.querySelector(ctrlForm.parameters.ID.DiasF).style.display = ctrlForm.parameters.style.None;
	spForms.loadedFields.FechaFinDisfrute.required = false
	document.querySelector(ctrlForm.parameters.ID.Reitegro).style.display = ctrlForm.parameters.style.None;
	spForms.loadedFields.FechadeReintegro.required = false
	}else{
	document.querySelector(ctrlForm.parameters.ID.DiasI).style.display = ctrlForm.parameters.style.Block;
	spForms.loadedFields.FechaInicioDisfrute.required = true
	document.querySelector(ctrlForm.parameters.ID.DiasF).style.display = ctrlForm.parameters.style.Block;
	spForms.loadedFields.FechaFinDisfrute.required = true
	document.querySelector(ctrlForm.parameters.ID.Reitegro).style.display = ctrlForm.parameters.style.Block;
	spForms.loadedFields.FechadeReintegro.required = true

	}
	},
	validateDateInicial:function(){
		var Inicial = document.getElementById(ctrlForm.parameters.ID.FechaI);
		var Final = document.getElementById(ctrlForm.parameters.ID.FechaF);
		var Reitegro = document.getElementById(ctrlForm.parameters.ID.Reintegro);
		if(Final != null){
				Final.addEventListener('blur',function(event){
				var FinalEvent = moment(this.value,'DD/MM/YYYY').format('MM/DD/YYYY');
				var InicialEvent = moment(Inicial.value,'DD/MM/YYYY').format('MM/DD/YYYY')


				var finalDate = document.querySelector(ctrlForm.parameters.ID.DiasF)
				if(FinalEvent == InicialEvent){
				finalDate.setAttribute(ctrlForm.parameters.style.Invalid,ctrlForm.parameters.style.MessageSame)	
				}
				if(FinalEvent < InicialEvent){
				finalDate.setAttribute(ctrlForm.parameters.style.Invalid,ctrlForm.parameters.style.MessageDate)
				}
				if(FinalEvent > InicialEvent){
				finalDate.removeAttribute(ctrlForm.parameters.style.Invalid)
				}		    		
  					});
					}
		
		if(Reitegro != null){
				Reitegro.addEventListener('blur',function(event){
				var ReitegroEvent = moment(this.value,'DD/MM/YYYY').format('MM/DD/YYYY');
				var FinalEvent = moment(Final.value,'DD/MM/YYYY').format('MM/DD/YYYY');
				var InicialEvent = moment(Inicial.value,'DD/MM/YYYY').format('MM/DD/YYYY')

				var reintegroDate = document.querySelector(ctrlForm.parameters.ID.ReintegroStatic)
				var finalDate = document.querySelector(ctrlForm.parameters.ID.DiasF)
				if(ReitegroEvent < InicialEvent || ReitegroEvent < FinalEvent ){
				reintegroDate.setAttribute(ctrlForm.parameters.style.Invalid,ctrlForm.parameters.style.MessageReintegro)	
				}
				if(ReitegroEvent > InicialEvent && ReitegroEvent >FinalEvent ){
				reintegroDate.removeAttribute(ctrlForm.parameters.style.Invalid)
				}
				if(ReitegroEvent == InicialEvent || ReitegroEvent == FinalEvent ){
				reintegroDate.setAttribute(ctrlForm.parameters.style.Invalid,ctrlForm.parameters.style.MessageReintegroSame)
				}
	
				
		
  					});
					}


			},

	validateEmailForm:function(){
		var input = document.getElementById(ctrlForm.parameters.ID.Correo);
			if(input != null){
				input.addEventListener('change',function(event){
				var email = this.value;
		  			if (ctrlForm.validateEmail(email)) {
		  				var staticName = "[sp-static-name="+ctrlForm.parameters.ID.Correo+"]";
		  				var InputSelect = document.querySelector(staticName);
   							InputSelect.removeAttribute(ctrlForm.parameters.style.Invalid)
   			  				} 	
   			  		else{
		  				var staticName = "[sp-static-name="+ctrlForm.parameters.ID.Correo+"]";
		  				var InputSelect = document.querySelector(staticName);
   						InputSelect.setAttribute(ctrlForm.parameters.style.Invalid,ctrlForm.parameters.style.Message)
   			  			}
  				return false;
  					});
					}
	},
	validateEmail:function(email){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 			return re.test(email);
 	 	},


	redirectCustomForm:function(name){
		window.location.href = window.location.href.replace(ctrlForm.global.formName,name);
	}
}
ctrlForm.initialize();