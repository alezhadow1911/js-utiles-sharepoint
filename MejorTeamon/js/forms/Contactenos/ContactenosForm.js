﻿var PopUP = {
	//create parameters
	parameters:{
			listName: "PruebaForm",
			bodyItem:"<div class='contact'><p>Te invitamos a resolver tus inquietudes y enviarnos tus comentarios y sugerencias sobre nuestra Intranet corporativa diligenciando este formulario. Si deseas solicitar información sobre otros servicios o canales de comunicación, por favor ponte en contacto directamente con el área encargada</p></div>",
			TitlePopUp:"Contactenos",
			IdEnviar: "Enviar",
			IdCancelar:"Cancelar",
			ContentType:"Contactenos",
			IdDiv:"spForm"
	},	
	InitPopUp:function(){
		try {
			sputilities.showModalDialog(PopUP.parameters.TitlePopUp,null,null,PopUP.parameters.bodyItem,function(data){
			},);
		}catch (e){
			console.log(e);
		}
	},
	Initialize:function(){
		try {
		PopUP.loadNewForm();
		}
		catch (e){
			console.log(e);
		}
	},
	loadNewForm:function(){
	spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:PopUP.parameters.IdEnviar,
			id:PopUP.parameters.IdEnviar,
			success:function(){
			PopUP.redirect();
			return false;

			},
			error:function(){},
			preSave:function(){
			return true;
			
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:PopUP.parameters.IdCancelar,
			id:PopUP.parameters.IdCancelar,
			callback:""
		},
	],
	contentTypeName:PopUP.parameters.ContentType,
	containerId:PopUP.parameters.IdDiv,
	successLoadNewForm:function(){
		PopUP.InitPopUp();
		PopUP.validate();
	} 
	};
	spForms.loadNewForm();
	},
	validate:function(input){
		var input = document.getElementById('Correo');
			if(input != null){
				input.addEventListener('change',function(event){
				var email = this.value;
		  			if (PopUP.validateEmail(email)) {
		  				var uno=document.getElementsByClassName('form-group row')[1]
   							uno.removeAttribute('sp-invalid')
   			  				} 	
   			  		else{
   						var uno=document.getElementsByClassName('form-group row')[1]
   						uno.setAttribute('sp-invalid','Formato de correo invalido.')
   			  			}
  				return false;
  					});
					}
	},
	
	validateEmail:function(email){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 		return re.test(email);
 	 	},
 	 	redirect:function(){
 	 	window.location.replace("http://stackoverflow.com");
 	 	},

}
PopUP.Initialize();


