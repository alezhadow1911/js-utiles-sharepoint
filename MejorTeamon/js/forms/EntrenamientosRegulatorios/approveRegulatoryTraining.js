﻿var regulatoryTrainingForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx",
			"htmlNotAllowed":"<div class='form-group row'><label>{0}</label></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>"
		},
		query:"?$Select=Responsable/EMail,Estacion/Title&$expand=Responsable,Estacion&$filter=Estacion/Title eq '{0}'",
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			Extension:"[sp-static-name='Extension']",
			NumeroTelefono:"[sp-static-name='NumeroTelefono']",
			CorreoElectronico:"[sp-static-name='CorreoElectronicoAlterno']",
			Estacion:"[sp-static-name='Estacion0']> div > div > select",
			Area:"[sp-static-name='Area0'] > div > div > select",
			COASelect:"[sp-static-name='COA'] > div > div > select",
			COA:"[sp-static-name='COA']",
			Manual:"[sp-static-name='Manual']",
			TipoCurso:"[sp-static-name='TipoCurso']",
			CantidadColaboradores:"[sp-static-name='CantidadColaboradores']",
			FechaVencimiento:"[sp-static-name='FechaVencimiento']",
			FechaPropuestaEntrenamieto:"[sp-static-name='FechaPropuestaEntrenamieto']",
			FechaEntrenamiento:"[sp-static-name='FechaEntrenamiento']",
			EstadoEntrenamientoRegulatorio:"[sp-static-name='EstadoEntrenamientoRegulatorio']",
			ManualSelect:"[sp-static-name='ManualSelect'] > div > div > select",
			CursosSelect:"[sp-static-name='TipoCursosSelect'] > div > div > select",
			ObservacionesResponsable:"[sp-static-name='ObservacionesResponsable']",
			NombreEntrenador:"[sp-static-name='NombreEntrenador']",
			Sala:"[sp-static-name='Sala']"
		},
		internalNameFields:{			
			NombrePersona:"NombrePersona1",
			Extension:"Extension",
			NumeroTelefono:"NumeroTelefono",
			NombreEntrenador:"NombreEntrenador",
			CorreoElectronico:"CorreoElectronicoAlterno",
			Estacion:"Estacion0",
			Area:"Area0",
			COA:"COA",
			Manual:"Manual",
			TipoCurso:"TipoCurso",
			CantidadColaboradores:"CantidadColaboradores",
			FechaVencimiento:"FechaVencimiento",
			FechaPropuestaEntrenamieto:"FechaPropuestaEntrenamieto",
			FechaEntrenamiento:"FechaEntrenamiento",
			EstadoEntrenamientoRegulatorio:"EstadoEntrenamientoRegulatorio",
			TipoCursoSelect:"TipoCursosElement",
			ManualSelect:"ManualElement",
			ObservacionesResponsable:"ObservacionesResponsable",
			NombreEntrenador:"NombreEntrenador",
			Sala:"Sala"			
		},
		controlsHtml:{
			Attachment:"sp-forms-add-attachment",
			Aprobar:"btn-aprobar",
			Corregir:"btn-corregir",
			Aplicar:"btn-aplicar",
			Rechazar:"btn-rechazar",
			classDelete:".delete-doc",
			classGroup:".form-group.row",
			colorDisable:"#B1B1B1",
			borderDisable:"#E1E1E1",
			iconTH:"ico-help-ObservacionesResponsable"
		},		
		errorInvalid:{text:"Esto no puede estar en blanco",area:"input", areaComments:"textarea"},
		htmlRequiered:{label:"label",block:"block",none:"none"},		
		invalid:"sp-invalid",
		errorInvalidCorreo:"El correo electrónico no tiene el formato correcto",
		hidden:"sp-hidden",
		listaNameDL:"ResponsablesEntrenamiento",	
		resultItems:null,
		resultManual:null,
		resultCurso:null,
		formsIsLoaded:false,
		selectIsLoaded:false,	
		listNameManual:"Manual",
		listNameCurso:"TipoCurso",
		codeStates:{ Solicitado:"0",ConfirmacionEntrenamiento:"1",Corregir:"2",ConfirmarFecha:"3",AsignarSala:"4",Finalizada:"5"},	
		userProperties:"Department",
		bodyItem:"Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior.",
		selectorFormContent:".form-content",
		queryCount:"({0})",
		htmlOptionCursos:"<option id='{0}-TipoCurso' value='{0}'>{1}</option>",
		htmlOptionManual:"<option id='{0}-Manual' value='{0}'>{1}</option>",
		htmlCurso:"<div class='form-group row' sp-static-name='TipoCursosSelect'>"+
						   "<label class='col-sm-4 control-label'>Tipo Cursos<i>*</i></label>"+
						   "<div class='col-sm-8'>"+
						      "<div class='form-control-help'>"+
						         "<select class='form-control input-lg' sp-required='true' sp-type='lookup'>{0}</select>"+
						         "<a class='glyphicon glyphicon-question-sign ico-help' aria-hidden='true'></a>"+
						      "</div>"+
						      "<span id='help-block-TipoCursosSelect' class='help-block forms-hide-element'></span>"+
						      "<span id='help-block-error-message-TipoCursosSelect' class='help-block forms-hide-element error-message'></span>"+
						   "</div>"+
					"</div>",
		htmlManual:"<div class='form-group row' sp-static-name='ManualSelect'>"+
						   "<label class='col-sm-4 control-label'>Manual<i>*</i></label>"+
						   "<div class='col-sm-8'>"+
						      "<div class='form-control-help'>"+
						         "<select class='form-control input-lg' sp-required='true' sp-type='lookup'>{0}</select>"+
						         "<a class='glyphicon glyphicon-question-sign ico-help' aria-hidden='true'></a>"+
						      "</div>"+
						      "<span id='help-block-ManualSelect' class='help-block forms-hide-element'></span>"+
						      "<span id='help-block-error-message-ManualSelect' class='help-block forms-hide-element error-message'></span>"+
						   "</div>"+
					"</div>",
		formsParameters:{
			title:"",
			description:"",
			titleApprove:"Aprobar solicitud de entrenamiento regulatorio",
			descriptionApprove:"Completa la información correspondiente, para aprobar o rechazar la solicitud",
			titleRoom:"Asignar fecha de entrega de entrenamiento regulatorio",
			descriptionRoom:"Completa la información correspondiente, para asociar la fecha de entrega",
			titleDate:"Asignar fecha de entrega de entrenamiento regulatorio",
			descriptionDate:"Completa la información correspondiente, para asociar la fecha de entrega",
			titleToCorrect:"Corregir solicitud entrenamiento regulatorio",
			descriptionToCorrect:"Complete la información correspondiente para corregir la solicitud",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Reject:{Text:"Enviar a corregir", Id:"enviar-corregir"},
				ToCorrect:{Text:"Corregir", Id:"corregir"}
			},
			contentType:"Entrenamientos Regulatorios",
			containerId:"container-form"			
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobacionEntrenamiento":"regulatoryTrainingForm.loadApprove()",
			"AsignarSala":"regulatoryTrainingForm.loadAssingRoom()",
			"ConfirmarFecha":"regulatoryTrainingForm.loadAssignDate()",
			"Corregir":"regulatoryTrainingForm.loadToCorrect()",
			"VerSolicitud":"Ver Solicitud"
		},
		flowParameters:{
			urlFlow:"https://prod-62.westus.logic.azure.com:443/workflows/3f2410a9add94059aa6f885775e57055/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=Bn7ciXBuBeUPCz4PiP8vMqOXnspWPwZoYfvaipJiBKs",
			confirmUrl:"",
			assingRoomUrl:"",
	    	approveUrl:"",
	   		assingRoomUrl:"",
	   		toCorrectUrl:"",
	   		confirmDateUrl:"",
	    	listNameAssing:"ResponsablesEntrenamiento",
	    	subjectTitle:"Entrenamiento Regulatorio"
		},
		formsState:{
			AprobacionEntrenamiento:"1",
			AsignarSala:"4",
			ConfirmarFecha:"3",
			Corregir:"2"
		},
		global:{
			formName:null,
			createdBy:null,
			created:null
		},
		initialState:null

	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
		
		var objectSelect = document.querySelector(regulatoryTrainingForm.parameters.fieldsHTML.Estacion);
		var textSelected = objectSelect.options[objectSelect.selectedIndex].text;

	    regulatoryTrainingForm.parameters.flowParameters.approveUrl = String.format(regulatoryTrainingForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(regulatoryTrainingForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    regulatoryTrainingForm.parameters.flowParameters.confirmDateUrl = String.format(regulatoryTrainingForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(regulatoryTrainingForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	   	regulatoryTrainingForm.parameters.flowParameters.assingRoomUrl = String.format(regulatoryTrainingForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(regulatoryTrainingForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    regulatoryTrainingForm.parameters.flowParameters.toCorrectUrl = String.format(regulatoryTrainingForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(regulatoryTrainingForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    regulatoryTrainingForm.parameters.flowParameters.confirmUrl = String.format(regulatoryTrainingForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(regulatoryTrainingForm.parameters.forms)[5],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	   		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":regulatoryTrainingForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
			    "numeroTelefono":spForms.loadedFields.NumeroTelefono.value,
			    "manual": spForms.loadedFields.Manual.value,
			    "cantidadColaboradores": spForms.loadedFields.CantidadColaboradores.value,
			    "fechaPropuesta": spForms.loadedFields.FechaPropuestaEntrenamieto.value,
			    "fechaVencimiento": spForms.loadedFields.FechaVencimiento.value,
			    "fechaEntrenamiento": spForms.loadedFields.FechaEntrenamiento.value,
			    "fechaEntrenamiento": spForms.loadedFields.FechaEntrenamiento.value,
			    "nombreEntrenador": spForms.loadedFields.NombreEntrenador.value,
			    "area":spForms.loadedFields.Area0.value,
			    "sala": spForms.loadedFields.Sala.value,
			    "estacion":textSelected
		    },
		    "state":spForms.loadedFields.EstadoEntrenamientoRegulatorio.value,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":regulatoryTrainingForm.parameters.flowParameters.listNameAssing,
		    "viewUrl":regulatoryTrainingForm.parameters.flowParameters.viewUrl,
		    "approveUrl":regulatoryTrainingForm.parameters.flowParameters.approveUrl,
		    "confirmDateUrl":regulatoryTrainingForm.parameters.flowParameters.confirmDateUrl,
		    "assingRoomUrl":regulatoryTrainingForm.parameters.flowParameters.assingRoomUrl,
		    "toCorrectUrl":regulatoryTrainingForm.parameters.flowParameters.toCorrectUrl,
		   	"subjectTitle":	regulatoryTrainingForm.parameters.flowParameters.subjectTitle
		}
			utilities.http.callRestService({
			url:regulatoryTrainingForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity = 0;
				document.querySelector('.form-content').style.opacity = 1;
				document.querySelector('.loader-container').style.height = 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){			
		var partsOfUrl = window.location.href.split('?')[0].split('/');
		regulatoryTrainingForm.parameters.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];
		eval(regulatoryTrainingForm.parameters.forms[regulatoryTrainingForm.parameters.global.formName]);
	},
	loadApprove:function(){	
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:regulatoryTrainingForm.parameters.formsParameters.buttons.Aprove.Text,
				id:regulatoryTrainingForm.parameters.formsParameters.buttons.Aprove.Id,
				success:function (){
					regulatoryTrainingForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return regulatoryTrainingForm.validateApprove()
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:regulatoryTrainingForm.parameters.formsParameters.buttons.Reject.Text,
				id:regulatoryTrainingForm.parameters.formsParameters.buttons.Reject.Id,
				success:function (){
					regulatoryTrainingForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return regulatoryTrainingForm.validateReject()
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:regulatoryTrainingForm.parameters.formsParameters.buttons.Cancel.Text,
				id:regulatoryTrainingForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		regulatoryTrainingForm.parameters.formsParameters.description = regulatoryTrainingForm.parameters.formsParameters.descriptionApprove;
		regulatoryTrainingForm.parameters.formsParameters.title = regulatoryTrainingForm.parameters.formsParameters.titleApprove;
		regulatoryTrainingForm.loadEdit(actions,regulatoryTrainingForm.loadResponsableState);
	},
	loadAssingRoom:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:regulatoryTrainingForm.parameters.formsParameters.buttons.Send.Text,
				id:regulatoryTrainingForm.parameters.formsParameters.buttons.Send.Id,
				success:function (){
					regulatoryTrainingForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					if(regulatoryTrainingForm.validateRoom()){
						return regulatoryTrainingForm.validateApprove()
					}
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:regulatoryTrainingForm.parameters.formsParameters.buttons.Cancel.Text,
				id:regulatoryTrainingForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		regulatoryTrainingForm.parameters.formsParameters.description = regulatoryTrainingForm.parameters.formsParameters.descriptionRoom;
		regulatoryTrainingForm.parameters.formsParameters.title = regulatoryTrainingForm.parameters.formsParameters.titleRoom;
        regulatoryTrainingForm.loadEdit(actions,regulatoryTrainingForm.loadResponsableState);
	},
	loadAssignDate:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:regulatoryTrainingForm.parameters.formsParameters.buttons.Send.Text,
				id:regulatoryTrainingForm.parameters.formsParameters.buttons.Send.Id,
				success:function (){
					regulatoryTrainingForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					if (regulatoryTrainingForm.validateAssignDate()){
						return regulatoryTrainingForm.validateApprove()
					}
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:regulatoryTrainingForm.parameters.formsParameters.buttons.Cancel.Text,
				id:regulatoryTrainingForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		regulatoryTrainingForm.parameters.formsParameters.description = regulatoryTrainingForm.parameters.formsParameters.descriptionDate;
		regulatoryTrainingForm.parameters.formsParameters.title = regulatoryTrainingForm.parameters.formsParameters.titleDate;
        regulatoryTrainingForm.loadEdit(actions,regulatoryTrainingForm.loadResponsableState);

	},
	loadToCorrect:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:regulatoryTrainingForm.parameters.formsParameters.buttons.ToCorrect.Text,
				id:regulatoryTrainingForm.parameters.formsParameters.buttons.ToCorrect.Id,
				success:function (){
					regulatoryTrainingForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					if (regulatoryTrainingForm.validateFormat()){
						return regulatoryTrainingForm.validateApprove();
					}
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:regulatoryTrainingForm.parameters.formsParameters.buttons.Cancel.Text,
				id:regulatoryTrainingForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		regulatoryTrainingForm.parameters.formsParameters.description = regulatoryTrainingForm.parameters.formsParameters.descriptionToCorrect;
		regulatoryTrainingForm.parameters.formsParameters.title = regulatoryTrainingForm.parameters.formsParameters.titleToCorrect;
        regulatoryTrainingForm.loadEdit(actions,regulatoryTrainingForm.loadResponsableState);

	},
	loadEdit:function(actions,sucessFunction){
		spForms.options = {	
			actions:actions,
			title:regulatoryTrainingForm.parameters.formsParameters.title,
			description:regulatoryTrainingForm.parameters.formsParameters.description,
			contentTypeName:regulatoryTrainingForm.parameters.formsParameters.contentType,
			containerId:regulatoryTrainingForm.parameters.formsParameters.containerId,
			allowAttachments:true,
		  	requiredAttachments:false,
		  	maxAttachments:20,
		  	minAttachments:0,
		  	maxSizeInMB:3,
			successLoadEditForm:function(){	
				regulatoryTrainingForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
				regulatoryTrainingForm.parameters.formsIsLoaded = true;
				regulatoryTrainingForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(regulatoryTrainingForm.parameters.formats.dateFormat);
				if (regulatoryTrainingForm.parameters.global.formName == "ConfirmarFecha"){
					$("#"+ regulatoryTrainingForm.parameters.internalNameFields.FechaEntrenamiento).data("DateTimePicker").minDate(new Date());
				}
				if(regulatoryTrainingForm.parameters.global.formName == "Corregir"){
					regulatoryTrainingForm.loadManual();
					regulatoryTrainingForm.setDates();
				}
				if(typeof sucessFunction== "function"){
					sucessFunction();
				}
			} 
		};
		spForms.loadEditForm();
	},
	setDates: function(){
		$("#"+ regulatoryTrainingForm.parameters.internalNameFields.FechaVencimiento).data("DateTimePicker").minDate(new Date());
		document.getElementById(regulatoryTrainingForm.parameters.internalNameFields.FechaVencimiento).value =  moment.utc(spForms.loadedFields.FechaVencimiento.value).format(regulatoryTrainingForm.parameters.formats.dateFormat);
		
		$("#"+ regulatoryTrainingForm.parameters.internalNameFields.FechaPropuestaEntrenamieto).data("DateTimePicker").minDate(new Date());
		document.getElementById(regulatoryTrainingForm.parameters.internalNameFields.FechaPropuestaEntrenamieto).value =  moment.utc(spForms.loadedFields.FechaPropuestaEntrenamieto.value).format(regulatoryTrainingForm.parameters.formats.dateFormat);
	},
	loadManual:function(){
		try {
			//running the query with parameters
			sputilities.getItems(regulatoryTrainingForm.parameters.listNameManual,"",function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				regulatoryTrainingForm.parameters.resultManual = resultItems;
				var ctrlInterval = setInterval(function(){
					if(regulatoryTrainingForm.parameters.formsIsLoaded)
					{
						regulatoryTrainingForm.loadCurso();
						clearInterval(ctrlInterval);
					}
				},500);
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	loadCurso:function(){
		try {
			//running the query with parameters
			sputilities.getItems(regulatoryTrainingForm.parameters.listNameCurso,"",function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				regulatoryTrainingForm.parameters.resultCurso = resultItems;
				var ctrlInterval = setInterval(function(){
					if(regulatoryTrainingForm.parameters.formsIsLoaded)
					{
						regulatoryTrainingForm.setValuesSelect();
						regulatoryTrainingForm.addOnChangeCOA();
						regulatoryTrainingForm.setOptionSelected();
						clearInterval(ctrlInterval);
					}
				},500);
										
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	setValuesSelect: function(){
		try {
			var objectSelect = parseInt(document.querySelector(regulatoryTrainingForm.parameters.fieldsHTML.COASelect).value);
			var htmlOption = "";
			for (var i=0; i < regulatoryTrainingForm.parameters.resultCurso.length; i++)
			{
				if (regulatoryTrainingForm.parameters.resultCurso[i].COAId == objectSelect)
				{
					htmlOption += String.format(regulatoryTrainingForm.parameters.htmlOptionCursos,regulatoryTrainingForm.parameters.resultCurso[i].Id,regulatoryTrainingForm.parameters.resultCurso[i].Title);
				}
			}
			
			regulatoryTrainingForm.insertItemsHTML(htmlOption,regulatoryTrainingForm.parameters.htmlCurso,regulatoryTrainingForm.parameters.internalNameFields.TipoCursoSelect);
			htmlOption = "";
			
			for (var i=0; i < regulatoryTrainingForm.parameters.resultManual.length; i++)
			{
				if (regulatoryTrainingForm.parameters.resultManual[i].COAId == objectSelect)
				{
					htmlOption += String.format(regulatoryTrainingForm.parameters.htmlOptionManual,regulatoryTrainingForm.parameters.resultManual[i].Id,regulatoryTrainingForm.parameters.resultManual[i].Title);
				}
			}
			regulatoryTrainingForm.insertItemsHTML(htmlOption,regulatoryTrainingForm.parameters.htmlManual,regulatoryTrainingForm.parameters.internalNameFields.ManualSelect);

		}
		catch (e){
			console.log(e);
		}
	},
	setOptionSelected: function(){
		var ctrlInterval = setInterval(function(){
			if(regulatoryTrainingForm.parameters.selectIsLoaded)
			{
				var manualSelect = document.querySelector(regulatoryTrainingForm.parameters.fieldsHTML.ManualSelect);
				var tipoSelect = document.querySelector(regulatoryTrainingForm.parameters.fieldsHTML.ManualSelect);
				var manualtext = document.getElementById(regulatoryTrainingForm.parameters.internalNameFields.Manual).value;
				var tipotext = document.getElementById(regulatoryTrainingForm.parameters.internalNameFields.TipoCurso).value;
	
				for (var i = 0; i < manualSelect.options.length; i++) {
				    if (manualSelect.options[i].text === manualtext) {
				        manualSelect.selectedIndex = i;
				        break;
				    }
				}
				for (var i = 0; i < tipoSelect.options.length; i++) {
				    if (tipoSelect.options[i].text === tipotext) {
				        tipoSelect.selectedIndex = i;
				        break;
				    }
				}
				clearInterval(ctrlInterval);
			}
		},500);
	},
	addOnChangeCOA: function(){
		document.querySelector(regulatoryTrainingForm.parameters.fieldsHTML.COA).onchange = function() {
		
			var ctrlCursos = document.getElementById(regulatoryTrainingForm.parameters.internalNameFields.TipoCursoSelect);
			var ctrlManual = document.getElementById(regulatoryTrainingForm.parameters.internalNameFields.ManualSelect);
					
			if(ctrlCursos != null && ctrlManual != null){
				ctrlCursos.parentNode.removeChild(ctrlCursos);
				ctrlManual.parentNode.removeChild(ctrlManual);
			}
			regulatoryTrainingForm.setValuesSelect();
		}
	},
	insertItemsHTML: function(hmtlOption,htmlElement,idElement){
		var htmlSelect = String.format(htmlElement,hmtlOption);
		var element = document.createElement('div');
		element.id = idElement;
		element.innerHTML = htmlSelect;
		var typeElement = document.querySelector(regulatoryTrainingForm.parameters.fieldsHTML.COA);
		typeElement.parentNode.insertBefore(element,typeElement.nextSibling);
		regulatoryTrainingForm.parameters.selectIsLoaded = true;
	},
	setValuesForm:function(){
		var manual = document.getElementById(regulatoryTrainingForm.parameters.internalNameFields.Manual);
		var cursos = document.getElementById(regulatoryTrainingForm.parameters.internalNameFields.TipoCurso);
		
		var objectSelectManual = document.querySelector(regulatoryTrainingForm.parameters.fieldsHTML.ManualSelect);
		var textSelectedManual = objectSelectManual.options[objectSelectManual.selectedIndex].text;
		
		var objectSelectCursos = document.querySelector(regulatoryTrainingForm.parameters.fieldsHTML.CursosSelect);
		var textSelectedCursos = objectSelectCursos.options[objectSelectCursos.selectedIndex].text;

		if (manual != null && cursos != null)
		{
			manual.value = textSelectedManual;
			cursos.value = textSelectedCursos;
		}
	},
	validateRequired:function(){
		var comentariosRow = document.querySelector(regulatoryTrainingForm.parameters.fieldsHTML.ObservacionesResponsable);
		var isValid = true;
		
		if(comentariosRow != null)
		{
			var comentarios = comentariosRow.querySelector(regulatoryTrainingForm.parameters.errorInvalid.areaComments);
	
			if(comentarios.value != "")
			{
				comentariosRow.removeAttribute(regulatoryTrainingForm.parameters.invalid);
			}
			else
			{
				isValid = false;
				comentariosRow.setAttribute(regulatoryTrainingForm.parameters.invalid,regulatoryTrainingForm.parameters.errorInvalid.text);			
			}	
		}
		else
		{
			isValid = false;
		}
		return isValid;
	},
	validateAssignDate: function(){
		var nombreEntrenadorRow = document.querySelector(regulatoryTrainingForm.parameters.fieldsHTML.NombreEntrenador);
		var isValid = true;
		
		if(nombreEntrenadorRow != null)
		{
			var nombreEntrenador = nombreEntrenadorRow.querySelector(regulatoryTrainingForm.parameters.errorInvalid.area);

			if(nombreEntrenador.value != "")
			{
				nombreEntrenadorRow.removeAttribute(regulatoryTrainingForm.parameters.invalid);
			}
			else
			{
				isValid = false;
				nombreEntrenadorRow.setAttribute(regulatoryTrainingForm.parameters.invalid,regulatoryTrainingForm.parameters.errorInvalid.text);			
			}	
		}
		else
		{
			isValid = false;
		}
		return isValid;
	},
	validateRoom: function(){
		var SalaRow = document.querySelector(regulatoryTrainingForm.parameters.fieldsHTML.Sala);
		var isValid = true;
		
		if(SalaRow != null)
		{
			var Sala = SalaRow.querySelector(regulatoryTrainingForm.parameters.errorInvalid.area);

			if(Sala.value != "")
			{
				SalaRow.removeAttribute(regulatoryTrainingForm.parameters.invalid);
			}
			else
			{
				isValid = false;
				SalaRow.setAttribute(regulatoryTrainingForm.parameters.invalid,regulatoryTrainingForm.parameters.errorInvalid.text);			
			}	
		}
		else
		{
			isValid = false;
		}
		return isValid;
	},
	validateApprove:function(){
		var state = document.getElementById(regulatoryTrainingForm.parameters.internalNameFields.EstadoEntrenamientoRegulatorio);
		var isValid = true;
		if(regulatoryTrainingForm.parameters.initialState == regulatoryTrainingForm.parameters.codeStates.ConfirmacionEntrenamiento)
		{			
			if(state!= null)
			{
				state.value = regulatoryTrainingForm.parameters.codeStates.ConfirmarFecha;
			}
			else
			{
				isValid = false;
			}
		}
		if(regulatoryTrainingForm.parameters.initialState == regulatoryTrainingForm.parameters.codeStates.Corregir)
		{			
			if(state!= null)
			{
				state.value = regulatoryTrainingForm.parameters.codeStates.ConfirmacionEntrenamiento;
				regulatoryTrainingForm.setValuesForm();
			}
			else
			{
				isValid = false;
			}
		}

		if(regulatoryTrainingForm.parameters.initialState == regulatoryTrainingForm.parameters.codeStates.ConfirmarFecha)
		{			
			if(state!= null){
				state.value = regulatoryTrainingForm.parameters.codeStates.AsignarSala;
			}
			else
			{
				isValid = false;
			}
		}
		if(regulatoryTrainingForm.parameters.initialState == regulatoryTrainingForm.parameters.codeStates.AsignarSala)
		{		
			if(state!= null)
			{
				state.value = regulatoryTrainingForm.parameters.codeStates.Finalizada;
			}
			else
			{
				isValid = false;
			}
		}	
		return isValid;		
	},
	validateReject:function(){
		var row = document.querySelector(regulatoryTrainingForm.parameters.fieldsHTML.ObservacionesResponsable);
		var state = document.getElementById(regulatoryTrainingForm.parameters.internalNameFields.EstadoEntrenamientoRegulatorio);		
		var isValid = true;
		
		if(row != null && state != null)
		{
			var comments = row.querySelector(regulatoryTrainingForm.parameters.errorInvalid.areaComments);
			if(comments.value != "")
			{
				row.removeAttribute(regulatoryTrainingForm.parameters.invalid);
				if(regulatoryTrainingForm.parameters.initialState == regulatoryTrainingForm.parameters.codeStates.ConfirmacionEntrenamiento)
				{
					state.value = regulatoryTrainingForm.parameters.codeStates.Corregir;
				}
				else
				{
					state.value = regulatoryTrainingForm.parameters.codeStates.ConfirmarFecha;

				}			
			}
			else
			{
				row.setAttribute(regulatoryTrainingForm.parameters.invalid,regulatoryTrainingForm.parameters.errorInvalid.text);			
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;

	},
	loadResponsableState:function(){	
		try
		{	
			var objectSelect = document.querySelector(regulatoryTrainingForm.parameters.fieldsHTML.Estacion);
			var textSelected = objectSelect.options[objectSelect.selectedIndex].text;
			var userCurrent = _spPageContextInfo.userEmail;
			var state = document.getElementById(regulatoryTrainingForm.parameters.internalNameFields.EstadoEntrenamientoRegulatorio);
			if(state != null)
			{
				if(state.value == regulatoryTrainingForm.parameters.formsState[regulatoryTrainingForm.parameters.global.formName])
				{
					if(state.value == regulatoryTrainingForm.parameters.codeStates.ConfirmacionEntrenamiento || state.value == regulatoryTrainingForm.parameters.codeStates.ConfirmarFecha)
					{
						var query = String.format(regulatoryTrainingForm.parameters.query,textSelected);
						sputilities.getItems(regulatoryTrainingForm.parameters.listaNameDL,query,function(data){
							
							var resultItems = JSON.parse(data).d.results;
							if(resultItems.length > 0)
							{
								var items = resultItems[0].Responsable.EMail;
								var find = false;
								if(items == userCurrent){
									find = true;
								}
								
								regulatoryTrainingForm.parameters.initialState = state.value;
								
								if(state.value == regulatoryTrainingForm.parameters.codeStates.ConfirmacionEntrenamiento ||  state.value == regulatoryTrainingForm.parameters.codeStates.ConfirmarFecha)
								{				
									if(find)
									{
										regulatoryTrainingForm.showComments(state.value);
									}
									else
									{
										regulatoryTrainingForm.showComments(state.value);
										regulatoryTrainingForm.hideButtons(state.value);
										regulatoryTrainingForm.showPopUp();
									}
								}
							}						
						});
					}
					else
					if (state.value == regulatoryTrainingForm.parameters.codeStates.Corregir || state.value == regulatoryTrainingForm.parameters.codeStates.AsignarSala)
					{	
						userCurrent = _spPageContextInfo.userId;
						var find = false;
						if(userCurrent == spForms.currentItem.AuthorId)
						{
							find = true;
						}
						
						regulatoryTrainingForm.parameters.initialState = state.value;
						
						if(find)
						{
							regulatoryTrainingForm.showComments(state.value);
						}
						else
						{
							regulatoryTrainingForm.showComments(state.value);
							regulatoryTrainingForm.hideButtons(state.value);
							regulatoryTrainingForm.showPopUp();
						}
					}
				}
				else
				{
					regulatoryTrainingForm.showComments(state.value);
					regulatoryTrainingForm.hideButtons(state.value);
					regulatoryTrainingForm.showPopUp();
				}
			}
		}
		catch(e){
			console.log(e);
		}	
	},
	hideButtons:function(state){
	
		if(state == regulatoryTrainingForm.parameters.codeStates.ConfirmacionEntrenamiento)
		{
			var bottonApp = document.getElementById(regulatoryTrainingForm.parameters.controlsHtml.Enviar);
			if(bottonApp != null)
			{
				bottonApp.style.display = regulatoryTrainingForm.parameters.htmlRequiered.none;
			}
		}
		else if(state == regulatoryTrainingForm.parameters.codeStates.ConfirmarFecha)
		{
			var bottonApp = document.getElementById(regulatoryTrainingForm.parameters.controlsHtml.Enviar);
			if(bottonApp != null)
			{
				bottonApp.style.display = regulatoryTrainingForm.parameters.htmlRequiered.none;
			}		
		}
		else if(state == regulatoryTrainingForm.parameters.codeStates.Corregir)
		{
			var bottonApp = document.getElementById(regulatoryTrainingForm.parameters.controlsHtml.Enviar);
			if(bottonApp != null)
			{
				bottonApp.style.display = regulatoryTrainingForm.parameters.htmlRequiered.none;
			}		
		}
		else if(state == regulatoryTrainingForm.parameters.codeStates.AsignarSala)
		{
			var bottonApp = document.getElementById(regulatoryTrainingForm.parameters.controlsHtml.Enviar);
			if(bottonApp != null)
			{
				bottonApp.style.display = regulatoryTrainingForm.parameters.htmlRequiered.none;
			}		
		}
		else if(state == regulatoryTrainingForm.parameters.codeStates.ConfirmarFecha)
		{
			var bottonApp = document.getElementById(regulatoryTrainingForm.parameters.controlsHtml.Enviar);
			if(bottonApp != null)
			{
				bottonApp.style.display = regulatoryTrainingForm.parameters.htmlRequiered.none;
			}		
		}

	},
	showPopUp:function(){
		var html = String.format(regulatoryTrainingForm.parameters.formats.htmlNotAllowed,regulatoryTrainingForm.parameters.bodyItem);
		var formContent = document.querySelector(regulatoryTrainingForm.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}	
	},
	validateEmail: function (email){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);
	},
	validateFormat:function(){
		var rowEmail = document.querySelector(regulatoryTrainingForm.parameters.fieldsHTML.CorreoElectronico);
		var email = document.getElementById(regulatoryTrainingForm.parameters.internalNameFields.CorreoElectronico);
		var isValid = true;		
		if(email != null && rowEmail!= null)
		{			
			if(regulatoryTrainingForm.validateEmail(email.value) || email.value == "")
			{
				rowEmail.removeAttribute(regulatoryTrainingForm.parameters.invalid);
			}
			else
			{
				rowEmail.setAttribute(regulatoryTrainingForm.parameters.invalid,regulatoryTrainingForm.parameters.errorInvalidCorreo);
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;
	},
	showComments:function(state){
		if(state == regulatoryTrainingForm.parameters.codeStates.ConfirmacionEntrenamiento)
		{
			var comments = document.querySelector(regulatoryTrainingForm.parameters.fieldsHTML.ObservacionesResponsable);	
			if(comments != null)
			{
				comments.setAttribute(regulatoryTrainingForm.parameters.hidden,false);
				comments.querySelector(regulatoryTrainingForm.parameters.errorInvalid.areaComments).value="";
	
			}
		}
		else if(state == regulatoryTrainingForm.parameters.codeStates.ConfirmarFecha)
		{
			var comments = document.querySelector(regulatoryTrainingForm.parameters.fieldsHTML.ObservacionesResponsable);	
			if(comments != null)
			{
				comments.setAttribute(regulatoryTrainingForm.parameters.hidden,false);
			}
		}
		else if(state == regulatoryTrainingForm.parameters.codeStates.Corregir)
		{
			var comments = document.querySelector(regulatoryTrainingForm.parameters.fieldsHTML.ObservacionesResponsable);	
			if(comments != null)
			{
				comments.setAttribute(regulatoryTrainingForm.parameters.hidden,false);
			}
		}
		else if(state == regulatoryTrainingForm.parameters.codeStates.AsignarSala)
		{
			var comments = document.querySelector(regulatoryTrainingForm.parameters.fieldsHTML.ObservacionesResponsable);	
			if(comments != null)
			{
				comments.setAttribute(regulatoryTrainingForm.parameters.hidden,false);
			}
		}
	}
}
regulatoryTrainingForm.initialize();