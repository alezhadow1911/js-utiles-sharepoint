﻿var trainingForm = {
	parameters:{
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']"
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico'			
		},
		resultItems:null,
		invalid:"sp-invalid",
		formsIsLoaded:false,
		formsParameters:{
			title:"Solicitud de Entrenamiento BI",
			description:"",
			buttons:{		
				Cancel:{Text:"Cancelar",Id:"cancelar"}
			},
			contentType:"Tipo de Contenido TI",
			containerId:"container-form"			
		}

	},	
	initialize:function(){
		
		spForms.loadEditForm();
	}
	
}

spForms.options={
	actions:[
		{
			type:spForms.parameters.cancelAction,
			name:trainingForm.parameters.formsParameters.buttons.Cancel.Text,
			id:trainingForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:trainingForm.parameters.formsParameters.title,
	description:trainingForm.parameters.formsParameters.description,
	contentTypeName:trainingForm.parameters.formsParameters.contentType,
	containerId:trainingForm.parameters.formsParameters.containerId,
	allowAttachments:false,
  	requiredAttachments:false,
  	maxAttachments:20,
  	minAttachments:0,
  	maxSizeInMB:3,
	successLoadEditForm:function(){		
	} 
};
trainingForm.initialize();