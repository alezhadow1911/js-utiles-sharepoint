﻿var legalizationForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		query:"?$Select=FlujoAsociado/Title,ResponsablesAbonos/EMail,ID&$expand=FlujoAsociado,ResponsablesAbonos&$filter=Title eq '{0}'",
		listName:"ResponsablesTH",
		assingTo:{
			Ventanilla:"Legalizacion Cesantias RT Ventanilla TH",
			Novedades:"Legalizacion Cesantias RT Novedades"
		},
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']",
			Comentarios:"[sp-static-name='ComentariosTH']"			
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico',	
			Comentarios:"ComentariosTH",			
			Estado:"EstadoCesantias"		
		},		
		controlsHtml:{
			Attachment:"sp-forms-add-attachment",
			Aprobar:"btn-aprobar",
			Corregir:"btn-corregir",
			Aplicar:"btn-aplicar",
			Rechazar:"btn-rechazar",
			classDelete:".delete-doc",
			classGroup:".form-group.row",
			colorDisable:"#B1B1B1",
			borderDisable:"#E1E1E1",
			iconTH:"ico-help-ComentariosTH"			
		},
		errorInvalid:{text:"Los comentarios son obligatorios",area:"textarea"},
		htmlRequiered:{label:"label",block:"block",none:"none"},		
		invalid:"sp-invalid",
		hidden:"sp-hidden",		
		resultItems:null,	
		codeStates:{Solicitado:"0",Ventanilla:"1",Novedades:"2",Aplicada:"3",Corregir:"4"},	
		userProperties:"CellPhone",
		bodyItem:"<div class='form-group row'><label>Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior</label></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
		containerForm:".form-content",
		TitlePopUp:"Acceso Denegado",
		formsParameters:{
			title:"",
			description:"",
			titleApprove:"Aprobar/ Corregir Solicitud Legalización Cesantías Régimen Tradicional",
			titleAplicate:"Aplicar Solicitud Legalización Cesantías Régimen Tradicional",
			descriptionApprove:"Completa la información correspondiente, para aprobar o rechazar la solicitud",
			descriptionAplicate:"Revisa la información de la solicitud, para aplicar la novedad",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Legalizacion Cesantias Regimen Tradicional",
			containerId:"container-form"			
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"legalizationForm.loadApprove()",			
			"AplicarSolicitud":"legalizationForm.loadApplicate()",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Novedades":"Vista Novedades",
			"Ventanilla":"Vista Ventanilla"
		},
		flowParameters:{
			urlFlow:"https://prod-51.westus.logic.azure.com:443/workflows/3899e1250acf47d9b6071cb68e6a3e27/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=vM8QzKS8t9Ouxs72sE0B3eWFavLpqV87qPi2cazzgsQ",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",
	   		approveBossUrl:"",
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Legalizacion Cesantias RT Ventanilla TH",
	    	applyUsers:"Legalizacion Cesantias RT Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:17,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Legalización Cesantías Régimen Tradicional"
		},
		formsState:{
			AprobarSolicitud:"1",			
			AplicarSolicitud:"2"
		},
		global:{
			formName:null,
			createdBy:null,
			created:null

		},
		initialState:null

	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
	    legalizationForm.parameters.flowParameters.approveUrl = String.format(legalizationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(legalizationForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);			   
	    legalizationForm.parameters.flowParameters.applyUrl = String.format(legalizationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(legalizationForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    legalizationForm.parameters.flowParameters.editUrl = String.format(legalizationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(legalizationForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    legalizationForm.parameters.flowParameters.listViewApply = String.format(legalizationForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(legalizationForm.parameters.forms)[5]);
	    legalizationForm.parameters.flowParameters.listViewApprove= String.format(legalizationForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(legalizationForm.parameters.forms)[6]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":legalizationForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":legalizationForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosTH.value		       
		    },
		    "state":spForms.loadedFields.EstadoCesantias.value,
		    "approveUsers":legalizationForm.parameters.flowParameters.approveUsers,
		    "applyUsers":legalizationForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":legalizationForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":legalizationForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":legalizationForm.parameters.flowParameters.listNameAssing,
		    "editUrl":legalizationForm.parameters.flowParameters.editUrl,
		    "approveUrl":legalizationForm.parameters.flowParameters.approveUrl,
		    "applyUrl":legalizationForm.parameters.flowParameters.applyUrl,		    
		    "listViewApply":legalizationForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":legalizationForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	legalizationForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:legalizationForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){			
		var partsOfUrl = window.location.href.split('?')[0].split('/');
		legalizationForm.parameters.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];
		eval(legalizationForm.parameters.forms[legalizationForm.parameters.global.formName]);

	},
	loadApprove:function(){	
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:legalizationForm.parameters.formsParameters.buttons.Aprove.Text,
				id:legalizationForm.parameters.formsParameters.buttons.Aprove.Id,
				success:function (){
					legalizationForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return legalizationForm.validateApprove()
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:legalizationForm.parameters.formsParameters.buttons.Revise.Text,
				id:legalizationForm.parameters.formsParameters.buttons.Revise.Id,
				success:function (){
					legalizationForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return legalizationForm.validateReject()
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:legalizationForm.parameters.formsParameters.buttons.Cancel.Text,
				id:legalizationForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		legalizationForm.parameters.formsParameters.description = legalizationForm.parameters.formsParameters.descriptionApprove;
		legalizationForm.parameters.formsParameters.title = legalizationForm.parameters.formsParameters.titleApprove;
		var query = String.format(legalizationForm.parameters.query,legalizationForm.parameters.assingTo.Ventanilla);
		legalizationForm.parameters.query = query;	
		legalizationForm.loadEdit(actions,legalizationForm.loadResponsableState);
		
	},	
	loadApplicate:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:legalizationForm.parameters.formsParameters.buttons.Aplicate.Text,
				id:legalizationForm.parameters.formsParameters.buttons.Aplicate.Id,
				success:function (){
					legalizationForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return legalizationForm.validateApprove()
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:legalizationForm.parameters.formsParameters.buttons.Cancel.Text,
				id:legalizationForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		legalizationForm.parameters.formsParameters.description = legalizationForm.parameters.formsParameters.descriptionAplicate;
		legalizationForm.parameters.formsParameters.title = legalizationForm.parameters.formsParameters.titleAplicate;
		var query = String.format(legalizationForm.parameters.query,legalizationForm.parameters.assingTo.Novedades);
		legalizationForm.parameters.query = query;
		legalizationForm.loadEdit(actions,legalizationForm.loadResponsableState);

	},
	loadEdit:function(actions,sucessFunction){
		spForms.options={	
			actions:actions,
			title:legalizationForm.parameters.formsParameters.title,
			description:legalizationForm.parameters.formsParameters.description,
			contentTypeName:legalizationForm.parameters.formsParameters.contentType,
			containerId:legalizationForm.parameters.formsParameters.containerId,
			allowAttachments:true,
		  	requiredAttachments:true,
		  	maxAttachments:20,
		  	minAttachments:1,
		  	maxSizeInMB:3,
			successLoadEditForm:function(){	
				legalizationForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
				legalizationForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(legalizationForm.parameters.formats.dateFormat);			
				if(typeof sucessFunction== "function"){
					sucessFunction();
				}
			} 
		};
		spForms.loadEditForm();
	},
	validateApprove:function(){
		var row = document.querySelector(legalizationForm.parameters.fieldsHTML.Comentarios);
		var state = document.getElementById(legalizationForm.parameters.internalNameFields.Estado);
		var isValid = true;
		if(legalizationForm.parameters.initialState == legalizationForm.parameters.codeStates.Ventanilla)
		{			
			if(row != null && state!= null)
			{
				row.removeAttribute(legalizationForm.parameters.invalid);
				state.value = legalizationForm.parameters.codeStates.Novedades;
			}
			else
			{
				isValid = false;
			}
		}		
		if(legalizationForm.parameters.initialState == legalizationForm.parameters.codeStates.Novedades)
		{	
			if(state!= null){				
				state.value = legalizationForm.parameters.codeStates.Aplicada;
			}
			else
			{
				isValid = false;
			}
		}
		return isValid;
		
	},
	validateReject:function(){
		var row = document.querySelector(legalizationForm.parameters.fieldsHTML.Comentarios);
		var state = document.getElementById(legalizationForm.parameters.internalNameFields.Estado);		
		var isValid = true;
		
		if(row != null && state != null)
		{
			var comments = row.querySelector(legalizationForm.parameters.errorInvalid.area);
			if(comments.value != "")
			{
				row.removeAttribute(legalizationForm.parameters.invalid);				
				state.value = legalizationForm.parameters.codeStates.Corregir;							
			}
			else
			{
				row.setAttribute(legalizationForm.parameters.invalid,legalizationForm.parameters.errorInvalid.text);			
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;

	},	
	loadResponsableState:function(){	
		try
		{
			var userCurrent = _spPageContextInfo.userEmail
			var state = document.getElementById(legalizationForm.parameters.internalNameFields.Estado);
			if(state != null)
			{
				if(state.value == legalizationForm.parameters.formsState[legalizationForm.parameters.global.formName])
				{
					if(state.value == legalizationForm.parameters.codeStates.Novedades || state.value == legalizationForm.parameters.codeStates.Ventanilla)
					{
						sputilities.getItems(legalizationForm.parameters.listName,legalizationForm.parameters.query,function(data){
							
							var resultItems = JSON.parse(data).d.results;
							if(resultItems.length > 0)
							{
								var items = resultItems[0].ResponsablesAbonos.results
								var find = false;
								for(var i=0;i<items.length;i++)
								{
									if(items[i].EMail == userCurrent){
										find = true;
									 	break;
									}
								}
								
								legalizationForm.parameters.initialState = state.value;
								
								if(state.value == legalizationForm.parameters.codeStates.Novedades)
								{				
									if(find)
									{
										legalizationForm.showComments(state.value);
									}
									else
									{										
										legalizationForm.hideNotAllowed();									
									}
								}
								else if(state.value == legalizationForm.parameters.codeStates.Ventanilla)
								{						
									if(find)
									{
										legalizationForm.showComments(state.value);
									}
									else
									{										
										legalizationForm.hideNotAllowed();
										
									}
								}
							}						
						});
					}					
                }
                else
                {                    
                    legalizationForm.hideNotAllowed();                   
                }
		    }
		}
		catch(e){
			console.log(e);
		}	
	},
	hideNotAllowed:function(){
		var html = legalizationForm.parameters.bodyItem;
		var formContent = document.querySelector(legalizationForm.parameters.containerForm);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	},
	hideButtons:function(state){
	
		if(state == legalizationForm.parameters.codeStates.Novedades)
		{
			var bottonApp = document.getElementById(legalizationForm.parameters.controlsHtml.Aplicar);
			if(bottonApp != null)
			{
				bottonApp.style.display = legalizationForm.parameters.htmlRequiered.none;
			}
		}
		else if(state == legalizationForm.parameters.codeStates.Ventanilla)
		{
			var bottonApp = document.getElementById(legalizationForm.parameters.controlsHtml.Aprobar);
			var bottonRej = document.getElementById(legalizationForm.parameters.controlsHtml.Corregir);
			var comments  = document.getElementById(legalizationForm.parameters.internalNameFields.Comentarios);
			var icon = document.getElementById(legalizationForm.parameters.controlsHtml.iconTH);
			
			if(bottonApp != null && bottonRej != null && comments != null && icon != null)
			{
				comments.style.pointerEvents = legalizationForm.parameters.htmlRequiered.none;
				icon.style.pointerEvents = legalizationForm.parameters.htmlRequiered.none;
				comments.style.color = legalizationForm.parameters.controlsHtml.colorDisable;
				comments.style.borderColor = legalizationForm.parameters.controlsHtml.borderDisable;	
				bottonApp.style.display = legalizationForm.parameters.htmlRequiered.none;
				bottonRej.style.display = legalizationForm.parameters.htmlRequiered.none;

			}
		
		}		
	},	
	showComments:function(state){
		if(state == legalizationForm.parameters.codeStates.Ventanilla)
		{		
			var comments = document.querySelector(legalizationForm.parameters.fieldsHTML.Comentarios);	
			if(comments != null)
			{
				comments.setAttribute(legalizationForm.parameters.hidden,false);				
				comments.querySelector(legalizationForm.parameters.errorInvalid.area).value="";	
			}
		}
		else if(state == legalizationForm.parameters.codeStates.Novedades)
		{			
			var comments = document.querySelector(legalizationForm.parameters.fieldsHTML.Comentarios);	
			if(comments != null)
			{
				comments.setAttribute(legalizationForm.parameters.hidden,false);				
			}
		}
	}
}
legalizationForm.initialize();