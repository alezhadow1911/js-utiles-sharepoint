﻿var legalizationForm= {
	parameters:{
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']",
			Comentarios:"[sp-static-name='ComentariosTH']"
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico'			
		},
		resultItems:null,
		invalid:"sp-invalid",
		hidden:"sp-hidden",		
		userProperties:"CellPhone", 
		errorInvalid:"El correo electrónico no tiene el formato correcto",
		loadUserInfo:"legalizationForm.loadUserInfo",
		formsIsLoaded:false,
		formsParameters:{
			title:"Solicitud Legalización Cesantías Régimen Tradicional",
			description:"",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Legalizacion Cesantias Regimen Tradicional",
			containerId:"container-form"				
		}

	},	
	initialize:function(){
		
		spForms.loadEditForm();
	},
	showComments:function(){
				
		var comments = document.querySelector(legalizationForm.parameters.fieldsHTML.Comentarios);	
		if(comments != null)
		{
			comments.setAttribute(legalizationForm.parameters.hidden,false);								
		}		
	}	
}

spForms.options={
	actions:[
		{
			type:spForms.parameters.cancelAction,
			name:legalizationForm.parameters.formsParameters.buttons.Cancel.Text,
			id:legalizationForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:legalizationForm.parameters.formsParameters.title,
	description:legalizationForm.parameters.formsParameters.description,
	contentTypeName:legalizationForm.parameters.formsParameters.contentType,
	containerId:legalizationForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:20,
  	minAttachments:1,
  	maxSizeInMB:3,
	successLoadEditForm:function(){
		legalizationForm.showComments();		
	} 
};
legalizationForm.initialize();