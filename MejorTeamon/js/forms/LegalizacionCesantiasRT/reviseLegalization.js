﻿var legalizationForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']",
			Comentarios:"[sp-static-name='ComentariosTH']",
			ComentariosJefe:"[sp-static-name='ComentariosJefe']"		
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico',	
			Comentarios:"ComentariosTH",
			Estado:"EstadoCesantias"		
		},		
		controlsHtml:{
            Attachment:"sp-forms-add-attachment",
            Enviar:"btn-enviar",            
			Aprobar:"btn-aprobar",
			Corregir:"btn-corregir",
			Aplicar:"btn-aplicar",
			Rechazar:"btn-rechazar",
			classDelete:".delete-doc",
			classGroup:".form-group.row",
			colorDisable:"#B1B1B1",
			borderDisable:"#E1E1E1",
			iconTH:"ico-help-ComentariosTH",
			iconJefe:"ico-help-ComentariosJefe"
		},		
		bodyItem:"<div class='form-group row'><label>Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior</label></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
		containerForm:".form-content",
		TitlePopUp:"Acceso Denegado",
    	htmlRequiered:{block:"block",none:"none"},	            
		invalid:"sp-invalid",
		userProperties:"CellPhone", 
		errorInvalid:"El correo electrónico no tiene el formato correcto",
        codeStates:{Solicitado:"0",Ventanilla:"1",Novedades:"2",Aplicada:"3",Corregir:"4",Jefe:"5",Rechazado:"6"},		
		formsParameters:{
			title:"Corregir Solicitud Legalización Cesantías Régimen Tradicional",
			description:"Completa la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Legalizacion Cesantias Regimen Tradicional",
			containerId:"container-form"			
		},		
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud",			
			"AplicarSolicitud":"Aplicar Solicitud",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Novedades":"Vista Novedades",
			"Ventanilla":"Vista Ventanilla"
		},
		flowParameters:{
			urlFlow:"https://prod-51.westus.logic.azure.com:443/workflows/3899e1250acf47d9b6071cb68e6a3e27/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=vM8QzKS8t9Ouxs72sE0B3eWFavLpqV87qPi2cazzgsQ",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",
	   		approveBossUrl:"",
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Legalizacion Cesantias RT Ventanilla TH",
	    	applyUsers:"Legalizacion Cesantias RT Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:17,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Legalización Cesantías Régimen Tradicional"
		},
		global:{		
			createdBy:null,
			created:null
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
	    legalizationForm.parameters.flowParameters.approveUrl = String.format(legalizationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(legalizationForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    legalizationForm.parameters.flowParameters.applyUrl = String.format(legalizationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(legalizationForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    legalizationForm.parameters.flowParameters.editUrl = String.format(legalizationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(legalizationForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    legalizationForm.parameters.flowParameters.listViewApply = String.format(legalizationForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(legalizationForm.parameters.forms)[5]);
	    legalizationForm.parameters.flowParameters.listViewApprove= String.format(legalizationForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(legalizationForm.parameters.forms)[6]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":legalizationForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":legalizationForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosTH.value		        
		    },
		    "state":spForms.loadedFields.EstadoCesantias.value,
		    "approveUsers":legalizationForm.parameters.flowParameters.approveUsers,
		    "applyUsers":legalizationForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":legalizationForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":legalizationForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":legalizationForm.parameters.flowParameters.listNameAssing,
		    "editUrl":legalizationForm.parameters.flowParameters.editUrl,
		    "approveUrl":legalizationForm.parameters.flowParameters.approveUrl,
		    "applyUrl":legalizationForm.parameters.flowParameters.applyUrl,		    
		    "listViewApply":legalizationForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":legalizationForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	legalizationForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:legalizationForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){
		
		spForms.loadEditForm();
	},
	validateFormat:function(){
		var rowEmail = document.querySelector(legalizationForm.parameters.fieldsHTML.CorreoElectronico);
		var email = document.getElementById(legalizationForm.parameters.internalNameFields.CorreoElectronico);
		var isValid = true;		
		if(email != null && rowEmail!= null)
		{			
			if(legalizationForm.validateEmail(email.value) || email.value == "")
			{
				rowEmail.removeAttribute(legalizationForm.parameters.invalid);
			}
			else
			{
				rowEmail.setAttribute(legalizationForm.parameters.invalid,legalizationForm.parameters.errorInvalid);
			}
		}
		else
		{
			isValid=false;
		}
		
		var state = document.getElementById(legalizationForm.parameters.internalNameFields.Estado);
		if(state != null)
		{
			state.value = legalizationForm.parameters.codeStates.Ventanilla;
		}
		else
		{
			isValid = false;
		}
		return isValid;

	},
	validateEmail: function (email){
		 var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);
	
	},	
	hideNotAllowed:function(){
		var html = legalizationForm.parameters.bodyItem;
		var formContent = document.querySelector(legalizationForm.parameters.containerForm);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	},
	loadResponsableState:function(){
	
		try
		{
			var currentUserId =_spPageContextInfo.userId;		
		
			var find = false;
			var state = document.getElementById(legalizationForm.parameters.internalNameFields.Estado);
			if(state != null)
			{
				if(spForms.currentItem.AuthorId == currentUserId){
					find = true;
				}
								
				if(state.value != legalizationForm.parameters.codeStates.Corregir)
				{				
					legalizationForm.hideNotAllowed();
				}
				else
				{
					if(!find)
					{
						legalizationForm.hideNotAllowed();
					}
				}
				
			}
		}
		catch(e){
			console.log(e);
		}	
	}
}

spForms.options={
	actions:[
		{
			type:spForms.parameters.updateAction,
			name:legalizationForm.parameters.formsParameters.buttons.Send.Text,
			id:legalizationForm.parameters.formsParameters.buttons.Send.Id,
			success:function (){
				legalizationForm.callFlowService();
				return false;
			},
			error:function (){},
			preSave: function (){
				return legalizationForm.validateFormat()
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:legalizationForm.parameters.formsParameters.buttons.Cancel.Text,
			id:legalizationForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:legalizationForm.parameters.formsParameters.title,
	description:legalizationForm.parameters.formsParameters.description,
	contentTypeName:legalizationForm.parameters.formsParameters.contentType,
	containerId:legalizationForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:20,
  	minAttachments:1,
  	maxSizeInMB:3,
	successLoadEditForm:function(){
		legalizationForm.loadResponsableState();
		legalizationForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
		legalizationForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(legalizationForm.parameters.formats.dateFormat);

	} 
};
legalizationForm.initialize();