﻿var legalizationForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']"
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico'			
		},
		resultItems:null,
		invalid:"sp-invalid",
		userProperties:"CellPhone", 
		errorInvalid:"El correo electrónico no tiene el formato correcto",
		loadUserInfo:"legalizationForm.loadUserInfo",
		formsIsLoaded:false,
		formsParameters:{
			title:"Solicitud Legalización Cesantías Régimen Tradicional",
			description:"Completa la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Legalizacion Cesantias Regimen Tradicional",
			containerId:"container-form"			
		},
		flowParameters:{
			urlFlow:"https://prod-51.westus.logic.azure.com:443/workflows/3899e1250acf47d9b6071cb68e6a3e27/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=vM8QzKS8t9Ouxs72sE0B3eWFavLpqV87qPi2cazzgsQ",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",
	   		approveBossUrl:"",
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Legalizacion Cesantias RT Ventanilla TH",
	    	applyUsers:"Legalizacion Cesantias RT Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:17,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Legalización Cesantías Régimen Tradicional"
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud Ventanilla TH",			
			"AplicarSolicitud":"Aplicar Novedad",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Novedades":"Vista Novedades",
			"Ventanilla":"Vista Ventanilla"
		},
		global:{
			createdBy:null,
			created:null
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
	    legalizationForm.parameters.flowParameters.approveUrl = String.format(legalizationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(legalizationForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);			    
	    legalizationForm.parameters.flowParameters.applyUrl = String.format(legalizationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(legalizationForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    legalizationForm.parameters.flowParameters.editUrl = String.format(legalizationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(legalizationForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    legalizationForm.parameters.flowParameters.listViewApply = String.format(legalizationForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(legalizationForm.parameters.forms)[5]);
	    legalizationForm.parameters.flowParameters.listViewApprove= String.format(legalizationForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(legalizationForm.parameters.forms)[6]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":legalizationForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":legalizationForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosTH.value		       
		    },
		    "state":spForms.loadedFields.EstadoCesantias.value,
		    "approveUsers":legalizationForm.parameters.flowParameters.approveUsers,
		    "applyUsers":legalizationForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":legalizationForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":legalizationForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":legalizationForm.parameters.flowParameters.listNameAssing,
		    "editUrl":legalizationForm.parameters.flowParameters.editUrl,
		    "approveUrl":legalizationForm.parameters.flowParameters.approveUrl,
		    "applyUrl":legalizationForm.parameters.flowParameters.applyUrl,		    
		    "listViewApply":legalizationForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":legalizationForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	legalizationForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:legalizationForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){
		sputilities.callFunctionOnLoadBody(legalizationForm.parameters.loadUserInfo);
		spForms.loadNewForm();
	},
	
	loadUserInfo:function(){
		try {
			//running the query with parameters
			sputilities.getMyUserProfileProperties("",function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d;
				legalizationForm.parameters.resultItems = resultItems;
				var ctrlInterval = setInterval(function(){
					if(legalizationForm.parameters.formsIsLoaded)
					{
						legalizationForm.setValuesUserInfo();
						clearInterval(ctrlInterval);
					}
				},500);
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	setValuesUserInfo:function(){
	
		if(legalizationForm.parameters.resultItems != null)
		{
		  	var name = document.getElementById(legalizationForm.parameters.internalNameFields.NombrePersona);
		  	var email = document.getElementById(legalizationForm.parameters.internalNameFields.CorreoElectronico);
		  	var cellPhone = document.getElementById(legalizationForm.parameters.internalNameFields.NumeroCelular);
		  	if(name != null){
		  		name.value = legalizationForm.parameters.resultItems.DisplayName ? legalizationForm.parameters.resultItems.DisplayName: "";
		  	}
		  	
		  	if(email != null){
		  		email.value = legalizationForm.parameters.resultItems.Email ? (legalizationForm.parameters.resultItems.Email).toLowerCase(): "";
		  	}
		  	
		  	if(cellPhone != null){
		  	
		  		var userProperties = legalizationForm.parameters.resultItems.UserProfileProperties.results;
		  	
			  	for(var i=0;i<userProperties.length;i++)
			  	{
			  		if(userProperties[i].Key == legalizationForm.parameters.userProperties)
			  		{
			  		 	cellPhone.value =  userProperties[i].Value;
			  		 	break;
			  		}
			  	}
			}
	  	}
	},
	validateFormat:function(){
		var rowEmail = document.querySelector(legalizationForm.parameters.fieldsHTML.CorreoElectronico);
		var email = document.getElementById(legalizationForm.parameters.internalNameFields.CorreoElectronico);
		var isValid = true;		
		if(email != null && rowEmail!= null)
		{			
			if(legalizationForm.validateEmail(email.value) || email.value == "")
			{
				rowEmail.removeAttribute(legalizationForm.parameters.invalid);
			}
			else
			{
				rowEmail.setAttribute(legalizationForm.parameters.invalid,legalizationForm.parameters.errorInvalid);
			}
		}
		else
		{
			isValid=false;
		}
		return isValid;
		
	},
	validateEmail: function (email){
		 var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);
	
	}
}

spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:legalizationForm.parameters.formsParameters.buttons.Send.Text,
			id:legalizationForm.parameters.formsParameters.buttons.Send.Id,
			success:function (){
				legalizationForm.callFlowService();
				return false;
			},
			error:function (){},
			preSave: function (){
				return legalizationForm.validateFormat()
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:legalizationForm.parameters.formsParameters.buttons.Cancel.Text,
			id:legalizationForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:legalizationForm.parameters.formsParameters.title,
	description:legalizationForm.parameters.formsParameters.description,
	contentTypeName:legalizationForm.parameters.formsParameters.contentType,
	containerId:legalizationForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:20,
  	minAttachments:1,
  	maxSizeInMB:3,
	successLoadNewForm:function(){
		legalizationForm.parameters.global.createdBy = sputilities.contextInfo().userEmail;
		legalizationForm.parameters.global.created = moment.utc(new Date()).format(legalizationForm.parameters.formats.dateFormat);
		legalizationForm.parameters.formsIsLoaded = true;
	} 
};
legalizationForm.initialize();