﻿var ctrlForm = {
	parameters:{
		formats:{
			"queryStaticNameRow":"[sp-static-name='{0}']",
			"htmlNotAllowed":"<div><h4>{0}</h4></div> <div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
			"querySelectRadio":'input[type="radio"][value="{0}"]:checked',
			"queryRadio":'input[type="radio"][value="{0}"]',
			"querySelectCheckbox":'input[type="checkbox"][value="{0}"]:checked',
			"queryCheckbox":'input[type="checkbox"][value="{0}"]',
			"emailUsers":"{0};",
			"dateFormat":"DD-MM-YYYY",
			"dateCustomFormat":"{1}{0}{2}{0}{3}",
			"formFormat":"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}"
		},
		forms:{
			"NewForm":"ctrlForm.redirectCustomForm('NuevaSolicitud')",
			"EditForm":"ctrlForm.redirectCustomForm('AprobarSolicitud')",
			"DispForm":"ctrlForm.redirectCustomForm('VerSolicitud')",
			"NuevaSolicitud":"ctrlForm.loadNew()",
			"AprobarSolicitud":"ctrlForm.loadApprove()",			
			"VerSolicitud":"ctrlForm.loadReview()"			
		},
		messages:{
			"idSaveForm":"enviarSolicitud",
			"idCancelForm":"cancelar",			
			"idApproveForm":"aprobar",
			"idRejectForm":"solicitarCorreccion",			
			"textSaveForm":"Enviar",
			"textCancelForm":"Cancelar",			
			"textApproveForm":"Aprobar",
			"textRejectForm":"Rechazar",
			"textApplyForm":"Novedad Aplicada",
			"textNotAllowed":"Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior",
			"textEquipmentOnloan":"Por lo menos uno de los equipos solicitados no esta disponible en este rango de fechas",
			"textApproveError":"A la fecha por lo menos uno de los equipos solicitados no esta disponible ó la solicitud a caducado",
			"textRequest":"Dependerá de la disponibilidad del equipo de comunicaciones",
			"textErrorDate":"",
		},
		columns:{
			"comments":{
				"staticName":"ComentariosRE"
			},
			"displayName":{
				"staticName":"NombrePersona1"
			},			
			"email":{
				"staticName":"CorreoElectronico"
			},			
			"startDate":{
				"staticName":"FechaSolicitud"
			},
			"endDate":{
				"staticName":"FechaDevolucion"
			},
			"location":{
				"staticName":"Ubicacion"
			},
			"equipment":{
				"idAuxField":"checkList-Equipo",
				"staticName":"Equipo",
				"selectHtml":"<div id='checkList-Equipo' class='list-checkbox-radio'></div>"
			},
			"requestHelp":{
				"staticName":"RequiereApoyo",
				"staticDiv":"div[sp-static-name='RequiereApoyo'] #help-block-RequiereApoyo"
			},
			"basicKnowledge":{
				"staticName":"ConocimientoBasico"
			}
		},
		states:[
			"Solicitado",
			"Revision",
			"Aprobado",
			"Rechazado"
		],
		parametersHTML:{
			"spHidden":"sp-hidden",
			"spDisable":"sp-disable",
			"spInvalid":"sp-invalid",
			"classError":"has-error",
			"block":"help-block",
			"hideBlock":"help-block forms-hide-element"
		},
		selectorFormContent:".form-content",
		cellPhone:"CellPhone",
		functionLoadMyProperties:"ctrlForm.loadMyProperties",
		containerId:"container-form",
		description:"Complete la información para la solicitud",
		descriptionApprove:"Revise la información, apruebe o rechaze segùn corresponda",
		descriptionReview:"Resultado de la solicitud",
		contentTypeName:"Equipos Audiovisuales",
		formIsLoaded:false,
		allowAttachments:false,
		requiredAttachments:false,
		maxAttachments:20,
		minAttachments:0,
		maxSizeInMB:3,
		listNameConsecutive:"ControlRadicados",
		idConsecutive:1,
		listUsersFlow:"ResponsablesRE",
		listNameInventory:"Inventario Equipos Audiovisuales",
		listWaitingList:"Solicitudes Pendientes",
		viewUrl:"",
	    approveUrl:"",
	    queryUsers:"?$filter=(FlujoAsociadoId eq 1)&$select=ResponsablesAbonos/EMail,EtapaResponsable,Ubicacion&$expand=ResponsablesAbonos",
		queryInventory:"?$select=Title,Ubicacion,Estado,Devolucion&$filter=Ubicacion eq '{0}'",
		queryWaitingInventory:"?$select=Title,Ubicacion,FechaPrestamo,FechaVencimiento&$filter=Ubicacion eq '{0}'",
	    flowUrl:"https://prod-45.westus.logic.azure.com:443/workflows/42cbe95054c84811bbb3947398964002/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=sI-p1QPQFAsnGMAa-No2kN0mIT2_RErsdwCEYjB5_a4"
	    
	},
	results:{
		inventory:null,
		awaitingRequest:null		
	},
	global:{
		formName:null,
		createdBy:null,
		created:null,
		approveUsers:{},
		equipmentRequest:'',		
		location:""
	},
	initialize:function(){
		var partsOfUrl = window.location.href.split('?')[0].split('/');
		ctrlForm.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];		
		eval(ctrlForm.parameters.forms[ctrlForm.global.formName]);
	},
	callFlowService:function(){
	
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
	
		ctrlForm.parameters.viewUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[5],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.approveUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":ctrlForm.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "fechaCreacion":ctrlForm.global.created,
		        "comentarios":spForms.loadedFields.ComentariosRE.value,
		        "fechaSolicitud":moment.utc(new Date(spForms.loadedFields.FechaSolicitud.value)).format(ctrlForm.parameters.formats.dateFormat),
				"fechaDevolucion":moment.utc(new Date(spForms.loadedFields.FechaDevolucion.value)).format(ctrlForm.parameters.formats.dateFormat),
				"equipmentRequest":ctrlForm.global.equipmentRequest,
				"location":ctrlForm.global.location
			},			
		    "state":spForms.loadedFields.EstadoSolicitudEquiposAudiovisua.value,
		    "approveUsers":ctrlForm.global.approveUsers[ctrlForm.global.location],		    
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":ctrlForm.parameters.listNameConsecutive,
		    "idConsecutive":ctrlForm.parameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
		    "viewUrl":ctrlForm.parameters.viewUrl,
		    "approveUrl":ctrlForm.parameters.approveUrl		    
		}
		utilities.http.callRestService({
			url:ctrlForm.parameters.flowUrl,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	getUsers:function(){		
		sputilities.getItems(ctrlForm.parameters.listUsersFlow,ctrlForm.parameters.queryUsers,
			function(data){
				var data = utilities.convertToJSON(data);
				if(data != null){
					var items = data.d.results;					
					for(var i = 0; i < items.length ;i++){
						var item = items[i];
						var location = item.Ubicacion;	
						var stringUsers = "";					
						for(var j= 0; j < item.ResponsablesAbonos.results.length;j++){
							var user = item.ResponsablesAbonos.results[j];												
							if(item.EtapaResponsable == ctrlForm.parameters.states[1]){
								stringUsers += String.format(ctrlForm.parameters.formats.emailUsers,user.EMail);
							}
						}
						ctrlForm.global.approveUsers[location] = stringUsers;						
					}	
				}
			},
			function(xhr){
				console.log(xhr);
			}
		);
	},
	changeState:function(state){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio,state));
		if(radio != null){
			radio.click();
		}
	},
	hideNotAllowed:function(){
		var html = String.format(ctrlForm.parameters.formats.htmlNotAllowed,ctrlForm.parameters.messages.textNotAllowed);
		var formContent = document.querySelector(ctrlForm.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	},
	validateAllUsers:function(){
		if(spForms.currentItem.AuthorId != sputilities.contextInfo().userId){
			ctrlForm.hideNotAllowed();
			return false;
		}
	},
	validateApproveUser:function(){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[1])) == null;
		if(ctrlForm.global.approveUsers[ctrlForm.global.location].split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
	},
	hideComments:function(){
		var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.comments.staticName));
		if(ctrlComments != null){
			ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,true);
		}
	},
	loadReview:function(){
		var actions = [
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionReview;
		ctrlForm.loadEdit(actions,ctrlForm.validateAllUsers);
	},	
	loadApprove:function(){
		var actions = [
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textApproveForm,
				id:ctrlForm.parameters.messages.idApproveForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					ctrlForm.validateAvailablity(true);
					spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
					ctrlForm.changeState(ctrlForm.parameters.states[2]);
					return true;					
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textRejectForm,
				id:ctrlForm.parameters.messages.idRejectForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					ctrlForm.setOrRemoveErrorToControl(ctrlForm.parameters.columns.comments.staticName,ctrlForm.parameters.messages.textApproveError,false);					
					spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
					ctrlForm.changeState(ctrlForm.parameters.states[3]);
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
		ctrlForm.loadEdit(actions,ctrlForm.validateApproveUser);

	},	
	loadEdit:function(actions,callBack){
		ctrlForm.getUsers();
		spForms.options={
			actions:actions,
			description:ctrlForm.parameters.description,
			contentTypeName:ctrlForm.parameters.contentTypeName,
			containerId:ctrlForm.parameters.containerId,
			allowAttachments:ctrlForm.parameters.allowAttachments,
		  	requiredAttachments:ctrlForm.parameters.requiredAttachments,
		  	maxAttachments:ctrlForm.parameters.maxAttachments,
		  	minAttachments:ctrlForm.parameters.minAttachments,
		  	maxSizeInMB:ctrlForm.parameters.maxSizeInMB,
			successLoadEditForm:function(){	
				ctrlForm.insertAuxiliarFields();				
				ctrlForm.hideRelationshipFields();
				ctrlForm.getEquipmentInventory();
				ctrlForm.getWaitingRequestInventory();		
				ctrlForm.global.createdBy = spForms.currentItem.Author.EMail;
				ctrlForm.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(ctrlForm.parameters.formats.dateFormat);
				ctrlForm.formIsLoaded = true;				
				if(typeof callBack== "function"){
					callBack();
				}
			} 
		};
		spForms.loadEditForm();

	},
	loadNew:function(){	
		ctrlForm.getUsers();
		ctrlForm.global.createdBy = sputilities.contextInfo().userEmail;
		ctrlForm.global.created = moment.utc(new Date()).format(ctrlForm.parameters.formats.dateFormat);
		sputilities.callFunctionOnLoadBody(ctrlForm.parameters.functionLoadMyProperties);
		ctrlForm.loadNewForm();
	},
	loadNewForm:function(){
		spForms.options={
			actions:[
				{
					type:spForms.parameters.saveAction,
					name:ctrlForm.parameters.messages.textSaveForm,
					id:ctrlForm.parameters.messages.idSaveForm,
					success:function (){
						ctrlForm.callFlowService();
						return false;
					},
					error:function (){return true;},
					preSave: function (){
						ctrlForm.validateAvailablity(false);
						ctrlForm.changeState(ctrlForm.parameters.states[0]);
						return true;
					}
				},
				{
					type:spForms.parameters.cancelAction,
					name:ctrlForm.parameters.messages.textCancelForm,
					id:ctrlForm.parameters.messages.idCancelForm,
					callback:null
				},
			],
			description:ctrlForm.parameters.description,
			contentTypeName:ctrlForm.parameters.contentTypeName,
			containerId:ctrlForm.parameters.containerId,
			allowAttachments:ctrlForm.parameters.allowAttachments,
		  	requiredAttachments:ctrlForm.parameters.requiredAttachments,
		  	maxAttachments:ctrlForm.parameters.maxAttachments,
		  	minAttachments:ctrlForm.parameters.minAttachments,
		  	maxSizeInMB:ctrlForm.parameters.maxSizeInMB,
			successLoadNewForm:function(){
				ctrlForm.formIsLoaded = true;	
				ctrlForm.getEquipmentInventory();
				ctrlForm.getWaitingRequestInventory();			
				ctrlForm.setEventsToFields();
				ctrlForm.insertAuxiliarFields();				
				ctrlForm.hideRelationshipFields();
				ctrlForm.CheckKnowledge();								
				ctrlForm.hideComments();
			} 
		};
		spForms.loadNewForm();
	},
	loadMyProperties:function(){
		sputilities.getUserProfileProperties(sputilities.contextInfo().userEmail,
			function(userInfo){
				var objUser = utilities.convertToJSON(userInfo);
				if(objUser != null){
					var displayName = objUser.d.DisplayName;
					var email = objUser.d.Email;
					var cellPhone = ctrlForm.findProperty(objUser,ctrlForm.parameters.cellPhone);
					var validateFormIsLoaded = setInterval(function(){
						if(ctrlForm.formIsLoaded){
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.displayName.staticName,displayName);
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.email.staticName,email);							
							clearInterval(validateFormIsLoaded);
						}
					},500);
				}
			},
			function(xhr){
				console.log(xhr)
			}
		);
	},
	setValueToControl:function(staticName,value){
		if(value != null){
			var ctrl = document.getElementById(staticName);
			if(ctrl != null){
				ctrl.value = value;
			}
		}
	},
	findProperty:function(objUser,name){
		var properties = objUser.d.UserProfileProperties.results;
		for(var i = 0; i < properties.length; i++){
			var property = properties[i];
			if(property.Key == name){
				return property.Value;
			}
		}
	},
	redirectCustomForm:function(name){
		window.location.href = window.location.href.replace(ctrlForm.global.formName,name);
	},	
	validateAvailablity:function(approveMode){
		var selectedItems = document.querySelectorAll('[name="Equipo"]:checked');
		var startDate = moment(document.getElementById(ctrlForm.parameters.columns.startDate.staticName).value.trim(),'DD/MM/YYYY hh:mm A').toDate();
		var endDate = moment(document.getElementById(ctrlForm.parameters.columns.endDate.staticName).value.trim(),'DD/MM/YYYY hh:mm A').toDate();
		var validDate = ctrlForm.validateDateCorrespondence(startDate,endDate);
		var areAbailable = true;
		
		if(validDate){
			var stringItems = "";
			for(var i = 0;i<selectedItems.length;i++){
				var selectedItem = selectedItems[i];
				var itemName = selectedItem.value;								
				var isAvailableInventory = ctrlForm.validateInInventoryList(itemName,startDate,endDate);
				var isAvailableWaiting = ctrlForm.validateInWaitingList(itemName,startDate,endDate);
				if(!isAvailableInventory || !isAvailableWaiting){
					areAbailable = false;
					ctrlForm.parameters.messages.textErrorDate = ctrlForm.parameters.messages.textEquipmentOnloan;					
				}
				stringItems += String.format(ctrlForm.parameters.formats.emailUsers,selectedItem.value);
			}			
		}else{
			areAbailable = false;			
		}
		//validate result by status
		switch(true){
			case(approveMode && areAbailable):
				ctrlForm.setOrRemoveErrorToControl(ctrlForm.parameters.columns.comments.staticName,ctrlForm.parameters.messages.textApproveError,false);
			break;
			case(approveMode && !areAbailable):
				ctrlForm.setOrRemoveErrorToControl(ctrlForm.parameters.columns.comments.staticName,ctrlForm.parameters.messages.textApproveError,true);
			break;
			case(!approveMode):
				ctrlForm.setRequestEquipmentChecked(areAbailable,stringItems);
			break
		}	
	},
	setRequestEquipmentChecked:function(areAbailable,stringItems){
		var isError = true;
		if(areAbailable){	
			isError = false;
		}	
		ctrlForm.setOrRemoveErrorToControl(ctrlForm.parameters.columns.startDate.staticName,ctrlForm.parameters.messages.textErrorDate,isError);
		ctrlForm.setOrRemoveErrorToControl(ctrlForm.parameters.columns.endDate.staticName,ctrlForm.parameters.messages.textErrorDate,isError);
		ctrlForm.global.equipmentRequest = stringItems;
		ctrlForm.setValueToControl(ctrlForm.parameters.columns.equipment.staticName,stringItems);
	},
	validateInWaitingList:function(itemName,startDate,endDate){	
		var areAvailable = true;	
		var filteredList = ctrlForm.results.awaitingRequest.filter(function(obj){
			if(obj.Title == itemName){
				return true
			}
			return false
		});
		if(filteredList.length > 0){			
			for(var j = 0;j<filteredList.length;j++){
				var item = filteredList[j];
				var expirationDate = new Date(item.FechaVencimiento);
				var loanDate = new Date(item.FechaPrestamo)
				switch(true){
					case(startDate >= loanDate && startDate <= expirationDate):
					case(endDate >= loanDate && startDate <= expirationDate):
					areAvailable = false;
					break;
					case(startDate < loanDate && endDate > expirationDate):
					areAvailable = false;											
					break;
				}
			}
		}
		return areAvailable;		
	},
	validateInInventoryList:function(itemName,startDate,endDate){		
		var filteredList = ctrlForm.results.inventory.filter(function(obj){
			if(obj.Title == itemName){
				return true
			}
			return false
		});
		if(filteredList.length > 0){
			var areAvailable = true;
			for(var j = 0;j<filteredList.length;j++){
				var item = filteredList[j];
				var returnDate = new Date(item.Devolucion)
				if(item.Estado != "Disponible" && startDate <= returnDate){
					areAvailable = false;
				}
			}
		}
		return areAvailable;
	},
	validateDateCorrespondence:function(startDate,endDate){
		var today =  new Date();	
		switch(true){
			case(startDate > endDate):
				ctrlForm.parameters.messages.textErrorDate = "La fecha de devolución no puede ser anterior a la fecha de solicitud";
				return false
			break;
			case(startDate <= today):
				ctrlForm.parameters.messages.textErrorDate = "La fecha de la solicitud debe ser posterior a la fecha actual";
				return false
			break;
			case(endDate <= today):
				ctrlForm.parameters.messages.textErrorDate = "La fecha de devolucion debe ser posterior a la fecha actual";
				return false
			break;
			default:
				return true
			break;
		}
		
	},
	hideRelationshipFields:function(){
		var requestHelp = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.requestHelp.staticName));
		var requestHelpCheck = document.getElementById(ctrlForm.parameters.columns.requestHelp.staticName);
		requestHelp.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,false);
		requestHelpCheck.checked = false;
		var apoyoInput = document.getElementById(ctrlForm.parameters.columns.requestHelp.staticName);
		if(apoyoInput != null){
				apoyoInput.addEventListener('change',function(event){
				if(requestHelpCheck.checked == true){
				var staticInput = document.querySelector(ctrlForm.parameters.columns.requestHelp.staticDiv);
				staticInput.className=ctrlForm.parameters.parametersHTML.block;
				staticInput.textContent = ctrlForm.parameters.messages.textRequest;
				}else{
				var staticInput= document.querySelector(ctrlForm.parameters.columns.requestHelp.staticDiv);
				staticInput.className=ctrlForm.parameters.parametersHTML.hideBlock;
				}
				}
			)}

	},
	CheckKnowledge:function(){
		var requestKnowledge = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.basicKnowledge.staticName));
		var requestHelpCheck = document.getElementById(ctrlForm.parameters.columns.basicKnowledge.staticName);
		//requestHelp.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,false);
		requestHelpCheck.checked = true;
		
	},
	setEventsToFields:function(){
	  //change location field
	 	var locationList = document.getElementById(ctrlForm.parameters.columns.location.staticName);
	  	locationList.onchange = function(){
				ctrlForm.getEquipmentInventory();
				ctrlForm.getWaitingRequestInventory();
				console.log(ctrlForm.results.inventory);
				console.log(ctrlForm.results.awaitingRequest)
			}
	  //select level knowledge
		var knowledge = document.getElementById(ctrlForm.parameters.columns.basicKnowledge.staticName);
		var requestHelpCheck = document.getElementById(ctrlForm.parameters.columns.requestHelp.staticName);
		var requestHelp = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.requestHelp.staticName));
		knowledge.onchange = function(){
			if(knowledge.checked){
				requestHelpCheck.checked = false;
				requestHelp.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,true);
			}else{
				requestHelpCheck.checked = true;
				requestHelp.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,false);
			}
		}	  
	},
	insertAuxiliarFields:function(){
		//auxiliar equipment list
		var equipmentField = document.getElementById(ctrlForm.parameters.columns.equipment.staticName);
		equipmentField.insertAdjacentHTML('afterend',ctrlForm.parameters.columns.equipment.selectHtml);
		ctrlForm.getEquipmentInventory();
	},	
	getEquipmentInventory:function(){
		var location = ctrlForm.getLocationSelected();		
		var queryEquipment = String.format(ctrlForm.parameters.queryInventory, location);
		sputilities.getItems(ctrlForm.parameters.listNameInventory,queryEquipment,function(data){
				var results = utilities.convertToJSON(data).d.results;	
				ctrlForm.results.inventory = results			
				var checkCollection = "";
				if(results != null){				
					for(var i = 0;i<results.length;i++){
						var item = results[i];
						var title = item.Title;
						var itemHtml = String.format(spForms.parameters.fieldType.multichoice.htmlCheckBox,i,ctrlForm.parameters.columns.equipment.staticName,title);
						checkCollection += itemHtml;
					}
					document.getElementById(ctrlForm.parameters.columns.equipment.idAuxField).innerHTML = checkCollection;
					ctrlForm.getEquipmentCheckedEditForm();													
				}
		},function(data){
			console.log(data)
		});		
	},
	getWaitingRequestInventory:function(){
		var location = ctrlForm.getLocationSelected();
		var queryEquipment = String.format(ctrlForm.parameters.queryWaitingInventory, location);
		sputilities.getItems(ctrlForm.parameters.listWaitingList,queryEquipment,function(data){
			var results = utilities.convertToJSON(data).d.results;
			ctrlForm.results.awaitingRequest = results;
		},function(data){
			console.log(utilities.convertToJSON(data))
		})
	},	
	getLocationSelected:function(){
		var locationList = document.getElementById(ctrlForm.parameters.columns.location.staticName);
		var locationIndex = locationList.value;
		var locationName = locationList[locationIndex].innerText;
		ctrlForm.global.location = locationName;
		return locationName;
	},
	setOrRemoveErrorToControl:function(ctrlName,errorMssg,isError){
		var rowForm = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlName));		
		if(isError){						
			rowForm.setAttribute(ctrlForm.parameters.parametersHTML.spInvalid,errorMssg);
		}else{
			rowForm.removeAttribute(ctrlForm.parameters.parametersHTML.spInvalid)
		}	
			
	},
	getEquipmentCheckedEditForm:function(){
		var listItems = document.getElementById(ctrlForm.parameters.columns.equipment.staticName).value.split(';');
		if(listItems.length != 1){
			for(var i=0;i<listItems.length;i++){
				var item = listItems[i];
				if(document.querySelector(String.format(ctrlForm.parameters.formats.queryCheckbox,item)) != null){				
					document.querySelector(String.format(ctrlForm.parameters.formats.queryCheckbox,item)).checked = true
				}
			}
		}
	}
}
ctrlForm.initialize();


