﻿var Cuentas = {
	//create parameters
	parameters:{
			IdEnviar: "Enviar",
			IdCancelar:"Cancelar",
			ContentType:"CuentasAFC",
			IdDiv:"spForm",
			TitleForm:"Nuevo Cuentas AFC Y AFP",
			IDSolicitud: "TipoSolicitud",
			fields:[{field:"NombrePersona1"},{field:"NumeroCelular"},{field:"CorreoElectronico"}],
				
	htmlRequiered:{
			label:'label',
			StaticName:	"sp-static-name",
			Position:"beforeend",
			IconRequired:"<i>*</i>",
						//event Select
			StaticNameBanco:'[sp-static-name="Banco"]',
			StaticNameCuenta:'[sp-static-name="NumeroCuenta"]',
			StaticNamePrimera:'[sp-static-name="Valordescontarenla1eraQuincena"]',
			StaticNameSegunda:'[sp-static-name="Valordescontarenla2daQuincena"]',
			StaticNameTotal:'[sp-static-name="TotalValordescontar"]',	
			StaticAttachment:'[sp-static-name="sp-forms-attachment"]'						
		},
	Style:{
		Block: "block",
		None: "none",
		//Atributtes
		Invalid:"sp-invalid",
		Message:"Formato de correo invalido"	
	},
		ID:{
		Correo: "CorreoElectronico",	
	    Primera:'Valordescontarenla1eraQuincena',
		Segunda:'Valordescontarenla2daQuincena',
		Total:'TotalValordescontar'

	},

},
	fieldType:{
	 "0":{
	    	Actualizar:function(){
	    	var Banco = document.querySelector(Cuentas.parameters.htmlRequiered.StaticNameBanco);			    	
	    	var Cuenta = document.querySelector(Cuentas.parameters.htmlRequiered.StaticNameCuenta);	
	    	var Primera = document.querySelector(Cuentas.parameters.htmlRequiered.StaticNamePrimera);	
	    	var Segunda = document.querySelector(Cuentas.parameters.htmlRequiered.StaticNameSegunda);
	    	var Total = document.querySelector(Cuentas.parameters.htmlRequiered.StaticNameTotal);		    	
			Banco.style.display = Cuentas.parameters.Style.Block;
			Cuenta.style.display = Cuentas.parameters.Style.Block;	
			Primera.style.display = Cuentas.parameters.Style.Block;	
			Segunda.style.display = Cuentas.parameters.Style.Block;		
			Total.style.display = Cuentas.parameters.Style.Block;	
	       	}
    },
       "1":{
	    	Actualizar:function(){
	    	var Banco = document.querySelector(Cuentas.parameters.htmlRequiered.StaticNameBanco);			    	
	    	var Cuenta = document.querySelector(Cuentas.parameters.htmlRequiered.StaticNameCuenta);	
			Banco.style.display = Cuentas.parameters.Style.None;
			Cuenta.style.display = Cuentas.parameters.Style.None;	
			spForms.loadedFields[Banco.getAttribute(Cuentas.parameters.htmlRequiered.StaticName)].required=false
			spForms.loadedFields[Cuenta.getAttribute(Cuentas.parameters.htmlRequiered.StaticName)].required=false
			var Primera = document.querySelector(Cuentas.parameters.htmlRequiered.StaticNamePrimera);	
	    	var Segunda = document.querySelector(Cuentas.parameters.htmlRequiered.StaticNameSegunda);
	    	var Total = document.querySelector(Cuentas.parameters.htmlRequiered.StaticNameTotal);			
	    	Primera.style.display = Cuentas.parameters.Style.Block;	
			Segunda.style.display = Cuentas.parameters.Style.Block;		
			Total.style.display = Cuentas.parameters.Style.Block;	
	       	}
    },
     "2":{
	    	Actualizar:function(){
	    	spForm_attachment.parameters.required = false
	    	spForm_attachment.parameters.minLengthFiles = 0
			var Banco = document.querySelector(Cuentas.parameters.htmlRequiered.StaticNameBanco);			    	
	    	var Cuenta = document.querySelector(Cuentas.parameters.htmlRequiered.StaticNameCuenta);	
	    	var Primera = document.querySelector(Cuentas.parameters.htmlRequiered.StaticNamePrimera);	
	    	var Segunda = document.querySelector(Cuentas.parameters.htmlRequiered.StaticNameSegunda);
	    	var Total = document.querySelector(Cuentas.parameters.htmlRequiered.StaticNameTotal);	
	    	var Attachment = document.querySelector(Cuentas.parameters.htmlRequiered.StaticAttachment);	    	
			spForms.loadedFields[Banco.getAttribute(Cuentas.parameters.htmlRequiered.StaticName)].required=false
			spForms.loadedFields[Cuenta.getAttribute(Cuentas.parameters.htmlRequiered.StaticName)].required=false
			spForms.loadedFields[Primera.getAttribute(Cuentas.parameters.htmlRequiered.StaticName)].required=false
			spForms.loadedFields[Segunda.getAttribute(Cuentas.parameters.htmlRequiered.StaticName)].required=false
			spForms.loadedFields[Total.getAttribute(Cuentas.parameters.htmlRequiered.StaticName)].required=false
			Attachment.style.display = Cuentas.parameters.Style.None;			
			Banco.style.display = Cuentas.parameters.Style.None;
			Cuenta.style.display = Cuentas.parameters.Style.None;	
			Primera.style.display = Cuentas.parameters.Style.None;	
			Segunda.style.display = Cuentas.parameters.Style.None;		
			Total.style.display = Cuentas.parameters.Style.None;										    	
	       	}
    },
	},
	//Cargar Formulario
	Initialize:function(){
		try {
			Cuentas.loadNewForm();
		}
		catch (e){
			console.log(e);
		}
	},
	loadNewForm:function(){
		spForms.options={
			actions:[
				{
					type:spForms.parameters.saveAction,
					name:Cuentas.parameters.IdEnviar,
					id:Cuentas.parameters.IdEnviar,
					success:function(){return true;},
					error:function(){},
					preSave:function(){return true;}
				},
				{
					type:spForms.parameters.cancelAction,
					name:Cuentas.parameters.IdCancelar,
					id:Cuentas.parameters.IdCancelar,
					callback:""
				},
			],
			title:Cuentas.parameters.TitleForm,
			contentTypeName:Cuentas.parameters.ContentType,
			containerId:Cuentas.parameters.IdDiv,
			allowAttachments:true,
		  	requiredAttachments:true,
		  	maxAttachments:5,
		  	minAttachments:1,
		
			successLoadNewForm:function(){
				Cuentas.InitForm();
			} 
			};
			spForms.loadNewForm();
			},
	
	
	//Despues de cargar
	loadUserInfo:function(){
		try {
				sputilities.getMyUserProfileProperties("",function(data){
				var resultItems = JSON.parse(data).d;
				Cuentas.parameters.resultItems = resultItems;
				Cuentas.setValuesUserInfo();
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	setValuesUserInfo:function(){
	  	document.getElementById(Cuentas.parameters.fields[0].field).value = Cuentas.parameters.resultItems.DisplayName ? Cuentas.parameters.resultItems.DisplayName: "";
	  	document.getElementById(Cuentas.parameters.fields[1].field).value = Cuentas.parameters.resultItems.CellPhone ? Cuentas.parameters.resultItems.CellPhone: "";
	  	document.getElementById(Cuentas.parameters.fields[2].field).value = Cuentas.parameters.resultItems.Email ? (Cuentas.parameters.resultItems.Email).toLowerCase(): "";
	},
	InitForm:function(){
		try {
			Cuentas.loadUserInfo();	
			Cuentas.validateEmailForm();
			Cuentas.validateTotal();
			Cuentas.ValidateTipoInscripcion();	
			Cuentas.ValidateFields();
			Cuentas.inputSelect();			
		}
		catch (e){
			console.log(e);
		}
	},
	inputSelect:function(){
		try {
			var Select = document.getElementById(Cuentas.parameters.IDSolicitud);
			var SelectInscripcion = Select.setAttribute('onchange','javascript:Cuentas.click(this)')										
		}
		catch (e){
			console.log(e);
		}
	},
	click:function(option){
		try {
			var CurrentEstado = document.getElementById(Cuentas.parameters.IDSolicitud).value;
			var spFormField = Cuentas.fieldType[CurrentEstado].Actualizar();
			
		}
		catch (e){
			console.log(e);
		}
	},
	ValidateTipoInscripcion:function(){
		try {
			var CurrentEstado = document.getElementById(Cuentas.parameters.IDSolicitud).value;
			var spFormField = Cuentas.fieldType[CurrentEstado].Actualizar();				
		}
		catch (e){
			console.log(e);
		}
	},
	ValidateFields:function(){
		try {
			var fields = document.querySelectorAll(Cuentas.parameters.htmlRequiered.StaticNameBanco+ "," + Cuentas.parameters.htmlRequiered.StaticNameCuenta+ ","
			+ Cuentas.parameters.htmlRequiered.StaticNamePrimera+"," + Cuentas.parameters.htmlRequiered.StaticNameSegunda);
			for(var i=0;i<fields.length;i++)
			{
				var required = spForms.loadedFields[fields[i].getAttribute(Cuentas.parameters.htmlRequiered.StaticName)].required=true
				var label = fields[i].querySelector(Cuentas.parameters.htmlRequiered.label);
				label.insertAdjacentHTML(Cuentas.parameters.htmlRequiered.Position,Cuentas.parameters.htmlRequiered.IconRequired)
				console.log(label)
				}
		}
		catch (e){
			console.log(e);
		}
	},

	validateEmailForm:function(){
		var input = document.getElementById(Cuentas.parameters.ID.Correo);
			if(input != null){
				input.addEventListener('change',function(event){
				var email = this.value;
		  			if (Cuentas.validateEmail(email)) {
		  				var staticName = "[sp-static-name="+Cuentas.parameters.ID.Correo+"]";
		  				var InputSelect = document.querySelector(staticName);
   							InputSelect.removeAttribute(Cuentas.parameters.Style.Invalid)
   			  				} 	
   			  		else{
		  				var staticName = "[sp-static-name="+Cuentas.parameters.ID.Correo+"]";
		  				var InputSelect = document.querySelector(staticName);
   						InputSelect.setAttribute(Cuentas.parameters.Style.Invalid,Cuentas.parameters.Style.Message)
   			  			}
  				return false;
  					});
					}
	},
	validateEmail:function(email){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 			return re.test(email);
 	 	},

		validateTotal:function(){
			var input = document.getElementById(Cuentas.parameters.ID.Primera);
			var inputt = document.getElementById(Cuentas.parameters.ID.Segunda);
				if(input != null){
					input.addEventListener('change',function(event){
					if(inputt.value == ""){
						document.getElementById(Cuentas.parameters.ID.Total).value= parseInt(input.value);
					}
					else{
						document.getElementById(Cuentas.parameters.ID.Total).value= parseInt(input.value)+parseInt(inputt.value);
					}	  				
	  					});
						}
						if(input != null){
							inputt.addEventListener('change',function(event){
							document.getElementById(Cuentas.parameters.ID.Total).value= parseInt(input.value)+parseInt(inputt.value)
  					});
					}

	},
	//validar campo numerico con decimales
	fieldNumber:function(input){
	var uno =document.getElementById('MontoSolicitado')
	var dos =uno.value;
	var todo = "";
	var tres = dos.split('$')[1].split(',');
	for(var i=0; i<tres.length; i++){
	var todoo = tres[i];
	todo = todo+todoo 
	console.log(todo)
	} 
	document.getElementById('MontoSolicitado').value = todo;
	console.log("el numero es"+todo);

		},
 	 	FormatNumero:function(input){
		try {
				var e = "$"
				var num = input.value.replace(/\,/g,'');
				var num = num.replace(/\$/g,'');
				if(!isNaN(num)){
				num = num.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
				num = num.split('').reverse().join('').replace(/^[\,]/,'');
				input.value =  e+num;
				var numero2 = input.value.replace(/\,/g,'');
				var numero3 = numero2.replace(/\$/g,'');//Variable que se debe guardar, variable sin puntos de miles.
				}
				 else{ alert('Solo se permiten numeros');
				input.value = input.value.replace(/[^\d\.]*/g,'');
				}		
			
		}
		catch (e){
			console.log(e);
		}
	},
}
Cuentas.Initialize();


