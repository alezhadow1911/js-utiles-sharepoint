﻿var Cuentas = {
	//create parameters
	parameters:{
		query:"?$Select=FlujoAsociado/Title,ResponsablesAbonos/EMail,ID&$expand=FlujoAsociado,ResponsablesAbonos&$filter=ID eq 17",
		queryNovedades:"?$Select=FlujoAsociado/Title,ResponsablesAbonos/EMail,ID&$expand=FlujoAsociado,ResponsablesAbonos&$filter=ID eq 18",
		listName: "ResponsablesTH",
		bodyItem:"<div class='contact'><p>Usted no tiene permisos sobre este formulario.</p></div>",
		Title:"Retiro de Cuentas - Acceso Denegado ",
		textNotAllowed: "Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior",
		selectorFormContent:".form-content",
		htmlNotAllowed:"<div><h4>{0}</h4></div>",
		title:"Formulario de Cuentas AFC -AFP Edición",
		description:"Completa la información correspondiente.",
		contentTypeName:"CuentasAFC",
		containerId:"spForm",

		Form:{
			IdEnviar: "Enviar",
			IdCancelar:"Cancelar",
			IdAprobar:"Aprobar",
			IdCorregir:"Enviar a Corregir",
			IdAplicar:"Aplicar Novedad",
			ContentType:"CuentasAFC",
			StaticComentarios:'[sp-static-name="ComentariosTH"]',
			Invalid: "sp-invalid",
			MessageInvalid:"Los comentarios son obligatorios"							
		},
			UpdateForm:{
			Enviar: "btn-Enviar",
			Aplicar:"btn-Aplicar Novedad",
			Corregir:"btn-Enviar a Corregir",
			Aprobar:"btn-Aprobar",
			Cancelar:"btn-Cancelar",
			//Ids
			Comentarios:"ComentariosTH",
			Base:"BasePersona",
			Nombre:"NombrePersona1",
			Documento:"DocumentoIdentidad",
			Celular:"NumeroCelular",
			Correo:"CorreoElectronico",
			Radicado:"NumeroRadicado",
			Estados:"EstadosCajaCompensacion",
			Banco:"Banco",
			Cuenta:"NumeroCuenta",
			Primera:"Valordescontarenla1eraQuincena",
			Segunda:"Valordescontarenla2daQuincena",
			Total:"TotalValordescontar",
			TpoAporte:"TipodeAporteVoluntario",
			TpoSolicitud:"TipoSolicitud",
			SpForm:"spForm",
			//etiquetas
			Input:"input",
			TextArea:"textarea"
							
		},
			StyleHTML:{
			Block: "block",
			None: "none",
			Point: "initial",
			PointNone: "none",
			Color: "#444"						
		},

		


		
	},	
	fieldType:{
	 "0":{
	    	Actualizar:function(){
	    	document.getElementById(Cuentas.parameters.UpdateForm.Aprobar).style.display =Cuentas.parameters.StyleHTML.None;
			document.getElementById(Cuentas.parameters.UpdateForm.Corregir).style.display =Cuentas.parameters.StyleHTML.None;	
			document.getElementById(Cuentas.parameters.UpdateForm.Enviar).style.display =Cuentas.parameters.StyleHTML.None;
			document.getElementById(Cuentas.parameters.UpdateForm.Cancelar).style.display =Cuentas.parameters.StyleHTML.None;
			document.getElementById(Cuentas.parameters.UpdateForm.Aplicar).style.display =Cuentas.parameters.StyleHTML.None;	
      	}
    },
     "1":{
	    	Actualizar:function(){
	    	Cuentas.userResponsablesVentanillaForm();
	    	var ObservacionesJefe = document.querySelector(Cuentas.parameters.Form.StaticComentarios);
			ObservacionesJefe.style.display = Cuentas.parameters.StyleHTML.Block;
	    	var TextObservacionesTH = document.getElementById(Cuentas.parameters.UpdateForm.Comentarios);
			TextObservacionesTH.style.pointerEvents = Cuentas.parameters.StyleHTML.Point;
			TextObservacionesTH.style.color = Cuentas.parameters.StyleHTML.Color;
			document.getElementById(Cuentas.parameters.UpdateForm.Enviar).style.display =Cuentas.parameters.StyleHTML.None;
			document.getElementById(Cuentas.parameters.UpdateForm.Aplicar).style.display =Cuentas.parameters.StyleHTML.None;
      	}
    },

    
     "2":{
	    	Actualizar:function(){
	    	Cuentas.userResponsablesNovedadesForm();
	    	 var ObservacionesJefe = document.querySelector(Cuentas.parameters.Form.StaticComentarios);
			ObservacionesJefe.style.display = Cuentas.parameters.StyleHTML.Block;
	    	document.getElementById(Cuentas.parameters.UpdateForm.Aprobar).style.display =Cuentas.parameters.StyleHTML.None;
			document.getElementById(Cuentas.parameters.UpdateForm.Corregir).style.display =Cuentas.parameters.StyleHTML.None;	
			document.getElementById(Cuentas.parameters.UpdateForm.Enviar).style.display =Cuentas.parameters.StyleHTML.None;	
     	 }
    },

   	"3":{
	    	Actualizar:function(){
	    	var ObservacionesJefe = document.querySelector(Cuentas.parameters.Form.StaticComentarios);
			ObservacionesJefe.style.display = Cuentas.parameters.StyleHTML.Block;
	    	document.getElementById(Cuentas.parameters.UpdateForm.Aprobar).style.display =Cuentas.parameters.StyleHTML.None;
			document.getElementById(Cuentas.parameters.UpdateForm.Corregir).style.display =Cuentas.parameters.StyleHTML.None;
			document.getElementById(Cuentas.parameters.UpdateForm.Aplicar).style.display =Cuentas.parameters.StyleHTML.None;		
			document.getElementById(Cuentas.parameters.UpdateForm.Nombre).style.pointerEvents = Cuentas.parameters.StyleHTML.Point;
			document.getElementById(Cuentas.parameters.UpdateForm.Documento).style.pointerEvents = Cuentas.parameters.StyleHTML.Point;
			document.getElementById(Cuentas.parameters.UpdateForm.Celular).style.pointerEvents = Cuentas.parameters.StyleHTML.Point;
			document.getElementById(Cuentas.parameters.UpdateForm.Correo).style.pointerEvents = Cuentas.parameters.StyleHTML.Point;
			document.getElementById(Cuentas.parameters.UpdateForm.Radicado).style.pointerEvents = Cuentas.parameters.StyleHTML.Point;
			document.getElementById(Cuentas.parameters.UpdateForm.Estados).style.pointerEvents = Cuentas.parameters.StyleHTML.Point;
			document.getElementById(Cuentas.parameters.UpdateForm.Nombre).style.color  = Cuentas.parameters.StyleHTML.Color;
			document.getElementById(Cuentas.parameters.UpdateForm.Documento).style.color  = Cuentas.parameters.StyleHTML.Color;
			document.getElementById(Cuentas.parameters.UpdateForm.Celular).style.color  = Cuentas.parameters.StyleHTML.Color;
			document.getElementById(Cuentas.parameters.UpdateForm.Correo).style.color  = Cuentas.parameters.StyleHTML.Color;
			document.getElementById(Cuentas.parameters.UpdateForm.Radicado).style.color  = Cuentas.parameters.StyleHTML.Color;
			document.getElementById(Cuentas.parameters.UpdateForm.Estados).style.color  = Cuentas.parameters.StyleHTML.Color;	
			document.getElementById(Cuentas.parameters.UpdateForm.Banco).style.pointerEvents = Cuentas.parameters.StyleHTML.Point;
			document.getElementById(Cuentas.parameters.UpdateForm.Cuenta).style.pointerEvents = Cuentas.parameters.StyleHTML.Point;
			document.getElementById(Cuentas.parameters.UpdateForm.Primera).style.pointerEvents = Cuentas.parameters.StyleHTML.Point;
			document.getElementById(Cuentas.parameters.UpdateForm.Segunda).style.pointerEvents = Cuentas.parameters.StyleHTML.Point;
			document.getElementById(Cuentas.parameters.UpdateForm.Total).style.pointerEvents = Cuentas.parameters.StyleHTML.Point;
			document.getElementById(Cuentas.parameters.UpdateForm.TpoAporte).style.pointerEvents = Cuentas.parameters.StyleHTML.Point;
			document.getElementById(Cuentas.parameters.UpdateForm.TpoSolicitud).style.pointerEvents = Cuentas.parameters.StyleHTML.Point;
			document.getElementById(Cuentas.parameters.UpdateForm.Banco).style.color  = Cuentas.parameters.StyleHTML.Color;
			document.getElementById(Cuentas.parameters.UpdateForm.Cuenta).style.color  = Cuentas.parameters.StyleHTML.Color;
			document.getElementById(Cuentas.parameters.UpdateForm.Primera).style.color  = Cuentas.parameters.StyleHTML.Color;
			document.getElementById(Cuentas.parameters.UpdateForm.Segunda).style.color  = Cuentas.parameters.StyleHTML.Color;
			document.getElementById(Cuentas.parameters.UpdateForm.Total).style.color  = Cuentas.parameters.StyleHTML.Color;
			document.getElementById(Cuentas.parameters.UpdateForm.TpoAporte).style.color  = Cuentas.parameters.StyleHTML.Color;
			document.getElementById(Cuentas.parameters.UpdateForm.TpoSolicitud).style.color  = Cuentas.parameters.StyleHTML.Color;		
			document.getElementsByClassName('col-md-6 col-xs-6 delete-doc')[0].style.display = Cuentas.parameters.StyleHTML.Block;
			document.getElementById('sp-forms-add-attachment').style.display = Cuentas.parameters.StyleHTML.Block;
			var Base = document.getElementById(Cuentas.parameters.UpdateForm.Base)
			var BaseInputs =Base.getElementsByTagName(Cuentas.parameters.UpdateForm.Input)
			for(var i=0; i<BaseInputs.length;i++){
			var input=BaseInputs[i];
			input.disabled=false;
			}	
      	}
    },
     "4":{
	    	Actualizar:function(){
	    	document.getElementById(Cuentas.parameters.UpdateForm.Aprobar).style.display =Cuentas.parameters.StyleHTML.None;
			document.getElementById(Cuentas.parameters.UpdateForm.Corregir).style.display =Cuentas.parameters.StyleHTML.None;	
			document.getElementById(Cuentas.parameters.UpdateForm.Enviar).style.display =Cuentas.parameters.StyleHTML.None;
			document.getElementById(Cuentas.parameters.UpdateForm.Cancelar).style.display =Cuentas.parameters.StyleHTML.None;
			document.getElementById(Cuentas.parameters.UpdateForm.Aplicar).style.display =Cuentas.parameters.StyleHTML.None;			
     	 }
    },
    },

	CtrlForm:function(){
		try {
			var Base = document.getElementById(Cuentas.parameters.UpdateForm.Base)
			var BaseInputs =Base.getElementsByTagName(Cuentas.parameters.UpdateForm.Input)
			for(var i=0; i<BaseInputs.length;i++){
				var input=BaseInputs[i];
				input.disabled=true;
				}	
			var CurrentEstado = document.getElementById(Cuentas.parameters.UpdateForm.Estados).value;
			var spFormField = Cuentas.fieldType[CurrentEstado].Actualizar();		
			}
		catch (e){
			console.log(e);
		}
	},
	userResponsablesVentanillaForm:function(){
		try {
			var UserEmailCurrent = _spPageContextInfo.userEmail
			var linkQuery = String.format(Cuentas.parameters.query);
			sputilities.getItems(Cuentas.parameters.listName,linkQuery,function(data){
				var resultItems = JSON.parse(data).d.results;
				var Items = resultItems[0].ResponsablesAbonos.results
				var find = false; 
				for(var i=0;i<Items.length;i++){
					var nextItem = Items[i+1];
					var prevItem = Items[i-1];
					var Email = Items[i].EMail
						if(Email == UserEmailCurrent){
							find = true;
							}
						}
				if(find){
					document.getElementById(Cuentas.parameters.UpdateForm.SpForm).style.display=Cuentas.parameters.StyleHTML.Block;
				}else{			
					Cuentas.hideNotAllowed();					
					}			
			},
			);
		}
		catch (e){
			console.log(e);
		}
	},
	userResponsablesNovedadesForm:function(){
		try {
		var UserEmailCurrent = _spPageContextInfo.userEmail
		var linkQuery = String.format(Cuentas.parameters.queryNovedades);
		sputilities.getItems(Cuentas.parameters.listName,linkQuery,function(data){
				var resultItems = JSON.parse(data).d.results;
				var Items = resultItems[0].ResponsablesAbonos.results
				var find = false; 
				for(var i=0;i<Items.length;i++){
					var nextItem = Items[i+1];
					var prevItem = Items[i-1];
					var Email = Items[i].EMail
					console.log(Email);
						if(Email == UserEmailCurrent){
							find = true;
						}
						}
				if(find){
					document.getElementById(Cuentas.parameters.UpdateForm.SpForm).style.display=Cuentas.parameters.StyleHTML.Block;
				}else{
					Cuentas.hideNotAllowed();					
					}		
			},
			);


		}
		catch (e){
			console.log(e);
		}
	},
		validateReject:function(){
	
		var row = document.querySelector(Cuentas.parameters.Form.StaticComentarios);
		var comments = row.querySelector(Cuentas.parameters.UpdateForm.TextArea);
		var isValid = true;
		if(comments.value != "")
		{
			row.removeAttribute(Cuentas.parameters.Form.Invalid);			
		}
		else
		{
			row.setAttribute(Cuentas.parameters.Form.Invalid,Cuentas.parameters.Form.MessageInvalid);			
		}
		return isValid;
	
	},	
	hideNotAllowed:function(){
		var html = String.format(Cuentas.parameters.htmlNotAllowed,Cuentas.parameters.textNotAllowed);
		var formContent = document.querySelector(Cuentas.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	},

	Initialize:function(){
		try {
		Cuentas.loadEditForm();
		}
		catch (e){
			console.log(e);
		}
	},
	loadEditForm:function(){
	spForms.options={
		actions:[
			{
			type:spForms.parameters.updateAction,
			name:Cuentas.parameters.Form.IdAprobar,
			id:Cuentas.parameters.Form.IdAprobar,
			preSave:function(){
			var CurrentEstado = document.getElementById(Cuentas.parameters.UpdateForm.Estados);
			if(CurrentEstado.value == "1"){
			CurrentEstado.value = "2";
			}
			return true},
			success:function(){return true},
			error:function(){return true}
		},
				{
			type:spForms.parameters.updateAction,
			name:Cuentas.parameters.Form.IdCorregir,
			id:Cuentas.parameters.Form.IdCorregir,
			preSave:function(){
			var CurrentEstado = document.getElementById(Cuentas.parameters.UpdateForm.Estados);
			if(CurrentEstado.value == "1"){
			CurrentEstado.value = "3";
			}
			return Cuentas.validateReject();
			return true},
			success:function(){return true},
			error:function(){return true}
		},
		{
			type:spForms.parameters.updateAction,
			name:Cuentas.parameters.Form.IdEnviar,
			id:Cuentas.parameters.Form.IdEnviar,
			preSave:function(){
			var CurrentEstado = document.getElementById(Cuentas.parameters.UpdateForm.Estados);
			if(CurrentEstado.value == "3"){
			CurrentEstado.value = "1";
			}
			return true},
			success:function(){return true},
			error:function(){return true}
		},
		{
			type:spForms.parameters.updateAction,
			name:Cuentas.parameters.Form.IdAplicar,
			id:Cuentas.parameters.Form.IdAplicar,
			preSave:function(){
			var CurrentEstado = document.getElementById(Cuentas.parameters.UpdateForm.Estados);
			if(CurrentEstado.value == "2"){
			CurrentEstado.value = "4";
			}

			return true},
			success:function(){return true},
			error:function(){return true}
		},


		{
			type:spForms.parameters.cancelAction,
			name:Cuentas.parameters.Form.IdCancelar,
			id:Cuentas.parameters.Form.IdCancelar,
			callback:""
		},
	],
	title:Cuentas.parameters.title,
	description:Cuentas.parameters.description,
	contentTypeName:Cuentas.parameters.contentTypeName,
	containerId:Cuentas.parameters.containerId,
		allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:5,
  	minAttachments:1,

	successLoadEditForm:function(){
		Cuentas.CtrlForm();
	} 
};
spForms.loadEditForm();
	},	
}
Cuentas.Initialize();







