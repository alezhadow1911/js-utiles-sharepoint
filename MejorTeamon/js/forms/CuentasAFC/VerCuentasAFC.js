﻿var Cuentas = {
	//create parameters
	parameters:{
			IdEnviar: "Enviar",
			IdCancelar:"Cancelar",
			ContentType:"CuentasAFC",
			IdDiv:"spForm",
			TitleForm:"Ver Cuentas AFC - AFP",
	},	
	Initialize:function(){
		try {
		Cuentas.loadEditForm();
		}
		catch (e){
			console.log(e);
		}
	},
	loadEditForm:function(){
	spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:Cuentas.parameters.IdEnviar,
			id:Cuentas.parameters.IdEnviar,
			success:function(){return true;},
			error:function(){},
			preSave:function(){
			return true;
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:Cuentas.parameters.IdCancelar,
			id:Cuentas.parameters.IdCancelar,
			callback:""
		},
	],
	title:Cuentas.parameters.TitleForm,
	contentTypeName:Cuentas.parameters.ContentType,
	containerId:Cuentas.parameters.IdDiv,
	allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:5,
  	minAttachments:1,

	successLoadEditForm:function(){
	
	} 
	};
	spForms.loadEditForm();
	},	
}
Cuentas.Initialize();


