﻿var ctrlForm = {
	parameters:{
		formats:{
			"queryStaticNameRow":"[sp-static-name='{0}']",
			"htmlNotAllowed":"<div><h4>{0}</h4></div> <div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
			"querySelectRadio":'input[type="radio"][value="{0}"]:checked',
			"queryRadio":'input[type="radio"][value="{0}"]',
			"emailUsers":"{0};",
			"dateFormat":"DD-MM-YYYY",
			"formFormat":"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}"
		},
		forms:{
			"NewForm":"ctrlForm.redirectCustomForm('NuevaSolicitud')",
			"EditForm":"ctrlForm.redirectCustomForm('CorregirSolicitud')",
			"DispForm":"ctrlForm.redirectCustomForm('VerSolicitud')",
			"NuevaSolicitud":"ctrlForm.loadNew()",
			"RevisionVentanillaTH":"ctrlForm.loadApprove()",
			"CorregirSolicitud":"ctrlForm.loadEditApplicant()",
			"AplicarNovedad":"ctrlForm.loadApply()",
			"VerSolicitud":"ctrlForm.loadReview()",
			"PendientesAprobar":"",
			"Aprobadas":""
		},
		messages:{
			"idSaveForm":"enviarSolicitud",
			"idCancelForm":"cancelar",
			"idEditForm":"enviarCorreccion",
			"idApproveForm":"aprobar",
			"idRejectForm":"solicitarCorreccion",
			"idApplyForm":"aplicarNovedad",
			"textSaveForm":"Enviar",
			"textCancelForm":"Cancelar",
			"textEditForm":"Enviar",
			"textApproveForm":"Aprobar",
			"textRejectForm":"Enviar a Corregir",
			"textApplyForm":"Novedad Aplicada",
			"textNotAllowed":"Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior"
		},
		htmlRequiered:{
			label:'label',
			StaticName:	"sp-static-name",
			Position:"beforeend",
			IconRequired:"<i>*</i>",
						//event Select
			StaticNameBanco:'[sp-static-name="Banco"]',
			StaticNameCuenta:'[sp-static-name="NumeroCuenta"]',
			StaticNamePrimera:'[sp-static-name="Valordescontarenla1eraQuincena"]',
			StaticNameSegunda:'[sp-static-name="Valordescontarenla2daQuincena"]',
			StaticNameTotal:'[sp-static-name="TotalValordescontar"]',	
			StaticAttachment:'[sp-static-name="sp-forms-attachment"]'						
		},

		columns:{
			"comments":{
				"staticName":"ComentariosTH"
			},
			"displayName":{
				"staticName":"NombrePersona1"
			},
			"idDoc":{
				"staticName":"NombrePersona1"
			},
			"cellPhone":{
				"staticName":"NumeroCelular"
			},
			"email":{
				"staticName":"CorreoElectronico"
			}
		},
		states:[
			"Solicitado",
			"Ventanilla TH",
			"Novedades",
			"Novedad Aplicada",
			"Corregir"
		]
		,
		parametersHTML:{
			"spHidden":"sp-hidden",
			"spDisable":"sp-disable"
		},
		ID:{
			ContentClose: ".col-md-6.col-xs-6.delete-doc",
			ContentAdd: "a#sp-forms-add-attachment",
			Attachment:"div[sp-static-name='sp-forms-attachment']",
			Correo: "CorreoElectronico",
			Banco:"Banco",
			Numero:"NumeroCuenta",	
			Primera:'Valordescontarenla1eraQuincena',
			Segunda:'Valordescontarenla2daQuincena',
			Total:'TotalValordescontar'


		},
		style:{
			None: "none",
			Opacity: "0.7",
			Block: "block",
			//Atributtes
			Invalid:"sp-invalid",
			Message:"Formato de correo invalido"
		},
		selectorFormContent:".form-content",
		IDSolicitud: "TipoSolicitud",
		cellPhone:"CellPhone",
		functionLoadMyProperties:"ctrlForm.loadMyProperties",
		containerId:"container-form",
		description:"Complete la información para la solicitud",
		descriptionEdit:"Corrija la información de la solicitud",
		descriptionApply:"Revise la información y aplique la novedad si corresponde",
		descriptionApprove:"Revise la información y apruebe o envíe a corrección según corresponda",
		descriptionReview:"Resultado de la solicitud",
		contentTypeName:"CuentasAFC",
		formIsLoaded:false,
		allowAttachments:true,
		requiredAttachments:true,
		maxAttachments:20,
		minAttachments:0,
		maxSizeInMB:3,
		listNameConsecutive:"ControlRadicados",
		idConsecutive:10,
		listUsersFlow:"ResponsablesTH",
		viewUrl:"",
	    editUrl:"",
	    approveUrl:"",
	    applyUrl:"",
	    listViewApply:"",
	    listViewApprove:"",
	    queryUsers:"?$filter=FlujoAsociadoId eq 10&$select=ResponsablesAbonos/EMail,EtapaResponsable&$expand=ResponsablesAbonos",
	    flowUrl:"https://prod-56.westus.logic.azure.com:443/workflows/d6cff379776c40deadf673bab348a03a/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=ebFoLLPQQdzPho6QKPIEmAOfCpt30eEm-lVg_hZ7Z5E"
	    
	},
	fieldType:{
	 "0":{
	    	Actualizar:function(){
	    	spForms.loadedFields.Banco.required=true
			spForms.loadedFields.NumeroCuenta.required=true


			spForms.loadedFields.Valordescontarenla1eraQuincena.required=true
			spForms.loadedFields.Valordescontarenla2daQuincena.required=true
	    	var Banco = document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNameBanco);			    	
	    	var Cuenta = document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNameCuenta);	
	    	var Primera = document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNamePrimera);	
	    	var Segunda = document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNameSegunda);
	    	var Total = document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNameTotal);		    	
			Banco.style.display = ctrlForm.parameters.style.Block;
			Cuenta.style.display = ctrlForm.parameters.style.Block;	
			Primera.style.display = ctrlForm.parameters.style.Block;	
			Segunda.style.display = ctrlForm.parameters.style.Block;		
			Total.style.display = ctrlForm.parameters.style.Block;	
			
	       	}
    },
       "1":{
	    	Actualizar:function(){
	    	spForms.loadedFields.Banco.required=false
			spForms.loadedFields.NumeroCuenta.required=false
						
			document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNameBanco).removeAttribute('sp-invalid')
			document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNameCuenta).removeAttribute('sp-invalid')

			
	    	spForms.loadedFields.Valordescontarenla1eraQuincena.required=true
			spForms.loadedFields.Valordescontarenla2daQuincena.required=true
	    	var Banco = document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNameBanco);			    	
	    	var Cuenta = document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNameCuenta);	
			Banco.style.display = ctrlForm.parameters.style.None;
			Cuenta.style.display = ctrlForm.parameters.style.None;	
			spForms.loadedFields[Banco.getAttribute(ctrlForm.parameters.htmlRequiered.StaticName)].required=false
			spForms.loadedFields[Cuenta.getAttribute(ctrlForm.parameters.htmlRequiered.StaticName)].required=false
			var Primera = document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNamePrimera);	
	    	var Segunda = document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNameSegunda);
	    	var Total = document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNameTotal);			
	    	Primera.style.display = ctrlForm.parameters.style.Block;	
			Segunda.style.display = ctrlForm.parameters.style.Block;		
			Total.style.display = ctrlForm.parameters.style.Block;	
	       	}
    },
     "2":{
	    	Actualizar:function(){
	    	spForms.loadedFields.Banco.required=false
			spForms.loadedFields.NumeroCuenta.required=false
	    	spForms.loadedFields.Valordescontarenla1eraQuincena.required=false
			spForms.loadedFields.Valordescontarenla2daQuincena.required=false
			
			
			document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNameBanco).removeAttribute('sp-invalid')
			document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNameCuenta).removeAttribute('sp-invalid')
			document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNamePrimera).removeAttribute('sp-invalid')
			document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNameSegunda).removeAttribute('sp-invalid')

	    	spForm_attachment.parameters.required = false
	    	spForm_attachment.parameters.minLengthFiles = 0
			var Banco = document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNameBanco);			    	
	    	var Cuenta = document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNameCuenta);	
	    	var Primera = document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNamePrimera);	
	    	var Segunda = document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNameSegunda);
	    	var Total = document.querySelector(ctrlForm.parameters.htmlRequiered.StaticNameTotal);	
	    	var Attachment = document.querySelector(ctrlForm.parameters.htmlRequiered.StaticAttachment);	    	
			spForms.loadedFields[Banco.getAttribute(ctrlForm.parameters.htmlRequiered.StaticName)].required=false
			spForms.loadedFields[Cuenta.getAttribute(ctrlForm.parameters.htmlRequiered.StaticName)].required=false
			spForms.loadedFields[Primera.getAttribute(ctrlForm.parameters.htmlRequiered.StaticName)].required=false
			spForms.loadedFields[Segunda.getAttribute(ctrlForm.parameters.htmlRequiered.StaticName)].required=false
			spForms.loadedFields[Total.getAttribute(ctrlForm.parameters.htmlRequiered.StaticName)].required=false
			Attachment.style.display = ctrlForm.parameters.style.None;			
			Banco.style.display = ctrlForm.parameters.style.None;
			Cuenta.style.display = ctrlForm.parameters.style.None;	
			Primera.style.display = ctrlForm.parameters.style.None;	
			Segunda.style.display = ctrlForm.parameters.style.None;		
			Total.style.display = ctrlForm.parameters.style.None;										    	
	       	}
    },
},
	global:{
		formName:null,
		createdBy:null,
		created:null,
		approveUsers:"",
		applyUsers:""
	},
	initialize:function(){
		var partsOfUrl = window.location.href.split('?')[0].split('/');
		ctrlForm.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];
		eval(ctrlForm.parameters.forms[ctrlForm.global.formName]);
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
		ctrlForm.parameters.viewUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[7],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.editUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[5],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.approveUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.applyUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[6],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.listViewApply = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[9]);
	    ctrlForm.parameters.listViewApprove= String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[8]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":ctrlForm.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":ctrlForm.global.created,
		        "comentarios":spForms.loadedFields.ComentariosTH.value,
		        "Correo":spForms.listInfo.title
		    },
		    "state":spForms.loadedFields.EstadosCajaCompensacion.value,
		    "titleCorreo":spForms.listInfo.title,  	
		    "approveUsers":ctrlForm.global.approveUsers,
		    "applyUsers":ctrlForm.global.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":ctrlForm.parameters.listNameConsecutive,
		    "idConsecutive":ctrlForm.parameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
		    "viewUrl":ctrlForm.parameters.viewUrl,
		    "editUrl":ctrlForm.parameters.editUrl,
		    "approveUrl":ctrlForm.parameters.approveUrl,
		    "applyUrl":ctrlForm.parameters.applyUrl,
		    "listViewApply":ctrlForm.parameters.listViewApply,
		    "listViewApprove":ctrlForm.parameters.listViewApprove	
		}
		utilities.http.callRestService({
			url:ctrlForm.parameters.flowUrl,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	getUsers:function(){
		sputilities.getItems(ctrlForm.parameters.listUsersFlow,ctrlForm.parameters.queryUsers,
			function(data){
				var data = utilities.convertToJSON(data);
				if(data != null){
					var items = data.d.results;
					for(var i = 0; i < items.length ;i++){
						var item = items[i];
						for(var j= 0; j < item.ResponsablesAbonos.results.length;j++){
							var user = item.ResponsablesAbonos.results[j];
							if(item.EtapaResponsable == ctrlForm.parameters.states[2]){
								ctrlForm.global.applyUsers += String.format(ctrlForm.parameters.formats.emailUsers,user.EMail);
							}else{
								ctrlForm.global.approveUsers += String.format(ctrlForm.parameters.formats.emailUsers,user.EMail);
							}
						}
					}	
				}
			},
			function(xhr){
				console.log(xhr);
			}
		);
	},
	validateEmailForm:function(){
		var input = document.getElementById(ctrlForm.parameters.ID.Correo);
			if(input != null){
				input.addEventListener('change',function(event){
				var email = this.value;
		  			if (ctrlForm.validateEmail(email)) {
		  				var staticName = "[sp-static-name="+ctrlForm.parameters.ID.Correo+"]";
		  				var InputSelect = document.querySelector(staticName);
   							InputSelect.removeAttribute(ctrlForm.parameters.style.Invalid)
   			  				} 	
   			  		else{
		  				var staticName = "[sp-static-name="+ctrlForm.parameters.ID.Correo+"]";
		  				var InputSelect = document.querySelector(staticName);
   						InputSelect.setAttribute(ctrlForm.parameters.style.Invalid,ctrlForm.parameters.style.Message)
   			  			}
  				return false;
  					});
					}
	},
	validateEmail:function(email){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 			return re.test(email);
 	 	},
	changeState:function(state){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio,state));
		if(radio != null){
			radio.click();
		}
	},
	hideNotAllowed:function(){
		var html = String.format(ctrlForm.parameters.formats.htmlNotAllowed,ctrlForm.parameters.messages.textNotAllowed);
		var formContent = document.querySelector(ctrlForm.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	},
	validateUserCreated:function(){
		if(spForms.currentItem.AuthorId != sputilities.contextInfo().userId){
			ctrlForm.hideNotAllowed();
			return false;
		}
	},
	validateAllUsers:function(){
		if(spForms.currentItem.AuthorId != sputilities.contextInfo().userId || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
	},
	validateEditUser:function(){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[4])) == null;
		if(spForms.currentItem.AuthorId != sputilities.contextInfo().userId || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
		return true;
	},
	//Novedades
	validateApplyUser:function(){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[2])) == null;
		if(ctrlForm.global.applyUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}
		ctrlForm.hideAttachments();
	},
	//Ventanilla TH
	validateApproveUser:function(){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio,ctrlForm.parameters.states[1])) == null;
		if(ctrlForm.global.approveUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio){
			ctrlForm.hideNotAllowed();
			return false;
		}	
		ctrlForm.hideAttachments();
	},
	hideAttachments:function(){
		var i;
		var CloaseAttachments = document.querySelectorAll(ctrlForm.parameters.ID.ContentClose);
			for(i=0; i<CloaseAttachments.length; i++){
			CloaseAttachments[i].style.display = ctrlForm.parameters.style.None;
			}
		document.querySelector(ctrlForm.parameters.ID.ContentAdd).style.display = ctrlForm.parameters.style.None;
		document.querySelector(ctrlForm.parameters.ID.Attachment).style.opacity = ctrlForm.parameters.style.Opacity;
	},

	hideComments:function(){
		var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.comments.staticName));
		if(ctrlComments != null){
			ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,true);
		}
	},
	blockedComments:function(){
		var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.comments.staticName));
		if(ctrlComments != null){
			ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,true);
		}
	},

	loadReview:function(){
		var actions = [
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionReview;
		ctrlForm.loadEdit(actions,ctrlForm.validateUserCreated);
	},
	loadApply:function(){
		var actions = [
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textApplyForm,
				id:ctrlForm.parameters.messages.idApplyForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					ctrlForm.changeState(ctrlForm.parameters.states[3]);
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionApply;
		ctrlForm.loadEdit(actions,ctrlForm.validateApplyUser);

	},
	loadApprove:function(){
		var actions = [
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textApproveForm,
				id:ctrlForm.parameters.messages.idApproveForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = false;
					ctrlForm.changeState(ctrlForm.parameters.states[2]);
					return true;
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textRejectForm,
				id:ctrlForm.parameters.messages.idRejectForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
					ctrlForm.changeState(ctrlForm.parameters.states[4]);
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
		ctrlForm.loadEdit(actions,ctrlForm.validateApproveUser);

	},
	loadEditApplicant:function(){
		var actions = [
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textEditForm,
				id:ctrlForm.parameters.messages.idEditForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){
					ctrlForm.changeState(ctrlForm.parameters.states[1]);
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionEdit;
		ctrlForm.loadEdit(actions,function(){
			if(ctrlForm.validateEditUser()){
			ctrlForm.hideComments();
				var ctrlComments = document.getElementById(ctrlForm.parameters.columns.comments.staticName);
				if(ctrlComments != null){
					ctrlComments.value = "";
				}
			}
		});
	},
	loadEdit:function(actions,callBack){
		ctrlForm.getUsers();
		spForms.options={
			actions:actions,
			description:ctrlForm.parameters.description,
			contentTypeName:ctrlForm.parameters.contentTypeName,
			containerId:ctrlForm.parameters.containerId,
			allowAttachments:ctrlForm.parameters.allowAttachments,
		  	requiredAttachments:ctrlForm.parameters.requiredAttachments,
		  	maxAttachments:ctrlForm.parameters.maxAttachments,
		  	minAttachments:ctrlForm.parameters.minAttachments,
		  	maxSizeInMB:ctrlForm.parameters.maxSizeInMB,
			successLoadEditForm:function(){	
				ctrlForm.global.createdBy = spForms.currentItem.Author.EMail;
				ctrlForm.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(ctrlForm.parameters.formats.dateFormat);
				ctrlForm.formIsLoaded = true;
				ctrlForm.HideFields();
				ctrlForm.inputSelectEdit();		
				
				if(typeof callBack== "function"){
					callBack();
				}
			} 
		};
		spForms.loadEditForm();

	},
	loadNew:function(){
		ctrlForm.getUsers();
		ctrlForm.global.createdBy = sputilities.contextInfo().userEmail;
		ctrlForm.global.created = moment.utc(new Date()).format(ctrlForm.parameters.formats.dateFormat);
		sputilities.callFunctionOnLoadBody(ctrlForm.parameters.functionLoadMyProperties);
		ctrlForm.loadNewForm();
	},
	loadNewForm:function(){
		spForms.options={
			actions:[
				{
					type:spForms.parameters.saveAction,
					name:ctrlForm.parameters.messages.textSaveForm,
					id:ctrlForm.parameters.messages.idSaveForm,
					success:function (){
						ctrlForm.callFlowService();
						return false;
					},
					error:function (){return true;},
					preSave: function (){
						ctrlForm.changeState(ctrlForm.parameters.states[0]);
						return true;
					}
				},
				{
					type:spForms.parameters.cancelAction,
					name:ctrlForm.parameters.messages.textCancelForm,
					id:ctrlForm.parameters.messages.idCancelForm,
					callback:null
				},
			],
			description:ctrlForm.parameters.description,
			contentTypeName:ctrlForm.parameters.contentTypeName,
			containerId:ctrlForm.parameters.containerId,
			allowAttachments:ctrlForm.parameters.allowAttachments,
		  	requiredAttachments:ctrlForm.parameters.requiredAttachments,
		  	maxAttachments:ctrlForm.parameters.maxAttachments,
		  	minAttachments:ctrlForm.parameters.minAttachments,
		  	maxSizeInMB:ctrlForm.parameters.maxSizeInMB,
			successLoadNewForm:function(){
				ctrlForm.formIsLoaded = true;
				ctrlForm.hideComments();
				ctrlForm.validateEmailForm();
				ctrlForm.inputSelect();	
				ctrlForm.validateTotal();
			} 
		};
		spForms.loadNewForm();
	},
	loadMyProperties:function(){
		sputilities.getUserProfileProperties(sputilities.contextInfo().userEmail,
			function(userInfo){
				var objUser = utilities.convertToJSON(userInfo);
				if(objUser != null){
					var displayName = objUser.d.DisplayName;
					var email = objUser.d.Email;
					var cellPhone = ctrlForm.findProperty(objUser,ctrlForm.parameters.cellPhone);
					var validateFormIsLoaded = setInterval(function(){
						if(ctrlForm.formIsLoaded){
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.displayName.staticName,displayName);
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.email.staticName,email);
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.cellPhone.staticName,cellPhone);
							clearInterval(validateFormIsLoaded);
						}
					},500);
				}
			},
			function(xhr){
				console.log(xhr)
			}
		);
	},
	setValueToControl:function(staticName,value){
		if(value != null){
			var ctrl = document.getElementById(staticName);
			if(ctrl != null){
				ctrl.value = value;
			}
		}
	},
	findProperty:function(objUser,name){
		var properties = objUser.d.UserProfileProperties.results;
		for(var i = 0; i < properties.length; i++){
			var property = properties[i];
			if(property.Key == name){
				return property.Value;
			}
		}
	},
		validateTotal:function(){
			var input = document.getElementById(ctrlForm.parameters.ID.Primera);
			var inputt = document.getElementById(ctrlForm.parameters.ID.Segunda);
				if(input != null){
					input.addEventListener('change',function(event){
					if(inputt.value == ""){
						document.getElementById(ctrlForm.parameters.ID.Total).value= parseInt(input.value);
					}
					else{
						document.getElementById(ctrlForm.parameters.ID.Total).value= parseInt(input.value)+parseInt(inputt.value);
					}	  				
	  					});
						}
						if(input != null){
							inputt.addEventListener('change',function(event){
							document.getElementById(ctrlForm.parameters.ID.Total).value= parseInt(input.value)+parseInt(inputt.value)
  					});
					}

	},

	inputSelect:function(){
		try {
			var Select = document.getElementById(ctrlForm.parameters.IDSolicitud);
			var SelectInscripcion = Select.setAttribute('onchange','javascript:ctrlForm.click(this)')
			spForms.loadedFields.Banco.required=true
			spForms.loadedFields.NumeroCuenta.required=true
			spForms.loadedFields.Valordescontarenla1eraQuincena.required=true
			spForms.loadedFields.Valordescontarenla2daQuincena.required=true
										
		}
		catch (e){
			console.log(e);
		}
	},
		inputSelectEdit:function(){
		try {
			var Select = document.getElementById(ctrlForm.parameters.IDSolicitud);
			var SelectInscripcion = Select.setAttribute('onchange','javascript:ctrlForm.click(this)')
										
		}
		catch (e){
			console.log(e);
		}
	},

	click:function(option){
		try {
			var CurrentEstado = document.getElementById(ctrlForm.parameters.IDSolicitud).value;
			var spFormField = ctrlForm.fieldType[CurrentEstado].Actualizar();
			
		}
		catch (e){
			console.log(e);
		}
	},
	HideFields:function(){
		try {
			var CurrentEstado = document.getElementById(ctrlForm.parameters.IDSolicitud).value;
			var spFormField = ctrlForm.fieldType[CurrentEstado].Actualizar();
			
		}
		catch (e){
			console.log(e);
		}
	},


	redirectCustomForm:function(name){
		window.location.href = window.location.href.replace(ctrlForm.global.formName,name);
	}
}
ctrlForm.initialize();