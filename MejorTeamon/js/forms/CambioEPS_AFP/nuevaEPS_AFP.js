﻿var cambioEpsForm = {
	
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		internalNameFields:{
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico',
			DeclaracionConocimiento:'DeclaracionConocimiento',
			icoDeclaracion:"ico-help-DeclaracionConocimiento"
		},
		resultItems:null,
		userProperties:"CellPhone", 
		loadUserInfo:"cambioEpsForm.loadUserInfo",
		formsIsLoaded:false,
		fieldsHTML:{
			NombrePersona:'[sp-static-name="NombrePersona1"]',
			NumeroCelular:'[sp-static-name="NumeroCelular"]',
			CorreoElectronico:'[sp-static-name="CorreoElectronico"]',
			DeclaracionConocimiento:'[sp-static-name="DeclaracionConocimiento"]'					
		},
		requiered:"sp-requiered",
		invalid:"sp-invalid",
		staticName:"sp-static-name",
		errorInvalid:"El correo electrónico no tiene el formato correcto",
		mainErrorInvalid:"Esto no puede estar en blanco",
		htmlRequiered:{position:"beforeend",html:"<i>*</i>",label:"label",block:"block",none:"none"},
		formsParameters:{
			title:"Solicitud Cambio EPS y/o AFP",
			description:"Completa la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"}
			},
			contentType:"Cambio EPS_AFP",
			containerId:"spForm"			
		},
		flowParameters:{
			urlFlow:"https://prod-05.westus.logic.azure.com:443/workflows/c59133af857d4de7a18fe5f572dd5214/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=GaG-VOm0mV4IcGheiM1P4jVGxl1MKJeUOOiEu63j45Q",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",	   		
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Cambio EPS y/o AFP Ventanilla TH",
	    	applyUsers:"Cambio EPS y/o AFP Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:5,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Cambio EPS, Cesantías, Pensiones"
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud Ventanilla TH",			
			"AplicarSolicitud":"Aplicar Novedad",
			"CorregirSolicitud":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Novedades":"Vista Novedades",
			"Ventanilla":"Vista Ventanilla"
		},
		global:{
			createdBy:null,
			created:null
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
	    cambioEpsForm.parameters.flowParameters.approveUrl = String.format(cambioEpsForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(cambioEpsForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);			    
	    cambioEpsForm.parameters.flowParameters.applyUrl = String.format(cambioEpsForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(cambioEpsForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    cambioEpsForm.parameters.flowParameters.editUrl = String.format(cambioEpsForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(cambioEpsForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    cambioEpsForm.parameters.flowParameters.listViewApply = String.format(cambioEpsForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(cambioEpsForm.parameters.forms)[5]);
	    cambioEpsForm.parameters.flowParameters.listViewApprove= String.format(cambioEpsForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(cambioEpsForm.parameters.forms)[6]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":cambioEpsForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":cambioEpsForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosEPS.value		       
		    },
		    "state":spForms.loadedFields.EstadoSolicitudEPS.value,
		    "approveUsers":cambioEpsForm.parameters.flowParameters.approveUsers,
		    "applyUsers":cambioEpsForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":cambioEpsForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":cambioEpsForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":cambioEpsForm.parameters.flowParameters.listNameAssing,
		    "editUrl":cambioEpsForm.parameters.flowParameters.editUrl,
		    "approveUrl":cambioEpsForm.parameters.flowParameters.approveUrl,
		    "applyUrl":cambioEpsForm.parameters.flowParameters.applyUrl,		    
		    "listViewApply":cambioEpsForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":cambioEpsForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	cambioEpsForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:cambioEpsForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
				return true;
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){
		sputilities.callFunctionOnLoadBody(cambioEpsForm.parameters.loadUserInfo);
		spForms.loadNewForm();
	},	
	loadUserInfo:function(){
		try {
			//running the query with parameters
			sputilities.getMyUserProfileProperties("",function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d;
				cambioEpsForm.parameters.resultItems = resultItems;
				var ctrlInterval = setInterval(function(){
					if(cambioEpsForm.parameters.formsIsLoaded)
					{
						cambioEpsForm.setValuesUserInfo();
						clearInterval(ctrlInterval);
					}
				},500);
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	setValuesUserInfo:function(){
	
		if(cambioEpsForm.parameters.resultItems != null)
		{
		  	var name = document.getElementById(cambioEpsForm.parameters.internalNameFields.NombrePersona);
		  	var email = document.getElementById(cambioEpsForm.parameters.internalNameFields.CorreoElectronico);
		  	var cellPhone = document.getElementById(cambioEpsForm.parameters.internalNameFields.NumeroCelular);
		  	if(name != null){
		  		name.value = cambioEpsForm.parameters.resultItems.DisplayName ? cambioEpsForm.parameters.resultItems.DisplayName: "";
		  	}		  	
		  	if(email != null){
		  		email.value = cambioEpsForm.parameters.resultItems.Email ? (cambioEpsForm.parameters.resultItems.Email).toLowerCase(): "";
		  	}		  	
		  	if(cellPhone != null){		  	
		  		var userProperties = cambioEpsForm.parameters.resultItems.UserProfileProperties.results;
		  	
			  	for(var i=0;i<userProperties.length;i++)
			  	{
			  		if(userProperties[i].Key == cambioEpsForm.parameters.userProperties)
			  		{
			  		 	cellPhone.value =  userProperties[i].Value;
			  		 	break;
			  		}
			  	}
			}
	  	}
	  	var icoDeclaracion = document.getElementById(cambioEpsForm.parameters.internalNameFields.icoDeclaracion)
	  	if(icoDeclaracion != null){
	  		icoDeclaracion.click();
	  	}
	},
	validateRequired:function(){
		
		var checkValue =  document.getElementById(cambioEpsForm.parameters.internalNameFields.DeclaracionConocimiento);
		var rowAccept = document.querySelector(cambioEpsForm.parameters.fieldsHTML.DeclaracionConocimiento);
		var rowEmail = document.querySelector(cambioEpsForm.parameters.fieldsHTML.CorreoElectronico);
		var email = document.getElementById(cambioEpsForm.parameters.internalNameFields.CorreoElectronico);
		var isValid = true;		
		if(email != null && rowEmail!= null && rowAccept != null)
		{	
			rowEmail.setAttribute(cambioEpsForm.parameters.invalid,cambioEpsForm.parameters.errorInvalid);
			rowAccept.setAttribute(cambioEpsForm.parameters.invalid,cambioEpsForm.parameters.mainErrorInvalid);
			
				if (cambioEpsForm.validateEmail(email.value) || email.value == ""){
					rowEmail.removeAttribute(cambioEpsForm.parameters.invalid);
				}
				if(checkValue.checked){
					rowAccept.removeAttribute(cambioEpsForm.parameters.invalid);
				}				
			
		}
		else
		{
			isValid=false;
		}		
		return isValid;
		
	},
	validateEmail: function (email){
		 var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);
	
	}

}

spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:cambioEpsForm.parameters.formsParameters.buttons.Send.Text,
			id:cambioEpsForm.parameters.formsParameters.buttons.Send.Id,
			success:function (){
				return cambioEpsForm.callFlowService();
			},
			error:function (){},
			preSave: function (){
				return cambioEpsForm.validateRequired()			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:cambioEpsForm.parameters.formsParameters.buttons.Cancel.Text,
			id:cambioEpsForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:cambioEpsForm.parameters.formsParameters.title,
	description:cambioEpsForm.parameters.formsParameters.description,
	contentTypeName:cambioEpsForm.parameters.formsParameters.contentType,
	containerId:cambioEpsForm.parameters.formsParameters.containerId,	
	allowAttachments:false,
  	requiredAttachments:false,
  	maxAttachments:0,
  	minAttachments:0,
  	maxSizeInMB:0,
	successLoadNewForm:function(){
		cambioEpsForm.parameters.global.createdBy = sputilities.contextInfo().userEmail;
		cambioEpsForm.parameters.global.created = moment.utc(new Date()).format(cambioEpsForm.parameters.formats.dateFormat);
		cambioEpsForm.parameters.formsIsLoaded = true;				
	} 
};

cambioEpsForm.initialize();