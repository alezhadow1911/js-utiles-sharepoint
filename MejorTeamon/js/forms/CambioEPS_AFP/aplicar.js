﻿
var cambioEpsForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},	
		query:"?$Select=FlujoAsociado/Title,ResponsablesAbonos/EMail,ID&$expand=FlujoAsociado,ResponsablesAbonos&$filter=Title eq 'Cambio EPS y/o AFP Novedades'",
		listName:"ResponsablesTH",
			bodyItem:"<div class='form-group row'><h4>Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior</h4></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
		TitlePopUp:"Cambio EPS y/o AFP - Acceso Denegado",
		containerForm:".form-content",
		internalNameFields:{
			Estado:"EstadoSolicitudEPS"
		},
		formsParameters:{
			title:"Aplicar Solicitud Cambio EPS y/o AFP",
			description:"Revisa la información de la solicitud, para aplicar la novedad",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"}
			},
			contentType:"Cambio EPS_AFP",
			containerId:"spForm"			
		},
		flowParameters:{
			urlFlow:"https://prod-05.westus.logic.azure.com:443/workflows/c59133af857d4de7a18fe5f572dd5214/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=GaG-VOm0mV4IcGheiM1P4jVGxl1MKJeUOOiEu63j45Q",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",	   		
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Cambio EPS y/o AFP Ventanilla TH",
	    	applyUsers:"Cambio EPS y/o AFP Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:5,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Cambio EPS, Cesantías, Pensiones"
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud Ventanilla TH",			
			"AplicarSolicitud":"Aplicar Novedad",
			"CorregirSolicitud":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Novedades":"Vista Novedades",
			"Ventanilla":"Vista Ventanilla"
		},
		global:{
			createdBy:null,
			created:null
		}	
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
	    cambioEpsForm.parameters.flowParameters.approveUrl = String.format(cambioEpsForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(cambioEpsForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);			    
	    cambioEpsForm.parameters.flowParameters.applyUrl = String.format(cambioEpsForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(cambioEpsForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    cambioEpsForm.parameters.flowParameters.editUrl = String.format(cambioEpsForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(cambioEpsForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    cambioEpsForm.parameters.flowParameters.listViewApply = String.format(cambioEpsForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(cambioEpsForm.parameters.forms)[5]);
	    cambioEpsForm.parameters.flowParameters.listViewApprove= String.format(cambioEpsForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(cambioEpsForm.parameters.forms)[6]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":cambioEpsForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":cambioEpsForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosEPS.value		       
		    },
		    "state":spForms.loadedFields.EstadoSolicitudEPS.value,
		    "approveUsers":cambioEpsForm.parameters.flowParameters.approveUsers,
		    "applyUsers":cambioEpsForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":cambioEpsForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":cambioEpsForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":cambioEpsForm.parameters.flowParameters.listNameAssing,
		    "editUrl":cambioEpsForm.parameters.flowParameters.editUrl,
		    "approveUrl":cambioEpsForm.parameters.flowParameters.approveUrl,
		    "applyUrl":cambioEpsForm.parameters.flowParameters.applyUrl,		    
		    "listViewApply":cambioEpsForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":cambioEpsForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	cambioEpsForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:cambioEpsForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
				return true;
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	validateAplicated:function(){
		var state =document.getElementById(cambioEpsForm.parameters.internalNameFields.Estado);

		if(state != null)
		{
			state.value = "3";
		}
		else
		{
			return false;
		}
		return true;
	},
	loadResponsableState:function(){
	
		try
		{
			var userCurrent = _spPageContextInfo.userEmail

			sputilities.getItems(cambioEpsForm.parameters.listName,cambioEpsForm.parameters.query,function(data){
				
				var resultItems = JSON.parse(data).d.results;
				var items = resultItems[0].ResponsablesAbonos.results
				var find = false;
				for(i=0;i<items.length;i++)
				{
					if(items[i].EMail == userCurrent){
						find = true;
						break;
					}
				}

				var state =document.getElementById(cambioEpsForm.parameters.internalNameFields.Estado);
				if(state != null)
				{
					if(!find || state.value != "2")
					{					
						cambioEpsForm.hideNotAllowed();					
					}
				}
				else
				{
					cambioEpsForm.hideNotAllowed();
				}

			});

		}
		catch(e){
			console.log(e);
		}	
	},
	hideNotAllowed:function(){
		var html = cambioEpsForm.parameters.bodyItem;
		var formContent = document.querySelector(cambioEpsForm.parameters.containerForm);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	}

}

spForms.options={
	actions:[
		{
			type:spForms.parameters.updateAction,
			name:cambioEpsForm.parameters.formsParameters.buttons.Aplicate.Text,
			id:cambioEpsForm.parameters.formsParameters.buttons.Aplicate.Id,
			success:function (){
				return cambioEpsForm.callFlowService();
			},
			error:function (){},
			preSave: function (){
				return cambioEpsForm.validateAplicated()
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:cambioEpsForm.parameters.formsParameters.buttons.Cancel.Text,
			id:cambioEpsForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:cambioEpsForm.parameters.formsParameters.title,
	description:cambioEpsForm.parameters.formsParameters.description,
	contentTypeName:cambioEpsForm.parameters.formsParameters.contentType,
	containerId:cambioEpsForm.parameters.formsParameters.containerId,	
	allowAttachments:false,
  	requiredAttachments:false,
  	maxAttachments:0,
  	minAttachments:0,
  	maxSizeInMB:0,	
	successLoadEditForm:function(){
		cambioEpsForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
		cambioEpsForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(cambioEpsForm.parameters.formats.dateFormat);

		cambioEpsForm.loadResponsableState();
	} 
};
spForms.loadEditForm();