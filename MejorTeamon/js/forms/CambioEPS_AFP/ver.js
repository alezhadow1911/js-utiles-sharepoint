﻿var cambioEpsForm = {	
	parameters:{
		internalNameFields:{	
			icoDeclaracion:"ico-help-DeclaracionConocimiento"			
		},
		fieldsHTML:{					
			DeclaracionConocimiento:'[sp-static-name="DeclaracionConocimiento"]'					
		},		
		formsParameters:{
			title:"Solicitud Cambio EPS y/o AFP",			
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"}
			},
			contentType:"Cambio EPS_AFP",
			containerId:"spForm"			
		}
	}		
}
spForms.options={
	actions:[		
		{
			type:spForms.parameters.cancelAction,
			name:cambioEpsForm.parameters.formsParameters.buttons.Cancel.Text,
			id:cambioEpsForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:cambioEpsForm.parameters.formsParameters.title,	
	contentTypeName:cambioEpsForm.parameters.formsParameters.contentType,
	containerId:cambioEpsForm.parameters.formsParameters.containerId,	
	allowAttachments:false,
  	requiredAttachments:false,
  	maxAttachments:0,
  	minAttachments:0,
  	maxSizeInMB:0,
	successLoadEditForm:function(){			
		var icoDeclaracion = document.getElementById(cambioEpsForm.parameters.internalNameFields.icoDeclaracion)
	  	if(icoDeclaracion != null){
	  		icoDeclaracion.click();
	  	}		
	} 
};
spForms.loadEditForm();
