﻿var ctrlForm = {
    parameters: {
        formats: {
            "queryStaticNameRow": "[sp-static-name='{0}']",
            "htmlNotAllowed": "<div><h4>{0}</h4></div> <div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
            "querySelectRadio": 'input[type="radio"][value="{0}"]:checked',
            "queryRadio": 'input[type="radio"][value="{0}"]',
            "emailUsers": "{0};",
            "emailUsersResponsables": "{0}",
            "dateFormat": "DD-MM-YYYY",
            "formFormat": "{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
            "htmlItemCheckBox": "<div class='checkbox'><label><input type='checkbox' id='{1}-Estacion2' name='Estacion2' value='{0}' data-responsable='{2}'>{0}</label></div>",
            "htmlCheckBox": "<div class='list-checkbox-radio'>{0}</div>",
            "htmlCheckBoxDiv": "<div class='form-group row form-checkbox' sp-static-name='Estacion2' sp-required='false' sp-type='multichoice' sp-hidden='false'><label class='col-sm-4 control-label'>Estacion2</label><div class='col-sm-8'><div class='form-control-help'>{0}<a class='glyphicon glyphicon-question-sign ico-help' id='ico-help-Estacion2' aria-hidden='true' onclick='javascript:spForms.showHelpDescription('Estacion2',this)'></a></div><span id='help-block-Estacion2' class='help-block forms-hide-element'></span><span id='help-block-error-message-Estacion2' class='help-block forms-hide-element error-message'></span></div></div>",
        },
        forms: {
            "NewForm": "ctrlForm.redirectCustomForm('NuevaSolicitud')",
            "EditForm": "ctrlForm.redirectCustomForm('CorregirSolicitud')",
            "DispForm": "ctrlForm.redirectCustomForm('VerSolicitud')",
            "NuevaSolicitud": "ctrlForm.loadNew()",
            "CargarInformacion": "ctrlForm.loadApprove()",
            "CorregirSolicitud": "ctrlForm.loadEditApplicant()",
            "AplicarNovedad": "ctrlForm.loadApply()",
            "VerSolicitud": "ctrlForm.loadReview()",
            "PendientesAprobar": "",
            "Aprobadas": ""
        },
        messages: {
            "idSaveForm": "enviarSolicitud",
            "idCancelForm": "cancelar",
            "idEditForm": "enviarCorreccion",
            "idApproveForm": "aprobar",
            "idRejectForm": "solicitarCorreccion",
            "idApplyForm": "aplicarNovedad",
            "textSaveForm": "Enviar",
            "textCancelForm": "Cancelar",
            "textEditForm": "Enviar",
            "textApproveForm": "Aprobar",
            "textRejectForm": "Responder",
            "textApplyForm": "Novedad Aplicada",
            "textNotAllowed": "Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior",
            "textNotAllowedRevisores": "Usted no tiene permisos sobre este formulario o la solicitud ya fue aprobada por usted"
        },
        columns: {
            "comments": {
                "staticName": "ComentariosInformacion"
            },
            "displayName": {
                "staticName": "NombrePersona1"
            },
            "idDoc": {
                "staticName": "NombrePersona1"
            },
            "cellPhone": {
                "staticName": "NumeroCelular"
            },
            "email": {
                "staticName": "CorreoElectronico"
            }
        },
        states: [
            "Cargar Información Solicitada",
            "Corregir",
            "Revision Estaciones",
            "Finalizado",
            "Corregir"
        ],
        parametersHTML: {
            "spHidden": "sp-hidden",
            "spDisable": "sp-disable"
        },
        ID: {
            ContentClose: ".col-md-6.col-xs-6.delete-doc",
            ContentAdd: "a#sp-forms-add-attachment",
            Attachment: "div[sp-static-name='sp-forms-attachment']",
            Correo: "CorreoElectonico",
            CorreoAltern: "CorreoAlterno",            
            CheckBodId: "[sp-static-name='Estacion2'] .form-control-help",
            CheckRemove: ".list-checkbox-radio .checkbox",
            CoaId: "COA",
            functionOnchange: "onchange",
            JsfunctionOnchange: "javascript:ctrlForm.click(this);",
            IdEstaciones:"CantidadRevisores",
            IdNumeroEstacionesAprobadas:"NumeroRevisoresAprobados",
            idJsonEstaciones:"EstacionesAprobadas"

        },
        style: {
            None: "none",
            Opacity: "0.7",
            Block: "block",
            //Atributtes
            Invalid: "sp-invalid",
            Message: "Formato de correo invalido"
        },
        selectorFormContent: ".form-content",
        cellPhone: "CellPhone",
        functionLoadMyProperties: "ctrlForm.loadMyProperties",
        containerId: "container-form",
        description: "Complete la información para la solicitud",
        descriptionEdit: "Corrija la información de la solicitud",
        descriptionApply: "Revise la información y aplique la novedad si corresponde",
        descriptionApprove: "Revise la información y apruebe o envíe a corrección según corresponda",
        descriptionReview: "Resultado de la solicitud",
        contentTypeName: "Solicitud Informacion",
        formIsLoaded: false,
        allowAttachments: true,
        requiredAttachments: true,
        maxAttachments: 20,
        minAttachments: 0,
        maxSizeInMB: 3,
        Contador: "",
        listNameEstacion: "Estacion",
        ListName: sputilities.contextInfo().listTitle,
        ObjectPerson: "",
        idConsecutive: 12,
        CurrentStation:"",
        listUsersFlow: "ResponsablesTH",
        RevisionInicial: true,
        viewUrl: "",
        editUrl: "",
        approveUrl: "",
        applyUrl: "",
        listViewApply: "",
        listViewApprove: "",
        queryUsers: "?$filter=ResponsablesAbonos eq '{0}'&$select=Title",
        queryEstacion: "?$filter=COAId eq {0}&$select=ResponsablesAbonos/EMail,Title,Id&$expand=ResponsablesAbonos",
        queryEstacionItems: "?$filter={0}&$select=ResponsablesAbonos/EMail,Title,Id&$expand=ResponsablesAbonos",
        queryItems: "?$select=ResponsablesAbonos/EMail,Title,Id&$expand=ResponsablesAbonos",
        queryList: "?$filter=ID eq {0}",
        flowUrl: "https://prod-09.westus.logic.azure.com:443/workflows/5cc55cb921a64d9bbfc0d51218e4a50e/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=jmEKfvgyBrY2JQ21uw7VlYkprM6dXTKoA_2MzG1nvVU"

    },
    global: {
        formName: null,
        createdBy: null,
        created: null,
        approveUsers: "",
        applyUsers: "",
        IdUsers: ""
    },
    initialize: function() {
        var partsOfUrl = window.location.href.split('?')[0].split('/');
        ctrlForm.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];
        eval(ctrlForm.parameters.forms[ctrlForm.global.formName]);
    },
    callFlowService: function() {
        document.getElementById('s4-workspace').scrollTop = 0;
        document.querySelector('.form-content').style.opacity = 0;
        document.querySelector('.loader-container').style.opacity = 1;
        document.querySelector('.loader-container').style.height = "auto";


        ctrlForm.parameters.viewUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[7], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.editUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[1], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.approveUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[4], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.applyUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[6], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.listViewApply = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[9]);
        ctrlForm.parameters.listViewApprove = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[8]);

        var objService = {
            "currentItemID": spForms.currentItemID,
            "currentItem": {
                "createdBy": ctrlForm.global.createdBy,
                "fechaCreacion": ctrlForm.global.created,
                "comentarios": spForms.loadedFields.ComentariosInformacion.value,
                "Correo": spForms.listInfo.title
            },
            "state": spForms.loadedFields.EstadosInformacion.value,
            "titleCorreo": spForms.listInfo.title,
            "approveUsers": ctrlForm.global.approveUsers,
            "applyUsers": ctrlForm.global.applyUsers,
            "siteUrl": sputilities.contextInfo().webAbsoluteUrl,
            "listName": sputilities.contextInfo().listTitle,
            "idConsecutive": ctrlForm.parameters.idConsecutive,
            "entityType": spForms.listInfo.entityItem,
            "viewUrl": ctrlForm.parameters.viewUrl,
            "editUrl": ctrlForm.parameters.editUrl,
            "approveUrl": ctrlForm.parameters.approveUrl,
            "applyUrl": ctrlForm.parameters.applyUrl,
            "listViewApply": ctrlForm.parameters.listViewApply,
            "Contador": ctrlForm.parameters.Contador,
            "JsonPerson": ctrlForm.parameters.ObjectPerson,
            "EmailInicialRevision": ctrlForm.parameters.RevisionInicial,
            "CurrentStation":ctrlForm.parameters.CurrentStation,
            "listViewApprove": ctrlForm.parameters.listViewApprove
        }
        utilities.http.callRestService({
            url: ctrlForm.parameters.flowUrl,
            method: utilities.http.methods.POST,
            data: JSON.stringify(objService),
            headers: {
                "accept": utilities.http.types.jsonOData,
                "content-type": utilities.http.types.jsonOData
            },
            success: function(data) {
                document.querySelector('.loader-container').style.opacity = 0;
                document.querySelector('.form-content').style.opacity = 1;
                document.querySelector('.loader-container').style.height = 0;

                spForms.cancelForm(null);
            },
            error: function(xhr) {
                console.log(xhr);
            }
        });

    },
    getStationCurrentUser: function() {
        var Id = _spPageContextInfo.userId;
    var query = String.format(ctrlForm.parameters.queryUsers,Id)
        sputilities.getItems(ctrlForm.parameters.listNameEstacion, query,
            function(data) {
                var data = utilities.convertToJSON(data);
                if (data != null) {
                    var items = data.d.results;
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i].Title;
                        ctrlForm.parameters.CurrentStation = item;
                        console.log(item);
                                     }
            }
			},
            function(xhr) {
                console.log(xhr);
            }
        );
    },
    
    validateEmailForm: function() {
        var input = document.getElementById(ctrlForm.parameters.ID.Correo);
        if (input != null) {
            input.addEventListener('change', function(event) {
                var email = this.value;
                if (ctrlForm.validateEmail(email)) {
                    var staticName = "[sp-static-name=" + ctrlForm.parameters.ID.Correo + "]";
                    var InputSelect = document.querySelector(staticName);
                    InputSelect.removeAttribute(ctrlForm.parameters.style.Invalid)
                } else {
                    var staticName = "[sp-static-name=" + ctrlForm.parameters.ID.Correo + "]";
                    var InputSelect = document.querySelector(staticName);
                    InputSelect.setAttribute(ctrlForm.parameters.style.Invalid, ctrlForm.parameters.style.Message)
                }
                return false;
            });
        }
    },
      validateEmailAltern: function() {
        var input = document.getElementById(ctrlForm.parameters.ID.CorreoAltern);
        if (input != null) {
            input.addEventListener('change', function(event) {
                var email = this.value;
                if (ctrlForm.validateEmail(email)) {
                    var staticName = "[sp-static-name=" + ctrlForm.parameters.ID.CorreoAltern+ "]";
                    var InputSelect = document.querySelector(staticName);
                    InputSelect.removeAttribute(ctrlForm.parameters.style.Invalid)
                } else {
                    var staticName = "[sp-static-name=" + ctrlForm.parameters.ID.CorreoAltern+ "]";
                    var InputSelect = document.querySelector(staticName);
                    InputSelect.setAttribute(ctrlForm.parameters.style.Invalid, ctrlForm.parameters.style.Message)
                }
                return false;
            });
        }
    },

    validateEmail: function(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },
    changeState: function(state) {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio, state));
        if (radio != null) {
            radio.click();
        }
    },
    hideNotAllowed: function() {
        var html = String.format(ctrlForm.parameters.formats.htmlNotAllowed, ctrlForm.parameters.messages.textNotAllowed);
        var formContent = document.querySelector(ctrlForm.parameters.selectorFormContent);
        if (formContent != null) {
            formContent.innerHTML = html;
        }
    },
    hideNotAllowedRevisores: function() {
        var html = String.format(ctrlForm.parameters.formats.htmlNotAllowed, ctrlForm.parameters.messages.textNotAllowedRevisores);
        var formContent = document.querySelector(ctrlForm.parameters.selectorFormContent);
        if (formContent != null) {
            formContent.innerHTML = html;
        }
    },

    validateUserCreated: function() {
        if (spForms.currentItem.AuthorId != sputilities.contextInfo().userId) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        ctrlForm.hideAttachments();
    },
    validateAllUsers: function() {
        if (spForms.currentItem.AuthorId != sputilities.contextInfo().userId || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
    },
    validateEditUser: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[1])) == null;
        if (spForms.currentItem.AuthorId != sputilities.contextInfo().userId || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        return true;
    },
    //Novedades
    validateApplyUser: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[2])) == null;
        if (ctrlForm.global.applyUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        ctrlForm.hideAttachments();
    },
    //Ventanilla TH
    validateApproveUser: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[0])) == null;
        if (ctrlForm.global.approveUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        ctrlForm.GetAprobadores();
        //ctrlForm.hideAttachments();
    },
       validateUserRevisionAprobadores: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[0])) == null;
        if (ctrlForm.global.IdUsers.split(';').indexOf(sputilities.contextInfo().userId.toString()) == -1 || radio) {
        } else {
            ctrlForm.hideNotAllowedRevisores();
        }
        //ctrlForm.hideAttachments();
    },

    hideAttachments: function() {
        var i;
        var CloaseAttachments = document.querySelectorAll(ctrlForm.parameters.ID.ContentClose);
        for (i = 0; i < CloaseAttachments.length; i++) {
            CloaseAttachments[i].style.display = ctrlForm.parameters.style.None;
        }
        document.querySelector(ctrlForm.parameters.ID.ContentAdd).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.Attachment).style.opacity = ctrlForm.parameters.style.Opacity;
    },

    hideComments: function() {
        var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow, ctrlForm.parameters.columns.comments.staticName));
        if (ctrlComments != null) {
            ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden, true);
        }
    },
    blockedComments: function() {
        var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow, ctrlForm.parameters.columns.comments.staticName));
        if (ctrlComments != null) {
            ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden, true);
        }
    },

    loadReview: function() {
        var actions = [{
            type: spForms.parameters.cancelAction,
            name: ctrlForm.parameters.messages.textCancelForm,
            id: ctrlForm.parameters.messages.idCancelForm,
            callback: null
        }, ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionReview;
        ctrlForm.loadEdit(actions, ctrlForm.validateUserCreated);
    },
    loadApply: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textApplyForm,
                id: ctrlForm.parameters.messages.idApplyForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.changeState(ctrlForm.parameters.states[3]);
                    return true;
                }
            },
            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionApply;
        ctrlForm.loadEdit(actions, ctrlForm.validateApplyUser);

    },
    loadApprove: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textApproveForm,
                id: ctrlForm.parameters.messages.idApproveForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
					ctrlForm.ActualizarEstaciones();                    
                    return true;
                }
            },
            {
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textRejectForm,
                id: ctrlForm.parameters.messages.idRejectForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
                    ctrlForm.changeState(ctrlForm.parameters.states[1]);
                    return true;
                }
            },
            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
        ctrlForm.loadEdit(actions, null);

    },
    loadEditApplicant: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textEditForm,
                id: ctrlForm.parameters.messages.idEditForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.changeState(ctrlForm.parameters.states[0]);
                    ctrlForm.getResponsables();
                    return true;
                }
            },
            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionEdit;
        ctrlForm.loadEdit(actions, function() {
            if (ctrlForm.validateEditUser()) {
                ctrlForm.hideComments();
                var ctrlComments = document.getElementById(ctrlForm.parameters.columns.comments.staticName);
                if (ctrlComments != null) {
                    ctrlComments.value = "";
                }
            }
        });
    },
    loadEdit: function(actions, callBack) {
        ctrlForm.getStationCurrentUser();
        spForms.options = {
            actions: actions,
            description: ctrlForm.parameters.description,
            contentTypeName: ctrlForm.parameters.contentTypeName,
            containerId: ctrlForm.parameters.containerId,
            allowAttachments: ctrlForm.parameters.allowAttachments,
            requiredAttachments: ctrlForm.parameters.requiredAttachments,
            maxAttachments: ctrlForm.parameters.maxAttachments,
            minAttachments: ctrlForm.parameters.minAttachments,
            maxSizeInMB: ctrlForm.parameters.maxSizeInMB,
            successLoadEditForm: function() {
                ctrlForm.global.createdBy = spForms.currentItem.Author.EMail;
                ctrlForm.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(ctrlForm.parameters.formats.dateFormat);
                ctrlForm.formIsLoaded = true;
                
                //ctrlForm.clickSelect();
              	 if(ctrlForm.global.formName == "CargarInformacion"){
					ctrlForm.GetUserStation();
				}
				 if(ctrlForm.global.formName == "CorregirSolicitud"){
					//ctrlForm.GetstateCurrent();
					ctrlForm.LoadHtml();
				}
				if(ctrlForm.global.formName == "VerSolicitud"){
					ctrlForm.ViewUserStation();
				}

				
				
				


                if (typeof callBack == "function") {
                    callBack();
                }
            }
        };
        spForms.loadEditForm();

    },
    loadNew: function() {
        //ctrlForm.getUsers();
        ctrlForm.global.createdBy = sputilities.contextInfo().userEmail;
        ctrlForm.global.created = moment.utc(new Date()).format(ctrlForm.parameters.formats.dateFormat);
        //sputilities.callFunctionOnLoadBody(ctrlForm.parameters.functionLoadMyProperties);
        ctrlForm.loadNewForm();
    },
    loadNewForm: function() {
        spForms.options = {
            actions: [{
                    type: spForms.parameters.saveAction,
                    name: ctrlForm.parameters.messages.textSaveForm,
                    id: ctrlForm.parameters.messages.idSaveForm,
                    success: function() {
                        ctrlForm.callFlowService();
                        return false;
                    },
                    error: function() {
                        return true;
                    },
                    preSave: function() {
                        ctrlForm.changeState(ctrlForm.parameters.states[0]);
                        ctrlForm.getResponsables();
                        return true;
                    }
                },
                {
                    type: spForms.parameters.cancelAction,
                    name: ctrlForm.parameters.messages.textCancelForm,
                    id: ctrlForm.parameters.messages.idCancelForm,
                    callback: null
                },
            ],
            description: ctrlForm.parameters.description,
            contentTypeName: ctrlForm.parameters.contentTypeName,
            containerId: ctrlForm.parameters.containerId,
            allowAttachments: ctrlForm.parameters.allowAttachments,
            requiredAttachments: ctrlForm.parameters.requiredAttachments,
            maxAttachments: ctrlForm.parameters.maxAttachments,
            minAttachments: ctrlForm.parameters.minAttachments,
            maxSizeInMB: ctrlForm.parameters.maxSizeInMB,
            successLoadNewForm: function() {
                ctrlForm.formIsLoaded = true;
                ctrlForm.hideComments();
                ctrlForm.LoadHtml();
                ctrlForm.validateEmailForm();
                ctrlForm.validateEmailAltern();
            }
        };
        spForms.loadNewForm();
    },
    loadMyProperties: function() {
        sputilities.getUserProfileProperties(sputilities.contextInfo().userEmail,
            function(userInfo) {
                var objUser = utilities.convertToJSON(userInfo);
                if (objUser != null) {
                    var displayName = objUser.d.DisplayName;
                    var email = objUser.d.Email;
                    var cellPhone = ctrlForm.findProperty(objUser, ctrlForm.parameters.cellPhone);
                    var validateFormIsLoaded = setInterval(function() {
                        if (ctrlForm.formIsLoaded) {
                            ctrlForm.setValueToControl(ctrlForm.parameters.columns.displayName.staticName, displayName);
                            ctrlForm.setValueToControl(ctrlForm.parameters.columns.email.staticName, email);
                            ctrlForm.setValueToControl(ctrlForm.parameters.columns.cellPhone.staticName, cellPhone);
                            clearInterval(validateFormIsLoaded);
                        }
                    }, 500);
                }
            },
            function(xhr) {
                console.log(xhr)
            }
        );
    },
    setValueToControl: function(staticName, value) {
        if (value != null) {
            var ctrl = document.getElementById(staticName);
            if (ctrl != null) {
                ctrl.value = value;
            }
        }
    },
    click: function(ctrl) {
        var currentSelect = ctrl.value;
        var html = "";
        var contador = 0;
        var responsable = "";
        var query = String.format(ctrlForm.parameters.queryEstacion, currentSelect);
        ctrlForm.parameters.Contador = "";
        sputilities.getItems(ctrlForm.parameters.listNameEstacion, query,
            function(data) {
                var data = utilities.convertToJSON(data);
                if (data != null) {
                    var items = data.d.results;
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        responsable = "";
                        for (var j = 0; j < item.ResponsablesAbonos.results.length; j++) {
                            var user = item.ResponsablesAbonos.results[j];
                            responsable += String.format(ctrlForm.parameters.formats.emailUsers, user.EMail);
                            contador = contador + 1;
                            ctrlForm.parameters.Contador = contador;
                        }
                        var estacion = items[i].Title;
                        var Id = items[i].Id;
                        var item = String.format(ctrlForm.parameters.formats.htmlItemCheckBox, estacion, Id, responsable);
                        html += item;
                    }
                    var check = String.format(ctrlForm.parameters.formats.htmlCheckBox, html);
                    var uno = document.querySelector(ctrlForm.parameters.ID.CheckBodId);
                    uno.innerHTML = check;
                }
            })
    },

    GetUserStation: function() {
		var estaciones = ""; 
        var clicCheckbox = document.querySelectorAll('[sp-static-name="Estacion2"] .list-checkbox-radio .checkbox');
        for (var i = 0; i < clicCheckbox.length; i++) {
            debugger;
            var dos = clicCheckbox[i].childNodes[0].childNodes[0]
            if (dos.checked == true) {
				  var title = dos.value
				  estaciones += title+";";
            } 
            else {
                clicCheckbox[i].style.display = "none";   
            }  
        }
        var query= "";
          var tres = estaciones.split(";")
          	for(var i=0;i<tres.length;i++){
				if(tres[i] != ""){
				if(i == tres.length - 2){
			 	var title = tres[i]
				query +="Title eq '"+title+"'";
	
			}
			else{
			var title = tres[i]
			query +="Title eq '"+title+"' or ";
			}
			}
          
    }
    ctrlForm.getResponsablesCarga(query);
    },
     ViewUserStation: function() {
		var estaciones = ""; 
        var clicCheckbox = document.querySelectorAll('[sp-static-name="Estacion2"] .list-checkbox-radio .checkbox');
        for (var i = 0; i < clicCheckbox.length; i++) {
            debugger;
            var dos = clicCheckbox[i].childNodes[0].childNodes[0]
            if (dos.checked == true) {
				  var title = dos.value
				  estaciones += title+";";
            } 
            else {
                clicCheckbox[i].style.display = "none";   
            }  
        }
    },

    GetstateCurrent: function() {
      var OnclickNameCapacitacion = document.getElementById(ctrlForm.parameters.ID.CoaId);
        OnclickNameCapacitacion.setAttribute(ctrlForm.parameters.ID.functionOnchange, ctrlForm.parameters.ID.JsfunctionOnchange);
		var estaciones = ""; 
        var clicCheckbox = document.querySelectorAll('[sp-static-name="Estacion2"] .list-checkbox-radio .checkbox');
        for (var i = 0; i < clicCheckbox.length; i++) {
            var dos = clicCheckbox[i].childNodes[0].childNodes[0]
            if (dos.checked == true) {
				  var title = dos.value
				  estaciones += title+";";
            } 
            else {
                clicCheckbox[i].style.display = "none";   
            }  
        }
    },
    	getResponsablesCarga: function(query) {
    	var queryItems = query;
	      var query = String.format(ctrlForm.parameters.queryEstacionItems,queryItems);
                sputilities.getItems(ctrlForm.parameters.listNameEstacion, query,
                    function(data) {
                        var data = utilities.convertToJSON(data);
                        if (data != null) {
                            var items = data.d.results;
                            for (var i = 0; i < items.length; i++) {
                                var item = items[i];
                                for (var j = 0; j < item.ResponsablesAbonos.results.length; j++) {
                                    var user = item.ResponsablesAbonos.results[j];
                                    ctrlForm.global.approveUsers += String.format(ctrlForm.parameters.formats.emailUsers, user.EMail);
                                }
                            }

                        }
                        ctrlForm.validateApproveUser();
                        //
                    })
	},
    getResponsables: function() {
    var contador = 0;
        var clicCheckbox = document.querySelectorAll('[sp-static-name="Estacion2"] .list-checkbox-radio input');
        for (var i = 0; i < clicCheckbox.length; i++) {
            if (clicCheckbox[i].checked == true) {
                var Email = clicCheckbox[i].getAttribute('data-Responsable');
                contador = contador+1;
                ctrlForm.global.approveUsers += String.format(ctrlForm.parameters.formats.emailUsersResponsables, Email);
            }
        }
                document.getElementById(ctrlForm.parameters.ID.IdEstaciones).value = contador;
    },
     ActualizarEstaciones: function() {
        var id = spForms.currentItemID;
        var query = String.format(ctrlForm.parameters.queryList, id);
        sputilities.getItems(ctrlForm.parameters.ListName, query,
            function(data) {
                var data = utilities.convertToJSON(data);
                if (data != null) {
                    var numero = data.d.results[0].NumeroRevisoresAprobados;
                    var ObjectPerson = data.d.results[0].EstacionesAprobadas;
                    var JsonPersonas = JSON.parse(ObjectPerson)
                    if (numero == 0) {
                        spForms.loadedFields.EstadosInformacion.value = ctrlForm.parameters.states[2];
                        var i = 0;
                        var contador = document.getElementById(ctrlForm.parameters.ID.IdNumeroEstacionesAprobadas).value = i;
                        contador = contador + 1;
                        ctrlForm.parameters.Contador = contador;
                        ctrlForm.ActualizarJson(JsonPersonas);
                    } else {
                    	spForms.loadedFields.EstadosInformacion.value = ctrlForm.parameters.states[2];
                        var contador = data.d.results[0].NumeroRevisoresAprobados;
                        contador = contador + 1;
                        ctrlForm.parameters.Contador = contador;
                        ctrlForm.ActualizarJson(JsonPersonas);
                    }
                    if (ctrlForm.parameters.Contador == spForms.loadedFields.CantidadRevisores.value) {
                        spForms.loadedFields.EstadosInformacion.value = ctrlForm.parameters.states[3];
                    } else {
                        ctrlForm.parameters.RevisionInicial = false
                    }
                }
            },
            function(xhr) {
                console.log(xhr);
            }
        );
    },
	ActualizarJson: function(JsonPersonas) {
        var state = ctrlForm.parameters.states[2];
        var nombre = _spPageContextInfo.userDisplayName;
        var today = new Date();
        var Fecha = today.format(ctrlForm.parameters.formats.dateFormatJson);
        var IdUser = _spPageContextInfo.userId;
        var estacion = ctrlForm.parameters.CurrentStation
        var estado = "Aprobación Estación";
        if(JsonPersonas == null){
        	 var JsonPersonas = {
                    "Personas": []
                }
        JsonPersonas.Personas.push({
            'Aprobador': '' + nombre + '',
            'Estado': '' + estado + '',
            'fecha': '' + Fecha + '',
            'IdUser': '' + IdUser + '',
            'Estacion': '' + estacion + ''            
        });
        }else{
          JsonPersonas.Personas.push({
            'Aprobador': '' + nombre + '',
            'Estado': '' + estado + '',
            'fecha': '' + Fecha + '',
            'IdUser': '' + IdUser + '',
            'Estacion': '' + estacion + ''              
        });

        }
	
        var jsonRevisoresComplete = JSON.stringify(JsonPersonas)
        ctrlForm.parameters.ObjectPerson = jsonRevisoresComplete;
    },
    GetAprobadores: function() {
        var ObjectPerson = spForms.loadedFields.EstacionesAprobadas.value;
        if(ObjectPerson != undefined){
         var JsonPersonas = JSON.parse(ObjectPerson)
        var i = 0;
        var RevisoresFilter = JsonPersonas.Personas.filter(function(entry) {
            return entry.Estado === 'Aprobación Estación';
        });
        for (i; i < RevisoresFilter.length; i++) {
            var item = RevisoresFilter[i].IdUser;
            ctrlForm.global.IdUsers += String.format(ctrlForm.parameters.formats.emailUsers, item);
        }
        ctrlForm.validateUserRevisionAprobadores();
        }
       
    },  
    
    LoadHtml: function(ctrl) {
        var check = document.querySelectorAll(ctrlForm.parameters.ID.CheckRemove);
        for (var i = 0; i < check.length; i++) {
            check[i].remove();
        }
        var OnclickNameCapacitacion = document.getElementById(ctrlForm.parameters.ID.CoaId);
        OnclickNameCapacitacion.setAttribute(ctrlForm.parameters.ID.functionOnchange, ctrlForm.parameters.ID.JsfunctionOnchange);
    },
    findProperty: function(objUser, name) {
        var properties = objUser.d.UserProfileProperties.results;
        for (var i = 0; i < properties.length; i++) {
            var property = properties[i];
            if (property.Key == name) {
                return property.Value;
            }
        }
    },
    redirectCustomForm: function(name) {
        window.location.href = window.location.href.replace(ctrlForm.global.formName, name);
    }
}
ctrlForm.initialize();