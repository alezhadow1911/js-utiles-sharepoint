﻿var compensatoryForm = {
	parameters:{
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']"
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico'			
		},
		resultItems:null,
		invalid:"sp-invalid",
		userProperties:"CellPhone", 
		errorInvalid:"El correo electrónico no tiene el formato correcto",
		loadUserInfo:"compensatoryForm.loadUserInfo",
		formsIsLoaded:false,
		formsParameters:{
			title:"Solicitud Compensatorio Jornada Electoral",
			description:"",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Compensatorio Jornada Electoral",
			containerId:"container-form"			
		}

	},	
	initialize:function(){
		
		spForms.loadEditForm();
	}
	
}

spForms.options={
	actions:[
		{
			type:spForms.parameters.cancelAction,
			name:compensatoryForm.parameters.formsParameters.buttons.Cancel.Text,
			id:compensatoryForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:compensatoryForm.parameters.formsParameters.title,
	description:compensatoryForm.parameters.formsParameters.description,
	contentTypeName:compensatoryForm.parameters.formsParameters.contentType,
	containerId:compensatoryForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:5,
  	minAttachments:1,
  	maxSizeInMB:3,
	successLoadEditForm:function(){		
	} 
};
compensatoryForm.initialize();