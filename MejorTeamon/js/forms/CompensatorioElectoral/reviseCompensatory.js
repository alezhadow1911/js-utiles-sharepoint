﻿var compensatoryForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']",
			Comentarios:"[sp-static-name='ComentariosTH']",
			ComentariosJefe:"[sp-static-name='ComentariosJefe']"		
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico',	
			Comentarios:"ComentariosTH",
			ComentariosJefe:"ComentariosJefe",
			Estado:"EstadoCompensatorio"		
		},		
		controlsHtml:{
            Attachment:"sp-forms-add-attachment",
            Enviar:"btn-enviar",            
			Aprobar:"btn-aprobar",
			Corregir:"btn-corregir",
			Aplicar:"btn-aplicar",
			Rechazar:"btn-rechazar",
			classDelete:".delete-doc",
			classGroup:".form-group.row",
			colorDisable:"#B1B1B1",
			borderDisable:"#E1E1E1",
			iconTH:"ico-help-ComentariosTH",
			iconJefe:"ico-help-ComentariosJefe"
		},
		bodyItem:"<div class='form-group row'><label>Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior</label></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
		containerForm:".form-content",
		TitlePopUp:"Acceso Denegado",
    	htmlRequiered:{block:"block",none:"none"},	            
		invalid:"sp-invalid",
		userProperties:"CellPhone", 
		errorInvalid:"El correo electrónico no tiene el formato correcto",
        codeStates:{Solicitado:"0",Ventanilla:"1",Novedades:"2",Aplicada:"3",Corregir:"4",Jefe:"5",Rechazado:"6"},		
		formsParameters:{
			title:"Corregir Solicitud Compensatorio Jornada Electoral",
			description:"Completa la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Compensatorio Jornada Electoral",
			containerId:"container-form"			
		},		
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud",
			"AprobarJefe":"Aprobar Jefe Solicitud",
			"AplicarSolicitud":"Aplicar Solicitud",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Novedades":"Vista Novedades",
			"Ventanilla":"Vista Ventanilla"
		},
		flowParameters:{
			urlFlow:"https://prod-30.westus.logic.azure.com:443/workflows/a7e12ca343434083963332a14ed9cbf3/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=K-r_qIfhuR4TMoYSBI0FlvAzX8YBGNiJXnWNVE62UF8",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",
	   		approveBossUrl:"",
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Compensatorio Jornada Electoral Ventanilla TH",
	    	applyUsers:"Compensatorio Jornada Electoral Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:11,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Compensatorio Jornada Electoral"
		},
		global:{		
			createdBy:null,
			created:null
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
	    compensatoryForm.parameters.flowParameters.approveUrl = String.format(compensatoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(compensatoryForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    compensatoryForm.parameters.flowParameters.approveBossUrl = String.format(compensatoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(compensatoryForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    compensatoryForm.parameters.flowParameters.applyUrl = String.format(compensatoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(compensatoryForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    compensatoryForm.parameters.flowParameters.editUrl = String.format(compensatoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(compensatoryForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    compensatoryForm.parameters.flowParameters.listViewApply = String.format(compensatoryForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(compensatoryForm.parameters.forms)[6]);
	    compensatoryForm.parameters.flowParameters.listViewApprove= String.format(compensatoryForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(compensatoryForm.parameters.forms)[7]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":compensatoryForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":compensatoryForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosTH.value,
		        "comentariosJefe":spForms.loadedFields.ComentariosJefe.value,
		        "fechaCompensatorio":moment.utc(new Date(spForms.loadedFields.FechaCompensatorio.value)).format(compensatoryForm.parameters.formats.dateFormat)
		    },
		    "state":spForms.loadedFields.EstadoCompensatorio.value,
		    "approveUsers":compensatoryForm.parameters.flowParameters.approveUsers,
		    "applyUsers":compensatoryForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":compensatoryForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":compensatoryForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":compensatoryForm.parameters.flowParameters.listNameAssing,
		    "editUrl":compensatoryForm.parameters.flowParameters.editUrl,
		    "approveUrl":compensatoryForm.parameters.flowParameters.approveUrl,
		    "applyUrl":compensatoryForm.parameters.flowParameters.applyUrl,
		    "approveBossUrl":compensatoryForm.parameters.flowParameters.approveBossUrl,
		    "listViewApply":compensatoryForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":compensatoryForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	compensatoryForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:compensatoryForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){
		
		spForms.loadEditForm();
	},
	validateFormat:function(){
		var rowEmail = document.querySelector(compensatoryForm.parameters.fieldsHTML.CorreoElectronico);
		var email = document.getElementById(compensatoryForm.parameters.internalNameFields.CorreoElectronico);
		var isValid = true;		
		if(email != null && rowEmail!= null)
		{			
			if(compensatoryForm.validateEmail(email.value) || email.value == "")
			{
				rowEmail.removeAttribute(compensatoryForm.parameters.invalid);
			}
			else
			{
				rowEmail.setAttribute(compensatoryForm.parameters.invalid,compensatoryForm.parameters.errorInvalid);
			}
		}
		else
		{
			isValid=false;
		}
		
		var state = document.getElementById(compensatoryForm.parameters.internalNameFields.Estado);
		if(state != null)
		{
			state.value = compensatoryForm.parameters.codeStates.Ventanilla;
		}
		else
		{
			isValid = false;
		}
		return isValid;

	},
	validateEmail: function (email){
		 var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);
	
	},	
	loadResponsableState:function(){
	
		try
		{
			var currentUserId =_spPageContextInfo.userId;		
		
			var find = false;
			var state = document.getElementById(compensatoryForm.parameters.internalNameFields.Estado);
			if(state != null)
			{
				if(spForms.currentItem.AuthorId == currentUserId){
					find = true;
				}
								
				if(state.value != compensatoryForm.parameters.codeStates.Corregir)
				{				
					compensatoryForm.hideNotAllowed();
				}
				else
				{
					if(!find)
					{
						compensatoryForm.hideNotAllowed();
					}
				}				
			}
		}
		catch(e){
			console.log(e);
		}	
	},	
	hideNotAllowed:function(){
		var html = compensatoryForm.parameters.bodyItem;
		var formContent = document.querySelector(compensatoryForm.parameters.containerForm);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	}

}

spForms.options={
	actions:[
		{
			type:spForms.parameters.updateAction,
			name:compensatoryForm.parameters.formsParameters.buttons.Send.Text,
			id:compensatoryForm.parameters.formsParameters.buttons.Send.Id,
			success:function (){
				compensatoryForm.callFlowService();
				return false;
			},
			error:function (){},
			preSave: function (){
				return compensatoryForm.validateFormat()
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:compensatoryForm.parameters.formsParameters.buttons.Cancel.Text,
			id:compensatoryForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:compensatoryForm.parameters.formsParameters.title,
	description:compensatoryForm.parameters.formsParameters.description,
	contentTypeName:compensatoryForm.parameters.formsParameters.contentType,
	containerId:compensatoryForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:5,
  	minAttachments:1,
  	maxSizeInMB:3,
	successLoadEditForm:function(){
		compensatoryForm.loadResponsableState();
		compensatoryForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
		compensatoryForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(compensatoryForm.parameters.formats.dateFormat);

	} 
};
compensatoryForm.initialize();