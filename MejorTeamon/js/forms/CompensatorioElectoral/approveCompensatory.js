﻿var compensatoryForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		query:"?$Select=FlujoAsociado/Title,ResponsablesAbonos/EMail,ID&$expand=FlujoAsociado,ResponsablesAbonos&$filter=Title eq '{0}'",
		listName:"ResponsablesTH",
		assingTo:{
			Ventanilla:"Compensatorio Jornada Electoral Ventanilla TH",
			Novedades:"Compensatorio Jornada Electoral Novedades"
		},
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']",
			Comentarios:"[sp-static-name='ComentariosTH']",
			ComentariosJefe:"[sp-static-name='ComentariosJefe']"		
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico',	
			Comentarios:"ComentariosTH",
			ComentariosJefe:"ComentariosJefe",
			Estado:"EstadoCompensatorio"		
		},		
		controlsHtml:{
			Attachment:"sp-forms-add-attachment",
			Aprobar:"btn-aprobar",
			Corregir:"btn-corregir",
			Aplicar:"btn-aplicar",
			Rechazar:"btn-rechazar",
			classDelete:".delete-doc",
			classGroup:".form-group.row",
			colorDisable:"#B1B1B1",
			borderDisable:"#E1E1E1",
			iconTH:"ico-help-ComentariosTH",
			iconJefe:"ico-help-ComentariosJefe"
		},
		errorInvalid:{text:"Los comentarios son obligatorios",area:"textarea"},
		htmlRequiered:{label:"label",block:"block",none:"none"},		
		invalid:"sp-invalid",
		hidden:"sp-hidden",		
		resultItems:null,	
		codeStates:{Solicitado:"0",Ventanilla:"1",Novedades:"2",Aplicada:"3",Corregir:"4",Jefe:"5",Rechazado:"6"},	
		userProperties:"CellPhone",
		bodyItem:"<div class='form-group row'><label>Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior</label></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
		containerForm:".form-content",
		TitlePopUp:"Acceso Denegado",
		formsParameters:{
			title:"",
			description:"",
			titleApprove:"Aprobar/ Corregir Solicitud Compensatorio Jornada Electoral",
			titleAplicate:"Aplicar Solicitud Compensatorio Jornada Electoral",
			descriptionApprove:"Completa la información correspondiente, para aprobar o rechazar la solicitud",
			descriptionAplicate:"Revisa la información de la solicitud, para aplicar la novedad",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Compensatorio Jornada Electoral",
			containerId:"container-form"			
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"compensatoryForm.loadApprove()",
			"AprobarJefe":"compensatoryForm.loadApproveBoss()",
			"AplicarSolicitud":"compensatoryForm.loadApplicate()",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Novedades":"Vista Novedades",
			"Ventanilla":"Vista Ventanilla"
		},
		flowParameters:{
			urlFlow:"https://prod-30.westus.logic.azure.com:443/workflows/a7e12ca343434083963332a14ed9cbf3/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=K-r_qIfhuR4TMoYSBI0FlvAzX8YBGNiJXnWNVE62UF8",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",
	   		approveBossUrl:"",
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Compensatorio Jornada Electoral Ventanilla TH",
	    	applyUsers:"Compensatorio Jornada Electoral Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:11,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Compensatorio Jornada Electoral"
		},
		formsState:{
			AprobarSolicitud:"1",
			AprobarJefe:"5",
			AplicarSolicitud:"2"
		},
		global:{
			formName:null,
			createdBy:null,
			created:null

		},
		initialState:null

	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
	    compensatoryForm.parameters.flowParameters.approveUrl = String.format(compensatoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(compensatoryForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    compensatoryForm.parameters.flowParameters.approveBossUrl = String.format(compensatoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(compensatoryForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    compensatoryForm.parameters.flowParameters.applyUrl = String.format(compensatoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(compensatoryForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    compensatoryForm.parameters.flowParameters.editUrl = String.format(compensatoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(compensatoryForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    compensatoryForm.parameters.flowParameters.listViewApply = String.format(compensatoryForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(compensatoryForm.parameters.forms)[6]);
	    compensatoryForm.parameters.flowParameters.listViewApprove= String.format(compensatoryForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(compensatoryForm.parameters.forms)[7]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":compensatoryForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":compensatoryForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosTH.value,
		        "comentariosJefe":spForms.loadedFields.ComentariosJefe.value,
		        "fechaCompensatorio":moment.utc(new Date(spForms.loadedFields.FechaCompensatorio.value)).format(compensatoryForm.parameters.formats.dateFormat)
		    },
		    "state":spForms.loadedFields.EstadoCompensatorio.value,
		    "approveUsers":compensatoryForm.parameters.flowParameters.approveUsers,
		    "applyUsers":compensatoryForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":compensatoryForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":compensatoryForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":compensatoryForm.parameters.flowParameters.listNameAssing,
		    "editUrl":compensatoryForm.parameters.flowParameters.editUrl,
		    "approveUrl":compensatoryForm.parameters.flowParameters.approveUrl,
		    "applyUrl":compensatoryForm.parameters.flowParameters.applyUrl,
		    "approveBossUrl":compensatoryForm.parameters.flowParameters.approveBossUrl,
		    "listViewApply":compensatoryForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":compensatoryForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	compensatoryForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:compensatoryForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){			
		var partsOfUrl = window.location.href.split('?')[0].split('/');
		compensatoryForm.parameters.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];
		eval(compensatoryForm.parameters.forms[compensatoryForm.parameters.global.formName]);

	},
	loadApprove:function(){	
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:compensatoryForm.parameters.formsParameters.buttons.Aprove.Text,
				id:compensatoryForm.parameters.formsParameters.buttons.Aprove.Id,
				success:function (){
					compensatoryForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return compensatoryForm.validateApprove()
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:compensatoryForm.parameters.formsParameters.buttons.Revise.Text,
				id:compensatoryForm.parameters.formsParameters.buttons.Revise.Id,
				success:function (){
					compensatoryForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return compensatoryForm.validateReject()
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:compensatoryForm.parameters.formsParameters.buttons.Cancel.Text,
				id:compensatoryForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		compensatoryForm.parameters.formsParameters.description = compensatoryForm.parameters.formsParameters.descriptionApprove;
		compensatoryForm.parameters.formsParameters.title = compensatoryForm.parameters.formsParameters.titleApprove;
		var query = String.format(compensatoryForm.parameters.query,compensatoryForm.parameters.assingTo.Ventanilla);
		compensatoryForm.parameters.query = query;	
		compensatoryForm.loadEdit(actions,compensatoryForm.loadResponsableState);
		
	},
	loadApproveBoss:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:compensatoryForm.parameters.formsParameters.buttons.Aprove.Text,
				id:compensatoryForm.parameters.formsParameters.buttons.Aprove.Id,
				success:function (){
					compensatoryForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return compensatoryForm.validateApprove()
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:compensatoryForm.parameters.formsParameters.buttons.Reject.Text,
				id:compensatoryForm.parameters.formsParameters.buttons.Reject.Id,
				success:function (){
					compensatoryForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return compensatoryForm.validateReject()
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:compensatoryForm.parameters.formsParameters.buttons.Cancel.Text,
				id:compensatoryForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		compensatoryForm.parameters.formsParameters.description = compensatoryForm.parameters.formsParameters.descriptionApprove;
		compensatoryForm.parameters.formsParameters.title = compensatoryForm.parameters.formsParameters.titleApprove;
        compensatoryForm.loadEdit(actions,compensatoryForm.loadResponsableState);
	
	},
	loadApplicate:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:compensatoryForm.parameters.formsParameters.buttons.Aplicate.Text,
				id:compensatoryForm.parameters.formsParameters.buttons.Aplicate.Id,
				success:function (){
					compensatoryForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return compensatoryForm.validateApprove()
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:compensatoryForm.parameters.formsParameters.buttons.Cancel.Text,
				id:compensatoryForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		compensatoryForm.parameters.formsParameters.description = compensatoryForm.parameters.formsParameters.descriptionAplicate;
		compensatoryForm.parameters.formsParameters.title = compensatoryForm.parameters.formsParameters.titleAplicate;
		var query = String.format(compensatoryForm.parameters.query,compensatoryForm.parameters.assingTo.Novedades);
		compensatoryForm.parameters.query = query;
		compensatoryForm.loadEdit(actions,compensatoryForm.loadResponsableState);

	},
	loadEdit:function(actions,sucessFunction){
		spForms.options={	
			actions:actions,
			title:compensatoryForm.parameters.formsParameters.title,
			description:compensatoryForm.parameters.formsParameters.description,
			contentTypeName:compensatoryForm.parameters.formsParameters.contentType,
			containerId:compensatoryForm.parameters.formsParameters.containerId,
			allowAttachments:true,
		  	requiredAttachments:true,
		  	maxAttachments:5,
		  	minAttachments:1,
		  	maxSizeInMB:3,
			successLoadEditForm:function(){	
				compensatoryForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
				compensatoryForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(compensatoryForm.parameters.formats.dateFormat);			
				if(typeof sucessFunction== "function"){
					sucessFunction();
				}
			} 
		};
		spForms.loadEditForm();
	},
	validateApprove:function(){
		var row = compensatoryForm.parameters.initialState == compensatoryForm.parameters.codeStates.Ventanilla ? document.querySelector(compensatoryForm.parameters.fieldsHTML.Comentarios) : document.querySelector(compensatoryForm.parameters.fieldsHTML.ComentariosJefe);
		var state = document.getElementById(compensatoryForm.parameters.internalNameFields.Estado);
		var isValid = true;
		if(compensatoryForm.parameters.initialState == compensatoryForm.parameters.codeStates.Ventanilla)
		{			
			if(row != null && state!= null){
				row.removeAttribute(compensatoryForm.parameters.invalid);
				state.value = compensatoryForm.parameters.codeStates.Novedades;
			}
			else
			{
				isValid = false;
			}
		}
		if(compensatoryForm.parameters.initialState == compensatoryForm.parameters.codeStates.Jefe)
		{		
			if(row != null && state!= null){
				row.removeAttribute(compensatoryForm.parameters.invalid);
				state.value = compensatoryForm.parameters.codeStates.Ventanilla;
			}
			else
			{
				isValid = false;
			}
		}
		if(compensatoryForm.parameters.initialState == compensatoryForm.parameters.codeStates.Novedades)
		{	
			if(state!= null){				
				state.value = compensatoryForm.parameters.codeStates.Aplicada;
			}
			else
			{
				isValid = false;
			}
		}
		return isValid;
		
	},
	validateReject:function(){
		var row = compensatoryForm.parameters.initialState == compensatoryForm.parameters.codeStates.Ventanilla ? document.querySelector(compensatoryForm.parameters.fieldsHTML.Comentarios): document.querySelector(compensatoryForm.parameters.fieldsHTML.ComentariosJefe);
		var state = document.getElementById(compensatoryForm.parameters.internalNameFields.Estado);		
		var isValid = true;
		
		if(row != null && state != null)
		{
			var comments = row.querySelector(compensatoryForm.parameters.errorInvalid.area);
			if(comments.value != "")
			{
				row.removeAttribute(compensatoryForm.parameters.invalid);
				if(compensatoryForm.parameters.initialState == compensatoryForm.parameters.codeStates.Jefe)
				{
					state.value = compensatoryForm.parameters.codeStates.Rechazado;
				}
				else
				{
					state.value = compensatoryForm.parameters.codeStates.Corregir;

				}			
			}
			else
			{
				row.setAttribute(compensatoryForm.parameters.invalid,compensatoryForm.parameters.errorInvalid.text);			
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;

	},	
	loadResponsableState:function(){	
		try
		{
			var userCurrent = _spPageContextInfo.userEmail
			var state = document.getElementById(compensatoryForm.parameters.internalNameFields.Estado);
			if(state != null)
			{
				if(state.value == compensatoryForm.parameters.formsState[compensatoryForm.parameters.global.formName])
				{
					if(state.value == compensatoryForm.parameters.codeStates.Novedades || state.value == compensatoryForm.parameters.codeStates.Ventanilla)
					{
						sputilities.getItems(compensatoryForm.parameters.listName,compensatoryForm.parameters.query,function(data){
							
							var resultItems = JSON.parse(data).d.results;
							if(resultItems.length > 0)
							{
								var items = resultItems[0].ResponsablesAbonos.results
								var find = false;
								for(var i=0;i<items.length;i++)
								{
									if(items[i].EMail == userCurrent){
										find = true;
									 	break;
									}
								}
								
								compensatoryForm.parameters.initialState = state.value;
								
								if(state.value == compensatoryForm.parameters.codeStates.Novedades)
								{				
									if(find)
									{
										compensatoryForm.showComments(state.value);
									}
									else
									{										
										compensatoryForm.hideNotAllowed();
									}
								}
								else if(state.value == compensatoryForm.parameters.codeStates.Ventanilla)
								{						
									if(find)
									{
										compensatoryForm.showComments(state.value);
									}
									else
									{										
										compensatoryForm.hideNotAllowed();
									}
								}
							}						
						});
					}
					else
					{
	                    var idUserCreated =  spForms.currentItem.AuthorId;
	                    
						sputilities.getUserInformation(idUserCreated,function(data){
	                        //save the results to an array
	                        var resultItems = utilities.convertToJSON(data);
	                        if(resultItems != null)
	                        {
	                            var email = resultItems.d.EMail;
	
	                            sputilities.getUserProfileProperty('Manager',email,function(data){
	                                var resultProperty =  utilities.convertToJSON(data);
	                                if(resultProperty != null)
	                                {
	                                    var emailJefe = resultProperty.d.GetUserProfilePropertyFor.split('|')[2];
	                                    var find = false;
	
	                                    if(userCurrent == emailJefe)
	                                    {
	                                    	find = true;
	                                    }
	                                    compensatoryForm.parameters.initialState = state.value;

	                                    if(state.value == compensatoryForm.parameters.codeStates.Jefe)
										{				
											if(find)
											{
												compensatoryForm.showComments(state.value);
											}
											else
											{												
												compensatoryForm.hideNotAllowed();
											}
									   }	
	                                    
	                                }
	                            });
	                        }
	                    
	                    });
                    }
                }
                else
                {                    
                    compensatoryForm.hideNotAllowed();
                }
		    }
		}
		catch(e){
			console.log(e);
		}	
	},		
	showComments:function(state){
		if(state == compensatoryForm.parameters.codeStates.Jefe)
		{
			var comments = document.querySelector(compensatoryForm.parameters.fieldsHTML.ComentariosJefe);	
			if(comments != null)
			{
				comments.setAttribute(compensatoryForm.parameters.hidden,false);
				comments.querySelector(compensatoryForm.parameters.errorInvalid.area).value="";
	
			}
		}
		else if(state == compensatoryForm.parameters.codeStates.Ventanilla)
		{
			var commentsJefe = document.querySelector(compensatoryForm.parameters.fieldsHTML.ComentariosJefe);	
			var comments = document.querySelector(compensatoryForm.parameters.fieldsHTML.Comentarios);	
			if(comments != null && commentsJefe != null)
			{
				comments.setAttribute(compensatoryForm.parameters.hidden,false);
				commentsJefe.setAttribute(compensatoryForm.parameters.hidden,false);
				comments.querySelector(compensatoryForm.parameters.errorInvalid.area).value="";
	
			}
		}
		else if(state == compensatoryForm.parameters.codeStates.Novedades)
		{
			var commentsJefe = document.querySelector(compensatoryForm.parameters.fieldsHTML.ComentariosJefe);	
			var comments = document.querySelector(compensatoryForm.parameters.fieldsHTML.Comentarios);	
			if(comments != null && commentsJefe != null)
			{
				comments.setAttribute(compensatoryForm.parameters.hidden,false);
				commentsJefe.setAttribute(compensatoryForm.parameters.hidden,false);
			}
		}
	},
	hideNotAllowed:function(){
		var html = compensatoryForm.parameters.bodyItem;
		var formContent = document.querySelector(compensatoryForm.parameters.containerForm);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	}
}
compensatoryForm.initialize();