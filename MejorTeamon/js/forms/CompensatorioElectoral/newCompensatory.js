﻿var compensatoryForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']"
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico'			
		},
		resultItems:null,
		invalid:"sp-invalid",
		userProperties:"CellPhone", 
		errorInvalid:"El correo electrónico no tiene el formato correcto",
		loadUserInfo:"compensatoryForm.loadUserInfo",
		formsIsLoaded:false,
		formsParameters:{
			title:"Solicitud Compensatorio Jornada Electoral",
			description:"Completa la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Compensatorio Jornada Electoral",
			containerId:"container-form"			
		},
		flowParameters:{
			urlFlow:"https://prod-30.westus.logic.azure.com:443/workflows/a7e12ca343434083963332a14ed9cbf3/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=K-r_qIfhuR4TMoYSBI0FlvAzX8YBGNiJXnWNVE62UF8",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",
	   		approveBossUrl:"",
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Compensatorio Jornada Electoral Ventanilla TH",
	    	applyUsers:"Compensatorio Jornada Electoral Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:11,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Compensatorio Jornada Electoral"
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud Ventanilla TH",
			"AprobarJefe":"Aprobar Jefe Inmediato",
			"AplicarSolicitud":"Aplicar Novedad",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Novedades":"Vista Novedades",
			"Ventanilla":"Vista Ventanilla"
		},
		global:{
			createdBy:null,
			created:null
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
	    compensatoryForm.parameters.flowParameters.approveUrl = String.format(compensatoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(compensatoryForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    compensatoryForm.parameters.flowParameters.approveBossUrl = String.format(compensatoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(compensatoryForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    compensatoryForm.parameters.flowParameters.applyUrl = String.format(compensatoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(compensatoryForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    compensatoryForm.parameters.flowParameters.editUrl = String.format(compensatoryForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(compensatoryForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    compensatoryForm.parameters.flowParameters.listViewApply = String.format(compensatoryForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(compensatoryForm.parameters.forms)[6]);
	    compensatoryForm.parameters.flowParameters.listViewApprove= String.format(compensatoryForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(compensatoryForm.parameters.forms)[7]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":compensatoryForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":compensatoryForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosTH.value,
		        "comentariosJefe":spForms.loadedFields.ComentariosJefe.value,
		        "fechaCompensatorio":moment.utc(new Date(spForms.loadedFields.FechaCompensatorio.value)).format(compensatoryForm.parameters.formats.dateFormat)
		    },
		    "state":spForms.loadedFields.EstadoCompensatorio.value,
		    "approveUsers":compensatoryForm.parameters.flowParameters.approveUsers,
		    "applyUsers":compensatoryForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":compensatoryForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":compensatoryForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":compensatoryForm.parameters.flowParameters.listNameAssing,
		    "editUrl":compensatoryForm.parameters.flowParameters.editUrl,
		    "approveUrl":compensatoryForm.parameters.flowParameters.approveUrl,
		    "applyUrl":compensatoryForm.parameters.flowParameters.applyUrl,
		    "approveBossUrl":compensatoryForm.parameters.flowParameters.approveBossUrl,
		    "listViewApply":compensatoryForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":compensatoryForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	compensatoryForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:compensatoryForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){
		sputilities.callFunctionOnLoadBody(compensatoryForm.parameters.loadUserInfo);
		spForms.loadNewForm();
	},
	
	loadUserInfo:function(){
		try {
			//running the query with parameters
			sputilities.getMyUserProfileProperties("",function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d;
				compensatoryForm.parameters.resultItems = resultItems;
				var ctrlInterval = setInterval(function(){
					if(compensatoryForm.parameters.formsIsLoaded)
					{
						compensatoryForm.setValuesUserInfo();
						clearInterval(ctrlInterval);
					}
				},500);
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	setValuesUserInfo:function(){
	
		if(compensatoryForm.parameters.resultItems != null)
		{
		  	var name = document.getElementById(compensatoryForm.parameters.internalNameFields.NombrePersona);
		  	var email = document.getElementById(compensatoryForm.parameters.internalNameFields.CorreoElectronico);
		  	var cellPhone = document.getElementById(compensatoryForm.parameters.internalNameFields.NumeroCelular);
		  	if(name != null){
		  		name.value = compensatoryForm.parameters.resultItems.DisplayName ? compensatoryForm.parameters.resultItems.DisplayName: "";
		  	}
		  	
		  	if(email != null){
		  		email.value = compensatoryForm.parameters.resultItems.Email ? (compensatoryForm.parameters.resultItems.Email).toLowerCase(): "";
		  	}
		  	
		  	if(cellPhone != null){
		  	
		  		var userProperties = compensatoryForm.parameters.resultItems.UserProfileProperties.results;
		  	
			  	for(var i=0;i<userProperties.length;i++)
			  	{
			  		if(userProperties[i].Key == compensatoryForm.parameters.userProperties)
			  		{
			  		 	cellPhone.value =  userProperties[i].Value;
			  		 	break;
			  		}
			  	}
			}
	  	}
	},
	validateFormat:function(){
		var rowEmail = document.querySelector(compensatoryForm.parameters.fieldsHTML.CorreoElectronico);
		var email = document.getElementById(compensatoryForm.parameters.internalNameFields.CorreoElectronico);
		var isValid = true;		
		if(email != null && rowEmail!= null)
		{			
			if(compensatoryForm.validateEmail(email.value) || email.value == "")
			{
				rowEmail.removeAttribute(compensatoryForm.parameters.invalid);
			}
			else
			{
				rowEmail.setAttribute(compensatoryForm.parameters.invalid,compensatoryForm.parameters.errorInvalid);
			}
		}
		else
		{
			isValid=false;
		}
		return isValid;
		
	},
	validateEmail: function (email){
		 var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);
	
	}
}

spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:compensatoryForm.parameters.formsParameters.buttons.Send.Text,
			id:compensatoryForm.parameters.formsParameters.buttons.Send.Id,
			success:function (){
				compensatoryForm.callFlowService();
				return false;
			},
			error:function (){},
			preSave: function (){
				return compensatoryForm.validateFormat()
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:compensatoryForm.parameters.formsParameters.buttons.Cancel.Text,
			id:compensatoryForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:compensatoryForm.parameters.formsParameters.title,
	description:compensatoryForm.parameters.formsParameters.description,
	contentTypeName:compensatoryForm.parameters.formsParameters.contentType,
	containerId:compensatoryForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:5,
  	minAttachments:1,
  	maxSizeInMB:3,
	successLoadNewForm:function(){
		compensatoryForm.parameters.global.createdBy = sputilities.contextInfo().userEmail;
		compensatoryForm.parameters.global.created = moment.utc(new Date()).format(compensatoryForm.parameters.formats.dateFormat);
		compensatoryForm.parameters.formsIsLoaded = true;
	} 
};
compensatoryForm.initialize();