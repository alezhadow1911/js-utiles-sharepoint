﻿var Legalizacion = {
	//create parameters
	parameters:{
			IdEnviar: "Enviar",
			IdCancelar:"Cancelar",
			ContentType:"Legalizacion Subsidio Educativo",
			IdDiv:"spForm",
			TitleForm:"Legalizacion Subsidio Educativo",
			IDSolicitud: "TipoSolicitud",
			fields:[{field:"NombrePersona1"},{field:"NumeroCelular"},{field:"CorreoElectronico"}],
				
	htmlRequiered:{
			label:'label',
			StaticName:	"sp-static-name",
			Position:"beforeend",
			IconRequired:"<i>*</i>",
						//event Select
			StaticNameBanco:'[sp-static-name="Banco"]',
			StaticNameCuenta:'[sp-static-name="NumeroCuenta"]',
			StaticNamePrimera:'[sp-static-name="Valordescontarenla1eraQuincena"]',
			StaticNameSegunda:'[sp-static-name="Valordescontarenla2daQuincena"]',
			StaticNameTotal:'[sp-static-name="TotalValordescontar"]',	
			StaticAttachment:'[sp-static-name="sp-forms-attachment"]'						
		},
	Style:{
		Block: "block",
		None: "none",
		//Atributtes
		Invalid:"sp-invalid",
		Message:"Formato de correo invalido"	
	},
		ID:{
		Correo: "CorreoElectronico",	
	    Primera:'Valordescontarenla1eraQuincena',
		Segunda:'Valordescontarenla2daQuincena',
		Total:'TotalValordescontar'

	},

},
	//Cargar Formulario
	Initialize:function(){
		try {
			Legalizacion.loadNewForm();
		}
		catch (e){
			console.log(e);
		}
	},
	loadNewForm:function(){
		spForms.options={
			actions:[
				{
					type:spForms.parameters.saveAction,
					name:Legalizacion.parameters.IdEnviar,
					id:Legalizacion.parameters.IdEnviar,
					success:function(){return true;},
					error:function(){},
					preSave:function(){return true;}
				},
				{
					type:spForms.parameters.cancelAction,
					name:Legalizacion.parameters.IdCancelar,
					id:Legalizacion.parameters.IdCancelar,
					callback:""
				},
			],
			title:Legalizacion.parameters.TitleForm,
			contentTypeName:Legalizacion.parameters.ContentType,
			containerId:Legalizacion.parameters.IdDiv,
			allowAttachments:true,
		  	requiredAttachments:true,
		  	maxAttachments:5,
		  	minAttachments:1,
		
			successLoadNewForm:function(){
				Legalizacion.InitForm();
			} 
			};
			spForms.loadNewForm();
			},
	
	
	//Despues de cargar
	loadUserInfo:function(){
		try {
				sputilities.getMyUserProfileProperties("",function(data){
				var resultItems = JSON.parse(data).d;
				Legalizacion.parameters.resultItems = resultItems;
				Legalizacion.setValuesUserInfo();
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	setValuesUserInfo:function(){
	  	document.getElementById(Legalizacion.parameters.fields[0].field).value = Legalizacion.parameters.resultItems.DisplayName ? Legalizacion.parameters.resultItems.DisplayName: "";
	  	document.getElementById(Legalizacion.parameters.fields[1].field).value = Legalizacion.parameters.resultItems.CellPhone ? Legalizacion.parameters.resultItems.CellPhone: "";
	  	document.getElementById(Legalizacion.parameters.fields[2].field).value = Legalizacion.parameters.resultItems.Email ? (Legalizacion.parameters.resultItems.Email).toLowerCase(): "";
	},
	InitForm:function(){
		try {
			Legalizacion.loadUserInfo();	
			Legalizacion.validateEmailForm();
			//Legalizacion.validateTotal();
			//Legalizacion.ValidateTipoInscripcion();	
			//Legalizacion.ValidateFields();
			//Legalizacion.inputSelect();			
		}
		catch (e){
			console.log(e);
		}
	},
	validateEmailForm:function(){
		var input = document.getElementById(Legalizacion.parameters.ID.Correo);
			if(input != null){
				input.addEventListener('change',function(event){
				var email = this.value;
		  			if (Legalizacion.validateEmail(email)) {
		  				var staticName = "[sp-static-name="+Legalizacion.parameters.ID.Correo+"]";
		  				var InputSelect = document.querySelector(staticName);
   							InputSelect.removeAttribute(Legalizacion.parameters.Style.Invalid)
   			  				} 	
   			  		else{
		  				var staticName = "[sp-static-name="+Legalizacion.parameters.ID.Correo+"]";
		  				var InputSelect = document.querySelector(staticName);
   						InputSelect.setAttribute(Legalizacion.parameters.Style.Invalid,Legalizacion.parameters.Style.Message)
   			  			}
  				return false;
  					});
					}
	},
	validateEmail:function(email){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 			return re.test(email);
 	 	},
}
Legalizacion.Initialize();


