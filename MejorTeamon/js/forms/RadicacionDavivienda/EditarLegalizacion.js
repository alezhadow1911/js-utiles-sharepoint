﻿var Legalizacion = {
	//create parameters
	parameters:{
		query:"?$Select=FlujoAsociado/Title,ResponsablesAbonos/EMail,ID&$expand=FlujoAsociado,ResponsablesAbonos&$filter=ID eq 23",
		queryNovedades:"?$Select=FlujoAsociado/Title,ResponsablesAbonos/EMail,ID&$expand=FlujoAsociado,ResponsablesAbonos&$filter=ID eq 26",
		listName: "ResponsablesTH",
		bodyItem:"<div class='contact'><p>Usted no tiene permisos sobre este formulario.</p></div>",
		Title:"Retiro de Legalizacion - Acceso Denegado ",
		textNotAllowed: "Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior",
		selectorFormContent:".form-content",
		htmlNotAllowed:"<div><h4>{0}</h4></div>",
		title:"Formulario de Legalización Subsidio Educativo Edición",
		description:"Completa la información correspondiente.",
		contentTypeName:"Legalizacion Subsidio Educativo",
		containerId:"spForm",

		Form:{
			IdEnviar: "Enviar",
			IdCancelar:"Cancelar",
			IdAprobar:"Aprobar",
			IdCorregir:"Enviar a Corregir",
			IdAplicar:"Aplicar Novedad",
			ContentType:"LegalizacionAFC",
			StaticComentarios:'[sp-static-name="ComentariosTH"]',
			Invalid: "sp-invalid",
			MessageInvalid:"Los comentarios son obligatorios"							
		},
			UpdateForm:{
			Enviar: "btn-Enviar",
			Aplicar:"btn-Aplicar Novedad",
			Corregir:"btn-Enviar a Corregir",
			Aprobar:"btn-Aprobar",
			Cancelar:"btn-Cancelar",
			//Ids
			Comentarios:"ComentariosTH",
			Base:"BasePersona",
			Nombre:"NombrePersona1",
			Documento:"DocumentoIdentidad",
			Celular:"NumeroCelular",
			Correo:"CorreoElectronico",
			Radicado:"NumeroRadicado",
			Estados:"EstadosCajaCompensacion",
			DocumentoBeneficiario:"DocumentoBeneficiario",
			SpForm:"spForm",
			//etiquetas
			Input:"input",
			TextArea:"textarea"
							
		},
			StyleHTML:{
			Block: "block",
			None: "none",
			Point: "initial",
			PointNone: "none",
			Color: "#444"						
		},

		


		
	},	
	fieldType:{
	 "0":{
	    	Actualizar:function(){
	    	document.getElementById(Legalizacion.parameters.UpdateForm.Aprobar).style.display =Legalizacion.parameters.StyleHTML.None;
			document.getElementById(Legalizacion.parameters.UpdateForm.Corregir).style.display =Legalizacion.parameters.StyleHTML.None;	
			document.getElementById(Legalizacion.parameters.UpdateForm.Enviar).style.display =Legalizacion.parameters.StyleHTML.None;
			document.getElementById(Legalizacion.parameters.UpdateForm.Cancelar).style.display =Legalizacion.parameters.StyleHTML.None;
			document.getElementById(Legalizacion.parameters.UpdateForm.Aplicar).style.display =Legalizacion.parameters.StyleHTML.None;	
      	}
    },
     "1":{
	    	Actualizar:function(){
	    	Legalizacion.userResponsablesVentanillaForm();
	    	var ObservacionesJefe = document.querySelector(Legalizacion.parameters.Form.StaticComentarios);
			ObservacionesJefe.style.display = Legalizacion.parameters.StyleHTML.Block;
	    	var TextObservacionesTH = document.getElementById(Legalizacion.parameters.UpdateForm.Comentarios);
			TextObservacionesTH.style.pointerEvents = Legalizacion.parameters.StyleHTML.Point;
			TextObservacionesTH.style.color = Legalizacion.parameters.StyleHTML.Color;
			document.getElementById(Legalizacion.parameters.UpdateForm.Enviar).style.display =Legalizacion.parameters.StyleHTML.None;
			document.getElementById(Legalizacion.parameters.UpdateForm.Aplicar).style.display =Legalizacion.parameters.StyleHTML.None;
      	}
    },

    
     "2":{
	    	Actualizar:function(){
	    	Legalizacion.userResponsablesNovedadesForm();
	    	 var ObservacionesJefe = document.querySelector(Legalizacion.parameters.Form.StaticComentarios);
			ObservacionesJefe.style.display = Legalizacion.parameters.StyleHTML.Block;
	    	document.getElementById(Legalizacion.parameters.UpdateForm.Aprobar).style.display =Legalizacion.parameters.StyleHTML.None;
			document.getElementById(Legalizacion.parameters.UpdateForm.Corregir).style.display =Legalizacion.parameters.StyleHTML.None;	
			document.getElementById(Legalizacion.parameters.UpdateForm.Enviar).style.display =Legalizacion.parameters.StyleHTML.None;	
     	 }
    },

   	"3":{
	    	Actualizar:function(){
	    	var ObservacionesJefe = document.querySelector(Legalizacion.parameters.Form.StaticComentarios);
			ObservacionesJefe.style.display = Legalizacion.parameters.StyleHTML.Block;
	    	document.getElementById(Legalizacion.parameters.UpdateForm.Aprobar).style.display =Legalizacion.parameters.StyleHTML.None;
			document.getElementById(Legalizacion.parameters.UpdateForm.Corregir).style.display =Legalizacion.parameters.StyleHTML.None;
			document.getElementById(Legalizacion.parameters.UpdateForm.Aplicar).style.display =Legalizacion.parameters.StyleHTML.None;		
			document.getElementById(Legalizacion.parameters.UpdateForm.Nombre).style.pointerEvents = Legalizacion.parameters.StyleHTML.Point;
			document.getElementById(Legalizacion.parameters.UpdateForm.Documento).style.pointerEvents = Legalizacion.parameters.StyleHTML.Point;
			document.getElementById(Legalizacion.parameters.UpdateForm.Celular).style.pointerEvents = Legalizacion.parameters.StyleHTML.Point;
			document.getElementById(Legalizacion.parameters.UpdateForm.Correo).style.pointerEvents = Legalizacion.parameters.StyleHTML.Point;
			document.getElementById(Legalizacion.parameters.UpdateForm.Radicado).style.pointerEvents = Legalizacion.parameters.StyleHTML.Point;
			document.getElementById(Legalizacion.parameters.UpdateForm.Estados).style.pointerEvents = Legalizacion.parameters.StyleHTML.Point;
			document.getElementById(Legalizacion.parameters.UpdateForm.DocumentoBeneficiario).style.pointerEvents = Legalizacion.parameters.StyleHTML.Point;
			document.getElementById(Legalizacion.parameters.UpdateForm.Nombre).style.color  = Legalizacion.parameters.StyleHTML.Color;
			document.getElementById(Legalizacion.parameters.UpdateForm.Documento).style.color  = Legalizacion.parameters.StyleHTML.Color;
			document.getElementById(Legalizacion.parameters.UpdateForm.Celular).style.color  = Legalizacion.parameters.StyleHTML.Color;
			document.getElementById(Legalizacion.parameters.UpdateForm.Correo).style.color  = Legalizacion.parameters.StyleHTML.Color;
			document.getElementById(Legalizacion.parameters.UpdateForm.Radicado).style.color  = Legalizacion.parameters.StyleHTML.Color;
			document.getElementById(Legalizacion.parameters.UpdateForm.Estados).style.color  = Legalizacion.parameters.StyleHTML.Color;
			document.getElementById(Legalizacion.parameters.UpdateForm.DocumentoBeneficiario).style.color  = Legalizacion.parameters.StyleHTML.Color;									
			document.getElementsByClassName('col-md-6 col-xs-6 delete-doc')[0].style.display = Legalizacion.parameters.StyleHTML.Block;
			document.getElementById('sp-forms-add-attachment').style.display = Legalizacion.parameters.StyleHTML.Block;
			var Base = document.getElementById(Legalizacion.parameters.UpdateForm.Base)
			var BaseInputs =Base.getElementsByTagName(Legalizacion.parameters.UpdateForm.Input)
			for(var i=0; i<BaseInputs.length;i++){
			var input=BaseInputs[i];
			input.disabled=false;
			}	
      	}
    },
     "4":{
	    	Actualizar:function(){
	    	document.getElementById(Legalizacion.parameters.UpdateForm.Aprobar).style.display =Legalizacion.parameters.StyleHTML.None;
			document.getElementById(Legalizacion.parameters.UpdateForm.Corregir).style.display =Legalizacion.parameters.StyleHTML.None;	
			document.getElementById(Legalizacion.parameters.UpdateForm.Enviar).style.display =Legalizacion.parameters.StyleHTML.None;
			document.getElementById(Legalizacion.parameters.UpdateForm.Cancelar).style.display =Legalizacion.parameters.StyleHTML.None;
			document.getElementById(Legalizacion.parameters.UpdateForm.Aplicar).style.display =Legalizacion.parameters.StyleHTML.None;			
     	 }
    },
    },

	CtrlForm:function(){
		try {
			var Base = document.getElementById(Legalizacion.parameters.UpdateForm.Base)
			var BaseInputs =Base.getElementsByTagName(Legalizacion.parameters.UpdateForm.Input)
			for(var i=0; i<BaseInputs.length;i++){
				var input=BaseInputs[i];
				input.disabled=true;
				}	
			var CurrentEstado = document.getElementById(Legalizacion.parameters.UpdateForm.Estados).value;
			var spFormField = Legalizacion.fieldType[CurrentEstado].Actualizar();		
			}
		catch (e){
			console.log(e);
		}
	},
	userResponsablesVentanillaForm:function(){
		try {
			var UserEmailCurrent = _spPageContextInfo.userEmail
			var linkQuery = String.format(Legalizacion.parameters.query);
			sputilities.getItems(Legalizacion.parameters.listName,linkQuery,function(data){
				var resultItems = JSON.parse(data).d.results;
				var Items = resultItems[0].ResponsablesAbonos.results
				var find = false; 
				for(var i=0;i<Items.length;i++){
					var nextItem = Items[i+1];
					var prevItem = Items[i-1];
					var Email = Items[i].EMail
						if(Email == UserEmailCurrent){
							find = true;
							}
						}
				if(find){
					document.getElementById(Legalizacion.parameters.UpdateForm.SpForm).style.display=Legalizacion.parameters.StyleHTML.Block;
				}else{			
					Legalizacion.hideNotAllowed();					
					}			
			});
		}
		catch (e){
			console.log(e);
		}
	},
	userResponsablesNovedadesForm:function(){
		try {
		var UserEmailCurrent = _spPageContextInfo.userEmail
		var linkQuery = String.format(Legalizacion.parameters.queryNovedades);
		sputilities.getItems(Legalizacion.parameters.listName,linkQuery,function(data){
				var resultItems = JSON.parse(data).d.results;
				var Items = resultItems[0].ResponsablesAbonos.results
				var find = false; 
				for(var i=0;i<Items.length;i++){
					var nextItem = Items[i+1];
					var prevItem = Items[i-1];
					var Email = Items[i].EMail
					console.log(Email);
						if(Email == UserEmailCurrent){
							find = true;
						}
						}
				if(find){
					document.getElementById(Legalizacion.parameters.UpdateForm.SpForm).style.display=Legalizacion.parameters.StyleHTML.Block;
				}else{
					Legalizacion.hideNotAllowed();					
					}		
			});


		}
		catch (e){
			console.log(e);
		}
	},
		validateReject:function(){
	
		var row = document.querySelector(Legalizacion.parameters.Form.StaticComentarios);
		var comments = row.querySelector(Legalizacion.parameters.UpdateForm.TextArea);
		var isValid = true;
		if(comments.value != "")
		{
			row.removeAttribute(Legalizacion.parameters.Form.Invalid);			
		}
		else
		{
			row.setAttribute(Legalizacion.parameters.Form.Invalid,Legalizacion.parameters.Form.MessageInvalid);			
		}
		return isValid;
	
	},	
	hideNotAllowed:function(){
		var html = String.format(Legalizacion.parameters.htmlNotAllowed,Legalizacion.parameters.textNotAllowed);
		var formContent = document.querySelector(Legalizacion.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	},

	Initialize:function(){
		try {
		Legalizacion.loadEditForm();
		}
		catch (e){
			console.log(e);
		}
	},
	loadEditForm:function(){
	spForms.options={
		actions:[
			{
			type:spForms.parameters.updateAction,
			name:Legalizacion.parameters.Form.IdAprobar,
			id:Legalizacion.parameters.Form.IdAprobar,
			preSave:function(){
			var CurrentEstado = document.getElementById(Legalizacion.parameters.UpdateForm.Estados);
			if(CurrentEstado.value == "1"){
			CurrentEstado.value = "2";
			}
			return true},
			success:function(){return true},
			error:function(){return true}
		},
				{
			type:spForms.parameters.updateAction,
			name:Legalizacion.parameters.Form.IdCorregir,
			id:Legalizacion.parameters.Form.IdCorregir,
			preSave:function(){
			var CurrentEstado = document.getElementById(Legalizacion.parameters.UpdateForm.Estados);
			if(CurrentEstado.value == "1"){
			CurrentEstado.value = "3";
			}
			return Legalizacion.validateReject();
			return true},
			success:function(){return true},
			error:function(){return true}
		},
		{
			type:spForms.parameters.updateAction,
			name:Legalizacion.parameters.Form.IdEnviar,
			id:Legalizacion.parameters.Form.IdEnviar,
			preSave:function(){
			var CurrentEstado = document.getElementById(Legalizacion.parameters.UpdateForm.Estados);
			if(CurrentEstado.value == "3"){
			CurrentEstado.value = "1";
			}
			return true},
			success:function(){return true},
			error:function(){return true}
		},
		{
			type:spForms.parameters.updateAction,
			name:Legalizacion.parameters.Form.IdAplicar,
			id:Legalizacion.parameters.Form.IdAplicar,
			preSave:function(){
			var CurrentEstado = document.getElementById(Legalizacion.parameters.UpdateForm.Estados);
			if(CurrentEstado.value == "2"){
			CurrentEstado.value = "4";
			}

			return true},
			success:function(){return true},
			error:function(){return true}
		},


		{
			type:spForms.parameters.cancelAction,
			name:Legalizacion.parameters.Form.IdCancelar,
			id:Legalizacion.parameters.Form.IdCancelar,
			callback:""
		},
	],
	title:Legalizacion.parameters.title,
	description:Legalizacion.parameters.description,
	contentTypeName:Legalizacion.parameters.contentTypeName,
	containerId:Legalizacion.parameters.containerId,
		allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:5,
  	minAttachments:1,

	successLoadEditForm:function(){
		Legalizacion.CtrlForm();
	} 
};
spForms.loadEditForm();
	},	
}
Legalizacion.Initialize();







