﻿var humanitarianAidForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		fieldsHTML:{
			FechaSolicitudAyuda:"[sp-static-name='FechaSolicitudAyuda']",
			OrganizacionSolicitante:"[sp-static-name='OrganizacionSolicitante']",
			MisionOrganizacion:"[sp-static-name='MisionOrganizacion']",
			TipoOrganizacion:"[sp-static-name='TipoOrganizacion'] > div > div > select",
			RutaOrigen:"[sp-static-name='RutaOrigen']",
			RutaDestino:"[sp-static-name='RutaDestino']",
			ElementosTransportar:"[sp-static-name='ElementosTransportar']",
			CantidadCajas:"[sp-static-name='CantidadCajas']",
			Medidas:"[sp-static-name='Medidas']",
			PesoPromedioCaja:"[sp-static-name='PesoPromedioCaja']",
			PesoTotal:"[sp-static-name='PesoTotal']",
			Remitente:"[sp-static-name='Remitente']",
			RutaDestino:"[sp-static-name='RutaDestino']",
			RutaOrigen:"[sp-static-name='RutaOrigen']",
			Destinatario:"[sp-static-name='Destinatario']"
		},
		internalNameFields:{			
			FechaSolicitudAyuda:'FechaSolicitudAyuda',
			OrganizacionSolicitante:'OrganizacionSolicitante',
			MisionOrganizacion:'MisionOrganizacion',
			TipoOrganizacion:"TipoOrganizacion",
			RutaOrigen:"RutaOrigen",
			RutaDestino:"RutaDestino",
			ElementosTransportar:"ElementosTransportar",
			CantidadCajas:"CantidadCajas",
			Medidas:"Medidas",
			PesoPromedioCaja:"PesoPromedioCaja",
			PesoTotal:"PesoTotal",
			Remitente:"Remitente",
			RutaDestino:"RutaDestino",
			RutaOrigen:"RutaOrigen",
			Destinatario:"Destinatario"
		},
		resultItems:null,
		resultRutas:null,
		invalid:"sp-invalid",
		requiredTag:"<i>*</i>",
		userProperties:"Department",
		errorInvalid:"El correo electrónico no tiene el formato correcto",
		errorInvalidNotText:"Esto no puede estar en blanco",
		loadLogisticsDirector:"humanitarianAidForm.loadLogisticsDirector",
		formsIsLoaded:false,
		queryDirector:"?$Select=Director/EMail&$expand=Director",
		listNameRutas:"Base",
		formsParameters:{
			title:"Solicitudes de ayudas humanitarias",
			description:"Completa la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Reject:{Text:"Enviar a corregir", Id:"corregir"}
			},
			contentType:"Ayudas Humanitarias",
			containerId:"container-form"			
		},
		flowParameters:{
			urlFlow:"https://prod-31.westus.logic.azure.com:443/workflows/78d374961400404e88bc11fb6dd9a788/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=h3Z1eQ0WAkugH5C71ndtoVcyal6O8FDlGGXaTW4Lruo",
			viewUrl:"",
	    	approveUrl:"",
	   		assingUrl:"",
	   		assingScore:"",
	    	listViewCoordination:"",
	    	listViewReschedule:"",
	    	listNameAssing:"DireccionLogistica",
	    	subjectTitle:"Ayudas Humanitarias"
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud",
			"RevisionCoordinador":"Revision Coordinador",
			"AsignarGuia":"Asignar Guia",
			"CorregirSolicitud":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud"
		},
		global:{
			createdBy:null,
			created:null,
			directorLogistica:"",
			coordinadorRegional:""
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
		
		var objectSelect = document.querySelector(humanitarianAidForm.parameters.fieldsHTML.TipoOrganizacion);
		var textSelected = objectSelect.options[objectSelect.selectedIndex].text;
							
	    humanitarianAidForm.parameters.flowParameters.approveUrl = String.format(humanitarianAidForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(humanitarianAidForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    humanitarianAidForm.parameters.flowParameters.editUrl = String.format(humanitarianAidForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(humanitarianAidForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	   	humanitarianAidForm.parameters.flowParameters.assingUrl = String.format(humanitarianAidForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(humanitarianAidForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    humanitarianAidForm.parameters.flowParameters.toCorrectUrl = String.format(humanitarianAidForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(humanitarianAidForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    humanitarianAidForm.parameters.flowParameters.viewUrl = String.format(humanitarianAidForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(humanitarianAidForm.parameters.forms)[5],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	   		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":humanitarianAidForm.parameters.global.createdBy,
		    	"emailSolicitante":humanitarianAidForm.parameters.global.createdBy,
			    "organizacion":spForms.loadedFields.OrganizacionSolicitante.value,
			    "tipoOrganizacion":textSelected,
			    "rutaOrigen":spForms.loadedFields.RutaOrigen.value,
			    "rutaDestino":spForms.loadedFields.RutaDestino.value,
			    "directorLogistica": humanitarianAidForm.parameters.global.directorLogistica,
			    "fechaSolicitud":humanitarianAidForm.parameters.global.created,
			    "mision":spForms.loadedFields.MisionOrganizacion.value,
			    "elementosTransportar":spForms.loadedFields.ElementosTransportar.value,
			    "cantidadCajas":Math.round(spForms.loadedFields.CantidadCajas.value),
			    "medidas": spForms.loadedFields.Medidas.value,
			    "promedio":Math.round(spForms.loadedFields.PesoPromedioCaja.value),
			    "peso": Math.round(spForms.loadedFields.PesoTotal.value),
			    "destinatario": spForms.loadedFields.Destinatario.value,
			    "remitente":spForms.loadedFields.Remitente.value,
				"coordinadoRegional":humanitarianAidForm.parameters.global.coordinadorRegional

		    },
		    "state":spForms.loadedFields.EstadoAyudas.value,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":humanitarianAidForm.parameters.flowParameters.listNameAssing,
		    "viewUrl":humanitarianAidForm.parameters.flowParameters.viewUrl,
		    "approveUrl":humanitarianAidForm.parameters.flowParameters.approveUrl,
		    "editUrl":humanitarianAidForm.parameters.flowParameters.editUrl,
		    "assingUrl":humanitarianAidForm.parameters.flowParameters.assingUrl,
		    "toCorrectUrl":humanitarianAidForm.parameters.flowParameters.assingScore,
		   	"subjectTitle":	humanitarianAidForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:humanitarianAidForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity = 0;
				document.querySelector('.form-content').style.opacity = 1;
				document.querySelector('.loader-container').style.height = 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){
		sputilities.callFunctionOnLoadBody(humanitarianAidForm.parameters.loadLogisticsDirector);
		spForms.loadNewForm();
	},
	loadLogisticsDirector:function(){
		try {
			//running the query with parameters
			sputilities.getItems(humanitarianAidForm.parameters.flowParameters.listNameAssing,humanitarianAidForm.parameters.queryDirector,function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d.results[0].Director.EMail;
				humanitarianAidForm.parameters.global.directorLogistica = resultItems;
				
				var ctrlInterval = setInterval(function(){
					if(humanitarianAidForm.parameters.formsIsLoaded)
					{
						humanitarianAidForm.setValuesFields();
						clearInterval(ctrlInterval);
					}
				},500);
				
				},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	setValuesFields: function(){
		try {
			//running the query with parameters
			sputilities.getItems(humanitarianAidForm.parameters.listNameRutas,"",function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				var rutas = [];
				for (var i=0; i < resultItems.length; i++)
				{
					rutas.push(resultItems[i].Title);	
				}
				
				humanitarianAidForm.parameters.resultRutas = rutas;
				var ctrlInterval = setInterval(function(){
					if(humanitarianAidForm.parameters.formsIsLoaded)
					{
						$("#"+humanitarianAidForm.parameters.internalNameFields.RutaOrigen).autocomplete({
      						source: humanitarianAidForm.parameters.resultRutas
    					});
    					$("#"+humanitarianAidForm.parameters.internalNameFields.RutaDestino).autocomplete({
      						source: humanitarianAidForm.parameters.resultRutas
    					});
						var dateCreated = document.getElementById(humanitarianAidForm.parameters.internalNameFields.FechaSolicitudAyuda);
						dateCreated.value = moment.utc(new Date()).format(humanitarianAidForm.parameters.formats.dateFormat);
						clearInterval(ctrlInterval);
					}
				},500);
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	}
}
spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:humanitarianAidForm.parameters.formsParameters.buttons.Send.Text,
			id:humanitarianAidForm.parameters.formsParameters.buttons.Send.Id,
			success:function (){
				humanitarianAidForm.callFlowService();
				return false;

			},
			error:function (){},
			preSave: function (){
				return true;
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:humanitarianAidForm.parameters.formsParameters.buttons.Cancel.Text,
			id:humanitarianAidForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:humanitarianAidForm.parameters.formsParameters.title,
	description:humanitarianAidForm.parameters.formsParameters.description,
	contentTypeName:humanitarianAidForm.parameters.formsParameters.contentType,
	containerId:humanitarianAidForm.parameters.formsParameters.containerId,
	allowAttachments:false,
  	requiredAttachments:false,
  	maxAttachments:20,
  	minAttachments:0,
  	maxSizeInMB:3,
	successLoadNewForm:function(){
		humanitarianAidForm.parameters.global.createdBy = sputilities.contextInfo().userEmail;
		humanitarianAidForm.parameters.global.created = moment.utc(new Date()).format(humanitarianAidForm.parameters.formats.dateFormat);
		humanitarianAidForm.parameters.formsIsLoaded = true;
	} 
};
humanitarianAidForm.initialize();