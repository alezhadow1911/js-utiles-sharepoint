﻿var humanitarianAidForm= {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx",
			"htmlNotAllowed":"<div class='form-group row'><label>{0}</label></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>"
		},
		fieldsHTML:{
			FechaSolicitudAyuda:"[sp-static-name='FechaSolicitudAyuda']",
			OrganizacionSolicitante:"[sp-static-name='OrganizacionSolicitante']",
			MisionOrganizacion:"[sp-static-name='MisionOrganizacion']",
			TipoOrganizacion:"[sp-static-name='TipoOrganizacion'] > div > div > select",
			RutaOrigen:"[sp-static-name='RutaOrigen']",
			RutaDestino:"[sp-static-name='RutaDestino']",
			ElementosTransportar:"[sp-static-name='ElementosTransportar']",
			CantidadCajas:"[sp-static-name='CantidadCajas']",
			Medidas:"[sp-static-name='Medidas']",
			PesoPromedioCaja:"[sp-static-name='PesoPromedioCaja']",
			PesoTotal:"[sp-static-name='PesoTotal']",
			Remitente:"[sp-static-name='Remitente']",
			RutaDestino:"[sp-static-name='RutaDestino']",
			RutaOrigen:"[sp-static-name='RutaOrigen']",
			Destinatario:"[sp-static-name='Destinatario']",
			ComentariosDirectorLogistica:"[sp-static-name='ComentariosDirectorLogistica']",
			ComentariosCoordinadorRegional:"[sp-static-name='ComentariosCoordinadorRegional']",
			FechaEntrega:"[sp-static-name='FechaEntrega']"
		},
		internalNameFields:{			
			FechaSolicitudAyuda:'FechaSolicitudAyuda',
			OrganizacionSolicitante:'OrganizacionSolicitante',
			MisionOrganizacion:'MisionOrganizacion',
			TipoOrganizacion:"TipoOrganizacion",
			RutaOrigen:"RutaOrigen",
			RutaDestino:"RutaDestino",
			ElementosTransportar:"ElementosTransportar",
			CantidadCajas:"CantidadCajas",
			Medidas:"Medidas",
			PesoPromedioCaja:"PesoPromedioCaja",
			PesoTotal:"PesoTotal",
			Remitente:"Remitente",
			RutaDestino:"RutaDestino",
			RutaOrigen:"RutaOrigen",
			Destinatario:"Destinatario",
			Estado:"EstadoAyudas"			
		},		
		controlsHtml:{
            Attachment:"sp-forms-add-attachment",
            Enviar:"btn-enviar",            
			Aprobar:"btn-aprobar",
			Corregir:"btn-corregir",
			Aplicar:"btn-aplicar",
			Rechazar:"btn-rechazar",
			classDelete:".delete-doc",
			classGroup:".form-group.row",
			colorDisable:"#B1B1B1",
			borderDisable:"#E1E1E1",
			iconTH:"ico-help-ComentariosDirectorLogistica"
		},
		bodyItem:"Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior.",
		TitlePopUp:"Acceso Denegado",
    	htmlRequiered:{block:"block",none:"none"},	            
		invalid:"sp-invalid",
		selectorFormContent:".form-content",
		queryDirector:"?$Select=Director/EMail&$expand=Director",
       	codeStates:{ Solicitado:"0",DirectorLogistica:"1",CoordinadorRegional:"2",EnEsperaGuia:"3",GeneraciondeGuia:"4",Corregir:"5",Finalizada:"6"},		
		formsParameters:{
			title:"Corregir Solicitud de ayudas humanitarias",
			description:"Completa la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			}		
		},
		formsParameters:{
			title:"Solicitud de ayudas humanitarias",
			description:"Corrige la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Ayudas Humanitarias",
			containerId:"container-form"			
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud",
			"RevisionCoordinador":"Revision Coordinador",
			"GeneracionGuia":"Generacion Guia",
			"CorregirSolicitud":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud"
		},
		flowParameters:{
			urlFlow:"https://prod-31.westus.logic.azure.com:443/workflows/78d374961400404e88bc11fb6dd9a788/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=h3Z1eQ0WAkugH5C71ndtoVcyal6O8FDlGGXaTW4Lruo",
			viewUrl:"",
	    	approveUrl:"",
	   		assingUrl:"",
	   		assingScore:"",
	    	listViewCoordination:"",
	    	listViewReschedule:"",
	    	listNameAssing:"DireccionLogistica",
	    	subjectTitle:"Ayudas Humanitarias"
		},
		global:{
			formName:null,
			createdBy:null,
			created:null,
			directorLogistica:"",
			coordinadorRegional:""
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
		
		var objectSelect = document.querySelector(humanitarianAidForm.parameters.fieldsHTML.TipoOrganizacion);
		var textSelected = objectSelect.options[objectSelect.selectedIndex].text;
									
	    humanitarianAidForm.parameters.flowParameters.approveUrl = String.format(humanitarianAidForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(humanitarianAidForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    humanitarianAidForm.parameters.flowParameters.editUrl = String.format(humanitarianAidForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(humanitarianAidForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	   	humanitarianAidForm.parameters.flowParameters.assingUrl = String.format(humanitarianAidForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(humanitarianAidForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    humanitarianAidForm.parameters.flowParameters.toCorrectUrl = String.format(humanitarianAidForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(humanitarianAidForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    humanitarianAidForm.parameters.flowParameters.viewUrl = String.format(humanitarianAidForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(humanitarianAidForm.parameters.forms)[5],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	   		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":humanitarianAidForm.parameters.global.createdBy,
		    	"emailSolicitante":humanitarianAidForm.parameters.global.createdBy,
			    "organizacion":spForms.loadedFields.OrganizacionSolicitante.value,
			    "tipoOrganizacion":textSelected,
			    "rutaOrigen":spForms.loadedFields.RutaOrigen.value,
			    "rutaDestino":spForms.loadedFields.RutaDestino.value,
			    "directorLogistica": humanitarianAidForm.parameters.global.directorLogistica,
			    "fechaSolicitud":humanitarianAidForm.parameters.global.created,
			    "mision":spForms.loadedFields.MisionOrganizacion.value,
			    "elementosTransportar":Math.round(spForms.loadedFields.ElementosTransportar.value),
			    "cantidadCajas":Math.round(spForms.loadedFields.CantidadCajas.value),
			    "medidas": spForms.loadedFields.Medidas.value,
			    "promedio":Math.round(spForms.loadedFields.PesoPromedioCaja.value),
			    "peso": Math.round(spForms.loadedFields.PesoTotal.value),
			    "destinatario": spForms.loadedFields.Destinatario.value,
			    "remitente":spForms.loadedFields.Remitente.value,
				"coordinadoRegional":humanitarianAidForm.parameters.global.coordinadorRegional,
				"fechaEntrega":spForms.loadedFields.FechaEntrega.value,
				"comentariosCoordinador":spForms.loadedFields.ComentariosCoordinadorRegional.value,
				"valorAsociado":spForms.loadedFields.ValorAsociado.value,
				"comentariosLogistica":spForms.loadedFields.ComentariosDirectorLogistica.value,
				"numeroGuia":spForms.loadedFields.NumeroGuia.value
		    },
		    "state":spForms.loadedFields.EstadoAyudas.value,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":humanitarianAidForm.parameters.flowParameters.listNameAssing,
		    "viewUrl":humanitarianAidForm.parameters.flowParameters.viewUrl,
		    "approveUrl":humanitarianAidForm.parameters.flowParameters.approveUrl,
		    "editUrl":humanitarianAidForm.parameters.flowParameters.editUrl,
		    "assingUrl":humanitarianAidForm.parameters.flowParameters.assingUrl,
		    "toCorrectUrl":humanitarianAidForm.parameters.flowParameters.toCorrectUrl,
		   	"subjectTitle":	humanitarianAidForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:humanitarianAidForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){
		spForms.loadEditForm();
	},
	loadLogisticsDirector:function(){
		try {
			//running the query with parameters
			sputilities.getItems(humanitarianAidForm.parameters.flowParameters.listNameAssing,humanitarianAidForm.parameters.queryDirector,function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d.results[0].Director.EMail;
				humanitarianAidForm.parameters.global.directorLogistica = resultItems;
				
				var ctrlInterval = setInterval(function(){
					if(humanitarianAidForm.parameters.formsIsLoaded)
					{
						humanitarianAidForm.loadResponsableState();
						clearInterval(ctrlInterval);
					}
				},500);
				
				},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	loadResponsableState:function(){
		try
		{
			var currentUserId =_spPageContextInfo.userId;		
		
			var find = false;
			var state = document.getElementById(humanitarianAidForm.parameters.internalNameFields.Estado);
			if(state != null)
			{
				if(spForms.currentItem.AuthorId == currentUserId){
					find = true;
				}
								
				if(state.value != humanitarianAidForm.parameters.codeStates.Corregir)
				{				
					humanitarianAidForm.showPermissions();
				}
				else
				{
					if(!find)
					{
						humanitarianAidForm.showPermissions();
					}
				}
			}
		}
		catch(e){
			console.log(e);
		}	
	},
	showPermissions:function(){
		var html = String.format(humanitarianAidForm.parameters.formats.htmlNotAllowed,humanitarianAidForm.parameters.bodyItem);
		var formContent = document.querySelector(humanitarianAidForm.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}						
	},
	disableInputs:function(){
	
		var rows = document.querySelectorAll(humanitarianAidForm.parameters.controlsHtml.classGroup);
		
		if(rows != null)
		{
			for(var i=0;i<rows.length;i++)
			{
				rows[i].style.pointerEvents = humanitarianAidForm.parameters.htmlRequiered.none;
			}
		}
	},
	validateState:function(){
		var state = document.getElementById(humanitarianAidForm.parameters.internalNameFields.Estado);
		var isValid = true;			
		if(state!= null)
		{
			state.value = humanitarianAidForm.parameters.codeStates.DirectorLogistica;
		}
		else
		{
			isValid = false;
		}
		return isValid;
	}
}
spForms.options={
	actions:[
		{
			type:spForms.parameters.updateAction,
			name:humanitarianAidForm.parameters.formsParameters.buttons.Send.Text,
			id:humanitarianAidForm.parameters.formsParameters.buttons.Send.Id,
			success:function (){
				humanitarianAidForm.callFlowService();
				return false;
			},
			error:function (){},
			preSave: function (){
				return humanitarianAidForm.validateState();
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:humanitarianAidForm.parameters.formsParameters.buttons.Cancel.Text,
			id:humanitarianAidForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:humanitarianAidForm.parameters.formsParameters.title,
	description:humanitarianAidForm.parameters.formsParameters.description,
	contentTypeName:humanitarianAidForm.parameters.formsParameters.contentType,
	containerId:humanitarianAidForm.parameters.formsParameters.containerId,
	allowAttachments:false,
  	requiredAttachments:false,
  	maxAttachments:20,
  	minAttachments:0,
  	maxSizeInMB:3,
	successLoadEditForm:function(){
		humanitarianAidForm.loadLogisticsDirector();
		humanitarianAidForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
		humanitarianAidForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(humanitarianAidForm.parameters.formats.dateFormat);
	} 
};
humanitarianAidForm.initialize();