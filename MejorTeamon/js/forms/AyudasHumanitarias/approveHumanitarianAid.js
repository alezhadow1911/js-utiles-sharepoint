﻿var humanitarianAidForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx",
			"htmlNotAllowed":"<div class='form-group row'><label>{0}</label></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>"
		},
		query:"?$Select=Director/EMail&$expand=Director",
		queryCR:"?$select=Base/Title,Coordinador/EMail&$expand=Base,Coordinador&$filter=Base/Title eq '{0}'",
		fieldsHTML:{
			FechaSolicitudAyuda:"[sp-static-name='FechaSolicitudAyuda']",
			OrganizacionSolicitante:"[sp-static-name='OrganizacionSolicitante']",
			MisionOrganizacion:"[sp-static-name='MisionOrganizacion']",
			TipoOrganizacion:"[sp-static-name='TipoOrganizacion'] > div > div > select",
			RutaOrigen:"[sp-static-name='RutaOrigen']",
			RutaDestino:"[sp-static-name='RutaDestino']",
			ElementosTransportar:"[sp-static-name='ElementosTransportar']",
			CantidadCajas:"[sp-static-name='CantidadCajas']",
			Medidas:"[sp-static-name='Medidas']",
			PesoPromedioCaja:"[sp-static-name='PesoPromedioCaja']",
			PesoTotal:"[sp-static-name='PesoTotal']",
			Remitente:"[sp-static-name='Remitente']",
			RutaDestino:"[sp-static-name='RutaDestino']",
			RutaOrigen:"[sp-static-name='RutaOrigen']",
			Destinatario:"[sp-static-name='Destinatario']",
			ComentariosDirectorLogistica:"[sp-static-name='ComentariosDirectorLogistica']",
			ComentariosCoordinadorRegional:"[sp-static-name='ComentariosCoordinadorRegional']",
			FechaEntrega:"[sp-static-name='FechaEntrega']",
			NumeroGuia:"[sp-static-name='NumeroGuia']",
			ValorAsociado:"[sp-static-name='ValorAsociado']"
		},
		internalNameFields:{			
			FechaSolicitudAyuda:'FechaSolicitudAyuda',
			OrganizacionSolicitante:'OrganizacionSolicitante',
			MisionOrganizacion:'MisionOrganizacion',
			TipoOrganizacion:"TipoOrganizacion",
			RutaOrigen:"RutaOrigen",
			RutaDestino:"RutaDestino",
			ElementosTransportar:"ElementosTransportar",
			CantidadCajas:"CantidadCajas",
			Medidas:"Medidas",
			PesoPromedioCaja:"PesoPromedioCaja",
			PesoTotal:"PesoTotal",
			Remitente:"Remitente",
			RutaDestino:"RutaDestino",
			RutaOrigen:"RutaOrigen",
			Destinatario:"Destinatario",
			Estado:"EstadoAyudas",
			FechaEntrega:"FechaEntrega"			
		},		
		controlsHtml:{
			Attachment:"sp-forms-add-attachment",
			Aprobar:"btn-aprobar",
			Corregir:"btn-corregir",
			Aplicar:"btn-aplicar",
			Rechazar:"btn-rechazar",
			classDelete:".delete-doc",
			classGroup:".form-group.row",
			colorDisable:"#B1B1B1",
			borderDisable:"#E1E1E1",
			iconTH:"ico-help-ComentariosDirectorLogistica"
		},
		errorInvalid:{text:"Esto no puede estar en blanco",area:"input", areaComments:"textarea"},
		htmlRequiered:{label:"label",block:"block",none:"none"},		
		invalid:"sp-invalid",
		hidden:"sp-hidden",
		listaNameDL:"DireccionLogistica",	
		listNameRegionalCoordinators:"CoordinadoresRegionales",	
		resultItems:null,	
		codeStates:{ Solicitado:"0",DirectorLogistica:"1",CoordinadorRegional:"2",EnEsperaGuia:"3",GeneraciondeGuia:"4",Corregir:"5",Finalizada:"6"},	
		userProperties:"Department",
		bodyItem:"Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior.",
		selectorFormContent:".form-content",
		queryCount:"({0})",
		formsParameters:{
			title:"",
			description:"",
			titleApprove:"Aprobar solicitud de ayudas humanitarias",
			descriptionApprove:"Completa la información correspondiente, para aprobar o rechazar la solicitud",
			titleAssign:"Asignar fecha de entrega de ayuda humanitaria",
			descriptionAssing:"Completa la información correspondiente, para asociar la fecha de entrega",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Ayudas Humanitarias",
			containerId:"container-form"			
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"humanitarianAidForm.loadApprove()",
			"RevisionCoordinador":"humanitarianAidForm.loadCheckCoordinator()",
			"CorregirSolicitud":"Corregir Solicitud",
			"GeneracionGuia":"humanitarianAidForm.loadGuide()",
			"VerSolicitud":"Ver Solicitud"
		},
		flowParameters:{
			urlFlow:"https://prod-31.westus.logic.azure.com:443/workflows/78d374961400404e88bc11fb6dd9a788/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=h3Z1eQ0WAkugH5C71ndtoVcyal6O8FDlGGXaTW4Lruo",
			viewUrl:"",
	    	approveUrl:"",
	   		assingUrl:"",
	   		toCorrectUrl:"",
	    	listViewCoordination:"",
	    	listViewReschedule:"",
	    	listNameAssing:"DireccionLogistica",
	    	subjectTitle:"Ayudas Humanitarias"
		},
		formsState:{
			AprobarSolicitud:"1",
			RevisionCoordinador:"2",
			AsignarGuia:"4",
			GeneracionGuia:"4"
			
		},
		global:{
			formName:null,
			createdBy:null,
			created:null,
			directorLogistica:"",
			coordinadorRegional:""

		},
		initialState:null

	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
		
		var objectSelect = document.querySelector(humanitarianAidForm.parameters.fieldsHTML.TipoOrganizacion);
		var textSelected = objectSelect.options[objectSelect.selectedIndex].text;
									
	    humanitarianAidForm.parameters.flowParameters.approveUrl = String.format(humanitarianAidForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(humanitarianAidForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    humanitarianAidForm.parameters.flowParameters.editUrl = String.format(humanitarianAidForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(humanitarianAidForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	   	humanitarianAidForm.parameters.flowParameters.assingUrl = String.format(humanitarianAidForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(humanitarianAidForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    humanitarianAidForm.parameters.flowParameters.toCorrectUrl = String.format(humanitarianAidForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(humanitarianAidForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    humanitarianAidForm.parameters.flowParameters.viewUrl = String.format(humanitarianAidForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(humanitarianAidForm.parameters.forms)[5],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	   		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":humanitarianAidForm.parameters.global.createdBy,
		    	"emailSolicitante":humanitarianAidForm.parameters.global.createdBy,
			    "organizacion":spForms.loadedFields.OrganizacionSolicitante.value,
			    "tipoOrganizacion":textSelected,
			    "rutaOrigen":spForms.loadedFields.RutaOrigen.value,
			    "rutaDestino":spForms.loadedFields.RutaDestino.value,
			    "directorLogistica": humanitarianAidForm.parameters.global.directorLogistica,
			    "fechaSolicitud":humanitarianAidForm.parameters.global.created,
			    "mision":spForms.loadedFields.MisionOrganizacion.value,
			    "elementosTransportar":spForms.loadedFields.ElementosTransportar.value,
			    "cantidadCajas":Math.round(spForms.loadedFields.CantidadCajas.value),
			    "medidas": spForms.loadedFields.Medidas.value,
			    "promedio":Math.round(spForms.loadedFields.PesoPromedioCaja.value),
			    "peso": Math.round(spForms.loadedFields.PesoTotal.value),
			    "destinatario": spForms.loadedFields.Destinatario.value,
			    "remitente":spForms.loadedFields.Remitente.value,
				"coordinadoRegional":humanitarianAidForm.parameters.global.coordinadorRegional,
				"fechaEntrega":spForms.loadedFields.FechaEntrega.value,
				"comentariosCoordinador":spForms.loadedFields.ComentariosCoordinadorRegional.value,
				"valorAsociado":Math.round(spForms.loadedFields.ValorAsociado.value),
				"comentariosLogistica":spForms.loadedFields.ComentariosDirectorLogistica.value,
				"numeroGuia":spForms.loadedFields.NumeroGuia.value
		    },
		    "state":spForms.loadedFields.EstadoAyudas.value,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":humanitarianAidForm.parameters.flowParameters.listNameAssing,
		    "viewUrl":humanitarianAidForm.parameters.flowParameters.viewUrl,
		    "approveUrl":humanitarianAidForm.parameters.flowParameters.approveUrl,
		    "editUrl":humanitarianAidForm.parameters.flowParameters.editUrl,
		    "assingUrl":humanitarianAidForm.parameters.flowParameters.assingUrl,
		    "toCorrectUrl":humanitarianAidForm.parameters.flowParameters.toCorrectUrl,
		   	"subjectTitle":	humanitarianAidForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:humanitarianAidForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity = 0;
				document.querySelector('.form-content').style.opacity = 1;
				document.querySelector('.loader-container').style.height = 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){			
		var partsOfUrl = window.location.href.split('?')[0].split('/');
		humanitarianAidForm.parameters.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];
		eval(humanitarianAidForm.parameters.forms[humanitarianAidForm.parameters.global.formName]);

	},
	loadApprove:function(){	
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:humanitarianAidForm.parameters.formsParameters.buttons.Aprove.Text,
				id:humanitarianAidForm.parameters.formsParameters.buttons.Aprove.Id,
				success:function (){
					humanitarianAidForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return humanitarianAidForm.validateApprove()
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:humanitarianAidForm.parameters.formsParameters.buttons.Reject.Text,
				id:humanitarianAidForm.parameters.formsParameters.buttons.Reject.Id,
				success:function (){
					humanitarianAidForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return humanitarianAidForm.validateReject()
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:humanitarianAidForm.parameters.formsParameters.buttons.Cancel.Text,
				id:humanitarianAidForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		humanitarianAidForm.parameters.formsParameters.description = humanitarianAidForm.parameters.formsParameters.descriptionApprove;
		humanitarianAidForm.parameters.formsParameters.title = humanitarianAidForm.parameters.formsParameters.titleApprove;
		humanitarianAidForm.loadEdit(actions,humanitarianAidForm.loadResponsableState);
	},
	loadCheckCoordinator:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:humanitarianAidForm.parameters.formsParameters.buttons.Send.Text,
				id:humanitarianAidForm.parameters.formsParameters.buttons.Send.Id,
				success:function (){
					humanitarianAidForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					if(humanitarianAidForm.validateRequired()){
						return humanitarianAidForm.validateApprove()
					}
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:humanitarianAidForm.parameters.formsParameters.buttons.Cancel.Text,
				id:humanitarianAidForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		humanitarianAidForm.parameters.formsParameters.description = humanitarianAidForm.parameters.formsParameters.descriptionAssing;
		humanitarianAidForm.parameters.formsParameters.title = humanitarianAidForm.parameters.formsParameters.titleAssing;
        humanitarianAidForm.loadEdit(actions,humanitarianAidForm.loadResponsableState);
	},
	loadGuide:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:humanitarianAidForm.parameters.formsParameters.buttons.Send.Text,
				id:humanitarianAidForm.parameters.formsParameters.buttons.Send.Id,
				success:function (){
					humanitarianAidForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					if (humanitarianAidForm.validateGuide()){
						return humanitarianAidForm.validateApprove()
					}
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:humanitarianAidForm.parameters.formsParameters.buttons.Cancel.Text,
				id:humanitarianAidForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		humanitarianAidForm.parameters.formsParameters.description = humanitarianAidForm.parameters.formsParameters.descriptionScore;
		humanitarianAidForm.parameters.formsParameters.title = humanitarianAidForm.parameters.formsParameters.titleScore;
        humanitarianAidForm.loadEdit(actions,humanitarianAidForm.loadResponsableState);

	},
	loadEdit:function(actions,sucessFunction){
		spForms.options={	
			actions:actions,
			title:humanitarianAidForm.parameters.formsParameters.title,
			description:humanitarianAidForm.parameters.formsParameters.description,
			contentTypeName:humanitarianAidForm.parameters.formsParameters.contentType,
			containerId:humanitarianAidForm.parameters.formsParameters.containerId,
			allowAttachments:false,
		  	requiredAttachments:false,
		  	maxAttachments:20,
		  	minAttachments:0,
		  	maxSizeInMB:3,
			successLoadEditForm:function(){	
				humanitarianAidForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
				humanitarianAidForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(humanitarianAidForm.parameters.formats.dateFormat);
				if(typeof sucessFunction== "function"){
					sucessFunction();
				}
			} 
		};
		spForms.loadEditForm();
	},
	validateRequired:function(){
		var fechaEntregaRow = document.querySelector(humanitarianAidForm.parameters.fieldsHTML.FechaEntrega);
		var comentariosRow = document.querySelector(humanitarianAidForm.parameters.fieldsHTML.ComentariosCoordinadorRegional);
		var isValid = true;
		
		if(fechaEntregaRow != null && comentariosRow != null)
		{
		
			var fechaEntrega = fechaEntregaRow.querySelector(humanitarianAidForm.parameters.errorInvalid.area);
			var comentarios = comentariosRow.querySelector(humanitarianAidForm.parameters.errorInvalid.areaComments);

						
			if(fechaEntrega.value != "")
			{
				fechaEntregaRow.removeAttribute(humanitarianAidForm.parameters.invalid);
			}
			else
			{
				isValid = false;
				fechaEntregaRow.setAttribute(humanitarianAidForm.parameters.invalid,humanitarianAidForm.parameters.errorInvalid.text);			
			}	
			if(comentarios.value != "")
			{
				comentariosRow.removeAttribute(humanitarianAidForm.parameters.invalid);
			}
			else
			{
				isValid = false;
				comentariosRow.setAttribute(humanitarianAidForm.parameters.invalid,humanitarianAidForm.parameters.errorInvalid.text);			
			}	
		}
		else
		{
			isValid = false;
		}
		return isValid;
		
	},
	validateGuide: function(){
		var numeroGuiaRow = document.querySelector(humanitarianAidForm.parameters.fieldsHTML.NumeroGuia);
		var valorRow = document.querySelector(humanitarianAidForm.parameters.fieldsHTML.ValorAsociado);
		var isValid = true;
		
		if(numeroGuiaRow != null && valorRow != null)
		{
		
			var numeroGuia = numeroGuiaRow.querySelector(humanitarianAidForm.parameters.errorInvalid.area);
			var valor = valorRow.querySelector(humanitarianAidForm.parameters.errorInvalid.area);

						
			if(numeroGuia.value != "")
			{
				numeroGuiaRow.removeAttribute(humanitarianAidForm.parameters.invalid);
			}
			else
			{
				isValid = false;
				numeroGuiaRow.setAttribute(humanitarianAidForm.parameters.invalid,humanitarianAidForm.parameters.errorInvalid.text);			
			}	
			if(valor.value != "")
			{
				valorRow.removeAttribute(humanitarianAidForm.parameters.invalid);
			}
			else
			{
				isValid = false;
				valorRow.setAttribute(humanitarianAidForm.parameters.invalid,humanitarianAidForm.parameters.errorInvalid.text);			
			}	
		}
		else
		{
			isValid = false;
		}
		return isValid;
	},
	validateApprove:function(){
		var state = document.getElementById(humanitarianAidForm.parameters.internalNameFields.Estado);
		var isValid = true;
		if(humanitarianAidForm.parameters.initialState == humanitarianAidForm.parameters.codeStates.DirectorLogistica)
		{			
			if(state!= null)
			{
				state.value = humanitarianAidForm.parameters.codeStates.CoordinadorRegional;
			}
			else
			{
				isValid = false;
			}
		}
		if(humanitarianAidForm.parameters.initialState == humanitarianAidForm.parameters.codeStates.CoordinadorRegional)
		{			
			if(state!= null){
				state.value = humanitarianAidForm.parameters.codeStates.EnEsperaGuia;
			}
			else
			{
				isValid = false;
			}
		}
		if(humanitarianAidForm.parameters.initialState == humanitarianAidForm.parameters.codeStates.GeneraciondeGuia)
		{		
			if(state!= null)
			{
				state.value = humanitarianAidForm.parameters.codeStates.Finalizada;
			}
			else
			{
				isValid = false;
			}
		}	
		return isValid;		
	},
	validateReject:function(){
		var row = document.querySelector(humanitarianAidForm.parameters.fieldsHTML.ComentariosDirectorLogistica);
		var state = document.getElementById(humanitarianAidForm.parameters.internalNameFields.Estado);		
		var isValid = true;
		
		if(row != null && state != null)
		{
			var comments = row.querySelector(humanitarianAidForm.parameters.errorInvalid.areaComments);
			if(comments.value != "")
			{
				row.removeAttribute(humanitarianAidForm.parameters.invalid);
				if(humanitarianAidForm.parameters.initialState == humanitarianAidForm.parameters.codeStates.DirectorLogistica)
				{
					state.value = humanitarianAidForm.parameters.codeStates.Corregir;
				}
				else
				{
					state.value = humanitarianAidForm.parameters.codeStates.CoordinadorRegional;

				}			
			}
			else
			{
				row.setAttribute(humanitarianAidForm.parameters.invalid,humanitarianAidForm.parameters.errorInvalid.text);			
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;

	},
	loadResponsableState:function(){	
		try
		{	
			var objectSelect = document.getElementById(humanitarianAidForm.parameters.internalNameFields.RutaOrigen);
			var textSelected = objectSelect.value;
			$("#"+ humanitarianAidForm.parameters.internalNameFields.FechaEntrega).data("DateTimePicker").minDate(new Date());
			
			var userCurrent = _spPageContextInfo.userEmail
			var state = document.getElementById(humanitarianAidForm.parameters.internalNameFields.Estado);
				if(state != null)
				{
					if(state.value == humanitarianAidForm.parameters.formsState[humanitarianAidForm.parameters.global.formName])
					{
						if(state.value == humanitarianAidForm.parameters.codeStates.DirectorLogistica)
						{
							sputilities.getItems(humanitarianAidForm.parameters.listaNameDL,humanitarianAidForm.parameters.query,function(data){
								
								var resultItems = JSON.parse(data).d.results;
								if(resultItems.length > 0)
								{
									var items = resultItems[0].Director.EMail;
									var find = false;
									if(items == userCurrent){
										find = true;
									}
									
									humanitarianAidForm.parameters.initialState = state.value;
									
									if(state.value == humanitarianAidForm.parameters.codeStates.DirectorLogistica)
									{				
										if(find)
										{
											humanitarianAidForm.showComments(state.value);
										}
										else
										{
											humanitarianAidForm.showComments(state.value);
											humanitarianAidForm.hideButtons(state.value);
											humanitarianAidForm.showPopUp();
										}
									}
								}						
							});
						}
						else 
						if(state.value == humanitarianAidForm.parameters.codeStates.CoordinadorRegional || state.value == humanitarianAidForm.parameters.codeStates.GeneraciondeGuia)
						{
							var objectSelect = document.getElementById(humanitarianAidForm.parameters.internalNameFields.RutaOrigen);
							var textSelected = objectSelect.value;
							
							var query = String.format(humanitarianAidForm.parameters.queryCR,textSelected);
							sputilities.getItems(humanitarianAidForm.parameters.listNameRegionalCoordinators,query,function(data){
								
								var resultItems = JSON.parse(data).d.results;
								if(resultItems.length > 0)
								{
									var items = resultItems[0].Coordinador.EMail;
									var find = false;
									if(items == userCurrent){
										find = true;
									}
									
									humanitarianAidForm.parameters.initialState = state.value;
									
									if(state.value == humanitarianAidForm.parameters.codeStates.CoordinadorRegional)
									{				
										if(find)
										{
											humanitarianAidForm.showComments(state.value);
										}
										else
										{
											humanitarianAidForm.showComments(state.value);
											humanitarianAidForm.hideButtons(state.value);
											humanitarianAidForm.showPopUp();
										}
									}
									
									if(state.value == humanitarianAidForm.parameters.codeStates.GeneraciondeGuia)
									{				
										if(find)
										{
											humanitarianAidForm.showComments(state.value);
										}
										else
										{
											humanitarianAidForm.showComments(state.value);
											humanitarianAidForm.hideButtons(state.value);
											humanitarianAidForm.showPopUp();
										}
									}
								}				
							});
						}
					}
					else
					{
						humanitarianAidForm.showComments(state.value);
						humanitarianAidForm.hideButtons(state.value);
						humanitarianAidForm.showPopUp();
					}
				}
		}
		catch(e){
			console.log(e);
		}	
	},
	hideButtons:function(state){
	
		if(state == humanitarianAidForm.parameters.codeStates.DirectorLogistica)
		{
			var bottonApp = document.getElementById(humanitarianAidForm.parameters.controlsHtml.Enviar);
			if(bottonApp != null)
			{
				bottonApp.style.display = humanitarianAidForm.parameters.htmlRequiered.none;
			}
		}
		else if(state == humanitarianAidForm.parameters.codeStates.CoordinadorRegional)
		{
			var bottonApp = document.getElementById(humanitarianAidForm.parameters.controlsHtml.Enviar);
			var comments  = document.getElementById(humanitarianAidForm.parameters.internalNameFields.ComentariosCoordinadorRegional);
			var icon = document.getElementById(humanitarianAidForm.parameters.controlsHtml.iconTH);
			
			if(bottonApp != null && bottonRej != null && comments != null && icon != null)
			{
				comments.style.pointerEvents = humanitarianAidForm.parameters.htmlRequiered.none;
				icon.style.pointerEvents = humanitarianAidForm.parameters.htmlRequiered.none;
				comments.style.color = humanitarianAidForm.parameters.controlsHtml.colorDisable;
				comments.style.borderColor = humanitarianAidForm.parameters.controlsHtml.borderDisable;	
				bottonRej.style.display = humanitarianAidForm.parameters.htmlRequiered.none;	
			}
		
		}
	},
	showPopUp:function(){
		var html = String.format(humanitarianAidForm.parameters.formats.htmlNotAllowed,humanitarianAidForm.parameters.bodyItem);
		var formContent = document.querySelector(humanitarianAidForm.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}	
	},
	showComments:function(state){
		if(state == humanitarianAidForm.parameters.codeStates.DirectorLogistica)
		{
			var comments = document.querySelector(humanitarianAidForm.parameters.fieldsHTML.ComentariosDirectorLogistica);	
			if(comments != null)
			{
				comments.setAttribute(humanitarianAidForm.parameters.hidden,false);
				comments.querySelector(humanitarianAidForm.parameters.errorInvalid.areaComments).value="";
	
			}
		}
		else if(state == humanitarianAidForm.parameters.codeStates.CoordinadorRegional)
		{
			var comments = document.querySelector(humanitarianAidForm.parameters.fieldsHTML.ComentariosCoordinadorRegional);	
			if(comments != null)
			{
				comments.setAttribute(humanitarianAidForm.parameters.hidden,false);
			}
		}
	}
}
humanitarianAidForm.initialize();