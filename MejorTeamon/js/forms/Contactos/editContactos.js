﻿spForms.options={
	actions:[
		{
			type:spForms.parameters.updateAction,
			name:"Actualizar",
			id:"enviar",
			success:function (){
				return true				
			},
			error:function (){},
			preSave: function (){
				return contactosForm.saveJsonFields()		
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:"Cancelar",
			id:"cancelar",
			callback:""
		},
	],	
	contentTypeName:"Contactos",
	containerId:"spForm",
	successLoadEditForm:function()
	{
		var loadControl = setInterval(function(){			

			if(contactosForm.parameters.itemsConfig != null)
			{				
				contactosForm.loadEditFrom();
				contactosForm.loadItems()
			}
			clearInterval(loadControl);	
		},500);

	}
};

var contactosForm = {
	parameters:{
		listName:"ConfiguracionContactos",
		query:"?$select=ID,Title,NombreCampo,ValoresCampos,Obligatorio,NombreSeccion/Title,NombreSeccion/ID,Created&$expand=NombreSeccion&$orderby=NombreSeccion/ID,Created",
		inputInfoPrimaria: [],
		inputCNotificacion: [],
		inputCLocales:[],
		inputSAsistencia:[],
		divID:"<div class='form-group row' id='tabs-contactos' sp-static-name='contactos-tbs'><ul id='nav-content' class='nav nav-tabs'></ul><div id='tab-content' class='tab-content'></div></div>",
		containerContent:"",
		contaierListTabs:"",
		bodyListTabs:"<li class='{0}'><a href='#{1}' data-toggle='tab' aria-expanded='{2}'>{3}</a></li>",		
		bodyTabPanel:"<div class='tab-pane fade {0}' id='{1}'></div>",
        htmlSectionRow:"<div class='form-group row col-tabs' sc-name='{1}'>{0}</div>",
        htmlSectionRowHeader:"<div class='form-group row col-header'>{0}</div>",
		htmlSectionHeaderSingle:"<div class='col-md-4'>{0}</div>",
		htmlSectionHeaderMultiple:"<div class='col-md-3'>{0}</div>",
		htmlSectionInput:"<input type='text' class='form-control input-lg' placeholder='{0}' required='{1}' name='{2}_custom{3}'>",
		htmlSectionLabel:"<label class='control-label' name='{0}_custom{1}'>{2}</label>",
		htmlSectionLabelHeader:"<label class='control-label column-header'>{0}</label>",
		itemsConfig:null,
        actionsTabs:["false","true","active","active in"],
        headers:null,
        structColum: ""
	},
	loadConfiguration:function(){
	
		try {			
				//running the query with parameters
				sputilities.getItems(contactosForm.parameters.listName,contactosForm.parameters.query,function(data){				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				contactosForm.parameters.itemsConfig =resultItems; 			
				
			},function(xhr){ console.log(xhr)});
			
		}
		catch(e)
		{
		 console.log(e);
		}
	},
	loadEditFrom: function(){
		
		//insert div container tabs
		var nodeForm = document.querySelector('div[sp-static-name="Region"]');
		nodeForm.insertAdjacentHTML('afterend',contactosForm.parameters.divID);	
		//get node that contains UL
		var nodeUL = document.getElementById('nav-content');		
		//get node that contains DIV Panel	
		var nodeDIV = document.getElementById('tab-content');
		
		var items = contactosForm.parameters.itemsConfig;
		var section = items[0].NombreSeccion.Title;		
		var initialTab = 0;
		var htmlTabs ="";
		var htmlBodyTabs ="";
		var htmlTableHeaders="";
        var htmlTableContent="";
        var htmlTableRows="";
		
		for(i=0; i < items.length; i++)
		{			
			if(initialTab == 0)
			{
					//Create first Tabs active
					htmlTabs = String.format(contactosForm.parameters.bodyListTabs,"active",items[i].Title,"true",items[i].NombreSeccion.Title);
					htmlBodyTabs = String.format(contactosForm.parameters.bodyTabPanel,"active in",items[i].Title);
					
					nodeUL.insertAdjacentHTML('beforeend', htmlTabs);
					nodeDIV.insertAdjacentHTML('beforeend', htmlBodyTabs);
					initialTab = 1;		
					
                    //Get header html table	
                    contactosForm.parameters.headers = [""];							
					var structColum ="";
                    var headers = contactosForm.parameters.headers.concat(items[i].ValoresCampos.split(','));
                    contactosForm.parameters.headers =headers;
					if(headers.length == 3)
					{
						 contactosForm.parameters.structColum = contactosForm.parameters.htmlSectionHeaderSingle;
					}
					else if(headers.length == 4)
					{
						 contactosForm.parameters.structColum = contactosForm.parameters.htmlSectionHeaderMultiple;
					}
					
					for(j=0; j < headers.length; j++)
					{	
                        var htmlElement = "";
						var contentHtml = "";
                        if(j==0) {

                            htmlElement = String.format(contactosForm.parameters.htmlSectionLabel, items[i].Title, items[i].ID, items[i].NombreCampo);
                            contentHtml= String.format(contactosForm.parameters.structColum, htmlElement);
                            htmlTableRows += contentHtml;
                        }
                        else {
                            
                            htmlElement = String.format(contactosForm.parameters.htmlSectionInput, headers[j], items[i].Obligatorio, items[i].Title, items[i].ID);
                            contentHtml= String.format(contactosForm.parameters.structColum, htmlElement);
                            htmlTableRows += contentHtml;
                        }
						var label =  String.format(contactosForm.parameters.htmlSectionLabelHeader,headers[j]);
						var contentlabel = String.format(contactosForm.parameters.structColum, label);
						htmlTableHeaders+= contentlabel;						
					}
					
					//Concat headers && first item
                    htmlTableContent += String.format(contactosForm.parameters.htmlSectionRowHeader,htmlTableHeaders);
                    htmlTableContent += String.format(contactosForm.parameters.htmlSectionRow,htmlTableRows,items[i].Title);
                    htmlTableHeaders = "";
                    htmlTableRows ="";
			}
			else
			{
				if(items[i].NombreSeccion.Title != section)
				{					
					// Concat items table in tabs before create new tab
					var nodeTabs = document.getElementById(items[i-1].Title);
					nodeTabs.insertAdjacentHTML('beforeend', htmlTableContent);
					htmlTableContent = "";					
					// Create others tabs not active
					htmlTabs = String.format(contactosForm.parameters.bodyListTabs,"",items[i].Title,"false",items[i].NombreSeccion.Title);
					htmlBodyTabs = String.format(contactosForm.parameters.bodyTabPanel,"",items[i].Title);					
					nodeUL.insertAdjacentHTML('beforeend', htmlTabs);
					nodeDIV.insertAdjacentHTML('beforeend', htmlBodyTabs);
					section =items[i].NombreSeccion.Title;										
                    //Get header html new table		
                    contactosForm.parameters.headers = [""];			
					var structColum ="";
                    var headers = contactosForm.parameters.headers.concat(items[i].ValoresCampos.split(','));
                    contactosForm.parameters.headers =headers;
                    
					if(headers.length == 3)
					{
						contactosForm.parameters.structColum = contactosForm.parameters.htmlSectionHeaderSingle;
					}
					else if(headers.length == 4)
					{
						contactosForm.parameters.structColum = contactosForm.parameters.htmlSectionHeaderMultiple;
					}
					
					for(j=0; j < headers.length; j++)
					{	
                        var htmlElement = "";
						var contentHtml = "";
                        if(j==0) {

                            htmlElement = String.format(contactosForm.parameters.htmlSectionLabel, items[i].Title, items[i].ID, items[i].NombreCampo);
                            contentHtml= String.format(contactosForm.parameters.structColum, htmlElement);
                            htmlTableRows += contentHtml;
                        }
                        else {
                            
                            htmlElement = String.format(contactosForm.parameters.htmlSectionInput, headers[j], items[i].Obligatorio, items[i].Title, items[i].ID);
                            contentHtml= String.format(contactosForm.parameters.structColum, htmlElement);
                            htmlTableRows += contentHtml;
                        }
						var label =  String.format(contactosForm.parameters.htmlSectionLabelHeader,headers[j]);
						var contentlabel = String.format(contactosForm.parameters.structColum, label);
						htmlTableHeaders+= contentlabel;						
					}
					
					//Concat headers && first item
                    htmlTableContent += String.format(contactosForm.parameters.htmlSectionRowHeader,htmlTableHeaders);
                    htmlTableContent += String.format(contactosForm.parameters.htmlSectionRow,htmlTableRows, items[i].Title);
                    htmlTableRows ="";
                    htmlTableHeaders = "";
				}
				else
				{
                    //concat all rows
                    var headers = contactosForm.parameters.headers;
                    for(j=0; j < headers.length; j++)
					{	
                        var htmlElement = "";
						var contentHtml = "";
                        if(j==0) {

                            htmlElement = String.format(contactosForm.parameters.htmlSectionLabel, items[i].Title, items[i].ID, items[i].NombreCampo);
                            contentHtml= String.format(contactosForm.parameters.structColum, htmlElement);
                            htmlTableRows += contentHtml;
                        }
                        else {
                            
                            htmlElement = String.format(contactosForm.parameters.htmlSectionInput, headers[j], items[i].Obligatorio, items[i].Title, items[i].ID);
                            contentHtml= String.format(contactosForm.parameters.structColum, htmlElement);
                            htmlTableRows += contentHtml;
                        }
                    }
                    htmlTableContent += String.format(contactosForm.parameters.htmlSectionRow,htmlTableRows,items[i].Title);
                    htmlTableRows ="";
                }
                
                if(i==items.length - 1)
			    {
					// Concat items table in tabs before create new tab
					var nodeTabs = document.getElementById(items[i].Title);
					nodeTabs.insertAdjacentHTML('beforeend', htmlTableContent);
					htmlTableContent = "";
			    }				
			}			
		}	
    },
    saveJsonFields:function(){
	   var isvalid=true;
	   var json = [];
       var datos = document.querySelectorAll('.col-tabs');
	   var seccion = datos[0].getAttribute('sc-name');
	   
	    for(i=0;i<datos.length;i++)
		{
			if(seccion != datos[i].getAttribute('sc-name'))
			{	
				document.querySelector("div[sp-static-name='"+ seccion +"'] textarea").value = JSON.stringify(json);	
				var children = datos[i].querySelectorAll('input');				
				var data = new Object();
				data["label"]=datos[i].querySelector('label').innerHTML;
				json =[];
				for(j=0;j<children.length;j++)
				{					
					data["value"+ j] = children[j].value;
				}
				
				json.push(data);		
				seccion = datos[i].getAttribute('sc-name');

			}
			else
			{
				var children = datos[i].querySelectorAll('input');
				var data = new Object();
				data["label"]=datos[i].querySelector('label').innerHTML;
				for(j=0;j<children.length;j++)
				{					
					data["value"+ j] = children[j].value;
				}
				
				json.push(data);
			}

			if(i == datos.length -1){
				document.querySelector("div[sp-static-name='"+ seccion +"'] textarea").value = JSON.stringify(json);		
			}
		}		
		return isvalid;
	},
	getValuesSection:function(section){
		var data = document.querySelector("div[sp-static-name='"+ section+"'] textarea").value;
		return JSON.parse(data);
	},
	loadItems:function(){
	  var datos = document.querySelectorAll('.col-tabs');
	  var seccion = datos[0].getAttribute('sc-name');
	  var valuesItems = contactosForm.getValuesSection(seccion);
	  var index=0;	  
	  
	  for(i=0;i<datos.length;i++)
		{
			if(seccion != datos[i].getAttribute('sc-name'))
			{	
				seccion = datos[i].getAttribute('sc-name');
				valuesItems = contactosForm.getValuesSection(seccion);
				index=0;
				
				var children = datos[i].querySelectorAll('input');	
				for(j=0;j<children.length;j++)
				{
					if(index < valuesItems.length)
					{					
					 	if(valuesItems[index]["value"+j] != undefined)
					 	{
							children[j].value = valuesItems[index]["value"+j]; 
						}
					}
					else
					{
						break;
					}
				}
				index++;
			}
			else
			{
				var children = datos[i].querySelectorAll('input');
				for(j=0;j<children.length;j++)
				{
					if(index < valuesItems.length)
					{
						if(valuesItems[index]["value"+j] != undefined)
					 	{
							children[j].value = valuesItems[index]["value"+j]; 
						}
					}
					else
					{
						break;
					}
				}
				
				index++;				
			}			
		}
	}
}
sputilities.callFunctionOnLoadBody('contactosForm.loadConfiguration');
spForms.loadEditForm();