﻿var ctrlForm = {
	parameters:{
		formats:{
			"queryStaticNameRow":"[sp-static-name='{0}']",
			"htmlNotAllowed":"<div><h4>{0}</h4></div>",
			"querySelectAllRadio":'input[type="radio"][name="{0}"]',
			"querySelectRadio":'input[type="radio"][value="{0}"]:checked',
			"queryRadio":'input[type="radio"][value="{0}"]',
			"emailUsers":"{0};",
			"dateFormat":"DD-MM-YYYY",
			"dateCustomFormat":"{1}{0}{2}{0}{3}",
			"formFormat":"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}"
		},
		forms:{
			"NewForm":"ctrlForm.redirectCustomForm('NuevaSolicitud')",
			"EditForm":"ctrlForm.redirectCustomForm('CorregirSolicitud')",
			"DispForm":"ctrlForm.redirectCustomForm('VerSolicitud')",
			"NuevaSolicitud":"ctrlForm.loadNew()",
			"CorregirSolicitud":"ctrlForm.loadEditApplicant()",
			"VerSolicitud":"ctrlForm.loadReview()"
		},
		messages:{
			"idSaveForm":"enviarSolicitud",
			"idCancelForm":"cancelar",
			"textSaveForm":"Enviar",
			"textCancelForm":"Cancelar",
			"textEditForm":"Enviar",
			"textNotAllowed":"Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior"
		},
		columns:{
			"comments":{
				"staticName":"ComentariosTH"
			},
			"displayName":{
				"staticName":"NombrePersona1"
			},
			"idDoc":{
				"staticName":"DocumentoIdentidad"
			},
			"cellPhone":{
				"staticName":"NumeroCelular"
			},
			"email":{
				"staticName":"CorreoElectronico"
			},
			"childs":{
                "staticName":"TieneHijos"
            },
            "countChilds":{
                "staticName":"CantidadHijos"
            },
            "typeOfHousing":{
                "staticName":"TipoVivienda",
                "values":{
                    "propia":"0",
                    "familiar":"1",
                    "arrendada":"2",
                    "otras":"3"					
                }
            },
            "otherTypeOfHousing":{
                "staticName":"TipoViviendaOtra"
            },
            "reason":{
            	"staticName":"MotivoPrestamo"
            },
            "admissionDate":{
            	"staticName":"FechaIngreso"
            }
			
		},
		states:[
			"Solicitado",
			"Radicado"
		]
		,
		parametersHTML:{
			"spHidden":"sp-hidden",
			"spDisable":"sp-disable"
		},
		selectorFormContent:".form-content",
		cellPhone:"CellPhone",
		functionLoadMyProperties:"ctrlForm.loadMyProperties",
		containerId:"container-form",
		title:"Solicitud fondo de vivienda",
		description:"Complete la información para la solicitud",
		descriptionEdit:"Modifique la información de la solicitud",
		descriptionReview:"Resultado de la solicitud",
		contentTypeName:"Fondo de Vivienda",
		formIsLoaded:false,
		allowAttachments:true,
		requiredAttachments:false,
		maxAttachments:20,
		minAttachments:0,
		maxSizeInMB:3,
		listNameConsecutive:"ControlRadicados",
		idConsecutive:18,
		listUsersFlow:"ResponsablesTH",
		viewUrl:"",
	    editUrl:"",
	    approveUrl:"",
	    applyUrl:"",
	    listViewApply:"",
	    listViewApprove:"",
	    queryUsers:"?$filter=FlujoAsociadoId eq 18&$select=ResponsablesAbonos/EMail,EtapaResponsable&$expand=ResponsablesAbonos",
	    flowUrl:"https://prod-38.westus.logic.azure.com:443/workflows/8d289d128f3e42a18986baff9e53af17/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=Rrk_pHCf55kd9xI_IxNti-mEFQzGhXUyhViNmVGvjCA"
	},
	global:{
		formName:null,
		createdBy:null,
		created:null,
		approveUsers:"",
		applyUsers:""
	},
	initialize:function(){
		var partsOfUrl = window.location.href.split('?')[0].split('/');
		ctrlForm.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];		
		eval(ctrlForm.parameters.forms[ctrlForm.global.formName]);
	},
	callFlowService:function(){
	
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
		ctrlForm.parameters.viewUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[5],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    ctrlForm.parameters.editUrl = String.format(ctrlForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    	
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":ctrlForm.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":ctrlForm.global.created,
		        "comentarios":spForms.loadedFields.ComentariosTH.value
		    },
		    "state":spForms.loadedFields.EstadoSolicitudFondoVivienda.value,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":ctrlForm.parameters.listNameConsecutive,
		    "idConsecutive":ctrlForm.parameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
		    "viewUrl":ctrlForm.parameters.viewUrl,
		    "editUrl":ctrlForm.parameters.editUrl
		}
		utilities.http.callRestService({
			url:ctrlForm.parameters.flowUrl,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	getUsers:function(){
		sputilities.getItems(ctrlForm.parameters.listUsersFlow,ctrlForm.parameters.queryUsers,
			function(data){
				var data = utilities.convertToJSON(data);
				if(data != null){
					var items = data.d.results;
					for(var i = 0; i < items.length ;i++){
						var item = items[i];
						for(var j= 0; j < item.ResponsablesAbonos.results.length;j++){
							var user = item.ResponsablesAbonos.results[j];
							if(item.EtapaResponsable == ctrlForm.parameters.states[2]){
								ctrlForm.global.applyUsers += String.format(ctrlForm.parameters.formats.emailUsers,user.EMail);
							}else{
								ctrlForm.global.approveUsers += String.format(ctrlForm.parameters.formats.emailUsers,user.EMail);
							}
						}
					}	
				}
			},
			function(xhr){
				console.log(xhr);
			}
		);
	},
	changeState:function(state){
		var radio = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio,state));
		if(radio != null){
			radio.click();
		}
	},
	hideNotAllowed:function(){
		var html = String.format(ctrlForm.parameters.formats.htmlNotAllowed,ctrlForm.parameters.messages.textNotAllowed);
		var formContent = document.querySelector(ctrlForm.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	},
	validateAllUsers:function(){
		if(spForms.currentItem.AuthorId != sputilities.contextInfo().userId){
			ctrlForm.hideNotAllowed();
			return false;
		}
	},
	validateEditUser:function(){
		if(spForms.currentItem.AuthorId != sputilities.contextInfo().userId){
			ctrlForm.hideNotAllowed();
			return false;
		}
		return true;
	},
	hideComments:function(){
		var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.comments.staticName));
		if(ctrlComments != null){
			ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,true);
		}
	},
	loadReview:function(){
		var actions = [
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionReview;
		ctrlForm.loadEdit(actions,ctrlForm.validateAllUsers);
	},
	loadEditApplicant:function(){
		var actions = [
			{
				type:spForms.parameters.updateAction,
				name:ctrlForm.parameters.messages.textEditForm,
				id:ctrlForm.parameters.messages.idEditForm,
				success:function (){
					ctrlForm.callFlowService();
					return false;
				},
				error:function (){return true;},
				preSave: function (){					
					ctrlForm.changeState(ctrlForm.parameters.states[1]);
					return true;
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:ctrlForm.parameters.messages.textCancelForm,
				id:ctrlForm.parameters.messages.idCancelForm,
				callback:null
			},
		];
		ctrlForm.parameters.description = ctrlForm.parameters.descriptionEdit;
		ctrlForm.loadEdit(actions,function(){			
				if(ctrlForm.validateEditUser()){
					ctrlForm.hideComments();
					var ctrlComments = document.getElementById(ctrlForm.parameters.columns.comments.staticName);
					if(ctrlComments != null){
						ctrlComments.value = "";
					}
				}			
		});
	},
	loadEdit:function(actions,callBack){
		ctrlForm.getUsers();
		spForms.options={
			actions:actions,
			title:ctrlForm.parameters.title,
			description:ctrlForm.parameters.description,
			contentTypeName:ctrlForm.parameters.contentTypeName,
			containerId:ctrlForm.parameters.containerId,
			allowAttachments:ctrlForm.parameters.allowAttachments,
		  	requiredAttachments:ctrlForm.parameters.requiredAttachments,
		  	maxAttachments:ctrlForm.parameters.maxAttachments,
		  	minAttachments:ctrlForm.parameters.minAttachments,
		  	maxSizeInMB:ctrlForm.parameters.maxSizeInMB,
			successLoadEditForm:function(){	
				ctrlForm.hideComments();
				ctrlForm.global.createdBy = spForms.currentItem.Author.EMail;
				ctrlForm.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(ctrlForm.parameters.formats.dateFormat);
				ctrlForm.formIsLoaded = true;
				
				
				if(typeof callBack== "function"){
					callBack();
				}
			} 
		};
		spForms.loadEditForm();

	},
	loadNew:function(){	
		ctrlForm.getUsers();
		ctrlForm.global.createdBy = sputilities.contextInfo().userEmail;
		ctrlForm.global.created = moment.utc(new Date()).format(ctrlForm.parameters.formats.dateFormat);
		sputilities.callFunctionOnLoadBody(ctrlForm.parameters.functionLoadMyProperties);
		ctrlForm.loadNewForm();
	},
	loadNewForm:function(){
		spForms.options={
			actions:[
				{
					type:spForms.parameters.saveAction,
					name:ctrlForm.parameters.messages.textSaveForm,
					id:ctrlForm.parameters.messages.idSaveForm,
					success:function (){
						ctrlForm.callFlowService();
						return false;
					},
					error:function (){return true;},
					preSave: function (){
						ctrlForm.validateRequiredFields();
						ctrlForm.changeState(ctrlForm.parameters.states[0]);
						return true;
					}
				},
				{
					type:spForms.parameters.cancelAction,
					name:ctrlForm.parameters.messages.textCancelForm,
					id:ctrlForm.parameters.messages.idCancelForm,
					callback:null
				},
			],
			title:ctrlForm.parameters.title,
			description:ctrlForm.parameters.description,
			contentTypeName:ctrlForm.parameters.contentTypeName,
			containerId:ctrlForm.parameters.containerId,
			allowAttachments:ctrlForm.parameters.allowAttachments,
		  	requiredAttachments:ctrlForm.parameters.requiredAttachments,
		  	maxAttachments:ctrlForm.parameters.maxAttachments,
		  	minAttachments:ctrlForm.parameters.minAttachments,
		  	maxSizeInMB:ctrlForm.parameters.maxSizeInMB,
			successLoadNewForm:function(){
				ctrlForm.formIsLoaded = true;
				ctrlForm.hideDependentFields();
				ctrlForm.hideComments();
			} 
		};
		spForms.loadNewForm();
	},
	loadMyProperties:function(){
		sputilities.getUserProfileProperties(sputilities.contextInfo().userEmail,
			function(userInfo){
				var objUser = utilities.convertToJSON(userInfo);
				if(objUser != null){
					var displayName = objUser.d.DisplayName;
					var email = objUser.d.Email;
					var cellPhone = ctrlForm.findProperty(objUser,ctrlForm.parameters.cellPhone);
					var validateFormIsLoaded = setInterval(function(){
						if(ctrlForm.formIsLoaded){
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.displayName.staticName,displayName);
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.email.staticName,email);
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.cellPhone.staticName,cellPhone);
							clearInterval(validateFormIsLoaded);
						}
					},500);
				}
			},
			function(xhr){
				console.log(xhr)
			}
		);
	},
	setValueToControl:function(staticName,value){
		if(value != null){
			var ctrl = document.getElementById(staticName);
			if(ctrl != null){
				ctrl.value = value;
			}
		}
	},
	findProperty:function(objUser,name){
		var properties = objUser.d.UserProfileProperties.results;
		for(var i = 0; i < properties.length; i++){
			var property = properties[i];
			if(property.Key == name){
				return property.Value;
			}
		}
	},
	redirectCustomForm:function(name){
		window.location.href = window.location.href.replace(ctrlForm.global.formName,name);
	},
	validateEmailField:function(){
		var emailRow = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.email.staticName));
		var email = document.getElementById(ctrlForm.parameters.columns.email.staticName).value;
		
		if(ctrlForm.validateEmail(email)){
			spForms.loadedFields[ctrlForm.parameters.columns.email.staticName].required = false;
			emailRow.classList.remove('has-error');
			emailRow.removeAttribute('sp-invalid','Esto no puede estar en blanco');
			emailRow.removeAttribute('sp-required','true');
			emailRow.setAttribute('sp-required','false');			
		}else{
			spForms.loadedFields[ctrlForm.parameters.columns.email.staticName].required = true;
			emailRow.setAttribute('sp-invalid','El formato de correo no es valido');
			
		}
	},
	validateChildsField:function(){
		var childs = document.getElementById(ctrlForm.parameters.columns.childs.staticName);
		var countChilds = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.countChilds.staticName));
		if( !childs.checked){
			spForms.loadedFields[ctrlForm.parameters.columns.countChilds.staticName].required = false;
			countChilds.classList.remove('has-error');
			countChilds.removeAttribute('sp-invalid','Esto no puede estar en blanco');
			countChilds.removeAttribute('sp-required','true');
			countChilds.setAttribute('sp-required','false');
		}else{
			spForms.loadedFields[ctrlForm.parameters.columns.countChilds.staticName].required = true;
		}

	},
	validateTypeHousingField:function(){
		var typeOfHousing = document.getElementById(ctrlForm.parameters.columns.typeOfHousing.staticName);
		var otherTypeOfHousing = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.otherTypeOfHousing.staticName));		
	
		if(typeOfHousing.value != ctrlForm.parameters.columns.typeOfHousing.values.otras){
			spForms.loadedFields[ctrlForm.parameters.columns.otherTypeOfHousing.staticName].required = false;
			otherTypeOfHousing.classList.remove('has-error');
			otherTypeOfHousing.removeAttribute('sp-invalid','Esto no puede estar en blanco');
			otherTypeOfHousing.removeAttribute('sp-required','true');
			otherTypeOfHousing.setAttribute('sp-required','false');

		}else{
			spForms.loadedFields[ctrlForm.parameters.columns.otherTypeOfHousing.staticName].required = true;
		}
	},
	validateAdmissionDateField:function(){
		var admissionSection = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.admissionDate.staticName));
		var admissionArray = document.getElementById(ctrlForm.parameters.columns.admissionDate.staticName).value.split('/');
		
		if(admissionArray.length == 3){			
			var admissionDate = String.format(ctrlForm.parameters.formats.dateCustomFormat,'/',admissionArray[1],admissionArray[0],admissionArray[2]);			
			var today = new Date();
			var selectDay = new Date(admissionDate);
			if(selectDay <= today){
				spForms.loadedFields[ctrlForm.parameters.columns.admissionDate.staticName].required = false;
				admissionSection.classList.remove('has-error');
				admissionSection.removeAttribute('sp-invalid','Esto no puede estar en blanco');
				admissionSection.removeAttribute('sp-required','true');
				admissionSection.setAttribute('sp-required','false');
			}else{
				spForms.loadedFields[ctrlForm.parameters.columns.admissionDate.staticName].required = true;
				admissionSection.setAttribute('sp-invalid','La fecha debe ser inferior al día de hoy');
			}			
		}
		
	},
	validateReasonField:function(){
		var reason = document.querySelectorAll(String.format(ctrlForm.parameters.formats.querySelectAllRadio,ctrlForm.parameters.columns.reason.staticName));
		var reasonSection = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.reason.staticName));
		var checked = false
		
		for(var i = 0;i<reason.lenght;i++){
			if(reason[i].checked){
				checked = true
			}
		}
		if(checked){
			spForms.loadedFields[ctrlForm.parameters.columns.reason.staticName].required = false;
			reasonSection.classList.remove('has-error');
			reasonSection.removeAttribute('sp-invalid','Esto no puede estar en blanco');
			reasonSection.removeAttribute('sp-required','true');
			reasonSection.setAttribute('sp-required','false');

		}else{
			spForms.loadedFields[ctrlForm.parameters.columns.reason.staticName].required = true;		
			
		}		
	},
	validateRequiredFields:function(){
		ctrlForm.validateEmailField();
		ctrlForm.validateChildsField();	
		ctrlForm.validateTypeHousingField();	
		ctrlForm.validateAdmissionDateField();
		ctrlForm.validateReasonField();
	},
	hideChildsField:function(){
		var childs = document.getElementById(ctrlForm.parameters.columns.childs.staticName);
		var countChilds = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.countChilds.staticName));
		
		childs.setAttribute('onchange','ctrlForm.hideChildsField()');
		
		if( childs.checked){
			//countChilds.classList.remove('hidden');
			countChilds.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,false)
			
		}else{
			//countChilds.classList.add('hidden');
			countChilds.setAttribute(ctrlForm.parameters.parametersHTML.spHidden,true)
		}
	},
	hideTypeHousingField:function(){
		var typeOfHousing = document.getElementById(ctrlForm.parameters.columns.typeOfHousing.staticName);
		var otherTypeOfHousing = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow,ctrlForm.parameters.columns.otherTypeOfHousing.staticName));		
		
		typeOfHousing.setAttribute('onchange','ctrlForm.hideTypeHousingField()');
		
		if(typeOfHousing.value == ctrlForm.parameters.columns.typeOfHousing.values.otras){
			otherTypeOfHousing.classList.remove('hidden');
		}else{
			otherTypeOfHousing.classList.add('hidden');
		}
	},
	hideDependentFields:function(){
		ctrlForm.hideChildsField();
		ctrlForm.hideTypeHousingField();
	},
	validateEmail: function (email){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);
	
	}
}
ctrlForm.initialize();