﻿var lustrumForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx",
			inputsFields:"input[name='{0}']"
		},
		query:"?$Select=FlujoAsociado/Title,ResponsablesAbonos/EMail,ID&$expand=FlujoAsociado,ResponsablesAbonos&$filter=Title eq '{0}'",
		listName:"ResponsablesTH",
		assingTo:{
			Ventanilla:"Lustros Ventanilla TH",
			Novedades:"Lustros Novedades"
		},
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']",
			Comentarios:"[sp-static-name='ComentariosTH']",
			ComentariosJefe:"[sp-static-name='ComentariosJefe']",
			Modalidad:"[sp-static-name='Modalidad']",
			FechaInicio:"[sp-static-name='FechaInicioLustro']",
			FechaFin:"[sp-static-name='FechaFinLustro']"
		
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico',	
			Comentarios:"ComentariosTH",
			ComentariosJefe:"ComentariosJefe",
			Estado:"EstadoLustro",
			Modalidad:"Modalidad",
			FechaInicio:"FechaInicioLustro",
			FechaFin:"FechaFinLustro"			
		},		
		controlsHtml:{
			Attachment:"sp-forms-add-attachment",
			Aprobar:"btn-aprobar",
			Corregir:"btn-corregir",
			Aplicar:"btn-aplicar",
			Rechazar:"btn-rechazar",
			classDelete:".delete-doc",
			classGroup:".form-group.row",
			colorDisable:"#B1B1B1",
			borderDisable:"#E1E1E1",
			iconTH:"ico-help-ComentariosTH",
			iconJefe:"ico-help-ComentariosJefe"
		},
		errorInvalid:{text:"Los comentarios son obligatorios",area:"textarea"},
		htmlRequiered:{label:"label",block:"block",none:"none"},		
		invalid:"sp-invalid",
		hidden:"sp-hidden",		
		resultItems:null,	
		codeStates:{Solicitado:"0",Ventanilla:"1",Novedades:"2",Aplicada:"3",RechazadoVentanilla:"4",Jefe:"5",RechazadoJefe:"6"},	
		userProperties:"CellPhone",
		bodyItem:"<div class='form-group row'><label>Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior</label></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
		containerForm:".form-content",
		TitlePopUp:"Acceso Denegado",
		formsParameters:{
			title:"",
			description:"",
			titleApprove:"Aprobar Solicitud Lustros",
			titleAplicate:"Aplicar Solicitud Lustros",
			descriptionApprove:"Completa la información correspondiente, para aprobar o rechazar la solicitud",
			descriptionAplicate:"Revisa la información de la solicitud, para aplicar la novedad",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Lustros",
			containerId:"container-form"			
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"lustrumForm.loadApprove()",
			"AprobarJefe":"lustrumForm.loadApproveBoss()",
			"AplicarSolicitud":"lustrumForm.loadApplicate()",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Novedades":"Vista Novedades",
			"Ventanilla":"Vista Ventanilla"
		},
		flowParameters:{
			urlFlow:"https://prod-62.westus.logic.azure.com:443/workflows/fc39cfd50202482880d2c014611bb05f/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=Nz2wek08CSeVW2xpMe1D9lCk1flDpl289H2wzNKoteU",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",
	   		approveBossUrl:"",
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Lustros Ventanilla TH",
	    	applyUsers:"Lustros Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:11,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Lustros"
		},
		formsState:{
			AprobarSolicitud:"1",
			AprobarJefe:"5",
			AplicarSolicitud:"2"
		},
		global:{
			formName:null,
			createdBy:null,
			created:null

		},
		initialState:null,
		modalidad:{disfrute:"Disfrute",dinero:"En dinero"}

	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
	    lustrumForm.parameters.flowParameters.approveUrl = String.format(lustrumForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(lustrumForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    lustrumForm.parameters.flowParameters.approveBossUrl = String.format(lustrumForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(lustrumForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    lustrumForm.parameters.flowParameters.applyUrl = String.format(lustrumForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(lustrumForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    lustrumForm.parameters.flowParameters.editUrl = String.format(lustrumForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(lustrumForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    lustrumForm.parameters.flowParameters.listViewApply = String.format(lustrumForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(lustrumForm.parameters.forms)[6]);
	    lustrumForm.parameters.flowParameters.listViewApprove= String.format(lustrumForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(lustrumForm.parameters.forms)[7]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":lustrumForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":lustrumForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosTH.value,
		        "comentariosJefe":spForms.loadedFields.ComentariosJefe.value,
		        "diasSolicitados":Math.round(spForms.loadedFields.DiasSolicitados.value)
		    },
		    "state":spForms.loadedFields.EstadoLustro.value,
		    "approveUsers":lustrumForm.parameters.flowParameters.approveUsers,
		    "applyUsers":lustrumForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":lustrumForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":lustrumForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":lustrumForm.parameters.flowParameters.listNameAssing,
		    "editUrl":lustrumForm.parameters.flowParameters.editUrl,
		    "approveUrl":lustrumForm.parameters.flowParameters.approveUrl,
		    "applyUrl":lustrumForm.parameters.flowParameters.applyUrl,
		    "approveBossUrl":lustrumForm.parameters.flowParameters.approveBossUrl,
		    "listViewApply":lustrumForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":lustrumForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	lustrumForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:lustrumForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){			
		var partsOfUrl = window.location.href.split('?')[0].split('/');
		lustrumForm.parameters.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];
		eval(lustrumForm.parameters.forms[lustrumForm.parameters.global.formName]);

	},
	loadApprove:function(){	
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:lustrumForm.parameters.formsParameters.buttons.Aprove.Text,
				id:lustrumForm.parameters.formsParameters.buttons.Aprove.Id,
				success:function (){
					lustrumForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return lustrumForm.validateApprove()
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:lustrumForm.parameters.formsParameters.buttons.Reject.Text,
				id:lustrumForm.parameters.formsParameters.buttons.Reject.Id,
				success:function (){
					lustrumForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return lustrumForm.validateReject()
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:lustrumForm.parameters.formsParameters.buttons.Cancel.Text,
				id:lustrumForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		lustrumForm.parameters.formsParameters.description = lustrumForm.parameters.formsParameters.descriptionApprove;
		lustrumForm.parameters.formsParameters.title = lustrumForm.parameters.formsParameters.titleApprove;
		var query = String.format(lustrumForm.parameters.query,lustrumForm.parameters.assingTo.Ventanilla);
		lustrumForm.parameters.query = query;	
		lustrumForm.loadEdit(actions,lustrumForm.loadResponsableState);
		
	},
	loadApproveBoss:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:lustrumForm.parameters.formsParameters.buttons.Aprove.Text,
				id:lustrumForm.parameters.formsParameters.buttons.Aprove.Id,
				success:function (){
					lustrumForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return lustrumForm.validateApprove()
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:lustrumForm.parameters.formsParameters.buttons.Reject.Text,
				id:lustrumForm.parameters.formsParameters.buttons.Reject.Id,
				success:function (){
					lustrumForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return lustrumForm.validateReject()
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:lustrumForm.parameters.formsParameters.buttons.Cancel.Text,
				id:lustrumForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		lustrumForm.parameters.formsParameters.description = lustrumForm.parameters.formsParameters.descriptionApprove;
		lustrumForm.parameters.formsParameters.title = lustrumForm.parameters.formsParameters.titleApprove;
        lustrumForm.loadEdit(actions,lustrumForm.loadResponsableState);
	
	},
	loadApplicate:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:lustrumForm.parameters.formsParameters.buttons.Aplicate.Text,
				id:lustrumForm.parameters.formsParameters.buttons.Aplicate.Id,
				success:function (){
					lustrumForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return lustrumForm.validateApprove()
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:lustrumForm.parameters.formsParameters.buttons.Cancel.Text,
				id:lustrumForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		lustrumForm.parameters.formsParameters.description = lustrumForm.parameters.formsParameters.descriptionAplicate;
		lustrumForm.parameters.formsParameters.title = lustrumForm.parameters.formsParameters.titleAplicate;
		var query = String.format(lustrumForm.parameters.query,lustrumForm.parameters.assingTo.Novedades);
		lustrumForm.parameters.query = query;
		lustrumForm.loadEdit(actions,lustrumForm.loadResponsableState);

	},
	loadEdit:function(actions,sucessFunction){
		spForms.options={	
			actions:actions,
			title:lustrumForm.parameters.formsParameters.title,
			description:lustrumForm.parameters.formsParameters.description,
			contentTypeName:lustrumForm.parameters.formsParameters.contentType,
			containerId:lustrumForm.parameters.formsParameters.containerId,
			allowAttachments:true,
		  	requiredAttachments:false,
		  	maxAttachments:5,
		  	minAttachments:0,
		  	maxSizeInMB:3,
			successLoadEditForm:function(){	
				lustrumForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
				lustrumForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(lustrumForm.parameters.formats.dateFormat);			
				if(typeof sucessFunction== "function"){
					sucessFunction();
				}
			} 
		};
		spForms.loadEditForm();
	},
	validateApprove:function(){
		var row = lustrumForm.parameters.initialState == lustrumForm.parameters.codeStates.Ventanilla ? document.querySelector(lustrumForm.parameters.fieldsHTML.Comentarios) : document.querySelector(lustrumForm.parameters.fieldsHTML.ComentariosJefe);
		var state = document.getElementById(lustrumForm.parameters.internalNameFields.Estado);
		var isValid = true;
		if(lustrumForm.parameters.initialState == lustrumForm.parameters.codeStates.Ventanilla)
		{			
			if(row != null && state!= null){
				row.removeAttribute(lustrumForm.parameters.invalid);
				state.value = lustrumForm.parameters.codeStates.Novedades;
			}
			else
			{
				isValid = false;
			}
		}
		if(lustrumForm.parameters.initialState == lustrumForm.parameters.codeStates.Jefe)
		{		
			if(row != null && state!= null){
				row.removeAttribute(lustrumForm.parameters.invalid);
				state.value = lustrumForm.parameters.codeStates.Ventanilla;
			}
			else
			{
				isValid = false;
			}
		}
		if(lustrumForm.parameters.initialState == lustrumForm.parameters.codeStates.Novedades)
		{	
			if(state!= null){				
				state.value = lustrumForm.parameters.codeStates.Aplicada;
			}
			else
			{
				isValid = false;
			}
		}
		return isValid;
		
	},
	validateReject:function(){
		var row = lustrumForm.parameters.initialState == lustrumForm.parameters.codeStates.Ventanilla ? document.querySelector(lustrumForm.parameters.fieldsHTML.Comentarios): document.querySelector(lustrumForm.parameters.fieldsHTML.ComentariosJefe);
		var state = document.getElementById(lustrumForm.parameters.internalNameFields.Estado);		
		var isValid = true;
		
		if(row != null && state != null)
		{
			var comments = row.querySelector(lustrumForm.parameters.errorInvalid.area);
			if(comments.value != "")
			{
				row.removeAttribute(lustrumForm.parameters.invalid);
				if(lustrumForm.parameters.initialState == lustrumForm.parameters.codeStates.Jefe)
				{
					state.value = lustrumForm.parameters.codeStates.RechazadoJefe;
				}
				else
				{
					state.value = lustrumForm.parameters.codeStates.RechazadoVentanilla;

				}			
			}
			else
			{
				row.setAttribute(lustrumForm.parameters.invalid,lustrumForm.parameters.errorInvalid.text);			
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;

	},	
	loadResponsableState:function(){	
		try
		{
			var userCurrent = _spPageContextInfo.userEmail
			var state = document.getElementById(lustrumForm.parameters.internalNameFields.Estado);
			if(state != null)
			{
				if(state.value == lustrumForm.parameters.formsState[lustrumForm.parameters.global.formName])
				{
					if(state.value == lustrumForm.parameters.codeStates.Novedades || state.value == lustrumForm.parameters.codeStates.Ventanilla)
					{
						sputilities.getItems(lustrumForm.parameters.listName,lustrumForm.parameters.query,function(data){
							
							var resultItems = JSON.parse(data).d.results;
							if(resultItems.length > 0)
							{
								var items = resultItems[0].ResponsablesAbonos.results
								var find = false;
								for(var i=0;i<items.length;i++)
								{
									if(items[i].EMail == userCurrent){
										find = true;
									 	break;
									}
								}
								
								lustrumForm.parameters.initialState = state.value;
								lustrumForm.showDates();
								lustrumForm.showComments(state.value);	
								
								if(state.value == lustrumForm.parameters.codeStates.Novedades)
								{				
									if(!find)
									{										
										lustrumForm.hideNotAllowed();				
									}									
								}
								else if(state.value == lustrumForm.parameters.codeStates.Ventanilla)
								{						
									if(!find)
									{	
										lustrumForm.hideNotAllowed();
									}
								}
							}						
						});
					}
					else
					{
	                    var idUserCreated =  spForms.currentItem.AuthorId;
	                    
						sputilities.getUserInformation(idUserCreated,function(data){
	                        //save the results to an array
	                        var resultItems = utilities.convertToJSON(data);
	                        if(resultItems != null)
	                        {
	                            var email = resultItems.d.EMail;
	
	                            sputilities.getUserProfileProperty('Manager',email,function(data){
	                                var resultProperty =  utilities.convertToJSON(data);
	                                if(resultProperty != null)
	                                {
	                                    var emailJefe = resultProperty.d.GetUserProfilePropertyFor.split('|')[2];
	                                    var find = false;
	
	                                    if(userCurrent == emailJefe)
	                                    {
	                                    	find = true;
	                                    }
	                                    lustrumForm.parameters.initialState = state.value;
										lustrumForm.showDates();
										lustrumForm.showComments(state.value);	

	                                    if(state.value == lustrumForm.parameters.codeStates.Jefe)
										{		
											if(!find)
											{												
												lustrumForm.hideNotAllowed();
											}											
									   }	                                    
	                                }
	                            });
	                        }
	                    
	                    });
                    }
                }
                else
                {	
                    lustrumForm.hideNotAllowed();
                }
		    }
		}
		catch(e){
			console.log(e);
		}	
	},
	showComments:function(state){
		if(state == lustrumForm.parameters.codeStates.Jefe)
		{
			var comments = document.querySelector(lustrumForm.parameters.fieldsHTML.ComentariosJefe);	
			if(comments != null)
			{
				comments.setAttribute(lustrumForm.parameters.hidden,false);
				comments.querySelector(lustrumForm.parameters.errorInvalid.area).value="";
	
			}
		}
		else if(state == lustrumForm.parameters.codeStates.Ventanilla)
		{
			var commentsJefe = document.querySelector(lustrumForm.parameters.fieldsHTML.ComentariosJefe);	
			var comments = document.querySelector(lustrumForm.parameters.fieldsHTML.Comentarios);	
			if(comments != null && commentsJefe != null)
			{
				comments.setAttribute(lustrumForm.parameters.hidden,false);
				commentsJefe.setAttribute(lustrumForm.parameters.hidden,false);
				comments.querySelector(lustrumForm.parameters.errorInvalid.area).value="";
	
			}
		}
		else if(state == lustrumForm.parameters.codeStates.Novedades)
		{
			var commentsJefe = document.querySelector(lustrumForm.parameters.fieldsHTML.ComentariosJefe);	
			var comments = document.querySelector(lustrumForm.parameters.fieldsHTML.Comentarios);	
			if(comments != null && commentsJefe != null)
			{
				comments.setAttribute(lustrumForm.parameters.hidden,false);
				commentsJefe.setAttribute(lustrumForm.parameters.hidden,false);
			}
		}
	},
	showDates:function(){
		
		var row = document.querySelectorAll(String.format(lustrumForm.parameters.formats.inputsFields,lustrumForm.parameters.internalNameFields.Modalidad)+":checked");
		if(row != null)
		{	
			if(row[0].value == lustrumForm.parameters.modalidad.disfrute)
			{
				var fechaInicio = document.querySelector(lustrumForm.parameters.fieldsHTML.FechaInicio);
				var fechaFin = document.querySelector(lustrumForm.parameters.fieldsHTML.FechaFin);
				if(fechaInicio != null && fechaFin != null)
				{
					fechaInicio.setAttribute(lustrumForm.parameters.hidden,false);
					fechaFin.setAttribute(lustrumForm.parameters.hidden,false);
				}
			}
		}
	},
	hideNotAllowed:function(){
		var html = lustrumForm.parameters.bodyItem;
		var formContent = document.querySelector(lustrumForm.parameters.containerForm);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	}
}
lustrumForm.initialize();