﻿var lustrumForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx",
			inputsFields:"input[name='{0}']"
		},
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']",
			Modalidad:"[sp-static-name='Modalidad']",
			FechaInicio:"[sp-static-name='FechaInicioLustro']",
			FechaFin:"[sp-static-name='FechaFinLustro']"
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico',
			Modalidad:"Modalidad",
			FechaInicio:"FechaInicioLustro",
			FechaFin:"FechaFinLustro"	
		},
		resultItems:null,
		invalid:"sp-invalid",
		hidden:"sp-hidden",
		userProperties:"CellPhone", 
		errorInvalid:"El correo electrónico no tiene el formato correcto",
		loadUserInfo:"lustrumForm.loadUserInfo",
		formsIsLoaded:false,
		formsParameters:{
			title:"Solicitud Lustros",
			description:"",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Lustros",
			containerId:"container-form"			
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud Ventanilla TH",
			"AprobarJefe":"Aprobar Jefe Inmediato",
			"AplicarSolicitud":"Aplicar Novedad",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Novedades":"Vista Novedades",
			"Ventanilla":"Vista Ventanilla"
		},
		global:{
			createdBy:null,
			created:null
		},
		htmlRequiered:{position:"beforeend",html:"<i>*</i>",label:"label",block:"block",none:"none"},
		modalidad:{disfrute:"Disfrute",dinero:"En dinero"},
		requiered:"sp-requiered",
		staticName:"sp-static-name"
	},	
	initialize:function(){		
		spForms.loadEditForm();
	},
	showDates:function(){
		
		var row = document.querySelectorAll(String.format(lustrumForm.parameters.formats.inputsFields,lustrumForm.parameters.internalNameFields.Modalidad)+":checked");
		if(row != null)
		{	
			if(row[0].value == lustrumForm.parameters.modalidad.disfrute)
			{
				var fechaInicio = document.querySelector(lustrumForm.parameters.fieldsHTML.FechaInicio);
				var fechaFin = document.querySelector(lustrumForm.parameters.fieldsHTML.FechaFin);
				if(fechaInicio != null && fechaFin != null)
				{
					fechaInicio.setAttribute(lustrumForm.parameters.hidden,false);
					fechaFin.setAttribute(lustrumForm.parameters.hidden,false);
				}
			}
		}
	}	
}

spForms.options={
	actions:[		
		{
			type:spForms.parameters.cancelAction,
			name:lustrumForm.parameters.formsParameters.buttons.Cancel.Text,
			id:lustrumForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:lustrumForm.parameters.formsParameters.title,
	description:lustrumForm.parameters.formsParameters.description,
	contentTypeName:lustrumForm.parameters.formsParameters.contentType,
	containerId:lustrumForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:false,
  	maxAttachments:5,
  	minAttachments:0,
  	maxSizeInMB:3,
	successLoadEditForm:function(){
		lustrumForm.showDates();
	} 
};
lustrumForm.initialize();