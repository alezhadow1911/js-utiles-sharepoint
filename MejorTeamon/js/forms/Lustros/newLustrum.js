﻿var lustrumForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx",
			inputsFields:"input[name='{0}']"
		},
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']",
			Modalidad:"[sp-static-name='Modalidad']",
			FechaInicio:"[sp-static-name='FechaInicioLustro']",
			FechaFin:"[sp-static-name='FechaFinLustro']"
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico',
			Modalidad:"Modalidad",
			FechaInicio:"FechaInicioLustro",
			FechaFin:"FechaFinLustro"	
		},
		resultItems:null,
		invalid:"sp-invalid",
		hidden:"sp-hidden",
		userProperties:"CellPhone", 
		errorInvalid:"El correo electrónico no tiene el formato correcto",
		loadUserInfo:"lustrumForm.loadUserInfo",
		formsIsLoaded:false,
		formsParameters:{
			title:"Solicitud Lustros",
			description:"Completa la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Lustros",
			containerId:"container-form"			
		},
		flowParameters:{
			urlFlow:"https://prod-62.westus.logic.azure.com:443/workflows/fc39cfd50202482880d2c014611bb05f/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=Nz2wek08CSeVW2xpMe1D9lCk1flDpl289H2wzNKoteU",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",
	   		approveBossUrl:"",
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Lustros Ventanilla TH",
	    	applyUsers:"Lustros Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:15,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Lustros"
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud Ventanilla TH",
			"AprobarJefe":"Aprobar Jefe Inmediato",
			"AplicarSolicitud":"Aplicar Novedad",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Novedades":"Vista Novedades",
			"Ventanilla":"Vista Ventanilla"
		},
		global:{
			createdBy:null,
			created:null
		},
		htmlRequiered:{position:"beforeend",html:"<i>*</i>",label:"label",block:"block",none:"none"},
		modalidad:{disfrute:"Disfrute",dinero:"En dinero"},
		requiered:"sp-requiered",
		staticName:"sp-static-name"
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
	    lustrumForm.parameters.flowParameters.approveUrl = String.format(lustrumForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(lustrumForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    lustrumForm.parameters.flowParameters.approveBossUrl = String.format(lustrumForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(lustrumForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    lustrumForm.parameters.flowParameters.applyUrl = String.format(lustrumForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(lustrumForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    lustrumForm.parameters.flowParameters.editUrl = String.format(lustrumForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(lustrumForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    lustrumForm.parameters.flowParameters.listViewApply = String.format(lustrumForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(lustrumForm.parameters.forms)[6]);
	    lustrumForm.parameters.flowParameters.listViewApprove= String.format(lustrumForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(lustrumForm.parameters.forms)[7]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":lustrumForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":lustrumForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosTH.value,
		        "comentariosJefe":spForms.loadedFields.ComentariosJefe.value,
		        "diasSolicitados":Math.round(spForms.loadedFields.DiasSolicitados.value)
		    },
		    "state":spForms.loadedFields.EstadoLustro.value,
		    "approveUsers":lustrumForm.parameters.flowParameters.approveUsers,
		    "applyUsers":lustrumForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":lustrumForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":lustrumForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":lustrumForm.parameters.flowParameters.listNameAssing,
		    "editUrl":lustrumForm.parameters.flowParameters.editUrl,
		    "approveUrl":lustrumForm.parameters.flowParameters.approveUrl,
		    "applyUrl":lustrumForm.parameters.flowParameters.applyUrl,
		    "approveBossUrl":lustrumForm.parameters.flowParameters.approveBossUrl,
		    "listViewApply":lustrumForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":lustrumForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	lustrumForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:lustrumForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){
		sputilities.callFunctionOnLoadBody(lustrumForm.parameters.loadUserInfo);
		spForms.loadNewForm();
	},
	onChangeValueModalidad:function(){
		var htmlInput = String.format(lustrumForm.parameters.formats.inputsFields,lustrumForm.parameters.internalNameFields.Modalidad);
		var inputs = document.querySelectorAll(htmlInput);
		if(inputs != null)
		{
			for(var i=0;i<inputs.length;i++)
			{
				var input = inputs[i];
				input.addEventListener('change',function(){
					var row = document.querySelectorAll(String.format(lustrumForm.parameters.formats.inputsFields,lustrumForm.parameters.internalNameFields.Modalidad)+":checked");
					if(row != null)
					{	
						if(row[0].value == lustrumForm.parameters.modalidad.dinero)
						{
							var fechaInicio = document.querySelector(lustrumForm.parameters.fieldsHTML.FechaInicio);
							var fechaFin = document.querySelector(lustrumForm.parameters.fieldsHTML.FechaFin);
							if(fechaInicio != null && fechaFin != null)
							{
								lustrumForm.showHiddenControls(fechaInicio,false);
								lustrumForm.showHiddenControls(fechaFin,false);								
							}							
						}
						
						if(row[0].value == lustrumForm.parameters.modalidad.disfrute)
						{
							var fechaInicio = document.querySelector(lustrumForm.parameters.fieldsHTML.FechaInicio);
							var fechaFin = document.querySelector(lustrumForm.parameters.fieldsHTML.FechaFin);
							if(fechaInicio != null && fechaFin != null)
							{
								lustrumForm.showHiddenControls(fechaInicio,true);
								lustrumForm.showHiddenControls(fechaFin,true);								
							}
						}
					}
				});
			}
		}
	},
	showHiddenControls:function(ctrl,isRequiered){
		
		if(!isRequiered)
		{
			ctrl.setAttribute(lustrumForm.parameters.hidden,true);
			var label = ctrl.querySelector(lustrumForm.parameters.htmlRequiered.label);
			if(label != null)
			{
				if(label.lastElementChild != null)
				{
					label.removeChild(label.lastElementChild);
				}
				ctrl.setAttribute(lustrumForm.parameters.requiered,isRequiered);
			}
		}
		else
		{
			ctrl.setAttribute(lustrumForm.parameters.hidden,false);
			ctrl.setAttribute(lustrumForm.parameters.requiered,isRequiered);
			var label = ctrl.querySelector(lustrumForm.parameters.htmlRequiered.label);
			if(label != null)
			{
				if(label.lastElementChild == null)
				{
					label.insertAdjacentHTML(lustrumForm.parameters.htmlRequiered.position,lustrumForm.parameters.htmlRequiered.html);
				}
			}
		}
	},
	loadUserInfo:function(){
		try {
			//running the query with parameters
			sputilities.getMyUserProfileProperties("",function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d;
				lustrumForm.parameters.resultItems = resultItems;
				var ctrlInterval = setInterval(function(){
					if(lustrumForm.parameters.formsIsLoaded)
					{
						lustrumForm.setValuesUserInfo();
						clearInterval(ctrlInterval);
					}
				},500);
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	setValuesUserInfo:function(){
	
		if(lustrumForm.parameters.resultItems != null)
		{
		  	var name = document.getElementById(lustrumForm.parameters.internalNameFields.NombrePersona);
		  	var email = document.getElementById(lustrumForm.parameters.internalNameFields.CorreoElectronico);
		  	var cellPhone = document.getElementById(lustrumForm.parameters.internalNameFields.NumeroCelular);
		  	if(name != null){
		  		name.value = lustrumForm.parameters.resultItems.DisplayName ? lustrumForm.parameters.resultItems.DisplayName: "";
		  	}
		  	
		  	if(email != null){
		  		email.value = lustrumForm.parameters.resultItems.Email ? (lustrumForm.parameters.resultItems.Email).toLowerCase(): "";
		  	}
		  	
		  	if(cellPhone != null){
		  	
		  		var userProperties = lustrumForm.parameters.resultItems.UserProfileProperties.results;
		  	
			  	for(var i=0;i<userProperties.length;i++)
			  	{
			  		if(userProperties[i].Key == lustrumForm.parameters.userProperties)
			  		{
			  		 	cellPhone.value =  userProperties[i].Value;
			  		 	break;
			  		}
			  	}
			}
	  	}
	},
	validateFormat:function(){
		var rowEmail = document.querySelector(lustrumForm.parameters.fieldsHTML.CorreoElectronico);
		var email = document.getElementById(lustrumForm.parameters.internalNameFields.CorreoElectronico);
		var isValid = true;
		if(lustrumForm.validateRequiered() && email != null && rowEmail!= null)
		{			
			if(lustrumForm.validateEmail(email.value) || email.value == "")
			{
				rowEmail.removeAttribute(lustrumForm.parameters.invalid);
			}
			else
			{
				rowEmail.setAttribute(lustrumForm.parameters.invalid,lustrumForm.parameters.errorInvalid);
			}
		}
		else
		{
			isValid=false;
		}
		return isValid;
		
	},
	validateRequiered:function()
	{
		var fields = document.querySelectorAll(lustrumForm.parameters.fieldsHTML.FechaInicio+","+lustrumForm.parameters.fieldsHTML.FechaFin);
		var isValid = true;	
		
		if(fields != null)
		{
			for(var i=0;i<fields.length;i++)
			{
				if(fields[i].getAttribute(lustrumForm.parameters.requiered) == "true")
				{
					spForms.loadedFields[fields[i].getAttribute(lustrumForm.parameters.staticName)].required =true;
				}
				else
				{
					spForms.loadedFields[fields[i].getAttribute(lustrumForm.parameters.staticName)].required =false;
					fields[i].removeAttribute(lustrumForm.parameters.invalid);
				}

			}
		}
		else
		{
			isValid=false;			
		}	
		
		return isValid;
		
	},
	validateEmail: function (email){
		 var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);
	
	}
}

spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:lustrumForm.parameters.formsParameters.buttons.Send.Text,
			id:lustrumForm.parameters.formsParameters.buttons.Send.Id,
			success:function (){
				lustrumForm.callFlowService();
				return false;
			},
			error:function (){},
			preSave: function (){
				return lustrumForm.validateFormat()
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:lustrumForm.parameters.formsParameters.buttons.Cancel.Text,
			id:lustrumForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:lustrumForm.parameters.formsParameters.title,
	description:lustrumForm.parameters.formsParameters.description,
	contentTypeName:lustrumForm.parameters.formsParameters.contentType,
	containerId:lustrumForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:false,
  	maxAttachments:5,
  	minAttachments:0,
  	maxSizeInMB:3,
	successLoadNewForm:function(){
		lustrumForm.parameters.global.createdBy = sputilities.contextInfo().userEmail;
		lustrumForm.parameters.global.created = moment.utc(new Date()).format(lustrumForm.parameters.formats.dateFormat);
		lustrumForm.parameters.formsIsLoaded = true;
		lustrumForm.onChangeValueModalidad();
	} 
};
lustrumForm.initialize();