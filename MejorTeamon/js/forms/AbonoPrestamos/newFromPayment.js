﻿var paymentFrom = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		fields:[{field:"NombrePersona1"},{field:"NumeroCelular"},{field:"CorreoElectronico"}],
		resultItems:null,
		requiredTag:"<i>*</i>",
		selectorHTML:"div[sp-static-name='{0}']",
		selecType:"select[id='TipoPrestamo']",
		messagesInvalid:{
		 CorreoElectronico:"El correo electrónico no tiene el formato correcto"
		},
		internalNameFields:{
			CorreoElectronico:"CorreoElectronico"
		},
		errorClass:"has-error",
		selectorError:"help-block-error-message-{0}",
		buttonsNames:{
			btnSend:"Enviar",
			btnCancel:"Cancelar"
		},
		buttonsId:{
			btnSend:"enviar",
			btnCancel:"cancelar"
		},
		labelsfielsHTML:{
			adjuntos:"[sp-static-name='sp-forms-attachment'] label",
		},
		descriptionForm:"Completa la información correspondiente",
		contentTypeNameForm:"Abono Prestamos",
		formsIsLoaded:false,
		flowIsExecute:false,
		containerIdForm:"spForm",
		loadUserInfo:"paymentFrom.loadUserInfo",
		regexEmail:"/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/",		
		flowParameters:{
			urlFlow:"https://prod-52.westus.logic.azure.com:443/workflows/53e3e8359b344974974ec11b92a92139/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=sHBKoLlJYRDYa-7opoxFRkj0Cncqj197XgfRGmE3ivw",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",	   		
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Abono Préstamos Ventanilla TH",
	    	applyUsers:"Abono Préstamos Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:1,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Abono a préstamo",
	    	tipoPrestamo:""
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud Ventanilla TH",			
			"AplicarSolicitud":"Aplicar Novedad",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Novedades":"Vista Novedades",
			"Ventanilla":"Vista Ventanilla"
		},
		global:{
			createdBy:null,
			created:null
		}

	},		
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
	    paymentFrom.parameters.flowParameters.approveUrl = String.format(paymentFrom.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(paymentFrom.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);			    
	    paymentFrom.parameters.flowParameters.applyUrl = String.format(paymentFrom.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(paymentFrom.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    paymentFrom.parameters.flowParameters.editUrl = String.format(paymentFrom.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(paymentFrom.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    paymentFrom.parameters.flowParameters.listViewApply = String.format(paymentFrom.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(paymentFrom.parameters.forms)[5]);
	    paymentFrom.parameters.flowParameters.listViewApprove= String.format(paymentFrom.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(paymentFrom.parameters.forms)[6]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":paymentFrom.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":paymentFrom.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosAbono.value,		        
		        "tipoPrestamo":paymentFrom.parameters.flowParameters.tipoPrestamo,
		        "montoPrestamo":Math.round(spForms.loadedFields.MontoAbono.value)
		    },
		    "state":spForms.loadedFields.EstadoSolicitudAbono.value,
		    "approveUsers":paymentFrom.parameters.flowParameters.approveUsers,
		    "applyUsers":paymentFrom.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":paymentFrom.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":paymentFrom.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":paymentFrom.parameters.flowParameters.listNameAssing,
		    "editUrl":paymentFrom.parameters.flowParameters.editUrl,
		    "approveUrl":paymentFrom.parameters.flowParameters.approveUrl,
		    "applyUrl":paymentFrom.parameters.flowParameters.applyUrl,		    
		    "listViewApply":paymentFrom.parameters.flowParameters.listViewApply,
		    "listViewApprove":paymentFrom.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	paymentFrom.parameters.flowParameters.subjectTitle
		}
		console.log(objService)
		utilities.http.callRestService({
			url:paymentFrom.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;
				spForms.cancelForm(null);
				return true;
			},
			error:function(xhr){
				console.log(xhr);
				return false;
			}
		});

	},
	initialize: function(){
		sputilities.callFunctionOnLoadBody(paymentFrom.parameters.loadUserInfo);
		spForms.loadNewForm();
	},	
	loadUserInfo:function(){
		try {
			//running the query with parameters
			sputilities.getMyUserProfileProperties("",function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d;
				paymentFrom.parameters.resultItems = resultItems;
				var ctrlInterval = setInterval(function(){
					if(paymentFrom.parameters.formsIsLoaded)
					{
						paymentFrom.setValuesUserInfo();
						clearInterval(ctrlInterval);
					}
				},500);

				
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	setValuesUserInfo:function(){
	
		var displayName = document.getElementById(paymentFrom.parameters.fields[0].field);
		var cellPhone = document.getElementById(paymentFrom.parameters.fields[1].field);
		var email = document.getElementById(paymentFrom.parameters.fields[2].field);
		if(displayName != null && cellPhone != null && email!= null)
		{
		  	displayName.value = paymentFrom.parameters.resultItems.DisplayName ? paymentFrom.parameters.resultItems.DisplayName: "";
		  	cellPhone.value = paymentFrom.parameters.resultItems.CellPhone ? paymentFrom.parameters.resultItems.CellPhone: "";
		  	email.value = paymentFrom.parameters.resultItems.Email ? (paymentFrom.parameters.resultItems.Email).toLowerCase(): "";
		  	displayName.disabled = true;
		  	
		  	document.querySelector(String.format(paymentFrom.parameters.selectorHTML,paymentFrom.parameters.internalNameFields.CorreoElectronico)).onchange = function(){
				var emailPerson = document.getElementById(paymentFrom.parameters.internalNameFields.CorreoElectronico).value;
					
				if(emailPerson != ""){
					if (!paymentFrom.validateEmail(emailPerson)){
					
						document.querySelector(String.format(paymentFrom.parameters.selectorHTML,paymentFrom.parameters.internalNameFields.CorreoElectronico)).classList.add(paymentFrom.parameters.errorClass);
						document.getElementById(String.format(paymentFrom.parameters.selectorError,paymentFrom.parameters.internalNameFields.CorreoElectronico)).textContent = paymentFrom.parameters.messagesInvalid.CorreoElectronico;
						
					}else{
					
						document.querySelector(String.format(paymentFrom.parameters.selectorHTML,paymentFrom.parameters.internalNameFields.CorreoElectronico)).classList.remove(paymentFrom.parameters.errorClass);
						document.getElementById(paymentFrom.parameters.selectorError,paymentFrom.parameters.internalNameFields.CorreoElectronico).textContent ="";
					}
				}
			}
		}
	},
	validateEmail: function (email){
		 var re = new RegExp(paymentFrom.parameters.regexEmail);
  		return re.test(email);
	},
	saveSelect:function(){
		var select = document.querySelector(paymentFrom.parameters.selecType);
		if(select != null)
		{
			paymentFrom.parameters.flowParameters.tipoPrestamo = select.querySelector('option:checked').text;
		}
	}
};
spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:paymentFrom.parameters.buttonsNames.btnSend,
			id:paymentFrom.parameters.buttonsId.btnSend,
			success:function (){
				paymentFrom.callFlowService();				
			},
			error:function (){},
			preSave: function (){
				paymentFrom.saveSelect();
				return true;
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:paymentFrom.parameters.buttonsNames.btnCancel,
			id:paymentFrom.parameters.buttonsId.btnCancel,
			callback:""
		},
	],
	description:paymentFrom.parameters.descriptionForm,
	contentTypeName:paymentFrom.parameters.contentTypeNameForm,
	containerId:paymentFrom.parameters.containerIdForm,
	allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:10,
  	minAttachments:1,
  	maxSizeInMB:3,
	successLoadNewForm:function(){
		paymentFrom.parameters.global.createdBy = sputilities.contextInfo().userEmail;
		paymentFrom.parameters.global.created = moment.utc(new Date()).format(paymentFrom.parameters.formats.dateFormat);
		paymentFrom.parameters.formsIsLoaded = true;
		document.querySelector(paymentFrom.parameters.labelsfielsHTML.adjuntos).innerHTML += paymentFrom.parameters.requiredTag;

	} 
};
paymentFrom.initialize();
