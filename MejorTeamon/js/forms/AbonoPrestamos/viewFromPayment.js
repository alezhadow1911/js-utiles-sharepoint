﻿var paymentForm = {
	parameters:{
		buttonsNames:{
			btnCancel:"Cancelar"
		},
		buttonsId:{
			btnCancel:"cancelar"
		},
		titleForm:"Solicitud Abono Prestamos",
		contentTypeNameForm:"Abono Prestamos",
		containerIdForm:"spForm"
	},
	initialize: function(){
		spForms.loadEditForm();
	}
};

spForms.options={
	actions:[		
		{
			type:spForms.parameters.cancelAction,
			name:paymentForm.parameters.buttonsNames.btnCancel,
			id:paymentForm.parameters.buttonsId.btnCancel,
			callback:""
		}
	],	
	title:paymentForm.parameters.titleForm,
	contentTypeName:paymentForm.parameters.contentTypeNameForm,
	containerId:paymentForm.parameters.containerIdForm,
	allowAttachments:true,
  	requiredAttachments:true,
 	maxAttachments:20,
  	minAttachments:1,
	successLoadEditForm:function(){
	} 
};
paymentForm.initialize();
