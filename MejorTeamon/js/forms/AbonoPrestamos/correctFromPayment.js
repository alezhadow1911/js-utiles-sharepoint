﻿var paymentForm = {
	parameters:{
		listName:"ResponsablesTH",
		query:"?$Select=EtapaResponsable,FlujoAsociado/Title,ResponsablesAbonos/EMail,ID&$expand=FlujoAsociado,ResponsablesAbonos&$filter=Title eq 'Abono Préstamos 	Abono Préstamos Novedades'", 
		resultItems:null,
		queryStringParameter:"Source",
		bodyItemPermissions:"Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior.",
		titlePopUp:"Abono a préstamos - Acceso denegado",
		selectorHTML:"div[sp-static-name='{0}']",
		messagesInvalid:{
		 CorreoElectronico:"El correo electrónico no tiene el formato correcto"
		},
		invalidClass:"sp-invalid",
		attachmentId:"sp-forms-add-attachment",
		colorDisabled:"#B1B1B1",
		borderDisabled:"#E1E1E1",
		display:"none",
		label:"label-{0}",
		button:"btn-{0}",
		requiredText:"<i>*</i>",
		btnClassDelete:".delete-doc",
		internalNameFields:{
			CorreoElectronico:"CorreoElectronico",
			EstadoSolicitudAbono:"EstadoSolicitudAbono",
			ComentariosAbono:"ComentariosAbono"
		},
		errorClass:"has-error",
		selectorError:"help-block-error-message-{0}",
		buttonsNames:{
			btnSend:"Enviar",
			btnCancel:"Cancelar"
		},
		buttonsId:{
			btnSend:"enviar",
			btnCancel:"cancelar"
		},
		descriptionForm:"Completa la información para corregir la solicitud",
		contentTypeNameForm:"Abono Prestamos",
		containerIdForm:"spForm",
		regexEmail:"/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/",
		formats:{
			htmlNotAllowed:"<div><h4>{0}</h4></div>"+
						   "<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		selectorFormContent:".form-content",
		flowParameters:{
			urlFlow:"https://prod-52.westus.logic.azure.com:443/workflows/53e3e8359b344974974ec11b92a92139/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=sHBKoLlJYRDYa-7opoxFRkj0Cncqj197XgfRGmE3ivw",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",	   		
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Abono Préstamos Ventanilla TH",
	    	applyUsers:"Abono Préstamos Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:1,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Abono a préstamo",
	    	tipoPrestamo:""
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud Ventanilla TH",			
			"AplicarSolicitud":"Aplicar Novedad",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Novedades":"Vista Novedades",
			"Ventanilla":"Vista Ventanilla"
		},
		global:{
			createdBy:null,
			created:null
		},		
		selecType:"select[id='TipoPrestamo']"

	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
	    paymentForm.parameters.flowParameters.approveUrl = String.format(paymentForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(paymentForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);			    
	    paymentForm.parameters.flowParameters.applyUrl = String.format(paymentForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(paymentForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    paymentForm.parameters.flowParameters.editUrl = String.format(paymentForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(paymentForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    paymentForm.parameters.flowParameters.listViewApply = String.format(paymentForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(paymentForm.parameters.forms)[5]);
	    paymentForm.parameters.flowParameters.listViewApprove= String.format(paymentForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(paymentForm.parameters.forms)[6]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":paymentForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":paymentForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosAbono.value,		        
		        "tipoPrestamo":paymentForm.parameters.flowParameters.tipoPrestamo,
		        "montoPrestamo":Math.round(spForms.loadedFields.MontoAbono.value)
		    },
		    "state":spForms.loadedFields.EstadoSolicitudAbono.value,
		    "approveUsers":paymentForm.parameters.flowParameters.approveUsers,
		    "applyUsers":paymentForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":paymentForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":paymentForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":paymentForm.parameters.flowParameters.listNameAssing,
		    "editUrl":paymentForm.parameters.flowParameters.editUrl,
		    "approveUrl":paymentForm.parameters.flowParameters.approveUrl,
		    "applyUrl":paymentForm.parameters.flowParameters.applyUrl,		    
		    "listViewApply":paymentForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":paymentForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	paymentForm.parameters.flowParameters.subjectTitle
		}
		console.log(objService)
		utilities.http.callRestService({
			url:paymentForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;
				spForms.cancelForm(null);
				return true;
			},
			error:function(xhr){
				console.log(xhr);
				return false;
			}
		});

	},
	initialize: function(){
		spForms.loadEditForm();
	},
	blockUsersForPermissions:function(){
		var currentUserId =_spPageContextInfo.userId;		
		
			var find = false;
			if(spForms.currentItem.AuthorId == currentUserId){
				find = true;
			}
							
			if(!find || document.getElementById(paymentForm.parameters.internalNameFields.EstadoSolicitudAbono).value != "4"){
				paymentForm.enableInputs();
			}
	},
	validatePreSaveFormEdit:function(){
		var ctrl = document.getElementById(paymentForm.parameters.internalNameFields.ComentariosAbono);
		if(ctrl.value.trim() == ''){
			document.querySelector(String.format(paymentForm.parameters.selectorHTML,paymentForm.parameters.internalNameFields.ComentariosAbono)).setAttribute(paymentForm.parameters.invalidClass,spForms.parameters.formats.requiredError);
		}
		else{
			document.querySelector(String.format(paymentForm.parameters.selectorHTML,paymentForm.parameters.internalNameFields.ComentariosAbono)).removeAttribute(paymentForm.parameters.invalidClass);
		}
	},
	enableInputs: function(){
		var html = String.format(paymentForm.parameters.formats.htmlNotAllowed,paymentForm.parameters.bodyItemPermissions);
		var formContent = document.querySelector(paymentForm.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}						
	},
	validateEmail: function (email){
		 var re = new RegExp(paymentForm.parameters.regexEmail);
  		return re.test(email);
	},
	onChangeEmail: function (){
		document.querySelector(String.format(paymentForm.parameters.selectorHTML,paymentForm.parameters.internalNameFields.CorreoElectronico)).onchange = function(){
			var emailPerson = document.getElementById(paymentForm.parameters.internalNameFields.CorreoElectronico).value;
				
			if(emailPerson != ""){
				if (!paymentForm.validateEmail(emailPerson)){
				
					document.querySelector(String.format(paymentForm.parameters.selectorHTML,paymentForm.parameters.internalNameFields.CorreoElectronico)).classList.add(paymentForm.parameters.errorClass);
					document.getElementById(String.format(paymentForm.parameters.selectorError,paymentForm.parameters.internalNameFields.CorreoElectronico)).textContent = paymentForm.parameters.messagesInvalid.CorreoElectronico;
					
				}else{
				
					document.querySelector(String.format(paymentForm.parameters.selectorHTML,paymentForm.parameters.internalNameFields.CorreoElectronico)).classList.remove(paymentForm.parameters.errorClass);
					document.getElementById(paymentForm.parameters.selectorError,paymentForm.parameters.internalNameFields.CorreoElectronico).textContent ="";
				}
			}
		}
	},
	saveSelect:function(){
		var select = document.querySelector(paymentForm.parameters.selecType);
		if(select != null)
		{
			paymentForm.parameters.flowParameters.tipoPrestamo = select.querySelector('option:checked').text;
		}
	}

};
spForms.options = {
	actions:[
		{
			type:spForms.parameters.updateAction,
			name:paymentForm.parameters.buttonsNames.btnSend,
			id:paymentForm.parameters.buttonsId.btnSend,
			preSave:function(){
				document.getElementById(paymentForm.parameters.internalNameFields.EstadoSolicitudAbono).value = "1";
				paymentForm.saveSelect();
				return true;
			},
			success:function(){paymentForm.callFlowService();},
			error:function(){return true}
		},
		{
			type:spForms.parameters.cancelAction,
			name:paymentForm.parameters.buttonsNames.btnCancel,
			id:paymentForm.parameters.buttonsId.btnCancel,
			callback:""
		},
	],
	description:paymentForm.parameters.descriptionForm,
	contentTypeName:paymentForm.parameters.contentTypeNameForm,
	containerId:paymentForm.parameters.containerIdForm,
	allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:5,
  	minAttachments:1,
  	maxSizeInMB:3,
	successLoadEditForm:function(){
		paymentForm.onChangeEmail();
		paymentForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
		paymentForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(paymentForm.parameters.formats.dateFormat);
		paymentForm.blockUsersForPermissions();
	}
};
paymentForm.initialize();
