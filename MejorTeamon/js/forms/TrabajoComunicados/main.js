﻿var ctrlForm = {
    parameters: {
        formats: {
            "queryStaticNameRow": "[sp-static-name='{0}']",
            "htmlNotAllowed": "<div><h4>{0}</h4></div> <div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
            "querySelectRadio": 'input[type="radio"][value="{0}"]:checked',
            "htmlTable": "<div id='header-contacts'><div><h3>Usuario Aprobador</h3></div><div><h3>Estado</h3></div><div><h3>Fecha</h3></div><div><h3>Otros</h3> </div></div>{0}",
            "HtmlUser": "<div class='data-row-contact'> <div><h3>Persona</h3>   </div>   <section>{0}</section>   <div>      <h3>Teléfonos</h3>   </div>   <section>{1}</section>   <div>      <h3>Celular</h3>   </div>   <section>{2}</section>   <div>      <h3>Email</h3>   </div>   <section>{3}</section></div>",
            "HtmlOneDrive": "<p>Haga clic <a href='{0}' target='_blank'>aquí</a> Para abrir el documento de one drive</p>",
            "queryRadio": 'input[type="radio"][value="{0}"]',
            "emailUsers": "{0};",
            "emailUsersRevisores": "{0};",
            "dateFormat": "DD-MM-YYYY",
            "dateFormatJson": "dd/MM/yyyy hh:mm tt",
            "formFormat": "{0}//{1}{2}/{3}.aspx?ID={4}&source={5}"
        },
        forms: {
            "NewForm": "ctrlForm.redirectCustomForm('NuevaSolicitud')",
            "EditForm": "ctrlForm.redirectCustomForm('CorregirSolicitud')",
            "DispForm": "ctrlForm.redirectCustomForm('VerSolicitud')",
            "NuevaSolicitud": "ctrlForm.loadNew()",
            "Notificacion": "ctrlForm.loadApprove()",
            "CorregirSolicitud": "ctrlForm.loadEditApplicant()",
            "RevisionAreas": "ctrlForm.loadRevisionArea()",
            "VerSolicitud": "ctrlForm.loadReview()",
            "PendientesAprobar": "",
            "Aprobadas": "",
            "SolicitudAprobada": "ctrlForm.loadRevision()",
            "Finalizado": "ctrlForm.loadFinalizado()",
        },
        messages: {
            "idSaveForm": "enviarSolicitud",
            "idCancelForm": "cancelar",
            "idEditForm": "enviarCorreccion",
            "idApproveForm": "aprobar",
            "idRejectForm": "solicitarCorreccion",
            "idApplyForm": "aplicarNovedad",
            "textSaveForm": "Enviar",
            "textCancelForm": "Cancelar",
            "textEditForm": "Enviar",
            "textApproveForm": "Aprobar",
            "textRejectForm": "Enviar a Corregir",
            "textApplyForm": "Novedad Aplicada",
            "textNotAllowed": "Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior",
            "textNotAllowedRevisores": "Usted no tiene permisos sobre este formulario o la solicitud ya fue aprobada por usted"
        },
        columns: {
            "comments": {
                "staticName": "ObservacionesComunicados"
            },
            "Onedrive": {
                "staticName": "UrlOnedrive"
            },

            "displayName": {
                "staticName": "NombrePersona1"
            },
            "idDoc": {
                "staticName": "NombrePersona1"
            },
            "cellPhone": {
                "staticName": "NumeroCelular"
            },
            "email": {
                "staticName": "CorreoElectronico"
            }
        },
        states: [
            "Solicitado",
            "Notificacion",
            "Corregir",
            "SolicitudAprobada",
            "RevisionAreas",
            "Finalizado",
        ],
        parametersHTML: {
            "spHidden": "sp-hidden",
            "spDisable": "sp-disable",
            div: "div",
            clase: "class",
            form: "form-group row",
            id: "id",
            idCapacitaciones: "Capacitaciones",
            formBody: "form-body form-horizontal",
            formHorizontal:".form-horizontal .form-header",
            formrow:"form-group row",
            formHeader:"form-header",
            textFicha:"Sección Ficha Tecnica",
            h2:"h2",
            formHeaderQuerySelector:".form-header",
            textMedios:"Sección Medios"
        },
        Functions: {
            Onchange: "onchange",
            functionOnchange: "javascript:ctrlForm.clickSelect(this);"
        },

        ID: {
            ContentClose: ".col-md-6.col-xs-6.delete-doc",
            ContentAdd: "a#sp-forms-add-attachment",
            Attachment: "div[sp-static-name='sp-forms-attachment']",
            Comments: "div[sp-static-name='ObservacionesComunicados']",
            Correo: "CorreoElectronico",
            TipoComunicacion: "TipoComunicacion",
            Medios: "[sp-static-name='Medios']",
            TipoComunicacionStatic: "[sp-static-name='TipoComunicacion'] label",
            FechaCierre: "[sp-static-name='FechaCierreRecoleccion']",
            FechaInicio: "[sp-static-name='FechaInicioRecoleccion']",
            Periodicidad: "[sp-static-name='Periodicidad']",
            TamanoPoblacion: "[sp-static-name='Tama_x00f1_oPoblacion']",
            Confianza: "[sp-static-name='NivelConfianza']",
            TamanoMuestra: "[sp-static-name='Tama_x00f1_oMuestra']",
            MargenError: "[sp-static-name='e']",
            Probabilidad: "[sp-static-name='P']",
            ProbabilidadCalculado: "[sp-static-name='q']",
            ValorCritico: "[sp-static-name='Z']",
            Estado: "[sp-static-name='EstadoComunicados']",
            Revision: "[sp-static-name='UsuariosRevisores']",
            FechaAprobadores: "[sp-static-name='FechaAprobacion']",
            Onedrive: "[sp-static-name='UrlOnedrive']",
            IdMedios: "Medios",
            IdFechaCierre: "FechaCierreRecoleccion",
            IdFechaInicio: "FechaInicioRecoleccion",
            IdPeriodicidad: "Periodicidad",
            IdTamanoPoblacion: "Tama_x00f1_oPoblacion",
            IdConfianza: "NivelConfianza",
            IdTamanoMuestra: "Tama_x00f1_oMuestra",
            IdMargenError: "e",
            IdProbabilidad: "P",
            IdProbabilidadCalculado: "q",
            IdValorCritico: "Z",
            IdEstado: "EstadoComunicados",
            IdRevision: "TieneRevision",
            IdNumeroAprobadores: "NumeroRevisoresAprobados",
            IdComentarios: "ObservacionesComunicados",
            IdCantidad:"CantidadRevisores",
            IdFechaAprobacion:"FechaAprobacion",
            FechaAprobacionValidation:"div[sp-static-name='FechaAprobacion']",
            IdRevisoresaprobados:"RevidoresAprobados",
            IdFechaSolicitud:"FechaSolicitud",
            FechaSolicitudValidation:"div[sp-static-name='FechaSolicitud']",
            IdFechaInicial:"FechaInicioRecoleccion",
            FechaInicialValidation:"div[sp-static-name='FechaInicioRecoleccion']",
 			IdFechaCierre:"FechaCierreRecoleccion",
            FechaCierreValidation:"div[sp-static-name='FechaCierreRecoleccion']"

        },
        style: {
            None: "none",
            Opacity: "0.7",
            Block: "block",
            //Atributtes
            Invalid: "sp-invalid",
            Message: "Formato de correo invalido",
            opacity: "0.7",
            MessageDate:"Fecha aprobacion menor a fecha actual",
            MessageDateSolicitud:"Fecha solicitud menor a fecha actual",
            MessageDateInicial:"Fecha inicio menor a hoy",
            MessageDateFinal:"Fecha final menor a fecha inicio"


        },
        selectorFormContent: ".form-content",
        cellPhone: "CellPhone",
        functionLoadMyProperties: "ctrlForm.loadMyProperties",
        containerId: "container-form",
        description: "Complete la información para la solicitud",
        descriptionEdit: "Corrija la información de la solicitud",
        descriptionApply: "Revise la información y aplique la novedad si corresponde",
        descriptionApprove: "Revise la información y apruebe o envíe a corrección según corresponda",
        descriptionFinalizado: "Finalizacion del comunicado",
        descriptionReview: "Resultado de la solicitud",
        contentTypeName: "TrabajoComunicados",
        formIsLoaded: false,
        allowAttachments: true,
        requiredAttachments: true,
        maxAttachments: 20,
        minAttachments: 0,
        maxSizeInMB: 3,
        listNameConsecutive: "ControlRadicados",
        idConsecutive: 1,
        listUsersFlow: "ResponsablesSEC",
        listParametros: "ParametrosSEC",
        ListName: sputilities.contextInfo().listTitle,
        viewUrl: "",
        editUrl: "",
        approveUrl: "",
        applyUrl: "",
        UrlNotificacion: "",
        UrlFinalizado: "",
        FechaAprobacion: "",
        Cantidad: "",
        RevisionInicial: true,
        Contador: "",
        ObjectPerson: "",
        successQuery: false,
        listViewApply: "",
        listViewApprove: "",
        queryList: "?$filter=ID eq {0}",
        queryUsersTH: "?$filter=ID eq 2&$select=ResponsablesAbonos/EMail&$expand=ResponsablesAbonos",
        queryUrlOnedrive: "?$filter=ID eq 1&$select=UrlArchivo",
        flowUrl: "https://prod-10.westus.logic.azure.com:443/workflows/871550eb18e843b2ab7e85ec0534deb4/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=Xb5IjNZdNZjNlDA4468BcTW5bdGlH62FT-Sl8l0JZSs"

    },
    global: {
        formName: null,
        createdBy: null,
        created: null,
        approveUsers: "",
        applyUsers: "",
        Revisores: "",
        IdUsers: ""

    },
    fieldType: {
        "0": {
            Actualizar: function() {
                ctrlForm.HideFieldsAll();
                ctrlForm.HideFields();
            }
        },
        "1": {
            Actualizar: function() {
                ctrlForm.HideFieldsAll();
                ctrlForm.HideFields();
            }
        },
        "2": {
            Actualizar: function() {
                ctrlForm.HideFieldsAll();
                ctrlForm.HideFields();
            }
        },
        "3": {
            Actualizar: function() {
                ctrlForm.HideFields();
                ctrlForm.HideFieldsSurvey();
                ctrlForm.PrintSectionMedios();
                spForms.loadedFields.Medios.required = true
            }
        },
        "4": {
            Actualizar: function() {
                ctrlForm.HideFields();
                ctrlForm.PrintSectionMedios();
                ctrlForm.PrintSectionFicha();
                ctrlForm.validateDateEncuestas();
                spForms.loadedFields.FechaCierreRecoleccion.required = true;
				spForms.loadedFields.FechaInicioRecoleccion.required = true;
				spForms.loadedFields.Periodicidad.required = true;
				spForms.loadedFields.Tama_x00f1_oPoblacion.required = true;
				spForms.loadedFields.Medios.required = true
				spForms.loadedFields.NivelConfianza.required = true;
				spForms.loadedFields.P.required = true;
				spForms.loadedFields.Z.required = true;
            }
        },
    },
    fieldState: {
        "Solicitado": {
            JsonAuditoriaApproved: function() {
                var state = spForms.loadedFields.EstadoComunicados.value;
                var nombre = _spPageContextInfo.userDisplayName;
                var IdUser = _spPageContextInfo.userId;
                var today = new Date();
                var Fecha = today.format(ctrlForm.parameters.formats.dateFormatJson);
                document.getElementById(ctrlForm.parameters.ID.IdComentarios).value = "Registro Inicial";
                var Comentarios = document.getElementById(ctrlForm.parameters.ID.IdComentarios).value;
                //save json
                var estado = spForms.loadedFields.EstadoComunicados.value;
                var JsonPersonas = {
                    "Personas": []
                }
                JsonPersonas.Personas.push({
                    'Aprobador': '' + nombre + '',
                    'Estado': '' + estado + '',
                    'fecha': '' + Fecha + '',
                    'Comentarios': '' + Comentarios + '',
                    'IdUser': '' + IdUser + ''
                });
                var jsonRevisoresComplete = JSON.stringify(JsonPersonas);
                document.getElementById(ctrlForm.parameters.ID.IdRevisoresaprobados).value = jsonRevisoresComplete;
            }
        },
        "Notificacion": {
            JsonAuditoriaApproved: function() {
                var state = spForms.loadedFields.EstadoComunicados.value;
                var nombre = _spPageContextInfo.userDisplayName;
                var today = new Date();
                var Fecha = today.format(ctrlForm.parameters.formats.dateFormatJson);
                var IdUser = _spPageContextInfo.userId;
                var Comentarios = document.getElementById(ctrlForm.parameters.ID.IdComentarios).value;
                var estado = "Aprobacion " + spForms.loadedFields.EstadoComunicados.value;
                var ObjectPerson = spForms.loadedFields.RevidoresAprobados.value;
                var JsonPersonas = JSON.parse(ObjectPerson)
                JsonPersonas.Personas.push({
                    'Aprobador': '' + nombre + '',
                    'Estado': '' + estado + '',
                    'fecha': '' + Fecha + '',
                    'Comentarios': '' + Comentarios + '',
                    'IdUser': '' + IdUser + ''
                });
                var jsonRevisoresComplete = JSON.stringify(JsonPersonas)
                document.getElementById(ctrlForm.parameters.ID.IdRevisoresaprobados).value = jsonRevisoresComplete
            }
        },
        "Corregir": {
            JsonAuditoriaApproved: function() {
                var state = spForms.loadedFields.EstadoComunicados.value;
                var nombre = _spPageContextInfo.userDisplayName;
                var today = new Date();
                var Fecha = today.format(ctrlForm.parameters.formats.dateFormatJson);
                var IdUser = _spPageContextInfo.userId;
                var Comentarios = document.getElementById(ctrlForm.parameters.ID.IdComentarios).value;
                var estado = "Correcion Realizada";
                var ObjectPerson = spForms.loadedFields.RevidoresAprobados.value;
                var JsonPersonas = JSON.parse(ObjectPerson)
                JsonPersonas.Personas.push({
                    'Aprobador': '' + nombre + '',
                    'Estado': '' + estado + '',
                    'fecha': '' + Fecha + '',
                    'Comentarios': '' + Comentarios + '',
                    'IdUser': '' + IdUser + ''
                });
                var jsonRevisoresComplete = JSON.stringify(JsonPersonas)
                document.getElementById(ctrlForm.parameters.ID.IdRevisoresaprobados).value = jsonRevisoresComplete
            }
        },
        "SolicitudAprobada": {
            JsonAuditoriaApproved: function() {
                var state = spForms.loadedFields.EstadoComunicados.value;
                var nombre = _spPageContextInfo.userDisplayName;
                var today = new Date();
                var Fecha = today.format(ctrlForm.parameters.formats.dateFormatJson);
                var IdUser = _spPageContextInfo.userId;
                var Comentarios = document.getElementById(ctrlForm.parameters.ID.IdComentarios).value;
                var estado = "Aprobación Revisión ";
                var ObjectPerson = spForms.loadedFields.RevidoresAprobados.value;
                var JsonPersonas = JSON.parse(ObjectPerson)
                JsonPersonas.Personas.push({
                    'Aprobador': '' + nombre + '',
                    'Estado': '' + estado + '',
                    'fecha': '' + Fecha + '',
                    'Comentarios': '' + Comentarios + '',
                    'IdUser': '' + IdUser + ''
                });
                var jsonRevisoresComplete = JSON.stringify(JsonPersonas)
                document.getElementById(ctrlForm.parameters.ID.IdRevisoresaprobados).value = jsonRevisoresComplete
            }
        },
    },

    fieldStateReject: {
        "Notificacion": {
            JsonAuditoriaReject: function() {
                var state = spForms.loadedFields.EstadoComunicados.value;
                var nombre = _spPageContextInfo.userDisplayName;
                var today = new Date();
                var Fecha = today.format(ctrlForm.parameters.formats.dateFormatJson);
                var IdUser = _spPageContextInfo.userId;
                var Comentarios = document.getElementById(ctrlForm.parameters.ID.IdComentarios).value;
                var estado = "Rechazo " + spForms.loadedFields.EstadoComunicados.value;
                var ObjectPerson = spForms.loadedFields.ControlInternoAuditoria.value;
                var JsonPersonas = JSON.parse(ObjectPerson)
                JsonPersonas.Personas.push({
                    'Aprobador': '' + nombre + '',
                    'Estado': '' + estado + '',
                    'fecha': '' + Fecha + '',
                    'Comentarios': '' + Comentarios + '',
                    'IdUser': '' + IdUser + ''
                });
                var jsonRevisoresComplete = JSON.stringify(JsonPersonas)
                document.getElementById(ctrlForm.parameters.ID.IdRevisoresaprobados).value = jsonRevisoresComplete
            }
        },
        "SolicitudAprobada": {
            JsonAuditoriaReject: function() {
                var state = spForms.loadedFields.EstadoComunicados.value;
                var nombre = _spPageContextInfo.userDisplayName;
                var today = new Date();
                var Fecha = today.format(ctrlForm.parameters.formats.dateFormatJson);
                var IdUser = _spPageContextInfo.userId;
                var Comentarios = document.getElementById(ctrlForm.parameters.ID.IdComentarios).value;
                var estado = "Rechazo " + spForms.loadedFields.EstadoComunicados.value;
                var ObjectPerson = spForms.loadedFields.RevidoresAprobados.value;
                var JsonPersonas = JSON.parse(ObjectPerson)
                JsonPersonas.Personas.push({
                    'Aprobador': '' + nombre + '',
                    'Estado': '' + estado + '',
                    'fecha': '' + Fecha + '',
                    'Comentarios': '' + Comentarios + '',
                    'IdUser': '' + IdUser + ''
                });
                var jsonRevisoresComplete = JSON.stringify(JsonPersonas)
                document.getElementById(ctrlForm.parameters.ID.IdRevisoresaprobados).value = jsonRevisoresComplete
            }
        },
     },


    initialize: function() {
        var partsOfUrl = window.location.href.split('?')[0].split('/');
        ctrlForm.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];
        eval(ctrlForm.parameters.forms[ctrlForm.global.formName]);
    },
    callFlowService: function() {
        document.getElementById('s4-workspace').scrollTop = 0;
        document.querySelector('.form-content').style.opacity = 0;
        document.querySelector('.loader-container').style.opacity = 1;
        document.querySelector('.loader-container').style.height = "auto";


        ctrlForm.parameters.viewUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[7], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.editUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[5], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.approveUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[4], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.applyUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[6], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.UrlNotificacion = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[10], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.UrlFinalizado = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[11], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.listViewApply = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[9]);
        ctrlForm.parameters.listViewApprove = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[8]);

        var objService = {
            "currentItemID": spForms.currentItemID,
            "currentItem": {
                "createdBy": ctrlForm.global.createdBy,
                "nombres":spForms.loadedFields.NombrePersona1.value,
                "area": spForms.loadedFields.AreaSolicitante.value,
                "tipo": spForms.loadedFields.TipoComunicacion.value,
                "consecutivo": spForms.loadedFields.NumeroRadicado.value,
                "NombrePublicacion": spForms.loadedFields.NombrePublicacion.value,
                "comentarios": spForms.loadedFields.ObservacionesComunicados.value,
                "fechaCreacion": ctrlForm.global.created,
            },
            "state": spForms.loadedFields.EstadoComunicados.value,
            "titleCorreo": spForms.listInfo.title,
            "approveUsers": ctrlForm.global.approveUsers,
            "applyUsers": ctrlForm.global.applyUsers,
            "applyUsersRevisores": ctrlForm.global.Revisores,
            "siteUrl": sputilities.contextInfo().webAbsoluteUrl,
            "listName": sputilities.contextInfo().listTitle,
            "listNameConsecutive": ctrlForm.parameters.listNameConsecutive,
            "idConsecutive": ctrlForm.parameters.idConsecutive,
            "entityType": spForms.listInfo.entityItem,
            "viewUrl": ctrlForm.parameters.viewUrl,
            "editUrl": ctrlForm.parameters.editUrl,
            "approveUrl": ctrlForm.parameters.approveUrl,
            "applyUrl": ctrlForm.parameters.applyUrl,
            "UrlNotificacion": ctrlForm.parameters.UrlNotificacion,
            "UrlFinalizado": ctrlForm.parameters.UrlFinalizado,
            "FechaAprobacion": ctrlForm.parameters.FechaAprobacion,
            "Cantidad": ctrlForm.parameters.Cantidad,
            "EmailInicialRevision": ctrlForm.parameters.RevisionInicial,
            "Contador": ctrlForm.parameters.Contador,
            "JsonPerson": ctrlForm.parameters.ObjectPerson,
            "listViewApply": ctrlForm.parameters.listViewApply,
            "listViewApprove": ctrlForm.parameters.listViewApprove
        }
        utilities.http.callRestService({
            url: ctrlForm.parameters.flowUrl,
            method: utilities.http.methods.POST,
            data: JSON.stringify(objService),
            headers: {
                "accept": utilities.http.types.jsonOData,
                "content-type": utilities.http.types.jsonOData
            },
            success: function(data) {
                document.querySelector('.loader-container').style.opacity = 0;
                document.querySelector('.form-content').style.opacity = 1;
                document.querySelector('.loader-container').style.height = 0;

                spForms.cancelForm(null);
            },
            error: function(xhr) {
                console.log(xhr);
            }
        });

    },
    getUsersTH: function() {
        sputilities.getItems(ctrlForm.parameters.listUsersFlow, ctrlForm.parameters.queryUsersTH,
            function(data) {
                var data = utilities.convertToJSON(data);
                if (data != null) {
                    var items = data.d.results;
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        for (var j = 0; j < item.ResponsablesAbonos.results.length; j++) {
                            var user = item.ResponsablesAbonos.results[j];
                            ctrlForm.global.approveUsers += String.format(ctrlForm.parameters.formats.emailUsers, user.EMail);
                        }
                    }
                }
            },
            function(xhr) {
                console.log(xhr);
            }
        );
    },
    getUrlOneDrieve: function() {
        sputilities.getItems(ctrlForm.parameters.listParametros, ctrlForm.parameters.queryUrlOnedrive,
            function(data) {
                var data = utilities.convertToJSON(data);
                if (data != null) {
                    var items = data.d.results;
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i].UrlArchivo;
                        var html = String.format(ctrlForm.parameters.formats.HtmlOneDrive, item)
                        document.querySelector('[sp-static-name="UrlOnedrive"] .form-control-help').innerHTML = html;
                    }
                }
            },
            function(xhr) {
                console.log(xhr);
            }
        );
    },
    changeState: function(state) {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio, state));
        if (radio != null) {
            radio.click();
        }
    },
    hideNotAllowed: function() {
        var html = String.format(ctrlForm.parameters.formats.htmlNotAllowed, ctrlForm.parameters.messages.textNotAllowed);
        var formContent = document.querySelector(ctrlForm.parameters.selectorFormContent);
        if (formContent != null) {
            formContent.innerHTML = html;
        }
    },
        hideNotAllowedRevisores: function() {
        var html = String.format(ctrlForm.parameters.formats.htmlNotAllowed, ctrlForm.parameters.messages.textNotAllowedRevisores);
        var formContent = document.querySelector(ctrlForm.parameters.selectorFormContent);
        if (formContent != null) {
            formContent.innerHTML = html;
        }
    },

    validateUserCreated: function() {
        var usuarios = setTimeout(function() {
            if (ctrlForm.global.approveUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1) {
                ctrlForm.hideNotAllowed();
                return false;
            }
            ctrlForm.hideAttachments();
        }, 7000);
    },
    validateAllUsers: function() {
        if (spForms.currentItem.AuthorId != sputilities.contextInfo().userId || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
    },
    validateEditUser: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[2])) == null;
        if (spForms.currentItem.AuthorId != sputilities.contextInfo().userId || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        return true;
    },
    //Novedades
    validateApplyUser: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[2])) == null;
        if (ctrlForm.global.applyUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        ctrlForm.hideAttachments();
    },
    //Notificacion Inicial
    validateApproveUser: function() {
    var usuarios = setTimeout(function() {    
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[1])) == null;
        if (ctrlForm.global.approveUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        //ctrlForm.hideAttachments();
          }, 9000);
    },
    //Notificacion para revision
    validateUserRevision: function() {
    var usuarios = setTimeout(function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[3])) == null;
       		if (ctrlForm.global.approveUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        }, 7000);
    },

    validateUserRevisionArea: function() {
    	
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[4])) == null;
        if (ctrlForm.global.Revisores.split(';').indexOf(sputilities.contextInfo().userEmail.toLowerCase()) == -1 || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        //ctrlForm.hideAttachments();
    },
    validateUserRevisionAprobadores: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[4])) == null;
        if (ctrlForm.global.IdUsers.split(';').indexOf(sputilities.contextInfo().userId.toString()) == -1 || radio) {
        } else {
            ctrlForm.hideNotAllowedRevisores();
        }
        //ctrlForm.hideAttachments();
    },
    validateUserFinalizacion: function() {
        var usuarios = setTimeout(function() {
            var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[5])) == null;
            if (ctrlForm.global.approveUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio) {
                ctrlForm.hideNotAllowed();
                return false;
            }
            ctrlForm.hideAttachments();

        }, 7000);
    },

    hideAttachments: function() {
        var i;
        var CloaseAttachments = document.querySelectorAll(ctrlForm.parameters.ID.ContentClose);
        for (i = 0; i < CloaseAttachments.length; i++) {
            CloaseAttachments[i].style.display = ctrlForm.parameters.style.None;
        }
        document.querySelector(ctrlForm.parameters.ID.ContentAdd).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.Attachment).style.opacity = ctrlForm.parameters.style.Opacity;
    },

    hideComments: function() {
        var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow, ctrlForm.parameters.columns.comments.staticName));
        if (ctrlComments != null) {
            ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden, true);
        }
    },
    blockedComments: function() {
        var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow, ctrlForm.parameters.columns.comments.staticName));
        if (ctrlComments != null) {
            ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden, true);
        }
    },

    loadReview: function() {
        var actions = [{
            type: spForms.parameters.cancelAction,
            name: ctrlForm.parameters.messages.textCancelForm,
            id: ctrlForm.parameters.messages.idCancelForm,
            callback: null
        }, ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionReview;
        ctrlForm.loadEdit(actions, null);
    },
    loadApply: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textApplyForm,
                id: ctrlForm.parameters.messages.idApplyForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.changeState(ctrlForm.parameters.states[3]);
                    return true;
                }
            },
            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionApply;
        ctrlForm.loadEdit(actions, ctrlForm.validateApplyUser);

    },
    loadApprove: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textApproveForm,
                id: ctrlForm.parameters.messages.idApproveForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.UnlockedComments();
                    ctrlForm.changeState(ctrlForm.parameters.states[3]);
                    ctrlForm.JsonRevisoresApprove();
                    return true;
                }
            },
            {
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textRejectForm,
                id: ctrlForm.parameters.messages.idRejectForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
                    ctrlForm.Validatecomments();
                    ctrlForm.JsonRevisoresReject();
                   
                    
                    return true;
                }
            },
            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
        ctrlForm.loadEdit(actions, null);

    },
    loadRevision: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textEditForm,
                id: ctrlForm.parameters.messages.idApproveForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;

                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.EmailRevisores();
                    ctrlForm.JsonRevisoresApprove();
                    ctrlForm.VerificarRevision();
                    return true;
                }
            },
            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
        ctrlForm.loadEdit(actions, ctrlForm.validateUserRevision);

    },
    loadRevisionArea: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textApproveForm,
                id: ctrlForm.parameters.messages.idApproveForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.ActualizarAprobadores();
                    return true;
                }
            },
            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
        ctrlForm.loadEdit(actions, null);

    },

    loadFinalizado: function() {
        var actions = [{
            type: spForms.parameters.cancelAction,
            name: ctrlForm.parameters.messages.textCancelForm,
            id: ctrlForm.parameters.messages.idCancelForm,
            callback: null
        }, ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionFinalizado;
        ctrlForm.loadEdit(actions, null);

    },


    loadEditApplicant: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textEditForm,
                id: ctrlForm.parameters.messages.idEditForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.JsonRevisoresApprove();
                    ctrlForm.changeState(ctrlForm.parameters.states[1]);
                    return true;
                }
            },
            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionEdit;
        ctrlForm.loadEdit(actions, function() {
            if (ctrlForm.validateEditUser()) {
                ctrlForm.hideComments();
                var ctrlComments = document.getElementById(ctrlForm.parameters.columns.comments.staticName);
                if (ctrlComments != null) {
                    ctrlComments.value = "";
                }
            }
        });
    },
    loadEdit: function(actions, callBack) {
        ctrlForm.getUsersTH();
        spForms.options = {
            actions: actions,
            description: ctrlForm.parameters.description,
            contentTypeName: ctrlForm.parameters.contentTypeName,
            containerId: ctrlForm.parameters.containerId,
            allowAttachments: ctrlForm.parameters.allowAttachments,
            requiredAttachments: ctrlForm.parameters.requiredAttachments,
            maxAttachments: ctrlForm.parameters.maxAttachments,
            minAttachments: ctrlForm.parameters.minAttachments,
            maxSizeInMB: ctrlForm.parameters.maxSizeInMB,
            successLoadEditForm: function() {
                ctrlForm.global.createdBy = spForms.currentItem.Author.EMail;
                ctrlForm.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(ctrlForm.parameters.formats.dateFormat);
                ctrlForm.formIsLoaded = true;
                ctrlForm.clickSelect();
                ctrlForm.RevisionAction();
                if (ctrlForm.global.formName == "CorregirSolicitud") {
                    ctrlForm.IniForm();
                }
                if (ctrlForm.global.formName == "Finalizado") {
                    ctrlForm.validateUserFinalizacion();
                    ctrlForm.getUrlOneDrieve();
                }
                if (ctrlForm.global.formName == "VerSolicitud") {
                    ctrlForm.commentsEmpty();
                    ctrlForm.Auditoria();
                    ctrlForm.validateUserCreated();
                }
                if (ctrlForm.global.formName == "Notificacion") {
                    ctrlForm.commentsEmpty();
                     ctrlForm.validateApproveUser();
                }
                if (ctrlForm.global.formName == "SolicitudAprobada") {
                    ctrlForm.commentsEmpty();
                    ctrlForm.validateDateInicial();

                }
                if (ctrlForm.global.formName == "RevisionAreas") {
                    ctrlForm.GetRevisores();
                    ctrlForm.commentsEmpty();
                }
                
                if (typeof callBack == "function") {
                    callBack();
                }
            }
        };
        spForms.loadEditForm();

    },
    loadNew: function() {
        ctrlForm.getUsersTH();
        ctrlForm.global.createdBy = sputilities.contextInfo().userEmail;
        ctrlForm.global.created = moment.utc(new Date()).format(ctrlForm.parameters.formats.dateFormat);
        sputilities.callFunctionOnLoadBody(ctrlForm.parameters.functionLoadMyProperties);
        ctrlForm.loadNewForm();
    },
    loadNewForm: function() {
        spForms.options = {
            actions: [{
                    type: spForms.parameters.saveAction,
                    name: ctrlForm.parameters.messages.textSaveForm,
                    id: ctrlForm.parameters.messages.idSaveForm,
                    success: function() {
                        ctrlForm.callFlowService();

                        return false;
                    },
                    error: function() {
                        return true;
                    },
                    preSave: function() {
                        ctrlForm.JsonRevisoresApprove();
                        ctrlForm.changeState(ctrlForm.parameters.states[0]);
                        return true;
                    }
                },
                {
                    type: spForms.parameters.cancelAction,
                    name: ctrlForm.parameters.messages.textCancelForm,
                    id: ctrlForm.parameters.messages.idCancelForm,
                    callback: null
                },
            ],
            description: ctrlForm.parameters.description,
            contentTypeName: ctrlForm.parameters.contentTypeName,
            containerId: ctrlForm.parameters.containerId,
            allowAttachments: ctrlForm.parameters.allowAttachments,
            requiredAttachments: ctrlForm.parameters.requiredAttachments,
            maxAttachments: ctrlForm.parameters.maxAttachments,
            minAttachments: ctrlForm.parameters.minAttachments,
            maxSizeInMB: ctrlForm.parameters.maxSizeInMB,
            successLoadNewForm: function() {
                ctrlForm.formIsLoaded = true;
                ctrlForm.hideComments();
                ctrlForm.IniForm();
                ctrlForm.stateChange();
                ctrlForm.validateDateNueva();
                ctrlForm.fieldsRequired();
            }
        };
        spForms.loadNewForm();
    },
    loadMyProperties: function() {
        sputilities.getUserProfileProperties(sputilities.contextInfo().userEmail,
            function(userInfo) {
                var objUser = utilities.convertToJSON(userInfo);
                if (objUser != null) {
                    var displayName = objUser.d.DisplayName;
                    var validateFormIsLoaded = setInterval(function() {
                        if (ctrlForm.formIsLoaded) {
                            ctrlForm.setValueToControl(ctrlForm.parameters.columns.displayName.staticName, displayName);
                            clearInterval(validateFormIsLoaded);
                        }
                    }, 500);
                }
            },
            function(xhr) {
                console.log(xhr)
            }
        );
    },
    EmailRevisores: function() {
        var Fecha = document.getElementById(ctrlForm.parameters.ID.IdFechaAprobacion).value
        ctrlForm.parameters.FechaAprobacion = Fecha;
        var uno = SPClientPeoplePicker.SPClientPeoplePickerDict.UsuariosRevisores_TopSpan.GetAllUserInfo()
        var dos = uno.length
        ctrlForm.parameters.Cantidad = dos;
        document.getElementById(ctrlForm.parameters.ID.IdCantidad).value = dos;

        var Items = SPClientPeoplePicker.SPClientPeoplePickerDict.UsuariosRevisores_TopSpan.GetAllUserInfo();
        var i = 0;
        for (i; i < Items.length; i++) {
            var Email = Items[i].EntityData.Email;
            ctrlForm.global.Revisores += String.format(ctrlForm.parameters.formats.emailUsersRevisores, Email);
        }
    },
    GetRevisores: function() {
        var usuarios = setInterval(function() {
            var uno = SPClientPeoplePicker.SPClientPeoplePickerDict.UsuariosRevisores_TopSpan.GetAllUserInfo()
            if (uno.length > 0) {
                clearInterval(usuarios)
                var dos = uno.length
                ctrlForm.parameters.Cantidad = dos;
                var Items = SPClientPeoplePicker.SPClientPeoplePickerDict.UsuariosRevisores_TopSpan.GetAllUserInfo();
                var i = 0;
                for (i; i < Items.length; i++) {
                    var Email = Items[i].Key.split("|")[2];
                    ctrlForm.global.Revisores += String.format(ctrlForm.parameters.formats.emailUsersRevisores, Email);
                }
                ctrlForm.validateUserRevisionArea();
                ctrlForm.GetAprobadores();
            }

        }, 2000);
    },
	GetAprobadores: function() {
        var ObjectPerson = spForms.loadedFields.RevidoresAprobados.value;
        var JsonPersonas = JSON.parse(ObjectPerson)
        var i = 0;
        var RevisoresFilter = JsonPersonas.Personas.filter(function(entry) {
            return entry.Estado === 'Aprobación revisión área';
        });
        for (i; i < RevisoresFilter.length; i++) {
            var item = RevisoresFilter[i].IdUser;
            ctrlForm.global.IdUsers += String.format(ctrlForm.parameters.formats.emailUsersRevisores, item);
        }
        ctrlForm.validateUserRevisionAprobadores();
    },    setValueToControl: function(staticName, value) {
        if (value != null) {
            var ctrl = document.getElementById(staticName);
            if (ctrl != null) {
                ctrl.value = value;
            }
        }
    },
    //Solo para información de interés y medios de promoción de encuestas
    PrintSectionMedios: function() {
        var uno = document.querySelectorAll(ctrlForm.parameters.parametersHTML.formHeaderQuerySelector)
        var SectionTwoPanel = document.getElementsByClassName(ctrlForm.parameters.parametersHTML.formrow)[7];
        var DivTitle = document.createElement(ctrlForm.parameters.parametersHTML.div);
        DivTitle.setAttribute(ctrlForm.parameters.parametersHTML.clase,ctrlForm.parameters.parametersHTML.formHeader);
        var HTitle = document.createElement(ctrlForm.parameters.parametersHTML.h2);
        HTitle.textContent = ctrlForm.parameters.parametersHTML.textMedios;
        DivTitle.appendChild(HTitle);
        SectionTwoPanel.insertBefore(DivTitle, SectionTwoPanel.childNodes[0])
        document.querySelector(ctrlForm.parameters.ID.Medios).style.display = ctrlForm.parameters.style.Block;
    },
    //Solo aplica para encuestas
    PrintSectionFicha: function() {
        var SectionTwoPanel = document.getElementsByClassName(ctrlForm.parameters.parametersHTML.formrow)[8];
        var DivTitle = document.createElement(ctrlForm.parameters.parametersHTML.div);
        DivTitle.setAttribute(ctrlForm.parameters.parametersHTML.clase,ctrlForm.parameters.parametersHTML.formHeader);
        var HTitle = document.createElement(ctrlForm.parameters.parametersHTML.h2);
        HTitle.textContent = ctrlForm.parameters.parametersHTML.textFicha;
        DivTitle.appendChild(HTitle);
        SectionTwoPanel.insertBefore(DivTitle, SectionTwoPanel.childNodes[0]);
        document.querySelector(ctrlForm.parameters.ID.FechaCierre).style.display = ctrlForm.parameters.style.Block;
        document.querySelector(ctrlForm.parameters.ID.FechaInicio).style.display = ctrlForm.parameters.style.Block;
        document.querySelector(ctrlForm.parameters.ID.Periodicidad).style.display = ctrlForm.parameters.style.Block;
        document.querySelector(ctrlForm.parameters.ID.TamanoPoblacion).style.display = ctrlForm.parameters.style.Block;
        document.querySelector(ctrlForm.parameters.ID.Confianza).style.display = ctrlForm.parameters.style.Block;
        document.querySelector(ctrlForm.parameters.ID.TamanoMuestra).style.display = ctrlForm.parameters.style.Block;
        document.querySelector(ctrlForm.parameters.ID.TamanoMuestra).style.pointerEvents = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.TamanoMuestra).style.opacity = ctrlForm.parameters.style.opacity;
        document.querySelector(ctrlForm.parameters.ID.MargenError).style.display = ctrlForm.parameters.style.Block;
        document.querySelector(ctrlForm.parameters.ID.MargenError).style.pointerEvents = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.MargenError).style.opacity = ctrlForm.parameters.style.opacity;
        document.querySelector(ctrlForm.parameters.ID.Probabilidad).style.display = ctrlForm.parameters.style.Block;
        document.querySelector(ctrlForm.parameters.ID.ProbabilidadCalculado).style.display = ctrlForm.parameters.style.Block;
        document.querySelector(ctrlForm.parameters.ID.ProbabilidadCalculado).style.pointerEvents = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.ProbabilidadCalculado).style.opacity = ctrlForm.parameters.style.opacity;
        document.querySelector(ctrlForm.parameters.ID.ValorCritico).style.display = ctrlForm.parameters.style.Block;
    },
    clickSelect: function(ctrl) {
        var CurrentType = document.getElementById(ctrlForm.parameters.ID.TipoComunicacion).value;
        var spFormField = ctrlForm.fieldType[CurrentType].Actualizar();

    },
    IniForm: function() {
        ctrlForm.validateTotal();
        var OnclickNametype = document.getElementById(ctrlForm.parameters.ID.TipoComunicacion);
        OnclickNametype.setAttribute(ctrlForm.parameters.Functions.Onchange, ctrlForm.parameters.Functions.functionOnchange);
    },
    HideFieldsAll: function() {
        document.querySelector(ctrlForm.parameters.ID.Medios).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.FechaCierre).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.FechaInicio).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.Periodicidad).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.TamanoPoblacion).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.Confianza).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.TamanoMuestra).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.MargenError).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.Probabilidad).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.ProbabilidadCalculado).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.ValorCritico).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.Estado).style.display = ctrlForm.parameters.style.None;
    },
    HideFieldsSurvey: function() {
        document.querySelector(ctrlForm.parameters.ID.FechaCierre).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.FechaInicio).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.Periodicidad).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.TamanoPoblacion).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.Confianza).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.TamanoMuestra).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.MargenError).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.Probabilidad).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.ProbabilidadCalculado).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.ValorCritico).style.display = ctrlForm.parameters.style.None;
    },
    HideFields: function() {
        var uno = document.querySelectorAll(ctrlForm.parameters.parametersHTML.formHorizontal)
        for (var i = 0; i < uno.length; i++) {
            var dos = uno[i].remove();
        }
    },
    validateTotal: function() {
        var TamanoPEncuesta = document.getElementById(ctrlForm.parameters.ID.IdTamanoPoblacion);
        var ValorEncuesta = document.getElementById(ctrlForm.parameters.ID.IdValorCritico); //z
        var Confianza = document.getElementById(ctrlForm.parameters.ID.IdConfianza);
        var ProbabilidadEncuesta = document.getElementById(ctrlForm.parameters.ID.IdProbabilidad); //p
        var ProbabilidadC = document.getElementById(ctrlForm.parameters.ID.IdProbabilidadCalculado); //q
        var MargenError = document.getElementById(ctrlForm.parameters.ID.IdMargenError) //e
        Confianza.value = 0;
        TamanoPEncuesta.value = 0;
        ValorEncuesta.value = 0;
        ProbabilidadEncuesta.value = 0;
        ProbabilidadC.value = 0;
        MargenError.value = 0;
        if (Confianza != null) {
            Confianza.addEventListener('change', function(event) {
                MargenError.value = 100 - parseInt(Confianza.value);
            });
            if (ProbabilidadEncuesta != null) {
                ProbabilidadEncuesta.addEventListener('change', function(event) {
                    ProbabilidadC.value = 100 - parseInt(ProbabilidadEncuesta.value);
                });
                if (ValorEncuesta != null) {
                    ValorEncuesta.addEventListener('change', function(event) {
                        var Critico = parseInt(ValorEncuesta.value);
                        var elevado = Math.pow(Critico, 2);
                        var p = parseInt(ProbabilidadEncuesta.value) / 100;
                        var q = parseInt(ProbabilidadC.value) / 100;
                        var Margen = parseInt(MargenError.value) / 100;
                        var e = Math.pow(Margen, 2);
                        document.getElementById(ctrlForm.parameters.ID.IdTamanoMuestra).value = elevado * parseInt(TamanoPEncuesta.value) * p * q / ((e * (parseInt(TamanoPEncuesta.value) - 1)) + (elevado * p * q));
                    });
                    if (TamanoPEncuesta != null) {
                        TamanoPEncuesta.addEventListener('change', function(event) {
                            var Critico = parseInt(ValorEncuesta.value);
                            var elevado = Math.pow(Critico, 2)
                            document.getElementById(ctrlForm.parameters.ID.IdTamanoMuestra).value = elevado * parseInt(TamanoPEncuesta.value);
                        });
                    }
                }
            }
        }

    },
    RevisionAction: function() {
        var Revision = document.getElementById(ctrlForm.parameters.ID.IdRevision)
        if (Revision != null) {
            Revision.addEventListener('change', function(event) {
                if (Revision.checked != false) {
                    document.querySelector(ctrlForm.parameters.ID.Revision).style.display = ctrlForm.parameters.style.Block;
                    document.querySelector(ctrlForm.parameters.ID.FechaAprobadores).style.display = ctrlForm.parameters.style.Block;
                    document.querySelector(ctrlForm.parameters.ID.Onedrive).style.display = ctrlForm.parameters.style.None;
                } else {
                    document.querySelector(ctrlForm.parameters.ID.Revision).style.display = ctrlForm.parameters.style.None;
                    document.querySelector(ctrlForm.parameters.ID.Onedrive).style.display = ctrlForm.parameters.style.None;
                    document.querySelector(ctrlForm.parameters.ID.FechaAprobadores).style.display = ctrlForm.parameters.style.None;
                }
            });
        }
    },
    Auditoria: function() {
        var html = "";
        var htmla = "";
        var i = 0;
        var uno = spForms.loadedFields.RevidoresAprobados.value;
        var Json = utilities.convertToJSON(uno);
        var DivCapacitaciones = document.createElement(ctrlForm.parameters.parametersHTML.div);
        DivCapacitaciones.setAttribute(ctrlForm.parameters.parametersHTML.clase, ctrlForm.parameters.parametersHTML.form);
        DivCapacitaciones.setAttribute(ctrlForm.parameters.parametersHTML.id, ctrlForm.parameters.parametersHTML.idCapacitaciones);
        var uno = document.getElementsByClassName(ctrlForm.parameters.parametersHTML.formBody);
        uno[0].insertBefore(DivCapacitaciones, uno[0].childNodes[55]);
        for (i; i < Json.Personas.length; i++) {
            var Nombre = Json.Personas[i].Aprobador;
            var Estado = Json.Personas[i].Estado;
            var Fecha = Json.Personas[i].fecha;
            var Comentarios = Json.Personas[i].Comentarios;
            html += String.format(ctrlForm.parameters.formats.HtmlUser, Nombre, Estado, Fecha, Comentarios);
        }
        htmla = String.format(ctrlForm.parameters.formats.htmlTable, html);
        document.getElementById(ctrlForm.parameters.parametersHTML.idCapacitaciones).innerHTML = htmla;

    },
    UnlockedComments: function() {
        spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = false;
        var uno = document.querySelector(ctrlForm.parameters.ID.Comments);
        console.log(uno)
        uno.removeAttribute(ctrlForm.parameters.style.Invalid);

    },
    VerificarRevision: function() {
        var Revision = document.getElementById(ctrlForm.parameters.ID.IdRevision);
         var comentarios = document.getElementById('ObservacionesComunicados').value;
         spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
         if(comentarios != ""){
        if (Revision.checked != false ) {
            ctrlForm.changeState(ctrlForm.parameters.states[4]);
            //spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
        } else {
            spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
            ctrlForm.changeState(ctrlForm.parameters.states[5]);
        }
        }
    },
    JsonRevisoresApprove: function() {
        var CurrentState = spForms.loadedFields.EstadoComunicados.value;
        var spFormField = ctrlForm.fieldState[CurrentState].JsonAuditoriaApproved();
    },

    JsonRevisoresReject: function() {
 

        var CurrentState = spForms.loadedFields.EstadoComunicados.value;
        var spFormField = ctrlForm.fieldStateReject[CurrentState].JsonAuditoriaReject();
    },
    stateChange: function() {
        spForms.loadedFields.EstadoComunicados.value = ctrlForm.parameters.states[0];
    },
    commentsEmpty: function() {
        document.getElementById(ctrlForm.parameters.ID.IdComentarios).value = "";
    },
    ActualizarJson: function(JsonPersonas) {
        var state = spForms.loadedFields.EstadoComunicados.value;
        var nombre = _spPageContextInfo.userDisplayName;
        var today = new Date();
        var Fecha = today.format(ctrlForm.parameters.formats.dateFormatJson);
        var IdUser = _spPageContextInfo.userId;
        var Comentarios = document.getElementById(ctrlForm.parameters.ID.IdComentarios).value;
        var estado = "Aprobación revisión área";
        JsonPersonas.Personas.push({
            'Aprobador': '' + nombre + '',
            'Estado': '' + estado + '',
            'fecha': '' + Fecha + '',
            'Comentarios': '' + Comentarios + '',
            'IdUser': '' + IdUser + ''
        });
        var jsonRevisoresComplete = JSON.stringify(JsonPersonas)
        ctrlForm.parameters.ObjectPerson = jsonRevisoresComplete;
    },
    ActualizarAprobadores: function() {
        var id = spForms.currentItemID;
        var query = String.format(ctrlForm.parameters.queryList, id);
        sputilities.getItems(ctrlForm.parameters.ListName, query,
            function(data) {
                var data = utilities.convertToJSON(data);
                if (data != null) {
                    var numero = data.d.results[0].NumeroRevisoresAprobados
                    var ObjectPerson = data.d.results[0].RevidoresAprobados;
                    var JsonPersonas = JSON.parse(ObjectPerson)
                    if (numero == 0) {
                        var i = 0;
                        var contador = document.getElementById(ctrlForm.parameters.ID.IdNumeroAprobadores).value = i;
                        contador = contador + 1;
                        ctrlForm.parameters.Contador = contador;
                        ctrlForm.ActualizarJson(JsonPersonas);
                    } else {
                        var contador = data.d.results[0].NumeroRevisoresAprobados;
                        contador = contador + 1;
                        ctrlForm.parameters.Contador = contador;
                        ctrlForm.ActualizarJson(JsonPersonas);
                    }
                    if (ctrlForm.parameters.Contador == ctrlForm.parameters.Cantidad) {
                        spForms.loadedFields.EstadoComunicados.value = ctrlForm.parameters.states[5];
                    } else {
                        ctrlForm.parameters.RevisionInicial = false
                    }
                }
            },
            function(xhr) {
                console.log(xhr);
            }
        );
    },
      Validatecomments: function() {
        var uno = document.getElementById('ObservacionesComunicados').value;
          			if(uno != ""){
          			ctrlForm.changeState(ctrlForm.parameters.states[2]);
          			}   
          },
      ValidatecommentsApproved: function() {
        var comentarios = document.getElementById('ObservacionesComunicados').value;
          			if(comentarios != ""){
          			ctrlForm.changeState(ctrlForm.parameters.states[2]);
          			}   
          },
     validateDateInicial:function(){
		var Inicial = document.getElementById(ctrlForm.parameters.ID.IdFechaAprobacion);
		var Today = new Date()
		if(Inicial != null){
				Inicial.addEventListener('blur',function(event){
				var InicialEvent = moment(this.value,'DD/MM/YYYY').format('MM/DD/YYYY');
				var todayEvent = moment(Today).format('MM/DD/YYYY')
				
				var finalDate = document.querySelector(ctrlForm.parameters.ID.FechaAprobacionValidation)
				if(InicialEvent < todayEvent){
				finalDate.setAttribute(ctrlForm.parameters.style.Invalid,ctrlForm.parameters.style.MessageDate)
				}
				if(InicialEvent => todayEvent){
				finalDate.removeAttribute(ctrlForm.parameters.style.Invalid)
				}		    		
  					});
					}
			},
	validateDateNueva:function(){
		var Inicial = document.getElementById(ctrlForm.parameters.ID.IdFechaSolicitud);
		var Today = new Date()
		if(Inicial != null){
				Inicial.addEventListener('blur',function(event){
				var InicialEvent = moment(this.value,'DD/MM/YYYY').format('MM/DD/YYYY');
				var todayEvent = moment(Today).format('MM/DD/YYYY')
				
				var finalDate = document.querySelector(ctrlForm.parameters.ID.FechaSolicitudValidation)
				if(todayEvent <= InicialEvent ){
				finalDate.removeAttribute(ctrlForm.parameters.style.Invalid);
				}
				if(todayEvent > InicialEvent ){
				finalDate.setAttribute(ctrlForm.parameters.style.Invalid,ctrlForm.parameters.style.MessageDateSolicitud);
				
				}		    		
  					});
					}
			},
	validateDateEncuestas:function(){
		var Inicial = document.getElementById(ctrlForm.parameters.ID.IdFechaInicial);
		var Final = document.getElementById(ctrlForm.parameters.ID.IdFechaCierre);
		var Today = new Date()
			if(Inicial != null){
				Inicial.addEventListener('blur',function(event){
				var FinalEvent = moment(this.value,'DD/MM/YYYY').format('MM/DD/YYYY');
				var InicialEvent = moment(Inicial.value,'DD/MM/YYYY').format('MM/DD/YYYY');
				var todayEvent = moment(Today).format('MM/DD/YYYY')
				var InicialDate = document.querySelector(ctrlForm.parameters.ID.FechaInicialValidation)
				if(InicialEvent  >= todayEvent ){
				InicialDate.removeAttribute(ctrlForm.parameters.style.Invalid);
				}
				if(InicialEvent  < todayEvent ){
				InicialDate.setAttribute(ctrlForm.parameters.style.Invalid,ctrlForm.parameters.style.MessageDateInicial);
				}
				});
				}
				

		if(Final != null){
				Final.addEventListener('blur',function(event){
				var FinalEvent = moment(this.value,'DD/MM/YYYY').format('MM/DD/YYYY');
				var InicialEvent = moment(Inicial.value,'DD/MM/YYYY').format('MM/DD/YYYY');
				var todayEvent = moment(Today).format('MM/DD/YYYY')
				var FinalDate = document.querySelector(ctrlForm.parameters.ID.FechaCierreValidation)
				if(FinalEvent >= InicialEvent){
				FinalDate .removeAttribute(ctrlForm.parameters.style.Invalid);
				}
				if(FinalEvent < InicialEvent){
				FinalDate.setAttribute(ctrlForm.parameters.style.Invalid,ctrlForm.parameters.style.MessageDateFinal);
				}	    		
  					});
					}
			},
	fieldsRequired: function() {
		var Tipo = document.querySelector(ctrlForm.parameters.ID.TipoComunicacionStatic);
		var required =document.createElement('i');
		required.textContent = "*";
		Tipo.appendChild(required);
    },
    redirectCustomForm: function(name) {
        window.location.href = window.location.href.replace(ctrlForm.global.formName, name);
    }
}
ctrlForm.initialize();