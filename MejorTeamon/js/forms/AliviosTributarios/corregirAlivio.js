﻿var aliviosForm = {	
	parameters:{
		formats:{
				dateFormat:"DD/MM/YYYY",
				formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
				viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		fields:[{field:"NombrePersona1"},{field:"NumeroCelular"},{field:"CorreoElectronico"},{field:"ValorCredito"},{field:"AplicacionCredito"},{field:"MedicinaPrepagada"},{field:"NumeroDependiente"}],		
		internalNameFields:{
			TipoAlivio:'TipoAlivio',
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico',
			ValorCredito:'ValorCredito',
			AplicacionCredito:'AplicacionCredito',
			MedicinaPrepagada:'MedicinaPrepagada',
			NumeroDependiente:'NumeroDependiente',
			Estado:"EstadoSolicitudAlivio"			
		},
		controlsHtml:{
			Attachment:"sp-forms-add-attachment",
			Enviar:"btn-enviar",
			classDelete:".delete-doc",
			classGroup:".form-group.row",
			colorDisable:"#B1B1B1",
			borderDisable:"#E1E1E1"
		},
		fieldsHTML:{
			TipoAlivio:'[sp-static-name="TipoAlivio"]',
			NombrePersona:'[sp-static-name="NombrePersona1"]',
			NumeroCelular:'[sp-static-name="NumeroCelular"]',
			CorreoElectronico:'[sp-static-name="CorreoElectronico"]',
			ValorCredito:'[sp-static-name="ValorCredito"]',
			AplicacionCredito:'[sp-static-name="AplicacionCredito"]',
			MedicinaPrepagada:'[sp-static-name="MedicinaPrepagada"]',
			NumeroDependiente:'[sp-static-name="NumeroDependiente"]'
			
		},
		requiered:"sp-requiered",
		invalid:"sp-invalid",
		staticName:"sp-static-name",
		errorInvalid:"El correo electrónico no tiene el formato correcto",
		htmlRequiered:{position:"beforeend",html:"<i>*</i>",label:"label",block:"block",none:"none"},
		typeRelief:{Credito:"Crédito Hipotecario",Leasing:"Leasing Habitacional",Medicina:"Medicina Prepagada",Dependientes:"Dependientes"},
		bodyItem:"<div class='form-group row'><label>Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior</label></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
		containerForm:".form-content",
		TitlePopUp:"Alivios Tributarios - Acceso Denegado",
		formsParameters:{
			title:"Corregir Aplicar Solicitud Alivios Tributarios",
			description:"Corrige la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"}
			},
			contentType:"Alivios Tributarios",
			containerId:"spForm"			
		},		
		flowParameters:{
			urlFlow:"https://prod-51.westus.logic.azure.com:443/workflows/a38149fde3b145a8b0d17a7800ce9c8c/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=sWrLYrXah0Q9bv7Sf2AVjd27xLnvtcBskjxtwIPClyE",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",	   		
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Alivios Tributarios Ventanilla TH",
	    	applyUsers:"Alivios Tributarios Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:2,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Alivios Tributarios"
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud Ventanilla TH",			
			"AplicarSolicitud":"Aplicar Novedad",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Novedades":"Vista Novedades",
			"Ventanilla":"Vista Ventanilla"
		},
		global:{
			createdBy:null,
			created:null
		}

	},	
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";

	
	    aliviosForm.parameters.flowParameters.approveUrl = String.format(aliviosForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(aliviosForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);			    
	    aliviosForm.parameters.flowParameters.applyUrl = String.format(aliviosForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(aliviosForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    aliviosForm.parameters.flowParameters.editUrl = String.format(aliviosForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(aliviosForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    aliviosForm.parameters.flowParameters.listViewApply = String.format(aliviosForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(aliviosForm.parameters.forms)[5]);
	    aliviosForm.parameters.flowParameters.listViewApprove= String.format(aliviosForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(aliviosForm.parameters.forms)[6]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":aliviosForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":aliviosForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosAlivio.value,		        
		        "tipoAlivio":spForms.loadedFields.TipoAlivio.value
		    },
		    "state":spForms.loadedFields.EstadoSolicitudAlivio.value,
		    "approveUsers":aliviosForm.parameters.flowParameters.approveUsers,
		    "applyUsers":aliviosForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":aliviosForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":aliviosForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":aliviosForm.parameters.flowParameters.listNameAssing,
		    "editUrl":aliviosForm.parameters.flowParameters.editUrl,
		    "approveUrl":aliviosForm.parameters.flowParameters.approveUrl,
		    "applyUrl":aliviosForm.parameters.flowParameters.applyUrl,		    
		    "listViewApply":aliviosForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":aliviosForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	aliviosForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:aliviosForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	loadEditForm:function(){
		var row = document.querySelector(aliviosForm.parameters.fieldsHTML.TipoAlivio);
		if(row != null)
		{		
			var check_checked = row.querySelectorAll('label input:checked');
			
			for(var i=0; i<check_checked.length;i++){
			
				if(check_checked[i].value == aliviosForm.parameters.typeRelief.Credito){
					var valorCredito = document.querySelector(aliviosForm.parameters.fieldsHTML.ValorCredito);
					var aplicacionCredito = document.querySelector(aliviosForm.parameters.fieldsHTML.AplicacionCredito);
					if(valorCredito != null && aplicacionCredito != null)
					{
						aliviosForm.showHiddenControls(valorCredito,true);
						aliviosForm.showHiddenControls(aplicacionCredito,true);	
					}						
				}
					
				if(check_checked[i].value == aliviosForm.parameters.typeRelief.Leasing){
					var valorCredito = document.querySelector(aliviosForm.parameters.fieldsHTML.ValorCredito);
					if(valorCredito != null)
					{
						aliviosForm.showHiddenControls(valorCredito,true);
					}
				}
							
				if(check_checked[i].value == aliviosForm.parameters.typeRelief.Medicina){
					var medicina = document.querySelector(aliviosForm.parameters.fieldsHTML.MedicinaPrepagada);
					if(medicina != null)
					{
						aliviosForm.showHiddenControls(medicina,true);
					}
				}
						
				if(check_checked[i].value == aliviosForm.parameters.typeRelief.Dependientes){
					var dependientes = document.querySelector(aliviosForm.parameters.fieldsHTML.NumeroDependiente);
	 				if(dependientes != null)
	 				{
	 					aliviosForm.showHiddenControls(dependientes,true);
	 				}
				}
			}
		}
	},
	onChangeValueTipoAlivio:function(){
		var row = document.querySelector(aliviosForm.parameters.fieldsHTML.TipoAlivio);
		if(row != null){
			var checks = row.querySelectorAll('label input');
			for(var j= 0; j < checks.length; j++){
				var check = checks[j];
				check.addEventListener('change',function(){
					var row = document.querySelector(aliviosForm.parameters.fieldsHTML.TipoAlivio);
					var check_checked = row.querySelectorAll('label input:checked');
					var unchecked = row.querySelectorAll('label input:not(:checked)');														
					
					for(var i=0; i<unchecked.length;i++){
						if(unchecked[i].value == aliviosForm.parameters.typeRelief.Credito){
							var valorCredito = document.querySelector(aliviosForm.parameters.fieldsHTML.ValorCredito);
							var aplicacionCredito = document.querySelector(aliviosForm.parameters.fieldsHTML.AplicacionCredito);
							if(valorCredito != null && aplicacionCredito != null)
							{
								aliviosForm.showHiddenControls(valorCredito,false);
								aliviosForm.showHiddenControls(aplicacionCredito,false);	
							}						
						}
						
						if(unchecked[i].value == aliviosForm.parameters.typeRelief.Leasing){
							var valorCredito = document.querySelector(aliviosForm.parameters.fieldsHTML.ValorCredito);
							if(valorCredito != null)
							{
								aliviosForm.showHiddenControls(valorCredito,false);
							}					
						}
						
						if(unchecked[i].value == aliviosForm.parameters.typeRelief.Medicina){
							var medicina = document.querySelector(aliviosForm.parameters.fieldsHTML.MedicinaPrepagada);
							if(medicina != null)
							{
								aliviosForm.showHiddenControls(medicina,false);
							}
						}
						
						if(unchecked[i].value == aliviosForm.parameters.typeRelief.Dependientes){
							var dependientes = document.querySelector(aliviosForm.parameters.fieldsHTML.NumeroDependiente);
 							if(dependientes != null)
 							{
 								aliviosForm.showHiddenControls(dependientes,false);
 							}
 						}
					}
					
					for(var i=0; i<check_checked.length;i++){
						if(check_checked[i].value == aliviosForm.parameters.typeRelief.Credito){
							var valorCredito = document.querySelector(aliviosForm.parameters.fieldsHTML.ValorCredito);
							var aplicacionCredito = document.querySelector(aliviosForm.parameters.fieldsHTML.AplicacionCredito);
							if(valorCredito != null && aplicacionCredito != null)
							{
								aliviosForm.showHiddenControls(valorCredito,true);
								aliviosForm.showHiddenControls(aplicacionCredito,true);	
							}						
						}
						
						if(check_checked[i].value == aliviosForm.parameters.typeRelief.Leasing){
							var valorCredito = document.querySelector(aliviosForm.parameters.fieldsHTML.ValorCredito);
							if(valorCredito != null)
							{
								aliviosForm.showHiddenControls(valorCredito,true);
							}
						}
						
						if(check_checked[i].value == aliviosForm.parameters.typeRelief.Medicina){
							var medicina = document.querySelector(aliviosForm.parameters.fieldsHTML.MedicinaPrepagada);
							if(medicina != null)
							{
								aliviosForm.showHiddenControls(medicina,true);
							}
						}
						
						if(check_checked[i].value == aliviosForm.parameters.typeRelief.Dependientes){
							var dependientes = document.querySelector(aliviosForm.parameters.fieldsHTML.NumeroDependiente);
 							if(dependientes != null)
 							{
 								aliviosForm.showHiddenControls(dependientes,true);
 							}
						}
					}
					
				});
			}
		}
	},
	validateRequired:function(){
		
		var fields = document.querySelectorAll(aliviosForm.parameters.fieldsHTML.ValorCredito + "," + aliviosForm.parameters.fieldsHTML.AplicacionCredito + ","
					+ aliviosForm.parameters.fieldsHTML.MedicinaPrepagada +"," + aliviosForm.parameters.fieldsHTML.Dependientes);
		var rowEmail = document.querySelector(aliviosForm.parameters.fieldsHTML.CorreoElectronico);
		var email = document.getElementById(aliviosForm.parameters.internalNameFields.CorreoElectronico);
		var isValid = true;		
		if(email != null && fields != null && rowEmail!= null)
		{
			for(var i=0;i<fields.length;i++)
			{
				if(fields[i].getAttribute(aliviosForm.parameters.requiered) == "true")
				{
					spForms.loadedFields[fields[i].getAttribute(aliviosForm.parameters.staticName)].required =true;
				}
				else
				{
					spForms.loadedFields[fields[i].getAttribute(aliviosForm.parameters.staticName)].required =false;
					fields[i].removeAttribute(aliviosForm.parameters.invalid);
				}
			}
			
			if(aliviosForm.validateEmail(email.value) || email.value == "")
			{
				rowEmail.removeAttribute(aliviosForm.parameters.invalid);
			}
			else
			{
				rowEmail.setAttribute(aliviosForm.parameters.invalid,aliviosForm.parameters.errorInvalid);
	
			}
		}
		else
		{
			isValid=false;
		}
		return isValid;
		
	},	
	loadResponsableState:function(){
	
		try
		{
			var currentUserId =_spPageContextInfo.userId;		
		
			var find = false;
			var state = document.getElementById(aliviosForm.parameters.internalNameFields.Estado);
			if(state != null)
			{
				if(spForms.currentItem.AuthorId == currentUserId){
					find = true;
				}
								
				if(find && state.value == "4")
				{				
					aliviosForm.loadEditForm();
				}
				else
				{					
					aliviosForm.hideNotAllowed();
				}
			}
		}
		catch(e){
			console.log(e);
		}	
	},
	disableInputs:function(){
	
		var rows = document.querySelectorAll(aliviosForm.parameters.controlsHtml.classGroup);
		
		if(rows != null)
		{
			for(var i=0;i<rows.length;i++)
			{
				rows[i].style.pointerEvents = aliviosForm.parameters.htmlRequiered.none;
			}
		}
	},
	showHiddenControls:function(ctl,isRequiered)
	{
		if(!isRequiered)
		{
			ctl.style.display = aliviosForm.parameters.htmlRequiered.none;
										
			var label = ctl.querySelector(aliviosForm.parameters.htmlRequiered.label);
			if(label != null)
			{
				if(label.lastElementChild != null)
				{
					label.removeChild(label.lastElementChild);
				}
			}
			ctl.setAttribute(aliviosForm.parameters.requiered,isRequiered);	
		}
		else
		{
			ctl.style.display = aliviosForm.parameters.htmlRequiered.block;
			ctl.setAttribute(aliviosForm.parameters.requiered,isRequiered);
			var label = ctl.querySelector(aliviosForm.parameters.htmlRequiered.label);
			if(label != null)
			{
				if(label.lastElementChild == null)
				{
					label.insertAdjacentHTML(aliviosForm.parameters.htmlRequiered.position,aliviosForm.parameters.htmlRequiered.html);
				}
			}

		}		
	},
	hideNotAllowed:function(){
		var html = aliviosForm.parameters.bodyItem;
		var formContent = document.querySelector(aliviosForm.parameters.containerForm);
		if(formContent != null){
			formContent.innerHTML = html;
		}
	},
	validateEmail: function (email){
		 var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);	
	}
}

spForms.options={
	actions:[
		{
			type:spForms.parameters.updateAction,
			name:aliviosForm.parameters.formsParameters.buttons.Send.Text,
			id:aliviosForm.parameters.formsParameters.buttons.Send.Id,
			success:function (){
				aliviosForm.callFlowService();
				return false;	
			},
			error:function (){},
			preSave: function (){
				document.getElementById(aliviosForm.parameters.internalNameFields.Estado).value = "1";
				return aliviosForm.validateRequired()			
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:aliviosForm.parameters.formsParameters.buttons.Cancel.Text,
			id:aliviosForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:aliviosForm.parameters.formsParameters.title,
	description:aliviosForm.parameters.formsParameters.description,
	contentTypeName:aliviosForm.parameters.formsParameters.contentType,
	containerId:aliviosForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:20,
  	minAttachments:1,
  	maxSizeInMB:5,
	successLoadEditForm:function(){	
		aliviosForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
		aliviosForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(aliviosForm.parameters.formats.dateFormat);
		aliviosForm.loadResponsableState();
		aliviosForm.onChangeValueTipoAlivio();		
	} 
};
spForms.loadEditForm();
