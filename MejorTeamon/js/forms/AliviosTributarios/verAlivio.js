﻿var aliviosForm = {
	parameters:{
	fields:[{field:"ValorCredito"},{field:"AplicacionCredito"},{field:"MedicinaPrepagada"},{field:"NumeroDependiente"}],
	internalNameFields:{
			TipoAlivio:'TipoAlivio',
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico',
			ValorCredito:'ValorCredito',
			AplicacionCredito:'AplicacionCredito',
			MedicinaPrepagada:'MedicinaPrepagada',
			NumeroDependiente:'NumeroDependiente',
			Estado:"EstadoSolicitudAlivio",
			Comentarios:"ComentariosAlivio"			
		},
	controlsHtml:{
		Attachment:"sp-forms-add-attachment",
		Aprobar:"btn-aprobar",
		Corregir:"btn-corregir",
		Aplicar:"btn-aplicar",
		classDelete:".delete-doc",
		classGroup:".form-group.row",
		colorDisable:"#B1B1B1",
		borderDisable:"#E1E1E1",
		iconAttachment:"ico-help-ComentariosAlivio"
	},
	fieldsHTML:{
		TipoAlivio:'[sp-static-name="TipoAlivio"]',
		NombrePersona:'[sp-static-name="NombrePersona1"]',
		NumeroCelular:'[sp-static-name="NumeroCelular"]',
		CorreoElectronico:'[sp-static-name="CorreoElectronico"]',
		ValorCredito:'[sp-static-name="ValorCredito"]',
		AplicacionCredito:'[sp-static-name="AplicacionCredito"]',
		MedicinaPrepagada:'[sp-static-name="MedicinaPrepagada"]',
		NumeroDependiente:'[sp-static-name="NumeroDependiente"]',	
		Comentarios:'[sp-static-name="ComentariosAlivio"]'			
	},
	requiered:"sp-requiered",
	invalid:"sp-invalid",
	staticName:"sp-static-name",
	errorInvalid:{text:"Los comentarios son obligatorios",area:"textarea"},
	htmlRequiered:{position:"beforeend",html:"<i>*</i>",label:"label",block:"block",none:"none"},
	typeRelief:{Credito:"Crédito Hipotecario",Leasing:"Leasing Habitacional",Medicina:"Medicina Prepagada",Dependientes:"Dependientes"},
	codeStates:{Solicitado:"0",Ventanilla:"1",Novedades:"2",Aplicada:"3",Corregir:"4"},
	formsParameters:{
			title:"Corregir Aplicar Solicitud Alivios Tributarios",
			description:"Corrige la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"}
			},
			contentType:"Alivios Tributarios",
			containerId:"spForm"			
		}	
	},
	loadSuccessForm:function(){		
		var row = document.querySelector(aliviosForm.parameters.fieldsHTML.TipoAlivio);
		if(row != null)
		{		
			var check_checked = row.querySelectorAll('label input:checked');
			
			for(var i=0; i<check_checked.length;i++){
			
				if(check_checked[i].value == aliviosForm.parameters.typeRelief.Credito){
					var valorCredito = document.querySelector(aliviosForm.parameters.fieldsHTML.ValorCredito);
					var aplicacionCredito = document.querySelector(aliviosForm.parameters.fieldsHTML.AplicacionCredito);
					if(valorCredito != null && aplicacionCredito != null)
					{
						aliviosForm.showHiddenControls(valorCredito,true);
						aliviosForm.showHiddenControls(aplicacionCredito,true);	
					}						
				}
					
				if(check_checked[i].value == aliviosForm.parameters.typeRelief.Leasing){
					var valorCredito = document.querySelector(aliviosForm.parameters.fieldsHTML.ValorCredito);
					if(valorCredito != null)
					{
						aliviosForm.showHiddenControls(valorCredito,true);
					}
				}
							
				if(check_checked[i].value == aliviosForm.parameters.typeRelief.Medicina){
					var medicina = document.querySelector(aliviosForm.parameters.fieldsHTML.MedicinaPrepagada);
					if(medicina != null)
					{
						aliviosForm.showHiddenControls(medicina,true);
					}
				}
						
				if(check_checked[i].value == aliviosForm.parameters.typeRelief.Dependientes){
					var dependientes = document.querySelector(aliviosForm.parameters.fieldsHTML.NumeroDependiente);
	 				if(dependientes != null)
	 				{
	 					aliviosForm.showHiddenControls(dependientes,true);
	 				}
				}
			}
		}
	},
	showHiddenControls:function(ctl,isRequiered)
	{
		if(!isRequiered)
		{
			ctl.style.display = aliviosForm.parameters.htmlRequiered.none;
										
			var label = ctl.querySelector(aliviosForm.parameters.htmlRequiered.label);
			if(label != null)
			{
				if(label.lastElementChild != null)
				{
					label.removeChild(label.lastElementChild);
				}
			}
			ctl.setAttribute(aliviosForm.parameters.requiered,isRequiered);	
		}
		else
		{
			ctl.style.display = aliviosForm.parameters.htmlRequiered.block;
			ctl.setAttribute(aliviosForm.parameters.requiered,isRequiered);
			var label = ctl.querySelector(aliviosForm.parameters.htmlRequiered.label);
			if(label != null)
			{
				if(label.lastElementChild == null)
				{
					label.insertAdjacentHTML(aliviosForm.parameters.htmlRequiered.position,aliviosForm.parameters.htmlRequiered.html);
				}
			}

		}		
	}	
}
spForms.options={
	actions:[		
		{
			type:spForms.parameters.cancelAction,
			name:aliviosForm.parameters.formsParameters.buttons.Cancel.Text,
			id:aliviosForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:aliviosForm.parameters.formsParameters.title,	
	contentTypeName:aliviosForm.parameters.formsParameters.contentType,
	containerId:aliviosForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:true,
 	maxAttachments:20,
  	minAttachments:1,
  	maxSizeInMB:5,
	successLoadEditForm:function(){
		aliviosForm.loadSuccessForm();
	} 
};
spForms.loadEditForm();

