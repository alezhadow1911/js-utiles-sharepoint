﻿var ctrlForm = {
    parameters: {
        formats: {
            "queryStaticNameRow": "[sp-static-name='{0}']",
            "htmlNotAllowed": "<div><h4>{0}</h4></div> <div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>",
            "querySelectRadio": 'input[type="radio"][value="{0}"]:checked',
            "htmlTable": "<div id='header-contacts'><div><h3>Usuario Aprobador</h3></div><div><h3>Estado</h3></div><div><h3>Fecha</h3></div><div><h3>Otros</h3> </div></div>{0}",
            "HtmlUser": "<div class='data-row-contact'> <div><h3>Persona</h3>   </div>   <section>{0}</section>   <div>      <h3>Teléfonos</h3>   </div>   <section>{1}</section>   <div>      <h3>Celular</h3>   </div>   <section>{2}</section>   <div>      <h3>Email</h3>   </div>   <section>{3}</section></div>",
            "HtmlOneDrive": "<p>Haga clic <a href='{0}' target='_blank'>aquí</a> Para abrir el documento de one drive</p>",
            "queryRadio": 'input[type="radio"][value="{0}"]',
            "emailUsers": "{0};",
            "emailUsersRevisores": "{0};",
            "dateFormat": "DD-MM-YYYY",
            "dateFormatJson": "dd/MM/yyyy hh:mm tt",
            "formFormat": "{0}//{1}{2}/{3}.aspx?ID={4}&source={5}"
        },
        forms: {
            "NewForm": "ctrlForm.redirectCustomForm('NuevaSolicitud')",
            "EditForm": "ctrlForm.redirectCustomForm('CorregirSolicitud')",
            "DispForm": "ctrlForm.redirectCustomForm('VerSolicitud')",
            "NuevaSolicitud": "ctrlForm.loadNew()",
            "Propietarios": "ctrlForm.loadApprove()",
            "CorregirSolicitud": "ctrlForm.loadEditApplicant()",
            "Business": "ctrlForm.loadBusiness()",
            "VerSolicitud": "ctrlForm.loadReview()",
            "PendientesAprobar": "",
            "Aprobadas": "",
            "Areas": "ctrlForm.loadAreas()",
            "Finalizado": "ctrlForm.loadFinalizado()",
            "AreaTI": "ctrlForm.loadAreasTI()",
            "Arquitectura": "ctrlForm.loadArquitectura()",
        },
        messages: {
            "idSaveForm": "enviarSolicitud",
            "idCancelForm": "cancelar",
            "idEditForm": "enviarCorreccion",
            "idApproveForm": "aprobar",
            "idRejectForm": "solicitarCorreccion",
            "idRejectGlobal": "Rechazar",
            "textSaveForm": "Enviar",
            "textCancelForm": "Cancelar",
            "textEditForm": "Enviar",
            "textApproveForm": "Aprobar",
            "textRejectForm": "Enviar a Corregir",
            "textRejectGlobal": "Rechazar",
            "textApplyForm": "Novedad Aplicada",
            "textNotAllowed": "Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior",
            "textNotAllowedRevisores": "Usted no tiene permisos sobre este formulario o la solicitud ya fue aprobada por usted"
        },
        columns: {
            "comments": {
                "staticName": "ComentariosPublicacion"
            },
            "Onedrive": {
                "staticName": "UrlOnedrive"
            },

            "displayName": {
                "staticName": "SolicitanteGobierno"
            },
            "idDoc": {
                "staticName": "NombrePersona1"
            },
            "cellPhone": {
                "staticName": "NumeroCelular"
            },
            "email": {
                "staticName": "CorreoElectronico"
            }
        },
        states: [
            "Solicitado",
            "Aprobación Propietario",
            "Rechazo Propietario",
            "Business Parther",
            "Rechazo Business",
            "Área Procesos",
            "Rechazo Procesos",
            "Área TI",
            "Rechazo TI",
            "Arquitectura",
            "Rechazo Arquitectura",
            "Finalizado",
        ],
        parametersHTML: {
            "spHidden": "sp-hidden",
            "spDisable": "sp-disable",
            div: "div",
            clase: "class",
            form: "form-group row",
            id: "id",
            idCapacitaciones: "Capacitaciones",
            formBody: "form-body form-horizontal",
            formHorizontal: ".form-horizontal .form-header",
            formrow: "form-group row",
            formHeader: "form-header",
            textFicha: "Sección Ficha Tecnica",
            h2: "h2",
            formHeaderQuerySelector: ".form-header",
            textMedios: "Sección Medios"
        },
        Functions: {
            Onchange: "onchange",
            functionOnchange: "javascript:ctrlForm.clickSelect(this);"
        },

        ID: {
            ContentClose: ".col-md-6.col-xs-6.delete-doc",
            ContentAdd: "a#sp-forms-add-attachment",
            Attachment: "div[sp-static-name='sp-forms-attachment']",
            Comments: "div[sp-static-name='ComentariosPublicacion']",
            Correo: "CorreoElectronico",
            TipoSolicitud: "TipoSolicitud",
            IdSelect:".form-content .form-header",
            IdOption:"#TipoSolicitud option",

            TipoFlujo: "[sp-static-name='TipoFlujo']",
            NumeroAprobadores: "[sp-static-name='NumeroAprobadores']",
            Ejecucion: "[sp-static-name='Ejecucion']",
            NumeroItems: "[sp-static-name='NumerosItems']",
            Estimado: "[sp-static-name='Tama_x00f1_oEstimado']",
            Crecimiento: "[sp-static-name='CrecimientoAnual']",
            IdComentarios: "ComentariosPublicacion",
            IdRevision: "RequiereAprobacionesTI"
        },
        style: {
            None: "none",
            Opacity: "0.7",
            Block: "block",
            //Atributtes
            Invalid: "sp-invalid",
            Message: "Formato de correo invalido",
            opacity: "0.7",
            MessageDate: "Fecha aprobacion menor a fecha actual",
            MessageDateSolicitud: "Fecha solicitud menor a fecha actual",
            MessageDateInicial: "Fecha inicio menor a hoy",
            MessageDateFinal: "Fecha final menor a fecha inicio"


        },
        selectorFormContent: ".form-content",
        cellPhone: "CellPhone",
        functionLoadMyProperties: "ctrlForm.loadMyProperties",
        containerId: "container-form",
        description: "Complete la información para la solicitud",
        descriptionEdit: "Corrija la información de la solicitud",
        descriptionApply: "Revise la información y aplique la novedad si corresponde",
        descriptionApprove: "Revise la información y apruebe o envíe a corrección según corresponda",
        descriptionFinalizado: "Finalizacion del comunicado",
        descriptionReview: "Resultado de la solicitud",
        contentTypeName: "PublicacionIntranet",
        formIsLoaded: false,
        allowAttachments: true,
        requiredAttachments: true,
        maxAttachments: 20,
        minAttachments: 0,
        maxSizeInMB: 3,
        listNameConsecutive: "ControlRadicados",
        idConsecutive: 1,
        listUsersFlow: "ResponsablesTecnologia",
        listSolicitud: "TipoSolicitud",
        ListName: sputilities.contextInfo().listTitle,
        viewUrl: "",
        editUrl: "",
        approveUrl: "",
        applyUrl: "",
        applyUrlTI: "",
        applyUrlArquitectura: "",
        UrlBusiness: "",
        UrlFinalizado: "",
        FechaAprobacion: "",
        Validacion: "",
        StateFinish: "",
        NameCurrentSelect: "",
        RevisionInicial: true,
        NameApplicant: "",
        ObjectPerson: "",
        successQuery: false,
        listViewApply: "",
        listViewApprove: "",
        queryUsersTH: "?$filter=ID eq 1&$select=ResponsablesAbonos/EMail&$expand=ResponsablesAbonos",
        queryUsersBusiness: "?$filter=ID eq 2&$select=ResponsablesAbonos/EMail&$expand=ResponsablesAbonos",
        queryUsersAreas: "?$filter=ID eq 3&$select=ResponsablesAbonos/EMail&$expand=ResponsablesAbonos",
        queryUsersAreasTI: "?$filter=ID eq 4&$select=ResponsablesAbonos/EMail&$expand=ResponsablesAbonos",
        queryUsersArquitectura: "?$filter=ID eq 5&$select=ResponsablesAbonos/EMail&$expand=ResponsablesAbonos",
        queryUrlSolicitud: "?$filter=ID eq {0}&$select=ValidacionTI",
        flowUrl: "https://prod-03.westus.logic.azure.com:443/workflows/54a9d11f69194ec08426e33ac656793f/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=BAWTCykikuqGsh66e4ReX3udc921MZwGOT1yvCzs_1k"

    },
    global: {
        formName: null,
        createdBy: null,
        created: null,
        approveUsers: "", // Propietarios Intranet
        applyUsers: "", // Business Parther
        Revisores: "", //Areas
        RevisoresTI: "", //Areas TI
        RevisoresArquitectura: "", //Areas TI
        IdUsers: ""

    },
    fieldType: {
        "0": { //para todos
            Actualizar: function() {
                ctrlForm.HideAll();
            }
        },
        "2": { //Flujos
            Actualizar: function() {
                ctrlForm.ShowFields();
            }
        },
        "9": {
            Actualizar: function() {
                ctrlForm.ShowFieldsLibrary();
            }
        },
    },

    initialize: function() {
        var partsOfUrl = window.location.href.split('?')[0].split('/');
        ctrlForm.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];
        eval(ctrlForm.parameters.forms[ctrlForm.global.formName]);
    },
    callFlowService: function() {
        document.getElementById('s4-workspace').scrollTop = 0;
        document.querySelector('.form-content').style.opacity = 0;
        document.querySelector('.loader-container').style.opacity = 1;
        document.querySelector('.loader-container').style.height = "auto";


        ctrlForm.parameters.viewUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[7], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.editUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[5], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.approveUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[4], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl); //Propietarios
        ctrlForm.parameters.applyUrl = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[10], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl); //areas
        ctrlForm.parameters.applyUrlTI = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[12], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl); //areasTI
        ctrlForm.parameters.applyUrlArquitectura = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[13], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl); //areasTI        
        ctrlForm.parameters.UrlBusiness = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[6], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl); //Business
        ctrlForm.parameters.UrlFinalizado = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[11], spForms.currentItemID, sputilities.contextInfo().webAbsoluteUrl);
        ctrlForm.parameters.listViewApply = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[9]);
        ctrlForm.parameters.listViewApprove = String.format(ctrlForm.parameters.formats.formFormat, window.location.protocol, window.location.hostname, sputilities.contextInfo().listUrl, utilities.getKeysOfAnObject(ctrlForm.parameters.forms)[8]);

        var objService = {
            "currentItemID": spForms.currentItemID,
            "currentItem": {
                "createdBy": ctrlForm.global.createdBy,
                "tipo": spForms.loadedFields.TipoSolicitud.value,
                "descripcion": spForms.loadedFields.DescripcionGobierno.value,
                "solicitante": spForms.loadedFields.SolicitanteGobierno.value,
                "consecutivo": spForms.loadedFields.NumeroRadicado.value,
                "NombrePublicacion": spForms.loadedFields.NombreContenido.value,
                "comentarios": spForms.loadedFields.ComentariosPublicacion.value,
                "validacion": ctrlForm.parameters.Validacion,
                "fechaCreacion": ctrlForm.global.created,
            },
            "state": spForms.loadedFields.EstadosPublicacion.value,
            "titleCorreo": spForms.listInfo.title,
            "approveUsers": ctrlForm.global.approveUsers,
            "applyUsers": ctrlForm.global.applyUsers,
            "applyUsersRevisores": ctrlForm.global.Revisores,
            "applyUsersRevisoresTI": ctrlForm.global.RevisoresTI,
            "applyUsersRevisoresArquitectura": ctrlForm.global.RevisoresArquitectura,
            "siteUrl": sputilities.contextInfo().webAbsoluteUrl,
            "listName": sputilities.contextInfo().listTitle,
            "listNameConsecutive": ctrlForm.parameters.listNameConsecutive,
            "idConsecutive": ctrlForm.parameters.idConsecutive,
            "entityType": spForms.listInfo.entityItem,
            "viewUrl": ctrlForm.parameters.viewUrl,
            "editUrl": ctrlForm.parameters.editUrl,
            "approveUrl": ctrlForm.parameters.approveUrl,
            "applyUrl": ctrlForm.parameters.applyUrl,
            "applyUrlTI": ctrlForm.parameters.applyUrlTI,
            "applyUrlArquitectura": ctrlForm.parameters.applyUrlArquitectura,
            "UrlBusiness": ctrlForm.parameters.UrlBusiness,
            "UrlFinalizado": ctrlForm.parameters.UrlFinalizado,
            "FechaAprobacion": ctrlForm.parameters.FechaAprobacion,
            "EmailInicialRevision": ctrlForm.parameters.RevisionInicial,
            "NameApplicant": ctrlForm.parameters.NameApplicant,
            "StateFinish": ctrlForm.parameters.StateFinish,
            "SelectCurrent": ctrlForm.parameters.NameCurrentSelect,
            "JsonPerson": ctrlForm.parameters.ObjectPerson,
            "listViewApply": ctrlForm.parameters.listViewApply,
            "listViewApprove": ctrlForm.parameters.listViewApprove
        }
        utilities.http.callRestService({
            url: ctrlForm.parameters.flowUrl,
            method: utilities.http.methods.POST,
            data: JSON.stringify(objService),
            headers: {
                "accept": utilities.http.types.jsonOData,
                "content-type": utilities.http.types.jsonOData
            },
            success: function(data) {
                document.querySelector('.loader-container').style.opacity = 0;
                document.querySelector('.form-content').style.opacity = 1;
                document.querySelector('.loader-container').style.height = 0;

                spForms.cancelForm(null);
            },
            error: function(xhr) {
                console.log(xhr);
            }
        });

    },
    getUsersOwners: function() {
        sputilities.getItems(ctrlForm.parameters.listUsersFlow, ctrlForm.parameters.queryUsersTH,
            function(data) {
                var data = utilities.convertToJSON(data);
                if (data != null) {
                    var items = data.d.results;
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        for (var j = 0; j < item.ResponsablesAbonos.results.length; j++) {
                            var user = item.ResponsablesAbonos.results[j];
                            ctrlForm.global.approveUsers += String.format(ctrlForm.parameters.formats.emailUsers, user.EMail);
                        }
                    }
                }
            },
            function(xhr) {
                console.log(xhr);
            }
        );
    },
    getUsersBusiness: function() {
        sputilities.getItems(ctrlForm.parameters.listUsersFlow, ctrlForm.parameters.queryUsersBusiness,
            function(data) {
                var data = utilities.convertToJSON(data);
                if (data != null) {
                    var items = data.d.results;
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        for (var j = 0; j < item.ResponsablesAbonos.results.length; j++) {
                            var user = item.ResponsablesAbonos.results[j];
                            ctrlForm.global.applyUsers += String.format(ctrlForm.parameters.formats.emailUsers, user.EMail);
                        }
                    }
                }
            },
            function(xhr) {
                console.log(xhr);
            }
        );
    },
    getUsersAreas: function() {
        sputilities.getItems(ctrlForm.parameters.listUsersFlow, ctrlForm.parameters.queryUsersAreas,
            function(data) {
                var data = utilities.convertToJSON(data);
                if (data != null) {
                    var items = data.d.results;
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        for (var j = 0; j < item.ResponsablesAbonos.results.length; j++) {
                            var user = item.ResponsablesAbonos.results[j];
                            ctrlForm.global.Revisores += String.format(ctrlForm.parameters.formats.emailUsers, user.EMail);
                        }
                    }
                }
            },
            function(xhr) {
                console.log(xhr);
            }
        );
    },
    getUsersAreasTI: function() {
        sputilities.getItems(ctrlForm.parameters.listUsersFlow, ctrlForm.parameters.queryUsersAreasTI,
            function(data) {
                var data = utilities.convertToJSON(data);
                if (data != null) {
                    var items = data.d.results;
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        for (var j = 0; j < item.ResponsablesAbonos.results.length; j++) {
                            var user = item.ResponsablesAbonos.results[j];
                            ctrlForm.global.RevisoresTI += String.format(ctrlForm.parameters.formats.emailUsers, user.EMail);
                        }
                    }
                }
            },
            function(xhr) {
                console.log(xhr);
            }
        );
    },
    getUsersArquitectura: function() {
        sputilities.getItems(ctrlForm.parameters.listUsersFlow, ctrlForm.parameters.queryUsersArquitectura,
            function(data) {
                var data = utilities.convertToJSON(data);
                if (data != null) {
                    var items = data.d.results;
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        for (var j = 0; j < item.ResponsablesAbonos.results.length; j++) {
                            var user = item.ResponsablesAbonos.results[j];
                            ctrlForm.global.RevisoresArquitectura += String.format(ctrlForm.parameters.formats.emailUsers, user.EMail);
                        }
                    }
                }
            },
            function(xhr) {
                console.log(xhr);
            }
        );
    },
    GetApplicant: function() {
        var FieldApplicant = SPClientPeoplePicker.SPClientPeoplePickerDict.SolicitanteGobierno_TopSpan.GetAllUserInfo()
        if (FieldApplicant.length > 0) {
            var i = 0;
            for (i; i < FieldApplicant.length; i++) {
                var Name = FieldApplicant[i].DisplayText;
                ctrlForm.parameters.NameApplicant = Name;
            }
        }
    },
    GetValidation: function(Id) {
        var query = String.format(ctrlForm.parameters.queryUrlSolicitud, Id);
        sputilities.getItems(ctrlForm.parameters.listSolicitud, query,
            function(data) {
                var data = utilities.convertToJSON(data);
                if (data != null) {
                    var items = data.d.results;
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i].ValidacionTI;
                        ctrlForm.parameters.Validacion = item
                    }
                }
            },
            function(xhr) {
                console.log(xhr);
            }
        );
    },
    changeState: function(state) {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.queryRadio, state));
        if (radio != null) {
            radio.click();
        }
    },
    hideNotAllowed: function() {
        var html = String.format(ctrlForm.parameters.formats.htmlNotAllowed, ctrlForm.parameters.messages.textNotAllowed);
        var formContent = document.querySelector(ctrlForm.parameters.selectorFormContent);
        if (formContent != null) {
            formContent.innerHTML = html;
        }
    },
    validateEditUser: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[11])) == null;
        if (spForms.currentItem.AuthorId != sputilities.contextInfo().userId || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        return true;
    },
    //Novedades
    validateApplyUser: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[3])) == null;
        if (ctrlForm.global.applyUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        ctrlForm.hideAttachments();
    },
    //Propietarios
    validateApproveUser: function() {
        var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[1])) == null;
        if (ctrlForm.global.approveUsers.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio) {
            ctrlForm.hideNotAllowed();
            return false;
        }
        ctrlForm.hideAttachments();
    },
    //Notificacion para revision
    validateUserRevision: function() {
        var usuarios = setTimeout(function() {
            var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[5])) == null;
            if (ctrlForm.global.Revisores.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio) {
                ctrlForm.hideNotAllowed();
                return false;
            }
            ctrlForm.hideAttachments();
        }, 7000);
    },

    validateUserRevisionArea: function() {
        var usuarios = setTimeout(function() {
            var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[7])) == null;
            if (ctrlForm.global.RevisoresTI.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio) {
                ctrlForm.hideNotAllowed();
                return false;
            }
            ctrlForm.hideAttachments();
        }, 7000);
    },
    validateUserArquitectura: function() {
        var usuarios = setTimeout(function() {
            var radio = document.querySelector(String.format(ctrlForm.parameters.formats.querySelectRadio, ctrlForm.parameters.states[9])) == null;
            if (ctrlForm.global.RevisoresArquitectura.split(';').indexOf(sputilities.contextInfo().userEmail) == -1 || radio) {
                ctrlForm.hideNotAllowed();
                return false;
            }
            ctrlForm.hideAttachments();
        }, 7000);
    },
    hideAttachments: function() {
        var i;
        var CloaseAttachments = document.querySelectorAll(ctrlForm.parameters.ID.ContentClose);
        for (i = 0; i < CloaseAttachments.length; i++) {
            CloaseAttachments[i].style.display = ctrlForm.parameters.style.None;
        }
        document.querySelector(ctrlForm.parameters.ID.ContentAdd).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.Attachment).style.opacity = ctrlForm.parameters.style.Opacity;
    },
    hideComments: function() {
        var ctrlComments = document.querySelector(String.format(ctrlForm.parameters.formats.queryStaticNameRow, ctrlForm.parameters.columns.comments.staticName));
        if (ctrlComments != null) {
            ctrlComments.setAttribute(ctrlForm.parameters.parametersHTML.spHidden, true);
        }
    },
    loadReview: function() {
        var actions = [{
            type: spForms.parameters.cancelAction,
            name: ctrlForm.parameters.messages.textCancelForm,
            id: ctrlForm.parameters.messages.idCancelForm,
            callback: null
        }, ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionReview;
        ctrlForm.loadEdit(actions, null);
    },
    loadApprove: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textApproveForm,
                id: ctrlForm.parameters.messages.idApproveForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.UnlockedComments();
                    ctrlForm.changeState(ctrlForm.parameters.states[11]);
                    ctrlForm.parameters.StateFinish = ctrlForm.parameters.states[1];
                    return true;
                }
            },
            {
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textRejectGlobal,
                id: ctrlForm.parameters.messages.idRejectGlobal,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.changeState(ctrlForm.parameters.states[2]);
                    spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
                    return true;
                }
            },
            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
        ctrlForm.loadEdit(actions, ctrlForm.validateApproveUser);

    },
    loadAreas: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textApproveForm,
                id: ctrlForm.parameters.messages.idApproveForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;

                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.UnlockedComments();
                    ctrlForm.changeState(ctrlForm.parameters.states[7]);
                    ctrlForm.GetApplicant();

                    return true;
                }
            },
            {
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textRejectGlobal,
                id: ctrlForm.parameters.messages.idRejectGlobal,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.changeState(ctrlForm.parameters.states[6]);
                    ctrlForm.parameters.StateFinish = ctrlForm.parameters.states[5];
                    spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
                    return true;
                }
            },
            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
        ctrlForm.loadEdit(actions, ctrlForm.validateUserRevision);

    },
    loadAreasTI: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textApproveForm,
                id: ctrlForm.parameters.messages.idApproveForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;

                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.UnlockedComments();
                    ctrlForm.changeState(ctrlForm.parameters.states[9]);
                    ctrlForm.GetApplicant();
                    return true;
                }
            },
            {
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textRejectGlobal,
                id: ctrlForm.parameters.messages.idRejectGlobal,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.changeState(ctrlForm.parameters.states[8]);
                    ctrlForm.parameters.StateFinish = ctrlForm.parameters.states[7];
                    spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
                    return true;
                }
            },

            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
        ctrlForm.loadEdit(actions, ctrlForm.validateUserRevisionArea);

    },
    loadArquitectura: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textEditForm,
                id: ctrlForm.parameters.messages.idApproveForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;

                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.UnlockedComments();
                    ctrlForm.changeState(ctrlForm.parameters.states[11]);
                    ctrlForm.parameters.StateFinish = ctrlForm.parameters.states[9];

                    return true;
                }
            },
            {
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textRejectGlobal,
                id: ctrlForm.parameters.messages.idRejectGlobal,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.changeState(ctrlForm.parameters.states[10]);
                    ctrlForm.parameters.StateFinish = ctrlForm.parameters.states[9];
                    spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
                    return true;
                }
            },

            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
        ctrlForm.loadEdit(actions, ctrlForm.validateUserArquitectura);

    },

    loadBusiness: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textApproveForm,
                id: ctrlForm.parameters.messages.idApproveForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.UnlockedComments();
                    ctrlForm.VerificarRevision();
                    return true;
                }
            },
            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionApprove;
        ctrlForm.loadEdit(actions, ctrlForm.validateApplyUser);

    },
    loadEditApplicant: function() {
        var actions = [{
                type: spForms.parameters.updateAction,
                name: ctrlForm.parameters.messages.textEditForm,
                id: ctrlForm.parameters.messages.idEditForm,
                success: function() {
                    ctrlForm.callFlowService();
                    return false;
                },
                error: function() {
                    return true;
                },
                preSave: function() {
                    ctrlForm.changeState(ctrlForm.parameters.states[1]);
                    return true;
                }
            },
            {
                type: spForms.parameters.cancelAction,
                name: ctrlForm.parameters.messages.textCancelForm,
                id: ctrlForm.parameters.messages.idCancelForm,
                callback: null
            },
        ];
        ctrlForm.parameters.description = ctrlForm.parameters.descriptionEdit;
        ctrlForm.loadEdit(actions, function() {
            if (ctrlForm.validateEditUser()) {
                ctrlForm.hideComments();
                var ctrlComments = document.getElementById(ctrlForm.parameters.columns.comments.staticName);
                if (ctrlComments != null) {
                    ctrlComments.value = "";
                }
            }
        });
    },
    loadEdit: function(actions, callBack) {
        ctrlForm.getUsersOwners();
        ctrlForm.getUsersBusiness();
        ctrlForm.getUsersAreas();
        ctrlForm.getUsersAreasTI();
        ctrlForm.getUsersArquitectura();
        spForms.options = {
            actions: actions,
            description: ctrlForm.parameters.description,
            contentTypeName: ctrlForm.parameters.contentTypeName,
            containerId: ctrlForm.parameters.containerId,
            allowAttachments: ctrlForm.parameters.allowAttachments,
            requiredAttachments: ctrlForm.parameters.requiredAttachments,
            maxAttachments: ctrlForm.parameters.maxAttachments,
            minAttachments: ctrlForm.parameters.minAttachments,
            maxSizeInMB: ctrlForm.parameters.maxSizeInMB,
            successLoadEditForm: function() {
                ctrlForm.global.createdBy = spForms.currentItem.Author.EMail;
                ctrlForm.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(ctrlForm.parameters.formats.dateFormat);
                ctrlForm.formIsLoaded = true;
                ctrlForm.clickSelect();
                ctrlForm.GetCurrentSelect();
                ctrlForm.commentsEmpty();
                if (ctrlForm.global.formName == "CorregirSolicitud") {
                    ctrlForm.IniForm();
                }
                if (ctrlForm.global.formName == "Business") {
                    ctrlForm.commentsEmpty();
                }
                if (typeof callBack == "function") {
                    callBack();
                }
            }
        };
        spForms.loadEditForm();

    },
    loadNew: function() {
        ctrlForm.getUsersOwners();
        ctrlForm.getUsersBusiness();
        ctrlForm.global.createdBy = sputilities.contextInfo().userEmail;
        ctrlForm.global.created = moment.utc(new Date()).format(ctrlForm.parameters.formats.dateFormat);
   		sputilities.callFunctionOnLoadBody(ctrlForm.parameters.functionLoadMyProperties);
        ctrlForm.loadNewForm();
    },
    loadNewForm: function() {
        spForms.options = {
            actions: [{
                    type: spForms.parameters.saveAction,
                    name: ctrlForm.parameters.messages.textSaveForm,
                    id: ctrlForm.parameters.messages.idSaveForm,
                    success: function() {
                        ctrlForm.callFlowService();

                        return false;
                    },
                    error: function() {
                        return true;
                    },
                    preSave: function() {
                        ctrlForm.changeState(ctrlForm.parameters.states[0]);
                        ctrlForm.GetApplicant();
                        return true;
                    }
                },
                {
                    type: spForms.parameters.cancelAction,
                    name: ctrlForm.parameters.messages.textCancelForm,
                    id: ctrlForm.parameters.messages.idCancelForm,
                    callback: null
                },
            ],
            description: ctrlForm.parameters.description,
            contentTypeName: ctrlForm.parameters.contentTypeName,
            containerId: ctrlForm.parameters.containerId,
            allowAttachments: ctrlForm.parameters.allowAttachments,
            requiredAttachments: ctrlForm.parameters.requiredAttachments,
            maxAttachments: ctrlForm.parameters.maxAttachments,
            minAttachments: ctrlForm.parameters.minAttachments,
            maxSizeInMB: ctrlForm.parameters.maxSizeInMB,
            successLoadNewForm: function() {
                ctrlForm.formIsLoaded = true;
                ctrlForm.hideComments();
                ctrlForm.IniForm();
                ctrlForm.stateChange();
                ctrlForm.clickSelect();
            }
        };
        spForms.loadNewForm();
    },
    	loadMyProperties:function(){
		sputilities.getUserProfileProperties(sputilities.contextInfo().userEmail,
			function(userInfo){
				var objUser = utilities.convertToJSON(userInfo);
				if(objUser != null){
					var displayName = objUser.d.DisplayName;
					var email = objUser.d.Email;
					var cellPhone = ctrlForm.findProperty(objUser,ctrlForm.parameters.cellPhone);
					var validateFormIsLoaded = setInterval(function(){
						if(ctrlForm.formIsLoaded){
							ctrlForm.setValueToControl(ctrlForm.parameters.columns.displayName.staticName,displayName);
							clearInterval(validateFormIsLoaded);
						}
					},500);
				}
			},
			function(xhr){
				console.log(xhr)
			}
		);
	},
	setValueToControl:function(staticName,value){
		if(value != null){
			var ctrl = document.getElementById(staticName);
			if(ctrl != null){
				ctrl.value = value;
			}
		}
	},
	findProperty:function(objUser,name){
		var properties = objUser.d.UserProfileProperties.results;
		for(var i = 0; i < properties.length; i++){
			var property = properties[i];
			if(property.Key == name){
				return property.Value;
			}
		}
	},

    clickSelect: function(ctrl) {
        var CurrentType = document.getElementById(ctrlForm.parameters.ID.TipoSolicitud).value;
        if (CurrentType == 2 || CurrentType == 9) {
            var spFormField = ctrlForm.fieldType[CurrentType].Actualizar();
            ctrlForm.GetCurrentSelect();
            ctrlForm.GetValidation(CurrentType);
        } else {
            ctrlForm.GetCurrentSelect();
            ctrlForm.GetValidation(CurrentType);
            var CurrentType = 0
            var spFormField = ctrlForm.fieldType[CurrentType].Actualizar();
        }
    },
    IniForm: function() {
        var OnclickNametype = document.getElementById(ctrlForm.parameters.ID.TipoSolicitud);
        OnclickNametype.setAttribute(ctrlForm.parameters.Functions.Onchange, ctrlForm.parameters.Functions.functionOnchange);
    },
    ShowFields: function() { //Tipo de solicitud flujo de trabajo
        document.querySelector(ctrlForm.parameters.ID.TipoFlujo).style.display = ctrlForm.parameters.style.Block;
        document.querySelector(ctrlForm.parameters.ID.NumeroAprobadores).style.display = ctrlForm.parameters.style.Block;
        document.querySelector(ctrlForm.parameters.ID.Ejecucion).style.display = ctrlForm.parameters.style.Block;
        document.querySelector(ctrlForm.parameters.ID.NumeroItems).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.Estimado).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.Crecimiento).style.display = ctrlForm.parameters.style.None;
        spForms.loadedFields.NumeroAprobadores.required = true;
    },
    ShowFieldsLibrary: function() { //Tipo de solicitud biblioteca
        document.querySelector(ctrlForm.parameters.ID.NumeroItems).style.display = ctrlForm.parameters.style.Block;
        document.querySelector(ctrlForm.parameters.ID.Estimado).style.display = ctrlForm.parameters.style.Block;
        document.querySelector(ctrlForm.parameters.ID.Crecimiento).style.display = ctrlForm.parameters.style.Block;
        spForms.loadedFields.NumerosItems.required = true;
        spForms.loadedFields.Tama_x00f1_oEstimado.required = true;
        spForms.loadedFields.CrecimientoAnual.required = true;
    },
    HideAll: function() {
        document.querySelector(ctrlForm.parameters.ID.TipoFlujo).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.NumeroAprobadores).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.Ejecucion).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.NumeroItems).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.Estimado).style.display = ctrlForm.parameters.style.None;
        document.querySelector(ctrlForm.parameters.ID.Crecimiento).style.display = ctrlForm.parameters.style.None;
        spForms.loadedFields.NumeroAprobadores.required = false;
        spForms.loadedFields.NumerosItems.required = false;
        spForms.loadedFields.Tama_x00f1_oEstimado.required = false;
        spForms.loadedFields.CrecimientoAnual.required = false;
    },
    UnlockedComments: function() {
        spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = false;
        var uno = document.querySelector(ctrlForm.parameters.ID.Comments);
        console.log(uno)
        uno.removeAttribute(ctrlForm.parameters.style.Invalid);
    },
    stateChange: function() {
        spForms.loadedFields.EstadosPublicacion.value = ctrlForm.parameters.states[0];
    },
    commentsEmpty: function() {
        document.getElementById(ctrlForm.parameters.ID.IdComentarios).value = "";
    },
    GetCurrentSelect: function() {
            var selectValue = document.getElementById(ctrlForm.parameters.ID.TipoSolicitud).value;
            var selectOption = document.querySelectorAll(ctrlForm.parameters.ID.IdOption);
            for (var i = 0; i < selectOption.length; i++) {
                if (selectOption[i].value == selectValue) {
                    ctrlForm.parameters.NameCurrentSelect = selectOption[i].text
                    console.log(ctrlForm.parameters.NameCurrentSelect);
                }
            }
        
    },
    VerificarRevision: function() {
        var Revision = document.getElementById(ctrlForm.parameters.ID.IdRevision);
        var comentarios = document.getElementById(ctrlForm.parameters.ID.IdComentarios).value;
        spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
        if (comentarios != "") {
            if (Revision.checked != false) {
                ctrlForm.changeState(ctrlForm.parameters.states[5]);
            } else {
                spForms.loadedFields[ctrlForm.parameters.columns.comments.staticName].required = true;
                ctrlForm.changeState(ctrlForm.parameters.states[11]);
                ctrlForm.parameters.StateFinish = ctrlForm.parameters.states[3];
            }
        }
    },
    redirectCustomForm: function(name) {
        window.location.href = window.location.href.replace(ctrlForm.global.formName, name);
    }
}
ctrlForm.initialize();