﻿spForms.options={
	actions:[
			{
			type:spForms.parameters.updateAction,
			name:"Aprobar",
			id:"Aprobar",
			preSave:function(){
			var CurrentEstado = document.getElementById("EstadosCajaCompensacion");
			if(CurrentEstado.value == "1"){
			CurrentEstado.value = "2";
			}

			return true},
			success:function(){return true},
			error:function(){return true}
		},
				{
			type:spForms.parameters.updateAction,
			name:"Enviar a Corregir",
			id:"Corregir",
			preSave:function(){
			var CurrentEstado = document.getElementById("EstadosCajaCompensacion");
			if(CurrentEstado.value == "1"){
			CurrentEstado.value = "3";
			}
			return Cajas.validateReject();
			return true},
			success:function(){return true},
			error:function(){return true}
		},
		{
			type:spForms.parameters.updateAction,
			name:"Enviar",
			id:"Enviar",
			preSave:function(){
			var CurrentEstado = document.getElementById("EstadosCajaCompensacion");
			if(CurrentEstado.value == "3"){
			CurrentEstado.value = "1";
			}
			return true},
			success:function(){return true},
			error:function(){return true}
		},
		{
			type:spForms.parameters.updateAction,
			name:"Aplicar Novedad",
			id:"ANovedad",
			preSave:function(){
			var CurrentEstado = document.getElementById("EstadosCajaCompensacion");
			if(CurrentEstado.value == "2"){
			CurrentEstado.value = "4";
			}

			return true},
			success:function(){return true},
			error:function(){return true}
		},


		{
			type:spForms.parameters.cancelAction,
			name:"Cancelar",
			id:"cancelar",
			callback:""
		},
	],
	description:"Completa la información correspondiente",
	contentTypeName:"Cajas de compensacion",
	containerId:"spForm",
		allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:5,
  	minAttachments:1,

	successLoadEditForm:function(){
		Cajas.initialize();
	} 
};
spForms.loadEditForm();

var Cajas = {
	//create parameters
	parameters:{
	query:"?$Select=FlujoAsociado/Title,ResponsablesAbonos/EMail,ID&$expand=FlujoAsociado,ResponsablesAbonos&$filter=ID eq 5",
	queryNovedades:"?$Select=FlujoAsociado/Title,ResponsablesAbonos/EMail,ID&$expand=FlujoAsociado,ResponsablesAbonos&$filter=ID eq 8",
	listName: "ResponsablesTH",
	bodyItem:"<div class='contact'><p>Usted no tiene permisos sobre este formulario.</p></div>",
	TitlePopUp:"Caja de Compensación - Acceso Denegado ",

	},	
	fieldType:{
	 "0":{
    	Actualizar:function(){
    	Cajas.userResponsablesVentanillaForm();
    	var ObservacionesJefe = document.querySelector('div[sp-static-name="ComentariosCorreciones"]');
		ObservacionesJefe.style.display = "block";
    	var TextObservacionesTH = document.getElementById("ComentariosCorreciones");
		TextObservacionesTH.style.pointerEvents = "initial";
		TextObservacionesTH.style.color = "#444";
		document.getElementById("btn-Enviar").style.display ="none";
		document.getElementById("btn-ANovedad").style.display ="none";


 		//alert("ingreso a solicitado")
      }
    },
    	 "1":{
    	Actualizar:function(){
    	Cajas.userResponsablesVentanillaForm();
    	var ObservacionesJefe = document.querySelector('div[sp-static-name="ComentariosCorreciones"]');
		ObservacionesJefe.style.display = "block";
    	var TextObservacionesTH = document.getElementById("ComentariosCorreciones");
		TextObservacionesTH.style.pointerEvents = "initial";
		TextObservacionesTH.style.color = "#444";
		document.getElementById("btn-Enviar").style.display ="none";
		document.getElementById("btn-ANovedad").style.display ="none";
		document.getElementById("0-Beneficiarios").disabled=true;
		document.getElementById("1-Beneficiarios").disabled=true;
		document.getElementById("2-Beneficiarios").disabled=true;
		document.getElementById("3-Beneficiarios").disabled=true;
		var Base = document.getElementById('BasePersona')
		var BaseInputs =Base.getElementsByTagName('input')
		for(var i=0; i<BaseInputs.length;i++){
		var input=BaseInputs[i];
		input.disabled=true;
		}					

 		//alert("ingreso a solicitado")
      }
    },

     	 "2":{
    	Actualizar:function(){
    	Cajas.userResponsablesNovedadesForm();
    	document.getElementById("btn-Aprobar").style.display ="none";
		document.getElementById("btn-Corregir").style.display ="none";	
		document.getElementById("btn-Enviar").style.display ="none";	
		document.getElementById("0-Beneficiarios").disabled=true;
		document.getElementById("1-Beneficiarios").disabled=true;
		document.getElementById("2-Beneficiarios").disabled=true;
		document.getElementById("3-Beneficiarios").disabled=true;
		var Base = document.getElementById('BasePersona')
		var BaseInputs =Base.getElementsByTagName('input')
		for(var i=0; i<BaseInputs.length;i++){
		var input=BaseInputs[i];
		input.disabled=true;
		}					
   		//alert("ingreso a aplicar novedad")
      }
    },

    	 "3":{
    	Actualizar:function(){
    	var ObservacionesJefe = document.querySelector('div[sp-static-name="ComentariosCorreciones"]');
		ObservacionesJefe.style.display = "block";
    	document.getElementById("btn-Aprobar").style.display ="none";
		document.getElementById("btn-Corregir").style.display ="none";
		document.getElementById("btn-ANovedad").style.display ="none";		
		document.getElementById('NombrePersona1').style.pointerEvents = "initial"
		document.getElementById('DocumentoIdentidad').style.pointerEvents = "initial"
		document.getElementById('NumeroCelular').style.pointerEvents = "initial"
		document.getElementById('CorreoElectronico').style.pointerEvents = "initial"
		document.getElementById('NumeroRadicado').style.pointerEvents = "initial"
		document.getElementById('EstadosCajaCompensacion').style.pointerEvents = "initial"
		document.getElementById('NombrePersona1').style.color  = "#444"
		document.getElementById('DocumentoIdentidad').style.color  = "#444"
		document.getElementById('NumeroCelular').style.color  = "#444"
		document.getElementById('CorreoElectronico').style.color  = "#444"
		document.getElementById('NumeroRadicado').style.color  = "#444"
		document.getElementById('EstadosCajaCompensacion').style.color  = "#444"
   		//alert("ingreso a corregir")
      }
    },
     "4":{
    	Actualizar:function(){
    	document.getElementById("btn-Aprobar").style.display ="none";
		document.getElementById("btn-Corregir").style.display ="none";	
		document.getElementById("btn-Enviar").style.display ="none";
		document.getElementById("btn-cancelar").style.display ="none";
		document.getElementById("btn-ANovedad").style.display ="none";
		document.getElementById("0-Beneficiarios").disabled=true;
		document.getElementById("1-Beneficiarios").disabled=true;
		document.getElementById("2-Beneficiarios").disabled=true;
		document.getElementById("3-Beneficiarios").disabled=true;
		var Base = document.getElementById('BasePersona')
		var BaseInputs =Base.getElementsByTagName('input')
		for(var i=0; i<BaseInputs.length;i++){
		var input=BaseInputs[i];
		input.disabled=true;
		}					

 		//alert("ingreso a Novedad aplicada")
      }
    },

    

    },

	initialize:function(){
		try {
		var CurrentEstado = document.getElementById("EstadosCajaCompensacion").value;
		var spFormField = Cajas.fieldType[CurrentEstado].Actualizar()
						

				console.log("ingreso a ver")
						

			}
		catch (e){
			console.log(e);
		}
	},
		userResponsablesVentanillaForm:function(){
		try {
		var UserEmailCurrent = _spPageContextInfo.userEmail

			//adding parameters to query
		var linkQuery = String.format(Cajas.parameters.query);
		
			sputilities.getItems(Cajas.parameters.listName,linkQuery,function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				var Items = resultItems[0].ResponsablesAbonos.results
				var find = false; 
				for(var i=0;i<Items.length;i++){
				var nextItem = Items[i+1];
				var prevItem = Items[i-1];
				var Email = Items[i].EMail
				console.log(Email);
				if(Email == UserEmailCurrent){
				find = true;
				}
				}
				if(find){
				document.getElementById('spForm').style.display="block";
				console.log("usuario con permisos")
				}else{
				document.getElementById('spForm').style.display="block";
				document.getElementById("btn-Aprobar").style.display ="none";
				document.getElementById("btn-Corregir").style.display ="none";
				document.getElementById('spForm').style.display="block";
				var TextObservacionesTH = document.getElementById("ComentariosCorreciones");
				TextObservacionesTH.style.pointerEvents = "block";
				TextObservacionesTH.style.color = "#B1B1B1";
				
				Cajas.InitPopUp();						

				Cajas.InitPopUp();					
				}
				console.log(resultItems);			
			});


		}
		catch (e){
			console.log(e);
		}
	},
userResponsablesNovedadesForm:function(){
		try {
		var UserEmailCurrent = _spPageContextInfo.userEmail

			//adding parameters to query
		var linkQuery = String.format(Cajas.parameters.queryNovedades);
		
			sputilities.getItems(Cajas.parameters.listName,linkQuery,function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				var Items = resultItems[0].ResponsablesAbonos.results
				var find = false; 
				for(var i=0;i<Items.length;i++){
				var nextItem = Items[i+1];
				var prevItem = Items[i-1];
				var Email = Items[i].EMail
				console.log(Email);
				if(Email == UserEmailCurrent){
				find = true;
				}
				}
				if(find){
				document.getElementById('spForm').style.display="block";
				console.log("usuario con permisos")
				}else{
				document.getElementById('spForm').style.display="block";
		    	document.getElementById("btn-Aprobar").style.display ="none";
				document.getElementById("btn-Corregir").style.display ="none";
				document.getElementById("btn-ANovedad").style.display ="none";
				var ObservacionesVentanilla = document.querySelector('div[sp-static-name="ComentariosCorreciones"]');
				ObservacionesVentanilla.style.display = "block";
				var TextObservacionesTH = document.getElementById("ComentariosCorreciones");
				TextObservacionesTH.style.pointerEvents = "none"
				TextObservacionesTH.style.color = "#B1B1B1";
				
				Cajas.InitPopUp();					
				}
				console.log(resultItems);			
			});


		}
		catch (e){
			console.log(e);
		}
	},
	validateReject:function(){
	
		var row = document.querySelector("[sp-static-name='ComentariosCorreciones']");
		var comments = row.querySelector("textarea");
		var isValid = true;
		if(comments.value != "")
		{
			row.removeAttribute('sp-invalid');			
		}
		else
		{
			row.setAttribute('sp-invalid',"Los comentarios son obligatorios");			
		}
		return isValid;
	
	},


	InitPopUp:function(){
		try {
		
				sputilities.showModalDialog(Cajas.parameters.TitlePopUp,null,null,Cajas.parameters.bodyItem,function(data){
				console.log("ingresa al pop up")				
			});
		}
		catch (e){
			console.log(e);
		}
	},


	
}