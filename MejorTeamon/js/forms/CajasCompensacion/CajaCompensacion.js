﻿spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:"Enviar",
			id:"enviar",
			success:function(){return true;},
			error:function(){},
			preSave:function(){
		
			return true;
			
			}

		},
		{
			type:spForms.parameters.cancelAction,
			name:"Cancelar",
			id:"cancelar",
			callback:""
		},
	],
	description:"Completa la información correspondiente",
	contentTypeName:"Caja Compensacion",
	containerId:"spForm",
	allowAttachments:true,
  	requiredAttachments:true,
  	maxAttachments:5,
  	minAttachments:1,

	successLoadNewForm:function(){
		Cajas.initialize();
		Cajas.loadUserInfo();
		Cajas.validate();

	} 
};
spForms.loadNewForm();

var Cajas = {
	//create parameters
	parameters:{
	fields:[{field:"NombrePersona1"},{field:"NumeroCelular"},{field:"CorreoElectronico"},{field:"ValorCredito"},{field:"AplicacionCredito"},{field:"MedicinaPrepagada"},{field:"NumeroDependiente"}],		
	},	
	initialize:function(){
		try {
		//alert("ingreso")				
			}
		catch (e){
			console.log(e);
		}
	},
	loadUserInfo:function(){
		try {
			//running the query with parameters
			sputilities.getMyUserProfileProperties("",function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d;
				Cajas.parameters.resultItems = resultItems;
				Cajas.setValuesUserInfo();
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	setValuesUserInfo:function(){
	  	document.getElementById(Cajas.parameters.fields[0].field).value = Cajas.parameters.resultItems.DisplayName ? Cajas.parameters.resultItems.DisplayName: "";
	  	document.getElementById(Cajas.parameters.fields[1].field).value = Cajas.parameters.resultItems.CellPhone ? Cajas.parameters.resultItems.CellPhone: "";
	  	document.getElementById(Cajas.parameters.fields[2].field).value = Cajas.parameters.resultItems.Email ? (Cajas.parameters.resultItems.Email).toLowerCase(): "";
	},
	validate:function(input){
		var input = document.getElementById('CorreoElectronico');
			if(input != null){
		input.addEventListener('change',function(event){
		var email = this.value;
		  if (Cajas.validateEmail(email)) {
		  console.log("email valido")
		  var uno=document.getElementsByClassName('form-group row')[4]
   			uno.removeAttribute('sp-invalid')

		  
   			  		} 	
   			  		
   			  		else {
   			console.log("email invalido")
   			var uno=document.getElementsByClassName('form-group row')[4]
   			uno.setAttribute('sp-invalid','Formato de correo no valido.')
   			  			}
  return false;
  			console.log(this.value);
  	});
		}
	},
	
	validateEmail:function(email){
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 	 return re.test(email);
 	 	},


					
}