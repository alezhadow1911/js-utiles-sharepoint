﻿var certificationForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx",
			"htmlNotAllowed":"<div class='form-group row'><label>{0}</label></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>"
		},
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']",
			typeRequest: "div[sp-static-name='TipoCertificacion'] > div > div > select",
			FechaFinViaje:"[sp-static-name='FechaFinViaje']",
			FechaInicioViaje:"[sp-static-name='FechaInicioViaje']",
			PaisViaje:"[sp-static-name='PaisViaje']",
			CargoCertificar:"[sp-static-name='CargoCertificar']",
			Comentarios:"[sp-static-name='ComentariosTH']",
			ComentariosJefe:"[sp-static-name='ComentariosJefe']"		
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico',	
			Comentarios:"ComentariosTH",
			ComentariosJefe:"ComentariosJefe",
			Estado:"EstadoCertificacion"		
		},		
		controlsHtml:{
            Attachment:"sp-forms-add-attachment",
            Enviar:"btn-enviar",            
			Aprobar:"btn-aprobar",
			Corregir:"btn-corregir",
			Aplicar:"btn-aplicar",
			Rechazar:"btn-rechazar",
			classDelete:".delete-doc",
			classGroup:".form-group.row",
			colorDisable:"#B1B1B1",
			borderDisable:"#E1E1E1",
			iconTH:"ico-help-ComentariosTH",
			iconJefe:"ico-help-ComentariosJefe"
		},
		bodyItem:"Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior.",
		TitlePopUp:"Acceso Denegado",
    	htmlRequiered:{block:"block",none:"none"},	            
		invalid:"sp-invalid",
		selectorFormContent:".form-content",
		userProperties:"CellPhone", 
		errorInvalid:"El correo electrónico no tiene el formato correcto",
        codeStates:{Solicitado:"0",Ventanilla:"1",Novedades:"2",Aplicada:"3",Corregir:"4",Jefe:"5",Rechazado:"6"},		
		formsParameters:{
			title:"Corregir Solicitud Certificación",
			description:"Completa la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			}		
		},
		formsParameters:{
			title:"Solicitud Certificación",
			description:"Corrige la información correspondiente",
			titleApprove:"Aprobar/ Corregir Solicitud Certificación",
			titleAplicate:"Aplicar Solicitud Certificación",
			descriptionApprove:"Completa la información correspondiente, para aprobar o rechazar la solicitud",
			descriptionAplicate:"Revisa la información de la solicitud, para aplicar la novedad",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Certificaciones",
			containerId:"container-form"			
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud",
			"AprobarJefe":"Aprobar Jefe Solicitud",
			"AplicarSolicitud":"Aplicar Solicitud",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Solicitudes pendientes Novedades":"Vista Novedades",
			"Solicitudes pendientes Ventanilla TH":"Vista Ventanilla TH"
		},
		flowParameters:{
			urlFlow:"https://prod-09.westus.logic.azure.com:443/workflows/c947fc025bcd461cbc2b076f91653caa/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=tDwp2hRmnHaPaxQ5VnjcZ9t-0kABL_cPV8Qfs7buJoA",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",
	   		approveBossUrl:"",
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Certificaciones Ventanilla TH",
	    	applyUsers:"Certificaciones Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:13,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Certificaciones"
		},
		global:{		
			createdBy:null,
			created:null
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
		
		var objectSelect = document.querySelector(certificationForm.parameters.fieldsHTML.typeRequest);
		var textSelected = objectSelect.options[objectSelect.selectedIndex].value;
	
	    certificationForm.parameters.flowParameters.approveUrl = String.format(certificationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(certificationForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    certificationForm.parameters.flowParameters.approveBossUrl = String.format(certificationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(certificationForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    certificationForm.parameters.flowParameters.applyUrl = String.format(certificationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(certificationForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    certificationForm.parameters.flowParameters.editUrl = String.format(certificationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(certificationForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    certificationForm.parameters.flowParameters.listViewApply = String.format(certificationForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(certificationForm.parameters.forms)[6]);
	    certificationForm.parameters.flowParameters.listViewApprove= String.format(certificationForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(certificationForm.parameters.forms)[7]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":certificationForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"fechaInicioViaje": moment.utc(spForms.loadedFields.FechaInicioViaje.value).format(certificationForm.parameters.formats.dateFormat),
				"fechaFinViaje": moment.utc(spForms.loadedFields.FechaFinViaje.value).format(certificationForm.parameters.formats.dateFormat),
				"paisViaje":spForms.loadedFields.PaisViaje.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":certificationForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosTH.value,
		        "comentariosJefe":spForms.loadedFields.ComentariosJefe.value,
		        "tipoCertificacion":textSelected
		    },
		    "state":spForms.loadedFields.EstadoCertificacion.value,
		    "approveUsers":certificationForm.parameters.flowParameters.approveUsers,
		    "applyUsers":certificationForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":certificationForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":certificationForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":certificationForm.parameters.flowParameters.listNameAssing,
		    "editUrl":certificationForm.parameters.flowParameters.editUrl,
		    "approveUrl":certificationForm.parameters.flowParameters.approveUrl,
		    "applyUrl":certificationForm.parameters.flowParameters.applyUrl,
		    "approveBossUrl":certificationForm.parameters.flowParameters.approveBossUrl,
		    "listViewApply":certificationForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":certificationForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	certificationForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:certificationForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){
		
		spForms.loadEditForm();
	},
	validateFormat:function(){
		var rowEmail = document.querySelector(certificationForm.parameters.fieldsHTML.CorreoElectronico);
		var email = document.getElementById(certificationForm.parameters.internalNameFields.CorreoElectronico);
		var isValid = true;		
		if(email != null && rowEmail!= null)
		{			
			if(certificationForm.validateEmail(email.value) || email.value == "")
			{
				rowEmail.removeAttribute(certificationForm.parameters.invalid);
			}
			else
			{
				rowEmail.setAttribute(certificationForm.parameters.invalid,certificationForm.parameters.errorInvalid);
			}
		}
		else
		{
			isValid=false;
		}
		
		var state = document.getElementById(certificationForm.parameters.internalNameFields.Estado);
		if(state != null)
		{
			state.value = certificationForm.parameters.codeStates.Ventanilla;
		}
		else
		{
			isValid = false;
		}
		return isValid;

	},
	validateEmail: function (email){
		 var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);
	
	},loadTypeRequest:function(){
		
		var objectSelect = document.querySelector(certificationForm.parameters.fieldsHTML.typeRequest);
		var textSelected = objectSelect.options[objectSelect.selectedIndex].value;
		
		if (textSelected == "1"){
				
			document.querySelector(certificationForm.parameters.fieldsHTML.CargoCertificar).style.display = "none";
			document.querySelector(certificationForm.parameters.fieldsHTML.FechaInicioViaje).style.display = "block";
			document.querySelector(certificationForm.parameters.fieldsHTML.FechaFinViaje).style.display = "block";
			document.querySelector(certificationForm.parameters.fieldsHTML.PaisViaje).style.display = "block";
			
		}else if (textSelected == "5")
		{
			
			document.querySelector(certificationForm.parameters.fieldsHTML.CargoCertificar).style.display = "block";
			document.querySelector(certificationForm.parameters.fieldsHTML.FechaInicioViaje).style.display = "none";
			document.querySelector(certificationForm.parameters.fieldsHTML.FechaFinViaje).style.display = "none";
			document.querySelector(certificationForm.parameters.fieldsHTML.PaisViaje).style.display = "none";
		
		}else{
		
			document.querySelector(certificationForm.parameters.fieldsHTML.CargoCertificar).style.display = "none";
			document.querySelector(certificationForm.parameters.fieldsHTML.FechaInicioViaje).style.display = "none";
			document.querySelector(certificationForm.parameters.fieldsHTML.FechaFinViaje).style.display = "none";
			document.querySelector(certificationForm.parameters.fieldsHTML.PaisViaje).style.display = "none";
		}
	}
	,addOnChangeTypeRequest: function(){
				
		 document.querySelector(certificationForm.parameters.fieldsHTML.typeRequest).onchange = function() {

			var objectSelect = document.querySelector(certificationForm.parameters.fieldsHTML.typeRequest);
			var textSelected = objectSelect.options[objectSelect.selectedIndex].text;
			var country = document.querySelector(certificationForm.parameters.fieldsHTML.PaisViaje);
			var startDate = document.querySelector(certificationForm.parameters.fieldsHTML.FechaInicioViaje);
			var finishDate = document.querySelector(certificationForm.parameters.fieldsHTML.FechaFinViaje);
			var titleJob = document.querySelector(certificationForm.parameters.fieldsHTML.CargoCertificar);

			if(textSelected == "Certificación Embajada por Misión Compañía")
			{
				certificationForm.disableInputs(country,'block');
				certificationForm.disableInputs(startDate,'block');
				certificationForm.disableInputs(finishDate,'block');
				certificationForm.disableInputs(titleJob,'none');

				
			}
			else if (textSelected == "Certificación Específicas con Funciones")
			{
				certificationForm.disableInputs(country,'none');
				certificationForm.disableInputs(startDate,'none');
				certificationForm.disableInputs(finishDate,'none');
				certificationForm.disableInputs(titleJob,'block');
				
			}
			else
			{
				certificationForm.disableInputs(country,'none');
				certificationForm.disableInputs(startDate,'none');
				certificationForm.disableInputs(finishDate,'none');
				certificationForm.disableInputs(titleJob,'none');
			}
		}
	},
	loadResponsableState:function(){
	
		try
		{
			var currentUserId =_spPageContextInfo.userId;		
		
			var find = false;
			var state = document.getElementById(certificationForm.parameters.internalNameFields.Estado);
			if(state != null)
			{
				if(spForms.currentItem.AuthorId == currentUserId){
					find = true;
				}
								
				if(state.value != certificationForm.parameters.codeStates.Corregir)
				{				
					certificationForm.showPermissions();
				}
				else
				{
					if(!find)
					{
						certificationForm.showPermissions();
					}
				}
				
			}
		}
		catch(e){
			console.log(e);
		}	
	},
	showPermissions:function(){
		var html = String.format(certificationForm.parameters.formats.htmlNotAllowed,certificationForm.parameters.bodyItem);
		var formContent = document.querySelector(certificationForm.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}						
	},
	disableInputs:function(){
	
		var rows = document.querySelectorAll(certificationForm.parameters.controlsHtml.classGroup);
		
		if(rows != null)
		{
			for(var i=0;i<rows.length;i++)
			{
				rows[i].style.pointerEvents = certificationForm.parameters.htmlRequiered.none;
			}
		}
	}
}
spForms.options={
	actions:[
		{
			type:spForms.parameters.updateAction,
			name:certificationForm.parameters.formsParameters.buttons.Send.Text,
			id:certificationForm.parameters.formsParameters.buttons.Send.Id,
			success:function (){
				certificationForm.callFlowService();
				return false;
			},
			error:function (){},
			preSave: function (){
				return certificationForm.validateFormat()
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:certificationForm.parameters.formsParameters.buttons.Cancel.Text,
			id:certificationForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:certificationForm.parameters.formsParameters.title,
	description:certificationForm.parameters.formsParameters.description,
	contentTypeName:certificationForm.parameters.formsParameters.contentType,
	containerId:certificationForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:false,
  	maxSizeInMB:3,
	successLoadEditForm:function(){
		certificationForm.loadResponsableState();
		certificationForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
		certificationForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(certificationForm.parameters.formats.dateFormat);
		certificationForm.loadTypeRequest();
	} 
};
certificationForm.initialize();