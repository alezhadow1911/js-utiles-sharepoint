﻿var certificationForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx"
		},
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']",
			typeRequest: "div[sp-static-name='TipoCertificacion'] > div > div > select",
			FechaFinViaje:"[sp-static-name='FechaFinViaje']",
			FechaInicioViaje:"[sp-static-name='FechaInicioViaje']",
			PaisViaje:"[sp-static-name='PaisViaje']",
			CargoCertificar:"[sp-static-name='CargoCertificar']"
		},
		labelsfielsHTML:{
			FechaFinViaje:"[sp-static-name='FechaFinViaje'] label",
			FechaInicioViaje:"[sp-static-name='FechaInicioViaje'] label",
			PaisViaje:"[sp-static-name='PaisViaje'] label",
			CargoCertificar:"[sp-static-name='CargoCertificar'] label"
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico',
			FechaFinViaje:"FechaFinViaje",
			FechaInicioViaje:"FechaInicioViaje",
			PaisViaje:"PaisViaje",
			CargoCertificar:"CargoCertificar"			
		},
		resultItems:null,
		invalid:"sp-invalid",
		requiredTag:"<i>*</i>",
		userProperties:"CellPhone", 
		errorInvalid:"El correo electrónico no tiene el formato correcto",
		errorInvalidNotText:"Esto no puede estar en blanco",
		errorInvalidDates:"La fecha de inicio no debe ser mayor a la fecha fin",
		loadUserInfo:"certificationForm.loadUserInfo",
		formsIsLoaded:false,
		formsParameters:{
			title:"Solicitudes para certificaciones",
			description:"Completa la información correspondiente",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Certificaciones",
			containerId:"container-form"			
		},
		flowParameters:{
			urlFlow:"https://prod-09.westus.logic.azure.com:443/workflows/c947fc025bcd461cbc2b076f91653caa/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=tDwp2hRmnHaPaxQ5VnjcZ9t-0kABL_cPV8Qfs7buJoA",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",
	   		approveBossUrl:"",
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Certificaciones Ventanilla TH",
	    	applyUsers:"Certificaciones Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:11,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Certificaciones"
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"Aprobar Solicitud Ventanilla TH",
			"AprobarJefe":"Aprobar Jefe Inmediato",
			"AplicarSolicitud":"Aplicar Novedad",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Solicitudes pendientes Novedades":"Vista Novedades",
			"Solicitudes pendientes Ventanilla TH":"Vista Ventanilla TH"
		},
		global:{
			createdBy:null,
			created:null
		}
	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
		
		var objectSelect = document.querySelector(certificationForm.parameters.fieldsHTML.typeRequest);
		var textSelected = objectSelect.options[objectSelect.selectedIndex].value;
	
	    certificationForm.parameters.flowParameters.approveUrl = String.format(certificationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(certificationForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    certificationForm.parameters.flowParameters.approveBossUrl = String.format(certificationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(certificationForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    certificationForm.parameters.flowParameters.applyUrl = String.format(certificationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(certificationForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    certificationForm.parameters.flowParameters.editUrl = String.format(certificationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(certificationForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    certificationForm.parameters.flowParameters.listViewApply = String.format(certificationForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(certificationForm.parameters.forms)[6]);
	    certificationForm.parameters.flowParameters.listViewApprove= String.format(certificationForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(certificationForm.parameters.forms)[7]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":certificationForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"fechaInicioViaje": moment.utc(spForms.loadedFields.FechaInicioViaje.value).format(certificationForm.parameters.formats.dateFormat),
				"fechaFinViaje": moment.utc(spForms.loadedFields.FechaFinViaje.value).format(certificationForm.parameters.formats.dateFormat),
				"paisViaje":spForms.loadedFields.PaisViaje.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":certificationForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosTH.value,
		        "comentariosJefe":spForms.loadedFields.ComentariosJefe.value,
		        "tipoCertificacion":textSelected
		    },
		    "state":spForms.loadedFields.EstadoCertificacion.value,
		    "approveUsers":certificationForm.parameters.flowParameters.approveUsers,
		    "applyUsers":certificationForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":certificationForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":certificationForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":certificationForm.parameters.flowParameters.listNameAssing,
		    "editUrl":certificationForm.parameters.flowParameters.editUrl,
		    "approveUrl":certificationForm.parameters.flowParameters.approveUrl,
		    "applyUrl":certificationForm.parameters.flowParameters.applyUrl,
		    "approveBossUrl":certificationForm.parameters.flowParameters.approveBossUrl,
		    "listViewApply":certificationForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":certificationForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	certificationForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:certificationForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){
		sputilities.callFunctionOnLoadBody(certificationForm.parameters.loadUserInfo);
		spForms.loadNewForm();
	},
	
	loadUserInfo:function(){
		try {
			//running the query with parameters
			sputilities.getMyUserProfileProperties("",function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d;
				certificationForm.parameters.resultItems = resultItems;
				var ctrlInterval = setInterval(function(){
					if(certificationForm.parameters.formsIsLoaded)
					{
						certificationForm.setValuesUserInfo();
						certificationForm.addOnChangeTypeRequest();
						clearInterval(ctrlInterval);
					}
				},500);
			},
			function(xhr){
				console.log(xhr)
			});
		}catch (e){
			console.log(e);
		}
	},
	setValuesUserInfo:function(){
	
		if(certificationForm.parameters.resultItems != null)
		{
		  	var name = document.getElementById(certificationForm.parameters.internalNameFields.NombrePersona);
		  	var email = document.getElementById(certificationForm.parameters.internalNameFields.CorreoElectronico);
		  	var cellPhone = document.getElementById(certificationForm.parameters.internalNameFields.NumeroCelular);
		  	if(name != null){
		  		name.value = certificationForm.parameters.resultItems.DisplayName ? certificationForm.parameters.resultItems.DisplayName: "";
		  	}
		  	
		  	if(email != null){
		  		email.value = certificationForm.parameters.resultItems.Email ? (certificationForm.parameters.resultItems.Email).toLowerCase(): "";
		  	}
		  	
		  	if(cellPhone != null){
		  	
		  		var userProperties = certificationForm.parameters.resultItems.UserProfileProperties.results;
		  	
			  	for(var i=0;i<userProperties.length;i++)
			  	{
			  		if(userProperties[i].Key == certificationForm.parameters.userProperties)
			  		{
			  		 	cellPhone.value =  userProperties[i].Value;
			  		 	break;
			  		}
			  	}
			}
	  	}
	},
	validateFormat:function(){
		var rowEmail = document.querySelector(certificationForm.parameters.fieldsHTML.CorreoElectronico);
		var email = document.getElementById(certificationForm.parameters.internalNameFields.CorreoElectronico);
		var isValid = true;		
		if(email != null && rowEmail!= null)
		{			
			if(certificationForm.validateEmail(email.value) || email.value == "")
			{
				rowEmail.removeAttribute(certificationForm.parameters.invalid);
			}
			else
			{
				rowEmail.setAttribute(certificationForm.parameters.invalid,certificationForm.parameters.errorInvalid);
			}
		}
		else
		{
			isValid=false;
		}
		return isValid;
		
	},
	validateEmail: function (email){
		 var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);
	
	},
	addOnChangeTypeRequest: function(){
				
		 document.querySelector(certificationForm.parameters.fieldsHTML.typeRequest).onchange = function() {

			var objectSelect = document.querySelector(certificationForm.parameters.fieldsHTML.typeRequest);
			var textSelected = objectSelect.options[objectSelect.selectedIndex].text;
			var country = document.querySelector(certificationForm.parameters.fieldsHTML.PaisViaje);
			var startDate = document.querySelector(certificationForm.parameters.fieldsHTML.FechaInicioViaje);
			var finishDate = document.querySelector(certificationForm.parameters.fieldsHTML.FechaFinViaje);
			var titleJob = document.querySelector(certificationForm.parameters.fieldsHTML.CargoCertificar);

			if(textSelected == "Certificación Embajada por Misión Compañía")
			{
				certificationForm.disableInputs(country,'block');
				certificationForm.disableInputs(startDate,'block');
				certificationForm.disableInputs(finishDate,'block');
				certificationForm.disableInputs(titleJob,'none');

				
			}
			else if (textSelected == "Certificación Específicas con Funciones")
			{
				certificationForm.disableInputs(country,'none');
				certificationForm.disableInputs(startDate,'none');
				certificationForm.disableInputs(finishDate,'none');
				certificationForm.disableInputs(titleJob,'block');
				
			}
			else
			{
				certificationForm.disableInputs(country,'none');
				certificationForm.disableInputs(startDate,'none');
				certificationForm.disableInputs(finishDate,'none');
				certificationForm.disableInputs(titleJob,'none');
			}
		}
	},
	disableInputs: function(object,display){
		object.style.display = display;
	},
	validateRequired: function(){
		var isValid = true;
		var objectSelect = document.querySelector(certificationForm.parameters.fieldsHTML.typeRequest);
		var textSelected = objectSelect.options[objectSelect.selectedIndex].text;
		var country = document.getElementById(certificationForm.parameters.internalNameFields.PaisViaje);
		var startDate = document.getElementById(certificationForm.parameters.internalNameFields.FechaInicioViaje);
		var finishDate = document.getElementById(certificationForm.parameters.internalNameFields.FechaFinViaje);
		var titleJob = document.getElementById(certificationForm.parameters.internalNameFields.CargoCertificar);
		var countryElement = document.querySelector(certificationForm.parameters.fieldsHTML.PaisViaje);
		var startDateElement = document.querySelector(certificationForm.parameters.fieldsHTML.FechaInicioViaje);
		var finishDateELement = document.querySelector(certificationForm.parameters.fieldsHTML.FechaFinViaje);
		var titleJobElement = document.querySelector(certificationForm.parameters.fieldsHTML.CargoCertificar);
		
		var splitStartDate = startDate.value.split('/');
		var splitEndDate = finishDate.value.split('/');
	
			if(textSelected == "Certificación Embajada por Misión Compañía")
			{
				if (country.value == "")
				{
					countryElement.removeAttribute(certificationForm.parameters.invalid);
					countryElement.setAttribute(certificationForm.parameters.invalid,certificationForm.parameters.errorInvalidNotText);	
				}else
				{
					countryElement.removeAttribute(certificationForm.parameters.invalid);
				}
				
				if (startDate.value == "")
				{
					startDateElement.removeAttribute(certificationForm.parameters.invalid);
					startDateElement.setAttribute(certificationForm.parameters.invalid,certificationForm.parameters.errorInvalidNotText);	
				}else
				{
					startDateElement.removeAttribute(certificationForm.parameters.invalid);
				}
				
				if (finishDate.value == "")
				{	
					startDateElement.removeAttribute(certificationForm.parameters.invalid);
					finishDateELement.setAttribute(certificationForm.parameters.invalid,certificationForm.parameters.errorInvalidNotText);	

				}else
				{	
					finishDateELement.removeAttribute(certificationForm.parameters.invalid);
				}
				
				if (new Date(splitStartDate[1] + "/" + splitStartDate[0] + "/" + splitStartDate[2]) > new Date(splitEndDate[1] + "/" + splitEndDate[0] + "/" + splitEndDate[2]))
				{	
					startDateElement.removeAttribute(certificationForm.parameters.invalid);
					startDateElement.setAttribute(certificationForm.parameters.invalid,certificationForm.parameters.errorInvalidDates);	

				}else if((finishDateELement.getAttribute(certificationForm.parameters.invalid)) == (certificationForm.parameters.errorInvalidDates))
				{	
					startDateElement.removeAttribute(certificationForm.parameters.invalid);
				}
								
			}
			else if (textSelected == "Certificación Específicas con Funciones")
			{
				if (titleJob.value == "")
				{
					titleJobElement.setAttribute(certificationForm.parameters.invalid,certificationForm.parameters.errorInvalidNotText);	
	
				}else
				{
					titleJobElement.removeAttribute(certificationForm.parameters.invalid);
				}	
			}
			else
			{
				startDateElement.removeAttribute(certificationForm.parameters.invalid);
				finishDateELement.removeAttribute(certificationForm.parameters.invalid);
				titleJobElement.removeAttribute(certificationForm.parameters.invalid);
				countryElement.removeAttribute(certificationForm.parameters.invalid);
            }
		return isValid;
	}
}

spForms.options={
	actions:[
		{
			type:spForms.parameters.saveAction,
			name:certificationForm.parameters.formsParameters.buttons.Send.Text,
			id:certificationForm.parameters.formsParameters.buttons.Send.Id,
			success:function (){
				certificationForm.callFlowService();
				return false;
			},
			error:function (){},
			preSave: function (){
				return certificationForm.validateRequired();
			}
		},
		{
			type:spForms.parameters.cancelAction,
			name:certificationForm.parameters.formsParameters.buttons.Cancel.Text,
			id:certificationForm.parameters.formsParameters.buttons.Cancel.Id,
			callback:""
		}
	],	
	title:certificationForm.parameters.formsParameters.title,
	description:certificationForm.parameters.formsParameters.description,
	contentTypeName:certificationForm.parameters.formsParameters.contentType,
	containerId:certificationForm.parameters.formsParameters.containerId,
	allowAttachments:true,
  	requiredAttachments:false,
  	maxAttachments:20,
  	minAttachments:0,
  	maxSizeInMB:3,
	successLoadNewForm:function(){
		certificationForm.parameters.global.createdBy = sputilities.contextInfo().userEmail;
		certificationForm.parameters.global.created = moment.utc(new Date()).format(certificationForm.parameters.formats.dateFormat);
		certificationForm.parameters.formsIsLoaded = true;
		document.querySelector(certificationForm.parameters.labelsfielsHTML.PaisViaje).innerHTML += certificationForm.parameters.requiredTag;
		document.querySelector(certificationForm.parameters.labelsfielsHTML.FechaInicioViaje).innerHTML += certificationForm.parameters.requiredTag;
		document.querySelector(certificationForm.parameters.labelsfielsHTML.FechaFinViaje).innerHTML += certificationForm.parameters.requiredTag;
		document.querySelector(certificationForm.parameters.labelsfielsHTML.CargoCertificar).innerHTML += certificationForm.parameters.requiredTag;
	} 
};
certificationForm.initialize();