﻿var certificationForm = {
	parameters:{
		formats:{
			dateFormat:"DD/MM/YYYY",
			formFormat:"{0}//{1}{2}/{3}.aspx?ID={4}&source={5}",
			viewFormat:"{0}//{1}{2}/{3}.aspx",
			"htmlNotAllowed":"<div class='form-group row'><label>{0}</label></div>" +
				"<div class='row buttons-form'><input type='button' class='btn btn-form' id='btn-cancelar' value='Regresar' onclick='javascript:return spForms.cancelForm()'></div>"
		},
		query:"?$Select=FlujoAsociado/Title,ResponsablesAbonos/EMail,ID&$expand=FlujoAsociado,ResponsablesAbonos&$filter=Title eq '{0}'",
		listName:"ResponsablesTH",
		assingTo:{
			Ventanilla:"Certificaciones Ventanilla TH",
			Novedades:"Certificaciones Novedades"
		},
		fieldsHTML:{
			NombrePersona:"[sp-static-name='NombrePersona1']",
			NumeroCelular:"[sp-static-name='NumeroCelular']",
			CorreoElectronico:"[sp-static-name='CorreoElectronico']",
			Comentarios:"[sp-static-name='ComentariosTH']",
			ComentariosJefe:"[sp-static-name='ComentariosJefe']",
			CargoCertificar:"[sp-static-name='CargoCertificar']",
			FechaInicioViaje:"[sp-static-name='FechaInicioViaje']",
			FechaFinViaje:"[sp-static-name='FechaFinViaje']",
			PaisViaje:"[sp-static-name='PaisViaje']",
			typeRequest: "div[sp-static-name='TipoCertificacion'] > div > div > select"
		},
		internalNameFields:{			
			NombrePersona:'NombrePersona1',
			NumeroCelular:'NumeroCelular',
			CorreoElectronico:'CorreoElectronico',	
			Comentarios:"ComentariosTH",
			ComentariosJefe:"ComentariosJefe",
			Estado:"EstadoCertificacion"		
		},		
		controlsHtml:{
			Attachment:"sp-forms-add-attachment",
			Aprobar:"btn-aprobar",
			Corregir:"btn-corregir",
			Aplicar:"btn-aplicar",
			Rechazar:"btn-rechazar",
			classDelete:".delete-doc",
			classGroup:".form-group.row",
			colorDisable:"#B1B1B1",
			borderDisable:"#E1E1E1",
			iconTH:"ico-help-ComentariosTH",
			iconJefe:"ico-help-ComentariosJefe"
		},
		errorInvalid:{text:"Los comentarios son obligatorios",area:"textarea"},
		htmlRequiered:{label:"label",block:"block",none:"none"},		
		invalid:"sp-invalid",
		hidden:"sp-hidden",		
		resultItems:null,	
		codeStates:{Solicitado:"0",Ventanilla:"1",Novedades:"2",Aplicada:"3",Corregir:"4",Jefe:"5",Rechazado:"6"},	
		userProperties:"CellPhone",
		bodyItem:"Usted no tiene permisos sobre este formulario o la solicitud se encuentra en un estado posterior.",
		TitlePopUp:"Acceso Denegado",
		selectorFormContent:".form-content",
		formsParameters:{
			title:"",
			description:"",
			titleApprove:"Aprobar/ Corregir Solicitud Certificación",
			titleAplicate:"Aplicar Solicitud Certificación",
			descriptionApprove:"Completa la información correspondiente, para aprobar o rechazar la solicitud",
			descriptionAplicate:"Revisa la información de la solicitud, para aplicar la novedad",
			buttons:{
				Send:{Text:"Enviar",Id:"enviar"},				
				Cancel:{Text:"Cancelar",Id:"cancelar"},
				Aplicate:{Text:"Novedad aplicada",Id:"aplicar"},
				Aprove:{Text:"Aprobar",Id:"aprobar"},
				Revise:{Text:"Enviar a corregir",Id:"corregir"},
				Reject:{Text:"Rechazar", Id:"rechazar"}
			},
			contentType:"Certificaciones",
			containerId:"container-form"			
		},
		forms:{
			"NuevaSolicitud":"Nueva Solicitud",
			"AprobarSolicitud":"certificationForm.loadApprove()",
			"AprobarJefe":"certificationForm.loadApproveBoss()",
			"AplicarSolicitud":"certificationForm.loadApplicate()",
			"Corregir":"Corregir Solicitud",
			"VerSolicitud":"Ver Solicitud",
			"Solicitudes pendientes Novedades":"Vista Novedades",
			"Solicitudes pendientes Ventanilla TH":"Vista Ventanilla"
		},
		flowParameters:{
			urlFlow:"https://prod-09.westus.logic.azure.com:443/workflows/c947fc025bcd461cbc2b076f91653caa/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=tDwp2hRmnHaPaxQ5VnjcZ9t-0kABL_cPV8Qfs7buJoA",
			editUrl:"",
	    	approveUrl:"",
	   		applyUrl:"",
	   		approveBossUrl:"",
	    	listViewApply:"",
	    	listViewApprove:"",
	    	approveUsers:"Certificaciones Ventanilla TH",
	    	applyUsers:"Certificaciones Novedades",
	    	listNameConsecutive:"ControlRadicados",
	    	idConsecutive:13,
	    	listNameAssing:"ResponsablesTH",
	    	subjectTitle:"Certificaciones"
		},
		formsState:{
			AprobarSolicitud:"1",
			AprobarJefe:"5",
			AplicarSolicitud:"2"
		},
		global:{
			formName:null,
			createdBy:null,
			created:null

		},
		initialState:null

	},
	callFlowService:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
		
		var objectSelect = document.querySelector(certificationForm.parameters.fieldsHTML.typeRequest);
		var textSelected = objectSelect.options[objectSelect.selectedIndex].value;
	
	    certificationForm.parameters.flowParameters.approveUrl = String.format(certificationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(certificationForm.parameters.forms)[1],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);		
	    certificationForm.parameters.flowParameters.approveBossUrl = String.format(certificationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(certificationForm.parameters.forms)[2],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    certificationForm.parameters.flowParameters.applyUrl = String.format(certificationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(certificationForm.parameters.forms)[3],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
   	    certificationForm.parameters.flowParameters.editUrl = String.format(certificationForm.parameters.formats.formFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(certificationForm.parameters.forms)[4],spForms.currentItemID,sputilities.contextInfo().webAbsoluteUrl);
	    certificationForm.parameters.flowParameters.listViewApply = String.format(certificationForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(certificationForm.parameters.forms)[6]);
	    certificationForm.parameters.flowParameters.listViewApprove= String.format(certificationForm.parameters.formats.viewFormat,window.location.protocol,window.location.hostname,sputilities.contextInfo().listUrl,utilities.getKeysOfAnObject(certificationForm.parameters.forms)[7]);	    
		
		var objService = {
		    "currentItemID":spForms.currentItemID,
		    "currentItem":{
		    	"createdBy":certificationForm.parameters.global.createdBy,
		    	"nombres":spForms.loadedFields.NombrePersona1.value,
		    	"fechaInicioViaje": moment.utc(spForms.loadedFields.FechaInicioViaje.value).format(certificationForm.parameters.formats.dateFormat),
				"fechaFinViaje": moment.utc(spForms.loadedFields.FechaFinViaje.value).format(certificationForm.parameters.formats.dateFormat),
				"paisViaje":spForms.loadedFields.PaisViaje.value,
		    	"documento":Math.round(spForms.loadedFields.DocumentoIdentidad.value),		    	
		    	"consecutivo":spForms.loadedFields.NumeroRadicado.value,
		        "base":spForms.loadedFields.BasePersona.value.label,
		        "fechaCreacion":certificationForm.parameters.global.created,
		        "comentariosTH":spForms.loadedFields.ComentariosTH.value,
		        "comentariosJefe":spForms.loadedFields.ComentariosJefe.value,
		        "tipoCertificacion":textSelected
		    },
		    "state":spForms.loadedFields.EstadoCertificacion.value,
		    "approveUsers":certificationForm.parameters.flowParameters.approveUsers,
		    "applyUsers":certificationForm.parameters.flowParameters.applyUsers,
		    "siteUrl":sputilities.contextInfo().webAbsoluteUrl,
		    "listName":sputilities.contextInfo().listTitle,
		    "listNameConsecutive":certificationForm.parameters.flowParameters.listNameConsecutive,
		    "idConsecutive":certificationForm.parameters.flowParameters.idConsecutive,
		    "entityType":spForms.listInfo.entityItem,
			"listNameAssing":certificationForm.parameters.flowParameters.listNameAssing,
		    "editUrl":certificationForm.parameters.flowParameters.editUrl,
		    "approveUrl":certificationForm.parameters.flowParameters.approveUrl,
		    "applyUrl":certificationForm.parameters.flowParameters.applyUrl,
		    "approveBossUrl":certificationForm.parameters.flowParameters.approveBossUrl,
		    "listViewApply":certificationForm.parameters.flowParameters.listViewApply,
		    "listViewApprove":certificationForm.parameters.flowParameters.listViewApprove,
		    "subjectTitle":	certificationForm.parameters.flowParameters.subjectTitle
		}
		utilities.http.callRestService({
			url:certificationForm.parameters.flowParameters.urlFlow,
			method:utilities.http.methods.POST,
			data: JSON.stringify(objService),
			headers:{
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData
			},
			success:function(data){
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;

				spForms.cancelForm(null);
			},
			error:function(xhr){
				console.log(xhr);
			}
		});

	},
	initialize:function(){			
		var partsOfUrl = window.location.href.split('?')[0].split('/');
		certificationForm.parameters.global.formName = partsOfUrl[partsOfUrl.length - 1].split('.')[0];
		eval(certificationForm.parameters.forms[certificationForm.parameters.global.formName]);

	},
	loadApprove:function(){	
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:certificationForm.parameters.formsParameters.buttons.Aprove.Text,
				id:certificationForm.parameters.formsParameters.buttons.Aprove.Id,
				success:function (){
					certificationForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return certificationForm.validateApprove()
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:certificationForm.parameters.formsParameters.buttons.Revise.Text,
				id:certificationForm.parameters.formsParameters.buttons.Revise.Id,
				success:function (){
					certificationForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return certificationForm.validateReject()
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:certificationForm.parameters.formsParameters.buttons.Cancel.Text,
				id:certificationForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		certificationForm.parameters.formsParameters.description = certificationForm.parameters.formsParameters.descriptionApprove;
		certificationForm.parameters.formsParameters.title = certificationForm.parameters.formsParameters.titleApprove;
		var query = String.format(certificationForm.parameters.query,certificationForm.parameters.assingTo.Ventanilla);
		certificationForm.parameters.query = query;
		certificationForm.loadEdit(actions,certificationForm.loadResponsableState);

		
	},
	loadTypeRequest:function(){
		
		var objectSelect = document.querySelector(certificationForm.parameters.fieldsHTML.typeRequest);
		var textSelected = objectSelect.options[objectSelect.selectedIndex].value;
		
		if (textSelected == "1"){
				
			document.querySelector(certificationForm.parameters.fieldsHTML.CargoCertificar).style.display = "none";
			document.querySelector(certificationForm.parameters.fieldsHTML.FechaInicioViaje).style.display = "block";
			document.querySelector(certificationForm.parameters.fieldsHTML.FechaFinViaje).style.display = "block";
			document.querySelector(certificationForm.parameters.fieldsHTML.PaisViaje).style.display = "block";
			
		}else if (textSelected == "5")
		{
			
			document.querySelector(certificationForm.parameters.fieldsHTML.CargoCertificar).style.display = "block";
			document.querySelector(certificationForm.parameters.fieldsHTML.FechaInicioViaje).style.display = "none";
			document.querySelector(certificationForm.parameters.fieldsHTML.FechaFinViaje).style.display = "none";
			document.querySelector(certificationForm.parameters.fieldsHTML.PaisViaje).style.display = "none";
		
		}else{
		
			document.querySelector(certificationForm.parameters.fieldsHTML.CargoCertificar).style.display = "none";
			document.querySelector(certificationForm.parameters.fieldsHTML.FechaInicioViaje).style.display = "none";
			document.querySelector(certificationForm.parameters.fieldsHTML.FechaFinViaje).style.display = "none";
			document.querySelector(certificationForm.parameters.fieldsHTML.PaisViaje).style.display = "none";
		}
	
	},
	loadApproveBoss:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:certificationForm.parameters.formsParameters.buttons.Aprove.Text,
				id:certificationForm.parameters.formsParameters.buttons.Aprove.Id,
				success:function (){
					certificationForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return certificationForm.validateApprove()
				}
			},
			{
				type:spForms.parameters.updateAction,
				name:certificationForm.parameters.formsParameters.buttons.Reject.Text,
				id:certificationForm.parameters.formsParameters.buttons.Reject.Id,
				success:function (){
					certificationForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return certificationForm.validateReject()
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:certificationForm.parameters.formsParameters.buttons.Cancel.Text,
				id:certificationForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		certificationForm.parameters.formsParameters.description = certificationForm.parameters.formsParameters.descriptionApprove;
		certificationForm.parameters.formsParameters.title = certificationForm.parameters.formsParameters.titleApprove;
        certificationForm.loadEdit(actions,certificationForm.loadResponsableState);
	
	},
	loadApplicate:function(){
		var actions =[
			{
				type:spForms.parameters.updateAction,
				name:certificationForm.parameters.formsParameters.buttons.Aplicate.Text,
				id:certificationForm.parameters.formsParameters.buttons.Aplicate.Id,
				success:function (){
					certificationForm.callFlowService();
					return false;
				},
				error:function (){},
				preSave: function (){
					return certificationForm.validateApprove()
				}
			},
			{
				type:spForms.parameters.cancelAction,
				name:certificationForm.parameters.formsParameters.buttons.Cancel.Text,
				id:certificationForm.parameters.formsParameters.buttons.Cancel.Id,
				callback:""
			}
		];
		certificationForm.parameters.formsParameters.description = certificationForm.parameters.formsParameters.descriptionAplicate;
		certificationForm.parameters.formsParameters.title = certificationForm.parameters.formsParameters.titleAplicate;
		var query = String.format(certificationForm.parameters.query,certificationForm.parameters.assingTo.Novedades);
		certificationForm.parameters.query = query;
		certificationForm.loadEdit(actions,certificationForm.loadResponsableState);

	},
	loadEdit:function(actions,sucessFunction){
		spForms.options={	
			actions:actions,
			title:certificationForm.parameters.formsParameters.title,
			description:certificationForm.parameters.formsParameters.description,
			contentTypeName:certificationForm.parameters.formsParameters.contentType,
			containerId:certificationForm.parameters.formsParameters.containerId,
			allowAttachments:true,
		  	requiredAttachments:false,
		  	maxSizeInMB:3,
			successLoadEditForm:function(){	
				certificationForm.parameters.global.createdBy = spForms.currentItem.Author.EMail;
				certificationForm.parameters.global.created = moment.utc(new Date(spForms.currentItem.Created)).format(certificationForm.parameters.formats.dateFormat);
				certificationForm.loadTypeRequest();			
				if(typeof sucessFunction== "function"){
					sucessFunction();
				}
			} 
		};
		spForms.loadEditForm();
	},
	validateApprove:function(){
		var row = certificationForm.parameters.initialState == certificationForm.parameters.codeStates.Ventanilla ? document.querySelector(certificationForm.parameters.fieldsHTML.Comentarios) : document.querySelector(certificationForm.parameters.fieldsHTML.ComentariosJefe);
		var state = document.getElementById(certificationForm.parameters.internalNameFields.Estado);
		var isValid = true;
		if(certificationForm.parameters.initialState == certificationForm.parameters.codeStates.Ventanilla)
		{			
			if(row != null && state!= null){
				row.removeAttribute(certificationForm.parameters.invalid);
				state.value = certificationForm.parameters.codeStates.Novedades;
			}
			else
			{
				isValid = false;
			}
		}
		if(certificationForm.parameters.initialState == certificationForm.parameters.codeStates.Jefe)
		{		
			if(row != null && state!= null){
				row.removeAttribute(certificationForm.parameters.invalid);
				state.value = certificationForm.parameters.codeStates.Ventanilla;
			}
			else
			{
				isValid = false;
			}
		}
		if(certificationForm.parameters.initialState == certificationForm.parameters.codeStates.Novedades)
		{	
			if(state!= null){				
				state.value = certificationForm.parameters.codeStates.Aplicada;
			}
			else
			{
				isValid = false;
			}
		}
		return isValid;
		
	},
	validateReject:function(){
		var row = certificationForm.parameters.initialState == certificationForm.parameters.codeStates.Ventanilla ? document.querySelector(certificationForm.parameters.fieldsHTML.Comentarios): document.querySelector(certificationForm.parameters.fieldsHTML.ComentariosJefe);
		var state = document.getElementById(certificationForm.parameters.internalNameFields.Estado);		
		var isValid = true;
		
		if(row != null && state != null)
		{
			var comments = row.querySelector(certificationForm.parameters.errorInvalid.area);
			if(comments.value != "")
			{
				row.removeAttribute(certificationForm.parameters.invalid);
				if(certificationForm.parameters.initialState == certificationForm.parameters.codeStates.Jefe)
				{
					state.value = certificationForm.parameters.codeStates.Rechazado;
				}
				else
				{
					state.value = certificationForm.parameters.codeStates.Corregir;

				}			
			}
			else
			{
				row.setAttribute(certificationForm.parameters.invalid,certificationForm.parameters.errorInvalid.text);			
			}
		}
		else
		{
			isValid = false;
		}
		return isValid;

	},
	loadResponsableState:function(){	
		try
		{
			var userCurrent = _spPageContextInfo.userEmail
			var state = document.getElementById(certificationForm.parameters.internalNameFields.Estado);
			if(state != null)
			{
				if(state.value == certificationForm.parameters.formsState[certificationForm.parameters.global.formName])
				{
					if(state.value == certificationForm.parameters.codeStates.Novedades || state.value == certificationForm.parameters.codeStates.Ventanilla)
					{
						sputilities.getItems(certificationForm.parameters.listName,certificationForm.parameters.query,function(data){
							
							var resultItems = JSON.parse(data).d.results;
							if(resultItems.length > 0)
							{
								var items = resultItems[0].ResponsablesAbonos.results
								var find = false;
								for(var i=0;i<items.length;i++)
								{
									if(items[i].EMail == userCurrent){
										find = true;
									 	break;
									}
								}
								
								certificationForm.parameters.initialState = state.value;
								
								if(state.value == certificationForm.parameters.codeStates.Novedades)
								{				
									if(find)
									{
										certificationForm.showComments(state.value);
									}
									else
									{
										certificationForm.showComments(state.value);
										certificationForm.hideButtons(state.value);
										certificationForm.showPopUp();
									}
								}
								else if(state.value == certificationForm.parameters.codeStates.Ventanilla)
								{						
									if(find)
									{
										certificationForm.showComments(state.value);
									}
									else
									{
										certificationForm.showComments(state.value);
										certificationForm.hideButtons(state.value);
										certificationForm.showPopUp();
									}
								}
							}						
						});
					}
					else
					{
	                    var idUserCreated =  spForms.currentItem.AuthorId;
	                    
						sputilities.getUserInformation(idUserCreated,function(data){
	                        //save the results to an array
	                        var resultItems = utilities.convertToJSON(data);
	                        if(resultItems != null)
	                        {
	                            var email = resultItems.d.EMail;
	
	                            sputilities.getUserProfileProperty('Manager',email,function(data){
	                                var resultProperty =  utilities.convertToJSON(data);
	                                if(resultProperty != null)
	                                {
	                                    var emailJefe = resultProperty.d.GetUserProfilePropertyFor.split('|')[2];
	                                    var find = false;
	
	                                    if(userCurrent == emailJefe)
	                                    {
	                                    	find = true;
	                                    }
	                                    certificationForm.parameters.initialState = state.value;

	                                    if(state.value == certificationForm.parameters.codeStates.Jefe)
										{				
											if(find)
											{
												certificationForm.showComments(state.value);
											}
											else
											{
												certificationForm.showComments(state.value);
												certificationForm.hideButtons(state.value);
												certificationForm.showPopUp();
											}
									   }
	
	                                    console.log(emailJefe)
	                                }
	                            });
	                        }
	                    
	                    });
                    }
                }
                else
                {
                    certificationForm.showComments(certificationForm.parameters.formsState[certificationForm.parameters.global.formName]);
                    certificationForm.hideButtons(certificationForm.parameters.formsState[certificationForm.parameters.global.formName]);
                    certificationForm.showPopUp();
                }
		    }
		}
		catch(e){
			console.log(e);
		}	
	},
	hideButtons:function(state){
	
		if(state == certificationForm.parameters.codeStates.Novedades)
		{
			var bottonApp = document.getElementById(certificationForm.parameters.controlsHtml.Aplicar);
			if(bottonApp != null)
			{
				bottonApp.style.display = certificationForm.parameters.htmlRequiered.none;
			}
		}
		else if(state == certificationForm.parameters.codeStates.Ventanilla)
		{
			var bottonApp = document.getElementById(certificationForm.parameters.controlsHtml.Aprobar);
			var bottonRej = document.getElementById(certificationForm.parameters.controlsHtml.Corregir);
			var comments  = document.getElementById(certificationForm.parameters.internalNameFields.Comentarios);
			var icon = document.getElementById(certificationForm.parameters.controlsHtml.iconTH);
			
			if(bottonApp != null && bottonRej != null && comments != null && icon != null)
			{
				comments.style.pointerEvents = certificationForm.parameters.htmlRequiered.none;
				icon.style.pointerEvents = certificationForm.parameters.htmlRequiered.none;
				comments.style.color = certificationForm.parameters.controlsHtml.colorDisable;
				comments.style.borderColor = certificationForm.parameters.controlsHtml.borderDisable;	
				bottonApp.style.display = certificationForm.parameters.htmlRequiered.none;
				bottonRej.style.display = certificationForm.parameters.htmlRequiered.none;	

			}
		
		}
		else if(state == certificationForm.parameters.codeStates.Jefe)
		{
			var bottonApp = document.getElementById(certificationForm.parameters.controlsHtml.Aprobar);
			var bottonRej = document.getElementById(certificationForm.parameters.controlsHtml.Rechazar);
			var comments  = document.getElementById(certificationForm.parameters.internalNameFields.ComentariosJefe);
			var icon = document.getElementById(certificationForm.parameters.controlsHtml.iconJefe);
			
			if(bottonApp != null && bottonRej != null && comments != null && icon != null)
			{
				comments.style.pointerEvents = certificationForm.parameters.htmlRequiered.none;
				icon.style.pointerEvents = certificationForm.parameters.htmlRequiered.none;
				comments.style.color = certificationForm.parameters.controlsHtml.colorDisable;
				comments.style.borderColor = certificationForm.parameters.controlsHtml.borderDisable;	
				bottonApp.style.display = certificationForm.parameters.htmlRequiered.none;
				bottonRej.style.display = certificationForm.parameters.htmlRequiered.none;	
			}
		}	
	},
	showPopUp:function(){
		var html = String.format(certificationForm.parameters.formats.htmlNotAllowed,certificationForm.parameters.bodyItem);
		var formContent = document.querySelector(certificationForm.parameters.selectorFormContent);
		if(formContent != null){
			formContent.innerHTML = html;
		}	
	},
	showComments:function(state){
		if(state == certificationForm.parameters.codeStates.Jefe)
		{
			var comments = document.querySelector(certificationForm.parameters.fieldsHTML.ComentariosJefe);	
			if(comments != null)
			{
				comments.setAttribute(certificationForm.parameters.hidden,false);
				comments.querySelector(certificationForm.parameters.errorInvalid.area).value="";
	
			}
		}
		else if(state == certificationForm.parameters.codeStates.Ventanilla)
		{
			var commentsJefe = document.querySelector(certificationForm.parameters.fieldsHTML.ComentariosJefe);	
			var comments = document.querySelector(certificationForm.parameters.fieldsHTML.Comentarios);	
			if(comments != null && commentsJefe != null)
			{
				comments.setAttribute(certificationForm.parameters.hidden,false);
				commentsJefe.setAttribute(certificationForm.parameters.hidden,false);
				comments.querySelector(certificationForm.parameters.errorInvalid.area).value="";
	
			}
		}
		else if(state == certificationForm.parameters.codeStates.Novedades)
		{
			var commentsJefe = document.querySelector(certificationForm.parameters.fieldsHTML.ComentariosJefe);	
			var comments = document.querySelector(certificationForm.parameters.fieldsHTML.Comentarios);	
			if(comments != null && commentsJefe != null)
			{
				comments.setAttribute(certificationForm.parameters.hidden,false);
				commentsJefe.setAttribute(certificationForm.parameters.hidden,false);
			}
		}
	}
}
certificationForm.initialize();