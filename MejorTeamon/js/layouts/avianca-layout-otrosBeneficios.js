﻿/* Component: Slider de Categorias de los convenios */
var sliderCategory = {
	//create parameters
	parameters:{
		formats:{
			camlCountryAndContract:"<View><Query><Where><And><Eq><FieldRef Name='PaisMultiple'/><Value Type='Text'>{0}</Value></Eq><Eq><FieldRef Name='TipoContrato'/><Value Type='Lookup'>{1}</Value></Eq></And></Where><OrderBy><FieldRef Name='Title' Ascending='TRUE'/></OrderBy></Query></View>"		
		},
		listName: "Categorias",
		top: 12,
		divID:"otherbenefits-content",
		bodyItemCategory:"<div class='otherbenefits-item'>"
							+"<div class='otherbenefits-item-int'>"
								+"<a href='javascript:void(0);' onclick='javascript:pageLayoutOtros.components.sliderCategory.click({0});'>"
									+"<img src='{1}' class='icon'>"
									+"<h3>{2}</h3>"
									+"<img src='../../SiteAssets/Development/images/beneficios/othersBenefits/ico-seemore.png'>"
								+"</a>"
							+"</div>"
						+"</div>",
		containerItemsCategory:"<div class='otherbenefits-file'>{0}</div>",
		query:"?$select=ID,Title,Orden,sys_txtImg,Descripcion&$orderby=Orden&$top={1}",
		queryUserProfile:"?$select=AccountName",
		url:"{0}/Paginas/ConveniosPais.aspx?IDCategoria={1}",
		resultPropertiesUser:null,
		nameSessionLabel:"CountrySessionUser"			
	},
	global:{
		currentUser:{
			country:"Pais",
			contract:"Contrato"
		},
		resultItemsCategory:null
	},
	getCurrentUserProperties:function(callback){
		sputilities.getMyUserProfileProperties('',
			function(userData){
				var data = utilities.convertToJSON(userData);
				if(data != null){
					sliderCategory.global.currentUser.country = sliderCategory.findProperty(data,sliderCategory.global.currentUser.country);
					sliderCategory.global.currentUser.contract = sliderCategory.findProperty(data,sliderCategory.global.currentUser.contract);
					callback();
				}
			},
			function(xhr){
				console.log(xhr);
			}
		);
	},
	findProperty:function(objUser,name){
		var properties = objUser.d.UserProfileProperties.results;
		for(var i = 0; i < properties.length; i++){
			var property = properties[i];
			if(property.Key == name){
				return property.Value;
			}
		}
		return null;
	},
	initialize:function(){
		document.getElementById('ms-designer-ribbon').style.opacity = 0;
		document.getElementById('s4-workspace').style.opacity = 0;
		try {
			sliderCategory.getCurrentUserProperties(
				function(){
					var country = sliderCategory.global.currentUser.country;
					var contract = sliderCategory.global.currentUser.contract;
					if(country != null && country != "" && contract != null && contract != ""){
						sputilities.getItemsByCaml({
							success:function(data){
								var data = utilities.convertToJSON(data);
								if(data != null){
									sliderCategory.global.resultItemsCategory = data.d.results;
									sliderCategory.insertHTML();
									document.getElementById('s4-workspace').style.opacity = 1;
									document.getElementById('ms-designer-ribbon').style.opacity = 1;
								}
							},
							error:function(xhr){
								console.log(xhr);
							},
							caml: String.format(sliderCategory.parameters.formats.camlCountryAndContract, country,contract),
							listName: sliderCategory.parameters.listName
							
						});
					}
				}
			);
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = String.empty;
		var htmlItems = String.empty;
		var data = sliderCategory.global.resultItemsCategory;
		for (var i=0; i< data.length; i++){			var currentItem = data[i];
			if(currentItem.sys_txtImg != null)
			{
				var title = currentItem.Title;
				var id = currentItem.ID;
				var desc = currentItem.Descripcion;
				var image = currentItem.sys_txtImg;
				var div = document.createElement('div');
				div.innerHTML = image;
				var urlimg = div.querySelector('img').src;
				if ((i+1)%4==0 || (i+1)== data.length){
					htmlItems += String.format(sliderCategory.parameters.bodyItemCategory,id,urlimg,title);
					html += String.format(sliderCategory.parameters.containerItemsCategory,htmlItems);
					htmlItems = String.empty;
					
				}else{
					htmlItems += String.format(sliderCategory.parameters.bodyItemCategory,id,urlimg,title);
				}
			}
		}
		
		document.getElementById(sliderCategory.parameters.divID).innerHTML = html;
	},
	click:function(idCategoria){
		try {
			var url = String.format(sliderCategory.parameters.url,sputilities.contextInfo().webAbsoluteUrl,idCategoria);			
		}
		catch (e){
			console.log(e);
		}

	}
}
sputilities.callFunctionOnLoadBody('sliderCategory.initialize');