﻿/* Component: Slider de Categorias de los convenios */
var sliderCategory = {
	//create parameters
	parameters:{
			listNameDetail:"Convenios",
			top: 18,
			divIDText:"text-int-detail",
			divIDDetail:"list-agreements-content",
			bodyCategoryDesc:"<h1 class='title-agreement'>{0}</h1>"
							 +"<p class='desc-agreement'>{1}</p>",
			bodyItemDetails:"<div class='item-agreement'>"
							  +"<a href='javascript:void(0);' onclick='{0}'>"
							  +"<div class='container-item-agreement'>"
								  +"<img src='{1}'/>"
								  +"<h3>{2}</h3>"
							  +"</div>"
							  +"</a>"
							+"</div>",
			queryDetails:"<View>"
							+"<Query>"
							   +"<Where>"
							      +"<And>"
							        +"<Contains>"
							            +"<FieldRef Name='Pais' />"
							            +"<Value Type='Text'>{0}</Value>"
							         +"</Contains>"
							         +"<Eq>"
							            +"<FieldRef Name='Categoria' LookupId='TRUE'/>"
							            +"<Value Type='Lookup'>{1}</Value>"
							         +"</Eq>"
							      +"</And>"
							   +"</Where>"
							+"</Query>"
						+"</View>",
			modal:'javascript:sputilities.showModalDialog("Convenio","{0}",window.location.href);',
			listUrl:"{0}/Lists/{1}",
			dispForm:"{0}/Lists/Convenios/Detalle.aspx?ID={1}&Source={2}",	
			detailUrl:"{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo=Convenios",
			idCategoria:null,
			Pais:null,
			resultItemsDetails:null,
			nameSessionLabel:"CountrySessionUser"
	},
	
	initialize:function(){
		try {
				
			sliderCategory.parameters.idCategoria = utilities.getQueryString('IDCategoria');
			sliderCategory.parameters.Pais = sessionStorage[sliderCategory.parameters.nameSessionLabel];
			
			if(sliderCategory.parameters.idCategoria != null && sliderCategory.parameters.Pais!= null)
			{
				//adding parameters to query
				var linkQuery = String.format(sliderCategory.parameters.queryDetails,sliderCategory.parameters.Pais,sliderCategory.parameters.idCategoria);
				
				//running the query with parameters
				sputilities.getItemsByCaml({
				caml:linkQuery,
				listName: sliderCategory.parameters.listNameDetail,
				success:function(items){
					var data = JSON.parse(items).d.results;
						sliderCategory.parameters.resultItemsDetails = data;
						pageLayoutConveniosPais.componentsList.push(sliderCategory);					
				},
				error:function(xhr){console.log(JSON.parse(xhr).error.message.value)}
				});	
			}
			
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var htmlDetails = "";
		var resultDetails = sliderCategory.parameters.resultItemsDetails;
		for (var j=0; j< resultDetails.length; j++){
		
			var currentItem = resultDetails[j];
			if(currentItem.sys_txtImg != null)
			{
				//retrieving the fields of the results
				var titleDetail = currentItem.Empresa;
				var idDetail = currentItem.ID;
										
				//taking the url of the image
				var imageDetail = currentItem.sys_txtImg;
				var divDetail = document.createElement('div');
				divDetail.innerHTML = imageDetail;
				var urlimgDetail = divDetail.querySelector('img').src;
				
				var detailURL = String.format(sliderCategory.parameters.dispForm,sputilities.contextInfo().webAbsoluteUrl,idDetail,sputilities.contextInfo().webAbsoluteUrl);
				var htmlModal = String.format(sliderCategory.parameters.modal,detailURL);
				
				htmlDetails += String.format(sliderCategory.parameters.bodyItemDetails,htmlModal,urlimgDetail,titleDetail);
			}
		}
			
		var textCategory = localStorage.getItem(sliderCategory.parameters.idCategoria);
		var dataJson = JSON.parse(textCategory);
		var htmlText = String.format(sliderCategory.parameters.bodyCategoryDesc,dataJson.Title,dataJson.Descripcion);
		document.getElementById(sliderCategory.parameters.divIDText).innerHTML = htmlText;
		document.getElementById(sliderCategory.parameters.divIDDetail).innerHTML = htmlDetails;
		
					
	},
}

sputilities.callFunctionOnLoadBody('pageLayoutConveniosPais.load');
var pageLayoutConveniosPais= {
	components:{
		sliderCategory : sliderCategory		
	},
	load:function(){
		pageLayoutConveniosPais.initComponents();
		var loadControl = setInterval(function(){
			document.getElementById('ms-designer-ribbon').style.opacity = 0;
			document.getElementById('s4-workspace').style.opacity = 0;
			if(pageLayoutConveniosPais.componentsList.length  == pageLayoutConveniosPais.max){
				for(var i = 0; i <  pageLayoutConveniosPais.componentsList.length ; i++){
					 pageLayoutConveniosPais.componentsList[i].insertHTML();
				}
				clearInterval(loadControl);	
				pageLayoutConveniosPais.animations();
				//document.getElementById("img-load").style.display = "none";
				document.getElementById('s4-workspace').style.opacity = 1;
				document.getElementById('ms-designer-ribbon').style.opacity = 1;
				setTimeout(function(){
				},5000);							
			}
		},500);	
	},
	initComponents:function(){
		pageLayoutConveniosPais.components.sliderCategory.initialize();		
	},
	animations:function(arraySliderDetails){
		$.noConflict();
			$('#list-agreements-content').slick({
			  dots: false,
			  infinite: true,
			  speed: 700,
			  rows:2,
			  slidesToShow: 4,
			  slidesToScroll: 1,
			  responsive: [
			    {
			      breakpoint: 1024,
			      settings: {
			       	rows:2,
			        slidesToShow: 2,
			        slidesToScroll: 1,
			        infinite: true,
			        dots: false
			      }
			    },
			    {
			      breakpoint: 600,
			      settings: {
			      	rows:2,
			        slidesToShow: 2,
			        slidesToScroll: 1
			      }
			    },
			    {
			      breakpoint: 480,
			      settings: {
			      	rows:2,
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    }
			  ]
		});
	},
	componentsList:[],
	componentsDetails:[],
	idItemInicial:null,	
	max:1
}