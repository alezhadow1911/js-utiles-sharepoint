﻿/*Components: Slider Galeria*/

var sliderImg = {
	//create parameters
	parameters:{
			listName: "Galeria",
			top: 7,
			divID:"gallery-images",
			contentType: "la imagen",
			detailParameter: "Galeria",
			linkpopUp: '{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo={3}&ListaLikes=%22%22',
			link:"<a href='{0}' target='_blank'>",
			bodyImg:"<img src='{0}' /><h3>{1}</h3></a>",
			formatHtml:"<div class='gallery-image' onclick='javascript:sputilities.showModalDialog(\"{2}\",\"{3}\")'>"
						+	'<div class="container-window"></div>'
    					+	'<div class="container-bg-gallery">'
    					+	'<img class="img-bg-gallery" src="{1}" onerror="displayGenericDocumentIcon(event.srcElement ? event.srcElement : event.target, 0); return false;" onload="(event.srcElement ? event.srcElement : event.target).style.visibility = \'visible\'; SP.ScriptHelpers.resizeImageToSquareLength(this, 280)">'
        				+		'<div class="gallery-ico">'
            			+			'<a>'
                		+ 				'<img src="{0}/SiteAssets/Development/images/components/gallery/resize.png">'
                		+				'<br>'
            			+ 			'</a>'
        				+ 		'</div>'
    					+	'</div>'
						+'</div>',
			query: "?$select=Title,FileRef,FileType,ContentType/Name,ID,Destacado&$expand=ContentType&$filter=(ContentType eq '{0}' and Destacado eq 1)&$orderby=Title&$top={1}",
			resultItems: null,
			sizeSquareImage:300
	},
	insertHTML:function(){
	var html = "";
		var resultItems = sliderImg.parameters.resultItems;
		for (var i=0; i< resultItems.length; i++){
			var currentItem = resultItems[i];
			//retrieving the fields of the results
			var ID = currentItem.ID;
			var urlImg = currentItem.FileRef;
			
			var segments = urlImg.split('/');
			var segmentName = String.format("{0}.jpg",segments[segments.length -1].replace('.','_'));
			segments[segments.length  -1] = "_w";
			segments.push(segmentName);
			urlImg = segments.join('/');
			
			
			var contentType = currentItem.ContentType.Name;
			var title = currentItem.Title;			
			//adding values to the parameters
			var linkpopUpNew = String.format(sliderImg.parameters.linkpopUp,sputilities.contextInfo().webAbsoluteUrl,sliderImg.parameters.listName,ID,sliderImg.parameters.detailParameter);
			html += String.format(sliderImg.parameters.formatHtml,sputilities.contextInfo().webAbsoluteUrl,urlImg,title,linkpopUpNew)				
		}	
		//insert the html code
		document.getElementById(sliderImg.parameters.divID).innerHTML = html;
	},
	initialize:function(){
		try {
			//adding parameters to query
			var linkQuery = String.format(sliderImg.parameters.query,sliderImg.parameters.contentType,sliderImg.parameters.top);
			//running the query with parameters
			sputilities.getItems(sliderImg.parameters.listName,linkQuery,function(data){	
				//save the results to an array
				sliderImg.parameters.resultItems = JSON.parse(data).d.results;
				 pageLayoutPae.componentsList.push(sliderImg);		
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	}
}

/*Component: Noticias Estaticas */

var staticNews = {
	//create parameters
	parameters:{
			listName: "Noticias Estáticas",
			top: 2,
			divID:"static-news",
			containerNews:"<div class='otherNewsVP-item'>{0}{1}</div>",			
			imageNews:"<img src='{0}' />",
			textNews:"<div class='otherNewsVP-item-text'><h3>{0}</h3><p>{1}</p><a href='{2}' target='_blank'>Ver más<i class='inc-arrow-3 centrado-y'></i></a></div>",
			query: "?$select=Title,FechaFinal,FechaInicio,Url,sys_txtImg,Descripcion,Orden&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}')&$orderby=Orden&$top={1}",
			resultItems:null		
	
	},
	initialize:function(){
		try {

			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	
			
			//add current date to query and filter
			var linkQuery = String.format(staticNews.parameters.query,date,staticNews.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(staticNews.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				staticNews.parameters.resultItems = resultItems;
				 pageLayoutPae.componentsList.push(staticNews);
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var data = staticNews.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];
			
			//retrieving the fields of the results
			var title = currentItem.Title;
			var description = currentItem.Descripcion;			
			var url = currentItem.Url.Url;
				
			//taking the url of the image
			var image = currentItem.sys_txtImg;
			var div = document.createElement('div');
			div.innerHTML = image;
			var urlimg = div.querySelector('img').src;			
					
			//make html with the fields 
			var imageHtml = String.format(staticNews.parameters.imageNews, urlimg);
			var textHtml = String.format(staticNews.parameters.textNews,title,description,url);			
					
			//adding html 
			html += String.format(staticNews.parameters.containerNews,imageHtml,textHtml);			
					
		}
			
		//insert the html code
		document.getElementById(staticNews.parameters.divID).innerHTML = html;
	}
}

/* Component: Eventos */

var events = {
//create parameters
	parameters:{
		listName: "Eventos",
		containerId:"items-events",	
		moreEventsId:"more-events",
		top: 6,	
		mounth:["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC"],
		query: "?$select=ID,Title,Description,EventDate,EndDate,fAllDayEvent&$filter=EndDate ge datetime'{0}'&$orderby=EventDate&$top={1}",
		containerEven:"<div class='item-events'>{0}{1}</div>",
		textEvent:"<div class='item-events-text'><h2>{0}</h2><p>{1}</p></div>"+
				  "<div class='item-events-date'><h3>{2}</h3><span>{3}</span></div>"+
				  "<div class='item-events-hour'><h3>{4}</h3><span>{5}</span></div>",
		linkEvent:"<div class='item-events-text-arrow' onclick='{0}'><a href='javascript:void(0);'><i class='inc-arrow-3 centrado-y'></i></a></div>",
		modal:'javascript:sputilities.showModalDialog("Evento","{0}",window.location.href);',
		listUrl:"{0}/Lists/{1}",
		dispForm:"{0}/DispForm.aspx?ID={1}",	
		detailUrl:"{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo=Eventos",
		linkAll:"<a href='{0}'>Ver todos <i class='inc-arrow-3 centrado-y'></i></a>",		
		resultItems:null	
	},
	initialize:function(){
		//Get current date value
		var today = new Date;
		today = today.toISOString();
		//We build the query			
		events.parameters.query = String.format(events.parameters.query,today,events.parameters.top)
		try{
			sputilities.getItems(events.parameters.listName,events.parameters.query,function(data){
			//We get the url from the list of events
				
				var results = JSON.parse(data).d.results;
				events.parameters.resultItems = results;
				 pageLayoutPae.componentsList.push(events);
					
						
					
			},function(data){
				console.log(JSON.parse(data).error.message.value)
			});		
		}catch(e){			
			console.log(String.format('Ocurrio un error inesperado en el metodo events.initialize {0}',e))
		}				
				
	},
	insertHTML: function(){
		var data = events.parameters.resultItems;
		var html = "";
		var listUrl = String.format(events.parameters.listUrl,sputilities.contextInfo().webAbsoluteUrl,events.parameters.listName)	;	
		//We construct the section of the event for each event in the result set
		for (var i = 0;i<data.length;i++){
			
			var form = String.format(events.parameters.dispForm,listUrl,data[i].ID);
			
			
			var allday = data[i].fAllDayEvent;
			console.log(allday)
			if(allday == true){
			var DateSite =data[i].EventDate.replace("Z","")	
			var startDateTime = new Date(DateSite);	
			var startDay =('0' +  startDateTime.getUTCDate()).slice(-2);
			var startMonth = events.parameters.mounth[startDateTime.getMonth()];
			eventTime = "el día";
			eventAMPM= "Todo";
			}
			else{
			
			var startDateTime = new Date(data[i].EventDate);
			var endDateTime = new Date(data[i].EndDate);							
			var startDay =('0' +  startDateTime.getUTCDate()).slice(-2);
			var startMonth = events.parameters.mounth[startDateTime.getMonth()];
			
			
			var startHour="";
			var startMinutes= "";
				if(startDateTime.getHours() > 12)
				{
					var hour = startDateTime.getHours() - 12;
					startHour = ('0' + hour).slice(-2);
					startMinutes = ('0' + startDateTime.getMinutes()).slice(-2);
					eventAMPM="PM";
					eventTime = String.format("{0}:{1}",startHour,startMinutes);	
				}
				else
				{
					startHour = ('0' + startDateTime.getHours()).slice(-2);
					startMinutes = ('0' + startDateTime.getMinutes()).slice(-2);
					eventAMPM="AM";
					eventTime = String.format("{0}:{1}",startHour,startMinutes);	
				}}
				
		
			var detailURL = String.format(events.parameters.detailUrl,sputilities.contextInfo().webAbsoluteUrl,events.parameters.listName,data[i].ID);
			var htmlModal = String.format(events.parameters.modal,detailURL);
			var htmlLink = String.format(events.parameters.linkEvent,htmlModal);
			var htmlBody = String.format(events.parameters.textEvent,data[i].Title,data[i].Description,startMonth,startDay,eventAMPM,eventTime);
			
			html += String.format(events.parameters.containerEven,htmlBody,htmlLink);
					
		}
			
		document.getElementById(events.parameters.containerId).innerHTML = html;
		var htmlAll = String.format(events.parameters.linkAll,listUrl);
		document.getElementById(events.parameters.moreEventsId).innerHTML=htmlAll ;
							
	}

}

/* Component: Preguntas claves */

var frecuentQuestions = {
	//create parameters
	parameters:{
			listName: "Preguntas Frecuentes",
			top: 5,
			divID:"accordion",
			bodyQuestionOne:"<div class='panel panel-default'><div class='panel-heading' role='tab' id='heading{0}'><div class='panel-title'><a role='button' data-toggle='collapse' data-parent='#accordion' href='#collapse{0}' aria-expanded='true' aria-controls='collapse{0}'><i class='keyquestions-ico'></i><h3>{1}</h3></a></div></div><div id='collapse{0}' class='panel-collapse collapse in' role='tabpanel' aria-labelledby='heading{0}'><div class='panel-body'><p>{2}</p></div></div></div>",
			bodyQuestion:"<div class='panel panel-default'><div class='panel-heading' role='tab' id='heading{0}'><div class='panel-title'><a class='collapsed' role='button' data-toggle='collapse' data-parent='#accordion' href='#collapse{0}' aria-expanded='false' aria-controls='collapse{0}'><i class='keyquestions-ico'></i><h3>{1}</h3></a></div></div><div id='collapse{0}' class='panel-collapse collapse' role='tabpanel' aria-labelledby='heading{0}'><div class='panel-body'><p>{2}</p></div></div></div>",
			query: "?$ID,select=Title,Respuesta,Orden&$orderby=Orden&$top={0}",
			resultItems:null
	},
	initialize:function(){
		try {
			//adding parameters to query
			var linkQuery = String.format(frecuentQuestions.parameters.query,frecuentQuestions.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(frecuentQuestions.parameters.listName,linkQuery,function(data){
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				frecuentQuestions.parameters.resultItems = resultItems;
				pageLayoutPae.componentsList.push(frecuentQuestions);	
							
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var bodyNew = "";
		//loop the array
		for (var i=0; i< frecuentQuestions.parameters.resultItems.length; i++){
			var currentItem = frecuentQuestions.parameters.resultItems[i];
			
			//retrieving the fields of the results
			var title = currentItem.Title;
			var idAnswer = currentItem.ID;
			var answer = currentItem.Respuesta;
			if(i == 0){
				//adding values to the parameters
				bodyNew  = String.format(frecuentQuestions.parameters.bodyQuestionOne,idAnswer,title,answer);

			}
			else{
				//adding values to the parameters
				bodyNew = String.format(frecuentQuestions.parameters.bodyQuestion,idAnswer,title,answer);
			}
			
			//adding values of parameters to the html variable
			html += bodyNew;					
		}
			//insert the html code
			document.getElementById(frecuentQuestions.parameters.divID).innerHTML = html;
	}
}

/* Component: Slider Noticias */
var sliderNews = {
	//create parameters
	parameters:{
			listName: "Páginas",
			top: 7,
			divID:"carousel-header",
			detailParameter: "Noticias",
			containerIndi:" <ol class='carousel-indicators'>{0}</ol>",
			bodyIndicators:" <li data-target='#carousel-header' data-slide-to='{0}'{1}></li>",
			containerSlides:" <div class='carousel-inner slider' role='listbox'>{0}</div>",
			bodyItemSlide:"<div class='item{0}' id='slide-0{1}'> <img src='{2}' alt='...'><div class='carousel-caption'><h2>{3}</h2><p>{4}</p><a href='{5}' target='_self' class='btn light-red-button'>Más información</a></div></div>",
			detailUrl:"{0}?ListaBase={1}&IDItem={2}&Tipo=Noticias",
			modal:'sputilities.showModalDialog("Noticia","{0}",window.location.href)',
			query: "?$select=FileRef,Title,ArticleStartDate,Entradilla,PublishingPageContent,ID,FechaInicio,FechaFinal,Orden,sys_txtImg&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}' and ContentType eq 'Noticias')&$orderby=Orden&$top={1}",
			resultItems:null
	
	},
	
	initialize:function(){
		try {
		
		
			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	
			
			//adding parameters to query
			var linkQuery = String.format(sliderNews.parameters.query,date,sliderNews.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(sliderNews.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;

				sliderNews.parameters.resultItems = resultItems;
				 pageLayoutPae.componentsList.push(sliderNews);			
				

			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var htmlIndicators="";
		var htmlSliders="";
		var data= sliderNews.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];
				if(currentItem.sys_txtImg != null)
				{
					//retrieving the fields of the results
					var title = currentItem.Title;
					var dateArticle = currentItem.ArticleStartDate;
					var description = currentItem.Entradilla;
					var content = currentItem.PublishingPageContent;
					var id = currentItem.ID;
					var startDate = currentItem.FechaInicio;
					var finalDate = currentItem.FechaFinal;
						
					//taking the url of the image
					var image = currentItem.sys_txtImg;
					var div = document.createElement('div');
					div.innerHTML = image;
					var urlimg = div.querySelector('img').src;
					
					//make html and detail popup
					//var urlDetail = String.format(sliderNews.parameters.detailUrl,sputilities.contextInfo().webAbsoluteUrl,sliderNews.parameters.listName,currentItem.ID);
					//var modal = String.format(sliderNews.parameters.modal, urlDetail);
					
					var urlPage= String.format(sliderNews.parameters.detailUrl,currentItem.FileRef,sliderNews.parameters.listName,currentItem.ID);
					//adding values to the parameters
					if(i==0)
					{
						htmlIndicators += String.format(sliderNews.parameters.bodyIndicators,i," class='active'");
						htmlSliders += String.format(sliderNews.parameters.bodyItemSlide," active",i,urlimg,title,description,urlPage);
					}
					else
					{
						htmlIndicators += String.format(sliderNews.parameters.bodyIndicators,i,"class=''");
						htmlSliders += String.format(sliderNews.parameters.bodyItemSlide,"",i,urlimg,title,description,urlPage);
		
					}
			}
					
		}
		//adding values of parameters to the html variable
			html += String.format(sliderNews.parameters.containerIndi,htmlIndicators);
			html += String.format(sliderNews.parameters.containerSlides,htmlSliders);
			
		//insert the html code
		document.getElementById(sliderNews.parameters.divID).innerHTML = html;
	}
}

/* Component: Tabs Campañas */

var tabContent = {
	//create parameters
	parameters:{
			listName:"Tabs Contenido PAE",
			top: 4,
			modalVideo:'javascript:sputilities.showModalDialog("Video","{0}",window.location.href);',
			modalImage:'javascript:sputilities.showModalDialog("Imagen","{0}",window.location.href);',
			divID:"module-projectTabs",
			detailUrlImage:"{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo=ImagenCampana",
			detailUrlVideo:"{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo=VideoCampana",
			contaierListTabs:"<ul class='nav nav-tabs'>{0}</ul>",
			containerContent:"<div id='myTabContent' class='tab-content'>{0}</div>",
			bodyListTabs:"<li class='{0}'><a href='#{1}' data-toggle='tab' aria-expanded='{2}'>{3}</a></li>",
			containerLibrary:"<div class='tab-pane fade {0}' id='{1}' style='padding: 8% 5% 0% 5%'>{2}</div>",
			bodyImg:"<div class='tab-pane fade {0}' id='{1}'>" 
							+"<div class='projectTabs-image'>"
								+"<img src='{2}'>"
								+"<a href='javascript:void(0);' onclick='{3}' class='btn-fullscreen'>"
									+"<img src='" + window.location + "/SiteAssets/Development/images/campaigns/projectTabs/btn-fullscreen.png'>"
								+"</a>"
							+"</div>"
					 +"</div>",
			bodyText:"<div class='tab-pane fade {0}' id='{1}'>" 							
							+"<div class='projectTabs-text'>"
								+"<h2>{2}</h2>"
								+"<h1>{3}</h1>"
								+"<p>{4}</p>"
							+"</div>"
						+"</div>",
			bodyLibrary: "<div class='projectTabs-document'>"
							+"<img src='"+sputilities.contextInfo().webAbsoluteUrl+"/_layouts/15/images/{0}'/><a href='{1}' target='_blank'>{2}</a>"
						+"</div>",
			bodyVideo:"<div class='tab-pane fade {0}' id='{1}'>" 
							+"<div class='projectTabs-image'>"
								+"<img src='{2}'>"
								+"<a href='{3}' class='circulo bg-play centrado-total'>"
					         		+"<i class='inc-play centrado-total'></i>"
					        	+"</a>"
							+"</div>"
						+"</div>",
			query: "?$select=ID,Title,TextoPrincipal,TextoSecundario,sys_txtImg,Url,TipoInfo,Orden&$orderby=Orden&$top={0}",
			topLibrary: 5,
			queryLibrary: "?$select=ID,DocIcon,ServerRedirectedEmbedUri,Title,FileRef&$orderby=Title&$top={0}",
			resultItems:null,
			files:null	
	},
	
	initialize:function(){
		try {
		
			//adding parameters to query
			var linkQuery = String.format(tabContent.parameters.query,tabContent.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(tabContent.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				tabContent.parameters.resultItems = resultItems;
				pageLayoutPae.componentsList.push(tabContent);	
				
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
			
		var html = "";
		var htmlIndicators="";
		var htmltabs ="";
		var typeContent = ["Texto","Imagen","Video","Biblioteca"];
		var actionsTabs = ["false","true","active","active in"];
		var data = tabContent.parameters.resultItems;
		
		if(data.length > 0)
		{
			for (var i=0; i< data.length; i++)
			{
				var currentItem = data[i];
				
				//retrieving the fields of the results
				var idItem = currentItem.ID;
				var title = currentItem.Title;
				var principalText = currentItem.TextoPrincipal;
				var secondaryText = currentItem.TextoSecundario;
				var type = currentItem.TipoInfo;
				var url ="";
				if(currentItem.Url != null)
				{				
					url = currentItem.Url.Url;
				}
				
				if(i==0)
				{
					htmlIndicators += String.format(tabContent.parameters.bodyListTabs,actionsTabs[2],title.replace(/\s/g,''),actionsTabs[1],title);
				}
				else
				{
					htmlIndicators += String.format(tabContent.parameters.bodyListTabs,"",title.replace(/\s/g,''),actionsTabs[0],title);
						
				}
				
				if(type == typeContent[0]){
					console.log(type);
					
					if(i==0)
					{
						//adding values to the parameters
						var bodyNew = String.format(tabContent.parameters.bodyText,actionsTabs[3],title.replace(/\s/g,''),title,principalText,secondaryText);
						//adding values of parameters to the html variable
					}
					else{
						
						var bodyNew = String.format(tabContent.parameters.bodyText,"",title.replace(/\s/g,''),title,principalText,secondaryText);
					}
					
					htmltabs += bodyNew;
				}
				if(type == typeContent[1]){
					console.log(type);
					//taking the url of the image
					var image = currentItem.sys_txtImg;
					var div = document.createElement('div');
					div.innerHTML = image;
					var urlimg = div.querySelector('img').src;
					var urlImagehtml= String.format(tabContent.parameters.detailUrlImage,sputilities.contextInfo().webAbsoluteUrl,tabContent.parameters.listName,idItem);
					var modalImagehtml= String.format(tabContent.parameters.modalImage,urlImagehtml);

					if(i==0)
					{
						//adding values to the parameters
						var bodyNew = String.format(tabContent.parameters.bodyImg,actionsTabs[3],title.replace(/\s/g,''),urlimg,modalImagehtml);
						//adding values of parameters to the html variable
					}
					else{
						
						var bodyNew = String.format(tabContent.parameters.bodyImg,"",title.replace(/\s/g,''),urlimg,modalImagehtml);
					}
					
					htmltabs += bodyNew;
				}

								
				if(type == typeContent[2]){
					console.log(type);
						//taking the url of the image
						var image = currentItem.sys_txtImg;
						var div = document.createElement('div');
						div.innerHTML = image;
						var urlimg = div.querySelector('img').src;
						var urlVideohtml= String.format(tabContent.parameters.detailUrlVideo,sputilities.contextInfo().webAbsoluteUrl,tabContent.parameters.listName,idItem);
						var modalVideohtml= String.format(tabContent.parameters.modalVideo,urlVideohtml);
	
					if(i==0)
					{	
						//adding values to the parameters
						var bodyNew = String.format(tabContent.parameters.bodyVideo,actionsTabs[3],title.replace(/\s/g,''),urlimg,modalVideohtml);
						//adding values of parameters to the html variable
					}
					else{
						
						var bodyNew = String.format(tabContent.parameters.bodyVideo,"",title.replace(/\s/g,''),urlimg,modalVideohtml);
					}
					
					htmltabs += bodyNew;
				}

				
				if(type == typeContent[3]){
					console.log(type);
					var htmlDocs="";
					url = currentItem.Url.Description;
					url = url.split("/");
					url = url[url.length-1];
										
					var linkQueryLibrary = String.format(tabContent.parameters.queryLibrary,tabContent.parameters.topLibrary);
					
					if(i==0)
						{
							//adding values to the parameters
							var bodyNew = String.format(tabContent.parameters.containerLibrary,actionsTabs[3],title.replace(/\s/g,''));
							//adding values of parameters to the html variable
						}
						else{
							
							var bodyNew = String.format(tabContent.parameters.containerLibrary,"",title.replace(/\s/g,''));
						}
						
						htmltabs += bodyNew;
					//adding values to the parameters
					
						sputilities.getItems(url,linkQueryLibrary,function(dataDoc){
						bodyNew ="";
						//save the results to an array
						var resultItemsDoc = JSON.parse(dataDoc).d.results;
						console.log(resultItemsDoc);						
						//loop the array
						for (var j=0; j< resultItemsDoc.length; j++){
						
							var currentItemDoc = resultItemsDoc[j];
							//retrieving the fields of the results
							var titleDoc = currentItemDoc.Title;
							var urlDoc = currentItemDoc.ServerRedirectedEmbedUri;
							var docIcon = currentItemDoc.DocIcon;
							if(docIcon == "pdf"){
								//adding values to the parameters
								urlDoc = currentItemDoc.FileRef;
								var bodyNew = String.format(tabContent.parameters.bodyLibrary,"ic"+docIcon+".png",urlDoc,titleDoc);
							}else
							 if(docIcon == "txt"){
								//adding values to the parameters
								urlDoc = currentItemDoc.FileRef;
								var bodyNew = String.format(tabContent.parameters.bodyLibrary,"ic"+docIcon+".gif",urlDoc,titleDoc);
								
							}
							else
							{
								var bodyNew = String.format(tabContent.parameters.bodyLibrary,"ic"+docIcon+".gif",urlDoc,titleDoc);

							}
							console.log(bodyNew);
							//adding values of parameters to the html variable
							htmlDocs += bodyNew;
						}
						
						
						var tabPrincipal = document.getElementById(title.replace(/\s/g,''));
						tabPrincipal.innerHTML = htmlDocs;
									
					},function(xhr){ console.log(xhr)});
				}
			}
			//adding values of parameters to the html variable
			html += String.format(tabContent.parameters.contaierListTabs, htmlIndicators);
			html += String.format(tabContent.parameters.containerContent, htmltabs);
			console.log(htmltabs);	
			//insert the html code
			document.getElementById(tabContent.parameters.divID).innerHTML = html;	
		}  
	}
}

sputilities.callFunctionOnLoadBody('pageLayoutPae.load');
var pageLayoutPae = {
	components:{
		staticNews : staticNews,
		sliderNews : sliderNews,
		events: events,
		frecuentQuestions: frecuentQuestions,
		sliderImg : sliderImg,
		tabContent : tabContent		
	},
	load:function(){
		pageLayoutPae.initComponents();
		var loadControl = setInterval(function(){
			document.getElementById('ms-designer-ribbon').style.opacity = 0;
			document.getElementById('s4-workspace').style.opacity = 0;
			if( pageLayoutPae.componentsList.length  == pageLayoutPae.max){
				for(var i = 0; i <  pageLayoutPae.componentsList.length ; i++){
					 pageLayoutPae.componentsList[i].insertHTML();
				}
				clearInterval(loadControl);	
				pageLayoutPae.animations();
				//document.getElementById("img-load").style.display = "none";
				document.getElementById('s4-workspace').style.opacity = 1;
				document.getElementById('ms-designer-ribbon').style.opacity = 1;
				setTimeout(function(){	
				},5000);							
			}
		},500);	

	},
	initComponents:function(){
		pageLayoutPae.components.sliderNews.initialize();
		pageLayoutPae.components.staticNews.initialize();
		pageLayoutPae.components.events.initialize();
		pageLayoutPae.components.frecuentQuestions.initialize();
		pageLayoutPae.components.sliderImg.initialize();
		pageLayoutPae.components.tabContent.initialize();

	},
	animations:function(){	
		// Slider Noticias
		$.noConflict();
		$('#carousel-header').on('slid.bs.carousel', function () {
		    $holder = $( "ol li.active" );
		    $holder.removeClass('active');
		    var idx = $('div.active').index('div.item');
		    $('ol.carousel-indicators li[data-slide-to="'+ idx+'"]').addClass('active');
		});
		
		$('ol.carousel-indicators  li').on("click",function(){ 
		    $('ol.carousel-indicators li.active').removeClass("active");
		    $(this).addClass("active");
		});

		$(".carousel").swipe({
		
		 	swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
		
		    if (direction == 'left') $(this).carousel('next');
		    if (direction == 'right') $(this).carousel('prev');
		
		  },
		  allowPageScroll:"vertical"
		
		  });
		
		
		$('#gallery-images').slick({
			  dots: false,
			  infinite: true,
			  speed: 700,
			  slidesToShow: 3,
			  slidesToScroll: 1,
			  responsive: [
			    {
			      breakpoint: 1024,
			      settings: {
			        slidesToShow: 2,
			        slidesToScroll: 1,
			        infinite: true,
			        dots: false
			      }
			    },
			    {
			      breakpoint: 600,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    },
			    {
			      breakpoint: 480,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    }
			  ]
			});
	},		
	componentsList:[],
	max:6

}