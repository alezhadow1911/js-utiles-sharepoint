﻿var teamPhotos = {
	//create parameters
	parameters:{
			listName: "Imagenes-Convenios",
			top: 5,
			divID:"team-images",
			contentType: "la imagen",
			detailParameter: "Imagen",
			containerSlide:"<div class='list-team'>{0}</div><div class='container-controls-team'><div class='slider-tag-team'><span></span></div></div>",
			containerItem:"<div class='team-item'><div class='team'>{0}</div></div>",
			
			bodyItem:"<div class='team-content'><img src='{0}' class='img-responsive' /><div class='team-text'><h3>{1}</h3><h2>{2}</h2></div></div>",
			query: "?$select=Title,Description,FileRef,ContentType/Name,FechaInicio,FechaFinal,ID,Orden&$expand=ContentType&$filter=(FechaFinal ge datetime'{2}' and FechaInicio le datetime'{2}' and ContentType eq '{0}')&$orderby=Created desc&$top={1}",
			resultItems:null
	
	},	
	initialize:function(){
		try {
		var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	

		
			//adding parameters to query
			var linkQuery = String.format(teamPhotos.parameters.query,teamPhotos.parameters.contentType,teamPhotos.parameters.top,date);
			
			//running the query with parameters
			sputilities.getItems(teamPhotos.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				teamPhotos.parameters.resultItems = resultItems;
				teamPhotos.insertHTML();	
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var data = teamPhotos.parameters.resultItems;
		if(data.length > 0)
		{
			//loop the array
			for (var i=0; i< data.length; i++){
					

				var currentItem = data[i];
				//retrieving the fields of the results
				var ID = currentItem.ID;
				var urlImg = currentItem.FileRef;
				var contentType = currentItem.ContentType.Name;
				var title = currentItem.Title;	
				console.log(urlImg);		
				var description = currentItem.Description;
				
				if(title != null && description != null)
				{
					//adding values to the parameters
					var bodyNew = String.format(teamPhotos.parameters.bodyItem,urlImg,title,description);	
				}
				else
				{
					var bodyNew = String.format(teamPhotos.parameters.bodyItem,urlImg,"","");	
	
				}			
				var containerNew = String.format(teamPhotos.parameters.containerItem,bodyNew);
	
				//adding values of parameters to the html variable
				html += containerNew;
													
			}			
			
			var htmlNew = String.format(teamPhotos.parameters.containerSlide, html);
			//insert the html code
			document.getElementById(teamPhotos.parameters.divID).innerHTML = htmlNew;
		}
		
	
	}
}

var Content = {
	//create parameters
	parameters:{
	
			listName: "Imagenes-Convenios",
			top: 5,
			ContentT : 'la imagen',
			divID:"team-images",
			contentType: "la imagen",
			detailParameter: "Imagen",
			containerSlide:"<div class='list-team'>{0}</div><div class='container-controls-team'><div class='slider-tag-team'><span></span></div></div>",
			containerItem:"<div class='team-item'><div class='team'>{0}</div></div>",
			
			bodyItem:"<div class='team-content'><img src='{0}' class='img-responsive' /><div class='team-text'><h3>{1}</h3><h2>{2}</h2></div></div>",
			query: "/contenttypes?$select=Name,Fields/TypeAsString&$expand=Fields&$filter=Name eq '{0}'",
			resultItems:null
	
	},	
	initialize:function(){
		try {
		var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	

		
			//adding parameters to query
			var linkQuery = String.format(Content.parameters.query,Content.parameters.contentT);
			
			//running the query with parameters
			sputilities.getItems(Content.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				console.log("ingreso al content"+resultItems); 
				
			
			},
			);
		}
		catch (e){
			console.log(e);
		}
	},
	
}


/* Component: Slider de Categorias de los convenios */
var sliderCategory = {
	//create parameters
	parameters:{
			listName: "Categorias",
			listNameDetail:"Convenios",
			top: 18,
			divID:"agreements-slider",
			divIDText:"text-int-detail",
			divIDDetail:"list-agreements-content",
			bodyCategoryDesc:"<h1 class='title-agreement'>{0}</h1>"
							 +"<p class='desc-agreement'>{1}</p>",
			bodyItemCategory:"<div class='item-agreement-slider'>"
								+"<a href='#text-int-detail' onclick='javascript:sliderCategory.click({0},this);'>"
									+"<div class='container-img-agreement'>"
										+"<img class='img-icon-agreement' src='{1}'/>"
									+"</div>"
									+"<div class='container-text-agreement'>"
										+"<h3>{2}</h3>"
										+"<img class='img-seemore' src='../../SiteAssets/Development/images/beneficios/agreements/ico-seemore.png'>"
									+"</div>"
									+"<div class='desc-agreement' style='display:none'>{3}</div>"
								+"</a>"
							 +"</div>",
			bodyItemDetails:"<div class='item-agreement'>"
							  +"<a href='javascript:void(0);' onclick='{0}'>"
							  +"<div class='container-item-agreement'>"
								  +"<img src='{1}'/>"
								  +"<h3>{2}</h3>"
							  +"</div>"
							  +"</a>"
							+"</div>",
			containerItemDetails:"<div class='list-agreements' id='{0}' style='display:none'>{1}</div>",
			query:"?$select=ID,Title,Descripcion,Orden,sys_txtImg&$orderby=Orden&$top={1}",
			queryDetails:"?$select=ID,Title,Empresa,sys_txtImg,CategoriaId&$top=5000",
			modal:'javascript:sputilities.showModalDialog("Convenio","{0}",window.location.href);',
			listUrl:"{0}/Lists/{1}",
			dispForm:"{0}/Lists/Convenios/Detalle.aspx?ID={1}&Source={2}",	
			detailUrl:"{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo=Convenios",
			resultItemsCategory:null,
			resultItemsDetails:null
	},
	initialize:function(){
		try {
			//adding parameters to query
			var linkQuery = String.format(sliderCategory.parameters.query,sliderCategory.parameters.top);
			//running the query with parameters
			sputilities.getItems(sliderCategory.parameters.listName,linkQuery,function(dataCategory){
				var resultItems = JSON.parse(dataCategory).d.results;
				sliderCategory.parameters.resultItemsCategory = resultItems;
			},function(xhr){ console.log(xhr)});		
			linkQuery = String.format(sliderCategory.parameters.queryDetails);
			sputilities.getItems(sliderCategory.parameters.listNameDetail,linkQuery,function(data){
				var resultItems = JSON.parse(data).d.results;
				sliderCategory.parameters.resultItemsDetails = resultItems;
			},function(xhr){ console.log(xhr)});
			var validateControl = setInterval(function(){
				document.getElementById('ms-designer-ribbon').style.opacity = 0;
				document.getElementById('s4-workspace').style.opacity = 0;
				if(sliderCategory.parameters.resultItemsDetails != null && sliderCategory.parameters.resultItemsCategory != null){
					sliderCategory.insertHTML();
					document.getElementById('ms-designer-ribbon').style.opacity = 1;
					document.getElementById('s4-workspace').style.opacity = 1;
					clearInterval(validateControl);
				}
			},500);
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		sliderCategory.insertCategories();
		sliderCategory.insertAgreements();
		sliderCategory.animate();	
		var a = document.querySelector('.item-agreement-slider:first-child a');
		a.onclick();

	},
	insertCategories:function(){
		var html = "";
		var htmlDetails = "";
		var htmlSliders ="";
		var data = sliderCategory.parameters.resultItemsCategory;
		//loop the array
		for (var i=0; i< data.length; i++){
			var currentItem = data[i];
			if(currentItem.sys_txtImg != null)
			{
				//retrieving the fields of the results
				var title = currentItem.Title;
				var id = currentItem.Id;
				var desc = currentItem.Descripcion;	
				//taking the url of the image
				var image = currentItem.sys_txtImg;
				var div = document.createElement('div');
				div.innerHTML = image;
				var urlimg = div.querySelector('img').src;	
				htmlSliders += String.format(sliderCategory.parameters.bodyItemCategory,id,urlimg,title,desc);			 
				/*if (i==0){
					html += String.format(sliderCategory.parameters.bodyCategoryDesc,title,desc);
				}*/
			}
		}
		//document.getElementById(sliderCategory.parameters.divIDText).innerHTML = html;
		document.getElementById(sliderCategory.parameters.divID).innerHTML = htmlSliders;	
	},
	insertAgreements:function(){
		var resultDetails = sliderCategory.parameters.resultItemsDetails;
		var obj = {};
		for (var j=0; j< resultDetails.length; j++){
			var currentItem = resultDetails[j];
			if(currentItem.sys_txtImg != null){	
				var divDetail = document.createElement('div');
				divDetail.innerHTML = currentItem.sys_txtImg;
				var urlimgDetail = divDetail.querySelector('img').src;					
				var detailURL = String.format(sliderCategory.parameters.dispForm,sputilities.contextInfo().webAbsoluteUrl,currentItem.Id,sputilities.contextInfo().webAbsoluteUrl);
				var htmlModal = String.format(sliderCategory.parameters.modal,detailURL);							
				if(obj[currentItem.CategoriaId] == null){
					obj[currentItem.CategoriaId] = "";
				}
				obj[currentItem.CategoriaId] += String.format(sliderCategory.parameters.bodyItemDetails,htmlModal,urlimgDetail,currentItem.Empresa);
			}
		}	
		var htmlContainer = "";
		for(var category in obj){
			htmlContainer += String.format(sliderCategory.parameters.containerItemDetails,category,obj[category]);
		}				
		document.getElementById(sliderCategory.parameters.divIDDetail).innerHTML = document.getElementById(sliderCategory.parameters.divIDDetail).innerHTML + htmlContainer;
	},
	click:function(idprincipal,ctrl){
		var slide = document.getElementById(idprincipal);
		if(slide != null){
			var nodes = slide.parentNode.childNodes;
			for(var i = 0 ; i < nodes.length ; i++){
				if(nodes[i].nodeName != '#text'){
					nodes[i].style.display = "none";
				}
			}
			slide.style.display = "block";
			$(slide).slick('setPosition');
			document.getElementById(sliderCategory.parameters.divIDText).innerHTML = String.format(sliderCategory.parameters.bodyCategoryDesc,ctrl.querySelector('h3').innerText,ctrl.querySelector('.desc-agreement').innerText);
		}
	},
	animate:function(){
		$.noConflict();
		$('.list-agreements-slider').slick({
		  dots: true,
		  infinite: true,
		  speed: 700,
		  rows:3,
		  slidesToShow: 3,
		  slidesToScroll: 3,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		      	rows:3,
		        slidesToShow: 3,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: true
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		      	rows:2,
		        slidesToShow: 2,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		      	rows:2,
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
		});
		//Slider Equipo
		$('.list-team').slick({
		  dots: false,
		  infinite: true,
		  speed: 700,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  autoplay: true,
  		  autoplaySpeed: 2000,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
		});

		$('.list-agreements').slick({
			  dots: false,
			  infinite: true,
			  speed: 700,
			  rows:2,
			  slidesToShow: 4,
			  slidesToScroll: 4,
			  responsive: [
			    {
			      breakpoint: 1024,
			      settings: {
			       	rows:2,
			        slidesToShow: 2,
			        slidesToScroll: 1,
			        infinite: true,
			        dots: false
			      }
			    },
			    {
			      breakpoint: 600,
			      settings: {
			      	rows:2,
			        slidesToShow: 2,
			        slidesToScroll: 1
			      }
			    },
			    {
			      breakpoint: 480,
			      settings: {
			      	rows:2,
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    }
			  ]
		});


	}
}
sputilities.callFunctionOnLoadBody('sliderCategory.initialize');
sputilities.callFunctionOnLoadBody('teamPhotos.initialize');
sputilities.callFunctionOnLoadBody('Content.initialize');