﻿/* Components: Detalle de los beneficios */
var detailsBenfits = {
	//create parameters
	parameters:{
			listName:"Beneficios",
			selectorClassContainer:".center-container",
			divIdDetails:"module-benefitdetail",
			detailUrlImage:"{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo=ImagenBeneficio",
			detailUrlVideo:"{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo=VideoBeneficio",
			optionDefault:"<option value='0'>Seleccione un país</option>",
			termSetId:"c9c4f4ec-e06e-4a30-a0d0-b735cbbe7912",
			bodyAssetItemVideo:"<div class='benefitdetail-image'>"
					            +"<img src='{0}'>"
					            +"<a onclick='javascript:detailsBenfits.showVideo(this)' class='circulo bg-play centrado-total'>"
					                +"<i class='inc-play centrado-total'></i>"
					                +"<div style='display:none' controls><video style='width:100%;padding-bottom: 40px;padding-top: 40px;' preaload controls=''><source src=\"{1}\"/></video></div>"
					            +"</a>"
					        +"</div>",
			bodyAssetItemImage:"<div class='benefitdetail-image'>"
					            +"<img src='{0}'>"
					       +"</div>",
			bodySectionDetails:"{0}<div class='benefitdetail-text'>"
				            +"<div class='benefitdetail-text-int'>"
				                +"<h1>{1}</h1>"
				                +"<h2>DEFINICIÓN</h2>"
				                +"<p>{2}</p>"
				                +"<div class='benefitdetail-country'>"
				                    +"<label>"
				                        +"<img src='../../SiteAssets/Development/images/beneficios/benefitDetail/ico-country.png'><p>País</p>"
				                    +"</label>"
				                    +"<select onChange='detailsBenfits.changeCountry(this);' id='countries'>{3}</select>"
				                +"</div>"
				            +"</div>"
				        +"</div>",
			bodyRequirements:'<section class="info-requirements country-hidden" data-country="{0}">' +
							    '<div class="module-requirements">' +
							        '<div class="requirements-int">' +
							            '<h1>Requerimiento</h1>' +
							            '<h2>Para acceder al beneficio se tienen los siguientes requerimientos</h2>' +
							            '<ul class="requirements-list">{1}</ul>' +
							            '<div class="requirements-seemore">' +
							                '<a href="javascript:sputilities.showModalDialog(\'beneficio\',\'{2}/Lists/Beneficios/modificar.aspx?ID={3}\',window.location.href)">Cargar más</a>' +
							            '</div>' +
							        '</div>' +
							    '</div>' +
							'</section>',
			bodyItemRequeriments:'<li>' +
								    '<div class="requirements-number">' +
								        '<div class="requirements-number-circle">' +
								            '<span>{0}</span>' +
								        '</div>' +
								    '</div>' +
								    '<p>{1}</p>' +
								'</li>',
			bodyInfoBenefits:'<section class="info-benefit country-hidden" data-country="{0}">' +
							    '<div class="row">' +
							        '<div class="col-md-4 col-sm-12 col-xs-12">' +
							            '<div class="module-moreinfo">' +
							                '<div class="moreinfo-content">' +
							                    '<h2>Aplicabilidad</h2>' +
							                    '{1}'+
							                '</div>' +
							                '<div class="moreinfo-buttons">' +
							                    '<a href="{2}" target="_blank" class="moreinfo-bmoreinfo">' +
							                        'Más información' +
							                    '</a>' +
							                    '<a href="{3}" target="_blank" class="moreinfo-bformat">' +
							                        'Formato' +
							                    '</a>' +
							                '</div>' +
							            '</div>' +
							        '</div>' +
							        '<div class="col-md-8 col-sm-12 col-xs-12">' +
							            '<div class="tittle-keyquestions">' +
							                '<h5>Preguntas claves</h5>' +
							            '</div>' +
							            '<div class="panel-group module-keyquestions" id="accordion" role="tablist" aria-multiselectable="true">' +
							                '{4}'+
							            '</div>' +
							        '</div>' +
							    '</div>' +
							'</section>',
			bodyItemApply:'<div class="moreinfo-item">' +
							    '<h3>{0}</h3>' +
							    '<p>{1}</p>' +
							'</div>',
			bodyItemPanel:'<div class="panel panel-default">' +
						    '<div class="panel-heading" role="tab" id="heading-{2}-{3}">' +
						        '<div class="panel-title">' +
						        '<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{2}-{3}" aria-expanded="false" aria-controls="collapse-{2}-{3}">' +
						        '<i class="keyquestions-ico"></i>' +
						        '<h3>{0}</h3>' +
						        '</a>' +
						        '</div>' +
						    '</div>' +
						    '<div id="collapse-{2}-{3}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-{2}-{3}">' +
						        '<div class="panel-body">' +
						        '<p>{1}</div>' +
						    '</div>' +
						'</div>',
			bodyAgreementsSection:'<section class="agreements country-hidden" data-country="{0}">' +
								    '<h1 class="title-agreement">Convenios</h1>' +
								    '<p class="desc-agreement">{1}</p>' +
								    '<div class="agreements-content">' +
								        '<div  id="list-agreements-{0}" class="list-agreements">' +
								            '{2}' +
								            '</div>' +
								    '</div>' +
								'</section>',
			bodyItemAgreement:'<div class="item-agreement">' +
							    "<a href='javascript:sputilities.showModalDialog(\"Convenio\",\"{0}/Lists/Convenios/Detalle.aspx?ID={1}\")'>" +
							        '<div class="container-item-agreement">' +
							            '{2}' +
							            '<h3>{3}</h3>' +
							        '</div>' +
							    '</a>' + 
							'</div>' ,
			countryItems:"<option value='{0}'>{1}</option>",	        
			queryBenefit:"?$top=5000&$select=ID,Title,Descripcion,DescripcionConvenios,OData__sys_requerimientos,OData__sys_aplicabilidad,OData__sys_preguntasclaves,OData__sys_convenios,OData__sys_formatos,OData__sys_activo,OData__sys_masinformacion&$filter=ID eq {0}",
			detailUrl:"",
			queryStringParameter:"IdBeneficio",
			benefitId:null,
			resultItemsCountries:null,
			resultItemBenefit:null
	},
	initialize:function(){
		try {
			detailsBenfits.parameters.benefitId = utilities.getQueryString(detailsBenfits.parameters.queryStringParameter);
			//running the query with parameters
			sputilities.executeDelayspTaxonomy(function(){
			   sputilities.getTermsInTermSet({
			     success:function(terms){
			      	detailsBenfits.parameters.resultItemsCountries = terms;
			     },
			     termSetId:detailsBenfits.parameters.termSetId
			   });
  			});	
  			var linkQuery = String.format(detailsBenfits.parameters.queryBenefit,detailsBenfits.parameters.benefitId);	
			sputilities.getItems(detailsBenfits.parameters.listName,linkQuery,function(data){
				var resultItems = JSON.parse(data).d.results;
				detailsBenfits.parameters.resultItemBenefit = resultItems;
		
			},function(xhr){ console.log(xhr)});
			var insertDetail = setInterval(function(){
				if(detailsBenfits.parameters.resultItemsCountries != null && detailsBenfits.parameters.resultItemBenefit != null){
					detailsBenfits.insertHTML();
					document.getElementById('s4-workspace').style.opacity = 1;
					
					document.getElementById('ms-designer-ribbon').style.opacity = 1;
					clearInterval(insertDetail);
				}
			},500);
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
	  	var data = detailsBenfits.parameters.resultItemBenefit;
	  	var detailSection = "";
	  	var html = "";
		for (var i=0; i < data.length; i++){
			var currentItem = data[i];
			detailSection = detailsBenfits.getDetailSection(currentItem);
			var requerimentSection = detailsBenfits.getRequirementsSection(currentItem);
			var moreInfo = detailsBenfits.getMoreInfoSection(currentItem);
			var agreement = detailsBenfits.getAgreementSection(currentItem);
			html = requerimentSection + moreInfo + agreement ;
		}
		document.getElementById(detailsBenfits.parameters.divIdDetails).innerHTML = detailSection; 
		var container = document.querySelector(detailsBenfits.parameters.selectorClassContainer);
		container.innerHTML += html;
		detailsBenfits.animate();
		
	},
	getCountries:function(){
		var enumerator = detailsBenfits.parameters.resultItemsCountries.getEnumerator();
      	var selectCountries = document.createElement('select');
      	selectCountries.setAttribute("id", "countries");
      	var htmlCountries = "";
      	var i = 0;
        while(enumerator.moveNext()){
      	 	var term  = enumerator.get_current();
         	var name =  term.get_name();
         	var id = term.get_id();
      	 	if(i == 0){
         		htmlCountries += String.format(detailsBenfits.parameters.optionDefault);
         		htmlCountries += String.format(detailsBenfits.parameters.countryItems,i,name);
         	}else{
         		htmlCountries += String.format(detailsBenfits.parameters.countryItems,i,name);
         	}
         	i++;
      	}
      	selectCountries.innerHTML = htmlCountries;
      	return selectCountries;
	},
	getRequirementsSection:function(currentItem){
		var reqs = utilities.convertToJSON(currentItem.OData__sys_requerimientos);
		if(reqs != null){
			var htmlReqs= "";
			for(var country in reqs){
				var reqs4Country = reqs[country];
				var htmlReqs4Country = "";
				for(var i = 0; i < reqs4Country.length ; i++){
					var req = reqs4Country[i];
					var num = "0";
					if(i < 10){
						num += (i+1);
					}else{
						num = i.toString();
					}
					htmlReqs4Country += String.format(detailsBenfits.parameters.bodyItemRequeriments,num,req.descripcion);
				}
				htmlReqs += String.format(detailsBenfits.parameters.bodyRequirements,country.replaceAll(' ','_'),htmlReqs4Country,sputilities.contextInfo().webAbsoluteUrl,detailsBenfits.parameters.benefitId);
			}
			return htmlReqs;
		}
	},
	getDetailSection:function(currentItem){
		var selectCountries = detailsBenfits.getCountries();
		var title = currentItem.Title;
		var desc = currentItem.Descripcion;
		var asset = utilities.convertToJSON(currentItem.OData__sys_activo);
		var htmlAsset ="";
		if(asset != null){
			if (asset.tipo == 0){
				htmlAsset = String.format(detailsBenfits.parameters.bodyAssetItemVideo,asset.urlPoster,asset.url);
			}else{
				htmlAsset  = String.format(detailsBenfits.parameters.bodyAssetItemImage,asset.url);
			}
		}
		return String.format(detailsBenfits.parameters.bodySectionDetails,htmlAsset,title,desc,selectCountries.innerHTML)
	},
	getMoreInfoSection:function(currentItem){
		var apply = utilities.convertToJSON(currentItem.OData__sys_aplicabilidad);
		var questions = utilities.convertToJSON(currentItem.OData__sys_preguntasclaves);
		var moreInfo = utilities.convertToJSON(currentItem.OData__sys_masinformacion);
		var formats = utilities.convertToJSON(currentItem.OData__sys_formatos);
		var htmlSection = "";
		for(var country in apply){
			var apply4Country = apply[country];
			var htmlApply = "";
			for(var i = 0; i < apply4Country.length;i++){
				var applyCountry = apply4Country[i];
				htmlApply += String.format(detailsBenfits.parameters.bodyItemApply,applyCountry.titulo,applyCountry.descripcion)
			}
			var questions4Country = questions[country];
			var htmlquestions = "";
			for(var i = 0; i < questions4Country.length;i++){
				var question = questions4Country[i];
				htmlquestions += String.format(detailsBenfits.parameters.bodyItemPanel,question.pregunta,question.respuesta,country.replaceAll(' ','_'),i)
			}
			htmlSection += String.format(detailsBenfits.parameters.bodyInfoBenefits,country.replaceAll(' ','_'),htmlApply,moreInfo[country],formats[country],htmlquestions );
		}
		return htmlSection;
	},
	getAgreementSection:function(currentItem){
		var agreements = utilities.convertToJSON(currentItem.OData__sys_convenios);
		var descAgrements = currentItem.DescripcionConvenios;
		if(agreements != null){
			var htmlAgreementsSection = "";
			for(var country in agreements){
				var agreements4Country = agreements[country];
				var htmlAgreements = ""
				for(var i = 0; i < agreements4Country.length;i++){
					var agreement = agreements4Country[i];
					htmlAgreements += String.format(detailsBenfits.parameters.bodyItemAgreement,sputilities.contextInfo().webAbsoluteUrl,agreement.id,agreement.imagen,agreement.titulo);
				}
				htmlAgreementsSection += String.format(detailsBenfits.parameters.bodyAgreementsSection,country.replaceAll(' ','_'),descAgrements,htmlAgreements)
			}
		}
		return htmlAgreementsSection;
	},
	showVideo:function(ctrl){
		var video = ctrl.querySelector('div video');
		var cloneVideo = video.cloneNode(true);
		cloneVideo.setAttribute('autoplay','true');
		sputilities.showModalDialog('Video',null,null,cloneVideo.outerHTML,function(){
			cloneVideo.pause();
		});
	},
	changeCountry:function(select){
		var index = parseInt(select.value) + 1;
		var country = select[index].innerText;
		$(String.format('[data-country="{0}"]',country.replaceAll(' ','_'))).removeClass('country-hidden');
		$(String.format('[data-country="{0}"]',country.replaceAll(' ','_'))).show('slow');
		$(String.format('section:not([data-country="{0}"]):not(.benefitdetail)',country.replaceAll(' ','_'))).hide('slow');
		console.log(String.format('#list-agreements-{0}',country.replaceAll(' ','_')));
		console.log(country.replaceAll(' ','_'));
		$(String.format('#list-agreements-{0}',country.replaceAll(' ','_'))).slick('setPosition');
	},
	animate:function(){
		if($('.list-agreements').length > 0){
			$('.list-agreements').slick({
				  dots: false,
				  infinite: true,
				  speed: 700,
				  rows:2,
				  slidesToShow: 4,
				  slidesToScroll: 1,
				  responsive: [
				    {
				      breakpoint: 1024,
				      settings: {
				       	rows:2,
				        slidesToShow: 2,
				        slidesToScroll: 1,
				        infinite: true,
				        dots: false
				      }
				    },
				    {
				      breakpoint: 600,
				      settings: {
				      	rows:2,
				        slidesToShow: 2,
				        slidesToScroll: 1
				      }
				    },
				    {
				      breakpoint: 480,
				      settings: {
				      	rows:2,
				        slidesToShow: 1,
				        slidesToScroll: 1
				      }
				    }
				  ]
				});
		}
	}
}
document.getElementById('ms-designer-ribbon').style.opacity = 0;
document.getElementById('s4-workspace').style.opacity = 0;
sputilities.callFunctionOnLoadBody('detailsBenfits.initialize');