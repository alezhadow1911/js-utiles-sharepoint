﻿/*Component: Noticias Estaticas */

var staticNews = {
	//create parameters
	parameters:{
			listName: "Noticias Estáticas",
			top: 2,
			divID:"static-news",
			containerNews:"<div class='otherNewsVP-item'>{0}{1}</div>",			
			imageNews:"<img src='{0}' />",
			textNews:"<div class='otherNewsVP-item-text'><h3>{0}</h3><p>{1}</p><a href='{2}' target='_blank'>Ver más<i class='inc-arrow-3 centrado-y'></i></a></div>",
			query: "?$select=Title,FechaFinal,FechaInicio,Url,sys_txtImg,Descripcion,Orden&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}')&$orderby=Orden&$top={1}",
			resultItems:null		
	
	},
	initialize:function(){
		try {

			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	
			
			//add current date to query and filter
			var linkQuery = String.format(staticNews.parameters.query,date,staticNews.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(staticNews.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				staticNews.parameters.resultItems = resultItems;
				 pageLayoutVP.componentsList.push(staticNews);
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var data = staticNews.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];
			
			//retrieving the fields of the results
			var title = currentItem.Title;
			var description = currentItem.Descripcion;			
			var url = currentItem.Url.Url;
				
			//taking the url of the image
			var image = currentItem.sys_txtImg;
			var div = document.createElement('div');
			div.innerHTML = image;
			var urlimg = div.querySelector('img').src;			
					
			//make html with the fields 
			var imageHtml = String.format(staticNews.parameters.imageNews, urlimg);
			var textHtml = String.format(staticNews.parameters.textNews,title,description,url);			
					
			//adding html 
			html += String.format(staticNews.parameters.containerNews,imageHtml,textHtml);			
					
		}
			
		//insert the html code
		document.getElementById(staticNews.parameters.divID).innerHTML = html;
	}
}

/* Component: Eventos */

var events = {
//create parameters
	parameters:{
		listName: "Eventos",
		containerId:"items-events",	
		moreEventsId:"more-events",
		top: 6,	
		mounth:["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC"],
		query: "?$select=ID,Title,Description,EventDate,EndDate,fAllDayEvent&$filter=EndDate ge datetime'{0}'&$orderby=EventDate&$top={1}",
		containerEven:"<div class='item-events'>{0}{1}</div>",
		textEvent:"<div class='item-events-text'><h2>{0}</h2><p>{1}</p></div>"+
				  "<div class='item-events-date'><h3>{2}</h3><span>{3}</span></div>"+
				  "<div class='item-events-hour'><h3>{4}</h3><span>{5}</span></div>",
		linkEvent:"<div class='item-events-text-arrow' onclick='{0}'><a href='javascript:void(0);'><i class='inc-arrow-3 centrado-y'></i></a></div>",
		modal:'javascript:sputilities.showModalDialog("Evento","{0}",window.location.href);',
		listUrl:"{0}/Lists/{1}",
		dispForm:"{0}/DispForm.aspx?ID={1}",	
		detailUrl:"{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo=Eventos",
		linkAll:"<a href='{0}'>Ver todos <i class='inc-arrow-3 centrado-y'></i></a>",		
		resultItems:null	
	},
	initialize:function(){
		//Get current date value
		var today = new Date;
		today = today.toISOString();
		//We build the query			
		events.parameters.query = String.format(events.parameters.query,today,events.parameters.top)
		try{
			sputilities.getItems(events.parameters.listName,events.parameters.query,function(data){
			//We get the url from the list of events
				
				var results = JSON.parse(data).d.results;
				events.parameters.resultItems = results;
				 pageLayoutVP.componentsList.push(events);
					
						
					
			},function(data){
				console.log(JSON.parse(data).error.message.value)
			});		
		}catch(e){			
			console.log(String.format('Ocurrio un error inesperado en el metodo events.initialize {0}',e))
		}				
				
	},
	insertHTML: function(){
		var data = events.parameters.resultItems;
		var html = "";
		var listUrl = String.format(events.parameters.listUrl,sputilities.contextInfo().webAbsoluteUrl,events.parameters.listName)	;	
		//We construct the section of the event for each event in the result set
		for (var i = 0;i<data.length;i++){
			
			var form = String.format(events.parameters.dispForm,listUrl,data[i].ID);
			
			
			var allday = data[i].fAllDayEvent;
			console.log(allday)
			if(allday == true){
			var DateSite =data[i].EventDate.replace("Z","")	
			var startDateTime = new Date(DateSite);	
			var startDay =('0' +  startDateTime.getUTCDate()).slice(-2);
			var startMonth = events.parameters.mounth[startDateTime.getMonth()];
			eventTime = "el día";
			eventAMPM= "Todo";
			}
			else{
			
			var startDateTime = new Date(data[i].EventDate);
			var endDateTime = new Date(data[i].EndDate);							
			var startDay =('0' +  startDateTime.getUTCDate()).slice(-2);
			var startMonth = events.parameters.mounth[startDateTime.getMonth()];
			
			
			var startHour="";
			var startMinutes= "";
				if(startDateTime.getHours() > 12)
				{
					var hour = startDateTime.getHours() - 12;
					startHour = ('0' + hour).slice(-2);
					startMinutes = ('0' + startDateTime.getMinutes()).slice(-2);
					eventAMPM="PM";
					eventTime = String.format("{0}:{1}",startHour,startMinutes);	
				}
				else
				{
					startHour = ('0' + startDateTime.getHours()).slice(-2);
					startMinutes = ('0' + startDateTime.getMinutes()).slice(-2);
					eventAMPM="AM";
					eventTime = String.format("{0}:{1}",startHour,startMinutes);	
				}}
				
		
			var detailURL = String.format(events.parameters.detailUrl,sputilities.contextInfo().webAbsoluteUrl,events.parameters.listName,data[i].ID);
			var htmlModal = String.format(events.parameters.modal,detailURL);
			var htmlLink = String.format(events.parameters.linkEvent,htmlModal);
			var htmlBody = String.format(events.parameters.textEvent,data[i].Title,data[i].Description,startMonth,startDay,eventAMPM,eventTime);
			
			html += String.format(events.parameters.containerEven,htmlBody,htmlLink);
					
		}
			
		document.getElementById(events.parameters.containerId).innerHTML = html;
		var htmlAll = String.format(events.parameters.linkAll,listUrl);
		document.getElementById(events.parameters.moreEventsId).innerHTML=htmlAll ;
					
	}

}

/* Components: Slider Cifras */

var graphicsImg = {
	//create parameters
	parameters:{
			listName:"Cifras",
			top: 6,
			divID:"information-numbers",
			bodyTab:"<div class='important'>{0}<img src='{1}' class='img-responsive' />{2}<h5>{3}</h5></div>",
			linkImg:"<a href='{0}'>",
			query: "?$select=sys_txtImg,Orden,Title,UrlCifras&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}')&$orderby=Orden&$top={1}",
			resultItems:null
	
	},
	
	initialize:function(){
		try {
		
			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	

			//adding parameters to query
			var linkQuery = String.format(graphicsImg.parameters.query,date,graphicsImg.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(graphicsImg.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				
				graphicsImg.parameters.resultItems = resultItems;
				 pageLayoutVP.componentsList.push(graphicsImg);
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var data = graphicsImg.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];
			//retrieving the fields of the results
			var title =	currentItem.Title;
			//taking the url of the image
			var image = currentItem.sys_txtImg;
			var div = document.createElement('div');
			div.innerHTML = image;
			var urlimg = div.querySelector('img') != null ? div.querySelector('img').src : "";
							
			//adding values to the parameters
			
			var url = currentItem.UrlCifras;
			var bodyNew = "";
			if(url != null)
			{
				var link = String.format(graphicsImg.parameters.linkImg,url.Url);
				bodyNew = String.format(graphicsImg.parameters.bodyTab,link,urlimg,"</a>",title);				
			}
			else
			{
				bodyNew = String.format(graphicsImg.parameters.bodyTab,"",urlimg,"", title);	
			}			
		
			//adding values of parameters to the html variable
			html += bodyNew;
					
		}
			
		//insert the html code
		document.getElementById(graphicsImg.parameters.divID).innerHTML = html;
	}
}

/* Component: Slider Noticias */
var sliderNews = {
	//create parameters
	parameters:{
			listName: "Páginas",
			top: 7,
			divID:"carousel-header",
			detailParameter: "Noticias",
			containerIndi:" <ol class='carousel-indicators'>{0}</ol>",
			bodyIndicators:" <li data-target='#carousel-header' data-slide-to='{0}'{1}></li>",
			containerSlides:" <div class='carousel-inner slider' role='listbox'>{0}</div>",
			bodyItemSlide:"<div class='item{0}' id='slide-0{1}'> <img src='{2}' alt='...'><div class='carousel-caption'><h2>{3}</h2><p>{4}</p><a href='{5}' target='_self' class='btn light-red-button'>Más información</a></div></div>",
			detailUrl:"{0}?ListaBase={1}&IDItem={2}&Tipo=Noticias",
			modal:'sputilities.showModalDialog("Noticia","{0}",window.location.href)',
			query: "?$select=FileRef,Title,ArticleStartDate,Entradilla,PublishingPageContent,ID,FechaInicio,FechaFinal,Orden,sys_txtImg&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}' and ContentType eq 'Noticias')&$orderby=Orden&$top={1}",
			resultItems:null
	
	},
	
	initialize:function(){
		try {
		
		
			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	
			
			//adding parameters to query
			var linkQuery = String.format(sliderNews.parameters.query,date,sliderNews.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(sliderNews.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;

				sliderNews.parameters.resultItems = resultItems;
				var newTop = sliderNews.parameters.top - resultItems.length;
				if(newTop > 0)
				{
					linkQuery = String.format(sliderNews.parameters.query,date,newTop);
					sputilities.getItems(sliderNews.parameters.listName,linkQuery,function(data){
					
						var result = JSON.parse(data).d.results;
						var items =  sliderNews.parameters.resultItems;
						
						resultItems = items.concat(result);
						sliderNews.parameters.resultItems = resultItems;
						pageLayoutVP.componentsList.push(sliderNews);

					},function(xhr){ console.log(xhr)},sputilities.contextInfo().siteAbsoluteUrl);
	
				}
				else
				{				
					pageLayoutVP.componentsList.push(sliderNews);
				}			
				

			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var htmlIndicators="";
		var htmlSliders="";
		var data= 	sliderNews.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];
				if(currentItem.sys_txtImg != null)
				{
					//retrieving the fields of the results
					var title = currentItem.Title;
					var dateArticle = currentItem.ArticleStartDate;
					var description = currentItem.Entradilla;
					var content = currentItem.PublishingPageContent;
					var id = currentItem.ID;
					var startDate = currentItem.FechaInicio;
					var finalDate = currentItem.FechaFinal;
						
					//taking the url of the image
					var image = currentItem.sys_txtImg;
					var div = document.createElement('div');
					div.innerHTML = image;
					var urlimg = div.querySelector('img').src;
					
					//make html and detail popup
					//var urlDetail = String.format(sliderNews.parameters.detailUrl,sputilities.contextInfo().webAbsoluteUrl,sliderNews.parameters.listName,currentItem.ID);
					//var modal = String.format(sliderNews.parameters.modal, urlDetail);
					
					var urlPage= String.format(sliderNews.parameters.detailUrl,currentItem.FileRef,sliderNews.parameters.listName,currentItem.ID);
					//adding values to the parameters
					if(i==0)
					{
						htmlIndicators += String.format(sliderNews.parameters.bodyIndicators,i," class='active'");
						htmlSliders += String.format(sliderNews.parameters.bodyItemSlide," active",i,urlimg,title,description,urlPage);
					}
					else
					{
						htmlIndicators += String.format(sliderNews.parameters.bodyIndicators,i,"class=''");
						htmlSliders += String.format(sliderNews.parameters.bodyItemSlide,"",i,urlimg,title,description,urlPage);
		
					}
			}
					
		}
		//adding values of parameters to the html variable
			html += String.format(sliderNews.parameters.containerIndi,htmlIndicators);
			html += String.format(sliderNews.parameters.containerSlides,htmlSliders);
			
		//insert the html code
		document.getElementById(sliderNews.parameters.divID).innerHTML = html;
	}
}
/*Components: Galeria Equipo */
var teamPhotos = {
	//create parameters
	parameters:{
			listName: "Fotos Equipo",
			top: 5,
			divID:"team-images",
			contentType: "la imagen",
			detailParameter: "Imagen",
			containerSlide:"<div class='list-team'>{0}</div><div class='container-controls-team'><h5>Equipo</h5><div class='slider-tag-team'><span></span></div><img class='img-medal' src='/sites/Intranet/SiteAssets/Development/images/vps/team/ico-medal.png'/></div>",
			containerItem:"<div class='team-item'><div class='team'>{0}</div></div>",
			
			bodyItem:"<div class='team-content'><img src='{0}' class='img-responsive' /><div class='team-text'><h3>{1}</h3><h2>{2}</h2></div></div>",
			query: "?$select=Title,Description,FileRef,ContentType/Name,ID,Activa,Orden&$expand=ContentType&$filter=(ContentType eq '{0}' and Activa eq 1)&$orderby=Orden&$top={1}",
			resultItems:null
	
	},	
	initialize:function(){
		try {
		
			//adding parameters to query
			var linkQuery = String.format(teamPhotos.parameters.query,teamPhotos.parameters.contentType,teamPhotos.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(teamPhotos.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				teamPhotos.parameters.resultItems = resultItems;
				pageLayoutVP.componentsList.push(teamPhotos);	
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var data = teamPhotos.parameters.resultItems;
		if(data.length > 0)
		{
			//loop the array
			for (var i=0; i< data.length; i++){
					
				var currentItem = data[i];
				//retrieving the fields of the results
				var ID = currentItem.ID;
				var urlImg = currentItem.FileRef;
				var contentType = currentItem.ContentType.Name;
				var title = currentItem.Title;			
				var description = currentItem.Description;
				
				if(title != null && description != null)
				{
					//adding values to the parameters
					var bodyNew = String.format(teamPhotos.parameters.bodyItem,urlImg,title,description);	
				}
				else
				{
					var bodyNew = String.format(teamPhotos.parameters.bodyItem,urlImg,"","");	
	
				}			
				var containerNew = String.format(teamPhotos.parameters.containerItem,bodyNew);
	
				//adding values of parameters to the html variable
				html += containerNew;
													
			}			
			
			var htmlNew = String.format(teamPhotos.parameters.containerSlide, html);
			//insert the html code
			document.getElementById(teamPhotos.parameters.divID).innerHTML = htmlNew;
		}
		
	
	}
}

/*Components: Proyecto Destacado y Proyectos */
var projectView = {
	//create method parameters
	parameters:{
		listName:"Proyectos",
		containerId:"componentTest",
		containerIdImportant:"project-important",
		containerIdProjects:"projects-module",
		importantProjectHTMLform:"<div class='hproject-content'><a href='{0}'><img src='{1}' class='img-responsive'></a><div class='hproject-text'><h3>{2}</h3><h2>{3}</h2></div></div><h5>Proyecto destacado</h5>",
		simpleProjectHTMLform:"<div class='list-projects-item'><a href='{0}'><img src='{1}'><h3>{2}</h3><p>{3}</p></a></div>", 
		containerSliderProjects:"<div class='slider-tag'><span class='current-items'> </span><span class='total-items'> </span></div>",
		query:"?$select=Title,Destacado,Activo,Descripcion,UrlContenido,Icono&$filter=Activo eq 1&$orderby=Created_x0020_Date desc",
		resultItems:null
	},
	initialize:function(){
	  try{
			//execute query 
			sputilities.getItems(projectView.parameters.listName,projectView.parameters.query,function(data){
				var projects=JSON.parse(data).d.results;
				projectView.parameters.resultItems=projects;
				pageLayoutVP.componentsList.push(projectView);
			},
			function(data){
				console.log(JSON.parse(data).error.message.value)
			});
		}catch(e){					
				console.log(String.format('se ah producido un error en la ejecución del metodo project.initialize {0}',e))
		}		
	},
	insertHTML:function(){
		var data = projectView.parameters.resultItems;
		var important = false;
		var htmlProjects="";	
		var htmlImportant ="";			
			//review the results of query
			for(var i = 0; i < data.length; i++){					
				var project = data[i];
				var title = project.Title;
				var img = project.Icono.Url;
				var description = project.Descripcion;
				var url = project.UrlContenido.Url;
				//select first important project and create html code
				if(project.Destacado && !important){
					//insert important project						
					htmlImportant = String.format(projectView.parameters.importantProjectHTMLform,url,img,title,description);						
					important = true;
				}
				//select others projects and create html code
				if(!project.Destacado){
					//insert simple project
					htmlProjects += String.format(projectView.parameters.simpleProjectHTMLform,url,img,title,description);
					
				}
			}
			//add important project to main container
			document.getElementById(projectView.parameters.containerIdImportant).innerHTML = htmlImportant;
			//add slide project to main container
			document.getElementById(projectView.parameters.containerIdProjects).innerHTML = htmlProjects;
			
			if(htmlProjects != "")
			{
				//add slider tag
				document.getElementById(projectView.parameters.containerIdProjects).insertAdjacentHTML('afterend',projectView.parameters.containerSliderProjects);
			}

	}
}

/*Components: Slider Galeria*/

var sliderImg = {
	//create parameters
	parameters:{
			listName: "Galeria",
			top: 7,
			divID:"gallery-images",
			contentType: "la imagen",
			detailParameter: "Galeria",
			linkpopUp: '{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo={3}&ListaLikes=%22%22',
			link:"<a href='{0}' target='_blank'>",
			bodyImg:"<img src='{0}' /><h3>{1}</h3></a>",
			formatHtml:"<div class='gallery-image' onclick='javascript:sputilities.showModalDialog(\"{2}\",\"{3}\")'>"
						+   "<a class='zoom-img' href='javascript:void(0);'>"
						+   '<img src="/sites/Intranet/SiteAssets/Development/images/vps/gallery/btn-fullscreen.png"/></a>'
						+	'<div class="container-window"></div>'
    					+	'<div class="container-bg-gallery">'
    					+	'<img class="img-bg-gallery" src="{1}" onerror="displayGenericDocumentIcon(event.srcElement ? event.srcElement : event.target, 0); return false;" onload="(event.srcElement ? event.srcElement : event.target).style.visibility = \'visible\'; SP.ScriptHelpers.resizeImageToSquareLength(this, 280)">'        			
    					+	'</div>'
						+'</div>',
			containerSlide:"<div class='slider-tag-images'><span class='current-items-images'> </span><span class='total-items-images'></span></div>",
			query: "?$select=Title,FileRef,FileType,ContentType/Name,ID,Destacado&$expand=ContentType&$filter=(ContentType eq '{0}' and Destacado eq 1)&$orderby=Title&$top={1}",
			resultItems: null
	},
	insertHTML:function(){
	var html = "";
		var resultItems = sliderImg.parameters.resultItems;
		if(resultItems.length >0)
		{
			for (var i=0; i< resultItems.length; i++){
				var currentItem = resultItems[i];
				//retrieving the fields of the results
				var ID = currentItem.ID;
				var urlImg = currentItem.FileRef;
				var segments = urlImg.split('/');
				var segmentName = String.format("{0}.{1}",segments[segments.length -1].replace('.','_'),currentItem.FileType);
				segments[segments.length  -1] = "_w";
				segments.push(segmentName);
				urlImg = segments.join('/');

				
				var contentType = currentItem.ContentType.Name;
				var title = currentItem.Title;			
				//adding values to the parameters
				var linkpopUpNew = String.format(sliderImg.parameters.linkpopUp,sputilities.contextInfo().webAbsoluteUrl,sliderImg.parameters.listName,ID,sliderImg.parameters.detailParameter);
				html += String.format(sliderImg.parameters.formatHtml,sputilities.contextInfo().webAbsoluteUrl,urlImg,title,linkpopUpNew)				
			}	
			//insert the html code
			document.getElementById(sliderImg.parameters.divID).innerHTML = html;
			document.getElementById(sliderImg.parameters.divID).insertAdjacentHTML('afterend',sliderImg.parameters.containerSlide);
		}

		
	},
	initialize:function(){
		try {
			//adding parameters to query
			var linkQuery = String.format(sliderImg.parameters.query,sliderImg.parameters.contentType,sliderImg.parameters.top);
			//running the query with parameters
			sputilities.getItems(sliderImg.parameters.listName,linkQuery,function(data){	
				//save the results to an array
				sliderImg.parameters.resultItems = JSON.parse(data).d.results;
				 pageLayoutVP.componentsList.push(sliderImg);		
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	}
}

/* Components: Accesos Rapidos */
var quickAccess = {
	 parameters:{
		containerId:"quickaccess-items",
		listName:"Accesos Rápidos",
		query:"?$select=Title,Icono,Direccion,Padre,Orden,Home&$filter={0}&$orderby={1}",
		containerRow:"<div class='row'>{0}</div>",
		parentContainer:"<div class='list-quickaccess-item'>"+
						"<div name='children-accesos-{0}'><a href='javascript:void(0);' onclick='javascript:quickAccess.click(this);'>"+
						"<img src='{1}' class='img-apps'></img></a><h3>{2}</h3></div>"+
						"<div style='display:none;'><div class='col-xs-12 apps-container-children' id='children-accesos-{0}'></div></div></div>",
		parentOnly:"<div class='list-quickaccess-item'>"+
					"<a href='{0}' target='_blank'><img src='{1}' class='img-apps'></img>"+
					"</a><h3>{2}</h3></div>",
		childrenHtml:"<style>.ms-dlgContent,.ms-dlgBorder,.ms-dlgFrameContainer > div{{height:auto!important;}}</style><div class='col-xs-6 apps-children'><a href='{0}' target='_blank'><div class='txt-accesos-apps'><img src='{1}' onclick='{0}' class='img-apps'></img><span>{2}</span></div></a></div>",		
		moreApps:"<a href='"+ sputilities.contextInfo().webAbsoluteUrl + "/Paginas/AccesosRapidos.aspx'>Todas las aplicaciones <i class='inc-arrow-3 centrado-y'></i></a>",
		idMoreApp:"more-access",
		patternTitle:[],
		resultItems:null,
		resultChildrens:null
	 },
	 initialize:function(){
		 
		 // adjusted the query to find parents and access
		 var queryFilter = String.format(quickAccess.parameters.query,"(Padre eq null and Home eq 1)","Orden");
		 try{
			 sputilities.getItems(quickAccess.parameters.listName,queryFilter,function(data){
				 var results = JSON.parse(data).d.results;
				 quickAccess.parameters.resultItems = results;
				 var queryChildrens = String.format(quickAccess.parameters.query,"(Padre ne null and Home eq 1)","Padre");
				 
					sputilities.getItems(quickAccess.parameters.listName,queryChildrens,function(dataContent){
						quickAccess.parameters.resultChildrens = JSON.parse(dataContent).d.results;
						 pageLayoutVP.componentsList.push(quickAccess);
					},function(data){
				 		console.log(JSON.parse(data).error.message.value)
			 		});
				 
			 },function(data){
				 console.log(JSON.parse(data).error.message.value)
			 });
		  }catch(e){
				 console.log(String.format("Ocurrió un error en la ejecución del método quickAccess.initialize() {0}",e))
          }
	 },
	 insertHTML:function(){
	    var data =  quickAccess.parameters.resultItems;
	    var count = 0;
	    var html = "";
	 	//run the results array to insert the accesses and the parent elements
		for(var i = 0;i < data.length;i++){
			 if(data[i].Direccion == null){
				quickAccess.parameters.patternTitle.push({'title': data[i].Title, 'orden':data[i].Orden});						
				html = String.format(quickAccess.parameters.parentContainer,data[i].Orden,data[i].Icono.Url,data[i].Title);												 
			 }else{
				html = String.format(quickAccess.parameters.parentOnly,data[i].Direccion.Url,data[i].Icono.Url,data[i].Title);						
			 }
			 count +=1;			 
			 var body = html;
			 document.getElementById(quickAccess.parameters.containerId).insertAdjacentHTML('beforeend',body);
			 	
		//document.getElementById('patternList').insertAdjacentHTML('beforeend',newAccess);
		}
		var dataChildren = quickAccess.parameters.resultChildrens;
		var parents = quickAccess.parameters.patternTitle;
		var currentParent="";
		var htmlChildren="";
		for(var i=0; i <dataChildren.length;i++)
		{
			if(dataChildren[i].Direccion != null){
			
				 if(i==0)
				 {
				 	currentParent = dataChildren[i].Padre;
				 	htmlChildren += String.format(quickAccess.parameters.childrenHtml,dataChildren[i].Direccion.Url,dataChildren[i].Icono.Url,dataChildren[i].Title);		
				 	
				 	if(i == dataChildren.length - 1)
				 	{
				 		for(var j=0;j<parents.length;j++)
				 		{
					 		if(parents[j].title == currentParent)
					 		{
					 			var containerid=String.format("children-accesos-{0}", parents[j].orden);
					 			document.getElementById(containerid).insertAdjacentHTML('beforeend',htmlChildren);
					 			htmlChildren="";
					 		}
				 		}
				 	}

				 }
				 else
				 {
				 	if(currentParent != dataChildren[i].Padre)
				 	{
				 		for(var j=0;j<parents.length;j++)
				 		{
				 			if(parents[j].title == currentParent)
				 			{
				 				var containerid=String.format("children-accesos-{0}", parents[j].orden);
				 				document.getElementById(containerid).insertAdjacentHTML('beforeend',htmlChildren);
				 				htmlChildren="";
				 			}
				 		}				 		
				 		currentParent = dataChildren[i].Padre;
				 		htmlChildren += String.format(quickAccess.parameters.childrenHtml,dataChildren[i].Direccion.Url,dataChildren[i].Icono.Url,dataChildren[i].Title);
				 	}
				 	else
				 	{			 		
				 		currentParent = dataChildren[i].Padre;
				 		htmlChildren += String.format(quickAccess.parameters.childrenHtml,dataChildren[i].Direccion.Url,dataChildren[i].Icono.Url,dataChildren[i].Title);	
				 		
				 	}
				 	
				 	if(i == dataChildren.length - 1)
				 	{
				 		for(var j=0;j<parents.length;j++)
				 		{
					 		if(parents[j].title == currentParent)
					 		{
					 			var containerid=String.format("children-accesos-{0}", parents[j].orden);
					 			document.getElementById(containerid).insertAdjacentHTML('beforeend',htmlChildren);
					 			htmlChildren="";
					 		}
				 		}
				 	}
				 	
				 }
					
				
			}
		}
		document.getElementById(quickAccess.parameters.idMoreApp).innerHTML = quickAccess.parameters.moreApps;
		//quickAccess.getChildrenList();
	 },
	 click:function(ctrl){
	  var id = ctrl.parentElement.attributes.name.value;
	  var node = document.getElementById(id);
	  //console.log(node.innerHtml);
		sputilities.showModalDialog(ctrl.parentElement.querySelector('h3').innerHTML,null,null,node.parentElement.innerHTML);

	 }	 	 
 }

sputilities.callFunctionOnLoadBody('pageLayoutVP.load');
var pageLayoutVP= {
	components:{
		staticNews : staticNews, 
		sliderNews : sliderNews,
		teamPhotos : teamPhotos,
		graphicsImg :graphicsImg,
		sliderImg : sliderImg, 
		projectView : projectView,
		events : events,
		quickAccess : quickAccess

	},
	load:function(){
		
		pageLayoutVP.initComponents();
		var loadControl = setInterval(function(){
			document.getElementById('ms-designer-ribbon').style.opacity = 0;
			document.getElementById('s4-workspace').style.opacity = 0;
			if( pageLayoutVP.componentsList.length  == pageLayoutVP.max){
				for(var i = 0; i <  pageLayoutVP.componentsList.length ; i++){
					 pageLayoutVP.componentsList[i].insertHTML();
				}
				clearInterval(loadControl);	
				pageLayoutVP.animations();
				
				document.getElementById('s4-workspace').style.opacity = 1;
				document.getElementById('ms-designer-ribbon').style.opacity = 1;
				setTimeout(function(){	
				

				},5000);							
			}
		},500);	

	},
	initComponents:function(){
		pageLayoutVP.components.staticNews.initialize();
		pageLayoutVP.components.events.initialize();
		pageLayoutVP.components.sliderNews.initialize();
		pageLayoutVP.components.teamPhotos.initialize();
		pageLayoutVP.components.graphicsImg.initialize();
		pageLayoutVP.components.sliderImg.initialize();
		pageLayoutVP.components.projectView.initialize();
		pageLayoutVP.components.quickAccess.initialize();
		
	},
	animations:function(){
	    // Slider Noticias
		$.noConflict();
		$(".carousel").swipe({
		
		 	swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
		
		    if (direction == 'left') $(this).carousel('next');
		    if (direction == 'right') $(this).carousel('prev');
		
		  },
		  allowPageScroll:"vertical"
		
		  });
		$('#carousel-header').on('slid.bs.carousel', function () {
		    $holder = $( "ol li.active" );
		    $holder.removeClass('active');
		    var idx = $('div.active').index('div.item');
		    $('ol.carousel-indicators li[data-slide-to="'+ idx+'"]').addClass('active');
		});
		
		$('ol.carousel-indicators  li').on("click",function(){ 
		    $('ol.carousel-indicators li.active').removeClass("active");
		    $(this).addClass("active");
		});
		$('.list-team').on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
		    var i = (currentSlide ? currentSlide : 0) + 1 ;
		    $('.slider-tag-team span').text(i + ' / ' + slick.slideCount);
		});
	
		//Slider Equipo
		$('.list-team').slick({
		  dots: false,
		  infinite: true,
		  speed: 700,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
		});
		
		// Slider Galeria
		$('#gallery-images').on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
		var i = (currentSlide ? currentSlide : 0) + 1 ;
		$('.slider-tag-images .current-items-images').text(i);
		$('.slider-tag-images .total-items-images').text(slick.slideCount);
		});
		
		$('#gallery-images').slick({
		  dots: false,
		  infinite: true,
		  speed: 700,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
		});
		
		// Slider Cifras
		$('.information-numbers').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
		});
		
		//Slider Proyectos
		$('.list-projects').on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
			    var i = (currentSlide ? currentSlide : 0) + 1 ;
			    $('.slider-tag .current-items').text(i);
				$('.slider-tag .total-items').text(slick.slideCount);
		});
		
		
		$('.list-projects').slick({
		  dots: false,
		  infinite: true,
		  speed: 700,
		  slidesToShow: 4,
		  slidesToScroll: 1,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
		});
		
		$('.list-quickaccess').on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
	    var i = (currentSlide ? currentSlide : 0) + 1 ;
	    $('.slider-tag-access .current-items-access').text(i);
		$('.slider-tag-access .total-items-access').text(slick.slideCount);
		});


		$('.list-quickaccess').slick({
		  dots: false,
		  infinite: true,
		  speed: 700,
		  slidesToShow: 4,
		  slidesToScroll: 1,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
		});

	},	
	componentsList:[],
	max:8


}