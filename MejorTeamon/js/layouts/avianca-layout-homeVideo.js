﻿/*Component: Noticias Estaticas */

var staticNews = {
	//create parameters
	parameters:{
			listName: "Noticias Estáticas",
			top: 3,
			divID:"static-news",
			containerNews:"<div class='otherNews-item'>{0}{1}</div>",			
			imageNews:"<div class='otherNews-item-image-container'><a href='{2}' target='_blank'><img src='{0}' alt='Avatar' class='otherNews-item-image-image'/>" +
					  "<div class='otherNews-item-image-overlay'><div class='otherNews-item-image-text'>" +
					  "<img src='{1}/SiteAssets/Development/images/components/news/avion-hover.png'/></div></div></a></div>",
			textNews:"<div class='otherNews-item-text-container'><h3>{0}</h3><p>{1}</p><a href='{2}' target='_blank'>Ver más<i class='inc-arrow-3 centrado-y'></i	></a></div>",
			query: "?$select=Title,FechaFinal,FechaInicio,Url,sys_txtImg,Descripcion,Orden&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}')&$orderby=Orden&$top={1}",
			resultItems:null		
	
	},
	initialize:function(){
		try {

			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	
			
			//add current date to query and filter
			var linkQuery = String.format(staticNews.parameters.query,date,staticNews.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(staticNews.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				staticNews.parameters.resultItems = resultItems;
				 pageLayout.componentsList.push(staticNews);
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var data = staticNews.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];
			
			//retrieving the fields of the results
			var title = currentItem.Title;
			var description = currentItem.Descripcion;			
			var url = currentItem.Url.Url;
				
			//taking the url of the image
			var image = currentItem.sys_txtImg;
			var div = document.createElement('div');
			div.innerHTML = image;
			var urlimg = div.querySelector('img').src;			
					
			//make html with the fields 
			var imageHtml = String.format(staticNews.parameters.imageNews, urlimg,sputilities.contextInfo().webAbsoluteUrl, url);
			var textHtml = String.format(staticNews.parameters.textNews,title,description,url);			
					
			//adding html 
			html += String.format(staticNews.parameters.containerNews,imageHtml,textHtml);			
					
		}
			
		//insert the html code
		document.getElementById(staticNews.parameters.divID).innerHTML = html;
	}
}

/* Component: Imagen del día */

var imageDay = {
	//create parameters
	parameters:{
			listName: "Contenido Estático",
			top: 1,
			divIDimg:"image-day",
			seccion:"ImagenDelDia",
			detailUrl:"{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo=ImagenDia",
			modal:'sputilities.showModalDialog("Imagen del Día","{0}",window.location.href)',
			bodyImg:"<a href='javascript:void(0);' onclick='{1}'><img src='{0}' class='img-responsive'></a>",
			query: "?$select=*,FechaInicio,FechaFinal&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}' and {1} eq 1)&$orderby=FechaFinal&$top={2}",
			resultItems:null
	
	},	
	initialize:function(){
		try {
		
			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	
			
			//add current date to query and filter
			var linkQuery = String.format(imageDay.parameters.query,date,imageDay.parameters.seccion,imageDay.parameters.top);

			//running the query with parameters
			sputilities.getItems(imageDay.parameters.listName,linkQuery,function(data){				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				imageDay.parameters.resultItems = resultItems;
				 pageLayout.componentsList.push(imageDay);
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML: function(){		
		var html = "";
		var data = imageDay.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];

			//taking the url of the image
			var image = currentItem.sys_txtImg;
			var div = document.createElement('div');
			div.innerHTML = image;
			var urlimg = div.querySelector('img').src;
				
			//retrieving the fields of the results
			var descripcion = currentItem.Descripcion;
				
			//make html and detail popup
			var urlDetail = String.format(imageDay.parameters.detailUrl,sputilities.contextInfo().webAbsoluteUrl,imageDay.parameters.listName,currentItem.ID);
			var modal = String.format(imageDay.parameters.modal, urlDetail);
						
			//adding values of parameters to the html variable
			html += String.format(imageDay.parameters.bodyImg,urlimg,modal,sputilities.contextInfo().webAbsoluteUrl);					
		}
			
		//insert the html code
		document.getElementById(imageDay.parameters.divIDimg).innerHTML = html;
	}
}

/* Components: marquee */
var marquee = {
//create parameters
	parameters:{
		containerId:"notifications",
		listName: "Marquesina",
		top:3,
		query: "?$select=Title,Url_vinculo&$orderby=Created_x0020_Date&$top={0}",
		containerMarquee:"<div class='swiper-wrapper variable-width2'>{0}</div>",
		contentMarquee:"<div class='swiper-slide text-center text-uppercase'><a href='{0}' target='_blank'><span class='txt-notificacion'><i class='inc-info centrado-y'></i>{1}</span></a></div>",
		resultItems:null
		
	},
	initialize:function(){
		//insert row limit to query 
		marquee.parameters.query = String.format(marquee.parameters.query,marquee.parameters.top)
		try{
			sputilities.getItems(marquee.parameters.listName,marquee.parameters.query,function(data){
				var results = JSON.parse(data).d.results;
				marquee.parameters.resultItems = results;
				 pageLayout.componentsList.push(marquee);
			
			},function(data){
				console.log(JSON.parse(data).error.message.value)
			});
		}catch(e){
			console.log(String.format("Ocurrió un error inesperado en la ejecución de método marquee.initialize {0}",e))
		}
	},
	insertHTML:function(){
		var html = "";
		
		var data = marquee.parameters.resultItems;
						
			for (var i = 0;i < data.length;i++){
				var currentItem = data[i];
				
				// get field by items
				var title = currentItem.Title;
				var url = currentItem.Url_vinculo.Url;
				
				var bodyHtml = String.format(marquee.parameters.contentMarquee,url,title);
				
				html += bodyHtml;				
			}
 
		var container = String.format(marquee.parameters.containerMarquee, html);
		document.getElementById(marquee.parameters.containerId).innerHTML = container;
	}
}

/* Component: Marquesina Cifras */

var economIndic = {
	//create parameters
	parameters:{
			listName: "Indicadores Económicos",
			top: 5,
			divID:"indicators",
			change:{
					up: "<span class='txt-indicador text-center indicador-subio'><i class='inc-arrow-5 centrado-y'></i>{0}</span>",
					down: "<span class='txt-indicador text-center indicador-bajo'><i class='inc-arrow-6 centrado-y'></i>{0}</span>",
					stable: "<span class='txt-indicador text-center indicador-estable'><i class='inc-arrow-7 centrado-y'></i>{0}</span>",
					},			
			containerIndic:" <div class='swiper-slide centrar-slide'>{0}</div>",
			body:"<h1>{0}</h1><h2>{1}&nbsp;{2}</h2>",
			query: "?$select=Title,AntiguoValor,NuevoValor,Moneda,Orden&$orderby=Orden&$top={0}",
			resultItems:null
	
	},	
	initialize:function(){
		try {
					
			//add current date to query and filter
			var linkQuery = String.format(economIndic.parameters.query,economIndic.parameters.top);

			//running the query with parameters
			sputilities.getItems(economIndic.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				economIndic.parameters.resultItems = resultItems;
				 pageLayout.componentsList.push(economIndic);

			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var data = economIndic.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];

			//retrieving the fields of the results
			var title = currentItem.Title;
			var oldValue = currentItem.AntiguoValor;
			var newValue = currentItem.NuevoValor;
			var coin = currentItem.Moneda;
			var change="";
			
			//make html 
			var bodyHtml = String.format(economIndic.parameters.body,title,coin,newValue);
						
			if(oldValue > newValue){
				change = String.format(economIndic.parameters.change.down,bodyHtml);
			}
			else if (oldValue < newValue){
				change = String.format(economIndic.parameters.change.up,bodyHtml);
			}
			else {
				change = String.format(economIndic.parameters.change.stable,bodyHtml);
			}
						
			//adding values to the parameters
			var bodyNew = String.format(economIndic.parameters.containerIndic,change);	
				
			//adding values of parameters to the html variable
			html += bodyNew;
					
		}
			
		//insert the html code

		document.getElementById(economIndic.parameters.divID).innerHTML = html;
	}
}

/* Component: Eventos */

var events = {
//create parameters
	parameters:{
		listName: "Eventos",
		containerId:"items-events",	
		moreEventsId:"more-events",
		top: 6,	
		mounth:["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC"],
		query: "?$select=ID,Title,Description,EventDate,EndDate,fAllDayEvent&$filter=EndDate ge datetime'{0}'&$orderby=EventDate&$top={1}",
		containerEven:"<div class='item-events'>{0}{1}</div>",
		textEvent:"<div class='item-events-text'><h2>{0}</h2><p>{1}</p></div>"+
				  "<div class='item-events-date'><h3>{2}</h3><span>{3}</span></div>"+
				  "<div class='item-events-hour'><h3>{4}</h3><span>{5}</span></div>",
		linkEvent:"<div class='item-events-text-arrow' onclick='{0}'><a href='javascript:void(0);'><i class='inc-arrow-3 centrado-y'></i></a></div>",
		modal:'javascript:sputilities.showModalDialog("Evento","{0}",window.location.href);',
		listUrl:"{0}/Lists/{1}",
		dispForm:"{0}/DispForm.aspx?ID={1}",	
		detailUrl:"{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo=Eventos",
		linkAll:"<a href='{0}'>Ver todos <i class='inc-arrow-3 centrado-y'></i></a>",		
		resultItems:null	
	},
	initialize:function(){
		//Get current date value
		var today = new Date;
		today = today.toISOString();
		//We build the query			
		events.parameters.query = String.format(events.parameters.query,today,events.parameters.top)
		try{
			sputilities.getItems(events.parameters.listName,events.parameters.query,function(data){
			//We get the url from the list of events
				
				var results = JSON.parse(data).d.results;
				events.parameters.resultItems = results;
				 pageLayout.componentsList.push(events);
					
						
					
			},function(data){
				console.log(JSON.parse(data).error.message.value)
			});		
		}catch(e){			
			console.log(String.format('Ocurrio un error inesperado en el metodo events.initialize {0}',e))
		}				
				
	},
	insertHTML: function(){
		var data = events.parameters.resultItems;
		var html = "";
		var listUrl = String.format(events.parameters.listUrl,sputilities.contextInfo().webAbsoluteUrl,events.parameters.listName)	;	
		//We construct the section of the event for each event in the result set
		for (var i = 0;i<data.length;i++){
			
			var form = String.format(events.parameters.dispForm,listUrl,data[i].ID);
			
			
			var allday = data[i].fAllDayEvent;
			console.log(allday)
			if(allday == true){
			var DateSite =data[i].EventDate.replace("Z","")	
			var startDateTime = new Date(DateSite);	
			var startDay =('0' +  startDateTime.getUTCDate()).slice(-2);
			var startMonth = events.parameters.mounth[startDateTime.getMonth()];
			eventTime = "el día";
			eventAMPM= "Todo";
			}
			else{
			
			var startDateTime = new Date(data[i].EventDate);
			var endDateTime = new Date(data[i].EndDate);							
			var startDay =('0' +  startDateTime.getUTCDate()).slice(-2);
			var startMonth = events.parameters.mounth[startDateTime.getMonth()];
			
			
			var startHour="";
			var startMinutes= "";
				if(startDateTime.getHours() > 12)
				{
					var hour = startDateTime.getHours() - 12;
					startHour = ('0' + hour).slice(-2);
					startMinutes = ('0' + startDateTime.getMinutes()).slice(-2);
					eventAMPM="PM";
					eventTime = String.format("{0}:{1}",startHour,startMinutes);	
				}
				else
				{
					startHour = ('0' + startDateTime.getHours()).slice(-2);
					startMinutes = ('0' + startDateTime.getMinutes()).slice(-2);
					eventAMPM="AM";
					eventTime = String.format("{0}:{1}",startHour,startMinutes);	
				}}
				
		
			var detailURL = String.format(events.parameters.detailUrl,sputilities.contextInfo().webAbsoluteUrl,events.parameters.listName,data[i].ID);
			var htmlModal = String.format(events.parameters.modal,detailURL);
			var htmlLink = String.format(events.parameters.linkEvent,htmlModal);
			var htmlBody = String.format(events.parameters.textEvent,data[i].Title,data[i].Description,startMonth,startDay,eventAMPM,eventTime);
			
			html += String.format(events.parameters.containerEven,htmlBody,htmlLink);
					
		}
			
		document.getElementById(events.parameters.containerId).innerHTML = html;
		var htmlAll = String.format(events.parameters.linkAll,listUrl);
		document.getElementById(events.parameters.moreEventsId).innerHTML=htmlAll ;
							
	}

}

/* Components: Beneficios */

var benefits = {
	//create parameters
	parameters:{
			listName: "Contenido Estático",
			top: 1,
			divIDimg:"benefit-day",
			seccion:"Beneficio",			
			bodyImg:"<img src='{0}' class='img-responsive'><a href='{1}' target='_blank' class='btn-campaing'>Ver beneficio</a>",
			query: "?$select=*,FechaInicio,FechaFinal&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}' and {1} eq 1)&$orderby=FechaFinal&$top={2}",
			resultItems:null
	
	},	
	initialize:function(){
		try {
		
			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	
			
			//add current date to query and filter
			var linkQuery = String.format(benefits.parameters.query,date,benefits.parameters.seccion,benefits.parameters.top);

			//running the query with parameters
			sputilities.getItems(benefits.parameters.listName,linkQuery,function(data){				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				benefits.parameters.resultItems = resultItems;
				 pageLayout.componentsList.push(benefits);
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML: function(){		
		var html = "";
		var data = benefits.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];

			//taking the url of the image
			var image = currentItem.sys_txtImg;
			var div = document.createElement('div');
			div.innerHTML = image;
			var urlimg = div.querySelector('img').src;
				
			//retrieving the fields of the results
			var url = currentItem.Url.Url;
												
			//adding values of parameters to the html variable
			html += String.format(benefits.parameters.bodyImg,urlimg,url);					
		}
			
		//insert the html code
		document.getElementById(benefits.parameters.divIDimg).innerHTML = html;
	}
}

/* Component: Destacados */
var important = {
	//create parameters
	parameters:{
			listName: "Contenido Estático",
			top: 1,
			divIDimg:"information-important",
			seccion:"Destacado",			
			bodyImg:"<div class='important'><a href='{0}' target='_blank'><img src='{1}' class='img-responsive'></a><h5>Destacados</h5>",
			query: "?$select=*,FechaInicio,FechaFinal&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}' and {1} eq 1)&$orderby=FechaFinal&$top={2}",
			resultItems:null
	
	},	
	initialize:function(){
		try {
		
			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	
			
			//add current date to query and filter
			var linkQuery = String.format(important.parameters.query,date,important.parameters.seccion,important.parameters.top);

			//running the query with parameters
			sputilities.getItems(important.parameters.listName,linkQuery,function(data){				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				important.parameters.resultItems = resultItems;
				 pageLayout.componentsList.push(important);
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML: function(){		
		var html = "";
		var data = important.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];

			//taking the url of the image
			var image = currentItem.sys_txtImg;
			var div = document.createElement('div');
			div.innerHTML = image;
			var urlimg = div.querySelector('img').src;
				
			//retrieving the fields of the results
			var url = currentItem.Url.Url;
												
			//adding values of parameters to the html variable
			html += String.format(important.parameters.bodyImg,url,urlimg);					
		}
			
		//insert the html code
		document.getElementById(important.parameters.divIDimg).innerHTML = html;
	}
}

/* Components: Slider Cifras */

var graphicsImg = {
	//create parameters
	parameters:{
			listName:"Cifras",
			top: 6,
			divID:"information-numbers",
			bodyTab:"<div class='important'><img src='{0}' class='img-responsive' /><h5>{1}</h5></div>",
			query: "?$select=sys_txtImg,Orden,Title&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}')&$orderby=Orden&$top={1}",
			resultItems:null
	
	},
	
	initialize:function(){
		try {
		
			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	

			//adding parameters to query
			var linkQuery = String.format(graphicsImg.parameters.query,date,graphicsImg.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(graphicsImg.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				
				graphicsImg.parameters.resultItems = resultItems;
				 pageLayout.componentsList.push(graphicsImg);
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var data = graphicsImg.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];
			//retrieving the fields of the results
			var title =	currentItem.Title;
			//taking the url of the image
			var image = currentItem.sys_txtImg;
			var div = document.createElement('div');
			div.innerHTML = image;
			var urlimg = div.querySelector('img').src;
							
			//adding values to the parameters
			var bodyNew = String.format(graphicsImg.parameters.bodyTab,urlimg, title);	
					
			//adding values of parameters to the html variable
			html += bodyNew;
					
		}
			
		//insert the html code
		document.getElementById(graphicsImg.parameters.divID).innerHTML = html;
	}
}

var offerDay = {
	//create parameters
	parameters:{
			listName:"Ofertas del día",
			top: 6,
			divID:"information-offers",
			linkImg:'openBasicDialog("{0}","{1}")',
			linkOffer: "<a href='javascript:void(0);' onclick='{0}'>",
			bodyOffer:"<div class='important'><a href='{0}' target='_blank'><img src='{1}' class='img-responsive' /></a><h5>{2}</h5></div>",
			query: "?$select=Title,sys_txtImg,Orden,FechaInicio,FechaFinal,CategoriaOferta,Url&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}')&$orderby=Orden&$top={1}",
			resultItems:null
	
	},
	initialize:function(){
		try {
		
			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	

			//adding parameters to query
			var linkQuery = String.format(offerDay.parameters.query,date,offerDay.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(offerDay.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				offerDay.parameters.resultItems = resultItems;
				 pageLayout.componentsList.push(offerDay);

				
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var data = offerDay.parameters.resultItems;
		//loop the array
				for (var i=0; i< data.length; i++){
				
				var currentItem = data[i];
				
				//retrieving the fields of the results
				var titleOffer = currentItem.Title;
				var categoryOffer = currentItem.CategoriaOferta;
				var url = currentItem.Url.Url;
				//taking the url of the image
				var image = currentItem.sys_txtImg;
				var div = document.createElement('div');
				div.innerHTML = image;
				var urlimg = div.querySelector('img').src;
				
				//adding values to the parameters
				var bodyHtml = String.format(offerDay.parameters.bodyOffer,url,urlimg,titleOffer);
										
				//adding values of parameters to the html variable
				html += bodyHtml;					
			}
			
			//insert the html code
			document.getElementById(offerDay.parameters.divID).innerHTML = html;
	}
}

/* Component: Slider Noticias */
var sliderNews = {
	//create parameters
	parameters:{
			listName: "Páginas",
			top: 7,
			divID:"carousel-header",
			detailParameter: "Noticias",
			containerIndi:" <ol class='carousel-indicators'>{0}</ol>",
			bodyIndicators:" <li data-target='#carousel-header' data-slide-to='{0}'{1}></li>",
			containerSlides:" <div class='carousel-inner slider' role='listbox'>{0}</div>",
			bodyItemSlide:"<div class='item{0}' id='slide-0{1}'> <img src='{2}' alt='...'><div class='carousel-caption'><h2>{3}</h2><p>{4}</p><a href='{5}' target='_self' class='btn light-red-button'>Más información</a></div></div>",
			detailUrl:"{0}?ListaBase={1}&IDItem={2}&Tipo=Noticias",
			modal:'sputilities.showModalDialog("Noticia","{0}",window.location.href)',
			query: "?$select=FileRef,Title,ArticleStartDate,Entradilla,PublishingPageContent,ID,FechaInicio,FechaFinal,Orden,sys_txtImg&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}' and ContentType eq 'Noticias' and sys_txtImg ne null)&$orderby=Created desc&$top={1}",
			resultItems:null
	
	},
	
	initialize:function(){
		try {
		
		
			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	
			
			//adding parameters to query
			var linkQuery = String.format(sliderNews.parameters.query,date,sliderNews.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(sliderNews.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;

				sliderNews.parameters.resultItems = resultItems;
				 pageLayout.componentsList.push(sliderNews);			
				

			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var htmlIndicators="";
		var htmlSliders="";
		var data= 	sliderNews.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];
				if(currentItem.sys_txtImg != null)
				{
					//retrieving the fields of the results
					var title = currentItem.Title;
					var dateArticle = currentItem.ArticleStartDate;
					var description = currentItem.Entradilla;
					var content = currentItem.PublishingPageContent;
					var id = currentItem.ID;
					var startDate = currentItem.FechaInicio;
					var finalDate = currentItem.FechaFinal;
						
					//taking the url of the image
					var image = currentItem.sys_txtImg;
					var div = document.createElement('div');
					div.innerHTML = image;
					var urlimg = div.querySelector('img').src;
					
					//make html and detail popup
					//var urlDetail = String.format(sliderNews.parameters.detailUrl,sputilities.contextInfo().webAbsoluteUrl,sliderNews.parameters.listName,currentItem.ID);
					//var modal = String.format(sliderNews.parameters.modal, urlDetail);
					
					var urlPage= String.format(sliderNews.parameters.detailUrl,currentItem.FileRef,sliderNews.parameters.listName,currentItem.ID);
					//adding values to the parameters
					if(i==0)
					{
						htmlIndicators += String.format(sliderNews.parameters.bodyIndicators,i," class='active'");
						htmlSliders += String.format(sliderNews.parameters.bodyItemSlide," active",i,urlimg,title,description,urlPage);
					}
					else
					{
						htmlIndicators += String.format(sliderNews.parameters.bodyIndicators,i,"class=''");
						htmlSliders += String.format(sliderNews.parameters.bodyItemSlide,"",i,urlimg,title,description,urlPage);
		
					}
			}
					
		}
		//adding values of parameters to the html variable
			html += String.format(sliderNews.parameters.containerIndi,htmlIndicators);
			html += String.format(sliderNews.parameters.containerSlides,htmlSliders);
			
		//insert the html code
		document.getElementById(sliderNews.parameters.divID).innerHTML = html;
	}
}

/* Components: Videos Home */
var videoPoster = {
parameters:{
		containerId:"video-day",
		top:3,
		listName:"Videos Intranet",
		imgDefault:"{0}/Style%20Library/Media%20Player/VideoPreview_Default.png",
		caml:"<View Scope='Recursive'><RowLimit>10</RowLimit><ViewFields><FieldRef Name='Title' /><FieldRef Name='FileDirRef' /><FieldRef Name='Activo' /><FieldRef Name='ID' /><FieldRef Name='ContentType' />"+
			"<FieldRef Name='FileRef' /><FieldRef Name='Created' /></ViewFields><Query><Where><Contains><FieldRef Name='FileDirRef' />"+
			"<Value Type='Text'>{0}</Value></Contains></Where></Query></View>",
		query:"?$filter=(Activo eq 1)&$select=*,Activo,ID,FileDirRef,ContentType,FileRef,Created&$expand=ContentType&$orderby=Created desc&$top={0}",
		queryContent:"?$select=*,Activo,ID,FileDirRef,ContentType/Name,FileRef,Created",		
		bodyPoster:"<a href='javascript:void(0);' onclick='{0}' class='circulo bg-play centrado-total'><i class='inc-play centrado-total'></i></a>",
		bodyImg:"<img src='{0}' class='img-responsive'>",
		itemSlick:"<div class='item'>{0}{1}</div>",
		detailUrl:"{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo=Videos",
		modal:'sputilities.showModalDialog("Video","{0}",window.location.href)',
		resultItems:null,
		Items:"",
		itemsHtml:""


	},
	initialize:function(){

		
		var query = String.format(videoPoster.parameters.query,videoPoster.parameters.top);
		try{
			sputilities.getItems(videoPoster.parameters.listName,query,function(data){
				var posters = JSON.parse(data).d.results;
				videoPoster.parameters.Items = posters.length;
				for(var i=0; i<posters.length;i++){
				var filter = posters[i].FileRef.substring(1);
				var queryNew = String.format(videoPoster.parameters.caml,filter);
				
				sputilities.getItemsByCaml({caml:queryNew,listName:videoPoster.parameters.listName,
					success:function(dataContent){				
	
						if(videoPoster.parameters.itemsHtml == ""){
						videoPoster.parameters.resultItems= JSON.parse(dataContent).d.results;
						videoPoster.parameters.itemsHtml =  videoPoster.parameters.resultItems
						}else{
						videoPoster.parameters.resultItems= JSON.parse(dataContent).d.results;
						if(videoPoster.parameters.resultItems.length > 0 ){
						for(var i=0;i<videoPoster.parameters.resultItems.length;i++){
						videoPoster.parameters.itemsHtml.push(videoPoster.parameters.resultItems[i]);
						console.log(videoPoster.parameters.itemsHtml);
						}
						}
						if(videoPoster.parameters.itemsHtml.length == videoPoster.parameters.Items*2){
						 pageLayout.componentsList.push(videoPoster);						
						}
						}						
					},error:function(data){
					console.log(JSON.parse(data).error.message.value)
					},
					query: videoPoster.parameters.queryContent
				});					
				}	
				
			},function(data){
				console.log(JSON.parse(data).error.message.value)
			});	
		}
		catch(e){
					console.log(String.format("Ocurrió un error inesperado en la ejecución de método videoPoster.initialize {0}",e))
				}	
	},
	insertHTML:function(){
		var htmlImg="";
		var htmlBody="";
		var html="";
		var contador = 1
		var data=videoPoster.parameters.itemsHtml;
		for(var i = 0;i < data.length;i++){
				if(data[i].FileDirRef.indexOf("/Preview Images") > 0){
				    var img = String.format("{0}{1}",sputilities.contextInfo().webServerRelativeUrl,data[i].FileRef);					
					htmlImg = String.format(videoPoster.parameters.bodyImg,data[i].FileRef);
					var item = String.format(videoPoster.parameters.itemSlick,htmlImg,htmlBody);
						html +=item;
				}
				else
				{
					//make html and detail popup
					var urlDetail = String.format(videoPoster.parameters.detailUrl,sputilities.contextInfo().webAbsoluteUrl,videoPoster.parameters.listName,data[i].ID);
					var modal = String.format(videoPoster.parameters.modal, urlDetail);
					htmlBody = String.format(videoPoster.parameters.bodyPoster,modal);

					if(data.length == contador)
					{
					
					}	
				}
						
		}
		document.getElementById(videoPoster.parameters.containerId).innerHTML = html;
		console.log("hola")
		//videoPoster.hola();
	}
	}
/* Commponents: Tabs Campañas */

var campaignTabs = {
	//create parameters
	parameters:{
			listName: "Tabs Campañas",
			top: 5,
			divID:"links-campaing",
			link:"<a href='{0}' target='_blank'>",
			containerLinks:"<ul>{0}</ul>",
			bodyLinks:"<li><a href='{0}' target='_blank'><img src='{1}'><h1>{2}</h1><h2>{3}</h2></a></li>",
			bodyTab:"<img src='{0}' /><h3>{1}</h3></a>",
			query: "?$select=Title,Url,sys_txtImg,Orden,Descripcion&$orderby=Orden&$top={0}",
			resultItems:null
	
	},
	
	initialize:function(){
		try {
								
			//adding parameters to query
			var linkQuery = String.format(campaignTabs.parameters.query,campaignTabs.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(campaignTabs.parameters.listName,linkQuery,function(data){				
				//save the results to an array
				campaignTabs.parameters.resultItems = JSON.parse(data).d.results;
				 pageLayout.componentsList.push(campaignTabs);
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	}, 
	insertHTML:function()
	{
		var html="";
		var data= campaignTabs.parameters.resultItems;
		for (var i=0; i< data.length; i++){
				
				var currentItem = data[i];
				//retrieving the fields of the results
				var title = currentItem.Title;
				var url = currentItem.Url.Url;
				var description = currentItem.Descripcion;
				
				//taking the url of the image
				var image = currentItem.sys_txtImg;
				var div = document.createElement('div');
				div.innerHTML = image;
				var urlimg = div.querySelector('img').src;					
				//adding values of parameters to the html variable
				html += String.format(campaignTabs.parameters.bodyLinks,url,urlimg,title,description);
			}
			
			var htmlContent = String.format(campaignTabs.parameters.containerLinks,html);
			//insert the html code
			document.getElementById(campaignTabs.parameters.divID).innerHTML = htmlContent;
	}
}

/* Component: Slider de Clasificados */
var classifiedsSlider={
	//create parameters
	parameters:{
		containerId:"classifieds-items",
		listName:"Categorías Clasificados",	
		formatNewForm:"{0}/Lists/Clasificados/PublicarClasificado.aspx",	
		urlDetalle:"{0}/Paginas/HistoricoClasificados.aspx?category={1}",
		tagSlideContainer:"section",
		tagItemContainer:"div",
		classItemContainer:"classifieds-apps-item",
		slideFormatHTML:'<div class="classifieds-apps-item">'
							+ '<a onclick="javascript:classifiedsSlider.change(this)">'
								+ '<img src="{0}">'
								+ '<h4 class="">{1}</h4>'
								+ '<div>'
								+ 	'<h3>{2}</h3>'
								+ 	"<p>{3}<span onclick='javascript:window.open(\"{4}\", \"_blank\");'>ver más</span></p>"
								+ 	"<label onclick='javascript:sputilities.showModalDialog(\"Nuevo Clasificado\",\"{5}\",window.location.href)'>Publica tu clasificado</label>"
								+ '</div>'
							+ '</a>'
						+'</div>',
		resultItems:null,
		descTooltipSelector:".classifieds-apps-tooltip"
	},
	initialize:function(){
		//execute the query of all the items
		sputilities.getItems(classifiedsSlider.parameters.listName,'',function(data){
			classifiedsSlider.parameters.resultItems = JSON.parse(data).d.results;
			 pageLayout.componentsList.push(classifiedsSlider);
		},function(data){
			console.log(JSON.parse(data).error.message.value)
		})
		
	},
	insertHTML:function(){
		var items = classifiedsSlider.parameters.resultItems;
		//create the container of the slider
		var slideContainer = "";
		try{                                                                  
			for(var i=0;i<items.length;i++){
				//create a item container for each item in list
				var url = String.format(classifiedsSlider.parameters.urlDetalle,sputilities.contextInfo().webAbsoluteUrl,items[i].Title);
				var urlDialog = String.format(classifiedsSlider.parameters.formatNewForm,sputilities.contextInfo().webAbsoluteUrl);
				//build html code and insert in item container
				var html = String.format(classifiedsSlider.parameters.slideFormatHTML,items[i].Icono.Url,(i+1),items[i].Title, items[i].Descripcion,url,urlDialog);
				slideContainer += html;
			}
			document.getElementById(classifiedsSlider.parameters.containerId).innerHTML = slideContainer;
			var ctrl = document.getElementById(classifiedsSlider.parameters.containerId).querySelector(String.format('div.{0}:first-child a',classifiedsSlider.parameters.classItemContainer))
			classifiedsSlider.change(ctrl)
		}
		catch(e){
			console.log(String.format("Ocurrió un error en la ejecución del método classifiedsSlider.initialize(){0}",e));
		}

	},
	change:function(ctrl){
		var desc = ctrl.querySelector('div').innerHTML;
		document.querySelector(classifiedsSlider.parameters.descTooltipSelector).innerHTML = desc;
	}

}
/* Components: Regionales y Avianca Solidaria */

var regional={
	 //create the initial parameters
	 parameters:{
		listName:"Regionales",
		containerId:"information-regional",
		bodyHtml:"<img src='{0}/SiteAssets/Development/images/components/regional/1-regional.jpg' class='img-responsive'><div class='regional-item-content'><h3><span>{1}</span></h3><a href='{2}' target='_blank' class='btn-regional'>Más información</a></div>",
		query:"?select=*,TipoContenido,UrlImagen&$filter=(Vencimiento ge datetime'{0}' and TipoContenido eq 'Regionales')&$orderby=Orden desc&$top=1",
		resultItems:null
		
	 },
	 initialize:function(){
		//calculate the current date at zero hour
		var today = new Date();
		var day = ('0' +  today.getDate()).slice(-2);
		var month = ('0' +  (today.getMonth()+1)).slice(-2);
		var year = today.getFullYear();
		var expirationDate = new Date(String.format('{0}-{1}-{2}',year,month,day));
		expirationDate = expirationDate.toISOString();
		//construct the query obtaining the first five publications not expired and ordered by the column "orden"
		regional.parameters.query = String.format(regional.parameters.query,expirationDate);
		try{
		//execute query function
			sputilities.getItems(regional.parameters.listName,regional.parameters.query,function(data){
				
				var resultItems = JSON.parse(data).d.results;
				regional.parameters.resultItems = resultItems;
				 pageLayout.componentsList.push(regional);		
				
			},function(data){
				//catch errors in query execution
				console.log(JSON.parse(data).error.message.value)
			});
		}catch(e){
				//caught unexpected errors
				console.log('Ocurrió un error en la ejecución del método regional.initialize() {0}',e)
			}
	 }, 
	 insertHTML:function(){

		var html = "";	
		var data = regional.parameters.resultItems; 	
			for(var i=0;i<data.length;i++){
				
				var currentItem = data[i];
				var text = currentItem.Contenido;
				var url =currentItem.Direccion.Url;
				html += String.format(regional.parameters.bodyHtml,sputilities.contextInfo().webAbsoluteUrl,text,url);				
						
			}	
			
		document.getElementById(regional.parameters.containerId).innerHTML = html;
	 }
}

var solidarity={
	 //create the initial parameters
	 parameters:{
		listName:"Regionales",
		containerId:"information-solidarity",
		bodyHtml:"<img src='{0}' class='img-responsive'><div class='regional-item-content'><h3><span>{1}</span></h3><a href='{2}' target='_blank' class='btn-regional'>Más información</a></div>",
		query:"?select=*,TipoContenido&$filter=(Vencimiento ge datetime'{0}' and TipoContenido eq 'Avianca Solidaria')&$orderby=Orden desc&$top=1",
		resultItems:null
		
	 },
	 initialize:function(){
		//calculate the current date at zero hour
		var today = new Date();
		var day = ('0' +  today.getDate()).slice(-2);
		var month = ('0' +  (today.getMonth()+1)).slice(-2);
		var year = today.getFullYear();
		var expirationDate = new Date(String.format('{0}-{1}-{2}',year,month,day));
		expirationDate = expirationDate.toISOString();
		//construct the query obtaining the first five publications not expired and ordered by the column "orden"
		solidarity.parameters.query = String.format(solidarity.parameters.query,expirationDate);
		try{
		//execute query function
			sputilities.getItems(solidarity.parameters.listName,solidarity.parameters.query,function(data){
				
				var resultItems = JSON.parse(data).d.results;
				solidarity.parameters.resultItems = resultItems;
				 pageLayout.componentsList.push(solidarity);		
				
			},function(data){
				//catch errors in query execution
				console.log(JSON.parse(data).error.message.value)
			});
		}catch(e){
				//caught unexpected errors
				console.log('Ocurrió un error en la ejecución del método solidarity.initialize() {0}',e)
			}
	 }, 
	 insertHTML:function(){

		var html = "";	
		var data = solidarity.parameters.resultItems; 	
			for(var i=0;i<data.length;i++){
				
				var currentItem = data[i];

				var img=currentItem.UrlImagen.Url;
				var text = currentItem.Contenido;
				var url =currentItem.Direccion.Url;
				html += String.format(solidarity.parameters.bodyHtml,img,text,url);
						
			}	
			
		document.getElementById(solidarity.parameters.containerId).innerHTML = html;
	 }
}

/* Components: Unidades de negocio */
var businessUnits = {
	//create parameters
	parameters:{
			listName: "Unidades de negocio",
			top: 1,
			divID:"information-bussines-units",
			link:"<a href='{0}' target='_blank'>",
			bodyImg:"<img src='{0}' class='img-responsive'><div class='regional-item-content'><h3><span>{1}</span></h3><a href='{2}' target='_blank' class='btn-regional'>Más información</a>",
			query: "?$select=Title,sys_txtImg,Url,Orden&$orderby=Orden&$top={0}",
			resultItems:null
	
	},	
	initialize:function(){
		try {
								
			//add current date to query and filter
			var linkQuery = String.format(businessUnits.parameters.query,businessUnits.parameters.top);

			//running the query with parameters
			sputilities.getItems(businessUnits.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				businessUnits.parameters.resultItems=resultItems;
				 pageLayout.componentsList.push(businessUnits);
				
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html="";
		var data=businessUnits.parameters.resultItems;
		//loop the array
				for (var i=0; i< data.length; i++){
				
				var currentItem = data[i];

				//taking the url of the image
				var image = currentItem.sys_txtImg;
				var div = document.createElement('div');
				div.innerHTML = image;
				var urlimg = div.querySelector('img').src;

				
				//retrieving the fields of the results
				var title = currentItem.Title;
				var url = currentItem.Url.Url;
				
				//adding values to the parameters
				var bodyNew = String.format(businessUnits.parameters.bodyImg,urlimg,title,url);	
				
				//adding values of parameters to the html variable
				html += bodyNew;
					
			}
			
			//insert the html code

			document.getElementById(businessUnits.parameters.divID).innerHTML = html;
			
		
	}
}

/* Components: Clients Media */
var clientsMedia = {
	//create parameters
	parameters:{
			listName: "Clientes Multimedia",
			top: 1,
			divID:"media-clients",			
			link:"<a href='{0}' target='_blank'>",
			bodyVideo: "<img src='{0}' class='img-responsive'><a  href='javascript:void(0);' data-Type='Video' onclick='javascript:clientsMedia.click(this);' class='circulo bg-play centrado-total'><i class='inc-play centrado-total'></i></a>",										  
			//bodyImg:"<a href='{0}' target='_blank'><img src='{1}' class='img-responsive' /></a>",
			bodyImg:"<a href='javascript:void(0);' data-Type='Image' onclick='javascript:clientsMedia.click(this);'><img src='{0}' class='img-responsive'></a>",
			query: "?$select=ID,Title,UrlMultimedia,TipoMultimedia,FechaInicio,FechaFinal,Url&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}')&$orderby=FechaFinal&$top={1}",
			resultItems:null,
			detailUrl:"{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo=VideoCliente",
			detailUrlImagen:"{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo=ImagenClientes",
			modal:'sputilities.showModalDialog("Video","{0}",window.location.href)',
			htmlVideo: "<style>video\{width:100%; max-width:500px; height:auto;\}</style><div class='video-clients' style='text-align: center;'><video controls><source src='{0}' media='(max-width:480px)'></video></a></div>",
			clientsTitle:"",
			clientsID:null
	
	},
	
	initialize:function(){
		try {
			
			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	
			
			//adding parameters to query
			var linkQuery = String.format(clientsMedia.parameters.query,date,clientsMedia.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(clientsMedia.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				clientsMedia.parameters.resultItems = resultItems;
				 pageLayout.componentsList.push(clientsMedia);
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	}, 
	insertHTML:function(){
		
		var html = "";
		var data = clientsMedia.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
		
			var currentItem = data[i];
			//retrieving the fields of the results
			var title = currentItem.Title;
			var url = currentItem.Url.Url;
			var media = currentItem.UrlMultimedia.Url;
			var video="";
			//Evaluating the value of the field target
			if(currentItem.TipoMultimedia == "Imagen"){
				
				var bodyNew = String.format(clientsMedia.parameters.bodyImg,url,media);

				
			}
			else{
				var bodyNew = String.format(clientsMedia.parameters.bodyVideo,url);	
				video = String.format(clientsMedia.parameters.htmlVideo, media);			
			}
			
			//clientsMedia.parameters.htmlVideo=video;
			clientsMedia.parameters.clientsTitle=title;
			clientsMedia.parameters.clientsID = currentItem.ID;
			//adding values of parameters to the html variable			
			html += bodyNew;
					
					
		}
			
		//insert the html code
		document.getElementById(clientsMedia.parameters.divID).innerHTML = html;

	}, 
	click:function(ctrl){	
	if(ctrl.dataset.type == "Image"){
	console.log("entro a imagen")
	var url = String.format(clientsMedia.parameters.detailUrlImagen,sputilities.contextInfo().webAbsoluteUrl,clientsMedia.parameters.listName,clientsMedia.parameters.clientsID);
	sputilities.showModalDialog(clientsMedia.parameters.clientsTitle,url,window.location.href,null);

	}else{
	
		var url = String.format(clientsMedia.parameters.detailUrl,sputilities.contextInfo().webAbsoluteUrl,clientsMedia.parameters.listName,clientsMedia.parameters.clientsID);
		sputilities.showModalDialog(clientsMedia.parameters.clientsTitle,url,window.location.href,null);
		}
	}
}

/* Components: Clientes texto */
var clientsText = {
	//create parameters
	parameters:{
			listName: "Clientes Contenido Informativo",
			top: 1,
			divID:"text-clients",
			link:"",
			bodyText:"<h2>CLIENTES</h2><h1>{0}</h1><p>{1}</p><a href='{2}' target='_blank'>MÁS INFORMACIÓN<i class='inc-arrow-3 centrado-y'></i></a>",
			query: "?$select=Title,Descripcion,Url,Orden&$orderby=Orden&$top={0}",
			resultItems:null
	
	},
	
	initialize:function(){
		try {
		
			var html = "";
			//adding parameters to query
			var linkQuery = String.format(clientsText.parameters.query,clientsText.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(clientsText.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				clientsText.parameters.resultItems = resultItems;
				 pageLayout.componentsList.push(clientsText);
		
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html="";
		var data= clientsText.parameters.resultItems;
						//loop the array
				for (var i=0; i< data.length; i++){
				
				var currentItem = data[i];
				//retrieving the fields of the results
				var title = currentItem.Title;
				var url = currentItem.Url.Url;
				var description = currentItem.Descripcion;
				
				//adding values to the parameters
				var bodyNew = String.format(clientsText.parameters.bodyText,title,description,url);
				
				//adding values of parameters to the html variable
				html += bodyNew;
			}
			
			//insert the html code
			document.getElementById(clientsText.parameters.divID).innerHTML = html;
			

	}
}

/*Components: Slider Galeria*/

var sliderImg = {
	//create parameters
	parameters:{
			listName: "Galeria",
			top: 7,
			divID:"gallery-images",
			contentType: "la imagen",
			detailParameter: "Galeria",
			linkpopUp: '{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo={3}&ListaLikes=%22%22',
			link:"<a href='{0}' target='_blank'>",
			bodyImg:"<img src='{0}' /><h3>{1}</h3></a>",
			formatHtml:"<div class='gallery-image' onclick='javascript:sputilities.showModalDialog(\"{2}\",\"{3}\")'>"
						+	'<div class="container-window"></div>'
    					+	'<div class="container-bg-gallery">'
    					+	'<img class="img-bg-gallery" src="{1}" onerror="displayGenericDocumentIcon(event.srcElement ? event.srcElement : event.target, 0); return false;" onload="(event.srcElement ? event.srcElement : event.target).style.visibility = \'visible\'; SP.ScriptHelpers.resizeImageToSquareLength(this, 280)">'
        				+		'<div class="gallery-ico">'
            			+			'<a>'
                		+ 				'<img src="{0}/SiteAssets/Development/images/components/gallery/resize.png">'
                		+				'<br>'
            			+ 			'</a>'
        				+ 		'</div>'
    					+	'</div>'
						+'</div>',
			query: "?$select=Title,FileRef,FileType,ContentType/Name,ID,Destacado&$expand=ContentType&$filter=(ContentType eq '{0}' and Destacado eq 1)&$orderby=Title&$top={1}",
			resultItems: null,
			sizeSquareImage:300
	},
	insertHTML:function(){
	var html = "";
		var resultItems = sliderImg.parameters.resultItems;
		for (var i=0; i< resultItems.length; i++){
			var currentItem = resultItems[i];
			//retrieving the fields of the results
			var ID = currentItem.ID;
			var urlImg = currentItem.FileRef;
			
			var segments = urlImg.split('/');
			var segmentName = String.format("{0}.jpg",segments[segments.length -1].replace('.','_'));
			segments[segments.length  -1] = "_w";
			segments.push(segmentName);
			urlImg = segments.join('/');
			
			
			var contentType = currentItem.ContentType.Name;
			var title = currentItem.Title;			
			//adding values to the parameters
			var linkpopUpNew = String.format(sliderImg.parameters.linkpopUp,sputilities.contextInfo().webAbsoluteUrl,sliderImg.parameters.listName,ID,sliderImg.parameters.detailParameter);
			html += String.format(sliderImg.parameters.formatHtml,sputilities.contextInfo().webAbsoluteUrl,urlImg,title,linkpopUpNew)				
		}	
		//insert the html code
		document.getElementById(sliderImg.parameters.divID).innerHTML = html;
	},
	initialize:function(){
		try {
			//adding parameters to query
			var linkQuery = String.format(sliderImg.parameters.query,sliderImg.parameters.contentType,sliderImg.parameters.top);
			//running the query with parameters
			sputilities.getItems(sliderImg.parameters.listName,linkQuery,function(data){	
				//save the results to an array
				sliderImg.parameters.resultItems = JSON.parse(data).d.results;
				 pageLayout.componentsList.push(sliderImg);		
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	}
}

/* Components: Accesos Rapidos */
var quickAccess = {
	 parameters:{
		containerId:"apps-1",
		listName:"Accesos Rápidos",
		query:"?$select=Title,Icono,Direccion,Padre,Orden,Home&$filter={0}&$orderby={1}",
		containerRow:"<div class='row'>{0}</div>",
		parentContainer:"<div class='col-md-4 col-sm-4 col-xs-12 swiper-slide'>"+
						"<a href='javascript:void(0);' onclick='javascript:quickAccess.click(this);'><div class='txt-accesos' name='children-accesos-{0}'>"+
						"<img src='{1}' class='img-apps'></img><span>{2}</span></div>"+
						"<div style='display:none;'><div class='col-xs-12 apps-container-children' id='children-accesos-{0}'></div></div></a></div>",
		parentOnly:"<div class='col-md-4 col-sm-4 col-xs-12 swiper-slide'>"+
					"<a href='{0}' target='_blank'><div class='txt-accesos'><img src='{1}' class='img-apps'></img>"+
					"<span>{2}</span></div></a></div>",
		childrenHtml:"<style>.ms-dlgContent,.ms-dlgBorder,.ms-dlgFrameContainer > div{{height:auto!important;}}</style><div class='col-xs-6 apps-children'><a href='{0}' target='_blank'><div class='txt-accesos-apps'><img src='{1}' onclick='{0}' class='img-apps'></img><span>{2}</span></div></a></div>",
		html:"<ul id=patternList></ul>",
		patternTitle:[],
		resultItems:null,
		resultChildrens:null
	 },
	 initialize:function(){
		 // insert the main list of fast links
		 //document.getElementById(quickAccess.parameters.containerId).innerHTML = quickAccess.parameters.html;
		 // adjusted the query to find parents and access
		 var queryFilter = String.format(quickAccess.parameters.query,"(Padre eq null and Home eq 1)","Orden");
		 try{
			 sputilities.getItems(quickAccess.parameters.listName,queryFilter,function(data){
				 var results = JSON.parse(data).d.results;
				 quickAccess.parameters.resultItems = results;
				 var queryChildrens = String.format(quickAccess.parameters.query,"(Padre ne null and Home eq 1)","Padre");
				 
					sputilities.getItems(quickAccess.parameters.listName,queryChildrens,function(dataContent){
						quickAccess.parameters.resultChildrens = JSON.parse(dataContent).d.results;
						 pageLayout.componentsList.push(quickAccess);
					},function(data){
				 		console.log(JSON.parse(data).error.message.value)
			 		});
				 
			 },function(data){
				 console.log(JSON.parse(data).error.message.value)
			 });
		  }catch(e){
				 console.log(String.format("Ocurrió un error en la ejecución del método quickAccess.initialize() {0}",e))
          }
	 },
	 insertHTML:function(){
	    var data =  quickAccess.parameters.resultItems;
	    var count = 0;
	    var html = "";
	 	//run the results array to insert the accesses and the parent elements
		for(var i = 0;i < data.length;i++){
			 if(data[i].Direccion == null){
				quickAccess.parameters.patternTitle.push({'title': data[i].Title, 'orden':data[i].Orden});						
				html += String.format(quickAccess.parameters.parentContainer,data[i].Orden,data[i].Icono.Url,data[i].Title);												 
			 }else{
				html += String.format(quickAccess.parameters.parentOnly,data[i].Direccion.Url,data[i].Icono.Url,data[i].Title);						
			 }
			 count +=1;
			 if(count == 3 || i== data.length-1)
			 {
			 	var body = String.format(quickAccess.parameters.containerRow, html);
			 	document.getElementById(quickAccess.parameters.containerId).insertAdjacentHTML('beforeend',body);
			 	html = "";
			 	count = 0;
			 }
		//document.getElementById('patternList').insertAdjacentHTML('beforeend',newAccess);
		}
		var dataChildren = quickAccess.parameters.resultChildrens;
		var parents = quickAccess.parameters.patternTitle;
		var currentParent="";
		var htmlChildren="";
		for(var i=0; i <dataChildren.length;i++)
		{
			if(dataChildren[i].Direccion != null){
			
				 if(i==0)
				 {
				 	currentParent = dataChildren[i].Padre;
				 	htmlChildren += String.format(quickAccess.parameters.childrenHtml,dataChildren[i].Direccion.Url,dataChildren[i].Icono.Url,dataChildren[i].Title);		
				 }
				 else
				 {
				 	if(currentParent != dataChildren[i].Padre)
				 	{
				 		for(var j=0;j<parents.length;j++)
				 		{
				 			if(parents[j].title == currentParent)
				 			{
				 				var containerid=String.format("children-accesos-{0}", parents[j].orden);
				 				document.getElementById(containerid).insertAdjacentHTML('beforeend',htmlChildren);
				 				htmlChildren="";
				 			}
				 		}				 		
				 		currentParent = dataChildren[i].Padre;
				 		htmlChildren += String.format(quickAccess.parameters.childrenHtml,dataChildren[i].Direccion.Url,dataChildren[i].Icono.Url,dataChildren[i].Title);
				 	}
				 	else
				 	{			 		
				 		currentParent = dataChildren[i].Padre;
				 		htmlChildren += String.format(quickAccess.parameters.childrenHtml,dataChildren[i].Direccion.Url,dataChildren[i].Icono.Url,dataChildren[i].Title);	
				 		
				 	}
				 	
				 	if(i == dataChildren.length - 1)
				 		{
				 			for(var j=0;j<parents.length;j++)
				 			{
					 			if(parents[j].title == currentParent)
					 			{
					 				var containerid=String.format("children-accesos-{0}", parents[j].orden);
					 				document.getElementById(containerid).insertAdjacentHTML('beforeend',htmlChildren);
					 				htmlChildren="";
					 			}
				 			}
				 		}
				 	
				 }
					
				
			}
		}
		//quickAccess.getChildrenList();
	 },
	 click:function(ctrl){
	  var id = ctrl.firstElementChild.attributes.name.value;
	  var node = document.getElementById(id);
	  //console.log(node.innerHtml);
		sputilities.showModalDialog(ctrl.querySelector('span').innerHTML,null,null,node.parentElement.innerHTML);

	 }	 	 
 }
 
 /*Component twitter*/
var twitter = {	
	parameters:{
		containerId: "social-twitter",
		htmlVideo: '<a class="twitter-timeline"  href="https://twitter.com/search?q=%40Avianca" data-widget-id="868102404264382464">Tweets sobre @Avianca</a>',
		/*'<a class="twitter-timeline" href="https://twitter.com/Avianca"></a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>',*/
		code: '<a class="twitter-timeline" href="https://twitter.com/Avianca">Tweets by Avianca</a>'
							
	},
	initialize:function(){
					
		try{	
		 pageLayout.componentsList.push(twitter);
			}catch(e){
			console.log(String.format("Error en metodo twitter.initialize: {0}",e))
		}				
	},	
	insertHTML:function(){
		var button = "<a href='javascript:void(0);' onclick='javascript:twitter.click();'><img src='https://avianca.sharepoint.com/sites/Intranet/SiteAssets/Development/images/components/twitter/aviancatwitter.jpg'></img></a>";
		var twitterContainer = document.getElementById(twitter.parameters.containerId);
		var embed = twitter.parameters.code;
		twitterContainer.innerHTML = button;
//		$('head').append('<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>');
	},
	insertscript:function(){	
		//$('head').append('<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>');
		$('head').append('<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>');
	},
	click:function(){		
		sputilities.showModalDialog("Twitter",null,null,twitter.parameters.htmlVideo);
		twitter.insertscript();
	}
}
/*component startPopup */
var startPopup={
    parameters:{
        controlList:'ControlPopUp',
        queryControl:'?$filter=userid eq {0}',
        contentList: 'Popup',
        queryControlVideo:'?$filter=activo eq 1&$orderby=ID desc',
        dateFormat:'{0}/{1}/{2}',
        today:null,
        userId:null,
        showPopup:null                
    },
    initialize:function(){
        try{
            //get today date            
            var today = new Date();
            var year = today.getFullYear();
            var month = today.getMonth()+1;
            var day = today.getDate();
            startPopup.parameters.today = String.format(startPopup.parameters.dateFormat,year,month,day)
            //get user id
            sputilities.getCurrentUserInformation(function(data){
                    startPopup.parameters.userId = JSON.parse(data).d.ID;                    
                    //confirm user in control list
                    var query = String.format(startPopup.parameters.queryControl,startPopup.parameters.userId);
                    sputilities.getItems(startPopup.parameters.controlList,query,function(data){
                        var results = JSON.parse(data).d.results;                        
                        if(results.length > 0){
                        	
                            //if user exist in list get current date
                            var date = new Date(results[0].date);
                            var year = date.getFullYear();
                            var month = date.getMonth()+1;
                            var day = date.getDate();
                            currentDate = String.format(startPopup.parameters.dateFormat,year,month,day);
                            if(currentDate == startPopup.parameters.today){
                                //start control in video list
                                startPopup.parameters.showPopup = false;
                                
                            }else{
                                startPopup.parameters.showPopup = true;
                                //update user register
                                SP.SOD.executeFunc('sp.js', 'SP.ClientContext',startPopup.updateControlPopup());                                
                            }
                        }else{                            
                            startPopup.parameters.showPopup = true;
                            //create new register
                            SP.SOD.executeFunc('sp.js', 'SP.ClientContext',startPopup.createUserControlPopup());                            
                        }
                    },function(data){
                        startPopup.msgError(data)
                    }
                );
                },function(data){
                    startPopup.msgError(data)
                }
            );
        }catch(e){            
            console.log(String.format('Ocurrío un error en la ejecucion del componente startPopup {0}',e))
        }        
    },
    showVideoPopup:function(){
    console.log('showVideo')
        try{
            //get video by list popup
            sputilities.getItems(startPopup.parameters.contentList,startPopup.parameters.queryControlVideo,
            function(data){
                var results = JSON.parse(data).d.results;
                if(results.length>0 && startPopup.parameters.showPopup == true){
                    //create container video
                    var videoContainer = document.createElement('div');
                    videoContainer.setAttribute('class','popUpVideo-container');
                    videoContainer.style.overflow = 'hidden';
                    videoContainer.style.width = '100%';
                    videoContainer.innerHTML = results[0].contenido;                    
                    var options ={
                        html: videoContainer,
                        title: ' ',
                        width: 400,
                        autoSize: true
                    };
                    SP.UI.ModalDialog.showModalDialog(options);
                    videoContainer.parentElement.parentElement.previousElementSibling.classList.add('dialog-title')
                }
                },function(data){
                        startPopup.msgError(data)
                    }
            );
            
        }catch(e){
            console.log(String.format('Ocurrío un error en la ejecucion del componente startPopup {0}',e))
        }
    },
    updateControlPopup:function(){
    console.log('update')
        try{
            var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
            var oList = clientContext.get_web().get_lists().getByTitle(startPopup.parameters.controlList);
            var today = new Date();
            console.log(startPopup.parameters.userId)
            var oListItem = oList.getItemById(startPopup.parameters.userId);
            oListItem.set_item('date', today.toString());
            oListItem.update();
            clientContext.executeQueryAsync(
                Function.createDelegate(this, this.showVideoPopup),
                Function.createDelegate(this, this.msgError));
        }catch(e){
            console.log(String.format('Ocurrío un error en la ejecucion del componente startPopup {0}',e))
        }
    },
    createUserControlPopup:function(){
    console.log('create')
        try{
            var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
            var oList = clientContext.get_web().get_lists().getByTitle(startPopup.parameters.controlList);
            var itemCreateInfo = new SP.ListItemCreationInformation();
            var today = new Date();
            var oListItem = oList.addItem(itemCreateInfo);
            oListItem.set_item('userid', startPopup.parameters.userId);
            oListItem.set_item('date', today.toString());
            oListItem.update();
            clientContext.load(oListItem);
            clientContext.executeQueryAsync(
                Function.createDelegate(this, this.showVideoPopup), 
                Function.createDelegate(this, this.msgError));  
        }catch(e){
           console.log(String.format('Ocurrío un error en la ejecucion del componente startPopup {0}',e))
        }
    },
    msgError:function(sender, args){
        console.log(args.get_message() + '\n' + args.get_stackTrace())
    }
}


sputilities.callFunctionOnLoadBody('pageLayout.load');
var pageLayout =
{
	components:{
		startPopup : startPopup,
		marquee : marquee,
		staticNews : staticNews, 
		twitter : twitter, 
		sliderNews : sliderNews, 
		economIndic : economIndic, 
		imageDay : imageDay, 	
		videoPoster : videoPoster, 	
		benefits : benefits,
		clientsMedia : clientsMedia, 
		clientsText : clientsText,
		campaignTabs : campaignTabs, 	
		important : important, 
		graphicsImg : graphicsImg,
		offerDay :  offerDay,
		regional : regional,
		solidarity : solidarity,
		businessUnits : businessUnits, 
		sliderImg : sliderImg, 
		events : events, 	
		quickAccess : quickAccess, 
		classifiedsSlider : classifiedsSlider 
	},
	load:function(){		
		/*var div = document.createElement('div');
		div.id = "img-load";
		div.setAttribute('class','row');
		div.setAttribute('style','margin-left:auto;margin-right:auto;text-align:center');
		
		var img = document.createElement('img');
		img.src = String.format("{0}/SiteAssets/Development/images/master/cargando.gif",sputilities.contextInfo().webAbsoluteUrl);
		img.setAttribute('class','img-responsive');
		img.setAttribute('style','margin-left:auto;margin-right:auto;text-align:center');
		
		div.appendChild(img);
		document.getElementById('s4-workspace').parentElement.insertBefore(div,document.getElementById('s4-workspace'));*/
		//document.getElementById("img-load").style.display = "none";
		
		pageLayout.initComponents();
		
		var loadControl = setInterval(function(){								
			document.getElementById('ms-designer-ribbon').style.opacity = 0;
			document.getElementById('s4-workspace').style.opacity = 0;			
			if( pageLayout.componentsList.length  == pageLayout.max){
				for(var i = 0; i <  pageLayout.componentsList.length ; i++){
					 pageLayout.componentsList[i].insertHTML();
				}
				//pageLayout.components.startPopup.initialize();				
				clearInterval(loadControl);	
				pageLayout.animations();
				//document.getElementById("img-load").style.display = "none";
				document.getElementById('s4-workspace').style.opacity = 1;
				document.getElementById('ms-designer-ribbon').style.opacity = 1;
				
				setTimeout(function(){
										
				},5000);							
			}
						
		},500);	
		
	},
	initComponents:function(){
		
		
		pageLayout.components.marquee.initialize();
		pageLayout.components.staticNews.initialize();
		pageLayout.components.twitter.initialize();
		pageLayout.components.sliderNews.initialize();
		pageLayout.components.economIndic.initialize();
		pageLayout.components.imageDay.initialize();	
		pageLayout.components.videoPoster.initialize();	
		pageLayout.components.benefits.initialize();
		pageLayout.components.clientsMedia.initialize();
		pageLayout.components.clientsText.initialize();
		pageLayout.components.campaignTabs.initialize();	
		pageLayout.components.important.initialize();
		pageLayout.components.graphicsImg.initialize();
		pageLayout.components.offerDay.initialize();
		pageLayout.components.regional.initialize();
		pageLayout.components.solidarity.initialize();
		pageLayout.components.businessUnits.initialize();
		pageLayout.components.sliderImg.initialize();
		pageLayout.components.events.initialize();	
		pageLayout.components.quickAccess.initialize();
		pageLayout.components.classifiedsSlider.initialize();
		
			

	},
	animations:function(){
		$.noConflict();
		$('#carousel-header').on('slid.bs.carousel', function () {
		    $holder = $( "ol li.active" );
		    $holder.removeClass('active');
		    var idx = $('div.active').index('div.item');
		    $('ol.carousel-indicators li[data-slide-to="'+ idx+'"]').addClass('active');
		});
		console.log("hola")
		$('#carousel-header').carousel({
  		interval: 10000
		})
		
		$('ol.carousel-indicators  li').on("click",function(){ 
		    $('ol.carousel-indicators li.active').removeClass("active");
		    $(this).addClass("active");
		});
		$(".carousel").swipe({
		
		 	swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
		
		    if (direction == 'left') $(this).carousel('next');
		    if (direction == 'right') $(this).carousel('prev');
		
		  },
		  allowPageScroll:"vertical"
		
		  });
		
		$('.information-offers').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
		});
		$('.video-todayy').slick({
		  dots: false,
		  autoplay:false,
		  autoplaySpeed:7000,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
		});

		
		$('.information-numbers').slick({
		  dots: false,
		  autoplay:true,
		  autoplaySpeed:7000,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
		});
		
		
		$('.slider-container').slick({
		  dots: false,
		  infinite: true,
		  speed: 700,
		  slidesToShow: 4,
		  slidesToScroll: 1,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 4,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
		});
		
		$('.responsive').slick({
		  dots: false,
		  infinite: true,
		  speed: 700,
		  slidesToShow: 2,
		  slidesToScroll: 1,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
		});
		
		$('.variable-width2').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 1,
		  variableWidth: false
		 });
		 
		$('#gallery-images').slick({
			  dots: false,
			  infinite: true,
			  speed: 700,
			  slidesToShow: 3,
			  slidesToScroll: 1,
			  responsive: [
			    {
			      breakpoint: 1024,
			      settings: {
			        slidesToShow: 2,
			        slidesToScroll: 1,
			        infinite: true,
			        dots: false
			      }
			    },
			    {
			      breakpoint: 600,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    },
			    {
			      breakpoint: 480,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    }
			  ]
			});
	},
	componentsList:[],
	max:21
	
}


/*Component: Noticias List Dic2018*/

var listNews = {
	//create parameters
	parameters:{
			listName: "Páginas",
			top: 5,
			divID:"zona-noticias-list",
			containerNews:"<div class='otherNews-item'>{0}{1}</div>",			
			imageNews:"<div class='otherNews-item-image-container'><a href='{2}' target='_blank'><img src='{0}' alt='Avatar' class='otherNews-item-image-image'/>" +
					  "<div class='otherNews-item-image-overlay'><div class='otherNews-item-image-text'>" +
					  "<img src='{1}/SiteAssets/Development/images/components/news/avion-hover.png'/></div></div></a></div>",
			textNews:"<div class='otherNews-item-text-container'><h3>{0}</h3><p>{1}</p><a href='' target='_blank'>Ver más<i class='inc-arrow-3 centrado-y'></i></a></div>",
			//query: "?$select=Title,FechaFinal,FechaInicio,Url,sys_txtImg,Descripcion,Orden&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}')&$orderby=Orden&$top={1}",
			query: "?$select=FileRef,ID,ContentType/Name,Title,Entradilla,sys_txtImg,FechaInicio,FechaFinal&$expand=ContentType&$filter=(ContentType eq 'Noticias' and FechaFinal le datetime'{0}')&$orderby=ID&$top={1}",
				
			resultItems:null		
	
	},
	initialize:function(){
		try {

			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			//get date and execute query
			var today = new Date();
			today = today.toISOString();
			finalQuery = String.format(listNews.parameters.query,today,listNews.parameters.top);

			//running the query with parameters
			sputilities.getItems(listNews.parameters.listName,finalQuery ,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				listNews.parameters.resultItems = resultItems;
				pageLayout.componentsList.push(listNews);
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var data = listNews.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];
			
			//retrieving the fields of the results
			var title = currentItem.Title;
			var url = currentItem.FileRef;
				
			//taking the url of the image
			var image = currentItem.sys_txtImg;
			var div = document.createElement('div');
			div.innerHTML = image;
			var urlimg = div.querySelector('img').src;			
					
			//make html with the fields 
			var imageHtml = String.format(listNews.parameters.imageNews, urlimg,sputilities.contextInfo().webAbsoluteUrl, url);
			var textHtml = String.format(listNews.parameters.textNews,title,url);			
					
			//adding html 
			html += String.format(listNews.parameters.containerNews,imageHtml,textHtml);			
					
		}
			
		//insert the html code
		document.getElementById(listNews.parameters.divID).innerHTML = html;
	}
}


