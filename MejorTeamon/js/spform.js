﻿var spForms	={
	options:{},
	parameters:{
		formats:{
			formsHideElement:"forms-hide-element",
			icoHelpSelected:"ico-help-selected",
			helpBlock:"help-block-{0}",
			requiredError: "Esto no puede estar en blanco",
			singleUserError:"Solo se admite una selección",
			maxNum:"El valor supera el máximo permitido",
			minNum:"El valor es menor al minimo permitido",
			requiredField:"<i>*</i>"
		},
		updateAction:"spForms.updateForm({0},{1},{2})",
		saveAction:"spForms.saveForm({0},{1},{2})",
		cancelAction:"spForms.cancelForm({0})",
		formatForm:	"<div class='container-fluid'>" +
					    "<div class='center-container forms-template'>" +
					    	"<div class='loader-container'><h3>Trabajando en ello</h3><div class='loader'></div></div>"+
					        "<div class='form-content'>" +
					            "<div class='form-header'>" +
					                "<h2>{0}</h2>" +
					                "<p>{1}</p>" +
					            "</div>" +
					            "<div class='form-body form-horizontal'>" +   
					                "{2}"+
					                "<div class='row buttons-form'>{3}</div>" +   
					            "</div>" + 
					        "</div>" +
					    "</div>" +
					"</div>",
		formatFormButtom:"<input type='button' class='btn btn-form' id='btn-{0}'  value='{1}' onclick='javascript:return {2}'/>",
		fieldType:{
			"text":{
	            name:"Text",
	            html:"<div class='form-group row' sp-static-name='{0}' sp-hidden='{6}'>" +
					    "<label class='col-sm-4 control-label' for='{0}' id='label-{0}'>{1}{5}</label>" +
					    "<div class='col-sm-8'>" +
					        "<div class='form-control-help'>" +
					            "<input type='text' class='form-control input-lg' sp-type='text' id='{0}' placeholder='{1}' sp-title='{1}'  sp-required='{3}' sp-max-length='{4}' autocomplete='off'>" +
					            "<a class='glyphicon glyphicon-question-sign ico-help' id='ico-help-{0}' onclick='javascript:spForms.showHelpDescription(\"{0}\",this)' aria-hidden='true'></a>" +
					        "</div>" +
					        "<span id='help-block-{0}' class='help-block forms-hide-element'>{2}</span>" +
					        "<span id='help-block-error-message-{0}' class='help-block error-message forms-hide-element'></span>" +
					    "</div>" +
					"</div>",
				load:function(spField,order,callback){
					var field = spField;
					return {
						required:field.Required,
						displayName: field.Title,
						internalName:field.InternalName,
						id:field.Id,
						type:field.TypeAsString,	
						row:String.format(spForms.parameters.fieldType.text.html,field.InternalName,field.Title,field.Description, field.Required,field.MaxLength, field.Required ? spForms.parameters.formats.requiredField: "", field.Hidden),
						order:order,
						value:null
					}
				},
				validateRequired:function(spFormField){
					spForms.validateCommonFieldRequired(spFormField);
				},
				getValue:function(spFormField){
					spFormField.value = document.getElementById(spFormField.internalName).value;
				},
				getSPValueField:function(spFormField){
					spForms.getValueFieldCommon(spFormField);
				},
				setSPFormValue:function(spFormField){
					if(spFormField.value != null){
						document.getElementById(spFormField.internalName).value = spFormField.value;
					}
				}
			},
		    "note":{
		            name:"Note",
		            html:	"<div class='form-group row' sp-static-name='{0}' sp-hidden='{6}'>" +
							    "<label class='col-sm-4 control-label' for='{0}' id='label-{0}'>{1}{5}</label>" +
							    "<div class='col-sm-8'>" +
							        "<div class='form-control-help'>" +
							            "<textarea class='form-control input-lg' rows='4' sp-type='note' id='{0}' placeholder='{1}' sp-title='{1}'  sp-required='{3}' sp-numlines='{4}'></textarea>" + 
							            "<a class='glyphicon glyphicon-question-sign ico-help' id='ico-help-{0}' onclick='javascript:spForms.showHelpDescription(\"{0}\",this)'aria-hidden='true'></a>" +
							        "</div>" +
							        "<span id='help-block-{0}' class='help-block forms-hide-element'>{2}</span>" +
							        "<span id='help-block-error-message-{0}' class='help-block forms-hide-element error-message' ></span>" +
							    "</div>" +
							"</div>",
		            load:function(spField,order,callback){
		            	var field = spField;
						return {
							required:field.Required,
							displayName: field.Title,
							internalName:field.InternalName,
							id:field.Id,
							type:field.TypeAsString,	
							row:String.format(spForms.parameters.fieldType.note.html,field.InternalName,field.Title,field.Description, field.Required,field.NumberOfLines,field.Required ? spForms.parameters.formats.requiredField: "",field.Hidden),
							order:order,
							value:null
						}
					},
					validateRequired:function(spFormField){
						spForms.validateCommonFieldRequired(spFormField);
					},
					getValue:function(spFormField){
						var row = document.querySelector(String.format('[sp-static-name="{0}"]',spFormField.internalName))
						if(row != null){
							spFormField.value = row.querySelector(String.format('#{0}',spFormField.internalName)).value;
						}
					},
					getSPValueField:function(spFormField){
						var data = spForms.getValueFieldCommon(spFormField);
						return data;
					},
					setSPFormValue:function(spFormField){
						if(spFormField.value != null){
							document.getElementById(spFormField.internalName).value = spFormField.value;
						}
					}
		    },
		    "choice":{
		            name:"Choice",
		            html:"<div class='ms-formlabel'>" +
		            		"<label for='{0}' id='label-{0}'>{1}</label>" +
		            	"</div>" + 
		            	"<div class='ms-formbody'>" + 
		            		"<{3} sp-type='choice' id='{0}' sp-title='{1}' sp-required='{2}'>{4}</{3}>"+
		            	"</div>",
		            htmlSelect:		'<div class="form-group row" sp-static-name="{0}" sp-hidden="{6}">' +
									    '<label class="col-sm-4 control-label">{1}{5}</label>' +
									    '<div class="col-sm-8">' +
									        '<div class="form-control-help">' +
									            '<select class="form-control input-lg" id="{0}" sp-required="{4}" sp-type="choice">' +
									            '{3}' +
									            '</select>' +
									            '<a class="glyphicon glyphicon-question-sign ico-help" id="ico-help-{0}" onclick="javascript:spForms.showHelpDescription(\'{0}\',this)" aria-hidden="true"></a>' +
									        '</div>' +
									        '<span id="help-block-{0}" class="help-block forms-hide-element">{2}</span>' +
									        "<span id='help-block-error-message-{0}' class='help-block forms-hide-element error-message' ></span>" +
									    '</div>' +
									'</div>',
					htmlOption:"<option id='{0}-{1}' value='{0}'>{2}</option>",
		            htmlRadios:	'<div class="form-group row form-radio" sp-type="choice" sp-static-name="{0}" sp-required="{4}" sp-hidden="{6}">' +
								    '<label class="col-sm-4 control-label">{1}{5}</label>' +
								    '<div class="col-sm-8">' +
								    	'<div class="form-control-help">' +
                                			'<div class="list-checkbox-radio">' +
								        		'{3}' +
								    		'</div>' +
								    		'<a class="glyphicon glyphicon-question-sign ico-help" id="ico-help-{0}" onclick="javascript:spForms.showHelpDescription(\'{0}\',this)" aria-hidden="true"></a>' +
										'</div>' +
										'<span id="help-block-{0}" class="help-block forms-hide-element">{2}</span>' +
										"<span id='help-block-error-message-{0}' class='help-block forms-hide-element error-message' ></span>" +
									'</div>' +
								'</div>', 
		            htmlRadio:	'<div class="radio">' +
							    '<label>' +
							        '<input type="radio" sp-type="choice" id="{0}-{1}" value="{2}" name="{1}">' +
							        '{2}' +
							    '</label>' +
							'</div>',
		            load:function(spField,order,callback){
		            	var field = spField;
		            	var parser = new DOMParser();
						var schema = parser.parseFromString(field.SchemaXml,"text/xml");
						var ctrlType = schema.documentElement.getAttribute('Format');
						var formatControl = spForms.parameters.fieldType.choice.htmlRadio;
						var htmlField = spForms.parameters.fieldType.choice.htmlRadios;
						
						if(ctrlType == "Dropdown"){
							htmlField = spForms.parameters.fieldType.choice.htmlSelect;
							formatControl = spForms.parameters.fieldType.choice.htmlOption;
						}
						var content = "";
						for(var j = 0; j < field.Choices.results.length; j++){
							content += String.format(formatControl,j,field.InternalName,field.Choices.results[j]);
						}
						return {
							required:field.Required,
							displayName: field.Title,
							internalName:field.InternalName,
							id:field.Id,
							type:field.TypeAsString,	
							row:String.format(htmlField,field.InternalName,field.Title,field.Description,content,field.Required,field.Required ? spForms.parameters.formats.requiredField: "",field.Hidden),
							order:order,
							value:null
						}

		            },
					validateRequired:function(spFormField){
						spForms.validateRadioChecksFieldRequired(spFormField);
					},
					getValue:function(spFormField){
						var row =  document.querySelector(String.format('[sp-static-name="{0}"]',spFormField.internalName));
						var select = row.querySelector('select');
						if(select != null){
							spFormField.value = select.options[select.selectedIndex].innerText;
							return;
						}
						
						var radio = row.querySelector('input[type="radio"]:checked');
						if(radio != null){
							spFormField.value = radio.value;
						}
					},
					getSPValueField:function(spFormField){
						spForms.getValueFieldCommon(spFormField);
					},
					setSPFormValue:function(spFormField){
						if(spFormField.value == null){
							return;
						}
						var row = document.querySelector(String.format('[sp-static-name="{0}"]',spFormField.internalName));
						var select = row.querySelector('select');
						if(select != null){
							for(var i = 0; i < select.options.length ; i++){
								var option = select.options[i];
								if(option.innerText == spFormField.value){
									select.selectedIndex = option.index;
								}
							}
						}else{
							var radio = row.querySelector(String.format('input[type="radio"][value="{0}"]',spFormField.value));
							radio.click();
						}
					}

		    },
		    "multichoice":{
		    	name:"MultiChoice",
		    	html:	'<div class="form-group row form-checkbox" sp-static-name="{0}" sp-required="{4}" sp-type="multichoice" sp-hidden="{6}">' +
						    '<label class="col-sm-4 control-label">{1}{5}</label>' +
						    '<div class="col-sm-8">' +
						    	'<div class="form-control-help">' +
                                	'<div class="list-checkbox-radio"> ' +
						        		'{3}' +
						    		'</div>' +
						    		'<a class="glyphicon glyphicon-question-sign ico-help" id="ico-help-{0}" aria-hidden="true" onclick="javascript:spForms.showHelpDescription(\'{0}\',this)"></a>' + 
						    	'</div>' +
								'<span id="help-block-{0}" class="help-block forms-hide-element">{2}</span>' +
								"<span id='help-block-error-message-{0}' class='help-block forms-hide-element error-message' ></span>" +
							'</div>' +
						'</div>',
			    htmlCheckBox:	'<div class="checkbox">' +
								    '<label>' +
								        '<input type="checkbox" id="{0}-{1}" name="{1}" value="{2}"/>' +
								        '{2}' +
								    '</label>' +
								'</div>',
		       	load:function(spField,order,callback){
	            	var field = spField;
					var content = "";
					for(var j = 0; j < field.Choices.results.length; j++){
						content += String.format(spForms.parameters.fieldType.multichoice.htmlCheckBox,j,field.InternalName,field.Choices.results[j]);
					}
					return {
							required:field.Required,
							displayName: field.Title,
							internalName:field.InternalName,
							id:field.Id,
							type:field.TypeAsString,	
							row:String.format(spForms.parameters.fieldType.multichoice.html,field.InternalName,field.Title,field.Description,content,field.Required,field.Required ? spForms.parameters.formats.requiredField: "",field.Hidden),
							order:order,
							value:null
						}

		       	},
				validateRequired:function(spFormField){
					spForms.validateRadioChecksFieldRequired(spFormField);
				},
				getValue:function(spFormField){
					var row =  document.querySelector(String.format('[sp-static-name="{0}"]',spFormField.internalName));						
					var checks = row.querySelectorAll('input[type="checkbox"]:checked');
					var values =[];
					for(var i = 0; i < checks.length;i++){
						var check = checks[i];
						values.push(check.value);
					}
					spFormField.value = values;
				},
				getSPValueField:function(spFormField){
					spForms.getValueFieldMultipleSelection(spFormField);
				},
				setSPFormValue:function(spFormField){
					var row = document.querySelector(String.format('[sp-static-name="{0}"]',spFormField.internalName));
					if(spFormField.value != null){
						for(var i=0; i < spFormField.value.results.length; i++){
							var val = spFormField.value.results[i];
							var check = row.querySelector(String.format('input[type="checkbox"][value="{0}"]',val));
							if(check != null){
								check.click();
							}
						}
					}
				}

		    },
		    "number":{
		            name:"Number",
		            html:"<div class='form-group row' sp-static-name='{0}' sp-hidden='{7}'>" +
						    "<label class='col-sm-4 control-label' for='{0}' id='label-{0}'>{1}{6}</label>" +
						    "<div class='col-sm-8'>" +
						        "<div class='form-control-help'>" +
						            "<input onwheel='javascript:event.preventDefault()' type='number' class='form-control input-lg' sp-type='number' id='{0}' placeholder='{1}' sp-title='{1}'  sp-required='{3}' sp-maxnum='{4}' sp-minnum='{5}' autocomplete='off'>" +
						            "<a class='glyphicon glyphicon-question-sign ico-help' id='ico-help-{0}' onclick='javascript:spForms.showHelpDescription(\"{0}\",this)'aria-hidden='true'></a>" +
						        "</div>" +
						        "<span id='help-block-{0}' class='help-block forms-hide-element'>{2}</span>" +
						        "<span id='help-block-error-message-{0}' class='help-block forms-hide-element error-message' id='{0}-error-message'></span>" +
						    "</div>" +
						"</div>",
		            load:function(spField,order,callback){
		            	var field = spField;
		            	var parser = new DOMParser();
						var schema = parser.parseFromString(field.SchemaXml,"text/xml");
						var decimals = schema.documentElement.getAttribute('Decimals');
						if(decimals != null){
							decimals = parseInt(decimals);
						}else{
							decimals = 0;
						}
						return {
							required:field.Required,
							displayName: field.Title,
							internalName:field.InternalName,
							id:field.Id,
							type:field.TypeAsString,	
							row:String.format(spForms.parameters.fieldType.number.html,field.InternalName,field.Title,field.Description, field.Required,field.MaximumValue,field.MinimumValue,field.Required ? spForms.parameters.formats.requiredField: "",field.Hidden),
							order:order,
							decimals:decimals,
							value:null
						}
		            },
					validateRequired:function(spFormField){
						spForms.validateCommonFieldRequired(spFormField);
					},
					getValue:function(spFormField){
						var value = document.getElementById(spFormField.internalName).value;
						if(value.indexOf('.') == -1){
							spFormField.value = String.format('{0}.0',value);
						}
						else if(value.split('.')[1] == ""){
							spFormField.value = value.replace('.','');
						}
						else{
							spFormField.value = value;
						}
						
					},
					getSPValueField:function(spFormField){
						spForms.getValueFieldCommon(spFormField);
					},
					setSPFormValue:function(spFormField){
						document.getElementById(spFormField.internalName).value = spFormField.value;
					}

		    },
		    "currency":{
		            name:"Currency",
		            html:	"<div class='form-group row' sp-static-name='{0}' sp-hidden='{7}'>" +
							    "<label class='col-sm-4 control-label' for='{0}' id='label-{0}'>{1}{6}</label>" +
							    "<div class='col-sm-8'>" +
							        "<div class='form-control-help'>" +
							            "<input onwheel='javascript:event.preventDefault()' type='number' class='form-control input-lg' sp-type='currency' id='{0}' placeholder='{1}' sp-title='{1}'  sp-required='{3}' sp-maxnum='{4}' sp-minnum='{5}' autocomplete='off'>" +
							            "<a class='glyphicon glyphicon-question-sign ico-help' id='ico-help-{0}' onclick='javascript:spForms.showHelpDescription(\"{0}\",this)'aria-hidden='true'></a>" +
							        "</div>" +
							        "<span id='help-block-{0}' class='help-block forms-hide-element'>{2}</span>" +
							        "<span id='help-block-error-message-{0}' class='help-block forms-hide-element error-message' id='{0}-error-message'></span>" +
							    "</div>" +
							"</div>",
	            	load:function(spField,order,callback){
		            	var field = spField;
		            	var parser = new DOMParser();
						var schema = parser.parseFromString(field.SchemaXml,"text/xml");
						var decimals = schema.documentElement.getAttribute('Decimals');
						if(decimals != null){
							decimals = parseInt(decimals);
						}else{
							decimals = 0;
						}
		            	return {
							required:field.Required,
							displayName: field.Title,
							internalName:field.InternalName,
							id:field.Id,
							type:field.TypeAsString,	
							row: String.format(spForms.parameters.fieldType.currency.html,field.InternalName,field.Title,field.Description, field.Required,field.MaximumValue,field.MinimumValue,field.Required ? spForms.parameters.formats.requiredField: "",field.Hidden),
							order:order,
							decimals:decimals,
							value:null
						}
	            	},
					validateRequired:function(spFormField){
						spForms.validateCommonFieldRequired(spFormField);
					},
					getValue:function(spFormField){
							var value = document.getElementById(spFormField.internalName).value;
							if(value.indexOf('.') == -1){
								spFormField.value = String.format('{0}.0',value);
							}
							else if(value.split('.')[1] == ""){
								spFormField.value = value.replace('.');
							}
							else{
								spFormField.value = value;
							}
					},
					getSPValueField:function(spFormField){
						spForms.getValueFieldCommon(spFormField);
					},
					setSPFormValue:function(spFormField){
						document.getElementById(spFormField.internalName).value = spFormField.value;
					}
		    },
		    "datetime":{
		            name:"DateTime",
		            html:'<div class="form-group row form-date" sp-static-name="{0}" sp-hidden="{6}">' +
						    '<label class="col-sm-4 control-label">{1}{4}</label>' +
						    '<div class="col-sm-8">' +
						    	'<div class="form-control-help">' +
							        '<div class="input-group date">' +
							            '<input type="text" class="form-control input-lg" sp-type="datetime" id="{0}" sp-dateOnly="{5}" placeholder="{1}" sp-title="{1}"  sp-required="{3}" autocomplete="off">' +
							            '<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>' +
							            '{2}' +
							       '</div>' +
							       '<a class="glyphicon glyphicon-question-sign ico-help" id="ico-help-{0}" aria-hidden="true" onclick="javascript:spForms.showHelpDescription(\'{0}\',this)"></a>' +
							   	'</div>' +
							   	'<span id="help-block-{0}" class="help-block forms-hide-element">{2}</span>' +
							   	"<span id='help-block-error-message-{0}' class='help-block forms-hide-element error-message' id='{0}-error-message'></span>" +
						    '</div>' +
						'</div>',		
						load:function(spField,order,callback){
							var field = spField;
		            		var parser = new DOMParser();
							var schema = parser.parseFromString(field.SchemaXml,"text/xml");
							var isDateOnly = schema.documentElement.getAttribute('Format') == "DateOnly";
			            	return {
								required:field.Required,
								displayName: field.Title,
								internalName:field.InternalName,
								id:field.Id,
								type:field.TypeAsString,	
								row: String.format(spForms.parameters.fieldType.datetime.html,field.InternalName,field.Title,field.Description, field.Required,field.Required ? spForms.parameters.formats.requiredField: "",isDateOnly,field.Hidden),
								order:order,
								value:null
							}
			            },
						validateRequired:function(spFormField){
							spForms.validateCommonFieldRequired(spFormField);
						},
						getValue:function(spFormField){
							if(document.getElementById(spFormField.internalName).value != null && document.getElementById(spFormField.internalName).value != ""){
								var date = moment(document.getElementById(spFormField.internalName).value.trim(),'DD/MM/YYYY hh:mm A').toDate();
								spFormField.value = date.toISOString();
							}
							else{
								spFormField.value = null;
							}
						},
						getSPValueField:function(spFormField){
							spForms.getValueFieldCommon(spFormField);
						},
						setSPFormValue:function(spFormField){
							var ctrl = document.getElementById(spFormField.internalName);
							var strDate = moment(spFormField.value).utc('-0500').format("DD/MM/YYYY");
							if(ctrl.getAttribute('sp-dateOnly') == "false"){
								strDate = moment(spFormField.value).utc('-0500').format("DD/MM/YYYY hh:mm A");
							}
							ctrl.value = strDate;
						}
		    },
		    "lookup":{
		            name:"Lookup",
		            html:'<div class="form-group row" sp-static-name="{0}" sp-hidden="{6}">' +
						    '<label class="col-sm-4 control-label">{1}{5}</label>' +
						    '<div class="col-sm-8">' +
						        '<div class="form-control-help">' +
						            '<select id="{0}" class="form-control input-lg" sp-required="{4}" sp-type="lookup">' +
						            '{3}' +
						            '</select>' +
						            '<a class="glyphicon glyphicon-question-sign ico-help" id="ico-help-{0}" aria-hidden="true" onclick="javascript:spForms.showHelpDescription(\'{0}\',this)"></a>' +
						        '</div>' +
						        '<span id="help-block-{0}" class="help-block forms-hide-element">{2}</span>' +
						        "<span id='help-block-error-message-{0}' class='help-block forms-hide-element error-message' id='{0}-error-message'></span>" +
						    '</div>' +
						'</div>',
					htmlOption:"<option id='{0}-{1}' value='{0}'>{2}</option>",
		            load:function(spField,order,callback){
		            	var field = spField;
		            	var formatControl =spForms.parameters.fieldType.lookup.htmlOption;
						var lookupFieldName = field.LookupField;
						var InternalName = field.InternalName;
						var listId = field.LookupList.replaceAll('{','').replaceAll('}','');
						sputilities.getItemsByListId(listId,"",function(data){
							var data = utilities.convertToJSON(data);
							if(data != null){
								var items = data.d.results;
								var content = "";
								for(var j=0; j < items.length; j++){
									var item = items[j];
									var text = item[lookupFieldName];
									if(text == null || text == ""){
										text = item[String.format("OData_{0}",lookupFieldName)]
									}
									if(text != ""){
										content += String.format(formatControl,item.Id,InternalName,text);
									}
								}
								callback({
									required:field.Required,
									displayName: field.Title,
									internalName:field.InternalName,
									id:field.Id,
									type:field.TypeAsString,	
									row: String.format(spForms.parameters.fieldType.lookup.html,field.InternalName,field.Title, field.Description,content,field.Required,field.Required ? spForms.parameters.formats.requiredField: "",field.Hidden),
									order:order,
									value:null
								},field.InternalName);
							}
						},function(xhr){
							console.log(xhr);
							callback("Not Supported",field.InternalName);
						});
		            },
					validateRequired:function(spFormField){
						//spForms.validateUserFieldRequired(spFormField);
					},
					getValue:function(spFormField){
						var row =  document.querySelector(String.format('[sp-static-name="{0}"]',spFormField.internalName));
						var select = row.querySelector('select');
						if(select != null){
							spFormField.value = select.options[select.selectedIndex].value;
						}
					},
					getSPValueField:function(spFormField){
						spForms.getValueFieldLookup(spFormField);
					},
					setSPFormValue:function(spFormField){
						var option = document.querySelector(String.format('[sp-static-name="{0}"] option[value="{1}"]',spFormField.internalName,spFormField.value));
						var select = document.getElementById(spFormField.internalName);
						if(select != null && option != null){
							select.selectedIndex = option.index;
						}
					}

		    },				    
		    "lookupmulti":{
		            name:"lookupMulti",
		            html:'<div class="row multiselect-form" sp-static-name="{0}" sp-required="{4}" sp-hidden="{6}">' +
				            '<h4>{1}{5}</h4>'+
						    '<div class="col-xs-5">' +
						        '<select name="{0}_from" id="{0}" sp-type="multilookup" class="form-control" size="8" multiple="multiple">' +
						        '{3}' +
						        '</select>' +
						    '</div>' +
						    '<div class="col-xs-2">' +
						        '<button type="button" id="{0}_rightAll" class="btn btn-form"><i class="glyphicon glyphicon-forward"></i></button>' +
						        '<button type="button" id="{0}_rightSelected" class="btn btn-form"><i class="glyphicon glyphicon-chevron-right"></i></button>' +
						        '<button type="button" id="{0}_leftSelected" class="btn btn-form"><i class="glyphicon glyphicon-chevron-left"></i></button>' +
						        '<button type="button" id="{0}_leftAll" class="btn btn-form"><i class="glyphicon glyphicon-backward"></i></button>' +
						        '<button type="button" class="btn btn-form"><i class="glyphicon glyphicon-question-sign" onclick="javascript:spForms.showHelpDescription(\'{0}\',this)"></i></button>' +
						    '</div>' +
						    '<div class="col-xs-5">' +
						        '<select name="{0}_to" id="{0}_to" class="form-control" size="8" multiple="multiple" sp-required="{4}" sp-type="multilookup-result"></select>' +
						    '</div>' +
						    '<span id="help-block-{0}" class="help-block forms-hide-element">{2}</span>' +
						    "<span id='help-block-error-message-{0}' class='help-block forms-hide-element error-message' id='{0}-error-message'></span>" +
						'</div>',
					htmlOption:"<option id='{0}-{1}' value='{0}'>{2}</option>",
		            load:function(spField,order,callback){
		            	var field = spField;
		            	var formatControl =spForms.parameters.fieldType.lookup.htmlOption;
						var lookupFieldName = field.LookupField;
						var InternalName = field.InternalName;
						var listId = field.LookupList.replaceAll('{','').replaceAll('}','');
						sputilities.getItemsByListId(listId,"",function(data){
							var data = utilities.convertToJSON(data);
							if(data != null){
								var items = data.d.results;
								var content = "";
								for(var j=0; j < items.length; j++){
									var item = items[j];
									var text = item[lookupFieldName];
									if(text == null || text == ""){
										text = item[String.format("OData_{0}",lookupFieldName)]
									}
									content += String.format(formatControl,item.Id,InternalName,text);
								}
								callback({
									required:field.Required,
									displayName: field.Title,
									internalName:field.InternalName,
									id:field.Id,
									type:field.TypeAsString,	
									row: String.format(spForms.parameters.fieldType.lookupmulti.html,field.InternalName,field.Title, field.Description,content,field.Required,field.Required ? spForms.parameters.formats.requiredField: "",field.Hidden),
									order:order,
									value:null
								},field.InternalName);
							}
						},function(xhr){
							console.log(xhr);
							callback("Not Supported",field.InternalName);
						});
					},
					validateRequired:function(spFormField){
						spForms.validateMultiLookupFieldRequired(spFormField);
					},
					getValue:function(spFormField){
						var select = document.querySelector(String.format('select#{0}_to',spFormField.internalName));
						if(select != null){
							var results = [];
							for(var i =0; i < select.options.length;i++){
								results.push(select.options[i].value);
							}
							spFormField.value = results;
						}

					},
					getSPValueField:function(spFormField){
						spForms.getValueFieldCommon(spFormField);
					},
					setSPFormValue:function(spFormField){
						var row = document.querySelector(String.format('[sp-static-name="{0}"]',spFormField.internalName));
						var select = row.querySelector(String.format('[name="{0}_from"]',spFormField.internalName));
						var selectTo = row.querySelector(String.format('[name="{0}_to"]',spFormField.internalName));
						if(spFormField.value != null){
							for(var i=0; i < spFormField.value.results.length;i++){
								var val = spFormField.value.results[i];
								var option = select.querySelector(String.format('option[value="{0}"]',val));
								if(option != null){
									selectTo.options.add(option);
								}
							}
						}
					}
		    },

		    "boolean":{
		            name:"Boolean",
		            html:'<div class="form-group row form-checkbox" sp-static-name="{0}" sp-hidden="{4}">' +
						    '<label class="col-sm-4 control-label">{1}{3}</label>' +
						    '<div class="col-sm-8">' +
						    	'<div class="form-control-help">' +
                                	'<div class="list-checkbox-radio"> ' +
						        		'<div class="checkbox">' +
										    '<label>' +
										        '<input type="checkbox" sp-type="boolean" sp-title="{1}" id="{0}" name="{1}"/>' +
										        '{1}' +
										    '</label>' +
										'</div>'+
						    		'</div>' +
						    		'<a class="glyphicon glyphicon-question-sign ico-help" id="ico-help-{0}" aria-hidden="true" onclick="javascript:spForms.showHelpDescription(\'{0}\',this)"></a>' + 
						    	'</div>' +
								"<span id='help-block-{0}' class='help-block forms-hide-element'>{2}</span>" +
					        	"<span id='help-block-error-message-{0}' class='help-block error-message forms-hide-element'></span>" +
							'</div>' +
						'</div>',
		            load:function(spField,order,callback){
		            	var field = spField;
						return {
							required:field.Required,
							displayName: field.Title,
							internalName:field.InternalName,
							id:field.Id,
							type:field.TypeAsString,	
							row: String.format(spForms.parameters.fieldType.boolean.html,field.InternalName, field.Title,field.Description,field.Required ? spForms.parameters.formats.requiredField: "",field.Hidden),
							order:order,
							value:null
						}

		            },
					validateRequired:function(spFormField){
						//spForms.validateCommonFieldRequired(spFormField);
					},
					getValue:function(spFormField){
						spFormField.value = document.getElementById(spFormField.internalName).checked;
					},
					getSPValueField:function(spFormField){
						spForms.getValueFieldCommon(spFormField);
					},
					setSPFormValue:function(spFormField){
						document.getElementById(spFormField.internalName).checked = spFormField.value;
					}

		    },
		    "user":{
		            name:"User",
		            html:	"<div class='form-group row' sp-static-name='{0}' sp-hidden='{5}'>" +
							    "<label class='col-sm-4 control-label' for='{0}' id='label-{0}'>{1}{4}</label>" +
							    "<div class='col-sm-8'>" +
							        "<div class='form-control-help'>" +
							            "<div sp-type='user' id='{0}' sp-title='{1}' placeholder='{1}' sp-required='{3}'></div>" +
							            "<a class='glyphicon glyphicon-question-sign ico-help' id='ico-help-{0}' onclick='javascript:spForms.showHelpDescription(\"{0}\",this)'aria-hidden='true'></a>" +
							        "</div>" +
							        "<span id='help-block-{0}' class='help-block forms-hide-element'>{2}</span>" +
							        "<span id='help-block-error-message-{0}' class='help-block forms-hide-element error-message' id='{0}-error-message'></span>" +
							    "</div>" +
							"</div>",
		            load:function(spField,order,callback){
		            	var field = spField;
		            	return {
							required:field.Required,
							displayName: field.Title,
							internalName:field.InternalName,
							id:field.Id,
							type:field.TypeAsString,	
							row: String.format(spForms.parameters.fieldType.user.html,field.InternalName,field.Title,field.Description, field.Required,field.Required ? spForms.parameters.formats.requiredField: "",field.Hidden),
							order:order,
							value:null
						}
		            },
		            validateRequired:function(spFormField){
						spForms.validateUserFieldRequired(spFormField);
					},
					getValue:function(spFormField){
						spFormField.value = Number(spForms.getValueUserField(spFormField).join(''));
					},
					getSPValueField:function(spFormField){
						spForms.getValueFieldUser(spFormField);
					},
					setSPFormValue:function(spFormField){
						var row = document.querySelector(String.format("[sp-static-name='{0}']",spFormField.internalName));
						if(row != null){
							var valusersLoaded = setInterval(function(){
								if(spForms.siteUsers != null){
									var user = spForms.finduserInSiteUsersById(spFormField.value);
									if(user != null){
										var loginName = user.LoginName;
										var peoplePicker = SPClientPeoplePicker.SPClientPeoplePickerDict[String.format('{0}_TopSpan',spFormField.internalName)];
										
										if(peoplePicker != null){
											if(document.getElementById(peoplePicker.TopLevelElementId) != null){
												peoplePicker.AddUserKeys(loginName);
												peoplePicker.ResolveAllUsers();
											}
										}
									}
									spForms.hideLoader();
									clearInterval(valusersLoaded);
								}
							},500);
							spForms.showLoader();
						}
					}

		    },
		    "usermulti":{
		            name:"UserMulti",
		            html:"<div class='form-group row' sp-static-name='{0}' sp-hidden='{5}'>" +
							    "<label class='col-sm-4 control-label' for='{0}' id='label-{0}'>{1}{4}</label>" +
							    "<div class='col-sm-8'>" +
							        "<div class='form-control-help'>" +
							            "<div sp-type='usermulti' id='{0}' sp-title='{1}' placeholder='{1}' sp-required='{3}'></div>" +
							            "<a class='glyphicon glyphicon-question-sign ico-help' id='ico-help-{0}' onclick='javascript:spForms.showHelpDescription(\"{0}\",this)'aria-hidden='true'></a>" +
							        "</div>" +
							        "<span id='help-block-{0}' class='help-block forms-hide-element'>{2}</span>" +
							        "<span id='help-block-error-message-{0}' class='help-block forms-hide-element error-message' id='{0}-error-message'></span>" +
							    "</div>" +
							"</div>",
		            load:function(spField,order,callback){
		            	var field = spField;
		            	return {
							required:field.Required,
							displayName: field.Title,
							internalName:field.InternalName,
							id:field.Id,
							type:field.TypeAsString,	
							row: String.format(spForms.parameters.fieldType.usermulti.html,field.InternalName,field.Title,field.Description, field.Required,field.Required ? spForms.parameters.formats.requiredField: "",field.Hidden),
							order:order,
							value:null
						}
		            },
					validateRequired:function(spFormField){
						spForms.validateUserFieldRequired(spFormField);
					},
					getValue:function(spFormField){
						spFormField.value=  {
	                 		"results": spForms.getValueUserField(spFormField)
	                 	};  
					},
					getSPValueField:function(spFormField){
						spForms.getValueFieldUser(spFormField);
					},
					setSPFormValue:function(spFormField){
						if(spFormField.value != null){
							var row = document.querySelector(String.format("[sp-static-name='{0}']",spFormField.internalName));
							if(row != null){
								var valusersLoaded = setInterval(function(){
									if(spForms.siteUsers != null){
										var users = [];
										for(var i = 0; i < spFormField.value.results.length;i++){
											var objUser = spFormField.value.results[i];
											var user = spForms.finduserInSiteUsersById(objUser);
											if(user != null){
												var loginName = user.LoginName;
												users.push(loginName);
											}
										}
										var peoplePicker = SPClientPeoplePicker.SPClientPeoplePickerDict[String.format('{0}_TopSpan',spFormField.internalName)];
										peoplePicker.AddUserKeys(users.join(';'));
										peoplePicker.ResolveAllUsers();
										spForms.hideLoader();
										clearInterval(valusersLoaded);
									}
								},500);
								spForms.showLoader();
							}
						}
					}

		    },

		    "url":{
	            name:"URL",
	            html:"<div class='ms-formlabel'>" +
	            		"<label for='{0}' id='label-{0}'>{1}</label>" +
	            	"</div>" + 
	            	"<div class='ms-formbody'>" + 
	            		"<input type='text' sp-type='url' id='{0}-text' sp-title='{1}-text' placeholder='texto para mostrar' sp-required='{3}' autocomplete='off'/>" +
	            		"<input type='text' sp-type='url' id='{0}-url' sp-title='{1}-url' placeholder='{2}' sp-required='{3}' autocomplete='off' />" +
	            	"</div>",
	            load:function(spField,order,callback){
	            	return "Not Supported";
	            	var field = spField;
	            	var title = field.Required ? String.format(spForms.parameters.formats.requiredField,field.Title) : field.Title;
	            	return {
						required:field.Required,
						displayName: field.Title,
						internalName:field.InternalName,
						id:field.Id,
						type:field.TypeAsString,	
						row: String.format(spForms.parameters.fieldType.url.html,field.InternalName,title,field.Description, field.Required),
						order:order,
						value:null
					}
	            },
				validateRequired:function(spFormField){
					//Forms.validateCommonFieldRequired(spFormField);
				},
				getValue:function(spFormField){
					//spFormField.value = document.getElementById(spFormField.internalName);
				},
				getSPValueField:function(spFormField){
					spForms.getValueFieldCommon(spFormField);
				},
				setSPFormValue:function(spFormField){
				}
		    },
		    "outcomechoice":{
		            name:"outcomeChoice",
		            html:"",
		            load:function(spField,order,callback){
		            	var field = spField;
		            	return "Not Supported";
		            }

		    },
		    "taxonomyfieldtype":{
	            name:"TaxonomyFieldType",
	            html:"<div class='form-group row' sp-static-name='{0}' sp-hidden='{7}'>" +
					    "<label class='col-sm-4 control-label' for='{0}' id='label-{0}'>{1}{6}</label>" +
					    "<div class='col-sm-8'>" +
					        "<div class='form-control-help'>" +
					        	"<div class='form-treeview' sp-type='taxonomyfieldtype' id='{0}' sp-title='{1}' sp-required='{3}' sp-allow-multiple='{4}'>{5}</div>" +
					            "<a class='glyphicon glyphicon-question-sign ico-help' id='ico-help-{0}' onclick='javascript:spForms.showHelpDescription(\"{0}\",this)'aria-hidden='true'></a>" +
					        "</div>" +
					        "<span id='help-block-{0}' class='help-block forms-hide-element'>{2}</span>" +
					        "<span id='help-block-error-message-{0}' class='help-block forms-hide-element error-message'></span>" +
					    "</div>" +
					"</div>",
	            htmlTerm:'<li>' +
						    '<input type="checkbox" id="{0}"/>' +
						    '<label>' +
						        '<input type="checkbox" sp-title="{4}" sp-id="{0}" sp-nameTerm="{1}" onchange="javascript:spForms.parameters.fieldType.taxonomyfieldtype.selectTerm(this,\'{3}\')" />' +
						        '<span></span>' +
						    '</label>' +
						    '<label for="{0}">{1}</label>' +
						    '{2}' +
						'</li>',
				selectTerm:function(ctrl,idRow){
					var mterm = String.format('{1}|{0}',ctrl.getAttribute('sp-id'),ctrl.getAttribute('sp-nameTerm'));
					var fieldText = document.querySelector(String.format('[sp-title="{0}_0"]',ctrl.getAttribute('sp-title')));
					fieldText = fieldText == null ? document.querySelector(String.format('[sp-title="{0}_0"]',idRow)): fieldText;
					if(fieldText != null){
						var text = fieldText.value;
					}
					
					var li = ctrl.parentNode.parentNode;
		            li.querySelector('input:first-child').click();
		            var ul = li.querySelector('ul')
		          	if(ctrl.checked){
		  		 		var row = document.getElementById(idRow);
		  		 		if(fieldText.value != null&& fieldText.value != ""){
		  		 			fieldText.value += String.format(';{0}',mterm);
		  		 		}else{
		  		 			fieldText.value = mterm;
		  		 		}
		  		 		if(row.getAttribute('sp-allow-multiple') == "false"){
		  		 			fieldText.value = mterm;
				    		var check = row.querySelector(String.format('label input[type="checkbox"]:not([sp-id="{0}"]):checked',ctrl.getAttribute('sp-id')));
				    		if(check != null){
					       		check.checked = false;
					       		var auxUl = check.parentNode.querySelector('ul');
					       		
					       		if(auxUl != null){
					       			 auxUl.style.display = "none";
					       		}
					       	}
					    }
			            if(ul != null){
			                ul.style.display = "none";
			           	}
			      	}
			      	else{
			      		fieldText.value = text.replaceAll(mterm,'');
			        	if(ul != null){
			        	   	ul.style.display = "block";
			        	}
		            }
				},
				getHtmlTerms:function(terms,field){
					var strTerms = "";
			    	for(var i =0; i < terms.length;i++){
			    		var term = terms[i];
			    		strTerms += String.format(spForms.parameters.fieldType.taxonomyfieldtype.htmlTerm,term.term.get_id().toString(),term.term.get_name(),spForms.parameters.fieldType.taxonomyfieldtype.getHtmlTerms(term.children,field),field.InternalName, field.Title);
			    	}
			    	return strTerms == "" ? strTerms: String.format('<ul>{0}</ul>',strTerms);
			    },
			    getTermSetAsTree : function (spTermSetId, callback) {
			       	sputilities.getTermsInTermSet({
						termSetId:spTermSetId ,
						success:function (terms) {
			            	var termsEnumerator = terms.getEnumerator(),
			                tree = {
			                    term: terms,
			                    children: []
			                };
				            while (termsEnumerator.moveNext()) {
				                var currentTerm = termsEnumerator.get_current();
				                var currentTermPath = currentTerm.get_pathOfTerm().split(';');
				                var children = tree.children;
				                for (var i = 0; i < currentTermPath.length; i++) {
				                    var foundNode = false;		 
				                    for (var j = 0; j < children.length; j++) {
				                        if (children[j].name === currentTermPath[i]) {
				                            foundNode = true;
				                            break;
				                        }
				                    }
				                    var term = foundNode ? children[j] : { name: currentTermPath[i], children: [] }; 
				                    if (i === currentTermPath.length - 1) {
				                        term.term = currentTerm;
				                        term.title = currentTerm.get_name();
				                        term.guid = currentTerm.get_id().toString();
				                    }
				                    if (foundNode) {
				                        children = term.children;
				                    }
				                    else {
				                        children.push(term);
				                        if (i !== currentTermPath.length - 1) {
				                            children = term.children;
				                        }
				                    }
				                }
				            } 
				            tree = spForms.parameters.fieldType.taxonomyfieldtype.sortTermsFromTree(tree); 
				            callback(tree);
			        	},
			        	error: function(xhr){
			        		console.log(xhr);
			        	}
			        });
			    },
				sortTermsFromTree : function (tree) {
			    	if (tree.children.length && tree.term.get_customSortOrder) {
			    	    var sortOrder = null;
			            if (tree.term.get_customSortOrder()) {
			                sortOrder = tree.term.get_customSortOrder();
			            }
			            if (sortOrder) {
				            sortOrder = sortOrder.split(':');
			                tree.children.sort(function (a, b) {
			                    var indexA = sortOrder.indexOf(a.guid);
			                    var indexB = sortOrder.indexOf(b.guid);
			 
			                    if (indexA > indexB) {
			                        return 1;
			                    } else if (indexA < indexB) {
			                        return -1;
			                    }
			 
			                    return 0;
			                });
			            }
			            else {
			                tree.children.sort(function (a, b) {
			                    if (a.title > b.title) {
			                        return 1;
			                    } else if (a.title < b.title) {
			                        return -1;
			                    }
			 
			                    return 0;
			                });
			            }
			        }
			 
			        for (var i = 0; i < tree.children.length; i++) {
			            tree.children[i] = spForms.parameters.fieldType.taxonomyfieldtype.sortTermsFromTree(tree.children[i]);
			        }
			 
			        return tree;
			    },
	            load:function(spField,order,callback){
	            	var field = spField;
					var spTermSetId = field.TermSetId;
					spForms.parameters.fieldType.taxonomyfieldtype.getTermSetAsTree(spTermSetId,function(tree){
						 var strTree = spForms.parameters.fieldType.taxonomyfieldtype.getHtmlTerms(tree.children,field);
						 callback({
						 	required:field.Required,
							displayName: field.Title,
							internalName:field.InternalName,
							id:field.Id,
							type:field.TypeAsString,	
							row: String.format(spForms.parameters.fieldType.taxonomyfieldtype.html,field.InternalName,field.Title,field.Description, field.Required,field.AllowMultipleValues,strTree,field.Required ? spForms.parameters.formats.requiredField: "",field.Hidden),
							order:order,value:null
						 },field.InternalName);
					});
	            },
				validateRequired:function(spFormField){
					spForms.validateTaxonomyFieldRequired(spFormField);
				},
				getValue:function(spFormField){
					var row =  document.querySelector(String.format('[sp-static-name="{0}"]',spFormField.internalName));						
					var check = row.querySelector('label input[type="checkbox"]:checked');
					if( check != null){
						spFormField.value ={
							id:check.getAttribute('sp-id'),
							label:check.getAttribute('sp-nameTerm')
						};
					}
				},
				getSPValueField:function(spFormField){
					spForms.getValueFieldCommon(spFormField);
					var obj = {
						'__metadata' : {'type': 'SP.Taxonomy.TaxonomyFieldValue'},
						'Label': spFormField.value.label,
						'TermGuid':spFormField.value.id, 
						'WssId': -1
					}					
					spForms.dataToSave[spFormField.internalName] = obj;
				},
				setSPFormValue:function(spFormField){
					if(spFormField.value != null){
						var row = document.querySelector(String.format('[sp-static-name="{0}"]',spFormField.internalName));
						var term = row.querySelector(String.format('label input[sp-id="{0}"]',spFormField.value.TermGuid));
						term.click(term,spFormField.internalName);
					}
				}


		    },
		    "taxonomyfieldtypemulti":{
		    	name:"TaxonomyFieldTypeMulti",
		    	load:function(spField,order,callback){
	            	var field = spField;
					var spTermSetId = field.TermSetId;
					spForms.parameters.fieldType.taxonomyfieldtype.getTermSetAsTree(spTermSetId,function(tree){
						 var strTree = spForms.parameters.fieldType.taxonomyfieldtype.getHtmlTerms(tree.children,field);
						 callback({
						 	required:field.Required,
							displayName: field.Title,
							internalName:field.InternalName,
							id:field.Id,
							type:field.TypeAsString,	
							row: String.format(spForms.parameters.fieldType.taxonomyfieldtype.html,field.InternalName,field.Title,field.Description, field.Required,field.AllowMultipleValues,strTree,field.Required ? spForms.parameters.formats.requiredField: "",field.Hidden),
							order:order,value:null
						 },field.InternalName);
					});
	           	},
				validateRequired:function(spFormField){
					spForms.validateTaxonomyFieldRequired(spFormField);
				},
				getValue:function(spFormField){
					/*var row =  document.querySelector(String.format('[sp-static-name="{0}"]',spFormField.internalName));						
					var checks = row.querySelectorAll('label input[type="checkbox"]:checked');
					var values =[];
					for(var i = 0; i < checks.length;i++){
						var check = checks[i];
						values.push(check.value);
					}*/
				},
				getSPValueField:function(spFormField){
					//spForms.getValueFieldCommon(spFormField);
				},
				setSPFormValue:function(spFormField){
					var row = document.querySelector(String.format('[sp-static-name="{0}"]',spFormField.internalName));
					if(spFormField.value != null){
						for(var i = 0; i < spFormField.value.results.length ; i++){
							var spTerm  = spFormField.value.results[i];
							var term = row.querySelector(String.format('label input[sp-id="{0}"]',spTerm.TermGuid));
							if(term != null){
								term.click(term,spFormField.internalName)
							}
						}
					}
				}
		    },
		    "computed":{
	            name:"Computed",
	            html:"",
	            load:function(spField,order,callback){
	            	var field = spField;
	            	return "Not Supported";
	            }
		    }
		}
	},
	currentItemID:utilities.getQueryString('ID'),
	siteUsers:null,
	loadedFields:null,
	dataToSave:{},
	currentItem:null,
	cancelForm:function(callback){
		var source = utilities.getQueryString('Source');
		if(source != null){
			window.location.href = source;
		}else{
			window.location.href = sputilities.contextInfo().listUrl;
		}
	},
	showLoader:function(){
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
	},
	hideLoader:function(){
		document.querySelector('.loader-container').style.opacity= 0;
		document.querySelector('.form-content').style.opacity= 1;
		document.querySelector('.loader-container').style.height= 0;
	},
	updateForm:function(success,error,preSave){
		if(typeof preSave == "function"){
			if(!preSave()){
				return false;
			}
		}
		
		spForms.validateAllFieldsRequired();
		if(!spForms.validateAllFieldInvalid()){
			return false;
		}
			
		spForms.dataToSave = {
			__metadata: { 'type': spForms.listInfo.entityItem  }, 
		}
		
		for(var nameField in spForms.loadedFields){
			var spFormField = spForms.loadedFields[nameField];
			if((spFormField != null) && (typeof spFormField == "object")){
				spForms.parameters.fieldType[spFormField.type.toLowerCase()].getSPValueField(spFormField);
			}
		}	
		document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
		sputilities.updateItem(spForms.listInfo.title,spForms.dataToSave,spForms.currentItemID,function(data){
			if(spForms.options.allowAttachments){
				spForm_attachment.deleteFiles(
					function(){
						spForm_attachment.currentDeletationComplete = true;
					},function(xhr){
						console.log(xhr);
					}
				);
				
				var validateDeletationComplete = setInterval(function(){
					if(spForm_attachment.currentDeletationComplete != null){
						spForm_attachment.addNewFiles(
							function(){
								document.querySelector('.loader-container').style.opacity= 0;
								document.querySelector('.form-content').style.opacity= 1;
								document.querySelector('.loader-container').style.height= 0;
								clearInterval(validateDeletationComplete)
								if(typeof success == "function"){
									if(!success()){
										return false;
									};
								}
								spForms.cancelForm(null);	
							},function(xhr){
								console.log(xhr);
							}
						);
					}
				},500);
			}
			else{
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;
				if(typeof success == "function"){
					if(!success()){
						return false;
					};
				}
				spForms.cancelForm(null);	
			}			
		},
		function(xhr){
			console.log(xhr);
			if(typeof error == "function"){
				error();
			}
		}
		);
		return false;

	},
	getSiteUsers:function(){
		sputilities.getSiteUsers(String.empty,
			function(data){
				data = utilities.convertToJSON(data);
				spForms.siteUsers = data.d.results;
			},
			function(xhr){
				console.log(xhr);
			}
		);
	},
	saveForm:function(success,error,preSave){
	console.log(success)
	
		if(typeof preSave == "function"){
			if(!preSave()){
				return false;
			}
		}
		
		spForms.validateAllFieldsRequired();
		if(!spForms.validateAllFieldInvalid()){
			return false;
		}
			
		spForms.dataToSave = {
			__metadata: { 'type': spForms.listInfo.entityItem  }, 
		}
		
		for(var nameField in spForms.loadedFields){
			var spFormField = spForms.loadedFields[nameField];
			if((spFormField != null) && (typeof spFormField == "object")){
				spForms.parameters.fieldType[spFormField.type.toLowerCase()].getSPValueField(spFormField);
			}
		}	
    	document.getElementById('s4-workspace').scrollTop = 0; 
		document.querySelector('.form-content').style.opacity = 0;
		document.querySelector('.loader-container').style.opacity= 1;
		document.querySelector('.loader-container').style.height= "auto";
		sputilities.addItem(spForms.listInfo.title,spForms.dataToSave,function(data){
			var data = utilities.convertToJSON(data);
			spForms.currentItemID = data != null ?  data.d.ID : null;
			if(spForms.options.allowAttachments){
				if(data != null){
					spForm_attachment.addNewFiles(
						function(){
							document.querySelector('.loader-container').style.opacity= 0;
							document.querySelector('.form-content').style.opacity= 1;
							document.querySelector('.loader-container').style.height= 0;
							if(typeof success == "function"){
								if(!success()){
									return false;
								};
							}
							spForms.cancelForm(null);	
						},function(xhr){
							console.log(xhr);
						}
					);
				}
			}
			else{
				document.querySelector('.loader-container').style.opacity= 0;
				document.querySelector('.form-content').style.opacity= 1;
				document.querySelector('.loader-container').style.height= 0;
				if(typeof success == "function"){
					if(!success()){
						return false;
					};
				}
				spForms.cancelForm(null);	
			}
						
		},
		function(xhr){
			console.log(xhr);
			if(typeof error == "function"){
				error();
			}
		}
		);
		return false;

	},
	getValueUserField:function(spFormField){
		var users = SPClientPeoplePicker.SPClientPeoplePickerDict[String.format('{0}_TopSpan',spFormField.internalName)].GetAllUserInfo();
		var objUsers = [];
		for(var i = 0; i < users.length; i++){
			var user = users[i];
			var objUser = spForms.findUserInSiteUsers(user.EntityData.Email);
			objUsers.push(Number(objUser.Id))
		}
		return objUsers;
	},
	getDateTimeFormatNumbers:function(number){
		if(number < 10){
			return String.format('0{0}',number.toString()); 	
		}
		return number.toString();
	},
	getTimeNonMilitaryFormat:function(date){
		var hour = date.getHours();
		var am_pm = "AM";
		if(hour > 12){
			hour = hour - 12;
			am_pm = "PM";
		}
		else if(hour == 0){
			hour = 12;
		}
		return String.format('{0}:{1} {2}',spForms.getDateTimeFormatNumbers(hour),spForms.getDateTimeFormatNumbers(date.getMinutes()),am_pm);
	},
	getValueFieldMultipleSelection:function(spFormField){
		var valorFinal = {
			"__metadata":{"type":"Collection(Edm.String)"},
		   	"results":spFormField.value
		} 
		spForms.dataToSave[spFormField.internalName] = valorFinal;
	},
 	getValueFieldCommon:function(spFormField){
		spForms.dataToSave[spFormField.internalName] = spFormField.value;
	},
	getValueFieldUser:function(spFormField){
		spForms.dataToSave[String.format('{0}Id',spFormField.internalName)] = spFormField.value;
	},
	getValueFieldLookup:function(spFormField){
		spForms.dataToSave[String.format('{0}Id',spFormField.internalName)] = spFormField.value;
	},
	validateMinMaxNumericFields:function(event){
		if(Number(this.value) < Number(this.getAttribute('sp-minnum'))){
			document.querySelector(String.format('[sp-static-name="{0}"]',this.id)).setAttribute('sp-invalid',spForms.parameters.formats.minNum);
		}
		else if(Number(this.value) > Number(this.getAttribute('sp-maxnum'))){
			document.querySelector(String.format('[sp-static-name="{0}"]',this.id)).setAttribute('sp-invalid',spForms.parameters.formats.maxNum);
		}
		else{
			document.querySelector(String.format('[sp-static-name="{0}"]',this.id)).removeAttribute('sp-invalid');
		}
	},
	validateNumericFields:function(event){
		if(event.keyCode == 8 && this.validity.badInput){
			this.removeAttribute('has-neg');
		}
		
		var decimals = spForms.loadedFields[this.id].decimals;
		if(event.charCode == 45){
			if(this.value.length < 1 && this.getAttribute('has-neg') != "true"){
				this.setAttribute('has-neg',true);
			}else{
				event.preventDefault();			
				return false;
			}
		}		
		var valid = (this.value.indexOf('-') == -1 && event.charCode == 45 && this.value.length < 1) || event.keyCode == 8 || (event.charCode  >= 48 && event.charCode <= 57);
		if(decimals != 0){
			valid = (this.value.indexOf(',') == -1 && event.charCode == 44) || (this.value.indexOf('-') == -1 && event.charCode == 45 && this.value.length < 1) || event.keyCode == 46 || event.keyCode == 8 || (event.charCode  >= 48 && event.charCode <= 57 && ( this.value.split(',').length > 1 ? this.value.split(',')[1].length < decimals : true));
		}
		if(valid){
			return true;
		}	
		console.log(event);	
		event.preventDefault();			
		return false;
	},
	validateAllFieldInvalid:function(){
		var rows = document.querySelectorAll('.row');
		var markError= false;
		var hasError = false;
		for(var i =0; i < rows.length;i++){
			var row = rows[i];
			var message = row.getAttribute('sp-invalid');
			if(message != null){
				row.classList.add('has-error');
				row.querySelector('.error-message').innerHTML = message;
				hasError  = true;
				if(!markError){
					var input = row.querySelector('input');
					if(input != null){
						input.focus();
						markError = true;
					}
				}
			}
			else{
				row.classList.remove('has-error');
				if(row.querySelector('.error-message') != null){
					row.querySelector('.error-message').innerHTML = "";
				}
			}
		}
		return !hasError;
	},
	validateAllFieldsRequired:function(){
		if(spForms.options.allowAttachments){
			spForm_attachment.validate();
		}
		var spFormFields = spForms.loadedFields;
		for(var nameField in spFormFields){
			var spFormField = spFormFields[nameField];
			if((spFormField != null) && (typeof spFormField == "object")){
				var objField = spForms.parameters.fieldType[spFormField.type.toLowerCase()];
				if(objField != null){
					objField.getValue(spFormField);
					if(spFormField.required){
						objField.validateRequired(spFormField);
					}
				}
			}
		}
	},
	validateTaxonomyFieldRequired:function(spFormField){
		var row = document.querySelector(String.format('div[sp-static-name="{0}"]',spFormField.internalName));
		if(row != null){
			var ctrls = row.querySelectorAll('label input:checked');
			if(ctrls.length == 0){
				row.setAttribute('sp-invalid',spForms.parameters.formats.requiredError);
			}else{
				row.removeAttribute('sp-invalid');
			}
		}
	},
	validateUserFieldRequired:function(spFormField){
		var users = SPClientPeoplePicker.SPClientPeoplePickerDict[String.format('{0}_TopSpan',spFormField.internalName)].GetAllUserInfo();
		var row = document.querySelector(String.format('div[sp-static-name="{0}"]',spFormField.internalName));			
		if(row != null && spFormField.required && users.length == 0){
			row.setAttribute('sp-invalid',spForms.parameters.formats.requiredError);
		}
		else if(row != null && spFormField.type == spForms.parameters.fieldType.user.name && users.length > 1 ){
			row.setAttribute('sp-invalid',spForms.parameters.formats.singleUserError);
		}
		else if(row != null){
			row.removeAttribute('sp-invalid');
		}
	},
	validateMultiLookupFieldRequired:function(spFormField){
		var row = document.querySelector(String.format('div[sp-static-name="{0}"]',spFormField.internalName));
		if(row != null){
			var option = row.querySelector(String.format("#{0}_to option",spFormField.internalName));
			if(option == null){
				row.setAttribute('sp-invalid',spForms.parameters.formats.requiredError);
			}else{
				row.removeAttribute('sp-invalid');
			}
		}
	},
	validateRadioChecksFieldRequired:function(spFormField){
		var row = document.querySelector(String.format('div[sp-static-name="{0}"]',spFormField.internalName));
		if(row != null){
			var ctrls = row.querySelectorAll('input:checked');
			if(ctrls.length == 0){
				row.setAttribute('sp-invalid',spForms.parameters.formats.requiredError);
			}else{
				row.removeAttribute('sp-invalid');
			}
		}
	},
	validateCommonFieldRequired:function(spFormField){
		var row = document.querySelector(String.format('div[sp-static-name="{0}"]',spFormField.internalName));
		var htmlField = row.querySelector(String.format('#{0}',spFormField.internalName));
		if(htmlField.value.trim() == ""){
			if(row != null){
				row.setAttribute('sp-invalid',spForms.parameters.formats.requiredError);
			}
		}else{
			if(row.getAttribute('sp-invalid') == spForms.parameters.formats.requiredError)
				row.removeAttribute('sp-invalid');
		}
	},
	showHelpDescription:function(InternalName,ctrl){
		var helpMessageElement= document.getElementById(String.format(spForms.parameters.formats.helpBlock,InternalName));
		if(ctrl.classList.contains(spForms.parameters.formats.icoHelpSelected)){
			ctrl.classList.remove(spForms.parameters.formats.icoHelpSelected);
			helpMessageElement.classList.add(spForms.parameters.formats.formsHideElement);
		}else{
			ctrl.classList.add(spForms.parameters.formats.icoHelpSelected);
			helpMessageElement.classList.remove(spForms.parameters.formats.formsHideElement);
		}
	},
	getActionButtons:function(options){
		var strActions = "";
		for(var i = 0; i < options.actions.length; i++){
			var action = options.actions[i];
			var strClick = String.format(action.type,action.success,action.error,action.preSave);
			strActions += String.format(spForms.parameters.formatFormButtom,action.id,action.name,strClick);
		}
		return strActions;
	},
	listInfo:sputilities.getInfoListById(sputilities.contextInfo().listId.replace('{','').replace('}',''),"",function(info){
			var listInfo = utilities.convertToJSON(info);
			if(listInfo != null){
				spForms.listInfo = {};
				spForms.listInfo.entityItem = listInfo.d.ListItemEntityTypeFullName;
				spForms.listInfo.title = listInfo.d.Title;
				spForms.listInfo.description = listInfo.d.Description;
				spForms.listInfo.staticName = listInfo.d.EntityTypeName.replace('List','');
			}
		},function(xhr){
			console.log(xhr);
	}),
	getFields:function(baseFields){
		var vecFields = new Array(Object.keys(baseFields).length + 1)
		var strFields = "";
		for(var strName in spForms.loadedFields){
			var field = spForms.loadedFields[strName];
			if(field.row != null){
				strFields += field.row;
				vecFields[field.order] = field.row;
			}
		}
		return  vecFields.join(' ');
	},
	initilizeControls:function(){
		try{
			if(window.jQuery){
				//$.noConflict()
				spForms.initializeSingleLineFields();
				spForms.initializeMultiLineFields();
				spForms.initializeNumericFields();
				spForms.initializeMultiSelectFields();
				spForms.initializeDateTimeFields();
				spForms.getSiteUsers();
			    spForms.initilizeUserFields();
		   	}
		   	if(spForms.options.allowAttachments){		   		
				spForm_attachment.parameters.required = spForms.options.requiredAttachments;
				spForm_attachment.parameters.maxlengthFiles = spForms.options.maxAttachments;
				spForm_attachment.parameters.minLengthFiles = spForms.options.minAttachments;
				spForm_attachment.parameters.maxSizeInMB = spForms.options.maxSizeInMB;
		   		spForm_attachment.initialize();
		   	}
		}
		catch(e){
			console.log(String.format("an error has ocurred in spForms.initilizeControls, please validate this error, stack Object: {0}",e));
		}
	},
	initializeSingleLineFields:function(){
		var ctrls = document.querySelectorAll('[sp-type="text"]');
		for(var i = 0; i < ctrls.length ; i++){
			var ctrl = ctrls[i];
			ctrl.addEventListener('keydown',function(event){
				if(event.keyCode == 46 || event.keyCode == 8 || (event.keyCode >= 37 && event.keyCode <= 40))
					return true;
					
				if(this.value.length >= this.getAttribute('sp-max-length')){
					event.preventDefault();
				};
			});
		}
	},
	initializeMultiLineFields:function(){
		var ctrls = document.querySelectorAll('[sp-type="note"]');
		for(var i = 0; i < ctrls.length ; i++){
			var ctrl = ctrls[i];
			ctrl.addEventListener('keydown',function(event){
				if(event.keyCode == 46 || event.keyCode == 8 || (event.keyCode >= 37 && event.keyCode <= 40))
					return true;
					
				if(this.value.split('\n').length > this.getAttribute('sp-numlines')){
					event.preventDefault();
				};
			});
		}

	},
	initializeNumericFields:function(){
		var ctrlsNum = document.querySelectorAll('[sp-type="number"],[sp-type="currency"]');		
		for(var i = 0; i < ctrlsNum.length ; i++){
			var ctrl = ctrlsNum[i];
			ctrl.addEventListener('keypress',spForms.validateNumericFields);			
			ctrl.addEventListener('change',spForms.validateMinMaxNumericFields);
		}
	},
	initializeMultiSelectFields:function(){
		var ctrls = document.querySelectorAll('select[sp-type="multilookup"]');
		if(ctrls.length > 0){
			for(var i = 0 ; i < ctrls.length; i++){
				var ctrl = ctrls[i];
				$(ctrl).multiselect();
			}
		 }
	},
	initializeDateTimeFields:function(){
		var ctrlsDate = document.querySelectorAll('input[sp-type="datetime"]');
		if(ctrlsDate.length > 0){
			moment.updateLocale("es",{
				months: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
		        monthsShort:  [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
		        weekdays: [ "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" ],
		        weekdaysMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
		        weekdaysShort: [ "Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab" ]		 
		  	});
		 	var ctrlsDate = document.querySelectorAll('input[sp-type="datetime"]');
		 	for(var i = 0; i < ctrlsDate.length ;i++){
		 		var ctrlDate = ctrlsDate[i];
		 		var format = "DD/MM/YYYY hh:mm A"
		 		if(ctrlDate.getAttribute('sp-dateOnly') == "true"){
		 			format = "DD/MM/YYYY"
		 		}
		 		$(ctrlDate).datetimepicker({
		 			format:format,
			    	locale:'es',
			    	toolbarPlacement:"bottom",
			    	showClose:true,
			    	showClear:true,
			    	tooltips: {
			    		selectDate:'Seleccione hora',
					    today: 'hoy',
					    clear: 'Borrar selección',
					    close: 'Cerrar',
					    selectMonth: 'Seleccione el mes',
					    prevMonth: 'Mes anterior',
					    nextMonth: 'Mes siguiente',
					    selectYear: 'Seleccione el año',
					    prevYear: 'Año anterior',
					    nextYear: 'Año siguiente',
					    selectDecade: 'Seleccione decada',
					    prevDecade: 'Decada anterior',
					    nextDecade: 'Decada siguiente',
					    prevCentury: 'Centenario siguiente',
					    nextCentury: 'Centenario anterior',
					    incrementHour: 'Incrementar hora',
					    pickHour: 'Seleccione hora',
					    decrementHour:'restar Hora',
					    incrementMinute: 'Incrementar Minute',
					    pickMinute: 'Seleccione minuto',
					    decrementMinute:'restar minuto',
					    incrementSecond: 'Incrementar segundos',
					    pickSecond: "Seleccionar Segundos",
					    decrementSecond:'Restar Segundos',
					}
				});
			}
		}
	},
	UserFieldResolved:function(peoplePickerId,currentInfo){
		var validUsers =[]
		var peoplePicker = SPClientPeoplePicker.SPClientPeoplePickerDict[peoplePickerId];
		var valid = true;
		for(var i = 0; i < currentInfo.length;i++){
			var obj = currentInfo[i];
			if(obj.EntityData != null){
				var user = spForms.findUserInSiteUsers(obj.EntityData.Email);
				if(user == null || validUsers.indexOf(obj.Key) != -1){
					valid = false;
				}else{
					validUsers.push(obj.Key);
				}
			}
		}
		if(!valid){
			document.getElementById(peoplePicker.HiddenInputId).value = String.empty;
			document.getElementById(peoplePicker.ResolvedListElementId).innerHTML = String.empty;
			peoplePicker.AddUserKeys(validUsers.join(';'));
			peoplePicker.ResolveAllUsers();
		}
		
	},
	findUserInSiteUsers:function(userEmail){
		for(var i = 0; i < spForms.siteUsers.length;i++){
			var siteUser = spForms.siteUsers[i];
			if(siteUser.Email.toUpperCase() == userEmail.toUpperCase()){
				return siteUser;
			}
		}
		return null;
	},
	finduserInSiteUsersById:function(userId){
		if(spForms.siteUsers != null){
			for(var i = 0; i < spForms.siteUsers.length;i++){
				var siteUser = spForms.siteUsers[i];
				if(siteUser.Id == userId){
					return siteUser;
				}
			}
		}
		return null;
	},
	initilizeUserFields:function(){
	 	var ctrlsPicker = document.querySelectorAll('div[sp-type="user"],div[sp-type="usermulti"]');
	 	for(var i = 0; i < ctrlsPicker.length; i++){
	 		var ctrlPicker = ctrlsPicker[i];
	    	sputilities.initializePeoplePicker({
				allowMultiple:ctrlPicker.getAttribute('sp-type') == "usermulti",
				placeHolder:ctrlPicker.getAttribute('sp-title'),
				userResolved:"spForms.UserFieldResolved",
				controlId:ctrlPicker.id
			});
	    }
	 },
	insertNewForm:function(options,fields){
		var formTitle = options.title != null && options.title != "" ? options.title : spForms.listInfo.title;
		var formDescription = options.description != null && options.description != "" ? options.description: spForms.listInfo.description;
		var actionButtons = spForms.getActionButtons(options);
		var strFields = spForms.getFields(fields);
		if(spForms.options.allowAttachments && spForms.options.maxAttachments != null && spForms.options.maxSizeInMB != null ){
			strFields += String.format(spForm_attachment.parameters.html,spForms.options.maxAttachments,spForms.options.maxSizeInMB);
		}
		var strForm = String.format(spForms.parameters.formatForm,formTitle,formDescription,strFields,actionButtons);
		var container =	document.getElementById(options.containerId);
		if(container != null){
			container.innerHTML = strForm;
		}
		document.getElementById('ms-designer-ribbon').style.opacity = "1";
		document.getElementById('s4-workspace').style.opacity = "1";
	},
	initLoadNewForm:function(){
		var options = spForms.options;	
		sputilities.executeDelayspTaxonomy(function(){
			sputilities.getContentTypeOfList(options.contentTypeName,sputilities.contextInfo().listTitle,"&$expand=fields&$select=*",function(data){
				var data = utilities.convertToJSON(data);
				if(data != null){
					var fields = data.d.results[0].Fields.results;					
					var spFormFields = {};
					for(var i = 0; i < fields.length ; i++){
						var field = fields[i];
						var spFormField = spForms.parameters.fieldType[field.TypeAsString.toLowerCase()].load(field,i,function(spFormField,fieldInternalName){
							if(spFormField != null){
								spFormFields[fieldInternalName] = spFormField;
							}
						});
						
						if(spFormField != null){
							spFormFields[field.InternalName] = spFormField;
						}
					}
					
					var validateFields = setInterval(function(){
						if(fields.length == Object.keys(spFormFields).length){
							spForms.loadedFields = spFormFields;
							spForms.insertNewForm(options,fields);
							spForms.initilizeControls();
							if(typeof options.successLoadNewForm == "function"){
								options.successLoadNewForm();
							}
							clearInterval(validateFields);
						}
					},500);
				}
			},
			function(err){
				console.log(err)
			});
		});
	},
	initLoadEditForm:function(){
		sputilities.getItems(sputilities.contextInfo().listTitle,String.format('({0})?$select=Author/EMail,Author/ID,*&$expand=AttachmentFiles,Author',spForms.currentItemID),function(data){
			var data = utilities.convertToJSON(data);
			if(data != null){
				var attachments =[];
				if(data.d.AttachmentFiles != null){
					var attachments = data.d.AttachmentFiles.results;
				}
				spForms.attachmentsLoadedIndex = 0;
				for(var i =0; i < attachments.length;i++){
					var attachment = attachments[i];
					var fileName = attachment.FileName;
					spForm_attachment.files[fileName] = {
			        	size: "",
			            type: "",
			            content:"",
			            url:String.format(spForm_attachment.parameters.urlFileAttachment,sputilities.contextInfo().webAbsoluteUrl,spForms.listInfo.staticName,spForms.currentItemID,fileName)
			       	}
				}
				spForms.currentItem = data.d;
			}
		}, function(xhr){
			console.log(xhr);
		});
		spForms.options.successLoadNewForm = function(){
			document.getElementById('ms-designer-ribbon').style.opacity = "0";
		document.getElementById('s4-workspace').style.opacity = "0";
			var valFields = setInterval(function(){
				if(spForms.currentItem != null){
					var item = spForms.currentItem;
					for(var strName in spForms.loadedFields){
						var spFormField = spForms.loadedFields[strName];
						if((spFormField != null) && (typeof spFormField == "object")){
							spFormField.value = item[spFormField.internalName];
							spFormField.value = spFormField.value == null ? item[String.format('{0}Id', spFormField.internalName)]: spFormField.value;
							spForms.parameters.fieldType[spFormField.type.toLowerCase()].setSPFormValue(spFormField)
						}
					}
					spForm_attachment.baseLoadedfiles =  utilities.convertToJSON(JSON.stringify(spForm_attachment.files));
					for(var fileName in spForm_attachment.files){
						var fileData = spForm_attachment.files[fileName].content;
						var strDocRow = String.format(spForm_attachment.parameters.htmlDocument,fileName,spForm_attachment.files[fileName].url)
                		document.getElementById(spForm_attachment.parameters.idBodyFilesContainer).innerHTML += strDocRow;
					}
					if(typeof spForms.options.successLoadEditForm == "function"){
						spForms.options.successLoadEditForm();
					}

					document.getElementById('ms-designer-ribbon').style.opacity = "1";
					document.getElementById('s4-workspace').style.opacity = "1";
					clearInterval(valFields);
				}
			},500);
		}
		spForms.initLoadNewForm();
	},
	loadNewForm:function(){
		sputilities.callFunctionOnLoadBody('spForms.initLoadNewForm');
	},
	loadEditForm:function(){
		sputilities.callFunctionOnLoadBody('spForms.initLoadEditForm');
	}
};

var spForm_attachment = {
    formats:{
        errorMinFiles:"No se ha cargado el numero mínimo de documentos ({0})",
        errorRequired:"Esto no puede estar en blanco"
    },
    parameters :{
    	baseLoadedfiles:null,
    	urlFileAttachment:"{0}/lists/{1}/Attachments/{2}/{3}",
        idFileCtrl:"sp-forms-attachment",
        idBtnAddFile:"sp-forms-add-attachment",
        idBodyFilesContainer:"sp-forms-attachment-container",
        required:false,
        minLengthFiles:2,
        maxlengthFiles:20,
        maxSizeCharacter:35,
        maxSizeInMB:3,
        formatAccept:[
            'xml',
            'txt',
            'csv',
            'doc',
            'docx',
            'xls',
            'xlsx',
            'ppt',
            'pptx',
            'pdf',
            'png',
            'jpg',
            'jpeg',
            'bmp',
            'gif',
            'ico',
            'tiff'
        ],
        html:'<div class="form-group row" sp-static-name="sp-forms-attachment">' +
                    '<label class="col-md-4 col-xs-8 control-label">Archivos Adjuntos</label>' +
                    '<div class="col-md-8 table-docs-items">' +
                    	'<div class="form-control-help">' +
                    		'<div id="sp-forms-attachment-container"></div>'+
                    		'<a class="glyphicon glyphicon-question-sign ico-help" id="ico-help-sp-forms-attachment" onclick="javascript:spForms.showHelpDescription(\'sp-forms-attachment\',this)" aria-hidden="true"></a>' +
                    	'</div>'+
                    	'<span id="help-block-sp-forms-attachment" class="help-block forms-hide-element">Agregue los archivos necesarios para el elemento (máximo {0}). Cada archivo con un tamaño menor o igual a {1} MB</span>' +
		        		'<span id="help-block-error-message-sp-forms-attachment" class="help-block forms-hide-element error-message" id="sp-forms-attachment-error-message"></span>' +
                    '</div>'+
                     '<div class="col-md-4 col-xs-4 button-tittle">' +
                   		'<a id="sp-forms-add-attachment" class="glyphicon glyphicon-plus" onclick="javascript:spForm_attachment.addEventToAddFile()"></a>' +
                   	'</div>' +
                    '<input type="file" id="sp-forms-attachment"/>' +
            '</div>',
        htmlDocument:'<div class="item-doc">' +
                        '<div class="col-md-6 col-xs-12 name-doc">'  +
                            '<a href="{1}" target="_blank">{0}</a>' +
                        '</div>' +
                        '<div class="col-md-6 col-xs-6 delete-doc">' +
                            '<a href="#" sp-attachment-file="{0}" onclick="spForm_attachment.deleteFile(this)" class="glyphicon glyphicon-remove"></a>' +
                        '</div>' +
                    '</div>'
    },
    files:{},
    addNewFiles:function(success,error){
    	spForm_attachment.getFilesToAdd();
    	var id = spForms.currentItemID;
		var docs = utilities.getKeysOfAnObject(spForm_attachment.filesToAdd);
		spForm_attachment.currentAddIndex = 0;
		spForm_attachment.addFileToItem(sputilities.contextInfo().listTitle,docs[spForm_attachment.currentAddIndex],id,success,error);
    },
    deleteFiles:function(success,error){
    	spForm_attachment.getFilesToDelete()
    	var id = spForms.currentItemID;
		var docs = utilities.getKeysOfAnObject(spForm_attachment.filesToDelete);
		spForm_attachment.currentDeletationIndex = 0;
		spForm_attachment.deleteFileToItem(sputilities.contextInfo().listTitle,docs[spForm_attachment.currentDeletationIndex],id,success,error);
    },
    getDocs:function(){
    	return utilities.getKeysOfAnObject(spForm_attachment.files);
    },
    getDocsBase:function(){
    	return utiltities.getKeysOfAnObject(spForm_attachment.baseLoadedfiles);
    },
    getFilesToAdd:function(){
    	var filesToAdd = {};
    	if(spForm_attachment.baseLoadedfiles == null){
    		spForm_attachment.filesToAdd  =  spForm_attachment.files;
    		return;
    	}
		for(var fileName in spForm_attachment.files){
			if(spForm_attachment.baseLoadedfiles[fileName] == null){
				filesToAdd[fileName] = spForm_attachment.files[fileName];
			}
		}
		spForm_attachment.filesToAdd = filesToAdd;
    },
    getFilesToDelete:function(){
    	var filesToDelete = {};
		for(var fileName in spForm_attachment.baseLoadedfiles){
			if(spForm_attachment.files[fileName] == null){
				filesToDelete[fileName] = spForm_attachment.baseLoadedfiles[fileName];
			}
		}
		spForm_attachment.filesToDelete  = filesToDelete;
    },
    currentFile:null,
    deleteFileToItem:function(listTitle,docName,id,success,error){
    	if(docName == null){
    		success();
    		return;
    	}
    	sputilities.deleteItemAttachment(
    		sputilities.contextInfo().listTitle,
    		id,
    		docName,
    		function(dataFile){
				spForm_attachment.currentDeletationIndex ++;
				var docs = utilities.getKeysOfAnObject(spForm_attachment.filesToDelete);
				if(spForm_attachment.currentDeletationIndex < docs.length){ 
					spForm_attachment.deleteFileToItem(listTitle,docs[spForm_attachment.currentDeletationIndex],id,success,error);
				}else{
					success();
				}
			},
			function(xhr){
				error(xhr);
			}
		);

    },
    addFileToItem:function(listTitle,docName,id,success,error){
    	if(docName == null){
    		success();
    		return;
    	}
    	var content = spForm_attachment.filesToAdd[docName].content;
    	sputilities.addItemAttachment(
    		sputilities.contextInfo().listTitle,
    		id,
    		docName,
    		content,
    		function(dataFile){
				spForm_attachment.currentAddIndex ++;
				var docs = utilities.getKeysOfAnObject(spForm_attachment.filesToAdd);
				if(spForm_attachment.currentAddIndex < docs.length){ 
					spForm_attachment.addFileToItem(listTitle,docs[spForm_attachment.currentAddIndex],id,success,error);
				}else{
					success();
				}
			},
			function(xhr){
				error(xhr);
			}
		);
    },
    addEventToAddFile:function(){	
        //document.getElementById(spForm_attachment.parameters.idBtnAddFile).attachEvent('onclick',function(){
            var countDocs = document.querySelectorAll('.item-doc').length;
            if(countDocs < spForm_attachment.parameters.maxlengthFiles && countDocs < 20){
                document.getElementById(spForm_attachment.parameters.idFileCtrl).click();
            }
        //})
    },
    addEventChangeFile:function(){ 
        document.getElementById(spForm_attachment.parameters.idFileCtrl).addEventListener("change", function(){
            spForm_attachment.currentFile = this.files[0];
            
            if(spForm_attachment.currentFile.name.length > spForm_attachment.parameters.maxSizeCharacter || spForm_attachment.currentFile.name.length > 35){
            	return false;
            }
            
            var regex = new RegExp(String.format("([a-zA-Z0-9\s_\\.\-:])+({0})$",spForm_attachment.parameters.formatAccept.join('|')));
            if(!regex.test(spForm_attachment.currentFile.name.toLowerCase())){
                return false;
            }

            var reader = new window.FileReader();
            reader.onload= function(event){
                if(spForm_attachment.files[spForm_attachment.currentFile.name] != null || (spForm_attachment.currentFile.size/1024) > (spForm_attachment.parameters.maxSizeInMB*1024)){
                    return false;
                }
                
                var contents = event.target.result;
                var countDocs = document.querySelectorAll('.item-doc').length;
                var strDocRow = String.format(spForm_attachment.parameters.htmlDocument,spForm_attachment.currentFile.name,"#")
                document.getElementById(spForm_attachment.parameters.idBodyFilesContainer).innerHTML += strDocRow;
                spForm_attachment.files[spForm_attachment.currentFile.name] = {
                    size: spForm_attachment.currentFile.size/1024,
                    type: spForm_attachment.currentFile.type,
                    content:contents
                }
                var row = document.querySelector(String.format('[sp-static-name="{0}"]',spForm_attachment.parameters.idFileCtrl))
		        if(row != null){
		        	row.removeAttribute('sp-invalid');
		        	row.classList.remove('has-error')
		        }

                if(document.querySelectorAll('.item-doc').length == spForm_attachment.parameters.maxlengthFiles){
                     document.getElementById(spForm_attachment.parameters.idBtnAddFile).classList.add('disabled')
                }
            }
            reader.onerror=function(event){
                console.error(String.format("File reading error {0}", event.target.error.code));
            }
            reader.readAsArrayBuffer(spForm_attachment.currentFile);
        });
    },
    setAcceptFiles:function(){
        var strAccept = ".".concat(spForm_attachment.parameters.formatAccept.join(',.'));
        document.getElementById(spForm_attachment.parameters.idFileCtrl).setAttribute('accept',strAccept);
    },
    deleteFile:function(ctrl){
        var nameDoc = ctrl.getAttribute('sp-attachment-file');
        delete spForm_attachment.files[nameDoc];
        var deleteElement = ctrl.parentNode.parentNode;
        ctrl.parentNode.parentNode.parentNode.removeChild(deleteElement);
        document.getElementById(spForm_attachment.parameters.idBtnAddFile).classList.remove('disabled');
        var row = document.querySelector(String.format('[sp-static-name="{0}"]',spForm_attachment.parameters.idFileCtrl))
        if(row != null){
        	row.removeAttribute('sp-invalid');
        	row.classList.remove('has-error')
        }
        
    },
    initialize:function(){
        spForm_attachment.setAcceptFiles();
        //spForm_attachment.addEventToAddFile();
        spForm_attachment.addEventChangeFile();
    },
    validate:function(){
        var row = document.querySelector(String.format('[sp-static-name="{0}"]',spForm_attachment.parameters.idFileCtrl))
        if(row != null){
	        if(spForm_attachment.parameters.required && document.querySelectorAll('.item-doc').length == 0){
	            row.setAttribute('sp-invalid',spForm_attachment.formats.errorRequired);
	        }
	        else if(document.querySelectorAll('.item-doc').length < spForm_attachment.parameters.minLengthFiles){
	            row.setAttribute('sp-invalid',String.format(spForm_attachment.formats.errorMinFiles,spForm_attachment.parameters.minLengthFiles));
	        }
	        else{
	            row.removeAttribute('sp-invalid');
	        }
	   	}
    }
}