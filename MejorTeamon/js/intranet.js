﻿String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

String.empty = "";

var utilities = {
	"http":{
		//default types of the http request
		"types":{
			"jsonOData": "application/json;odata=verbose"
		},
		//default methods of the http request
		"methods":{
			"GET":"GET",
			"POST":"POST",
			"PUT":"PUT",
			"MERGE":"MERGE",
			"DELETE":"DELETE"
		},
		//defult states of the http request
		"states":{
			"ready":4,
			"success":200,
			"created":201,
			"noContent":204
		},
		//custom function for the generic rest calls
		callRestService: function(options){
			try{
				//creating a request object for a rest call	
				var httpRequest = new XMLHttpRequest();
				//opening the rest client with the url and method specified
				httpRequest.open(options.method, options.url, true );
				//httpRequest.withCredentials = true;  
				//reading the keys of the header and adding properties
				for(var key in options.headers){
					httpRequest.setRequestHeader(key,options.headers[key]);
				}	
				//validating the method of the rest call and adding the custom data	
				if(options.method != utilities.http.methods.GET){
					httpRequest.send(options.data);
				}
				else{
					httpRequest.send();
				}
				//capturing change of the state in the rest call and calling custom success function
				httpRequest.onreadystatechange = function() {
					if (httpRequest.readyState == utilities.http.states.ready && ( httpRequest.status == utilities.http.states.success || httpRequest.status == utilities.http.states.created || httpRequest.status == utilities.http.states.noContent) ){
						//validating the correct type of the success function
						if(typeof options.success == "function"){
							options.success(httpRequest.responseText);
						}
					}
					else if(httpRequest.readyState == utilities.http.states.ready){
						if(typeof options.error == "function"){
							options.error(httpRequest.responseText);
						}
					}
				}
			}
			catch(e){
				//validating the correct type of the error function
				if(typeof options.error == "function"){
					options.error(e);
				}
			}
		}
	},
	//this function is used to get specific query string parameter
	getQueryString: function(name){
		var qs = decodeURIComponent(window.location.search).replace('?','');
		var params = qs.split('&');
		for( var i = 0 ; i < params.length; i++){
			var param = params[i];
			var vals = param.split('=');
			if(vals[0] == name){
				return vals[1];
			}
		}
		return null;
	},
	//this function can calulate a new GUID in the S4 format
	createGuid: function(){  
        function S4() {  
        	return (((1+Math.random())*0x10000)|0).toString(16).substring(1);  
    	}  
    	return (S4() + S4() + "-" + S4() + "-4" + S4().substr(0,3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();  
    },
    convertToJSON : function(str){
		try{
			var obj = JSON.parse(str);
			return obj;
		}
		catch(e){
			return null;
		}
	},
	str2ab:function(str) {
	  var buf = new ArrayBuffer(str.length*2); 
	  var bufView = new Uint32Array(buf);
	  for (var i=0, strLen=str.length; i < strLen; i++) {
	    bufView[i] = str.charCodeAt(i);
	  }
	  return buf;
	},
	getKeysOfAnObject:function(obj){
		return Object.keys(obj);
	}

}

var sputilities = {
	//saving current context of the SharePoint Page	
	contextInfo : function(){return _spPageContextInfo},	
	//tool for the api call in SharePoint Site
	api:{
		//default formats to url of SharePoint api calls
		urlFormats:{
			getFileByServerRelativeUrl : "{0}/_api/web/getfilebyserverrelativeurl('{1}')/$value",
			getFileByServerRelativeUrlMetaData : "{0}/_api/web/getfilebyserverrelativeurl('{1}')",
			addFileByServerRelativeUrl:"{0}/_api/web/GetFolderByServerRelativeUrl('{1}')/Files/add(url='{2}',overwrite=true)",
			getCurrentUserInfo:"{0}/_api/web/lists/getbytitle('Lista de información del usuario')/items({1})",
			getItems:"{0}/_api/web/lists/GetByTitle('{1}')/items{2}",
			getFieldsContentType:"{0}/_api/web/lists/getbytitle('{1}')/contenttypes?$filter=Name eq '{2}'{3}",
			getItemUpdate:"{0}/_api/web/lists/GetByTitle('{1}')/items({2})",
			getItemsByCaml:"{0}/_api/web/lists/getbytitle('{1}')/getitems{2}",
			getPeopleSearch:"{0}/_api/search/query{1}",
			getItemsByListId:"{0}/_api/web/lists(guid'{1}')/items{2}",
			getInfoListById:"{0}/_api/web/lists(guid'{1}'){2}",
			getWebInformation:"{0}/_api/web{1}",
			getMyUserProfileProperties:"{0}/_api/SP.UserProfiles.PeopleManager/GetMyProperties/{1}",
			getFieldsContentType:"{0}/_api/web/lists/getbytitle('{1}')/contenttypes?$filter=Name eq '{2}'{3}",
			addItemAttachment:"{0}/_api/web/lists/GetByTitle('{1}')/items({2})/AttachmentFiles/add(FileName='{3}')",
			getAttachmentFiles:"{0}/_api/web/lists/GetByTitle('{1}')/items({2})/AttachmentFiles{3}",
			getUserProfileProerty:"{0}/_api/SP.UserProfiles.PeopleManager/GetUserProfilePropertyFor(accountName=@v,PropertyName='{1}')?@v='{2}'",
			getUserProfileProperties:"{0}/_api/SP.UserProfiles.PeopleManager/GetPropertiesFor(accountName=@v)?@v='{1}'",
			getUserById:"{0}/_api/web/GetUserById({1})",
			deleteItemAttachment:"{0}/_api/web/lists/GetByTitle('{1}')/items({2})/AttachmentFiles/getbyFileName('{3}')",
			getSiteUsers:"{0}/_api/web/siteusers{1}",
			getSiteGroup:"{0}/_api/web/roleassignments/groups{1}"
		},
		scripts:{
			layoutsFormat:"{0}/_layouts/15/{1}",
			sp : "sp.js",
			spTaxonomy:"sp.taxonomy.js",
			spRuntime:"sp.runtime.js",
			spExecutor:"sp.requestexecutor.js",
			spSharing:"sharing.js"
		},
		functions:{
			displaySharingDialog : "displaySharingDialog"
		},
		types:{
			spCaml: 'SP.CamlQuery'
		},
		views:{
			allItems:"<View Scope='Recursive><RowLimit>5000</RowLimit><Query></Query></View>"
		}
	},
	//this tool is required to get all security information  of a SharePoint page
	formDigestInfo:{
		interval: 5,
		request: function(){return document.getElementById("__REQUESTDIGEST").value},
		updateFormDigest:function(){
			UpdateFormDigest(sputilities.contextInfo().webServerRelativeUrl,sputilities.formDigestInfo.interval)
		}
	},
	//this function allow call function when a page is completely loaded
	callFunctionOnLoadBody: function(nameFunction){
		_spBodyOnLoadFunctionNames.push(nameFunction);

	},
	displaySharingDialog:function(){
		sputilities.ensureSpScriptFunction(sputilities.api.scripts.spSharing,sputilities.api.functions.displaySharingDialog ,function(){
			DisplaySharingDialog(sputilities.contextInfo().webAbsoluteUrl, sputilities.contextInfo().pageListId, sputilities.contextInfo().pageItemId.toString()); 
		});
	},
	ensureSpScriptFunction:function(spScriptFile,spFunctionName,callBack){
		EnsureScriptFunc(spScriptFile,spFunctionName,function(){
			callBack();
		});
	},
	//this function allow get fields of sharepoint list form
	getFormFields:function(fieldsName){
		var objFields = {};
		for( var i = 0; i < fieldsName.length ; i++){
			var idQueryFormat = '[id^="{0}"]';
			var strField = fieldsName[i];
			var queryRule = String.format(idQueryFormat,strField);
			var nodes = document.querySelectorAll(queryRule);
			objFields[strField] = {};
			for(var j = 0; j < nodes.length; j++){
				var node = nodes[j];
				if(node.nodeName == "SPAN"){
					objFields[strField].label = node;
				}else {
					objFields[strField].input = node;
					objFields[strField].row = node.parentNode.parentNode.parentNode;
				}
			}
		}
		return objFields;
	},
	getFormFieldDisp:function(fieldsName){
		var objFields = {};
		for(var i = 0; i < fieldsName.length; i++){
			var idQueryFormat = '[name$="{0}"]';
			var strField = fieldsName[i];
			var queryRule = String.format(idQueryFormat,strField);
			var labelField = document.querySelector(queryRule);
			var textCell = labelField.parentNode.parentNode.nextElementSibling;
			var row = textCell.parentNode;
			objFields[strField] = {};
			objFields[strField].label = labelField;
			objFields[strField].input = textCell;
			objFields[strField].row = row;
		}
		return objFields;
	},
	//this function return the actual form table
	getFormTable:function(){
		return document.querySelectorAll('.ms-formtable')[0];
	},
	//this function convert the default form buttons to lite format
	convertFormButtons:function(){
		var btnSave = document.querySelector('[id$="SaveItem"]');
		var btnGoBack = document.querySelector('[id$="GoBack"]');
		var tableForm = document.querySelector('.ms-formtable');
		var divActions = document.createElement('div');
		divActions.appendChild(btnSave);
		divActions.appendChild(btnGoBack);
		tableForm.parentNode.insertBefore(divActions,tableForm.nextElementSibling);
		divActions.parentNode.removeChild(divActions.nextElementSibling);
		divActions.style.textAlign = "right";
		divActions.style.width = "100%";
	},
	overrideSaveButton:function(value,clickFunction){
		var newBtnSave = document.createElement('input');
		newBtnSave.value = value;
		newBtnSave.type = "button";
		document.querySelector('[id$="SaveItem"]').parentNode.insertBefore(newBtnSave,document.querySelector('[id$="SaveItem"]'));
		document.querySelector('[id$="SaveItem"]').style.display = "none";
		newBtnSave.addEventListener('click',function(){
			if(clickFunction()){
				document.querySelector('[id$="SaveItem"]').click();
			}
		});
	},
	//this function is required for calls to CSOM or JSOM
	executeDelaysp: function(callBack){
		var firstScriptTag = document.getElementsByTagName('body')[0]
		var scriptsRT = document.querySelectorAll(String.format("script[src$='{0}']",sputilities.api.scripts.spRuntime));
		var scriptRT = document.createElement('script');
		scriptRT.src = String.format(sputilities.api.scripts.layoutsFormat,sputilities.contextInfo().webAbsoluteUrl,sputilities.api.scripts.spRuntime);
		if(scriptsRT.length > 0){
			scriptRT = scriptsRT[0];
		}
		else{
			firstScriptTag.appendChild(scriptRT);
		}
		var scriptsSP = document.querySelectorAll(String.format("script[src$='{0}']",sputilities.api.scripts.sp));
		if(scriptsSP.length == 0){
			var scriptSP = document.createElement('script');
			scriptSP.src = String.format(sputilities.api.scripts.layoutsFormat,sputilities.contextInfo().webAbsoluteUrl,sputilities.api.scripts.sp);
			firstScriptTag.appendChild(scriptSP)
		}
		
		SP.SOD.executeOrDelayUntilScriptLoaded(function(){
			SP.SOD.executeOrDelayUntilScriptLoaded(callBack,sputilities.api.scripts.sp);
		},sputilities.api.scripts.spRuntime);
	},
	executeDelayspTaxonomy:function(callBack){
		sputilities.executeDelaysp(function(){
			var scriptsTaxonomy = document.querySelectorAll(String.format("script[src$='{0}']",sputilities.api.scripts.spTaxonomy));
			if(scriptsTaxonomy.length == 0){
				var firstScriptTag = document.getElementsByTagName('body')[0]
				var scriptTaxonomy = document.createElement('script');
				scriptTaxonomy.src = String.format(sputilities.api.scripts.layoutsFormat,sputilities.contextInfo().webAbsoluteUrl,sputilities.api.scripts.spTaxonomy);
				firstScriptTag.appendChild(scriptTaxonomy)
			}
			SP.SOD.executeOrDelayUntilScriptLoaded(callBack,sputilities.api.scripts.spTaxonomy);
		});
	},
	executeDelayspRequestExecutor:function(callBack){
		sputilities.executeDelaysp(function(){
			var firstScriptTag = document.getElementsByTagName('script')[0]
			var scriptExecutor = document.createElement('script');
			scriptExecutor.src = String.format(sputilities.api.scripts.layoutsFormat,sputilities.contextInfo().webAbsoluteUrl,sputilities.api.scripts.spExecutor);
			firstScriptTag.parentNode.insertBefore(scriptExecutor,firstScriptTag)
			SP.SOD.executeOrDelayUntilScriptLoaded(callBack,sputilities.api.scripts.spExecutor);
		});
	},
	//this function is used to get all content of a library file
	getFileContent: function(fileUrl,success,error){
		var url = String.format(sputilities.api.urlFormats.getFileByServerRelativeUrl, sputilities.contextInfo().webAbsoluteUrl,fileUrl);
		utilities.http.callRestService({
			url:url,
			method:utilities.http.methods.GET,
			headers:{
				"accept":utilities.http.types.jsonOData
			},
			success:success,
			error:error
		});
	},
	//this function is used to update all content of a library file
	updateFileContent:function(fileUrl,data,success,error){
		var url = String.format(sputilities.api.urlFormats.getFileByServerRelativeUrl, sputilities.contextInfo().webAbsoluteUrl,fileUrl);
		sputilities.formDigestInfo.updateFormDigest();
		utilities.http.callRestService({
			url:url,
			method:utilities.http.methods.POST,
			data: data,
			headers:{
				"accept":utilities.http.types.jsonOData,
				"X-RequestDigest":sputilities.formDigestInfo.request(),
				"X-HTTP-Method": utilities.http.methods.PUT
			},
			success:success,
			error:error
		});
	},
	//this function is used to add a new file of library
	addFileContent:function(urlLibrary,fileName,data,success,error){
		var url = String.format(sputilities.api.urlFormats.addFileByServerRelativeUrl, sputilities.contextInfo().webAbsoluteUrl,urlLibrary,fileName);
		sputilities.formDigestInfo.updateFormDigest();
		utilities.http.callRestService({
			url:url,
			method:utilities.http.methods.POST,
			data: data,
			headers:{
				"accept":utilities.http.types.jsonOData,
				"X-RequestDigest":sputilities.formDigestInfo.request()			
			},
			success:success,
			error:error
		});
	},
	addItem:function(ListName,data,success,error,webUrl)	{
		var url = String.format(sputilities.api.urlFormats.getItems,sputilities.contextInfo().webAbsoluteUrl,ListName);
		if(webUrl!= null){
			url = String.format(sputilities.api.urlFormats.getItems,webUrl,query);
		}
		utilities.http.callRestService({
			url:url,
			method:utilities.http.methods.POST,
			data: JSON.stringify(data),
			headers:{
				"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData,
				"X-RequestDigest":sputilities.formDigestInfo.request(),
	        
			},
			success:success,
			error:error
		});
	},
	deleteItemAttachment:function(ListName,idItem,name,success,error,webUrl){
		var url = String.format(sputilities.api.urlFormats.deleteItemAttachment,sputilities.contextInfo().webAbsoluteUrl,ListName,idItem,name);
		if(webUrl!= null){
		    url = String.format(sputilities.api.urlFormats.deleteItemAttachment,webUrl,query);
		}
		utilities.http.callRestService({
			url:url,
			method:utilities.http.methods.DELETE,
			headers:{
				"X-RequestDigest":sputilities.formDigestInfo.request(),   
				"X-HTTP-Method": utilities.http.methods.DELETE 
			},
			success:success,
			error:error
		});

	},
	addItemAttachment:function(ListName,idItem,name, data,success,error,webUrl){
		var url = String.format(sputilities.api.urlFormats.addItemAttachment,sputilities.contextInfo().webAbsoluteUrl,ListName,idItem,name);
		if(webUrl!= null){
		    url = String.format(sputilities.api.urlFormats.addItemAttachment,webUrl,query);
		}
		utilities.http.callRestService({
			url:url,
			method:utilities.http.methods.POST,
			data: data,
			headers:{
				"X-RequestDigest":sputilities.formDigestInfo.request(),    
			},
			success:success,
			error:error
		});

	},
	updateItem:function(ListName,data,ID,success,error,webUrl)	{
		var url = String.format(sputilities.api.urlFormats.getItemUpdate,sputilities.contextInfo().webAbsoluteUrl,ListName,ID);
		if(webUrl!= null){
			url = String.format(sputilities.api.urlFormats.getItems,webUrl,query);
		}
		utilities.http.callRestService({
			url:url,
			method:utilities.http.methods.POST,
			data: JSON.stringify(data),
			headers:{
			  	"IF-MATCH": "*",
			  	"X-HTTP-Method": utilities.http.methods.MERGE,
        	  	"accept":utilities.http.types.jsonOData,
			  	"content-type": utilities.http.types.jsonOData,
				"X-RequestDigest":sputilities.formDigestInfo.request(),

		             
			},
			success:success,
			error:error
		});

	},
	//this function is used to get all current user information
	getCurrentUserInformation: function(success,error){
		var url = String.format(sputilities.api.urlFormats.getCurrentUserInfo, sputilities.contextInfo().siteAbsoluteUrl,sputilities.contextInfo().userId);
		utilities.http.callRestService({
			url:url,
			method:utilities.http.methods.GET,
			headers:{
				"accept":utilities.http.types.jsonOData
			},
			success:success,
			error:error
		});
	},
	getSiteUsers:function(query,success,error){
		var url = String.format(sputilities.api.urlFormats.getSiteUsers, sputilities.contextInfo().siteAbsoluteUrl,query);
		utilities.http.callRestService({
		    url:url,
			method:utilities.http.methods.GET,
			headers:{
				"accept":utilities.http.types.jsonOData
			},
			success:success,
			error:error
	    });
	},
	getSiteGroups:function(query,success,error){
		var url = String.format(sputilities.api.urlFormats.getSiteGroup, sputilities.contextInfo().webAbsoluteUrl,query);
		utilities.http.callRestService({
		    url:url,
			method:utilities.http.methods.GET,
			headers:{
				"accept":utilities.http.types.jsonOData
			},
			success:success,
			error:error
	    });
	},
	getSiteUserByEmail:function(email,success,error){
		var claimsEmail = String.format("(@v)?@v='{0}'i:0{1}.f|membership|{0}",email,encodeURIComponent('#'));
		sputilities.getSiteUsers(query,success,error);
	},
	//this function is user to get all user informations by id
	getUserInformation:function(id,success,error){
		var url = String.format(sputilities.api.urlFormats.getCurrentUserInfo, sputilities.contextInfo().siteAbsoluteUrl,id);
		utilities.http.callRestService({
			url:url,
			method:utilities.http.methods.GET,
			headers:{
				"accept":utilities.http.types.jsonOData
			},
			success:success,
			error:error
		});
	},
	//this function allow read specific property for user by email
	getUserProfileProperty: function(property,email,success,error){
		var claimsEmail =String.format('i:0{1}.f|membership|{0}',email,encodeURIComponent('#'));
		var url = String.format(sputilities.api.urlFormats.getUserProfileProerty, sputilities.contextInfo().siteAbsoluteUrl,property,claimsEmail);
		utilities.http.callRestService({
		    url:url,
			method:utilities.http.methods.GET,
			headers:{
				"accept":utilities.http.types.jsonOData
			},
			success:success,
			error:error
	    });
	},
	getUserProfileProperties: function(email,success,error){
		var claimsEmail =String.format('i:0{1}.f|membership|{0}',email,encodeURIComponent('#'));
		var url = String.format(sputilities.api.urlFormats.getUserProfileProperties, sputilities.contextInfo().siteAbsoluteUrl,claimsEmail);
		utilities.http.callRestService({
		    url:url,
			method:utilities.http.methods.GET,
			headers:{
				"accept":utilities.http.types.jsonOData
			},
			success:success,
			error:error
	    });
	},
	getUserId: function(Id,success,error){
		var url = String.format(sputilities.api.urlFormats.getUserById, sputilities.contextInfo().siteAbsoluteUrl,Id);
		utilities.http.callRestService({
		    url:url,
			method:utilities.http.methods.GET,
			headers:{
				"accept":utilities.http.types.jsonOData
			},
			success:success,
			error:error
	    });
	},


	//this function is used to get items with a specific filter in the list sended
	getItems:function(listName,query,success,error,webUrl)	{
		var url = String.format(sputilities.api.urlFormats.getItems,sputilities.contextInfo().webAbsoluteUrl,listName,query);
		if(webUrl!= null){
			url = String.format(sputilities.api.urlFormats.getItems,webUrl,listName,query);
		}
		utilities.http.callRestService({
			url:encodeURI(url),
			method:utilities.http.methods.GET,
			headers:{
				"accept":utilities.http.types.jsonOData
			},
			success:success,
			error:error
		});

	},
	getAttachmentsOfItem:function(listName,id,query,success,error,webUrl,nameAttachment)	{
		var url = String.format(sputilities.api.urlFormats.getAttachmentFiles,sputilities.contextInfo().webAbsoluteUrl,listName,id,query);
		if(webUrl!= null){
			url = String.format(sputilities.api.urlFormats.getAttachmentFiles,webUrl,listName,id,query);
		}
		utilities.http.callRestService({
			url:url,
			method:utilities.http.methods.GET,
			headers:{
				"accept":utilities.http.types.jsonOData
			},
			success:function(data){
				success(data,nameAttachment)
			},
			error:error
		});

	},
	//this function is used to get items with a specific filter in the list sended
	getMyUserProfileProperties:function(query,success,error,webUrl)	{
		var url = String.format(sputilities.api.urlFormats.getMyUserProfileProperties,sputilities.contextInfo().webAbsoluteUrl,query);
		if(webUrl!= null){
			url = String.format(sputilities.api.urlFormats.getMyUserProfileProperties,webUrl,query);
		}
		utilities.http.callRestService({
			url:url,
			method:utilities.http.methods.GET,
			headers:{
				"accept":utilities.http.types.jsonOData
			},
			success:success,
			error:error
		});
	},
	getItemsByCaml:function(options){
		if(options.siteUrl == null){
			options["siteUrl"] = sputilities.contextInfo().webAbsoluteUrl;
		}
		var data = { 
		       query:{ 
		           "__metadata": { 'type': sputilities.api.types.spCaml},
		           "ViewXml": options.caml == null ? sputilities.api.views.allItems : options.caml
		        } 
		   }; 
		var url = String.format(sputilities.api.urlFormats.getItemsByCaml, options.siteUrl,options.listName,options.query);
		//sputilities.formDigestInfo.updateFormDigest();
		utilities.http.callRestService({
			url:url,
			method:utilities.http.methods.POST,
			data: JSON.stringify(data),
			headers:{
				"accept":utilities.http.types.jsonOData,
				"content-type": utilities.http.types.jsonOData,
				'odata-version':'3.0',
				"X-RequestDigest":sputilities.formDigestInfo.request()			
			},
			success:options.success,
			error:options.error
		});
	},
	getTermsInTermSet: function(options){
		if(options.siteUrl == null){
			options["siteUrl"] = sputilities.contextInfo().webAbsoluteUrl;
		}
		var ctx  = new SP.ClientContext(options.siteUrl);
		var ssTaxonomy = SP.Taxonomy.TaxonomySession.getTaxonomySession(ctx);
		var termStore = ssTaxonomy.getDefaultSiteCollectionTermStore();
		if(options.termStoreName != null){
	    	termStore = ssTaxonomy.get_termStores().getByName(options.termStoreName);
		}
		var termSet = termStore.getTermSet(options.termSetId);
		var terms = termSet.getAllTerms();
		ctx.load(terms);
		ctx.executeQueryAsync(function(){
			if(typeof options.success == "function"){
				options.success(terms);
			}
		
		},function(sender,args){
			if(typeof options.error == "function"){
				options.error(args.get_message());
			}
		});
	},
	getItemsByListId:function(listID,query,success,error){
		var url = String.format(sputilities.api.urlFormats.getItemsByListId,sputilities.contextInfo().webAbsoluteUrl,listID,query);
		utilities.http.callRestService({
			url:url,
			method:utilities.http.methods.GET,
			headers:{
				"accept":utilities.http.types.jsonOData
			},
			success:success,
			error:error
		});

	},
	getInfoListById:function(listID,query,success,error){
		var url = String.format(sputilities.api.urlFormats.getInfoListById,sputilities.contextInfo().webAbsoluteUrl,listID,query);
		utilities.http.callRestService({
			url:url,
			method:utilities.http.methods.GET,
			headers:{
				"accept":utilities.http.types.jsonOData
			},
			success:success,
			error:error
		});
	},
	getWebInformation:function(webUrl,query,success,error)	{
		var url = String.format(sputilities.api.urlFormats.getWebInformation,sputilities.contextInfo().webAbsoluteUrl,query);
		if(webUrl!= null){
			url = String.format(sputilities.api.urlFormats.getWebInformation,webUrl,query);
		}
		utilities.http.callRestService({
			url:url,
			method:utilities.http.methods.GET,
			headers:{
				"accept":utilities.http.types.jsonOData
			},
			success:success,
			error:error
		});

	},
	showModalDialog:function(title,page,tUrlRedirect,html,callBack){
		var div = document.createElement('div');
		div.innerHTML = html;
		var options = {
				autosize:true,				
	        	url: page,
	        	title: title,
	        	dialogReturnValueCallback : function(result, returnValue){
	        		if(callBack != null){
	        			callBack();
	        		}
	        		if(tUrlRedirect != null){
	        			if(result == SP.UI.DialogResult.OK){
				    		window.location.href = tUrlRedirect;
				   		}
				   	}
				},
				html: html != null ? div : null
    		};
    	try {
        	SP.UI.ModalDialog.showModalDialog(options);
    	}
    	catch (error) {
        	SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showModalDialog', options);
    	}
	},
	showModalDialogForms:function(title,page,tUrlRedirect,html,callBack){
		var div = document.createElement('div');
		div.innerHTML = html;
		var options = {
				autosize:false,
				allowMaximize: false,			
	        	url: page,
	        	title: title,
	        	dialogReturnValueCallback : function(result, returnValue){
	        		if(callBack != null){
	        			callBack();
	        		}
	        		if(tUrlRedirect != null){
	        			if(result == SP.UI.DialogResult.OK){
				    		window.location.href = tUrlRedirect;
				   		}
				   		if(result == SP.UI.DialogResult.cancel){
				    		window.location.href = tUrlRedirect;
				   		}

				   	}
				},
				html: html != null ? div : null
    		};
    	try {
        	SP.UI.ModalDialog.showModalDialog(options);
    	}
    	catch (error) {
        	SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showModalDialog', options);
    	}
	},
	getPeople:function(query,success,error,webUrl)	{
		var url = String.format(sputilities.api.urlFormats.getPeopleSearch,sputilities.contextInfo.webAbsoluteUrl,query);
		if(webUrl!= null){
			url = String.format(sputilities.api.urlFormats.getPeopleSearch,webUrl,query);
		}
		utilities.http.callRestService({
			url:url,
			method:utilities.http.methods.GET,
			headers:{
				"accept":utilities.http.types.jsonOData
			},
			success:success,
			error:error
		});
	},
	// this content type 
	getContentTypeOfList:function(ctName,ListName,query,success,error,webUrl)	{
		ctName = encodeURIComponent(ctName);
		var url = String.format(sputilities.api.urlFormats.getFieldsContentType,sputilities.contextInfo().webAbsoluteUrl,ListName,ctName,query);
		if(webUrl!= null){
			url = String.format(sputilities.api.urlFormats.getItems,webUrl,query);
		}
		utilities.http.callRestService({
			url:url,
			method:utilities.http.methods.GET,
			headers:{
				"accept":utilities.http.types.jsonOData
			},
			success:success,
			error:error
		});

	},
	initializePeoplePicker:function(options) {
	    var schema = {};
	    schema['PrincipalAccountType'] = 'User,DL,SecGroup,SPGroup';
	    schema['SearchPrincipalSource'] = 16;
	    schema['ResolvePrincipalSource'] = 16;
	    schema['AllowMultipleValues'] = options.allowMultiple;
	    schema['MaximumEntitySuggestions'] = 50;
	    schema["InitialHelpText"] = options.placeHolder;
	    schema["OnUserResolvedClientScript"] = options.userResolved;
	    //schema['Width'] = '165px';
	    //schema['Height'] = '55px';
	    schema['lcid'] = 3082;
	    schema['langid']= 3082;
	    SPClientPeoplePicker_InitStandaloneControlWrapper(options.controlId, null, schema);
	}


}
var likesComments = {
	parameters:{
		queryStringListParam : "ListaBase",
		queryStringIDItemParam : "IDItem",
		fileUrlFormat : "{0}/LikesComment/{1}.js",
		libraryUrlFormat:"{0}/LikesComment",
		fileNameFormat:"{0}.js",
		inputIdComment:"txtComment",
		idCommentsContainer:"comments-container",
		tagCommentContainer:"div",
		formatContentComment:"<div><h3>{1}</h3><a class='coments-delete'  id-comment='{0}' onclick='javascript:components.likesComments.removeComment(this)'><img src='{4}/SiteAssets/Development/images/components/likesComment/ico-delete.png'></a></div><label ><h4>{2}</h4><p>{3}</p>",
		formatDate:"{0}/{1}/{2}  {3}:{4}",
		idBtnLike:"btnLike",
		idBtnDontLike:"btnDontLike",
		attributeIdComment:"id-comment",
		formatHTMLMyComment:"<div class='coments-item' id='{0}'><div><h3>{1}</h3><a class='coments-delete'  id-comment='{0}' onclick='javascript:components.likesComments.removeComment(this)'><img src='{4}/SiteAssets/Development/images/components/likesComment/ico-delete.png'></a></div><label ><h4>{2}</h4><p>{3}</p></div>",
		formatHTMLComment:"<div class='coments-item' id='{0}'><div><h3>{1}</h3><a class='coments-delete'  id-comment='{0}'><img src='{4}/SiteAssets/Development/images/components/likesComment/ico-delete.png'></a></div><label ><h4>{2}</h4><p>{3}</p></div>",
		classComment : "coments-item",
		formatComponent:'<div class="coments-writetext"><textarea id="txtComment" rows="5"></textarea></div><div class="coments-actioncoments"><div class="coments-actioncoments-like"><a id="btnLike" onclick="javascript:components.likesComments.like(this)"><img src="{0}/SiteAssets/Development/images/components/likesComment/ico-like.png"/></a><a id="btnDontLike" onclick="javascript:components.likesComments.dontlike(this)"><img src="{0}/SiteAssets/Development/images/components/likesComment/ico-dontlike.png"/></a><span id="likes-count"></span></div><div class="coments-actioncoments-send"><input value="comentar" type="button" onclick="javascript:components.likesComments.comment(this)"/></div></div><div class="coments-list" id="comments-container"></div>',
		principalContent:"comments-component",
		idLikesCount:"likes-count"
	},
	initialize:function(listParam,idItemParam){
		document.getElementById("comments-component").innerHTML = String.format(likesComments.parameters.formatComponent,sputilities.contextInfo().webAbsoluteUrl);
		sputilities.getCurrentUserInformation(function(data){
			sputilities.contextInfo["userLoginName"]= JSON.parse(data).d.Name;
			sputilities.contextInfo["userName"]= String.format("{0} {1}",JSON.parse(data).d.FirstName,JSON.parse(data).d.LastName);
		},function(xhr){console.log(xhr)});
		var idItem = idItemParam;
		var list = listParam;
		idItem = idItem == null ? utilities.getQueryString(likesComments.parameters.queryStringIDItemParam) : idItem;
		list = list == null ? utilities.getQueryString(likesComments.parameters.queryStringListParam) : list;
		var fileUrl = String.format(likesComments.parameters.fileUrlFormat,sputilities.contextInfo().webServerRelativeUrl,list);
		sessionStorage["fileUrl"] = fileUrl;
		if(idItem != null && list != null){
			sputilities.getFileContent(fileUrl,function(data){
				console.log("associated file to list finded");
				sessionStorage["currentItem"] = idItem.toString();
				sessionStorage["jsonLikesComment"] = data;
				var obj = JSON.parse(data);
				document.getElementById(likesComments.parameters.idBtnDontLike).style.display = "none";
				document.getElementById(likesComments.parameters.idLikesCount).innerHTML = 0;
				if(obj[sessionStorage["currentItem"]] == null){
					obj[sessionStorage["currentItem"]] = {
						likes:[],
						comments:[]
					}
					sessionStorage["jsonLikesComment"] = JSON.stringify(obj);
					sputilities.updateFileContent(sessionStorage["fileUrl"], sessionStorage["jsonLikesComment"],function(data){
						console.log("data updated successfully")
					});
				}
				else{
					components.likesComments.insertAllComments()
					components.likesComments.validateLike()	
				}
			},
			function(){
				var library = String.format(likesComments.parameters.libraryUrlFormat,sputilities.contextInfo().webServerRelativeUrl);
				var fileName = String.format(likesComments.parameters.fileNameFormat,list);
				var initialObj = {};
				initialObj[idItem] = {
					likes:[],
					comments:[]
				}
				sputilities.addFileContent(library,fileName,JSON.stringify(initialObj),function(data){
					console.log("associated file to list created");
					sessionStorage["currentItem"] = idItem.toString();
					sessionStorage["jsonLikesComment"] = JSON.stringify(initialObj);
				},
				function(){})
			});
		}
	},
	comment:function(ctrl){
		ctrl.disabled = true;
		var txtComment = document.getElementById(likesComments.parameters.inputIdComment).value;
		if(txtComment.trim() != ''){
			var objComment = new components.likesComments.objects.Comment(txtComment);
			var obj = JSON.parse(sessionStorage["jsonLikesComment"]);
			obj[sessionStorage["currentItem"]].comments.push(objComment);
			sessionStorage["jsonLikesComment"] = JSON.stringify(obj);
			sputilities.updateFileContent(sessionStorage["fileUrl"], sessionStorage["jsonLikesComment"],function(data){
				console.log("data updated successfully")
				ctrl.disabled = false;
			});
			var date = new Date(objComment.date);
			var minutes = (date.getMinutes() < 10 ?'0':'') + date.getMinutes();
			var strDate = String.format(likesComments.parameters.formatDate,date.getDate(),date.getMonth() + 1,date.getFullYear(),date.getHours(),minutes);
			var htmlComment = String.format(likesComments.parameters.formatContentComment,objComment.id,objComment.user,strDate,objComment.text,sputilities.contextInfo().webAbsoluteUrl);
			var section = document.createElement(likesComments.parameters.tagCommentContainer);
			section.id = objComment.id;
			section.setAttribute('class',likesComments.parameters.classComment);
			section.innerHTML = htmlComment;
			document.getElementById(likesComments.parameters.idCommentsContainer).appendChild(section);
			document.getElementById(likesComments.parameters.inputIdComment).value = "";
		}
		ctrl.disabled = false;
	},
	like:function(ctrl){
		ctrl.style.display = "none";
		document.getElementById(likesComments.parameters.idBtnDontLike).style.display = "inline-block";
		var objLike = new components.likesComments.objects.Like();
		var obj = JSON.parse(sessionStorage["jsonLikesComment"]);
		obj[sessionStorage["currentItem"]].likes.push(objLike);
		sessionStorage["jsonLikesComment"] = JSON.stringify(obj);
		sputilities.updateFileContent(sessionStorage["fileUrl"], sessionStorage["jsonLikesComment"],function(data){
			console.log("data updated successfully")
			ctrl.disabled = false;
			document.getElementById(likesComments.parameters.idLikesCount).innerHTML = parseInt(document.getElementById(likesComments.parameters.idLikesCount).innerHTML) + 1
		});
	},
	dontlike:function(ctrl){
		ctrl.style.display = "none";
		document.getElementById(likesComments.parameters.idBtnLike).style.display = "inline-block";
		var obj = JSON.parse(sessionStorage["jsonLikesComment"]);
		var objItem = obj[sessionStorage["currentItem"]];
		obj[sessionStorage["currentItem"]].likes = objItem.likes.filter(function(like){
			return like.userId != sputilities.contextInfo().userId;
		});
		sessionStorage["jsonLikesComment"] = JSON.stringify(obj);
		sputilities.updateFileContent(sessionStorage["fileUrl"], sessionStorage["jsonLikesComment"],function(data){
			console.log("data updated successfully")
			ctrl.disabled = false;
			document.getElementById(likesComments.parameters.idLikesCount).innerHTML = parseInt(document.getElementById(likesComments.parameters.idLikesCount).innerHTML) - 1
		});
	},
	removeComment:function(ctrl){
		ctrl.disabled = true;
		var obj = JSON.parse(sessionStorage["jsonLikesComment"]);
		var objItem = obj[sessionStorage["currentItem"]];
		obj[sessionStorage["currentItem"]].comments = objItem.comments.filter(function(comment){
			return comment.id != ctrl.getAttribute(likesComments.parameters.attributeIdComment);
		});
		sessionStorage["jsonLikesComment"] = JSON.stringify(obj);
		sputilities.updateFileContent(sessionStorage["fileUrl"], sessionStorage["jsonLikesComment"],function(data){
			console.log("data updated successfully")
			ctrl.disabled = false;
		});
		var nodeComment = document.getElementById(ctrl.getAttribute(likesComments.parameters.attributeIdComment));
		nodeComment.parentNode.removeChild(nodeComment);
	},
	insertAllComments: function(){
		var obj = JSON.parse(sessionStorage["jsonLikesComment"])[sessionStorage["currentItem"]];
		if(obj == null){
			return;
		}
		var comments = obj.comments;
		var strComments = '';
		for(var i = 0; i < comments.length ; i++){
			var comment = comments[i];
			var date = new Date(comment.date);
			var minutes = (date.getMinutes() < 10 ?'0':'') + date.getMinutes();
			var strDate = String.format(likesComments.parameters.formatDate,date.getDate(),date.getMonth() + 1,date.getFullYear(),date.getHours(),minutes);
			var htmlComment = String.format(likesComments.parameters.formatHTMLComment,comment.id,comment.user,strDate,comment.text,sputilities.contextInfo().webAbsoluteUrl);
			if(comment.userId == sputilities.contextInfo().userId){
				htmlComment = String.format(likesComments.parameters.formatHTMLMyComment,comment.id,comment.user,strDate,comment.text,sputilities.contextInfo().webAbsoluteUrl);
			}
			strComments += htmlComment;
		}
		document.getElementById(likesComments.parameters.idCommentsContainer).innerHTML = strComments;
	},
	validateLike:function(){
		var obj = JSON.parse(sessionStorage["jsonLikesComment"])[sessionStorage["currentItem"]];
		if(obj == null){
			document.getElementById(likesComments.parameters.idBtnDontLike).style.display = "none";
			return;
		} 
		var likes = obj.likes;
		document.getElementById(likesComments.parameters.idLikesCount).innerHTML =  likes.length;
		for(var i = 0; i < likes.length ;i++){
			var like = likes[i];
			if(like.userId == sputilities.contextInfo().userId){
				document.getElementById(likesComments.parameters.idBtnDontLike).style.display = "inline-block";
				document.getElementById(likesComments.parameters.idBtnLike).style.display = "none";
				return;
			}
		}
		document.getElementById(likesComments.parameters.idBtnDontLike).style.display = "none";
		document.getElementById(likesComments.parameters.idBtnLike).style.display = "inline-block";
	},
	objects:{
		Comment: function(text){
			this.id = utilities.createGuid();
		    this.date = new Date();
		    this.userId = sputilities.contextInfo().userId;
		    this.user = sputilities.contextInfo.userName;
		    this.text = text;
		},
		Like:function(){
			this.id = utilities.createGuid();
	   		this.userId = sputilities.contextInfo().userId;
		}
	}
}

var components = {
	likesComments:likesComments
}
var lagash ={
	components:components,
	utilities:utilities,
	sputilities: sputilities
} 
