$(document).ready(function(){
    $('.accordion-titleSection').click(function(){
        var index = $(this).index();
        var parent = $(this).parent();
        var items = $(parent).find('.accordion-titleSection:not(:nth-of-type(' + (index+1) +'))');
        console.log(items);
        
        if($(this).attr('aria-expanded') == "true"){
        	$(this).addClass('collapsed');
	        $(this).attr('aria-expanded','false');
	        $(this).next().attr('class','collapse');
	        $(this).next().attr('aria-expanded','false');

        }else{
        	$(this).removeClass('collapsed');
	        $(this).attr('aria-expanded','true');
	        $(this).next().attr('class','collapse in');
	        $(this).next().attr('aria-expanded','true');
        }
        
        $(items[0]).addClass('collapsed');
        $(items[0]).attr('aria-expanded','false');
        $(items[0]).next().attr('class','collapse');
        $(items[0]).next().attr('aria-expanded','false');
    });
})