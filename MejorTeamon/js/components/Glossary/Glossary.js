﻿ /***********************/
 //initialize before body load
sputilities.callFunctionOnLoadBody('startGlossary');
 
 function startGlossary(){
	 sputilities.executeDelayspTaxonomy(glossary.initialize)
 } 
 
 var glossary={
	 //create components Parameters
	 parameters:{
		listName:"Glosario Corporativo",
		containerId:"glossaryContainer",
		termIdAreas:"cdb7e347-a3ed-43fa-81b5-8748ffbaa65b",
		glossaryFormatHTML:"<div class='content-search'><div class='searchPaginator'><label>Resultados por pagina:</label><select id='itemsBypage' onchange='glossary.getFilteredItems(\"start\")'>"
					+"<option value='10'>10</option><option value='20'>20</option><option value='30'>30</option></select></div><div class='Search-Box'><label>Busqueda:</label>"
					+"<input type='text' placeholder='Nombre/Palabra Clave' id='searchBox'/></div></div>"
					+"<section id='headerList'>"
					+"<section id='letterFilter'></section><div class='business-list'><label>Area de negocio:</label><select id='businessAreaList' onchange='glossary.filterOptionBybussinesArea(this)'>"
					+"<option value='Todos' >Todos</option></select></div><div class='category-list'><label>Categoria:</label><select id='businessCategoryList' onchange='glossary.getFilteredItems(this)'>"
					+"<option value='Todos'>Todos</option></select></div></section><section id='glossaryList'>"
					+"</section></section></section><section id='glossaryFooter'></section>"		
	 },
	 initialize:function(){
		//insert HTML format in HTML Code
		document.getElementById(glossary.parameters.containerId).innerHTML = glossary.parameters.glossaryFormatHTML;
		//get sections by insert glossary information
		var letterFilterList = document.getElementById('letterFilter');
		var businessList = document.getElementById('businessAreaList');
		var categoryList = document.getElementById('businessCategoryList');
		var glossaryList = document.getElementById('glossaryList');
		//create alphabet filter
		glossary.createAlphabetFilter(letterFilterList);
		// get terms administered
		glossary.getSelectValues(glossary.parameters.termIdAreas,businessList,categoryList,glossaryList);
		//add envent searchBox
		var searchBox = document.getElementById('searchBox')		
		searchBox.addEventListener('keypress', function (e) {			
			if (e.keyCode == 13) { 
				e.preventDefault();					
				glossary.getFilteredItems(searchBox)
			}			
		});		
	 },
	 getSelectValues:function(termSetId,businessList,categoryList,glossaryList){
		try{
			//clear Storage
			sessionStorage.clear();
			//execute function get terms adminstered
			sputilities.getTermsInTermSet({
				termSetId:termSetId,
				success:function(terms){
					var enumerator = terms.getEnumerator();
					var businessInventory = {};
					try{
						while(enumerator.moveNext()){
							//create option element 
							var term = enumerator.get_current();
							var terms = term.get_pathOfTerm();
							var strTerms = terms.split(";");							
							//create object for business Inventory
							if(businessInventory[strTerms[0]] == null){
								businessInventory[strTerms[0]] = []
							}
							//create select options in list
							if(strTerms.length == 2){
								// create option in category list
								var selectOption = document.createElement('option');
								selectOption.value = strTerms[1];
								selectOption.setAttribute('data-category',strTerms[1]);
								selectOption.style.display = 'none';
								var textOption = document.createTextNode(strTerms[1]);
								selectOption.appendChild(textOption);
								categoryList.appendChild(selectOption);
								businessInventory[strTerms[0]].push(strTerms[1]);								
							}else{
								// create options in businessArea list
								var businessOption = document.createElement('option');
								businessOption.setAttribute('value',strTerms[0]);
								var optionText = document.createTextNode(strTerms[0]);
								businessOption.appendChild(optionText);							
								businessList.appendChild(businessOption);
							}						
						}
						//insert in sessionStorage 
						sessionStorage["businessArea"] = JSON.stringify(businessInventory);						
						//get glossary terms
						glossary.getGlossaryTerms(glossaryList);						
					}catch(e){
						throw e
					}				
				},
				error:function(error){
					console.log(error)
				}
			});
		}catch(e){
			console.log(String.format("Ocurrió un error en la ejecución del método glossary.getSelectValues() {0}",e))
		}		
	 },
	 createAlphabetFilter:function(letterFilterList){
		 try{
			//create a label element by abc filter
			for(var i=65;i<91;i++){
				var char = String.fromCharCode(i);
				var letterFilter = document.createElement('label');
				letterFilter.setAttribute('class','letter');
				letterFilter.setAttribute('onclick','glossary.getFilteredItems(this)');
				var letter = document.createTextNode(char);				
				letterFilter.appendChild(letter);
				letterFilterList.appendChild(letterFilter);	
				if(i == 78){
					var letterFilter = document.createElement('label');
					letterFilter.setAttribute('class','letter');
					letterFilter.setAttribute('onclick','glossary.getFilteredItems(this)');
					var letter = document.createTextNode('Ñ');
					letterFilter.appendChild(letter);
					letterFilterList.appendChild(letterFilter);
				}	
			}
			//create label element by reset filter
			var letterFilter = document.createElement('label');
			letterFilter.setAttribute('class','letter');
			letterFilter.setAttribute('onclick','glossary.getFilteredItems(this)');
			letterFilter.setAttribute('id','todos');
			var letter = document.createTextNode('Todos');
			letterFilter.appendChild(letter);
			
			letterFilterList.appendChild(letterFilter);	
		 }catch(e){
			console.log(String.format("Ocurrió un error en la ejecución del método glossary.createAlphabetFilter() {0}",e)) 
		 } 
	},
	getGlossaryTerms:function(glossaryList){
		try{
			//execute function for get Items
			sputilities.getItemsByCaml({
				listName:glossary.parameters.listName,
				success:function(data){
					var items = JSON.parse(data).d.results;
					try{
						for (var i=0;i<items.length;i++){
							var term = items[i];
							//create a term Container
							var termContainer = document.createElement('section');
							termContainer.setAttribute('class','glossaryTerm');
							termContainer.setAttribute('data-title',term.Title);
							termContainer.setAttribute('data-word',term.palabrasClaves);
							termContainer.setAttribute('data-letter',term.letraGlosario);
							termContainer.setAttribute('data-category',term.AreaNegocio.Label);
							//create title term
							var termTitle = document.createElement('label');
							termTitle.setAttribute('class','termTitle')
							var titleText = document.createTextNode(term.Title);
							termTitle.appendChild(titleText);
							termContainer.appendChild(termTitle);
							//create definition of term
							var termDefinition = document.createElement('article');
							termDefinition.setAttribute('class','termDefinition');
							var definitionContainer = document.createElement('p');
							var definitionText = document.createTextNode(term.Definicion);
							definitionContainer.appendChild(definitionText);
							termDefinition.appendChild(definitionContainer);
							termContainer.appendChild(termDefinition);
							//append term in glossary list							
							glossaryList.appendChild(termContainer);
						}
						glossary.getFilteredItems('start');
					}catch(e){
						throw e
					}
				}
			});
		}catch(e){
			console.log(String.format("Ocurrió un error en la ejecución del método glossary.getGlossaryTerms() {0}",e)) 
		}
	},
	getFilteredItems:function(ctrl){
		if(ctrl == 'start'){
			var ctrl = document.getElementById('todos');			
		}else{
			var activeControl = document.getElementsByClassName('active-letter')[0];
			activeControl.classList.remove('active-letter');
			activeControl.classList.add('letter');
		}
		ctrl.classList.remove('letter');
		ctrl.classList.add('active-letter');

		var limitPage = Number(document.getElementById('itemsBypage').value);
		var limit = limitPage;
		var counterTerms = 0;
		var page=1;
		var filteredTerms = [];
		var footerFormat="{0}{1}" ;
		//get all Classifieds
		var classifieds = document.getElementsByClassName('glossaryTerm');
		// get foilter values
		var businessArea = document.getElementById('businessAreaList').value;
		var businessCategory = document.getElementById('businessCategoryList').value;		
		switch (true){
			case (ctrl.tagName == 'LABEL'):
				var letterFilter = ctrl.innerText
				sessionStorage['letterFilter'] = letterFilter;
			break;
			case (sessionStorage['letterFilter'] != undefined):
				var letterFilter = sessionStorage['letterFilter'];
			break;
			case (sessionStorage['letterFilter'] == undefined):
				var letterFilter = 'Todos';
			break;			
		}
		if(document.getElementById('searchBox').value == ""){
			var searchFilter = 'Todos';
		}else{
			var searchFilter = document.getElementById('searchBox').value;
		}		
		try{			
			for(var i = 0;i<classifieds.length;i++){
					var classifiedValue = ({
					"title":classifieds[i].dataset.title.toLowerCase(),
					"word":classifieds[i].dataset.word.toLowerCase(),
					"category":classifieds[i].dataset.category,
					"letter":classifieds[i].dataset.letter.toLowerCase()
				});
				//filter by data title and word
				if(searchFilter != 'Todos'){
					var data = String.format('{0};{1}',classifiedValue.word,classifiedValue.title);
					if(data.indexOf(searchFilter.toLowerCase()) == -1){
						var hiddenBySearch = true;
					}else{
						var hiddenBySearch = false;
					}					
				}else{
					var hiddenBySearch = false;
				}
				//filter by category
				if(businessArea != 'Todos'){
					var data = JSON.parse(sessionStorage['businessArea'])[businessArea]; 
					if(businessCategory != 'Todos'){
						var hiddenByCategory = (businessCategory != classifiedValue.category);
					}else{
						var hiddenByCategory = glossary.hideOrShowTerm(classifiedValue.category,data);
					}					
				}else{
					var hiddenByCategory = false;
					//restart select category
					var optionsCategory = document.querySelectorAll('option[data-category]');
					for(var j=0;j<optionsCategory.length;j++){
						optionsCategory[j].style.display = 'none';
					}
					document.getElementById('businessCategoryList').value = 'Todos';
				}
				//filter by letter
				if(letterFilter != 'Todos'){
					var hiddenByLetter = (classifiedValue.letter != letterFilter.toLowerCase())
				}else{
					var hiddenByLetter = false;
				}
				//insert style to item
				switch(true){
					case (ctrl.id == 'searchBox' && hiddenBySearch):
						classifieds[i].removeAttribute('data-page');
						classifieds[i].style.display = 'none';						
					break;
					case (ctrl.id != 'searchBox' && (hiddenByCategory || hiddenByLetter)):
						classifieds[i].removeAttribute('data-page');
						classifieds[i].style.display = 'none';
					break;
					default:
						filteredTerms.push(classifieds[i]);
					break;
				}				
			}
			if(filteredTerms.length == 0){
				footerFormat = 'No se encontraron resultados para la búsqueda.'
			}
			//get page
			for (var i=0;i<filteredTerms.length;i++){
				switch (true){
					case i < limit && i != (filteredTerms.length-1):
						filteredTerms[i].setAttribute('data-page',page);
						break;
					case i == limit && i != (filteredTerms.length-1):
						var letterPage = String.format('<label class="paginator" onclick="glossary.goFromPage(this.innerText,this)">{0}</label>',page);
						footerFormat=String.format(footerFormat,letterPage,'{0}{1}');
						page +=1;
						limit +=limitPage;
						filteredTerms[i].setAttribute('data-page',page);
						break;
					case i == limit && i == (filteredTerms.length-1):
					    var letterPage = String.format('<label class="paginator" onclick="glossary.goFromPage(this.innerText,this)">{0}</label>',page);
						footerFormat=String.format(footerFormat,letterPage,'{0}{1}');
						page +=1;
						limit +=limitPage;
						filteredTerms[i].setAttribute('data-page',page);
						var letterPage = String.format('<label class="paginator" onclick="glossary.goFromPage(this.innerText,this)">{0}</label>',page);
						footerFormat=String.format(footerFormat,letterPage,'');
						break;
					case i < limit && i == (filteredTerms.length-1):
						filteredTerms[i].setAttribute('data-page',page);
						var letterPage = String.format('<label class="paginator" onclick="glossary.goFromPage(this.innerText,this)">{0}</label>',page);
						footerFormat=String.format(footerFormat,letterPage,'');
						break;
				}		
			}			
			document.getElementById('glossaryFooter').innerHTML = footerFormat;
			glossary.goFromPage(1);	
					


			
		}catch(e){
			console.log(String.format("Ocurrió un error en la ejecución del método glossary.getFilteredItems() {0}",e)) 
		}
				
	},
	goFromPage:function(page,ctrl){
		if(ctrl == undefined){
			var ctrl = document.getElementsByClassName('paginator')[0];			
		}else{
			var activeControl = document.getElementsByClassName('active-page')[0];
			activeControl.classList.remove('active-page');
			activeControl.classList.add('paginator');
		}
		ctrl.classList.remove('paginator');
		ctrl.classList.add('active-page');
				
		var termsSelected = document.querySelectorAll('[data-page]');
		for(var i=0;i<termsSelected.length;i++){
			if(termsSelected[i].getAttribute('data-page') == page){
				termsSelected[i].style.display='block';
				
				
			}else{
				termsSelected[i].style.display='none';				
			}
		}
	},
	hideOrShowTerm:function(value,data){
		var hidden = true;
		//search in inventory if category is content
	 	for(var i=0;i<data.length;i++){
	 		if(data[i] == value && hidden){
	 			hidden = false;
	 			return hidden;
			} 		
	 	}
	 	return hidden;		
	},
	filterOptionBybussinesArea:function(ctrl){
		//get category value
	 	var businessArea = ctrl.value;
		//get all items with tag data-city
		var items = document.querySelectorAll('[data-category]');
		//get data of location inventory in the object country
		var data = JSON.parse(sessionStorage['businessArea'])[businessArea];
		try{
			for (var i = 0;i<items.length;i++){
				var item = items[i];
				var category = item.attributes[1].nodeValue;
				if(item.tagName == 'OPTION'){
					//if country is Todos to show all items for category
					if(businessArea == 'Todos'){
						item.style.display = 'block';
					}else{
						//execute matchFunction to search cities for country
						var hidden =  glossary.hideOrShowTerm(category,data);
						if(hidden){
							item.style.display = 'none';
						}else{
							item.style.display = 'block';
						}
					}
				}					
			}
			//get filtered Items
			glossary.getFilteredItems(document.getElementById('businessAreaList'))
		}catch(e){
			console.log(
					String.format(
						"Ocurrió un error en la ejecución del método glossary.filterOptionBYbussynesArea {0}",e
					)
				)
		}		
		
	}
 }
