 //initialize before body load
 sputilities.callFunctionOnLoadBody('startHistory');
 
 function startHistory(){
	 sputilities.executeDelayspTaxonomy(classifiedsHistory.initialize)
 }
 
 var classifiedsHistory={
	 //create main parameters1
	 parameters:{
		containerId:"module-classifiedsInt",
		listName:"Clasificados",
		termIdUbicacion:"ec309af4-d95d-41ac-8586-af5b32ff65fc",
		termIdMoney:"8723ec42-c915-461d-822a-0b4becbb039b",
		historyFormatHTML:"<div class='classifiedsInt-controls'><div class='classifiedsInt-controls-country'>"
					+"<label>País</label><select id='countryList' onchange='classifiedsHistory.filterToCity(this)'>"
					+"<option value='Todos'>Todos</option></select></div><div class='classifiedsInt-controls-city'>"
					+"<label>Ciudad</label><select id='cityList' onchange='classifiedsHistory.filterToCity(this)'>"
					+"<option value='Todos'>Todas</option></select></div><div class='classifiedsInt-controls-search'>"
					+"<label>Busqueda</label><input type='text' placeholder='Busqueda...' id='searchBox'/>"
					+"</div><div class='classifiedsInt-controls-page'><label>Resultados por pagina:</label>"
					+"<select id='itemsBypage'onchange='classifiedsHistory.filterToCity(this)'><option value='1'>1</option>"
					+"<option value='3'>3</option><option value='6'>6</option></select></div></div>"
					+"<div id='classifiedsInt-list' class='classifiedsInt-list'></div><div id='historyFooter'></div>",
		classifiedInfo:"<h3>{0}</h3><p>{1}</p><h4>{2} - {3}</h4><p class='classifiedsInt-item-contact'>"
					+" {4} - <span>{5}</span></p>",				
		camlquery:"<View><Query>"
					+"<OrderBy><FieldRef Name='Created_x0020_Date' Ascending='FALSE'></FieldRef></OrderBy>"
					+"<Where><Eq><FieldRef Name='Categoria'/><Value Type='Lookup'>{0}</Value></Eq></Where>"
					+"</Query></View>"	 
	 },
	 initialize:function(){
		//insert histroyStructure
		document.getElementById(classifiedsHistory.parameters.containerId).innerHTML = classifiedsHistory.parameters.historyFormatHTML;
		//get list Containers and add event onchange
		var countryList = document.getElementById('countryList');		
		var cityList = document.getElementById('cityList');				
		//execute query country and city list
		classifiedsHistory.getSelectValues(classifiedsHistory.parameters.termIdUbicacion,countryList,cityList);
		//add event searchBox to enter key
		var searchBox = document.getElementById('searchBox')		
		searchBox.addEventListener('keypress', function (e) {			
			if (e.keyCode == 13) { 
				e.preventDefault();					
				classifiedsHistory.filterToCity()(searchBox)
			}			
		});
	 },
	getClassifiedsHistory:function(category,list){
		try{
			var camlQuery = String.format(classifiedsHistory.parameters.camlquery,category);
			//execute query to classified filter by category
			sputilities.getItemsByCaml({
				caml:camlQuery,
				listName:"Clasificados",
				success:function(items){
					var data = JSON.parse(items).d.results	
					var classifiedsContainer = document.getElementById('classifiedsInt-list');
					for(var i = 0;i<data.length;i++){
						var item = data[i];												
						//create classified container
						var classifiedContainer = document.createElement('div');
						classifiedContainer.setAttribute('class','classifiedsInt-item');
						classifiedContainer.setAttribute('data-city',item.Ubicacion.Label);
						//taking the url of the image
						var image = item.sys_txtImg;						
						var div = document.createElement('div');
						div.innerHTML = image;
						var urlImg = div.querySelector('img').src;
						//create image of classified
						var classifiedImg = document.createElement('img');
						classifiedImg.setAttribute('src',urlImg);
						classifiedContainer.appendChild(classifiedImg);
						//create information container
						var classifiedInfoContainer = document.createElement('div');
						classifiedInfoContainer.setAttribute('class','classifiedsInt-item-dates');
						//create Content
						var price = new Intl.NumberFormat('de-DE').format(item.Precio);
						var classifiedInfo = String.format(classifiedsHistory.parameters.classifiedInfo,item.Title,
										item.Descripcion,price,item.Moneda.Label,item.Contacto,item.TelefonoContacto);
						classifiedInfoContainer.innerHTML = classifiedInfo;
						classifiedContainer.appendChild(classifiedInfoContainer);
						classifiedsContainer.appendChild(classifiedContainer);
						classifiedsHistory.filterToCity()
					}
										
				},
				error:function(xhr){console.log(JSON.parse(xhr).error.message.value)}	
			});	
					
		}catch(e){
			console.log(
				String.format(
					"Ocurrió un error en la ejecución del método classifiedHistory.getClassifiedsHistory {0}",e
				)
			)
		}					 

	 },
	 getSelectValues:function(termSetId,countrylist,cityList){
		 try{
			 //cliar sessionStorage
		 	sessionStorage.clear();
			//call function for get terms of location
			sputilities.getTermsInTermSet({
			termSetId:termSetId,
			success:function(terms){
				var enumerator = terms.getEnumerator();
				var locationInventory = {};
				try{
					while (enumerator.moveNext()){
						var term = enumerator.get_current();
						//get pattern item
						var terms = term.get_pathOfTerm();
						var strTerms = terms.split(';');						
						//create object for location inventory
						if(locationInventory[strTerms[0]] == null){
								locationInventory[strTerms[0]] = []
						}
						if(strTerms.length == 2){
							//create option in select list
							var selectOption = document.createElement('option');
							selectOption.value = strTerms[1];
							locationInventory[strTerms[0]].push(strTerms[1]);							
							selectOption.setAttribute('data-city',strTerms[1]);
							selectOption.setAttribute('class','hidden');
							var textOption = document.createTextNode(strTerms[1]);
							selectOption.appendChild(textOption);
							cityList.appendChild(selectOption);
						}else{
							//create option in select list
							var selectOption = document.createElement('option');
							selectOption.value = strTerms[0];
							var textOption = document.createTextNode(strTerms[0]);
							selectOption.appendChild(textOption);
							countryList.appendChild(selectOption);
						}			
					}
					//insert in sessionStorage 
					sessionStorage["Paises"] = JSON.stringify(locationInventory);
					//execute query to classified history
					var classifiedList = document.getElementById('classifiedsList');
					var category = utilities.getQueryString('category');
					classifiedsHistory.getClassifiedsHistory(category,classifiedList);
				}catch(e){
					throw e
				}
			},
			error:function(data){
				console.log(JSON.Parse(data).error.message.value)
			}			
		});			 
		}catch(e){
			console.log(
					String.format(
						"Ocurrió un error en la ejecución del método classifiedHistory.getSelectValues {0}",e
					)
				)
		}		 
	 },	
	 filterToCity:function(){
		//inizilice filter
		var filteredItems = [];
		//get city select
		var city = document.getElementById('cityList').value;
		//get country select
		var country = document.getElementById('countryList').value;
		//get word or letter in search box
		var searchText = document.getElementById('searchBox').value;
		// ger all items for tag data-city
		var items = document.querySelectorAll('[data-city]');
		var data = JSON.parse(sessionStorage['Paises'])[country];	
		try{
			for (var i = 0;i<items.length;i++){
				var item = items[i];
				//get attribute value data-city
				var cityItem = item.attributes[1].value;	
				//execute march function to show the cities option for country select
				if(country != 'Todos'){
					var hidden =  classifiedsHistory.matchFunction(cityItem,data);
				}	
				//search text in element
				if(searchText != null){
					var itemText = item.outerHTML;
					itemText = itemText.toUpperCase();
					searchText = searchText.toUpperCase();
					if(itemText.indexOf(searchText) != -1){
						var searchResults = true;
					}else{
						var searchResults = false;
					}
				}else{
					var SearchResults = true;
				}
				switch (true){					
					case (item.tagName == 'OPTION' && country == 'Todos'):
						item.classList.remove('visible');
						item.classList.add('hidden');
					break;
					case (item.tagName == 'OPTION' && country != 'Todos' && hidden == true):
						item.classList.remove('visible');
						item.classList.add('hidden');
					break;
					case (item.tagName == 'OPTION' && country != 'Todos' && hidden == false):
						item.classList.remove('hidden');
						item.classList.add('visible');
					break;
					case (item.tagName == 'DIV' && searchResults == false):
						item.removeAttribute('data-page');
						item.classList.remove('visible');
						item.classList.add('hidden');
					break;					
					case (item.tagName == 'DIV' && country != 'Todos' && city == 'Todos' && hidden == true):
						item.removeAttribute('data-page');
						item.classList.remove('visible');
						item.classList.add('hidden');						
					break;										
					case (item.tagName == 'DIV' && country != 'Todos' && city != cityItem && city != 'Todos'):
						item.removeAttribute('data-page');
						item.classList.remove('visible');
						item.classList.add('hidden');						
					break;
					default:
						filteredItems.push(item);
					break;
				}
			}
			classifiedsHistory.getFilteredItems(filteredItems);									
		}catch(e){
			console.log(
					String.format(
						"Ocurrió un error en la ejecución del método classifiedHistory.filterToCity {0}",e
					)
				)
		}
	 },
	 matchFunction:function(city,data){
	 	var hidden = true;
		//search in inventory if city is content
	 	for(var i=0;i<data.length;i++){
	 		if(data[i] == city && hidden){
	 			hidden = false;
	 			return hidden;
	 		} 		
	 	}
	 	return hidden;
	 }, 
	 getFilteredItems:function(filteredItems){		 
		//get values of filter
		var limitPage = Number(document.getElementById('itemsBypage').value);
		var limit = limitPage;
		if(filteredItems.length == 0){
			var footerFormat="No se encontraron Resultados" ;
			var paginator = document.getElementById('historyFooter');
		}else{
			var footerFormat="{0}{1}" ;
			//create msg de elementos en la pagina
			var paginator = document.getElementById('historyFooter');
			var msgResults = document.createElement('div');
			if(filteredItems.length > 1){
				var msgText = document.createTextNode(String.format('Se encontraron {0} clasificados',filteredItems.length));
			}else{
				var msgText = document.createTextNode(String.format('Se encontro {0} clasificado',filteredItems.length));
			}
			msgResults.appendChild(msgText);			
		}			
		var page=1;
		//start Filter by year
		try{
			//get page
			for (var i=0;i<filteredItems.length;i++){
				switch (true){
					case i < limit && i != (filteredItems.length-1):
						filteredItems[i].setAttribute('data-page',page);
						break;
					case i == limit && i != (filteredItems.length-1):
						var letterPage = String.format('<label class="paginator" onclick="classifiedsHistory.goFromPage(this.innerText,this)">{0}</label>',page);
						footerFormat=String.format(footerFormat,letterPage,'{0}{1}');
						page +=1;
						limit +=limitPage;
						filteredItems[i].setAttribute('data-page',page);
						break;
					case i == limit && i == (filteredItems.length-1):
					    var letterPage = String.format('<label class="paginator" onclick="classifiedsHistory.goFromPage(this.innerText,this)">{0}</label>',page);
						footerFormat=String.format(footerFormat,letterPage,'{0}{1}');
						page +=1;
						limit +=limitPage;
						filteredItems[i].setAttribute('data-page',page);
						var letterPage = String.format('<label class="paginator" onclick="classifiedsHistory.goFromPage(this.innerText,this)">{0}</label>',page);
						footerFormat=String.format(footerFormat,letterPage,'');
						break;
					case i < limit && i == (filteredItems.length-1):
						filteredItems[i].setAttribute('data-page',page);
						var letterPage = String.format('<label class="paginator" onclick="classifiedsHistory.goFromPage(this.innerText,this)">{0}</label>',page);
						footerFormat=String.format(footerFormat,letterPage,'');
						break;
				}		
			}			
			paginator.innerHTML = footerFormat;
			paginator.appendChild(msgResults);
			
				
			
			
			
			classifiedsHistory.goFromPage(1);

		}catch(e){
			console.log(String.format('ah ocurrido un error en ejecucion del metodo classifiedsHistory.getFilteredItems {0}',e))	
		}
	 },
	 goFromPage:function(page,ctrl){
	 if(ctrl == undefined){
			var ctrl = document.getElementsByClassName('paginator')[0];			
		}else{
			var activeControl = document.getElementsByClassName('active-page')[0];
			activeControl.classList.remove('active-page');
			activeControl.classList.add('paginator');
		}
		ctrl.classList.remove('paginator');
		ctrl.classList.add('active-page');

	 
	 
	 
		var itemsSelected = document.querySelectorAll('[data-page]');
		for(var i=0;i<itemsSelected.length;i++){
			if(itemsSelected[i].getAttribute('data-page') == page){
				itemsSelected[i].classList.remove('hidden');
				itemsSelected[i].classList.add('visible');
			}else{
				itemsSelected[i].classList.remove('visible');
				itemsSelected[i].classList.add('hidden');			}

		}
	}
 }