﻿function inicioPageLayout()
{
	marquee.initialize();
	staticNews.initialize();
	twitter.initialize();
	sliderNews.initialize();
	economIndic.initialize();
	imageDay.initialize();	
	videoPoster.initialize();	
	benefits.initialize();
	clientsMedia.initialize();
	clientsText.initialize();
	campaignTabs.initialize();	
	important.initialize();
	graphicsImg.initialize();
	offerDay.initialize();
	regional.initialize();
	solidarity.initialize();
	businessUnits.initialize();
	sliderImg.initialize();
	events.initialize();	
	quickAccess.initialize();
	classifiedsSlider.initialize();
}

var pageLayout =
{
	components:[],
	max:21

}
/*Component: Noticias Estaticas */

var staticNews = {
	//create parameters
	parameters:{
			listName: "Noticias Estáticas",
			top: 3,
			divID:"static-news",
			containerNews:"<div class='otherNews-item'>{0}{1}</div>",			
			imageNews:"<div class='otherNews-item-image-container'><a href='{2}' target='_blank'><img src='{0}' alt='Avatar' class='otherNews-item-image-image'/>" +
					  "<div class='otherNews-item-image-overlay'><div class='otherNews-item-image-text'>" +
					  "<img src='{1}/SiteAssets/Development/images/components/news/avion-hover.png'/></div></div></a></div>",
			textNews:"<div class='otherNews-item-text-container'><h3>{0}</h3><p>{1}</p><a href='{2}' target='_blank'>Ver más<i class='inc-arrow-3 centrado-y'></i	></a></div>",
			query: "?$select=Title,FechaFinal,FechaInicio,Url,sys_txtImg,Descripcion,Orden&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}')&$orderby=Orden&$top={1}",
			resultItems:null		
	
	},
	initialize:function(){
		try {

			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	
			
			//add current date to query and filter
			var linkQuery = String.format(staticNews.parameters.query,date,staticNews.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(staticNews.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				staticNews.parameters.resultItems = resultItems;
				pageLayout.components.push(staticNews);
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var data = staticNews.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];
			
			//retrieving the fields of the results
			var title = currentItem.Title;
			var description = currentItem.Descripcion;			
			var url = currentItem.Url.Url;
				
			//taking the url of the image
			var image = currentItem.sys_txtImg;
			var div = document.createElement('div');
			div.innerHTML = image;
			var urlimg = div.querySelector('img').src;			
					
			//make html with the fields 
			var imageHtml = String.format(staticNews.parameters.imageNews, urlimg,sputilities.contextInfo().webAbsoluteUrl, url);
			var textHtml = String.format(staticNews.parameters.textNews,title,description,url);			
					
			//adding html 
			html += String.format(staticNews.parameters.containerNews,imageHtml,textHtml);			
					
		}
			
		//insert the html code
		document.getElementById(staticNews.parameters.divID).innerHTML = html;
	}
}

/* Component: Imagen del día */

var imageDay = {
	//create parameters
	parameters:{
			listName: "Contenido Estático",
			top: 1,
			divIDimg:"image-day",
			seccion:"ImagenDelDia",
			detailUrl:"{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo=ImagenDia",
			modal:'sputilities.showModalDialog("Imagen del Día","{0}",window.location.href)',
			bodyImg:"<a href='javascript:void(0);' onclick='{1}'><img src='{0}' class='img-responsive'></a>",
			query: "?$select=*,FechaInicio,FechaFinal&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}' and {1} eq 1)&$orderby=FechaFinal&$top={2}",
			resultItems:null
	
	},	
	initialize:function(){
		try {
		
			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	
			
			//add current date to query and filter
			var linkQuery = String.format(imageDay.parameters.query,date,imageDay.parameters.seccion,imageDay.parameters.top);

			//running the query with parameters
			sputilities.getItems(imageDay.parameters.listName,linkQuery,function(data){				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				imageDay.parameters.resultItems = resultItems;
				pageLayout.components.push(imageDay);
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML: function(){		
		var html = "";
		var data = imageDay.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];

			//taking the url of the image
			var image = currentItem.sys_txtImg;
			var div = document.createElement('div');
			div.innerHTML = image;
			var urlimg = div.querySelector('img').src;
				
			//retrieving the fields of the results
			var descripcion = currentItem.Descripcion;
				
			//make html and detail popup
			var urlDetail = String.format(imageDay.parameters.detailUrl,sputilities.contextInfo().webAbsoluteUrl,imageDay.parameters.listName,currentItem.ID);
			var modal = String.format(imageDay.parameters.modal, urlDetail);
						
			//adding values of parameters to the html variable
			html += String.format(imageDay.parameters.bodyImg,urlimg,modal,sputilities.contextInfo().webAbsoluteUrl);					
		}
			
		//insert the html code
		document.getElementById(imageDay.parameters.divIDimg).innerHTML = html;
	}
}

/* Components: marquee */
var marquee = {
//create parameters
	parameters:{
		containerId:"notifications",
		listName: "Marquesina",
		top:3,
		query: "?$select=Title,Url_vinculo&$orderby=Created_x0020_Date&$top={0}",
		containerMarquee:"<div class='swiper-wrapper variable-width2'>{0}</div>",
		contentMarquee:"<div class='swiper-slide text-center text-uppercase'><a href='{0}' target='_blank'><span class='txt-notificacion'><i class='inc-info centrado-y'></i>{1}</span></a></div>",
		resultItems:null
		
	},
	initialize:function(){
		//insert row limit to query 
		marquee.parameters.query = String.format(marquee.parameters.query,marquee.parameters.top)
		try{
			sputilities.getItems(marquee.parameters.listName,marquee.parameters.query,function(data){
				var results = JSON.parse(data).d.results;
				marquee.parameters.resultItems = results;
				pageLayout.components.push(marquee);
			
			},function(data){
				console.log(JSON.parse(data).error.message.value)
			});
		}catch(e){
			console.log(String.format("Ocurrió un error inesperado en la ejecución de método marquee.initialize {0}",e))
		}
	},
	insertHTML:function(){
		var html = "";
		
		var data = marquee.parameters.resultItems;
						
			for (var i = 0;i < data.length;i++){
				var currentItem = data[i];
				
				// get field by items
				var title = currentItem.Title;
				var url = currentItem.Url_vinculo.Url;
				
				var bodyHtml = String.format(marquee.parameters.contentMarquee,url,title);
				
				html += bodyHtml;				
			}
 
		var container = String.format(marquee.parameters.containerMarquee, html);
		document.getElementById(marquee.parameters.containerId).innerHTML = container;
	}
}

/* Component: Marquesina Cifras */

var economIndic = {
	//create parameters
	parameters:{
			listName: "Indicadores Económicos",
			top: 5,
			divID:"indicators",
			change:{
					up: "<span class='txt-indicador text-center indicador-subio'><i class='inc-arrow-5 centrado-y'></i>{0}</span>",
					down: "<span class='txt-indicador text-center indicador-bajo'><i class='inc-arrow-6 centrado-y'></i>{0}</span>",
					stable: "<span class='txt-indicador text-center indicador-estable'><i class='inc-arrow-7 centrado-y'></i>{0}</span>",
					},			
			containerIndic:" <div class='swiper-slide centrar-slide'>{0}</div>",
			body:"<h1>{0}</h1><h2>{1}&nbsp;{2}</h2>",
			query: "?$select=Title,AntiguoValor,NuevoValor,Moneda,Orden&$orderby=Orden&$top={0}",
			resultItems:null
	
	},	
	initialize:function(){
		try {
					
			//add current date to query and filter
			var linkQuery = String.format(economIndic.parameters.query,economIndic.parameters.top);

			//running the query with parameters
			sputilities.getItems(economIndic.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				economIndic.parameters.resultItems = resultItems;
				pageLayout.components.push(economIndic);

			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var data = economIndic.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];

			//retrieving the fields of the results
			var title = currentItem.Title;
			var oldValue = currentItem.AntiguoValor;
			var newValue = currentItem.NuevoValor;
			var coin = currentItem.Moneda;
			var change="";
			
			//make html 
			var bodyHtml = String.format(economIndic.parameters.body,title,coin,newValue);
						
			if(oldValue > newValue){
				change = String.format(economIndic.parameters.change.down,bodyHtml);
			}
			else if (oldValue < newValue){
				change = String.format(economIndic.parameters.change.up,bodyHtml);
			}
			else {
				change = String.format(economIndic.parameters.change.stable,bodyHtml);
			}
						
			//adding values to the parameters
			var bodyNew = String.format(economIndic.parameters.containerIndic,change);	
				
			//adding values of parameters to the html variable
			html += bodyNew;
					
		}
			
		//insert the html code

		document.getElementById(economIndic.parameters.divID).innerHTML = html;
	}
}

/* Component: Eventos */

var events = {
//create parameters
	parameters:{
		listName: "Eventos",
		containerId:"items-events",	
		moreEventsId:"more-events",
		top: 6,	
		mounth:["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC"],
		query: "?$select=ID,Title,Description,EventDate,EndDate,fAllDayEvent&$filter=EndDate ge datetime'{0}'&$orderby=EventDate&$top={1}",
		containerEven:"<div class='item-events'>{0}{1}</div>",
		textEvent:"<div class='item-events-text'><h2>{0}</h2><p>{1}</p></div>"+
				  "<div class='item-events-date'><h3>{2}</h3><span>{3}</span></div>"+
				  "<div class='item-events-hour'><h3>{4}</h3><span>{5}</span></div>",
		linkEvent:"<div class='item-events-text-arrow' onclick='{0}'><a href='javascript:void(0);'><i class='inc-arrow-3 centrado-y'></i></a></div>",
		modal:'javascript:sputilities.showModalDialog("Evento","{0}",window.location.href);',
		listUrl:"{0}/Lists/{1}",
		dispForm:"{0}/DispForm.aspx?ID={1}",	
		detailUrl:"{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo=Eventos",
		linkAll:"<a href='{0}'>Ver todos <i class='inc-arrow-3 centrado-y'></i></a>",		
		resultItems:null	
	},
	initialize:function(){
		//Get current date value
		var today = new Date;
		today = today.toISOString();
		//We build the query			
		events.parameters.query = String.format(events.parameters.query,today,events.parameters.top)
		try{
			sputilities.getItems(events.parameters.listName,events.parameters.query,function(data){
			//We get the url from the list of events
				
				var results = JSON.parse(data).d.results;
				events.parameters.resultItems = results;
				pageLayout.components.push(events);
					
						
					
			},function(data){
				console.log(JSON.parse(data).error.message.value)
			});		
		}catch(e){			
			console.log(String.format('Ocurrio un error inesperado en el metodo events.initialize {0}',e))
		}				
				
	},
	insertHTML: function(){
		var data = events.parameters.resultItems;
		var html = "";
		var listUrl = String.format(events.parameters.listUrl,sputilities.contextInfo().webAbsoluteUrl,events.parameters.listName)	;	
						//We construct the section of the event for each event in the result set
						for (var i = 0;i<data.length;i++){
							
							var form = String.format(events.parameters.dispForm,listUrl,data[i].ID);
							
							var startDateTime = new Date(data[i].EventDate);
							var endDateTime = new Date(data[i].EndDate);							
							var startDay =('0' +  startDateTime.getUTCDate()).slice(-2);
							var startMonth = events.parameters.mounth[startDateTime.getMonth()];
							
							var eventTime = "";
							var eventAMPM="";
							if(data[i].fAllDayEvent == 0){ 
							var startHour="";
							var startMinutes= "";
								if(startDateTime.getHours() > 12)
								{
									var hour = startDateTime.getHours() - 12;
									startHour = ('0' + hour).slice(-2);
									startMinutes = ('0' + startDateTime.getMinutes()).slice(-2);
									eventAMPM="PM";
								}
								else
								{
									startHour = ('0' + startDateTime.getHours()).slice(-2);
									startMinutes = ('0' + startDateTime.getMinutes()).slice(-2);
									eventAMPM="AM";
								}
								
								eventTime = String.format("{0}:{1}",startHour,startMinutes);
							
							}else{
								eventTime = "el día";
								eventAMPM= "Todo";
							}
							
							var detailURL = String.format(events.parameters.detailUrl,sputilities.contextInfo().webAbsoluteUrl,events.parameters.listName,data[i].ID);
							var htmlModal = String.format(events.parameters.modal,detailURL);
							var htmlLink = String.format(events.parameters.linkEvent,htmlModal);
							var htmlBody = String.format(events.parameters.textEvent,data[i].Title,data[i].Description,startMonth,startDay,eventAMPM,eventTime);
							
							html += String.format(events.parameters.containerEven,htmlBody,htmlLink);
									
						}
							
						document.getElementById(events.parameters.containerId).innerHTML = html;
						var htmlAll = String.format(events.parameters.linkAll,listUrl);
						document.getElementById(events.parameters.moreEventsId).innerHTML=htmlAll ;
					
	}

}

/* Components: Beneficios */

var benefits = {
	//create parameters
	parameters:{
			listName: "Contenido Estático",
			top: 1,
			divIDimg:"benefit-day",
			seccion:"Beneficio",			
			bodyImg:"<img src='{0}' class='img-responsive'><a href='{1}' target='_blank' class='btn-campaing'>Ver beneficio</a>",
			query: "?$select=*,FechaInicio,FechaFinal&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}' and {1} eq 1)&$orderby=FechaFinal&$top={2}",
			resultItems:null
	
	},	
	initialize:function(){
		try {
		
			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	
			
			//add current date to query and filter
			var linkQuery = String.format(benefits.parameters.query,date,benefits.parameters.seccion,benefits.parameters.top);

			//running the query with parameters
			sputilities.getItems(benefits.parameters.listName,linkQuery,function(data){				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				benefits.parameters.resultItems = resultItems;
				pageLayout.components.push(benefits);
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML: function(){		
		var html = "";
		var data = benefits.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];

			//taking the url of the image
			var image = currentItem.sys_txtImg;
			var div = document.createElement('div');
			div.innerHTML = image;
			var urlimg = div.querySelector('img').src;
				
			//retrieving the fields of the results
			var url = currentItem.Url.Url;
												
			//adding values of parameters to the html variable
			html += String.format(benefits.parameters.bodyImg,urlimg,url);					
		}
			
		//insert the html code
		document.getElementById(benefits.parameters.divIDimg).innerHTML = html;
	}
}

/* Component: Destacados */
important = {
	//create parameters
	parameters:{
			listName: "Contenido Estático",
			top: 1,
			divIDimg:"information-important",
			seccion:"Destacado",			
			bodyImg:"<div class='important'><a href='{0}' target='_blank'><img src='{1}' class='img-responsive'></a><h5>Destacados</h5>",
			query: "?$select=*,FechaInicio,FechaFinal&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}' and {1} eq 1)&$orderby=FechaFinal&$top={2}",
			resultItems:null
	
	},	
	initialize:function(){
		try {
		
			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	
			
			//add current date to query and filter
			var linkQuery = String.format(important.parameters.query,date,important.parameters.seccion,important.parameters.top);

			//running the query with parameters
			sputilities.getItems(important.parameters.listName,linkQuery,function(data){				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				important.parameters.resultItems = resultItems;
				pageLayout.components.push(important);
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML: function(){		
		var html = "";
		var data = important.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];

			//taking the url of the image
			var image = currentItem.sys_txtImg;
			var div = document.createElement('div');
			div.innerHTML = image;
			var urlimg = div.querySelector('img').src;
				
			//retrieving the fields of the results
			var url = currentItem.Url.Url;
												
			//adding values of parameters to the html variable
			html += String.format(important.parameters.bodyImg,url,urlimg);					
		}
			
		//insert the html code
		document.getElementById(important.parameters.divIDimg).innerHTML = html;
	}
}

/* Components: Slider Cifras */

var graphicsImg = {
	//create parameters
	parameters:{
			listName:"Cifras",
			top: 6,
			divID:"information-numbers",
			bodyTab:"<div class='important'><img src='{0}' class='img-responsive' /><h5>{1}</h5></div>",
			query: "?$select=sys_txtImg,Orden,Title&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}')&$orderby=Orden&$top={1}",
			resultItems:null
	
	},
	
	initialize:function(){
		try {
		
			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	

			//adding parameters to query
			var linkQuery = String.format(graphicsImg.parameters.query,date,graphicsImg.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(graphicsImg.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				
				graphicsImg.parameters.resultItems = resultItems;
				pageLayout.components.push(graphicsImg);
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var data = graphicsImg.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];
			//retrieving the fields of the results
			var title =	currentItem.Title;
			//taking the url of the image
			var image = currentItem.sys_txtImg;
			var div = document.createElement('div');
			div.innerHTML = image;
			var urlimg = div.querySelector('img').src;
							
			//adding values to the parameters
			var bodyNew = String.format(graphicsImg.parameters.bodyTab,urlimg, title);	
					
			//adding values of parameters to the html variable
			html += bodyNew;
					
		}
			
		//insert the html code
		document.getElementById(graphicsImg.parameters.divID).innerHTML = html;
	}
}

var offerDay = {
	//create parameters
	parameters:{
			listName:"Ofertas del día",
			top: 6,
			divID:"information-offers",
			linkImg:'openBasicDialog("{0}","{1}")',
			linkOffer: "<a href='javascript:void(0);' onclick='{0}'>",
			bodyOffer:"<div class='important'><a href='{0}' target='_blank'><img src='{1}' class='img-responsive' /></a><h5>{2}</h5></div>",
			query: "?$select=Title,sys_txtImg,Orden,FechaInicio,FechaFinal,CategoriaOferta,Url&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}')&$orderby=Orden&$top={1}",
			resultItems:null
	
	},
	initialize:function(){
		try {
		
			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	

			//adding parameters to query
			var linkQuery = String.format(offerDay.parameters.query,date,offerDay.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(offerDay.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				offerDay.parameters.resultItems = resultItems;
				pageLayout.components.push(offerDay);

				
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var data = offerDay.parameters.resultItems;
		//loop the array
				for (var i=0; i< data.length; i++){
				
				var currentItem = data[i];
				
				//retrieving the fields of the results
				var titleOffer = currentItem.Title;
				var categoryOffer = currentItem.CategoriaOferta;
				var url = currentItem.Url.Url;
				//taking the url of the image
				var image = currentItem.sys_txtImg;
				var div = document.createElement('div');
				div.innerHTML = image;
				var urlimg = div.querySelector('img').src;
				
				//adding values to the parameters
				var bodyHtml = String.format(offerDay.parameters.bodyOffer,url,urlimg,titleOffer);
										
				//adding values of parameters to the html variable
				html += bodyHtml;					
			}
			
			//insert the html code
			document.getElementById(offerDay.parameters.divID).innerHTML = html;
	}
}

/* Component: Slider Noticias */
var sliderNews = {
	//create parameters
	parameters:{
			listName: "Páginas",
			top: 7,
			divID:"carousel-header",
			detailParameter: "Noticias",
			containerIndi:" <ol class='carousel-indicators'>{0}</ol>",
			bodyIndicators:" <li data-target='#carousel-header' data-slide-to='{0}'{1}></li>",
			containerSlides:" <div class='carousel-inner slider' role='listbox'>{0}</div>",
			bodyItemSlide:"<div class='item{0}' id='slide-0{1}'> <img src='{2}' alt='...'><div class='carousel-caption'><h2>{3}</h2><p>{4}</p><a href='{5}' target='_self' class='btn light-red-button'>Más información</a></div></div>",
			detailUrl:"{0}?ListaBase={1}&IDItem={2}&Tipo=Noticias",
			modal:'sputilities.showModalDialog("Noticia","{0}",window.location.href)',
			query: "?$select=FileRef,Title,ArticleStartDate,Entradilla,PublishingPageContent,ID,FechaInicio,FechaFinal,Orden,sys_txtImg&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}' and ContentType eq 'Noticias')&$orderby=Orden&$top={1}",
			resultItems:null
	
	},
	
	initialize:function(){
		try {
		
		
			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	
			
			//adding parameters to query
			var linkQuery = String.format(sliderNews.parameters.query,date,sliderNews.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(sliderNews.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;

				sliderNews.parameters.resultItems = resultItems;
				pageLayout.components.push(sliderNews);			
				

			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html = "";
		var htmlIndicators="";
		var htmlSliders="";
		var data= 	sliderNews.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
				
			var currentItem = data[i];
				if(currentItem.sys_txtImg != null)
				{
					//retrieving the fields of the results
					var title = currentItem.Title;
					var dateArticle = currentItem.ArticleStartDate;
					var description = currentItem.Entradilla;
					var content = currentItem.PublishingPageContent;
					var id = currentItem.ID;
					var startDate = currentItem.FechaInicio;
					var finalDate = currentItem.FechaFinal;
						
					//taking the url of the image
					var image = currentItem.sys_txtImg;
					var div = document.createElement('div');
					div.innerHTML = image;
					var urlimg = div.querySelector('img').src;
					
					//make html and detail popup
					//var urlDetail = String.format(sliderNews.parameters.detailUrl,sputilities.contextInfo().webAbsoluteUrl,sliderNews.parameters.listName,currentItem.ID);
					//var modal = String.format(sliderNews.parameters.modal, urlDetail);
					
					var urlPage= String.format(sliderNews.parameters.detailUrl,currentItem.FileRef,sliderNews.parameters.listName,currentItem.ID);
					//adding values to the parameters
					if(i==0)
					{
						htmlIndicators += String.format(sliderNews.parameters.bodyIndicators,i," class='active'");
						htmlSliders += String.format(sliderNews.parameters.bodyItemSlide," active",i,urlimg,title,description,urlPage);
					}
					else
					{
						htmlIndicators += String.format(sliderNews.parameters.bodyIndicators,i,"");
						htmlSliders += String.format(sliderNews.parameters.bodyItemSlide,"",i,urlimg,title,description,urlPage);
		
					}
			}
					
		}
		//adding values of parameters to the html variable
			html += String.format(sliderNews.parameters.containerIndi,htmlIndicators);
			html += String.format(sliderNews.parameters.containerSlides,htmlSliders);
			
		//insert the html code
		document.getElementById(sliderNews.parameters.divID).innerHTML = html;
	}
}

/* Components: Videos Home */
var videoPoster = {
	parameters:{
		containerId:"video-day",
		top:1,
		listName:"Videos Intranet",
		imgDefault:"{0}/Style%20Library/Media%20Player/VideoPreview_Default.png",
		caml:"<View Scope='Recursive'><RowLimit>10</RowLimit><ViewFields><FieldRef Name='Title' /><FieldRef Name='FileDirRef' /><FieldRef Name='Activo' /><FieldRef Name='ID' /><FieldRef Name='ContentType' />"+
			"<FieldRef Name='FileRef' /><FieldRef Name='Created' /></ViewFields><Query><Where><Contains><FieldRef Name='FileDirRef' />"+
			"<Value Type='Text'>{0}</Value></Contains></Where></Query></View>",
		query:"?$filter=(Activo eq 1)&$select=*,Activo,ID,FileDirRef,ContentType,FileRef,Created&$expand=ContentType&$orderby=Created desc&$top={0}",
		queryContent:"?$select=*,Activo,ID,FileDirRef,ContentType/Name,FileRef,Created",		
		bodyPoster:"<a href='javascript:void(0);' onclick='{0}' class='circulo bg-play centrado-total'><i class='inc-play centrado-total'></i></a>",
		bodyImg:"<img src='{0}' class='img-responsive'>",
		detailUrl:"{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo=Videos",
		modal:'sputilities.showModalDialog("Video","{0}",window.location.href)',
		resultItems:null

	},
	initialize:function(){
		
		var query = String.format(videoPoster.parameters.query,videoPoster.parameters.top);
		try{
			sputilities.getItems(videoPoster.parameters.listName,query,function(data){
				var posters = JSON.parse(data).d.results;
				
				var filter = posters[0].FileRef.substring(1);
				var queryNew = String.format(videoPoster.parameters.caml,filter);
				
				sputilities.getItemsByCaml({caml:queryNew,listName:videoPoster.parameters.listName,
					success:function(dataContent){				
	
						videoPoster.parameters.resultItems= JSON.parse(dataContent).d.results;						
						pageLayout.components.push(videoPoster);
						
					},error:function(data){
					console.log(JSON.parse(data).error.message.value)
					},
					query: videoPoster.parameters.queryContent
				});					
					
				
			},function(data){
				console.log(JSON.parse(data).error.message.value)
			});	
		}
		catch(e){
					console.log(String.format("Ocurrió un error inesperado en la ejecución de método videoPoster.initialize {0}",e))
				}	
	},
	insertHTML:function(){
		var htmlImg="";
		var htmlBody="";
		var html="";
		var data=videoPoster.parameters.resultItems;
		for(var i = 0;i < data.length;i++){
				if(data[i].FileDirRef.indexOf("/Preview Images") > 0){
				    var img = String.format("{0}{1}",sputilities.contextInfo().webServerRelativeUrl,data[i].FileRef);					
					htmlImg = String.format(videoPoster.parameters.bodyImg,data[i].FileRef);
				}
				else
				{
					//make html and detail popup
					var urlDetail = String.format(videoPoster.parameters.detailUrl,sputilities.contextInfo().webAbsoluteUrl,videoPoster.parameters.listName,data[i].ID);
					var modal = String.format(videoPoster.parameters.modal, urlDetail);
					htmlBody = String.format(videoPoster.parameters.bodyPoster,modal);

					if(data.length == 1)
					{
						var img = String.format(videoPoster.parameters.imgDefault,sputilities.contextInfo().webAbsoluteUrl);
						htmlImg = String.format(videoPoster.parameters.bodyImg,img);

					}	
				}
		}
		html +=htmlImg;
		html +=htmlBody;
		
		document.getElementById(videoPoster.parameters.containerId).innerHTML = html;					
	}
}
/* Commponents: Tabs Campañas */

var campaignTabs = {
	//create parameters
	parameters:{
			listName: "Tabs Campañas",
			top: 5,
			divID:"links-campaing",
			link:"<a href='{0}' target='_blank'>",
			containerLinks:"<ul>{0}</ul>",
			bodyLinks:"<li><a href='{0}' target='_blank'><img src='{1}'><h1>{2}</h1><h2>{3}</h2></a></li>",
			bodyTab:"<img src='{0}' /><h3>{1}</h3></a>",
			query: "?$select=Title,Url,sys_txtImg,Orden,Descripcion&$orderby=Orden&$top={0}",
			resultItems:null
	
	},
	
	initialize:function(){
		try {
								
			//adding parameters to query
			var linkQuery = String.format(campaignTabs.parameters.query,campaignTabs.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(campaignTabs.parameters.listName,linkQuery,function(data){				
				//save the results to an array
				campaignTabs.parameters.resultItems = JSON.parse(data).d.results;
				pageLayout.components.push(campaignTabs);
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	}, 
	insertHTML:function()
	{
		var html="";
		var data= campaignTabs.parameters.resultItems;
		for (var i=0; i< data.length; i++){
				
				var currentItem = data[i];
				//retrieving the fields of the results
				var title = currentItem.Title;
				var url = currentItem.Url.Url;
				var description = currentItem.Descripcion;
				
				//taking the url of the image
				var image = currentItem.sys_txtImg;
				var div = document.createElement('div');
				div.innerHTML = image;
				var urlimg = div.querySelector('img').src;					
				//adding values of parameters to the html variable
				html += String.format(campaignTabs.parameters.bodyLinks,url,urlimg,title,description);
			}
			
			var htmlContent = String.format(campaignTabs.parameters.containerLinks,html);
			//insert the html code
			document.getElementById(campaignTabs.parameters.divID).innerHTML = htmlContent;
	}
}

/* Component: Slider de Clasificados */
var classifiedsSlider={
	//create parameters
	parameters:{
		containerId:"classifieds-items",
		listName:"Categorías Clasificados",	
		formatNewForm:"{0}/Lists/Clasificados/NewForm.aspx",	
		urlDetalle:"{0}/Paginas/HistoricoClasificados.aspx?category={1}",
		tagSlideContainer:"section",
		tagItemContainer:"div",
		classItemContainer:"classifieds-apps-item",
		slideFormatHTML:'<div class="classifieds-apps-item">'
							+ '<a onclick="javascript:classifiedsSlider.change(this)">'
								+ '<img src="{0}">'
								+ '<h4 class="">{1}</h4>'
								+ '<div>'
								+ 	'<h3>{2}</h3>'
								+ 	"<p>{3}<span onclick='javascript:window.open(\"{4}\", \"_blank\");'>ver más</span></p>"
								+ 	"<label onclick='javascript:sputilities.showModalDialog(\"Nuevo Clasificado\",\"{5}\",window.location.href)'>Publica tu clasificado</label>"
								+ '</div>'
							+ '</a>'
						+'</div>',
		resultItems:null,
		descTooltipSelector:".classifieds-apps-tooltip"
	},
	initialize:function(){
		//execute the query of all the items
		sputilities.getItems(classifiedsSlider.parameters.listName,'',function(data){
			classifiedsSlider.parameters.resultItems = JSON.parse(data).d.results;
			pageLayout.components.push(classifiedsSlider);
		},function(data){
			console.log(JSON.parse(data).error.message.value)
		})
		
	},
	insertHTML:function(){
		var items = classifiedsSlider.parameters.resultItems;
		//create the container of the slider
		var slideContainer = "";
		try{                                                                  
			for(var i=0;i<items.length;i++){
				//create a item container for each item in list
				var url = String.format(classifiedsSlider.parameters.urlDetalle,sputilities.contextInfo().webAbsoluteUrl,items[i].Title);
				var urlDialog = String.format(classifiedsSlider.parameters.formatNewForm,sputilities.contextInfo().webAbsoluteUrl);
				//build html code and insert in item container
				var html = String.format(classifiedsSlider.parameters.slideFormatHTML,items[i].Icono.Url,(i+1),items[i].Title, items[i].Descripcion,url,urlDialog);
				slideContainer += html;
			}
			document.getElementById(classifiedsSlider.parameters.containerId).innerHTML = slideContainer;
			var ctrl = document.getElementById(classifiedsSlider.parameters.containerId).querySelector(String.format('div.{0}:first-child a',classifiedsSlider.parameters.classItemContainer))
			classifiedsSlider.change(ctrl)
		}
		catch(e){
			console.log(String.format("Ocurrió un error en la ejecución del método classifiedsSlider.initialize(){0}",e));
		}

	},
	change:function(ctrl){
		var desc = ctrl.querySelector('div').innerHTML;
		document.querySelector(classifiedsSlider.parameters.descTooltipSelector).innerHTML = desc;
	}

}
/* Components: Regionales y Avianca Solidaria */

var regional={
	 //create the initial parameters
	 parameters:{
		listName:"Regionales",
		containerId:"information-regional",
		bodyHtml:"<img src='{0}/SiteAssets/Development/images/components/regional/1-regional.jpg'><div class='regional-item-content'><h3>{1}</h3><a href='{2}' target='_blank' class='btn-regional'>Más información</a></div>",
		query:"?select=*,TipoContenido,UrlImagen&$filter=(Vencimiento ge datetime'{0}' and TipoContenido eq 'Regionales')&$orderby=Orden desc&$top=1",
		resultItems:null
		
	 },
	 initialize:function(){
		//calculate the current date at zero hour
		var today = new Date();
		var day = ('0' +  today.getDate()).slice(-2);
		var month = ('0' +  (today.getMonth()+1)).slice(-2);
		var year = today.getFullYear();
		var expirationDate = new Date(String.format('{0}-{1}-{2}',year,month,day));
		expirationDate = expirationDate.toISOString();
		//construct the query obtaining the first five publications not expired and ordered by the column "orden"
		regional.parameters.query = String.format(regional.parameters.query,expirationDate);
		try{
		//execute query function
			sputilities.getItems(regional.parameters.listName,regional.parameters.query,function(data){
				
				var resultItems = JSON.parse(data).d.results;
				regional.parameters.resultItems = resultItems;
				pageLayout.components.push(regional);		
				
			},function(data){
				//catch errors in query execution
				console.log(JSON.parse(data).error.message.value)
			});
		}catch(e){
				//caught unexpected errors
				console.log('Ocurrió un error en la ejecución del método regional.initialize() {0}',e)
			}
	 }, 
	 insertHTML:function(){

		var html = "";	
		var data = regional.parameters.resultItems; 	
			for(var i=0;i<data.length;i++){
				
				var currentItem = data[i];
				var text = currentItem.Contenido;
				var url =currentItem.Direccion.Url;
				html += String.format(regional.parameters.bodyHtml,sputilities.contextInfo().webAbsoluteUrl,text,url);				
						
			}	
			
		document.getElementById(regional.parameters.containerId).innerHTML = html;
	 }
}

var solidarity={
	 //create the initial parameters
	 parameters:{
		listName:"Regionales",
		containerId:"information-solidarity",
		bodyHtml:"<img src='{0}'><div class='regional-item-content'><h3>{1}</h3><a href='{2}' target='_blank' class='btn-regional'>Más información</a></div>",
		query:"?select=*,TipoContenido&$filter=(Vencimiento ge datetime'{0}' and TipoContenido eq 'Avianca Solidaria')&$orderby=Orden desc&$top=1",
		resultItems:null
		
	 },
	 initialize:function(){
		//calculate the current date at zero hour
		var today = new Date();
		var day = ('0' +  today.getDate()).slice(-2);
		var month = ('0' +  (today.getMonth()+1)).slice(-2);
		var year = today.getFullYear();
		var expirationDate = new Date(String.format('{0}-{1}-{2}',year,month,day));
		expirationDate = expirationDate.toISOString();
		//construct the query obtaining the first five publications not expired and ordered by the column "orden"
		solidarity.parameters.query = String.format(solidarity.parameters.query,expirationDate);
		try{
		//execute query function
			sputilities.getItems(solidarity.parameters.listName,solidarity.parameters.query,function(data){
				
				var resultItems = JSON.parse(data).d.results;
				solidarity.parameters.resultItems = resultItems;
				pageLayout.components.push(solidarity);		
				
			},function(data){
				//catch errors in query execution
				console.log(JSON.parse(data).error.message.value)
			});
		}catch(e){
				//caught unexpected errors
				console.log('Ocurrió un error en la ejecución del método solidarity.initialize() {0}',e)
			}
	 }, 
	 insertHTML:function(){

		var html = "";	
		var data = solidarity.parameters.resultItems; 	
			for(var i=0;i<data.length;i++){
				
				var currentItem = data[i];

				var img=currentItem.UrlImagen.Url;
				var text = currentItem.Contenido;
				var url =currentItem.Direccion.Url;
				html += String.format(solidarity.parameters.bodyHtml,img,text,url);
						
			}	
			
		document.getElementById(solidarity.parameters.containerId).innerHTML = html;
	 }
}

/* Components: Unidades de negocio */
var businessUnits = {
	//create parameters
	parameters:{
			listName: "Unidades de negocio",
			top: 1,
			divID:"information-bussines-units",
			link:"<a href='{0}' target='_blank'>",
			bodyImg:"<img src='{0}'><div class='regional-item-content'><h3>{1}</h3><a href='{2}' target='_blank' class='btn-regional'>Más información</a>",
			query: "?$select=Title,sys_txtImg,Url,Orden&$orderby=Orden&$top={0}",
			resultItems:null
	
	},	
	initialize:function(){
		try {
								
			//add current date to query and filter
			var linkQuery = String.format(businessUnits.parameters.query,businessUnits.parameters.top);

			//running the query with parameters
			sputilities.getItems(businessUnits.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				businessUnits.parameters.resultItems=resultItems;
				pageLayout.components.push(businessUnits);
				
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html="";
		var data=businessUnits.parameters.resultItems;
		//loop the array
				for (var i=0; i< data.length; i++){
				
				var currentItem = data[i];

				//taking the url of the image
				var image = currentItem.sys_txtImg;
				var div = document.createElement('div');
				div.innerHTML = image;
				var urlimg = div.querySelector('img').src;

				
				//retrieving the fields of the results
				var title = currentItem.Title;
				var url = currentItem.Url.Url;
				
				//adding values to the parameters
				var bodyNew = String.format(businessUnits.parameters.bodyImg,urlimg,title,url);	
				
				//adding values of parameters to the html variable
				html += bodyNew;
					
			}
			
			//insert the html code

			document.getElementById(businessUnits.parameters.divID).innerHTML = html;
			
		
	}
}

/* Components: Clients Media */
var clientsMedia = {
	//create parameters
	parameters:{
			listName: "Clientes Multimedia",
			top: 1,
			divID:"media-clients",			
			link:"<a href='{0}' target='_blank'>",
			bodyVideo: "<img src='{0}' class='img-responsive'><a href='javascript:void(0);' onclick='javascript:clientsMedia.click();' class='circulo bg-play centrado-total'><i class='inc-play centrado-total'></i></a>",										  
			bodyImg:"<a href='{0}' target='_blank'><img src='{1}' class='img-responsive' /></a>",
			query: "?$select=ID,Title,UrlMultimedia,TipoMultimedia,FechaInicio,FechaFinal,Url&$filter=(FechaFinal ge datetime'{0}' and FechaInicio le datetime'{0}')&$orderby=FechaFinal&$top={1}",
			resultItems:null,
			detailUrl:"{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo=VideoCliente",
			modal:'sputilities.showModalDialog("Video","{0}",window.location.href)',
			htmlVideo: "<style>video\{width:100%; max-width:500px; height:auto;\}</style><div class='video-clients' style='text-align: center;'><video controls><source src='{0}' media='(max-width:480px)'></video></a></div>",
			clientsTitle:"",
			clientsID:null
	
	},
	
	initialize:function(){
		try {
			
			//get current date
			var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();	
			
			//adding parameters to query
			var linkQuery = String.format(clientsMedia.parameters.query,date,clientsMedia.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(clientsMedia.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				clientsMedia.parameters.resultItems = resultItems;
				pageLayout.components.push(clientsMedia);
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	}, 
	insertHTML:function(){
		
		var html = "";
		var data = clientsMedia.parameters.resultItems;
		//loop the array
		for (var i=0; i< data.length; i++){
		
			var currentItem = data[i];
			//retrieving the fields of the results
			var title = currentItem.Title;
			var url = currentItem.Url.Url;
			var media = currentItem.UrlMultimedia.Url;
			var video="";
			//Evaluating the value of the field target
			if(currentItem.TipoMultimedia == "Imagen"){
				
				var bodyNew = String.format(clientsMedia.parameters.bodyImg,url,media);
			}
			else{
				var bodyNew = String.format(clientsMedia.parameters.bodyVideo,url);	
				video = String.format(clientsMedia.parameters.htmlVideo, media);			
			}
			
			//clientsMedia.parameters.htmlVideo=video;
			clientsMedia.parameters.clientsTitle=title;
			clientsMedia.parameters.clientsID = currentItem.ID;
			//adding values of parameters to the html variable			
			html += bodyNew;
					
					
		}
			
		//insert the html code
		document.getElementById(clientsMedia.parameters.divID).innerHTML = html;

	}, 
	click:function(){	
		var url = String.format(clientsMedia.parameters.detailUrl,sputilities.contextInfo().webAbsoluteUrl,clientsMedia.parameters.listName,clientsMedia.parameters.clientsID);
		sputilities.showModalDialog(clientsMedia.parameters.clientsTitle,url,window.location.href,null);
	}
}

/* Components: Clientes texto */
var clientsText = {
	//create parameters
	parameters:{
			listName: "Clientes Contenido Informativo",
			top: 1,
			divID:"text-clients",
			link:"",
			bodyText:"<h2>CLIENTES</h2><h1>{0}</h1><p>{1}</p><a href='{2}' target='_blank'>MÁS INFORMACIÓN<i class='inc-arrow-3 centrado-y'></i></a>",
			query: "?$select=Title,Descripcion,Url,Orden&$orderby=Orden&$top={0}",
			resultItems:null
	
	},
	
	initialize:function(){
		try {
		
			var html = "";
			//adding parameters to query
			var linkQuery = String.format(clientsText.parameters.query,clientsMedia.parameters.top);
			
			//running the query with parameters
			sputilities.getItems(clientsText.parameters.listName,linkQuery,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				clientsText.parameters.resultItems = resultItems;
				pageLayout.components.push(clientsText);
		
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	},
	insertHTML:function(){
		var html="";
		var data= clientsText.parameters.resultItems;
						//loop the array
				for (var i=0; i< data.length; i++){
				
				var currentItem = data[i];
				//retrieving the fields of the results
				var title = currentItem.Title;
				var url = currentItem.Url.Url;
				var description = currentItem.Descripcion;
				
				//adding values to the parameters
				var bodyNew = String.format(clientsText.parameters.bodyText,title,description,url);
				
				//adding values of parameters to the html variable
				html += bodyNew;
			}
			
			//insert the html code
			document.getElementById(clientsText.parameters.divID).innerHTML = html;
			

	}
}

/*Components: Slider Galeria*/

var sliderImg = {
	//create parameters
	parameters:{
			listName: "Galeria",
			top: 7,
			divID:"gallery-images",
			contentType: "la imagen",
			detailParameter: "Galeria",
			linkpopUp: '{0}/Paginas/Detalle.aspx?ListaBase={1}&IDItem={2}&Tipo={3}&ListaLikes=%22%22',
			link:"<a href='{0}' target='_blank'>",
			bodyImg:"<img src='{0}' /><h3>{1}</h3></a>",
			formatHtml:"<div class='gallery-image' onclick='javascript:sputilities.showModalDialog(\"{2}\",\"{3}\")'>"
						+	'<div class="container-window"></div>'
    					+	'<div class="container-bg-gallery">'
    					+	'<img class="img-bg-gallery" src="{1}">'
        				+		'<div class="gallery-ico">'
            			+			'<a>'
                		+ 				'<img src="{0}/SiteAssets/Development/images/components/gallery/resize.png">'
                		+				'<br>'
            			+ 			'</a>'
        				+ 		'</div>'
    					+	'</div>'
						+'</div>',
			query: "?$select=Title,FileRef,ContentType/Name,ID,Destacado&$expand=ContentType&$filter=(ContentType eq '{0}' and Destacado eq 1)&$orderby=Title&$top={1}",
			resultItems: null
	},
	insertHTML:function(){
	var html = "";
		var resultItems = sliderImg.parameters.resultItems;
		for (var i=0; i< resultItems.length; i++){
			var currentItem = resultItems[i];
			//retrieving the fields of the results
			var ID = currentItem.ID;
			var urlImg = currentItem.FileRef;
			var contentType = currentItem.ContentType.Name;
			var title = currentItem.Title;			
			//adding values to the parameters
			var linkpopUpNew = String.format(sliderImg.parameters.linkpopUp,sputilities.contextInfo().webAbsoluteUrl,sliderImg.parameters.listName,ID,sliderImg.parameters.detailParameter);
			html += String.format(sliderImg.parameters.formatHtml,sputilities.contextInfo().webAbsoluteUrl,urlImg,title,linkpopUpNew)				
		}	
		//insert the html code
		document.getElementById(sliderImg.parameters.divID).innerHTML = html;
	},
	initialize:function(){
		try {
			//adding parameters to query
			var linkQuery = String.format(sliderImg.parameters.query,sliderImg.parameters.contentType,sliderImg.parameters.top);
			//running the query with parameters
			sputilities.getItems(sliderImg.parameters.listName,linkQuery,function(data){	
				//save the results to an array
				sliderImg.parameters.resultItems = JSON.parse(data).d.results;
				pageLayout.components.push(sliderImg);		
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	}
}

/* Components: Accesos Rapidos */
var quickAccess = {
	 parameters:{
		containerId:"apps-1",
		listName:"Accesos Rápidos",
		query:"?$select=Title,Icono,Direccion,Padre,Orden,Home&$filter={0}&$orderby={1}",
		containerRow:"<div class='row'>{0}</div>",
		parentContainer:"<div class='col-md-4 col-sm-4 col-xs-12 swiper-slide'>"+
						"<a href='javascript:void(0);' onclick='javascript:quickAccess.click(this);'><div class='txt-accesos' name='children-accesos-{0}'>"+
						"<img src='{1}' class='img-apps'></img><span>{2}</span></div>"+
						"<div style='display:none;'><div class='col-xs-12 apps-container-children' id='children-accesos-{0}'></div></div></a></div>",
		parentOnly:"<div class='col-md-4 col-sm-4 col-xs-12 swiper-slide'>"+
					"<a href='{0}' target='_blank'><div class='txt-accesos'><img src='{1}' class='img-apps'></img>"+
					"<span>{2}</span></div></a></div>",
		childrenHtml:"<div class='col-xs-6 apps-children'><a href='{0}' target='_blank'><div class='txt-accesos-apps'><img src='{1}' onclick='{0}' class='img-apps'></img><span>{2}</span></div></a></div>",
		html:"<ul id=patternList></ul>",
		patternTitle:[],
		resultItems:null,
		resultChildrens:null
	 },
	 initialize:function(){
		 // insert the main list of fast links
		 //document.getElementById(quickAccess.parameters.containerId).innerHTML = quickAccess.parameters.html;
		 // adjusted the query to find parents and access
		 var queryFilter = String.format(quickAccess.parameters.query,"(Padre eq null and Home eq 1)","Orden");
		 try{
			 sputilities.getItems(quickAccess.parameters.listName,queryFilter,function(data){
				 var results = JSON.parse(data).d.results;
				 quickAccess.parameters.resultItems = results;
				 var queryChildrens = String.format(quickAccess.parameters.query,"(Padre ne null and Home eq 1)","Padre");
				 
					sputilities.getItems(quickAccess.parameters.listName,queryChildrens,function(dataContent){
						quickAccess.parameters.resultChildrens = JSON.parse(dataContent).d.results;
						pageLayout.components.push(quickAccess);
					},function(data){
				 		console.log(JSON.parse(data).error.message.value)
			 		});
				 
			 },function(data){
				 console.log(JSON.parse(data).error.message.value)
			 });
		  }catch(e){
				 console.log(String.format("Ocurrió un error en la ejecución del método quickAccess.initialize() {0}",e))
          }
	 },
	 insertHTML:function(){
	    var data =  quickAccess.parameters.resultItems;
	    var count = 0;
	    var html = "";
	 	//run the results array to insert the accesses and the parent elements
		for(var i = 0;i < data.length;i++){
			 if(data[i].Direccion == null){
				quickAccess.parameters.patternTitle.push({'title': data[i].Title, 'orden':data[i].Orden});						
				html += String.format(quickAccess.parameters.parentContainer,data[i].Orden,data[i].Icono.Url,data[i].Title);												 
			 }else{
				html += String.format(quickAccess.parameters.parentOnly,data[i].Direccion.Url,data[i].Icono.Url,data[i].Title);						
			 }
			 count +=1;
			 if(count == 3 || i== data.length-1)
			 {
			 	var body = String.format(quickAccess.parameters.containerRow, html);
			 	document.getElementById(quickAccess.parameters.containerId).insertAdjacentHTML('beforeend',body);
			 	html = "";
			 	count = 0;
			 }
		//document.getElementById('patternList').insertAdjacentHTML('beforeend',newAccess);
		}
		var dataChildren = quickAccess.parameters.resultChildrens;
		var parents = quickAccess.parameters.patternTitle;
		var currentParent="";
		var htmlChildren="";
		for(var i=0; i <dataChildren.length;i++)
		{
			if(dataChildren[i].Direccion != null){
			
				 if(i==0)
				 {
				 	currentParent = dataChildren[i].Padre;
				 	htmlChildren += String.format(quickAccess.parameters.childrenHtml,dataChildren[i].Direccion.Url,dataChildren[i].Icono.Url,dataChildren[i].Title);		
				 }
				 else
				 {
				 	if(currentParent != dataChildren[i].Padre)
				 	{
				 		for(var j=0;j<parents.length;j++)
				 		{
				 			if(parents[j].title == currentParent)
				 			{
				 				var containerid=String.format("children-accesos-{0}", parents[j].orden);
				 				document.getElementById(containerid).insertAdjacentHTML('beforeend',htmlChildren);
				 				htmlChildren="";
				 			}
				 		}				 		
				 		currentParent = dataChildren[i].Padre;
				 		htmlChildren += String.format(quickAccess.parameters.childrenHtml,dataChildren[i].Direccion.Url,dataChildren[i].Icono.Url,dataChildren[i].Title);
				 	}
				 	else
				 	{			 		
				 		currentParent = dataChildren[i].Padre;
				 		htmlChildren += String.format(quickAccess.parameters.childrenHtml,dataChildren[i].Direccion.Url,dataChildren[i].Icono.Url,dataChildren[i].Title);	
				 		
				 	}
				 	
				 	if(i == dataChildren.length - 1)
				 		{
				 			for(var j=0;j<parents.length;j++)
				 			{
					 			if(parents[j].title == currentParent)
					 			{
					 				var containerid=String.format("children-accesos-{0}", parents[j].orden);
					 				document.getElementById(containerid).insertAdjacentHTML('beforeend',htmlChildren);
					 				htmlChildren="";
					 			}
				 			}
				 		}
				 	
				 }
					
				
			}
		}
		//quickAccess.getChildrenList();
	 },
	 click:function(ctrl){
	  var id = ctrl.firstElementChild.attributes.name.value;
	  var node = document.getElementById(id);
	  //console.log(node.innerHtml);
		sputilities.showModalDialog(ctrl.querySelector('span').innerHTML,null,null,node.parentElement.innerHTML);

	 }	 	 
 }
 
 /*Component twitter*/
var twitter = {	
	parameters:{
		containerId: "social-twitter",
		htmlVideo: '<a class="twitter-timeline"  href="https://twitter.com/search?q=%40Avianca" data-widget-id="868102404264382464">Tweets sobre @Avianca</a>',
		/*'<a class="twitter-timeline" href="https://twitter.com/Avianca"></a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>',*/
		code: '<a class="twitter-timeline" href="https://twitter.com/Avianca">Tweets by Avianca</a>'
							
	},
	initialize:function(){
					
		try{	
		pageLayout.components.push(twitter);
			}catch(e){
			console.log(String.format("Error en metodo twitter.initialize: {0}",e))
		}				
	},	
	insertHTML:function(){
		var button = "<a href='javascript:void(0);' onclick='javascript:twitter.click();'><img src='https://avianca.sharepoint.com/sites/Intranet/SiteAssets/Development/images/components/twitter/aviancatwitter.jpg'></img></a>";
		var twitterContainer = document.getElementById(twitter.parameters.containerId);
		var embed = twitter.parameters.code;
		twitterContainer.innerHTML = button;
//		$('head').append('<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>');
	},
	insertscript:function(){	
		//$('head').append('<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>');
		$('head').append('<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>');
	},
	click:function(){		
		sputilities.showModalDialog("Twitter",null,null,twitter.parameters.htmlVideo);
		twitter.insertscript();
	}
}
