﻿//04-10-2016
//Lagash Co
// Popup una vez al día por usuario CCB
// Uso de Listas: 'Popup' <- para el contenido y 'ControlPopUp' para el control de una vista por día por usuario

 $(function () {
        ExecuteOrDelayUntilScriptLoaded(executePopUp, "sp.js");
    });
var popupUrl = 'https://avianca.sharepoint.com/sites/Intranet/Paginas/diaryPopup.aspx';
var controlList = 'ControlPopUp';
var contentList = 'Popup';
var siteUrl = _spPageContextInfo.webAbsoluteUrl;
var userId = _spPageContextInfo.userId;
var _tTitle = '';

function executePopUp()
{	
    getList(contentList , function (data) {
        if(data.length > 0)
        {
            validatePopUpView();
            _tTitle = data[0].Title
        }
    }, 'activo+eq+1');
}

function validatePopUpView() {

    getList(controlList, function (data) {
        if (data.length > 0) {
            if (!checkIfToday(data[0].date))
                updateViewStatus(data[0].Id);
        }
        else {
            firstTimeOpening();
        }
    }, "userid+eq+" + userId);
}


function firstTimeOpening() {
    createRecord();
}

function checkIfToday(date) {
    var today = new Date();
    var currentDate = new Date(date);
    if (today.getDate() == currentDate.getDate())
        return true;
    else
        return false;
}

function updateViewStatus(id)
{
    var clientContext = new SP.ClientContext(siteUrl);
    var oList = clientContext.get_web().get_lists().getByTitle(controlList);
    var today = new Date();

    this.oListItem = oList.getItemById(id);
    oListItem.set_item('date', today.toString());
    oListItem.update();
    clientContext.executeQueryAsync(Function.createDelegate(this, this.okViewStatus), Function.createDelegate(this, this.errorViewStatus));
}

function okViewStatus()
{
    openBasicDialog(popupUrl, '');
}
function errorViewStatus(error) {
    console.log('Error al actualizar el estado de visto del popup: ' + JSON.stringify(error));
}
function createRecord() {

    var clientContext = new SP.ClientContext(siteUrl);
    var oList = clientContext.get_web().get_lists().getByTitle(controlList);
    var itemCreateInfo = new SP.ListItemCreationInformation();
    var today = new Date();
        today.setDate(today.getDate() - 1);
    this.oListItem = oList.addItem(itemCreateInfo);
    oListItem.set_item('userid', userId);
    oListItem.set_item('date', today.toString());
    oListItem.update();
    clientContext.load(oListItem);
    clientContext.executeQueryAsync(Function.createDelegate(this, this.okRecord), Function.createDelegate(this, this.errorRecord));
}

function okRecord() {
    executePopUp();
}

function errorRecord(response) {
    console.log('Error al crear registro de visto en popup: ' + JSON.stringify(response));
}
function getList(lista, success, filtro) {
    if (filtro)
        var consulta = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('" + lista + "')/items?$filter=" + filtro;
    else
        var consulta = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('" + lista + "')/items?$orderby=Id desc";
    $.ajax({
        url: consulta,
        method: "GET",
        async: false,
        headers: {
            "accept": "application/json; odata=verbose",
        },
        success: function (data) {
            success(data.d.results);
        },
        error: function () {
            errorList();
        }
    });
}
function openBasicDialog(tUrl, tTitle) {
	var options = SP.UI.$create_DialogOptions();
	
	//options.title = _tTitle;
		options.height = 400;
	 options.autoSize = true;
	 options.url = tUrl;
	
	SP.UI.ModalDialog.showModalDialog(options);

   /*ar options = {
        url: tUrl,
        title: tTitle,
        autoSize : true
    };*/
}
