﻿function popup(lista, success, filtro) {
   	var consulta = "https://avianca.sharepoint.com/sites/Intranet/_api/web/lists/getbytitle('Popup')/items?$filter=activo+eq+1";
	$.ajax({
        url: consulta,
        method: "GET",
        async: false,
        headers: {
            "accept": "application/json; odata=verbose",
        },
        success: function (data) {
	        $('#contenido').append(data.d.results[0].contenido);  
	        $('#contenido').css('overflow', 'hidden');
	        $('#contenido').css('width', '100%');
	        $('#contenido img').css('width', '96%');
	        $('#contenido video').css('width', '96%');
	        $('#contenido iframe').css('width', '96%');
        },
        error: function () {
            
        }
    });
}

setTimeout(function(){
	popup();
}, 1000);
