sputilities.callFunctionOnLoadBody('startPopup.initialize');
var startPopup={
    parameters:{
        controlList:'ControlPopUp',
        queryControl:'?$filter=userid eq {0}',
        contentList: 'Popup',
        queryControlVideo:'?$filter=activo eq 1&$orderby=ID desc',
        dateFormat:'{0}/{1}/{2}',
        today:null,
        userId:null,
        showPopup:null                
    },
    initialize:function(){
        try{
            //get today date
            var today = new Date();
            var year = today.getFullYear();
            var month = today.getMonth()+1;
            var day = today.getDate();
            startPopup.parameters.today = String.format(startPopup.parameters.dateFormat,year,month,day)
            //get user id
            sputilities.getCurrentUserInformation(function(data){
                    startPopup.parameters.userId = JSON.parse(data).d.ID;
                    //confirm user in control list
                    var query = String.format(startPopup.parameters.queryControl,startPopup.parameters.userId);
                    sputilities.getItems(startPopup.parameters.controlList,query,function(data){
                        var results = JSON.parse(data).d.results;                        
                        if(results.length > 0){
                            //if user exist in list get current date
                            var date = new Date(results[0].date);
                            var year = date.getFullYear();
                            var month = date.getMonth()+1;
                            var day = date.getDate();
                            currentDate = String.format(startPopup.parameters.dateFormat,year,month,day);
                            console.log(currentDate + '--' + startPopup.parameters.today)
                            if(currentDate == startPopup.parameters.today){
                                //start control in video list
                                startPopup.parameters.showPopup = false;
                            }else{
                                startPopup.parameters.showPopup = true;
                                //update user register
                                startPopup.updateControlPopup();
                            }
                        }else{                            
                            startPopup.parameters.showPopup = true;
                            //create new register
                            startPopup.createUserControlPopup();
                        }
                    },function(data){
                        startPopup.msgError(data)
                    }
                );
                },function(data){
                    startPopup.msgError(data)
                }
            );
        }catch(e){            
            console.log(String.format('Ocurrío un error en la ejecucion del componente startPopup {0}',e))
        }        
    },
    showVideoPopup:function(){
        try{
            //get video by list popup
            sputilities.getItems(startPopup.parameters.contentList,startPopup.parameters.queryControlVideo,
            function(data){
                var results = JSON.parse(data).d.results;
                if(results.length>0 && startPopup.parameters.showPopup == true){
                    //create container video
                    var videoContainer = document.createElement('div');
                    videoContainer.setAttribute('class','popUpVideo-container');
                    videoContainer.style.overflow = 'hidden';
                    videoContainer.style.width = '100%';
                    videoContainer.innerHTML = results[0].contenido;                    
                    var options ={
                        html: videoContainer,
                        title: ' ',
                        width: 400,
                        autoSize: true
                    };
                    SP.UI.ModalDialog.showModalDialog(options);
                    videoContainer.parentElement.parentElement.previousElementSibling.classList.add('dialog-title')
                }
                },function(data){
                        startPopup.msgError(data)
                    }
            );
            
        }catch(e){
            console.log(String.format('Ocurrío un error en la ejecucion del componente startPopup {0}',e))
        }
    },
    updateControlPopup:function(){
        try{
            var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
            var oList = clientContext.get_web().get_lists().getByTitle(startPopup.parameters.controlList);
            var today = new Date();
            var oListItem = oList.getItemById(startPopup.parameters.userId);
            oListItem.set_item('date', today.toString());
            oListItem.update();
            clientContext.executeQueryAsync(
                Function.createDelegate(this, this.showVideoPopup),
                Function.createDelegate(this, this.msgError));
        }catch(e){
            console.log(String.format('Ocurrío un error en la ejecucion del componente startPopup {0}',e))
        }
    },
    createUserControlPopup:function(){
        try{
            var clientContext = new SP.ClientContext(_spPageContextInfo.webAbsoluteUrl);
            var oList = clientContext.get_web().get_lists().getByTitle(startPopup.parameters.controlList);
            var itemCreateInfo = new SP.ListItemCreationInformation();
            var today = new Date();
            var oListItem = oList.addItem(itemCreateInfo);
            oListItem.set_item('userid', startPopup.parameters.userId);
            oListItem.set_item('date', today.toString());
            oListItem.update();
            clientContext.load(oListItem);
            clientContext.executeQueryAsync(
                Function.createDelegate(this, this.showVideoPopup), 
                Function.createDelegate(this, this.msgError));  
        }catch(e){
           console.log(String.format('Ocurrío un error en la ejecucion del componente startPopup {0}',e))
        }
    },
    msgError:function(data){
        console.log(data)
    }
}
