﻿var directoryContacts = {
    
        //create  parameters
        parameters:{
            listName:"Contactos Aeropuertos",
            listConfig:"ConfiguracionContactos",
            containerId:"module-directory",
            query:"?$select=*",
            queryConfig:"?$select=ID,Title,NombreCampo,ValoresCampos,Obligatorio,NombreSeccion/Title,NombreSeccion/ID&$expand=NombreSeccion&$orderby=NombreSeccion/ID",
            regionControlHTMLformat:"<label>Región</label><select id='region' onchange='directoryContacts.getFilteredItems(this)'><option value='Todos'>Todos</option></select>",
            baseControlHTMLformat:"<label>Base</label><select id='base' onchange='directoryContacts.getFilteredItems(this)'><option value='Todos'>Todos</option></select> ",
            searchControlHTMLformat:"<img src='/sites/Intranet/SiteAssets/Development/images/components/employeeDirectory/ico-directory.png'><input type='text' placeholder='Digite el nombre a buscar' class='directory-formname' id='searchBox'>",
            paginatorControlHTMLformat:"<label>Resultados</label><select id='itemsBypage' onchange='directoryContacts.getFilteredItems(this)'><option value='2'>2</option>"
                                        +"<option value='4'>4</option><option value='6'>6</option></select>",
            containerUL:"<ul class='directory-list-contact' id='directory-list-contact'></ul>",
            htmlContact:"<li class='item-contact' data-region='{0}' data-base='{1}'><div class='info-contact'><h4>{2}</h4><p>{3}</p></div></li>",
            msgError: "Ah ocurrido un error en la ejecucion del metodo {0}, {1}",
            msgMethod:	"directoryContacts.initialize",
            itemsContacts:null,
            isLoaded:false,
            htmlElments:{
            	btnSearch: {
            		id:"searchBox",
            		onEvent:"keypress"
            	},
            	idHTMLElements:{
            		directoryId:"directory-list",
                    directoryListId:"directory-list-contact",                   
            		region:"region",
                    base:"base",
                    paginator:"paginator"
            	},
            	insertHTML:{
            		html:"<br />",
                    insert:"beforeend",
                    formatItems:"{0}{1}"
            	},
            	classElements:{
                    div:"div",
                    option:"option",
            		classElement:"class",
            		classRegion:"controls-region",
            		classBase:"controls-base",
            		classHeader:"directory-header",
            		classSearch:"controls-search",
            		classPaginator:"controls-paginator",
            		classContent:"controls-directory-content",            		
            		classControls:"controls-directory",
                    classNumbers:"paginator-Numbers",
                    items:"item-contact"                                        
            	}
            },
            paginatorModule:{
                itemsByPage:"itemsBypage",
                noResults:"No se encontraron Resultados",
                multipleResults:"Se encontraron {0} contactos",
                singleResults:"Se encontro {0} contacto",
                formatFooter:"{0}{1}",
                labelPaginator:'<label class="paginator" onclick="directoryContacts.goFromPage(this.innerText,this)">{0}</label>',
                dataPage:"data-page",
                activePage:"active-page",
                display:{
                    hidden:"hidden",
                    visible:"visible"
                },
                allItems:"Todos"
            }	
        
        },
        initialize:function(){
            try{
                
                directoryContacts.loadContacts();
                var loadControl = setInterval(function(){
                 	if(directoryContacts.parameters.isLoaded)
	             	{ 
		                var container = document.getElementById(directoryContacts.parameters.containerId)
		                if(container != null)
		                {
		                	container.innerHTML ="";
		                	 //create the main container
		                	directoryContacts.createMainContainer(container);
		
                            var searchBox = document.getElementById(directoryContacts.parameters.htmlElments.btnSearch.id);
                            if(searchBox != null)
                            {		
                                searchBox.addEventListener(directoryContacts.parameters.htmlElments.btnSearch.onEvent, function (e) {			
                                    if (e.keyCode == 13) { 
                                        e.preventDefault();					
                                        directoryContacts.getFilteredItems(searchBox);
                                    }			
                                });
                            }
	                   		directoryContacts.createDirectory();
	                   	}
	                 }
	                 clearInterval(loadControl);	
	            },1000);    
            }
            catch(e){
                console.log(String.format(directoryContacts.parameters.msgError,directoryContacts.parameters.msgMethod,e))
            }

        },
        loadContacts:function(){
            
            try {			
                    //running the query with parameters
                    sputilities.getItems(directoryContacts.parameters.listName,directoryContacts.parameters.query,function(data){				
                    //save the results to an array
                    var resultItems = utilities.convertToJSON(data);
                    if(resultItems != null)
                    {
                        directoryContacts.parameters.itemsContacts = resultItems.d.results;
                        directoryContacts.parameters.isLoaded = true;
                    }
                        
                },function(xhr){ console.log(xhr)});			
            }
            catch(e)
            {
               console.log(e);
            }
        },  
        createDirectory:function(){
            try {
            
                var items = directoryContacts.parameters.itemsContacts;
                var directoryList =  document.getElementById(directoryContacts.parameters.htmlElments.idHTMLElements.directoryId);
                if(directoryList != null)
                {
                    directoryList.innerHTML = directoryContacts.parameters.containerUL;
                    var directory = document.getElementById(directoryContacts.parameters.htmlElments.idHTMLElements.directoryListId);
                    var listRegion = document.getElementById(directoryContacts.parameters.htmlElments.idHTMLElements.region);
                    var listBase = document.getElementById(directoryContacts.parameters.htmlElments.idHTMLElements.base);
                    var regionsptions= [];
                    var basesOptions=[];
                    if(directory != null && listRegion != null && listBase != null)
                    {   
                        for (i=0;i<items.length;i++)
                        {
                            var nombreBase = items[i].Title;
                            var nombreRegion = items[i].Region;
                            var infoPrimaria = utilities.convertToJSON(items[i].sysInformacionPrimaria);
                            var salasAsistencia = utilities.convertToJSON(items[i].sysSalasAsistencia);
                            var contactosNotificacion = utilities.convertToJSON(items[i].sysContactosNotificacion);
                            var contactosLocales = utilities.convertToJSON(items[i].sysContactosLocales);

                            var region = {"text":nombreRegion,
                            "value": i};
                            regionsptions.push(region);
                            
                            var base = {"text":nombreBase,
                            "value": i};
                            basesOptions.push(base);
                            
                            directoryContacts.htmlContacts(infoPrimaria, directory, nombreBase, nombreRegion);
                            directoryContacts.htmlContacts(salasAsistencia, directory, nombreBase, nombreRegion);
                            directoryContacts.htmlContacts(contactosNotificacion, directory, nombreBase, nombreRegion);
                            directoryContacts.htmlContacts(contactosLocales, directory, nombreBase, nombreRegion);                   

                        }
                        //create Options select
                        directoryContacts.createOptionSelect(basesOptions,listBase);
                        directoryContacts.createOptionSelect(regionsptions,listRegion);	
                        directoryContacts.getFilteredItems(listBase);
                    }
                }
            }
            catch(e){
                console.log(e)
            }

        },   
        createOptionSelect:function(info,list){
            try{
                for(var i=0;i<info.length;i++){
                    var selectOption = document.createElement(directoryContacts.parameters.htmlElments.classElements.option);
                    selectOption.value = info[i].value;
                    var textOption = document.createTextNode(info[i].text);
                    selectOption.appendChild(textOption);
                    list.appendChild(selectOption);		
                }
            }catch(e){
                console.log(e)
            }
        },
        htmlContacts:function(infoArray, directory, nombreBase, nombreRegion){
          
            if(infoArray != null)
            {                
                for(var j=0;j<infoArray.length;j++)
                {                   
                    var keys = utilities.getKeysOfAnObject(infoArray[j]);
                    var htmlItem = "";
                    if(infoArray[j][keys[1]] != ""){
                        var label = infoArray[j][keys[1]];
                        var html ="";
                        for(var k=0;k<keys.length;k++)
                        {
                            if(k != 1)
                            {
                                html += String.format(directoryContacts.parameters.htmlElments.insertHTML.formatItems,infoArray[j][keys[k]],directoryContacts.parameters.htmlElments.insertHTML.html);
                            }
                        }
                        html += String.format(directoryContacts.parameters.htmlElments.insertHTML.formatItems,nombreBase,directoryContacts.parameters.htmlElments.insertHTML.html);
                        html += String.format(directoryContacts.parameters.htmlElments.insertHTML.formatItems,nombreRegion,directoryContacts.parameters.htmlElments.insertHTML.html)
                        htmlItem = String.format(directoryContacts.parameters.htmlContact, i, i, label, html);
                        directory.insertAdjacentHTML(directoryContacts.parameters.htmlElments.insertHTML.insert,htmlItem);        
                    }                        
                }                    
             }
        },
        createMainContainer:function(mainContainer){
            try{
                
                //create seccion control search
                var controlsSearch= document.createElement(directoryContacts.parameters.htmlElments.classElements.div);
                controlsSearch.setAttribute(directoryContacts.parameters.htmlElments.classElements.classElement,directoryContacts.parameters.htmlElments.classElements.classHeader);
                //create search container
                var searchContainer = document.createElement(directoryContacts.parameters.htmlElments.classElements.div);
                searchContainer.setAttribute(directoryContacts.parameters.htmlElments.classElements.classElement,directoryContacts.parameters.htmlElments.classElements.classSearch);
                searchContainer.innerHTML = directoryContacts.parameters.searchControlHTMLformat;
                controlsSearch.appendChild(searchContainer);

                //create controls section
                var controlsContainer = document.createElement(directoryContacts.parameters.htmlElments.classElements.div);
                controlsContainer.setAttribute(directoryContacts.parameters.htmlElments.classElements.classElement,directoryContacts.parameters.htmlElments.classElements.classControls);
                
                var controlsContent = document.createElement(directoryContacts.parameters.htmlElments.classElements.div);
                controlsContent.setAttribute(directoryContacts.parameters.htmlElments.classElements.classElement,directoryContacts.parameters.htmlElments.classElements.classContent);			

                //create region container
                var regionContainer = document.createElement(directoryContacts.parameters.htmlElments.classElements.div);
                regionContainer.setAttribute(directoryContacts.parameters.htmlElments.classElements.classElement,directoryContacts.parameters.htmlElments.classElements.classRegion);
                regionContainer.innerHTML = directoryContacts.parameters.regionControlHTMLformat;
                controlsContent.appendChild(regionContainer);
                //create base container
                var baseContainer = document.createElement(directoryContacts.parameters.htmlElments.classElements.div);
                baseContainer.setAttribute(directoryContacts.parameters.htmlElments.classElements.classElement,directoryContacts.parameters.htmlElments.classElements.classBase);
                baseContainer.innerHTML = directoryContacts.parameters.baseControlHTMLformat;
                controlsContent.appendChild(baseContainer);
                
                //create paginator container
                var paginatorContainer = document.createElement(directoryContacts.parameters.htmlElments.classElements.div);
                paginatorContainer.setAttribute(directoryContacts.parameters.htmlElments.classElements.classElement,directoryContacts.parameters.htmlElments.classElements.classPaginator);
                paginatorContainer.innerHTML = directoryContacts.parameters.paginatorControlHTMLformat;
                controlsContent.appendChild(paginatorContainer);
                controlsContainer.appendChild(controlsContent);
                //create news list container 
                var newsListContainer = document.createElement(directoryContacts.parameters.htmlElments.classElements.div);
                newsListContainer.setAttribute(directoryContacts.parameters.htmlElments.classElements.classElement, directoryContacts.parameters.htmlElments.idHTMLElements.directoryId);
                newsListContainer.id=directoryContacts.parameters.htmlElments.idHTMLElements.directoryId;	
                //create paginator container
                var paginatorContainer = document.createElement(directoryContacts.parameters.htmlElments.classElements.div);
                paginatorContainer.setAttribute(directoryContacts.parameters.htmlElments.classElements.classElement, directoryContacts.parameters.htmlElments.classElements.classNumbers);
                paginatorContainer.id = directoryContacts.parameters.htmlElments.idHTMLElements.paginator;
                //add controls, list and paginator to main Container
                mainContainer.appendChild(controlsSearch);
                mainContainer.appendChild(controlsContainer);
                mainContainer.appendChild(newsListContainer);
                mainContainer.appendChild(paginatorContainer);		
                
            }catch(e){
                console.log(e)
            }
        },
        getPaginatorModule:function(filteredItems){
            // get values for paginator mode
            var limitPage = document.getElementById(directoryContacts.parameters.paginatorModule.itemsByPage);
            if(limitPage != null)
            {
                var numberLimit = Number(limitPage.value);
                var limit = numberLimit;
                var footerFormat = filteredItems.length == 0 ? directoryContacts.parameters.paginatorModule.noResults : directoryContacts.parameters.paginatorModule.formatFooter;
                if(filteredItems.length == 0){                    
                    var paginator = document.getElementById(directoryContacts.parameters.htmlElments.idHTMLElements.paginator);
                }else{                   
                    //create msg de elementos en la pagina
                    var paginator = document.getElementById(directoryContacts.parameters.htmlElments.idHTMLElements.paginator);
                    var msgResults = document.createElement(directoryContacts.parameters.htmlElments.classElements.div);
                    if(filteredItems.length > 1){
                        var msgText = document.createTextNode(String.format(directoryContacts.parameters.paginatorModule.multipleResults,filteredItems.length));
                    }else{
                        var msgText = document.createTextNode(String.format(directoryContacts.parameters.paginatorModule.singleResults,filteredItems.length));
                    }
                    msgResults.appendChild(msgText);			
                };
                if(paginator != null)
                {
                    var page=1;
                    try{
                        //get page
                        for (var i=0;i<filteredItems.length;i++){
                            switch (true){
                                case i < limit && i != (filteredItems.length-1):
                                    filteredItems[i].setAttribute(directoryContacts.parameters.paginatorModule.dataPage,page);
                                    break;
                                case i == limit && i != (filteredItems.length-1):
                                    var letterPage = String.format(directoryContacts.parameters.paginatorModule.labelPaginator,page);
                                    footerFormat=String.format(footerFormat,letterPage,'{0}{1}');
                                    page +=1;
                                    limit +=numberLimit;
                                    filteredItems[i].setAttribute(directoryContacts.parameters.paginatorModule.dataPage,page);
                                    break;
                                case i == limit && i == (filteredItems.length-1):
                                    var letterPage = String.format(directoryContacts.parameters.paginatorModule.labelPaginator,page);
                                    footerFormat=String.format(footerFormat,letterPage,'{0}{1}');
                                    page +=1;
                                    limit +=numberLimit;
                                    filteredItems[i].setAttribute(directoryContacts.parameters.paginatorModule.dataPage,page);
                                    var letterPage = String.format(directoryContacts.parameters.paginatorModule.labelPaginator,page);
                                    footerFormat=String.format(footerFormat,letterPage,'');
                                    break;
                                case i < limit && i == (filteredItems.length-1):
                                    filteredItems[i].setAttribute(directoryContacts.parameters.paginatorModule.dataPage,page);
                                    var letterPage = String.format(directoryContacts.parameters.paginatorModule.labelPaginator,page);
                                    footerFormat=String.format(footerFormat,letterPage,'');
                                    break;
                            }		
                        }			
                        paginator.innerHTML = footerFormat;
                        paginator.appendChild(msgResults);		
                        directoryContacts.goFromPage(1);
            
                    }catch(e){                
                        console.log(e);	
                    }
                }
            }
        },
        goFromPage:function(page,ctrl){
        if(ctrl == undefined){
                var ctrl = document.getElementsByClassName(directoryContacts.parameters.htmlElments.idHTMLElements.paginator)[0];			
            }else{
                var activeControl = document.getElementsByClassName(directoryContacts.parameters.paginatorModule.activePage)[0];
                activeControl.classList.remove(directoryContacts.parameters.paginatorModule.activePage);
                activeControl.classList.add(directoryContacts.parameters.htmlElments.idHTMLElements.paginator);
            }
            ctrl.classList.remove(directoryContacts.parameters.htmlElments.idHTMLElements.paginator);
            ctrl.classList.add(directoryContacts.parameters.paginatorModule.activePage);	 
         
            var itemsSelected = document.querySelectorAll('[data-page]');
            for(var i=0;i<itemsSelected.length;i++){
                if(itemsSelected[i].getAttribute(directoryContacts.parameters.paginatorModule.dataPage) == page){
                    itemsSelected[i].classList.remove(directoryContacts.parameters.paginatorModule.display.hidden);
                    itemsSelected[i].classList.add(directoryContacts.parameters.paginatorModule.display.visible);
                }else{
                    itemsSelected[i].classList.remove(directoryContacts.parameters.paginatorModule.display.visible);
                    itemsSelected[i].classList.add(directoryContacts.parameters.paginatorModule.display.hidden);
                }    
            }
        },
        getFilteredItems:function(ctrl){
            //get values of filter
            var region = document.getElementById(directoryContacts.parameters.htmlElments.idHTMLElements.region);
            var base = document.getElementById(directoryContacts.parameters.htmlElments.idHTMLElements.base);
            var search = document.getElementById(directoryContacts.parameters.htmlElments.btnSearch.id);
            if(region != null && base != null && search != null)
            {
                region = region.value;
                base = base.value;
                search = search.value;

                search = search.toUpperCase();
                var filteredNews=[];
                //get all element in news list
                if(ctrl.id == directoryContacts.parameters.paginatorModule.itemsByPage){
                    var news = document.querySelectorAll('[data-page]');
                }else{
                    var news = document.getElementsByClassName(directoryContacts.parameters.htmlElments.classElements.items);
                }
                try{
                    for(var i = 0;i<news.length;i++){
                        var item = news[i];
                        var contentItem = item.outerHTML;
                        contentItem = contentItem.toUpperCase();
                        //reset filter in document
                        if(item.getAttribute(directoryContacts.parameters.paginatorModule.dataPage) != null){
                            item.removeAttribute(directoryContacts.parameters.paginatorModule.dataPage);					
                        }
                        //Get items by filter	
                        if(ctrl.id != directoryContacts.parameters.paginatorModule.itemsByPage){
                            switch(true){
                                case(search != "" && contentItem.indexOf(search) != -1):
                                    filteredNews.push(item);
                                break;
                                case(region == directoryContacts.parameters.paginatorModule.allItems && base == directoryContacts.parameters.paginatorModule.allItems && search == ""):
                                case(item.dataset.region == region && item.dataset.base == base && search == ""):
                                case(region == directoryContacts.parameters.paginatorModule.allItems && item.dataset.base == base && search == ""):
                                case(item.dataset.region == region && base == directoryContacts.parameters.paginatorModule.allItems && search == ""):
                                    filteredNews.push(item);
                                break;
                                default:
                                    item.classList.remove(directoryContacts.parameters.paginatorModule.display.visible);
                                    item.classList.add(directoryContacts.parameters.paginatorModule.display.hidden);
                                break;
                            }	
                        }else{
                            filteredNews.push(item);
                        }				
                    }
                    directoryContacts.getPaginatorModule(filteredNews);
        
                }catch(e){                
                    console.log(e);
                }
            }
        }
    }
    directoryContacts.initialize();