﻿

/*
-- Componente: Guiones corporativos
	Parameters: 
		* listName: List name for the coporate screenplays
		* divID: Div with that contain of the corporate screenplays
		* divIdCategory: Id for the differents groups of the screenplays
		* divIdQuestion: Id for each question
		* divCategory: Html for each group
		* divAcordiion: Html for each question and answer
		* query: Query for get items of the list

*/


sputilities.callFunctionOnLoadBody("screenplays.initialize");

var screenplays = {

	parameters:{
		listName:"Guiones Corporativos",
		divID:"accordion",
		
		divIdCategory:"category-{0}",
		divIdQuestion:"tab-question-{0}",
		divCategorys:"Categorys",
		divCategorysPrincipal:"<a class='MenuItemCategory' href='#{1}' onclick='javascript:screenplays.click(this);' category='{1}'>{0}</a>",
		divCategory:"<div class='accordion-title' id='{1}' style='display:none;' >",
		divAcordion:"  <div class='panel panel-default'><div class='panel-heading' role='tab' id='{2}'><div class='panel-title'><a role='button' data-toggle='collapse' data-parent='#accordion' href='#{3}' aria-expanded='{4}' aria-controls='{3}'><i class='keyquestions-ico'></i><h3 id='Question'>{0}</h3></a></div></div><div id='{3}' class='{5}' role='tabpanel' aria-labelledby='{2}'><div class='panel-body'><p id='Answer'>{1}</p> </div></div></div>",
		query:"?$select=*,Respuesta,AsociadoA&$orderby=AsociadoA"
	},
	initialize:function(){
		
		try {
			
			var html="";
			var htmll="";
			sputilities.getItems(screenplays.parameters.listName,screenplays.parameters.query,function(data){
			
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				console.log(resultItems)
				//print categorys items//
				for (var i=0; i< resultItems.length; i++){
				var currentItem = resultItems[i];
				var previousItem = resultItems[i-1];
				var nextItem = resultItems[i+1];
				var asociated = currentItem.AsociadoA;
                var Paginator = currentItem.AsociadoA;
                var uno = Paginator.split(" ")[0];
					if (i== 0){
						htmll += String.format(screenplays.parameters.divCategorysPrincipal,asociated,uno);
						document.getElementById(screenplays.parameters.divCategorys).innerHTML = htmll;
					}
					else{
						if(previousItem != undefined && asociated != previousItem.AsociadoA){ 	
								htmll += String.format(screenplays.parameters.divCategorysPrincipal,asociated,uno);
								document.getElementById(screenplays.parameters.divCategorys).innerHTML = htmll;
						}
					}
						}

				//print items //
				for (var i=0; i< resultItems.length; i++){
					
					var currentItem = resultItems[i];
					var previousItem = resultItems[i-1];
					var nextItem = resultItems[i+1];


					var question = currentItem.Title;
					var answer = currentItem.Respuesta;
					var asociated = currentItem.AsociadoA;
                    var Paginator = currentItem.AsociadoA;
                    var uno = Paginator.split(" ")[0];
					var heading = "heading"+i;
					var collapse = "collapse"+i;
					var tcollapse = true
					var Fcollapse = false
					var abierto = "panel-collapse collapse"
					var cerrado = "panel-collapse collapse"
					
					if (i== 0)
					{
                        html += String.format(screenplays.parameters.divCategory,asociated,uno);
                        

					}
					else
					{
						if(previousItem != undefined && asociated != previousItem.AsociadoA && nextItem != undefined){ 
							
							html += "</div>" + String.format(screenplays.parameters.divCategory,asociated,uno);
	
						}
						/*else
						{
							if(nextItem == undefined){
								html += "</div>" + String.format(screenplays.parameters.divCategory,idCategory,asociated);
							}
						}*/
					}
					
					if(i == resultItems.length - 1 ){ 
						
						if(resultItems[i-1].AsociadoA != asociated)
						{
							html += "</div>"+String.format(screenplays.parameters.divCategory,asociated,uno);

						}
						else
						{
						console.log("paso")
						}
					}
					
					if(i == resultItems.length - 1 ){
					    html += String.format(screenplays.parameters.divAcordion,question,answer,heading,collapse,Fcollapse,cerrado);
					}
					else
					{
                        if(i==0){
                        html += String.format(screenplays.parameters.divAcordion,question,answer,heading,collapse,tcollapse,abierto);
                    }
                    else{
					html += String.format(screenplays.parameters.divAcordion,question,answer,heading,collapse,Fcollapse,cerrado);
					}
                    }
				}
			document.getElementById(screenplays.parameters.divID).innerHTML = html;
			CloseAcordeon()
			
			});

			
			
			
			
		}
		catch(e){
			console.log(e);
		}
	
	},
	InitPaginator:function(Name){
	    $('#'+Name).dynatable({
  			table: {
   			 bodyRowSelector: 'li'
  			},
  		inputs: {
      	queries: $('#search-year')
  		},
  		dataset: {
  		   perPageDefault: 2,
   			 perPageOptions: [2,4, 6]
  		},
  		writers: {
   		_rowWriter: screenplays.WriterPaginator
  		},
  		readers: {
    _rowReader: screenplays.ReaderPaginator
  },
  params: {
    records: 'palabras'
  }
});
	 },
	 ReaderPaginator:function(index, li, record){
	   var $li = $(li);
  		 var $li = $(li);
  record.make = $li.find('span').text();
  record.Question = $li.find("#Question").text(); //get question
  record.id = $li.find(".panel-heading")[0].id; // get id ej heading0
  record.collapse = $li.find(".panel-title").find('a')[0].hash.split("#")[1];//get collapse ej collapse0
  record.Answer= $li.find("#Answer").text(); //get Answer
	 },
	  WriterPaginator:function(rowIndex, record, columns, cellWriter){
	   var cssClass = "Item", li;
  if (rowIndex % 3 === 0) { cssClass += ' first'; }
  li = '<li class="' + cssClass + '"><div class="panel panel-default"><div class="panel-heading" role="tab" id="'+record.id+'"><div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#'+record.collapse +'" aria-expanded="false" aria-controls="'+record.collapse +'"><i class="keyquestions-ico"></i><h3 id ="question">'+record.Question+'</h3></a></div></div><div id="'+record.collapse +'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="'+record.id+'"><div class="panel-body"><p id="Answer">'+record.Answer+'</p></div></div></div>';
  return li;
	 },

	click:function(ctrl){
		var category = document.getElementsByClassName("accordion-title")[0].getAttribute('id');	
		var uno =document.getElementsByClassName("accordion-title");
		var title =  document.querySelectorAll('#Categorys a');
		for (var i=0; i< uno.length; i++){
			var ID = uno[i].getAttribute('id');
			document.getElementById(ID).style.display = "none";
		}
		for (var i=0; i< title.length; i++){
		title[i].className = "MenuItemCategory"
		}

		var Name = ctrl.attributes.category.value;
		document.getElementById(Name).style.display = "block";
		ctrl.className = "MenuItemCategory active"
//		screenplays.InitPaginator(Name);
	}
}
function CloseAcordeon(){
	var accordions = document.querySelectorAll('.accordion-title a');
	console.log(accordions);
	for(var i = 0; i <  accordions.length;i++){
		var accordion = accordions[i];
		accordion.setAttribute('class','collapsed');
		
	}
$('.accordion-title a').click(function(){
    if($(this).hasClass('collapsed')){
        var index = $(this).parents('.panel').index();
        $(String.format('.accordion-title div.panel:not(:nth-of-type({0})) a',(index + 1))).each(function(){
            $(this).addClass('collapsed');
        })
         $(String.format('.accordion-title div.panel:not(:nth-of-type({0})) .panel-collapse',(index + 1))).each(function(){
            $(this).removeClass('in');
        })
    }
})
}

