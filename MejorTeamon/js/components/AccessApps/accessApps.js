﻿var accessApps = {
	//create parameters
	parameters:{
			listName: "Accesos Rápidos",
			divID:"accessApps",
			link:"<div class='col-md-4 col-sm-4 col-xs-12 info-apps'><a href='{0}' target='_blank'><img src='{1}' class='img-apps img-responsive' /><p>{2}</p></a></div>",
			bodyImg:"",
			query: "?$select=Title,Icono,Direccion,AreaNegocio,Padre&$orderby=AreaNegocio&$filter=AreaNegocio ne null",
			bodyArea: "<div class='col-md-12 col-sm-12 col-xs-12'><div class='apps-detail-area' id='{0}'><h1>{0}</h1>{1}</div></div>",
	
	},
	
	initialize:function(){
		try {
		
			var html = "";			
						
			//running the query with parameters
			sputilities.getItems(accessApps.parameters.listName,accessApps.parameters.query,function(data){
				
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				
				//loop the array
				for (var i=0; i< resultItems.length; i++){
				
				var currentItem = resultItems[i];
				var nextItem = resultItems[i+1];
				
					if (currentItem.Direccion != null){
						

						//retrieving the fields of the results
						var image = currentItem.Icono.Url;
						var url = currentItem.Direccion.Url;					
						var title = currentItem.Title;
							
						//adding values to the parameters
						var linkNew = String.format(accessApps.parameters.link,url,image,title);					
						
						//adding values of parameters to the html variable
						html += linkNew;						
						
						//evaluating if the field "AreaNegocio" of the current item is the same to the next item
						if(nextItem != null && currentItem.AreaNegocio != nextItem.AreaNegocio){ 
							Print();
						}
						//printing the last bussinessArea
						if(nextItem == null || nextItem == undefined){ 
							Print();
						}
						
					}
					
					
				}
			
			//function to print bussiness areas and elements into the bussiness areas
			function Print(){
				document.getElementById(accessApps.parameters.divID).insertAdjacentHTML('beforeend',String.format(accessApps.parameters.bodyArea,currentItem.AreaNegocio, html));						
				//document.getElementById(currentItem.AreaNegocio).insertAdjacentHTML('beforeend',html);
				html ="";
			}		
			
			},function(xhr){ console.log(xhr)});
		}
		catch (e){
			console.log(e);
		}
	}
}
accessApps.initialize();