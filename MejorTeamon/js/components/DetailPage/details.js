/*
 -- component: details  --
 -- Parameters:
 		type = Name of the parameter where the element type is sent
 		id  = Name of the parameter where the item id is sent
 		listName  = Name of the list where the items will be taken
 		parameterlistName  = Name of the parameterization list of details
 		query  = Query rest with which to call the item in the base list
 -- Calls: 
 		"details.parameters.containerId = 'Container name of the item'
 		"details.parameters.type = 'Name of the parameter'"
 		"details.parameters.id = 'Name of the parameter'"
 		"details.parameters.parameterslistName = 'Name of the parameter list'"
 		"details.parameters.listName = 'Name of the list where we will take the item'"
 		"details.initialize();"
 -- Process: 
 			The initialize method takes the type parameter and queries its value in the TYPE column of the list in the parameter
  			ParameterlistName, this item extracts the values from the Fields and Html column, with the fields value a new query is performed
  			Rest to the list of items in the listName parameter, selecting from the query the fields contained in the fields field and filtering
  			For the contained id in the parameter "id", the result is inserted in the variable that contains the HTML field of the list of parameters
  			And its result is inserted in the html code 			 
		
 
 */

var details = {
	parameters:{
		containerId:'content-page-detail',
		containerIdLikes:'comments-component',
		type:'Tipo',
		id:'IDItem',
		listName:'ListaBase',
		parameterlistName:'ParametrosDetalle',
		query:"?$filter=Title eq '{0}'",
		idSessionName:"detailPageSessionItems"
	},
	initialize:function(){
		var itemType = utilities.getQueryString(details.parameters.type);
		var itemId = utilities.getQueryString(details.parameters.id);
		var listName = utilities.getQueryString(details.parameters.listName);
		var query = String.format(details.parameters.query,itemType);
		var sessionName = utilities.getQueryString(details.parameters.idSessionName);
		var sessionItems =  sessionName != null && sessionName != "" ? sessionStorage[sessionName] : null;
		
		if(sessionItems != null){
			var items = JSON.parse(sessionItems);
			try{
				sputilities.getItems(details.parameters.parameterlistName,query,function(data){
					var detail = JSON.parse(data).d.results[0];		
					var fields = detail.Campos.split(',');
					var formatHtml = detail.HTML;
					var html = "";
					for(var i =0; i < items.length; i++){
						var item = items[i];
						var itemHtml =  formatHtml;
						for(var  j = 0; j < fields.length;j++){
							var fieldNew = fields[j];							
							var field = String.format('@{0}',fields[j]);
						    itemHtml = itemHtml.replaceAll(field,item[fields[j]]);							
						}
						html += itemHtml;
					}
					document.getElementById(details.parameters.containerId).innerHTML = html;
					if(document.getElementById(details.parameters.containerId).querySelector('script') != null){
						eval(document.getElementById(details.parameters.containerId).querySelector('script').innerHTML);
				 	}
				}
				,null);
			}
			catch(e){
				console.log(e);
			}
		}
		else{
			try{
				sputilities.getItems(details.parameters.parameterlistName,query,function(data){
					var item = JSON.parse(data).d.results[0];		
					var fields = item.Campos.split(',');
					var html = item.HTML;
					var query = String.format("?$select={0}&$filter=ID eq '{1}'",item.Campos,itemId);			
					sputilities.getItems(listName,query,function(data){
						var item = JSON.parse(data).d.results[0];
						for(var  i = 0; i< fields.length;i++){
						
							if(fields[i].indexOf('/') > 0)
							{
								var fieldNew = fields[i].split('/')[0];							
								var aditional = fields[i].split('/')[1];							
								var field = String.format('@{0}',fieldNew);
							    html = html.replaceAll(field,item[fieldNew][aditional]);						    
							}
							else
							{
								var fieldNew = fields[i];							
								var field = String.format('@{0}',fields[i]);
							    html = html.replaceAll(field,item[fields[i]]);
							}						
						}
				 		document.getElementById(details.parameters.containerId).innerHTML = html;
				 		if(document.getElementById(details.parameters.containerIdLikes) != null)
				 		{
				 			lagash.components.likesComments.initialize();
				 		}
				 		if(document.getElementById(details.parameters.containerId).querySelector('script') != null){
					 		eval(document.getElementById(details.parameters.containerId).querySelector('script').innerHTML);
				 		}		
					},null);		
				},null);	
			}catch(e){
				console.log(String.format('Ocurri� un error en inesperado en m�todo "details.initialize"',e))
			}
		}
	}
}
details.initialize();