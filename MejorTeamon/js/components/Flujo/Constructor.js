﻿sputilities.callFunctionOnLoadBody("Content.initialize");

var Content = {
	//create parameters
	parameters:{
	
			listName: "Capacitaciones",
			top: 5,
			ContentT : 'la imagen',
			divselect:"Capacitaciones",
			divinput:"Fechas",			
			NameField:"NombreCapacitaciones",
			contentType: "Todos",
			Divcontent:"<Select id='SelectName' onchange='javascript:Content.click(this);'><option value='Seleccione..'>Seleccione..</option><option value='{0}'>{0}</option>",
			DivHour:"<div class='form-group row form-checkbox' sp-static-name='Eleccion' sp-required='true' sp-type='multichoice'> <label class='col-sm-4 control-label'>Fecha Capacitacion<i>*</i></label><div class='col-sm-8'><div class='form-control-help'><div class='list-checkbox-radio'><div class='checkbox' id={1} style='display:none;'>",
			DivHourSecond:"<div class='checkbox' id={1} style='display:none;'>",
			DivSelectInput:"<label><input type='checkbox' id={1} value='{0}'>{0}</label><br>",
			DivOption:"<option  value='{0}'>{0}</option>",
			query:"?$select=*,NombreCapacitacion,CuposCapacitaciones&$filter=(CuposCapacitaciones gt 0)&$orderby=NombreCapacitacion",
			resultItems:null
	
	},	
	initialize:function(){
		try {
		var htmll="";
		var html="";

			//adding parameters to query
			var linkQuery = String.format(Content.parameters.query,Content.parameters.contentType);
			
			//running the query with parameters
				sputilities.getItems(Content.parameters.listName,Content.parameters.query,function(data){
				console.log("ingresa")
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				
				console.log(resultItems); 
				
				for (var i=0; i< resultItems.length; i++){
				var currentItem = resultItems[i];
				var previousItem = resultItems[i-1];
				var nextItem = resultItems[i+1];
				var Nombre = currentItem.NombreCapacitacion;
				var Cupos = currentItem.CuposCapacitaciones;
				var FechaCurrent= currentItem.FechaCapacitacion;
				var CurrentDate = new Date(FechaCurrent);
			    var Fecha = CurrentDate.format("dd/MM/yyyy hh:mm tt")
					
					
					if (i== 0)
					{
                        html += String.format(Content.parameters.Divcontent,Nombre);
                        //document.getElementById(Content.parameters.divID).innerHTML = htmll;
					}
					else
					{
						if(previousItem != undefined && Nombre != previousItem.NombreCapacitacion && nextItem != undefined){ 
							html += String.format(Content.parameters.DivOption,Nombre);
							 
						//document.getElementById(Content.parameters.divID).innerHTML = htmll;
						}
						}
						
							if (i== 0)
					{
						var IdDiv = Nombre.split(" ")[0];
                        htmll += String.format(Content.parameters.DivHour,Fecha,IdDiv);
                        //document.getElementById(Content.parameters.divID).innerHTML = htmll;
					}
					else
					{
						if(previousItem != undefined && Nombre != previousItem.NombreCapacitacion && nextItem != undefined){ 
                        htmll += "</div>" + String.format(Content.parameters.DivHourSecond,Fecha,Nombre);
						}
						}

					if(i == resultItems.length){
						html += String.format(Content.parameters.DivOption,Nombre)+"</select>";
						htmll += String.format(Content.parameters.DivHour,Fecha,Nombre)+"</div></div></div></div>";
						
					}
					else
					{ 
				
                        if(i==0){
                     htmll += String.format(Content.parameters.DivSelectInput,Fecha,Nombre);
                    }
                    else{
					  htmll += String.format(Content.parameters.DivSelectInput,Fecha,Nombre);
					}
					
                    }

				
				}
				
				document.getElementById(Content.parameters.divselect).innerHTML = html;
				document.getElementById(Content.parameters.divinput).innerHTML = htmll;

				
			
			},
			);
		}
		catch (e){
			console.log(e);
		}
	},
	click:function(ctrl){
	 	var SelectCapacitacion = document.getElementById("SelectName").value;
		var FechasForm =document.getElementsByClassName("checkbox");
		for (var i=0; i< FechasForm.length; i++){
			var ID = FechasForm[i].getAttribute('id');
			document.getElementById(ID).style.display = "none";

		}
		var Name = SelectCapacitacion .split(" ")[0];
		document.getElementById(Name).style.display = "block";
//		screenplays.InitPaginator(Name);
	}


	
}