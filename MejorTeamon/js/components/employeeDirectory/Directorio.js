﻿var directory = {
	//create parameters
	parameters:{
			rowlimit: 10,
			containerId:"divUserProfiles",
			headerImg:"/sites/Intranet/SiteAssets/Development/images/components/employeeDirectory/ico-directory.png",
			linkImg: "/sites/Intranet/SiteAssets/Development/images/components/employeeDirectory/imagePerson.png",
			inputID: "txtSearchBox",
			queryText:"%22{0}%22",
			query: "?querytext='{0}'&rowlimit={1}&selectproperties='PreferredName,PictureURL,Title,WorkPhone,WorkEmail,RefinableString00,Department,Path,JobTitle'&sortlist='RefinableString00:ascending'&sourceid=%27b09a7990-05ea-4af9-81ef-edfab16c4e31%27",
			infoFormatHTML:"Cargo: {0}</br>Area: {1}</br>Telefono: {2}</br>Email: {3}",
			errMsg: "Ocurrió un error en la ejecución del método {0} {1}",
			errMethod: 'directory.initialize()'
	},	
	initialize:function(){
		try {	
			//get directory container
			var directoryContainer = document.getElementById('module-directory');			
			//create directory header
			var directoryHeader = document.createElement('div');
			directoryHeader.setAttribute('class','directory-header');
			//create search box img and insert in header
			var searchBoxImg = document.createElement('img');
			searchBoxImg.setAttribute('src',directory.parameters.headerImg);
			directoryHeader.appendChild(searchBoxImg);
			//create search box and insert in header
			var searchBox = document.createElement('input');
			searchBox.setAttribute('type','text');
			searchBox.setAttribute('placeholder','Digite el nombre a buscar');
			searchBox.setAttribute('class','directory-formname');
			searchBox.setAttribute('id','txtSearchBox');
			directoryHeader.appendChild(searchBox);
			//create search box button and insert in header
			var searchBoxBtn = document.createElement('input');
			searchBoxBtn.setAttribute('type','button');
			searchBoxBtn.setAttribute('value','Buscar usuario');
			searchBoxBtn.setAttribute('class','directory-button');
			searchBoxBtn.setAttribute('id','btnSearch');
			directoryHeader.appendChild(searchBoxBtn);
			//insert directory header in Container
			directoryContainer.appendChild(directoryHeader);
			//create list container
			var listContainer = document.createElement('div');
			listContainer.setAttribute('id',directory.parameters.containerId);
			listContainer.setAttribute('class','directory-list');
			directoryContainer.appendChild(listContainer);
			//excecute search by letter A
			directory.getAllUsers("A",listContainer);
			//create event click for searhc Button
			searchBoxBtn.addEventListener('click',function(){
				var searchTerm = document.getElementById(directory.parameters.inputID).value;                                                                                                                                                     
	            directory.getAllUsers(searchTerm.toUpperCase(),listContainer);
			});		
		}
		catch (e){
			console.log(String.format(directory.parameters.errMsg,directory.parameters.errMethod,e));
		}
	},
	getAllUsers:function(searchTerm,listContainer){			
		//create list of results
		var resultsList = document.createElement('ul');
		resultsList.style.listStyleType = 'none';
		resultsList.setAttribute('id','resultsList');
		//adding parameters to query
		var textQuery = String.format(directory.parameters.queryText,searchTerm)
		var linkQuery = String.format(directory.parameters.query,textQuery,directory.parameters.rowlimit);			
		//running the query with parameters
		sputilities.getPeople(linkQuery,function(data){
			//save the results to an array
			var resultItems = JSON.parse(data).d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results;
			try{
				//loop the array
				for (var i=0; i< resultItems.length; i++){					
					var currentItem = resultItems[i];
					var propertiesUsers = currentItem.Cells.results;
					//create item Container  
					var itemListContainer = document.createElement('li');
					itemListContainer.setAttribute('class','itemList');
					//create user container
					var userContainer = document.createElement('div');
					userContainer.setAttribute('class','element-person')
					//create and insert Img User
					var imgUser = document.createElement('img');
					if(propertiesUsers[3].Value == null){
						imgUser.setAttribute('src',directory.parameters.linkImg);
					}else{
						imgUser.setAttribute('src',propertiesUsers[3].Value);
					}
					userContainer.appendChild(imgUser);					
					//create profile information container
					var profileUserInfo = document.createElement('div');
					profileUserInfo.setAttribute('class','info-person');
					//create  Profil User link
					var profileUser = document.createElement('a');
					profileUser.setAttribute('href',propertiesUsers[9].Value)
					profileUser.setAttribute('target','_blank');
					//create user container and insert userName
					var preferredName = document.createElement('h4');
					var textPreferredName = document.createTextNode(propertiesUsers[2].Value);
					preferredName.appendChild(textPreferredName);
					profileUser.appendChild(preferredName);
					profileUserInfo.appendChild(profileUser);					
					//create and insert other information
					var otherInfo = document.createElement('p');
					var infoText = String.format(directory.parameters.infoFormatHTML,propertiesUsers[10].Value,
											propertiesUsers[8].Value,propertiesUsers[5].Value,propertiesUsers[6].Value);
					//var textOtherInfo = document.createTextNode(infoText);
					//otherInfo.appendChild(textOtherInfo);
					otherInfo.innerHTML = infoText;
					profileUserInfo.appendChild(otherInfo);
					//insert user information in user Container
					userContainer.appendChild(profileUserInfo);				
					// append item in list
					itemListContainer.appendChild(userContainer);
					resultsList.appendChild(itemListContainer);
					//
				}				
				//insert all results in directory container	
				listContainer.innerHTML = "";			
				listContainer.appendChild(resultsList);
				directory.paginatorMode();
			}catch(e){
				directory.parameters.errMethod = 'directory.getAllItems';
				throw e
			}	
		},function(data){ console.log(JSON.parse(data).error.message.value)});
	},
	paginatorMode:function(){
		try{
			// Function that renders the list items from our records
			function ulWriter(rowIndex, record, columns, cellWriter) {
			  var cssClass = "itemList", li;
			  if (rowIndex % 3 === 0) { cssClass += ' first'; }
			  li = '<li class="' + cssClass + '"><div class="element-person">' + record.caption + '</div></li>';
			  return li;
			}

			// Function that creates our records from the DOM when the page is loaded
			function ulReader(index, li, record) {
			  var $li = $(li),
			      $caption = $li.find('.element-person');			  
			  record.caption = $caption.html();			  
			}

			$('#resultsList').dynatable({
			  table: {
			    bodyRowSelector: 'li'
			  },
			  dataset: {
			    perPageDefault: 2,
			    perPageOptions: [2, 6, 12]
			  },
			  writers: {
			    _rowWriter: ulWriter
			  },
			  readers: {
			    _rowReader: ulReader
			  },
			  params: {
				  records: 'resultados'
			  }
			});


		}catch(e){
			throw e
		}
	}
}
directory.initialize();