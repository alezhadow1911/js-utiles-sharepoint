﻿var imgGallery = {	 
	 parameters:{
		 urlParameters:{
			 listName:'ListaBase',
			 albumName:'nombreFolder'
		},
		 containerId:"imgGallery",
		 tagCollection:"div",
		 tagFolder:"article",
		 formatDialog:'imgGallery.showDetail("{0}","{1}/Paginas/Detalle.aspx?ListaBase={2}&IDItem={3}&Tipo=DetalleGaleria&detailPageSessionItems={4}",this)',
		 queryFolder:"?$select=Title,Id,FileDirRef,FileLeafRef,FileRef,FileType,Description,ContentType/Name&$expand=ContentType",
		 formatBackImg:"{0}/SiteAssets/Development/images/components/gallery/vovler.png",
	     classContainerImg : "col-md-3 col-sm-6 col-xs-12",
	     classPrincipalContainer:"cont-img",
	     onLoadImage:"(event.srcElement ? event.srcElement : event.target).style.visibility = 'visible'; SP.ScriptHelpers.resizeImageToSquareLength(this, {0})",
	     onErrorImage:"displayGenericDocumentIcon(event.srcElement ? event.srcElement : event.target, 0); return false;",
	     sizeSquareImage:250,
	     sessionName:"detailGalleryitems",
	     albumItems:[]
	 },
	 initialize:function(){
		//take the name of the list to consult and forder name
		var listName = utilities.getQueryString(imgGallery.parameters.urlParameters.listName);
		var albumName = utilities.getQueryString(imgGallery.parameters.urlParameters.albumName);
		//execut rest query
		sputilities.getItems(listName,imgGallery.parameters.queryFolder,function(data){
			var sessionItems = [];
			var data = JSON.parse(data).d.results;
			//create the album container
			var albumCollection = document.createElement(imgGallery.parameters.tagCollection);
			albumCollection.setAttribute("id","albumCollection");
			albumCollection.setAttribute("class","collection visible row");			
			try{
				for(var i = 0;i<data.length;i++){
					//identify the albums 
					if(data[i].ContentType.Name == 'Carpeta'){
						//create folder container
						var folder = document.createElement(imgGallery.parameters.tagFolder);
						folder.setAttribute("id",String.format("{0}Item",data[i].FileLeafRef));												
						folder.setAttribute('class','col-md-4 col-sm-6 col-xs-12');	
						//create img folder collection
						var imgCollection = document.createElement(imgGallery.parameters.tagCollection);
						imgCollection.setAttribute("id",String.format("{0}Collection",data[i].FileLeafRef));
						imgCollection.setAttribute("class","collection hidden img-responsive");						
						//search in the result all the images that correspond to the current folder
						var counter = 0; //identify the first element
						
						for(var j=0;j<data.length;j++){
							// build image rendition to lite weigth
							var segments = data[j].FileRef.split('/');
							var segmentName = String.format("{0}.{1}",segments[segments.length -1].replace('.','_'),data[j].FileType);
							segments[segments.length  -1] = "_w";
							segments.push(segmentName);
							if(i == 0){
								var objItem = {
									Id:data[j].Id,
									Title:data[j].Title,
									FileRef:data[j].FileRef,
									Description: data[j].Description
								}
								var pOfFileDirRef = data[j].FileDirRef.split('/');
								var ssfolderName  = pOfFileDirRef[pOfFileDirRef.length -1]
								if(imgGallery.parameters.albumItems[ssfolderName] == null){
									imgGallery.parameters.albumItems[ssfolderName] = [];
								}						
								imgGallery.parameters.albumItems[ssfolderName].push(objItem);	
							}
							
							if(data[i].FileRef == data[j].FileDirRef && counter == 0){
								
								counter = 1;
								data[j].FileRef = segments.join('/');
								var containerDiv = document.createElement('div');
								var imgCoverPage = document.createElement('img');
								imgCoverPage.setAttribute("class","coverPage img-responsive");
								imgCoverPage.setAttribute("onclick",String.format("imgGallery.getImgs('{0}')",data[i].FileLeafRef));
								imgCoverPage.setAttribute("src",data[j].FileRef);								
								var nameContainer = document.createElement('h1');
								var folderName = document.createTextNode(data[i].FileLeafRef);
								nameContainer.appendChild(folderName);
								var imgFolderContent = document.createElement('img');
								imgFolderContent.setAttribute("onload",String.format(imgGallery.parameters.onLoadImage,imgGallery.parameters.sizeSquareImage));
								imgFolderContent.setAttribute("src",data[j].FileRef);
								imgFolderContent.setAttribute('onerror',imgGallery.parameters.onErrorImage);					
								imgFolderContent.setAttribute('onclick',String.format(imgGallery.parameters.formatDialog,data[i].FileLeafRef,sputilities.contextInfo().webAbsoluteUrl,listName,data[j].Id,imgGallery.parameters.sessionName));								
								//Insert elements
								containerDiv.setAttribute('class',imgGallery.parameters.classContainerImg);
								var contImg = document.createElement('div');
								contImg.setAttribute('class',imgGallery.parameters.classPrincipalContainer);
								contImg.appendChild(imgFolderContent);
								containerDiv.appendChild(contImg);
								imgCollection.appendChild(containerDiv);
								folder.appendChild(imgCoverPage);
								folder.appendChild(nameContainer);
								albumCollection.appendChild(folder);
								//dentify the remaining elements of the folder
							}else if(data[i].FileRef == data[j].FileDirRef && counter == 1){
								data[j].FileRef = segments.join('/');
								var containerDiv = document.createElement('div');
								var imgFolderContent = document.createElement('img');
								imgFolderContent.setAttribute("onload",String.format(imgGallery.parameters.onLoadImage,imgGallery.parameters.sizeSquareImage));
								imgFolderContent.setAttribute('onerror',imgGallery.parameters.onErrorImage);
								imgFolderContent.setAttribute("src",data[j].FileRef);
								imgFolderContent.setAttribute('onclick',String.format(imgGallery.parameters.formatDialog,data[i].FileLeafRef,sputilities.contextInfo().webAbsoluteUrl,listName,data[j].Id,imgGallery.parameters.sessionName));
								containerDiv.setAttribute('class',imgGallery.parameters.classContainerImg);
								var contImg = document.createElement('div');
								contImg.setAttribute('class',imgGallery.parameters.classPrincipalContainer);
								contImg.appendChild(imgFolderContent);
								containerDiv.appendChild(contImg);
								imgCollection.appendChild(containerDiv);						
							}								
						}						
					}
					document.getElementById(imgGallery.parameters.containerId).appendChild(imgCollection);
				}
				document.getElementById(imgGallery.parameters.containerId).appendChild(albumCollection);
			}catch(e){
				console.log(String.format("Ocurrió un error en la ejecución del método imgGallery.initialize() {0}",e))
			}
			imgGallery.getImgs(albumName);
		},function(data){
			console.log(JSON.parse(data).error.message.value)
		})		
	 },
	 getImgs:function(folder){
		//change the class of section 
		var albumContainer = document.getElementById("albumCollection");
		albumContainer.setAttribute("class","collection hidden");
		var imgContainer = document.getElementById(String.format("{0}Collection",folder));
		imgContainer.setAttribute("class","collection visible row");
		
		if(imgContainer.querySelectorAll('[data-name="back"]').length == 0){
		//crate and Insert back link
			var backElement = document.createElement("a");
			backElement.setAttribute('data-name','back');
			//var backValue = document.createTextNode("volver");
			var imgBack = document.createElement('img');
			imgBack.src = String.format(imgGallery.parameters.formatBackImg,sputilities.contextInfo().webAbsoluteUrl);
			backElement.appendChild(imgBack);
			//backElement.appendChild(backValue);
			backElement.setAttribute("href",String.format("javascript:imgGallery.imgSetBack('{0}')",folder))
			backElement.setAttribute('data-album',folder);
			//insert back link
			 var firstChild = imgContainer.childNodes[0];
			firstChild.parentNode.insertBefore(backElement,firstChild); 
			//document.getElementById(String.format('{0}Collection',folder)).appendChild(backElement);	
		}	
	 },
	 imgSetBack:function(folder){		 
	 //change the class of section 
		var albumContainer = document.getElementById('albumCollection');
		albumContainer.setAttribute('class','collection visible row');
		var imgContainer = document.getElementById(String.format('{0}Collection',folder));
		imgContainer.setAttribute('class','collection hidden');		
	},
	showDetail:function(title, url, ctrl){
		sessionStorage[imgGallery.parameters.sessionName] = JSON.stringify(imgGallery.parameters.albumItems[ctrl.parentNode.parentNode.parentNode.querySelector('[data-name="back"]').getAttribute('data-album')])
		sputilities.showModalDialog(title, url);
	}				
 }  
 imgGallery.initialize();