
function initStyleGallery(){
    var slider = document.getElementById('gallery-slide-range');
    slider.value = slider.max/2;
    document.getElementById('gallery-slide-progress').querySelector('div').innerHTML= String.format('{0} / {1}',Math.ceil(slider.value/10),(slider.max/10));
    document.getElementById('gallery-slide-max').querySelector('div').innerHTML= (slider.max/10);
    slider.addEventListener('input',function(){
        moveImage(this);
    });
    slider.addEventListener('click',function(){
        moveImage(this);
    });
    slider.click();
}

function nextImg(){
    var slider = document.getElementById('gallery-slide-range');
    slider.value=parseInt(slider.value) + 10;
    slider.click()
}
function prevImg(){
    var slider = document.getElementById('gallery-slide-range');
    slider.value= slider.value - 10;
    slider.click()
}

function moveImage(ctrl){
    var rVal = Math.ceil(ctrl.value/10);
	document.getElementById('gallery-slide-progress').querySelector('div').innerHTML= String.format('{0} / {1}', rVal,(ctrl.max/10));
    var images = document.querySelectorAll('.gallery-image');
    for(var i = 0; i < images.length ; i++){
        var image = images[i];
        var percent = Math.ceil((300*100)/document.body.clientWidth);
        console.log(percent)
        if(rVal != 1){
            image.style.left = String.format('{0}%', (-(percent*(rVal-1))));
        } else{
            image.style.left = "5%";
        }
    }
}

initStyleGallery();