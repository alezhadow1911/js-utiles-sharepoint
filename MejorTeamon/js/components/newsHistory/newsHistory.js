﻿var newsHistory={
	//create main parameters
	parameters:{
		listName:"Páginas",
		containerId:"module-newsInt",
		query:"?$select=FileRef,ID,ContentType/Name,Title,Entradilla,sys_txtImg,FechaInicio,FechaFinal&$expand=ContentType&$filter="
				+"(ContentType eq 'Noticias' and FechaFinal le datetime'{0}')",	
		yearControlHTMLformat:"<label>Año</label><select id='year' onchange='newsHistory.getFilteredItems(this)'><option value='Todos'>Todos</option></select>",
		monthControlHTMLformat:"<label>Mes</label><select id='month' onchange='newsHistory.getFilteredItems(this)'><option value='Todos'>Todos</option></select> ",
		searchControlHTMLformat:"<label>Buscar</label><input id='searchBox' type='text' placeholder='búsqueda'/>",
		paginatorControlHTMLformat:"<label>Resultados</label><select id='itemsBypage' onchange='newsHistory.getFilteredItems(this)'><option value='5'>5</option>"
									+"<option value='10'>10</option><option value='15'>15</option></select>",		
		//linkpopUp: "/sites/Intranet/Paginas/Detalle.aspx?ListaBase={0}&IDItem={1}&Tipo=Noticia&ListaLikes=%22%22",
		msgError: "Ah ocurrido un error en la ejecucion del metodo {0}, {1}",
		msgMethod:	"newsHistory.initialize"	
	},
	initialize:function(){
		try{
			document.getElementById(newsHistory.parameters.containerId).innerHTML ="";
			var container = document.getElementById(newsHistory.parameters.containerId);
			//create the main container
			newsHistory.createMainContainer(container);
			var listNews = document.getElementById('newsInt-list');
			var listYears = document.getElementById('year');
			var listMonth = document.getElementById('month');
			var listPage = document.getElementById('itemsBypage');

			var searchBox = document.getElementById('searchBox')		
			searchBox.addEventListener('keypress', function (e) {			
				if (e.keyCode == 13) { 
					e.preventDefault();					
					newsHistory.getFilteredItems(searchBox);
				}			
			});
			//get date and execute query
			var today = new Date();
			today = today.toISOString();
			finalQuery = String.format(newsHistory.parameters.query,today);
			
			sputilities.getItems(newsHistory.parameters.listName,finalQuery,function(data){
				var items = JSON.parse(data).d.results;
				var dataYears = {};
				var dataMonths =["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
				var yearOptions= [];
				var monthOptions=[];
				var lastYear =0;
				var lastMonth = 0;
			
				for(var i=0;i<items.length;i++){
					var item = items[i];
					console.log(item)
					//get date
					var date = new Date(item.FechaInicio);
					var month = date.getMonth()+1;
					var year = date.getFullYear();
					var url = item.FileRef;//String.format(newsHistory.parameters.linkpopUp,newsHistory.parameters.listName,item.ID);					
					//create news container
					var newsContainer = document.createElement('div');
					newsContainer.setAttribute('class','newsInt-item');
					newsContainer.setAttribute('data-year',year);
					newsContainer.setAttribute('data-month',month);					
					//taking the url of the image
					if(item.sys_txtImg !=null){
						var image = item.sys_txtImg;
						var div = document.createElement('div');
						div.innerHTML = image;
						var urlImg = div.querySelector('img').src;
						//create news image
						var newsImg = document.createElement('img');
						newsImg.setAttribute('src',urlImg);
						newsImg.setAttribute('class','newsImg');
						newsContainer.appendChild(newsImg);
					}
					//create news information container
					var infoContainer = document.createElement('div');
					infoContainer.setAttribute('class','newsInt-item-info');
					newsContainer.appendChild(infoContainer);					
					//create link popUP
					var newsDetails = document.createElement('a');
					newsDetails.setAttribute('href',url);
					infoContainer.appendChild(newsDetails);
					//create news title
					var newsTitle = document.createElement('h3');					
					var title = document.createTextNode(item.Title);
					newsTitle.appendChild(title);
					newsDetails.appendChild(newsTitle);
					infoContainer.appendChild(newsDetails);					
					//create news text
					var newsText = document.createElement('p');					
					var text = document.createTextNode(item.Entradilla);
					newsText.appendChild(text);
					infoContainer.appendChild(newsText);
					listNews.appendChild(newsContainer);
											
					//getOptions
					if(yearOptions.indexOf(year) == -1){
						if(lastYear != year)
						{
							var yearNews = {"text":year,
							"value": year};
							yearOptions.push(yearNews);
							lastYear = year;
						}
					}					
					if(monthOptions.map(function(d) { return d['value']; }).indexOf(month) == -1){
						
						if(lastMonth != month)
						{
							var monthsNews = {"value": month,"text":dataMonths[month-1]};
							monthOptions.push(monthsNews);
							lastMonth =month;
						}
					}					
				}		
			//create Options select
			newsHistory.createOptionSelect(yearOptions,listYears);
			newsHistory.createOptionSelect(monthOptions,listMonth);	
			newsHistory.getFilteredItems(listYears);	
			},function(data){
				console.log(JSON.parse(data).error.message.value)
			})		
		}catch(e){
			console.log(String.format(newsHistory.parameters.msgError,newsHistory.parameters.msgMethod,e))
		}
	},createOptionSelect:function(data,list){
		try{
			for(var i=0;i<data.length;i++){
				var selectOption = document.createElement('option');
				selectOption.value = data[i].value;
				var textOption = document.createTextNode(data[i].text);
				selectOption.appendChild(textOption);
				list.appendChild(selectOption);		
			}
		}catch(e){
			if(newsHistory.parameters.msgMethod == 'newsHistory.initialize'){
				newsHistory.parameters.msgMethod = 'newsHistory.createOptionSelect';
			}
			throw e;
		}
	},
	createMainContainer:function(mainContainer){
		try{			
			//create controls section
			var controlsContainer = document.createElement('div');
			controlsContainer.setAttribute('class','news-controls');
			//create year container
			var yearContainer = document.createElement('div');
			yearContainer.setAttribute('class','news-controls-year');
			yearContainer.innerHTML = newsHistory.parameters.yearControlHTMLformat;
			controlsContainer.appendChild(yearContainer);
			//create month container
			var monthContainer = document.createElement('div');
			monthContainer.setAttribute('class','news-controls-month');
			monthContainer.innerHTML = newsHistory.parameters.monthControlHTMLformat;
			controlsContainer.appendChild(monthContainer);
			//create search container
			var searchContainer = document.createElement('div');
			searchContainer.setAttribute('class','news-controls-search');
			searchContainer.innerHTML = newsHistory.parameters.searchControlHTMLformat;
			controlsContainer.appendChild(searchContainer);
			//create paginator container
			var paginatorContainer = document.createElement('div');
			paginatorContainer.setAttribute('class','news-controls-paginator');
			paginatorContainer.innerHTML = newsHistory.parameters.paginatorControlHTMLformat;
			controlsContainer.appendChild(paginatorContainer);
			//create news list container 
			var newsListContainer = document.createElement('div');
			newsListContainer.setAttribute('class','newsInt-list');
			newsListContainer.id='newsInt-list';	
			//create paginator container
			var paginatorContainer = document.createElement('div');
			paginatorContainer.setAttribute('class','news-paginator-Numbers');
			paginatorContainer.id = 'paginator';
			//add controls, list and paginator to main Container
			mainContainer.appendChild(controlsContainer);
			mainContainer.appendChild(newsListContainer);
			mainContainer.appendChild(paginatorContainer);		
			
		}catch(e){
			if(newsHistory.parameters.msgMethod == 'newsHistory.initialize'){
				newsHistory.parameters.msgMethod = 'newsHistory.createMainContainer';
			}
			throw e;
		}
	},
	getFilteredItems:function(ctrl){
		//get values of filter
		var year = document.getElementById('year').value;
		var month = document.getElementById('month').value;
		var search = document.getElementById('searchBox').value;
		search = search.toUpperCase();
		var filteredNews=[];
		//get all element in news list
		if(ctrl.id == 'itemsBypage'){
			var news = document.querySelectorAll('[data-page]');
		}else{
			var news = document.getElementsByClassName('newsInt-item');
		}
		try{
			for(var i = 0;i<news.length;i++){
				var item = news[i];
				var contentItem = item.outerHTML;
				contentItem = contentItem.toUpperCase();
				//reset filter in document
				if(item.getAttribute('data-page') != null){
					item.removeAttribute('data-page');					
				}
				//Get items by filter	
				if(ctrl.id != 'itemsBypage'){
					switch(true){
						case(search != "" && contentItem.indexOf(search) != -1):
							filteredNews.push(item);
						break;
						case(year == 'Todos' && month == 'Todos' && search == ""):
						case(item.dataset.year == year && item.dataset.month == month && search == ""):
						case(year == 'Todos' && item.dataset.month == month && search == ""):
						case(item.dataset.year == year && month == 'Todos' && search == ""):
							filteredNews.push(item);
						break;
						default:
							item.classList.remove('visible');
							item.classList.add('hidden');
						break;
					}	
				}else{
					filteredNews.push(item);
				}				
			}
			newsHistory.getPaginatorModule(filteredNews);

		}catch(e){
			newsHistory.parameters.msgMethod = 'newsHistory.getFilteredItems';	
			throw e;
		}
	},
	getPaginatorModule:function(filteredItems){
		// get values for paginator mode
		var limitPage = Number(document.getElementById('itemsBypage').value);
		var limit = limitPage;
		if(filteredItems.length == 0){
			var footerFormat="No se encontraron Resultados" ;
			var paginator = document.getElementById('paginator');
		}else{
			var footerFormat="{0}{1}" ;
			//create msg de elementos en la pagina
			var paginator = document.getElementById('paginator');
			var msgResults = document.createElement('div');
			if(filteredItems.length > 1){
				var msgText = document.createTextNode(String.format('Se encontraron {0} noticias',filteredItems.length));
			}else{
				var msgText = document.createTextNode(String.format('Se encontro {0} noticia',filteredItems.length));
			}
			msgResults.appendChild(msgText);			
		};
		var page=1;
		try{
			//get page
			for (var i=0;i<filteredItems.length;i++){
				switch (true){
					case i < limit && i != (filteredItems.length-1):
						filteredItems[i].setAttribute('data-page',page);
						break;
					case i == limit && i != (filteredItems.length-1):
						var letterPage = String.format('<label class="paginator" onclick="newsHistory.goFromPage(this.innerText,this)">{0}</label>',page);
						footerFormat=String.format(footerFormat,letterPage,'{0}{1}');
						page +=1;
						limit +=limitPage;
						filteredItems[i].setAttribute('data-page',page);
						break;
					case i == limit && i == (filteredItems.length-1):
					    var letterPage = String.format('<label class="paginator" onclick="newsHistory.goFromPage(this.innerText,this)">{0}</label>',page);
						footerFormat=String.format(footerFormat,letterPage,'{0}{1}');
						page +=1;
						limit +=limitPage;
						filteredItems[i].setAttribute('data-page',page);
						var letterPage = String.format('<label class="paginator" onclick="newsHistory.goFromPage(this.innerText,this)">{0}</label>',page);
						footerFormat=String.format(footerFormat,letterPage,'');
						break;
					case i < limit && i == (filteredItems.length-1):
						filteredItems[i].setAttribute('data-page',page);
						var letterPage = String.format('<label class="paginator" onclick="newsHistory.goFromPage(this.innerText,this)">{0}</label>',page);
						footerFormat=String.format(footerFormat,letterPage,'');
						break;
				}		
			}			
			paginator.innerHTML = footerFormat;
			paginator.appendChild(msgResults);		
			newsHistory.goFromPage(1);

		}catch(e){
			newsHistory.parameters.msgMethod = 'newsHistory.getPaginatorModule';	
			throw e;	
		}
	},
	goFromPage:function(page,ctrl){
	if(ctrl == undefined){
			var ctrl = document.getElementsByClassName('paginator')[0];			
		}else{
			var activeControl = document.getElementsByClassName('active-page')[0];
			activeControl.classList.remove('active-page');
			activeControl.classList.add('paginator');
		}
		ctrl.classList.remove('paginator');
		ctrl.classList.add('active-page');	 
	 
		var itemsSelected = document.querySelectorAll('[data-page]');
		for(var i=0;i<itemsSelected.length;i++){
			if(itemsSelected[i].getAttribute('data-page') == page){
				itemsSelected[i].classList.remove('hidden');
				itemsSelected[i].classList.add('visible');
			}else{
				itemsSelected[i].classList.remove('visible');
				itemsSelected[i].classList.add('hidden');			}

		}
	}

}
newsHistory.initialize();