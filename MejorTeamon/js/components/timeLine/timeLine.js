﻿//component timeLine
var timeLine = {
    parameters:{
        htmlimage:'<img src="{0}"   data-year="{1}" class="{2} img-responsive">',
        htmlContentText:'<div data-year="{0}" class="{1} timeline-text-content col-sm-12 col-xs-12"><h2 class="text-year">{0}</h2><h3 class="text-subtittle">{2}</h3>{3}</div>',
        htmlitem:'<div class="{0} item-company"><img src="{1}" class="picture-company"><p>{2}<p></div>',        
        htmlSlideItem:'<div class="{0}" onclick=\"timeLine.getElementsByYear(this.innerText)\"><label>{1}</label></div>',
        tableClassName:'timeLine',
        
        urlLogoAviancataca:'/sites/Intranet/SiteAssets/Development/images/components/distributionTable/timeLine/aviancataca.png',
        urlLogoAviancaNuevo:'/sites/Intranet/SiteAssets/Development/images/components/distributionTable/timeLine/AviancaN.png',
		urlLogoAvianca:'/sites/Intranet/SiteAssets/Development/images/components/distributionTable/timeLine/logo-avianca.jpg',
		urlLogoTaca:'/sites/Intranet/SiteAssets/Development/images/components/distributionTable/timeLine/logo-taca.png',
		urlImagen:'/sites/Intranet/SiteAssets/Development/images/components/distributionTable/timeLine/img.jpg',
        errorMsg:'Ocurrion un error en la ejecusion del metodo {0} Error:{1}',
		errorMethod:'timeLine.initialize',
		containerId:"module-timeline",
		listName:"Historia Corporativa",
		query:"?$select=Anio,Title,PrimeraDescripcion,PrimerLogo,SegundaDescripcion,SegundoLogo,sys_txtImg&$orderby=Anio",
		results:null,
		loadItems:false,
		classHTML:{
			content:"timeline-content col-sm-12 col-xs-12",
			control:"timeline-control col-sm-12 col-xs-12",
			contentInfo:"timeline-text col-sm-12",
			imageContent:"timeline-image-content col-sm-12 col-xs-12",
			imageContainer:"timeline-image",			
			itemActive:"item-active",
			itemInactive:"item-inactive",
			slickClass:"slick-slide",
			selector:"[data-year]",
			hidden:"hidden",
			visible:"visible",
			classLogoInitial:"item-avianca",
			classLogoOther:"item-taca"
		},
		attributesHTML:{
		 	classHTML:"class",
		 	data:"data-year",
		 	click:"onclick"
		},
		tagHTML:{
			div:"div"
		}
		
    },
    initialize:function(){
    	timeLine.getItemsList();
    	var loadControl = setInterval(function(){								
			document.getElementById('ms-designer-ribbon').style.opacity = 0;
			document.getElementById('s4-workspace').style.opacity = 0;		
			
			if(timeLine.parameters.loadItems)
			{
				timeLine.createSlidesCarousel();
				clearInterval(loadControl);	
				document.getElementById('s4-workspace').style.opacity = 1;
				document.getElementById('ms-designer-ribbon').style.opacity = 1;
			}
		},500
		);
    },
    getItemsList:function(){
    	try{
			sputilities.getItems(timeLine.parameters.listName,timeLine.parameters.query,function(data){
				var results = JSON.parse(data).d.results;
				timeLine.parameters.resultItems = results;
				timeLine.parameters.loadItems=true;
			
			},function(data){
				console.log(JSON.parse(data).error.message.value)
			});
		}catch(e){
			console.log(String.format("Ocurrió un error inesperado en la ejecución de método marquee.initialize {0}",e))
		}

    },
    createSlidesCarousel:function(){       
        
		//create Timeline container
		var timeLineContainer = document.createElement(timeLine.parameters.tagHTML.div);
		timeLineContainer.setAttribute(timeLine.parameters.attributesHTML.classHTML,timeLine.parameters.classHTML.content);
        //create a carousel container
        var carouselContainer = document.createElement(timeLine.parameters.tagHTML.div);
        carouselContainer.setAttribute(timeLine.parameters.attributesHTML.classHTML,timeLine.parameters.classHTML.control);
		//create a image section
		var imgSection = document.createElement(timeLine.parameters.tagHTML.div);
		imgSection.setAttribute(timeLine.parameters.attributesHTML.classHTML,timeLine.parameters.classHTML.imageContainer);
		var imgContainer = document.createElement(timeLine.parameters.tagHTML.div);
		imgContainer.setAttribute(timeLine.parameters.attributesHTML.classHTML,timeLine.parameters.classHTML.imageContent);
		//
		//create info container
		var dataContainer = document.createElement(timeLine.parameters.tagHTML.div);
		dataContainer.setAttribute(timeLine.parameters.attributesHTML.classHTML,timeLine.parameters.classHTML.contentInfo);
        try{
        	var items = timeLine.parameters.resultItems;
        	if(items != null)
        	{
        		if(items.length > 0)
        		{
	        		for(var i=0;i < items.length;i++)
	        		{
	        			var item = items[i];
	        			var htmlImage ="";
	        			var htmlSlider="";
	        			var itemInfo="";
	        			var contentInfo="";
	        			if(i==0)
	        			{
	        				var image = timeLine.getImage(item.sys_txtImg);
	        				htmlImage= String.format(timeLine.parameters.htmlimage,image,item.Anio,timeLine.parameters.classHTML.visible);
	        				htmlSlider = String.format(timeLine.parameters.htmlSlideItem,timeLine.parameters.classHTML.itemActive,item.Anio);
	        				
	        				if(item.SegundaDescripcion != null  && item.SegundoLogo != null && item.PrimerLogo != null)
		        			{
		        				itemInfo += String.format(timeLine.parameters.htmlitem,timeLine.parameters.classHTML.classLogoInitial,item.PrimerLogo.Url,item.PrimeraDescripcion);
		        				itemInfo += String.format(timeLine.parameters.htmlitem,timeLine.parameters.classHTML.classLogoOther,item.SegundoLogo.Url,item.SegundaDescripcion);
		        				contentInfo = String.format(timeLine.parameters.htmlContentText,item.Anio,timeLine.parameters.classHTML.visible,item.Title,itemInfo);
		        			}
		        			else
		        			{
			        			if(item.PrimerLogo != null)
			        			{
		        					itemInfo += String.format(timeLine.parameters.htmlitem,timeLine.parameters.classHTML.classLogoInitial,item.PrimerLogo.Url,item.PrimeraDescripcion);
									contentInfo = String.format(timeLine.parameters.htmlContentText,item.Anio,timeLine.parameters.classHTML.visible,item.Title,itemInfo);
								}
		        			}
	
	        			}
	        			else
	        			{
	        				var image = timeLine.getImage(item.sys_txtImg);
	        				htmlImage= String.format(timeLine.parameters.htmlimage,image,item.Anio,timeLine.parameters.classHTML.hidden);
	        				htmlSlider = String.format(timeLine.parameters.htmlSlideItem,timeLine.parameters.classHTML.itemInactive,item.Anio);
	        				
	        				if(item.SegundaDescripcion != null  && item.SegundoLogo != null && item.PrimerLogo != null)
		        			{
		        				itemInfo += String.format(timeLine.parameters.htmlitem,timeLine.parameters.classHTML.classLogoInitial,item.PrimerLogo.Url,item.PrimeraDescripcion);
		        				itemInfo += String.format(timeLine.parameters.htmlitem,timeLine.parameters.classHTML.classLogoOther,item.SegundoLogo.Url,item.SegundaDescripcion);
		        				contentInfo = String.format(timeLine.parameters.htmlContentText,item.Anio,timeLine.parameters.classHTML.hidden,item.Title,itemInfo);
		        			}
		        			else
		        			{
		        				if(item.PrimerLogo != null)
			        			{
		        					itemInfo += String.format(timeLine.parameters.htmlitem,timeLine.parameters.classHTML.classLogoInitial,item.PrimerLogo.Url,item.PrimeraDescripcion);
									contentInfo = String.format(timeLine.parameters.htmlContentText,item.Anio,timeLine.parameters.classHTML.hidden,item.Title,itemInfo);
								}
	
		        			}
	
	        			}
	        			
	        			carouselContainer.insertAdjacentHTML('beforeend',htmlSlider);
	        			imgContainer.insertAdjacentHTML('beforeend',htmlImage);
	        			dataContainer.insertAdjacentHTML('beforeend', contentInfo);
	        		}
	        		imgSection.appendChild(imgContainer);
					timeLineContainer.appendChild(carouselContainer);
					timeLineContainer.appendChild(imgSection);
					timeLineContainer.appendChild(dataContainer);
					
					var mainContainer = document.getElementById(timeLine.parameters.containerId);
				
					if(mainContainer != null)
					{
						mainContainer.classList.add('.module-timeline');
						mainContainer.appendChild(timeLineContainer);
						timeLine.createSlickCarousel();			
					}	
	        	}
			}
           
        }catch(e){
            if(timeLine.parameters.errorMethod == 'timeLine.initialize'){
                timeLine.parameters.errorMethod = 'timeLine.createSlidesCarousel';
            }
            throw e;
        }
    },    
    getImage:function(img){
    	var container = document.createElement(timeLine.parameters.tagHTML.div);
    	container.insertAdjacentHTML("beforeend",img);
    	 
		if(container.children.length > 0)
		{
			var image = container.children[0].getAttribute('src');
			return image;
		}
		else
		{
			return "";
		}  	
    	
    },	
	getElementsByYear:function(year){		
		try{
			var items = document.getElementsByClassName('slick-slide');
			for(var i=0;i<items.length;i++){
				if(items[i].innerText == year){				
					items[i].classList.add('item-active');
					items[i].classList.remove('item-inactive');
				}else{
					items[i].classList.remove('item-active');
					items[i].classList.add('item-inactive');				
				}
			}				
			var items = document.querySelectorAll('[data-year]');
			for(var i=0;i<items.length;i++){
				if(items[i].getAttribute('data-year') == year){
					items[i].classList.remove('hidden');
					items[i].classList.add('visible');
				}else{
					items[i].classList.remove('visible');
					items[i].classList.add('hidden');
				}
			}
		}catch(e){
			timeLine.parameters.errorMethod = 'timeLine.getElementsByYear';
			throw e
		}		
	},
    createSlickCarousel:function(){
    	$.noConflict();
    	$('.timeline-control').slick({
			infinite: true,
  			slidesToShow: 1,
  			slidesToScroll: 1,
  			dots: false,
			arrows:true,
			responsive:[
				{
				breakpoint: 1024,
					settings: {						
						slidesToShow: 1,
						slidesToScroll:1,
						dots: false,
						arrows:true
					}
				},
				{
				breakpoint: 600,
					settings: {						
						infinite: true,
						slidesToShow: 1,
						slidesToScroll:1,
						dots: false,
						arrows:true
					}
				},
				{
				breakpoint: 480,
					settings: {						
						infinite: true,
						slidesToShow: 1,
						slidesToScroll:1,
						dots: false,
						arrows:false
					}
				},
				{
				breakpoint: 350,
					settings: {						
						infinite: true,
						slidesToShow: 1,
						slidesToScroll:1,
						dots: false,
						arrows:false
					}
				}				
			]
		});
    }

}

timeLine.initialize();