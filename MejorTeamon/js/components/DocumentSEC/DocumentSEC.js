﻿sputilities.callFunctionOnLoadBody("ItemsComunicaciones.initialize");

var ItemsComunicaciones = {
	//create parameters
	parameters:{
			DivId: "Documentos",
			listName: "Boletines",
			listNameAlertas: "Alertas",
			listNameComunicaciones:"Comunicaciones",
			topLibrary: 1,
			queryLibrary: "?$select=ID,DocIcon,ServerRedirectedEmbedUri,Areas,Created,Title,FileRef,FechaVigencia&$filter=FechaVigencia ge datetime'{0}'&$orderby=FechaVigencia desc&$top={1}",
			bodyLibrary: "<div class='projectTabs-document col-md-4 col-sm-4 col-xs-12'>"
							+" <p class='TitleSection'><a href='{4}'>{3}</a></p> <img src='"+sputilities.contextInfo().webAbsoluteUrl+"/_layouts/15/images/{0}'/><a href='{1}' target='_blank'>{2}</a>"
							+"</div>",
			searchUrl:"{0}/Paginas/Busqueda.aspx?k=Contenttype:{1}"


				
	},	
	initialize:function(){
		try {
		var htmlDocs="";
		var today = new Date;
			var month = today.getMonth();
			var day = today.getDate();
			var year = today.getFullYear();
			
			var date = new Date(year,month,day);
			date = date.toISOString();
			var linkQueryLibrary = String.format(ItemsComunicaciones.parameters.queryLibrary,date,ItemsComunicaciones.parameters.topLibrary);

		var url = "Boletines"
		sputilities.getItems(ItemsComunicaciones.parameters.listName,linkQueryLibrary,function(dataDoc){
						bodyNew ="";
						//save the results to an array
						var resultItemsDoc = JSON.parse(dataDoc).d.results;
						var Bole="Boletines";		
						var urlSearch= String.format(ItemsComunicaciones.parameters.searchUrl,sputilities.contextInfo().webAbsoluteUrl,Bole);
						for (var j=0; j< resultItemsDoc.length; j++){
						
							var currentItemDoc = resultItemsDoc[j];
							//retrieving the fields of the results
							var titleDoc = currentItemDoc.Title;
							var urlDoc = currentItemDoc.ServerRedirectedEmbedUri;
							var docIcon = currentItemDoc.DocIcon;
												
							if(docIcon == "pdf"){
								//adding values to the parameters
								urlDoc = currentItemDoc.FileRef;
								var bodyNew = String.format(ItemsComunicaciones.parameters.bodyLibrary,"ic"+docIcon+".png",urlDoc,titleDoc,Bole,urlSearch);
							}else
							 if(docIcon == "txt"){
								//adding values to the parameters
								urlDoc = currentItemDoc.FileRef;
								var bodyNew = String.format(ItemsComunicaciones.parameters.bodyLibrary,"ic"+docIcon+".gif",urlDoc,titleDoc,Bole,urlSearch);
								
							}
							else
							{
								var bodyNew = String.format(ItemsComunicaciones.parameters.bodyLibrary,"ic"+docIcon+".gif",urlDoc,titleDoc,Bole,urlSearch);

							}
							//adding values of parameters to the html variable
							htmlDocs += bodyNew;
						}
						var tabPrincipal = document.getElementById(ItemsComunicaciones.parameters.DivId);
						tabPrincipal.innerHTML = htmlDocs;

							


		});
			var linkQueryLibrary = String.format(ItemsComunicaciones.parameters.queryLibrary,date,ItemsComunicaciones.parameters.topLibrary);
			sputilities.getItems(ItemsComunicaciones.parameters.listNameAlertas,linkQueryLibrary,function(dataDoc){
						bodyNew ="";
						//save the results to an array
						var resultItemsDoc = JSON.parse(dataDoc).d.results;
						var Alertas="Alertas";
						var urlSearch= String.format(ItemsComunicaciones.parameters.searchUrl,sputilities.contextInfo().webAbsoluteUrl,Alertas);
						for (var j=0; j< resultItemsDoc.length; j++){
						
							var currentItemDoc = resultItemsDoc[j];
							//retrieving the fields of the results
							var titleDoc = currentItemDoc.Title;
							var urlDoc = currentItemDoc.ServerRedirectedEmbedUri;
							var docIcon = currentItemDoc.DocIcon;
							
							console.log("ingreso")
							if(docIcon == "pdf"){
								//adding values to the parameters
								urlDoc = currentItemDoc.FileRef;
								var bodyNew = String.format(ItemsComunicaciones.parameters.bodyLibrary,"ic"+docIcon+".png",urlDoc,titleDoc,Alertas,urlSearch);
							}else
							 if(docIcon == "txt"){
								//adding values to the parameters
								urlDoc = currentItemDoc.FileRef;
								var bodyNew = String.format(ItemsComunicaciones.parameters.bodyLibrary,"ic"+docIcon+".gif",urlDoc,titleDoc,Alertas,urlSearch);
								
							}
							else
							{
								var bodyNew = String.format(ItemsComunicaciones.parameters.bodyLibrary,"ic"+docIcon+".gif",urlDoc,titleDoc,Alertas,urlSearch);

							}
							//adding values of parameters to the html variable
							htmlDocs += bodyNew;
						}
						var tabPrincipal = document.getElementById(ItemsComunicaciones.parameters.DivId);
						tabPrincipal.innerHTML = htmlDocs;

							


		});
		
		sputilities.getItems(ItemsComunicaciones.parameters.listNameComunicaciones,linkQueryLibrary,function(dataDoc){
						bodyNew ="";
						//save the results to an array
						var resultItemsDoc = JSON.parse(dataDoc).d.results;
						var Comunicaciones="Comunicaciones";
						var urlSearch= String.format(ItemsComunicaciones.parameters.searchUrl,sputilities.contextInfo().webAbsoluteUrl,Comunicaciones);
						for (var j=0; j< resultItemsDoc.length; j++){
						
							var currentItemDoc = resultItemsDoc[j];
							//retrieving the fields of the results
							var titleDoc = currentItemDoc.Title;
							var urlDoc = currentItemDoc.ServerRedirectedEmbedUri;
							var docIcon = currentItemDoc.DocIcon;
							
							
							if(docIcon == "pdf"){
								//adding values to the parameters
								urlDoc = currentItemDoc.FileRef;
								var bodyNew = String.format(ItemsComunicaciones.parameters.bodyLibrary,"ic"+docIcon+".png",urlDoc,titleDoc,Comunicaciones,urlSearch);
							}else
							 if(docIcon == "txt"){
								//adding values to the parameters
								urlDoc = currentItemDoc.FileRef;

								var bodyNew = String.format(ItemsComunicaciones.parameters.bodyLibrary,"ic"+docIcon+".gif",urlDoc,titleDoc,Comunicaciones,urlSearch);
								
							}
							else
							{
								var bodyNew = String.format(ItemsComunicaciones.parameters.bodyLibrary,"ic"+docIcon+".gif",urlDoc,titleDoc,Comunicaciones,urlSearch);

							}
							//adding values of parameters to the html variable
							htmlDocs += bodyNew;
						}
						var tabPrincipal = document.getElementById(ItemsComunicaciones.parameters.DivId);
						tabPrincipal.innerHTML = htmlDocs;

							


		});
		
		}
		catch (e){
			console.log(e);
		}
	},
	
}