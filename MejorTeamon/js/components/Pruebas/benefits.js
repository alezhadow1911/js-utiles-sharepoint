

/*
-- Componente: Guiones corporativos
	Parameters: 
		* listName: List name for the coporate screenplays
		* divID: Div with that contain of the corporate screenplays
		* divIdCategory: Id for the differents groups of the screenplays
		* divIdQuestion: Id for each question
		* divCategory: Html for each group
		* divAcordiion: Html for each question and answer
		* query: Query for get items of the list

*/


sputilities.callFunctionOnLoadBody("screenplays.initialize");

var screenplays = {

	parameters:{
		listName:"Guiones Corporativos",
		divID:"screenplays",
		divIdCategory:"category-{0}",
		divIdQuestion:"tab-question-{0}",

		divCategory:"<div class='accordion-title' id='{0}'>{1}",
		divAcordion:"<div class='acordion-avianca'><input id='{0}' type='checkbox'><label for='{0}'>{1}</label><div class='accordion-content'>{2}</div></div>",
		query:"?$select=*,Respuesta,AsociadoA&$orderby=AsociadoA"
	},
	initialize:function(){
		
		try {
			
			var html="";
			sputilities.getItems(screenplays.parameters.listName,screenplays.parameters.query,function(data){
			
				//save the results to an array
				var resultItems = JSON.parse(data).d.results;
				
				for (var i=0; i< resultItems.length; i++){
					
					var currentItem = resultItems[i];
					var previousItem = resultItems[i-1];
					var nextItem = resultItems[i+1];


					var question = currentItem.Title;
					var answer = currentItem.Respuesta;
					var asociated = currentItem.AsociadoA;
					
					var idCategory = String.format(screenplays.parameters.divIdCategory, i+1);
					var idQuestion = String.format(screenplays.parameters.divIdQuestion,i+1);
					
					if (i== 0)
					{
						html += String.format(screenplays.parameters.divCategory,idCategory,asociated);
					}
					else
					{
						if(previousItem != undefined && asociated != previousItem.AsociadoA && nextItem != undefined){ 
							
							html += "</div>" + String.format(screenplays.parameters.divCategory,idCategory,asociated);
	
						}
						/*else
						{
							if(nextItem == undefined){
								html += "</div>" + String.format(screenplays.parameters.divCategory,idCategory,asociated);
							}
						}*/
					}
					
					if(i == resultItems.length - 1 ){ 
						
						if(resultItems[i-1].AsociadoA != asociated)
						{
							html += "</div>" + String.format(screenplays.parameters.divCategory,idCategory,asociated);

						}
						else
						{
							html += "</div>";
						}
					}
					
					if(i == resultItems.length - 1 ){
					    html +=  String.format(screenplays.parameters.divAcordion,idQuestion,question,answer) + "</div>";
					}
					else
					{
						html +=  String.format(screenplays.parameters.divAcordion,idQuestion,question,answer);
					}
					
				}
			document.getElementById(screenplays.parameters.divID).innerHTML = html;
			});

			
			
			
			
		}
		catch(e){
			console.log(e);
		}
	
	}


}

