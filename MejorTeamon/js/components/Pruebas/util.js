function ulWriter2(rowIndex, record, columns, cellWriter) {
  var cssClass = "span4", li;
  if (rowIndex % 3 === 0) { cssClass += ' first'; }
  li = '<li class="' + cssClass + '"><span>' + record.make + '</span> <label class="termTitle">'+record.title+'</label> <article class="termDefinition"><p>'+record.description+'</p></article>';
  return li;
}

// Function that creates our records from the DOM when the page is loaded
function ulReader2(index, li, record) {
  var $li = $(li);
  record.make = $li.find('span').text();
  record.title = $li.find(".termTitle").text()
  record.description = $li.find(".termDefinition").text()
}
$( document ).ready(function() {
   $('#search-function-example-list').dynatable({
  table: {
    bodyRowSelector: 'li'
  },
  inputs: {
      queries: $('#search-year')
  },
  dataset: {
    perPageDefault: 2,
    perPageOptions: [2, 4, 6]
  },
  writers: {
    _rowWriter: ulWriter2
  },
  readers: {
    _rowReader: ulReader2
  },
  params: {
    records: 'palabras'
  }
});
   
});
