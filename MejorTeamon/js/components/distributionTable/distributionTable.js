sputilities.callFunctionOnLoadBody('startDistribution')

function startDistribution(){
	distributionTable.initialize();
	timeLine.initialize();
	tableToCarousel.initialize();
	appInsertResponsive.initialize();
}
//component distributionTable
var distributionTable={
	parameters:{		
		tableClassName:'distributionTable',
		errorMsg:'Ocurrion un error en la ejecusion del metodo {0} Error:{1}',
		errorMethod:'distributionTable.initialize'
	},
	initialize:function(){
		try{
			//get tables by class Name
			var editTables = document.getElementsByClassName(distributionTable.parameters.tableClassName);
			//identify if page is in edit mode
			var editMode = document.forms[MSOWebPartPageFormName].MSOLayout_InDesignMode.value;
			if(editMode != 1){
				//take all elements and verify thath the item taken is a table
				for(var i=0;i<editTables.length;i++){
					if(editTables[i].localName == 'table'){
						//hidden Table
						editTables[i].classList.add('hidden');
						editTables[i].classList.remove('visible');						
						//go to distribution
						distributionTable.getItemsByTable(editTables[i]);						
					}
				}				
			}else{
				//take all elements and verify thath the item taken is a table
				for(var i=0;i<editTables.length;i++){
					if(editTables[i].localName == 'table'){
						//hidden Table
						editTables[i].classList.add('visible');
						editTables[i].classList.remove('hidden');
						editTables[i].classList.add('table-responsive');
					}
				}
			}
		}catch(e){
			console.log(String.format(distributionTable.parameters.errorMsg,distributionTable.parameters.errorMethod,e));	
		}		
	},
	getItemsByTable:function(editTable){
		try{
			//create parent Element
			var mainContainer = document.createElement('div');			
			editTable.parentElement.appendChild(mainContainer);
			//get rows in table		
			var rows = editTable.children[0].children;
			//get Columns in Row
			for(var j=0;j<rows.length;j++){
				//create new Row
				var rowContainer = document.createElement('div');								
				//distribution of fields in page
				var columns = rows[j].children.length;
				var widthCol = [];
				var countWidth = Math.floor(12/columns);
				var addWidth = 12%columns;
				widthCol.push({
					'col':countWidth,
					'add':addWidth
				});
				distributionTable.setDistributedItems(rowContainer,rows[j].children,widthCol);
				mainContainer.appendChild(rowContainer);
				mainContainer.classList.add('container');
			}
		}catch(e){
			if (distributionTable.parameters.errorMethod == 'distributionTable.initialize'){
				distributionTable.parameters.errorMethod = 'distributionTable.getItemsByTable';
			}				
			throw e;	
		}
	},
	setDistributedItems:function(rowContainer,rowContent,widthCol){
		try{
			for (var k=0;k<rowContent.length;k++){
				switch (rowContent.length){
					case 1:
						var colSize = 'fullCol';
						var rowClass = 'row distributionTable justify-content-center';
						var type = rowContent[0].getAttribute('class');						
						break;
					case 2: 
						var colSize = 'middleCol';
						var rowClass = 'row distributionTable justify-content-around';
						var type = 'none';
						break;
					default:
						var colSize = 'smallCol';
						var rowClass = 'row distributionTable justify-content-between';
						var type = 'none';
						break;
				}
				if(type != 'none' && type.indexOf('title')!= (-1)){
					var textStyle = 'title';
				}else if(type != 'none' && type.indexOf('lead')!=(-1)){
					var textStyle = 'lead';
				}else{
					var textStyle = '';
				}				
				var fieldContainer = document.createElement('div');
				if(widthCol[0].add != 0 && k < widthCol[0].add){
					var classResponsive = String.format('distributionTable {1} col-sm-12 col-md-{0} col-lg-{0} col-xl-{0} {2}',(widthCol[0].col + 1),colSize,textStyle);	
				}else{
					var classResponsive = String.format('distributionTable {1} col-sm-12 col-md-{0} col-lg-{0} col-xl-{0} {2}',widthCol[0].col,colSize,textStyle);					
				}
				fieldContainer.setAttribute('class',classResponsive);
				fieldContainer.innerHTML = rowContent[k].innerHTML;
				//add controls tu video tag
				var buttonsMedia = fieldContainer.getElementsByClassName('mediaPlayerInitialPlayButton');
				for(var l=0;l<buttonsMedia.length;l++){
					buttonsMedia[l].parentElement.removeChild(buttonsMedia[l]);
				}
				var videoElements = fieldContainer.getElementsByTagName('VIDEO');
				for(var l=0;l<videoElements.length;l++){
					videoElements[l].setAttribute('controls','controls');
				}
				rowContainer.setAttribute('class',rowClass);
				rowContainer.appendChild(fieldContainer);				
			}			
		}catch(e){
			distributionTable.parameters.errorMethod = 'distributionTable.setDistributedItems';	
			throw e;	
		}
	}
}
//component timeLine
var timeLine = {
    parameters:{
        tableClassName:'timeLine',
        urlLogoAviancataca:'/sites/Intranet/SiteAssets/Development/images/components/distributionTable/timeLine/aviancataca.png',
        urlLogoAviancaNuevo:'/sites/Intranet/SiteAssets/Development/images/components/distributionTable/timeLine/AviancaN.png',
		urlLogoAvianca:'/sites/Intranet/SiteAssets/Development/images/components/distributionTable/timeLine/logo-avianca.jpg',
		urlLogoTaca:'/sites/Intranet/SiteAssets/Development/images/components/distributionTable/timeLine/logo-taca.png',
		urlImagen:'/sites/Intranet/SiteAssets/Development/images/components/distributionTable/timeLine/img.jpg',
        errorMsg:'Ocurrion un error en la ejecusion del metodo {0} Error:{1}',
		errorMethod:'timeLine.initialize'
    },
    initialize:function(){
        try{					
			//get tables by class Name
			var editTables = document.getElementsByClassName(timeLine.parameters.tableClassName);
			//identify if page is in edit mode
			var editMode = document.forms[MSOWebPartPageFormName].MSOLayout_InDesignMode.value;
			if(editMode != 1){
				if(editTables.length>0){
					//take all elements and verify thath the item taken is a table
					for(var i=0;i<editTables.length;i++){
						if(editTables[i].localName == 'table'){
							//hidden Table
							editTables[i].classList.add('hidden');
							editTables[i].classList.remove('visible');
							timeLine.createSlidesCarousel(editTables[i]);
						}
					}
					timeLine.createSlickCarousel();
				}
			}else{
				//take all elements and verify thath the item taken is a table
				for(var i=0;i<editTables.length;i++){
					if(editTables[i].localName == 'table'){
						//hidden Table
						editTables[i].classList.add('visible');
						editTables[i].classList.remove('hidden');
					}
				}
			}
		}catch(e){
			console.log(String.format(timeLine.parameters.errorMsg,timeLine.parameters.errorMethod,e));	
		}
    },
    createSlidesCarousel:function(dataTable){
        //get all lines in information Table
        var yearsTimeLine = dataTable.getElementsByTagName('tr');
		//create Timeline container
		var timeLineContainer = document.createElement('div');
		timeLineContainer.setAttribute('class','timeline-content col-sm-12 col-xs-12')
        //create a carousel container
        var carouselContainer = document.createElement('div');
        carouselContainer.setAttribute('class','timeline-control col-sm-12 col-xs-12');
		//create a image section
		var imgSection = document.createElement('div');
		imgSection.setAttribute('class','timeline-image');
		var imgContainer = document.createElement('div');
		imgContainer.setAttribute('class','timeline-image-content col-sm-12 col-xs-12');
		imgSection.appendChild(imgContainer);
		//create info container
		var dataContainer = document.createElement('div');
		dataContainer.setAttribute('class','timeline-text col-sm-12');
        try{
            for(var i=0;i<yearsTimeLine.length;i++){
                var item = yearsTimeLine[i];
				//review error by field empty
				if(item.firstElementChild.innerText != "​"){
					//create slide
					var slideContainer = document.createElement('div');
					//start in first year
					if(i==0){
						slideContainer.setAttribute('class','item-active');
					}else{
						slideContainer.setAttribute('class','item-inactive');
					}
					slideContainer.innerHTML = String.format('<label>{0}</label>',item.firstElementChild.innerText.trim());
					slideContainer.setAttribute('onclick','timeLine.getElementsByYear(this.innerText)')
					//add new slide to carousel container
					carouselContainer.appendChild(slideContainer);  
					timeLine.createInfoTimeLine(item,dataContainer,i,imgContainer); 

                }				
            }			
			timeLineContainer.appendChild(carouselContainer);
            //insert linteTime in parent item
            var mainContainer = dataTable.parentElement;
			mainContainer.classList.add('.module-timeline')	;
			timeLineContainer.appendChild(imgSection);
			timeLineContainer.appendChild(dataContainer);
			mainContainer.appendChild(timeLineContainer);			
        }catch(e){
            if(timeLine.parameters.errorMethod == 'timeLine.initialize'){
                timeLine.parameters.errorMethod = 'timeLine.createSlidesCarousel';
            }
            throw e;
        }
    },
	createInfoTimeLine:function(item,container,numerator,imgContainer){
		try{
			//create data conatainer
			var yearValidation = "1919"
			var infoContainer = document.createElement('div');
			infoContainer.setAttribute('data-year',item.children[0].innerText.trim());
			if(numerator == 0){
				infoContainer.setAttribute('class','visible timeline-text-content col-sm-12 col-xs-12');
			}else{
				infoContainer.setAttribute('class','hidden timeline-text-content col-sm-12 col-xs-12');
			}
			//create and add text year
			var yearTextContainer = document.createElement('h2');
			yearTextContainer.setAttribute('class','text-year');
			var yearText = document.createTextNode(item.children[0].innerText.trim());
			var vecYear = yearText.data.split('');
			var aux = vecYear.splice(1,4)
			var yearJoin = parseInt(aux.join(''));
			yearTextContainer.appendChild(yearText);
			infoContainer.appendChild(yearTextContainer);
			
			//create and add title
			var titleContainer = document.createElement('h3');
			titleContainer.setAttribute('class','text-subtittle');
			var titleText = document.createTextNode(item.children[1].innerText.trim());
			titleContainer.appendChild(titleText);
			infoContainer.appendChild(titleContainer);
			//create and add content 
			for(var i=2;i<4;i++){
				switch(i){
					case 2:
					if(yearJoin > 1918 && yearJoin < 2010 ){
											
						}

					if(yearJoin > 2009 && yearJoin < 2013 ){
						var classItem ='item-avianca item-company';
						var urlLogo = timeLine.parameters.urlLogoAviancataca;						
						}else{
						var classItem ='item-avianca item-company';
						var urlLogo = timeLine.parameters.urlLogoAvianca;	
						}
					if(yearJoin > 2012){
						var classItem ='item-avianca item-company';
						var urlLogo = timeLine.parameters.urlLogoAviancaNuevo;						
						}
					break;
					case 3:
						var classItem ='item-taca item-company';
						var urlLogo = timeLine.parameters.urlLogoTaca;
					break;
				}
				var content = item.children[i].innerText.trim();
				var contentHTML = item.children[i].innerHTML.trim();				
				if(content.length>1){				
					var contentContainer = document.createElement('div');
					contentContainer.setAttribute('class',classItem);
					var logoCompany = document.createElement('img');
					logoCompany.setAttribute('src',urlLogo);
					logoCompany.setAttribute('class','picture-company');
					var textContentContainer = document.createElement('p');
					//var textContent = document.createTextNode(content);
					//textContentContainer.appendChild(textContent);
					textContentContainer.innerHTML = contentHTML
					contentContainer.appendChild(logoCompany);
					contentContainer.appendChild(textContentContainer);			
					infoContainer.appendChild(contentContainer);
				}
			}
			// add main image
			var mainImg = item.children[4].getElementsByTagName('img');
			
			if(mainImg.length>0){
				mainImg[0].setAttribute('data-year',item.children[0].innerText.trim())
				if(numerator == 0){
					mainImg[0].setAttribute('class','visible img-responsive')
				}else{
					mainImg[0].setAttribute('class','hidden img-responsive')
				}				
				imgContainer.appendChild(mainImg[0]);
			}
			//add element in container
			container.appendChild(infoContainer);
		}catch(e){
			timeLine.parameters.errorMethod = 'timeLine.createInfoTimeLine';
			throw e
		}
	},
	getElementsByYear:function(year){		
		try{
			var items = document.getElementsByClassName('slick-slide');
			for(var i=0;i<items.length;i++){
				if(items[i].innerText == year){				
					items[i].classList.add('item-active');
					items[i].classList.remove('item-inactive');
				}else{
					items[i].classList.remove('item-active');
					items[i].classList.add('item-inactive');				
				}
			}				
			var items = document.querySelectorAll('[data-year]');
			for(var i=0;i<items.length;i++){
				if(items[i].getAttribute('data-year') == year){
					items[i].classList.remove('hidden');
					items[i].classList.add('visible');
				}else{
					items[i].classList.remove('visible');
					items[i].classList.add('hidden');
				}
			}
		}catch(e){
			timeLine.parameters.errorMethod = 'timeLine.getElementsByYear';
			throw e
		}		
	},
    createSlickCarousel:function(){
        jQuery('.timeline-control').slick({
			infinite: true,
			slidesToShow: 7,
			slidesToScroll:1,
			dots: false,
			arrows:true,
			responsive:[
				{
				breakpoint: 1024,
					settings: {						
						slidesToShow: 1,
						slidesToScroll:1,
						dots: false,
						arrows:true
					}
				},
				{
				breakpoint: 600,
					settings: {						
						infinite: true,
						slidesToShow: 1,
						slidesToScroll:1,
						dots: false,
						arrows:true
					}
				},
				{
				breakpoint: 480,
					settings: {						
						infinite: true,
						slidesToShow: 1,
						slidesToScroll:1,
						dots: false,
						arrows:false
					}
				},
				{
				breakpoint: 350,
					settings: {						
						infinite: true,
						slidesToShow: 1,
						slidesToScroll:1,
						dots: false,
						arrows:false
					}
				}				
			]
		});
    }

}
//component table to carousell
var tableToCarousel={
	parameters:{		
		tableClassName:'carouselTable',
		errorMsg:'Ocurrion un error en la ejecusion del metodo {0} Error:{1}',
		errorMethod:'tableToCarousel.initialize'
	},	
	initialize:function(){
		try{			
			//get tables by class Name
			var editTables = document.getElementsByClassName(tableToCarousel.parameters.tableClassName);
			//identify if page is in edit mode
			var editMode = document.forms[MSOWebPartPageFormName].MSOLayout_InDesignMode.value;
			if(editMode != 1){
				if(editTables.length>0){
					//take all elements and verify thath the item taken is a table
					for(var i=0;i<editTables.length;i++){
						if(editTables[i].localName == 'table'){
							//hidden Table
							editTables[i].classList.add('hidden');
							editTables[i].classList.remove('visible');
							tableToCarousel.createSlidesCarousel(editTables[i]);
						}
					}
					tableToCarousel.createSlickCarousel();
				}
			}else{
				//take all elements and verify thath the item taken is a table
				for(var i=0;i<editTables.length;i++){
					if(editTables[i].localName == 'table'){
						//hidden Table
						editTables[i].classList.add('visible');
						editTables[i].classList.remove('hidden');
					}
				}
			}
		}catch(e){
			console.log(String.format(tableToCarousel.parameters.errorMsg,tableToCarousel.parameters.errorMethod,e));	
		}
	},
	createSlidesCarousel:function(dataTable){
		try{
			//get content table
			var tableContent = dataTable.children[0].children[0].children;
			var carouselContainer = document.createElement('div')
			for(var j=0;j<tableContent.length;j++){
				//create a slide container
				var slideContainer = document.createElement('div');
				slideContainer.innerHTML = tableContent[j].innerHTML;
				//add controls tu video tag
				var buttonsMedia = slideContainer.getElementsByClassName('mediaPlayerInitialPlayButton');
				for(var k=0;k<buttonsMedia.length;k++){
					buttonsMedia[k].parentElement.removeChild(buttonsMedia[k]);
				}
				var videoElements = slideContainer.getElementsByTagName('VIDEO');
				for(var k=0;k<videoElements.length;k++){
					videoElements[k].setAttribute('controls','controls');
				}
				//get carousel container
				carouselContainer.setAttribute('class','carouselResponsive col-sm-12 col-md-12 col-lg-12 col-xl-12');
				carouselContainer.appendChild(slideContainer);							
			}		
			var mainContainer = dataTable.parentElement;
			mainContainer.style.overflow = 'hidden';	
			mainContainer.appendChild(carouselContainer);
			var counter = carouselContainer.parentElement.parentElement.getElementsByClassName('carouselResponsive').length;
		}catch(e){
			if (tableToCarousel.parameters.errorMethod == 'tableToCarousel.initialize'){
				tableToCarousel.parameters.errorMethod = 'tableToCarousel.createSlidesCarousel';
			}				
			throw e;
		}		
	},
	createSlickCarousel:function(breakPoint){
		jQuery('.carouselResponsive').slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll:1,
			dots: true,
			arrows:true,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						dots: true,
						arrows:true
					}
				},
				{
					breakpoint: 600,
					settings: {
						dots: false,
						arrows:true
					}
				},
				{
					breakpoint: 480,
					settings: {
						dots: false,
						arrows:false
					}
				}
			]
		});
	}
}
//component table to  Horizontal tabs 
var tableToHorizontalTabs={
	parameters:{		
		className:'tableHorizontalTabs',
		tabsFormHtml:'<div class="row tabsContainer align-items-start" style="height: 200px;"><div id="headerTabsHz" class="col-sm-3 col-md-3 col-lg-3 col-xl-3">'
					+'</div><div id="bodyTabsHz" class="col-sm-9 col-md-9 col-lg-9 col-xl-9"></div></div>',
		errorMsg:'Ocurrion un error en la ejecusion del metodo {0} Error:{1}',
		errorMethod:'tableToHorizontalTabs.initialize'			
	},
	initialize:function(){
		try{		
			//get Elements by class
			var editTables = document.getElementsByClassName(tableToHorizontalTabs.parameters.className);
			//identify if page is in edit mode
			var editMode = document.forms[MSOWebPartPageFormName].MSOLayout_InDesignMode.value;
			if (editMode != 1){
				if(editTables.length>0){
					//verify that the item taken is a table and take childs
					for(var i=0;i<editTables.length;i++){
						if(editTables[i].localName == 'table'){
							//hidden Table
							editTables[i].classList.add('hidden');
							editTables[i].classList.remove('visible');	
							//select tabs container
							var tabsContainer = editTables[i].parentElement;
							tabsContainer.setAttribute('class','row tabsContainer align-items-start');
							tabsContainer.style.height = '200px';
							//take childs in Tbody
							var tablesLanes = editTables[i].children[0].children;
							var headerContainer = document.createElement('div');
							headerContainer.setAttribute('class','col-sm-3 col-md-3 col-lg-3 col-xl-3');
							headerContainer.setAttribute('id','headerTabsHz');
							var bodyContainer = document.createElement('div');
							bodyContainer.setAttribute('class','col-sm-9 col-md-9 col-lg-9 col-xl-9');
							bodyContainer.setAttribute('id','bodyTabsHz');

												
							//take content in field
							for(var j=0;j<tablesLanes.length;j++){
								var line = tablesLanes[j];
								var fieldContent = line.children;
								var fieldtype = j % 2;
								//teke Content and create tabs section
								tableToHorizontalTabs.transformTable(fieldContent,fieldtype,j,headerContainer,bodyContainer)
							}
							tabsContainer.innerHTML = '';
							tabsContainer.appendChild(headerContainer);
							tabsContainer.appendChild(bodyContainer);				
						}				
					}
				}
			}else{
				//verify that the item taken is a table and take childs
				for(var i=0;i<editTables.length;i++){
					if(editTables[i].localName == 'table'){
						//hidden Table
						editTables[i].classList.add('visible');
						editTables[i].classList.remove('hidden');															
					}
				}				
			}		
		}catch(e){
			console.log(String.format(tableToHorizontalTabs.parameters.errorMsg,tableToHorizontalTabs.parameters.errorMethod,e));
		}
	},
	transformTable:function(fieldContent,fieldType,tabId,headerContainer,bodyContainer){
		try{							
			//create Tabs sections 
			if(fieldType == 0){
				//create tab 
				var tab = document.createElement('div');
				tab.setAttribute('class','hzTab col-sm-12 col-md-12 col-lg-12 col-xl-12');
				tab.setAttribute('name',String.format('hzTab {0}',tabId));
				tab.setAttribute('onclick','tableToHorizontalTabs.tabsEffect(this.getAttribute("name"))');
				tab.innerHTML = fieldContent[0].innerHTML;
				headerContainer.appendChild(tab);				
			}else{
				//create container field
				var tabContent = document.createElement('div');
				tabContent.setAttribute('class','hzTabContent col-sm-12 col-md-12 col-lg-12 col-xl-12');
				tabContent.setAttribute('name',String.format('hzTab {0}',(tabId-1)));
				if(tabId == 1){					
					tabContent.classList.add('visible');	
				}else{
					tabContent.classList.add('hidden');
				}
				tabContent.innerHTML = fieldContent[0].innerHTML;
				//add controls tu video tag
				var buttonsMedia = tabContent.getElementsByClassName('mediaPlayerInitialPlayButton');
				for(var k=0;k<buttonsMedia.length;k++){
					buttonsMedia[k].parentElement.removeChild(buttonsMedia[k]);
				}
				var videoElements = tabContent.getElementsByTagName('VIDEO');
				for(var k=0;k<videoElements.length;k++){
					videoElements[k].setAttribute('controls','controls');
				}
				//appendChild content in tabs body
				bodyContainer.appendChild(tabContent)

			}
		}catch(e){
			if(tableToHorizontalTabs.parameters.errorMethod == 'tableToHorizontalTabs.initialize'){
				tableToHorizontalTabs.parameters.errorMethod = 'tableToHorizontalTabs.transformTable'
			}
			throw e			
		}
	},
	tabsEffect:function(name){
		try{
			//show and hide elements
			var content = document.getElementsByClassName('hzTabContent');
			for (var i=0;i<content.length;i++){
				var item = content[i];
				var tabName = item.getAttribute('name');
				if(tabName == name){
					item.classList.add('visible');
					item.classList.remove('hidden');	
				}else{
					item.classList.add('hidden');
					item.classList.remove('visible');	
				}
			}			
		}catch(e){
			tableToHorizontalTabs.parameters.errorMethod = 'tableToTabs.tabsEffect';
			throw e
		}
	}
}
//component library responsive
var appInsertResponsive={
	initialize:function(){
		//get all app insert
		var tableContainers = document.getElementsByTagName("table")
		//select Table conainer list view
		for(var i = 0;i<tableContainers.length;i++){
			//select main table an add class attribute
			var listView = tableContainers[i];
			var className = listView.getAttribute('class');
			if(className == null){
				listView.setAttribute('class','table-responsive');
			}else{
				listView.classList.add('table-responsive');
			}
		}
	}
}
//component  view links
var tableToLinks={
	parameters:{
		containerId:'componentTest',
		className:'linksTable',
		errorMsg:'Ocurrion un error en la ejecusion del metodo {0} Error:{1}',
		errorMethod:'tableToLinks.initialize'
	},
	initialize:function(){
		try{				
			//get Elements by class
			var editTables = document.getElementsByClassName(tableToLinks.parameters.className);	
			if(editTables.length != 0){
				//identify if page is in edit mode
				var editMode = document.forms[MSOWebPartPageFormName].MSOLayout_InDesignMode.value;
				if (editMode != 1 && editTables != null){
					//verify that the item taken is a table and take childs
					for(var i=0;i<editTables.length;i++){
						if(editTables[i].localName == 'table'){
							//hidden Table
							editTables[i].classList.add('hidden');
							editTables[i].classList.remove('visible');	
							//change name container
							editTables[i].parentElement.setAttribute('id',tableToLinks.parameters.containerId);						
							//take childs in Tbody
							var rowsbyTable = editTables[i].children[0].children;
							var linkList = [];
							//clear Session Storage
							sessionStorage.removeItem('linksList');
							//take content in field
							for (var i = 0;i<rowsbyTable.length;i++){
								var patternName = tableToLinks.parameters.containerId;
								var rowContent = rowsbyTable[i].children[0].children;
								var levelContent = [];
								tableToLinks.getRowsContent(rowContent,levelContent,patternName);
								linkList.push(levelContent);							
							}
							sessionStorage['linkList']=JSON.stringify(linkList);									
						}				
					}	
					tableToLinks.setLinksList();		
				}else{
					//verify that the item taken is a table and take childs
					for(var i=0;i<editTables.length;i++){
						if(editTables[i].localName == 'table'){
							//hidden Table
							editTables[i].classList.add('visible');
							editTables[i].classList.remove('hidden');															
						}
					}
					//clear component container
					tabsContainer.innerHTML = '';
				}
			}		
		}catch(e){
			console.log(String.format(tableToLinks.parameters.errorMsg,tableToLinks.parameters.errorMethod,e));
		}
	},
	getRowsContent:function(rowContent,levelContent,pattern){
		try{
			//get content by level
			for (var j=0;j<rowContent.length;j++){
				switch (rowContent[j].localName){
					case 'a':
						//create link in level list
						var patternName = rowContent[j].text							
						levelContent.push({
							'pattern':pattern,
							'url':rowContent[j].href,
							'text':rowContent[j].text
						})						
					break;
					case 'table':
						//search new links in next level
						//take childs in Tbody
						var rowsbyTable = rowContent[j].children[0].children;
						//take content in field
						for (var k = 0;k<rowsbyTable.length;k++){
							var Content = rowsbyTable[k].children[0].children;
							tableToLinks.getRowsContent(Content,levelContent,patternName);
							}
					break;
				}
			}	
		}catch(e){
			var err = String.format('/tableToLinks.getRowsContent {0}',e);
			throw err
		}			
	},
	setLinksList:function(){
		try{
			//get links of sessionStorage
			var storage = JSON.parse(sessionStorage['linkList'])
			for (var i=0;i<storage.length;i++){
				for(var j=0;j<storage[i].length;j++){
					//get intems value
					var link = storage[i][j];
					var pattern = link.pattern;
					var url = link.url;
					var text = link.text;
					//get HTML pattern Object
					var container = document.getElementById(pattern);
					var linkContainer = document.createElement('div');
					//create new link space
					linkContainer.setAttribute('id',text);
					//add class attribute by div id (level)
					if(pattern == tableToLinks.parameters.containerId){
						container.setAttribute('class','container col')				
						linkContainer.setAttribute('class','main col');
					}else{
						linkContainer.setAttribute('class','child col')
					}
					var link = document.createElement('a');
					link.href = url;
					var linkText = document.createTextNode(text);
					link.appendChild(linkText);
					linkContainer.appendChild(link);
					container.appendChild(linkContainer);				
				}
			}
		}catch(e){
			var err = String.format('/tableToLinks.setLinksList {0}',e);
			throw err
		}
	}	
}
//component tabs
var tableToTabs={
	parameters:{		
		className:'tableTabs',		
		errorMsg:'Ocurrion un error en la ejecucion del metodo {0} Error:{1}',
		errorMethod:'tableToTabs.initialize'			
	},
	initialize:function(){
		try{		
			//get Elements by class
			var editTables = document.getElementsByClassName(tableToTabs.parameters.className);
			//identify if page is in edit mode
			var editMode = document.forms[MSOWebPartPageFormName].MSOLayout_InDesignMode.value;
			if (editMode != 1){
				//verify that the item taken is a table and take childs
				if(editTables.length>0){
					for(var i=0;i<editTables.length;i++){
						if(editTables[i].localName == 'table'){
							//hidden Table
							editTables[i].classList.add('hidden');
							editTables[i].classList.remove('visible');	
							//select tabs container
							var tabsContainer = editTables[i].parentElement;
							//take childs in Tbody
							var tablesLanes = editTables[i].children[0].children;							
							var headerContainer = document.createElement('div');
							headerContainer.setAttribute('class','row');
							headerContainer.setAttribute('id','headerTabs');
							var bodyContainer = document.createElement('div');
							bodyContainer.setAttribute('class','row');
							bodyContainer.setAttribute('id','bodyTabs');
					
							//take content in field
							for(var j=0;j<tablesLanes.length;j++){
								var line = tablesLanes[j];
								var fieldContent = line.children;
								var fieldtype = j % 2;
								//teke Content and create tabs section
								tableToTabs.transformTable(fieldContent,fieldtype,j,headerContainer,bodyContainer)
							}
							tabsContainer.innerHTML='';
							tabsContainer.appendChild(headerContainer);
							tabsContainer.appendChild(bodyContainer);				
						}				
					}					
				}
				var firstTab = document.getElementsByName('tab 0');
				if(firstTab.length>0){
					firstTab[0].classList.add('activeTab');
				}
			}else{
				//verify that the item taken is a table and take childs
				for(var i=0;i<editTables.length;i++){
					if(editTables[i].localName == 'table'){
						//hidden Table
						editTables[i].classList.add('visible');
						editTables[i].classList.remove('hidden');															
					}
				}				
			}		
		}catch(e){
			console.log(String.format(tableToTabs.parameters.errorMsg,tableToTabs.parameters.errorMethod,e));
		}
	},
	transformTable:function(fieldContent,fieldType,tabId,headerContainer,bodyContainer){
		try{					
			//create Tabs sections 
			if(fieldType == 0){
				//create tab 
				var tab = document.createElement('div');
				tab.setAttribute('class','tab col-xl-2 col-lg-3 col-md-4 col-sm-6');
				tab.setAttribute('name',String.format('tab {0}',tabId));
				tab.setAttribute('onclick','tableToTabs.tabsEffect(this)');
				tab.innerHTML = fieldContent[0].innerHTML;
				headerContainer.appendChild(tab);				
			}else{
				//create container field
				var tabContent = document.createElement('div');
				tabContent.setAttribute('class','tabContent col');
				tabContent.setAttribute('name',String.format('tab {0}',(tabId-1)));
				if(tabId == 1){					
					tabContent.classList.add('visible');	
				}else{
					tabContent.classList.add('hidden');
				}
				tabContent.innerHTML = fieldContent[0].innerHTML;
				//add controls tu video tag
				var buttonsMedia = tabContent.getElementsByClassName('mediaPlayerInitialPlayButton');
				for(var k=0;k<buttonsMedia.length;k++){
					buttonsMedia[k].parentElement.removeChild(buttonsMedia[k]);
				}
				var videoElements = tabContent.getElementsByTagName('VIDEO');
				for(var k=0;k<videoElements.length;k++){
					videoElements[k].setAttribute('controls','controls');
				}
				//appendChild content in tabs body
				bodyContainer.appendChild(tabContent)

			}
		}catch(e){
			if(tableToTabs.parameters.errorMethod == 'tableToTabs.initialize'){
				tableToTabs.parameters.errorMethod = 'tableToTabs.transformTable'
			}
			throw e			
		}
	},
	tabsEffect:function(ctrl){	
		var tabs = document.getElementsByClassName('tab');		
		var name = ctrl.getAttribute("name");		
		try{
			for (var i=0;i<tabs.length;i++){
				tabs[i].classList.remove('activeTab');
			}	
			ctrl.classList.add('activeTab');
			//show and hide elements
			var content = document.getElementsByClassName('tabContent');
			for (var i=0;i<content.length;i++){
				var item = content[i];
				var tabName = item.getAttribute('name');
				if(tabName == name){
					item.classList.add('visible');
					item.classList.remove('hidden');	
				}else{
					item.classList.add('hidden');
					item.classList.remove('visible');	
				}
			}
						
		}catch(e){
			tableToTabs.parameters.errorMethod = 'tableToTabs.tabsEffect';
			throw e
		}
	}
}

var tableToAccordion={
	parameters:{
		className:'accordionTable',
		errorMsg:'Ocurrion un error en la ejecusion del metodo {0} Error:{1}',
		errorMethod:'tableToAccordion.initialize'
	},
	initialize:function(){
		try{			
			//get Elements by class
			var editTables = document.getElementsByClassName(tableToAccordion.parameters.className);
			//identify if page is in edit mode
			var editMode = document.forms[MSOWebPartPageFormName].MSOLayout_InDesignMode.value;
			if (editMode != 1){
				if(editTables.length>0){
					//verify that the item taken is a table and take childs
					for(var i=0;i<editTables.length;i++){
						if(editTables[i].localName == 'table'){
							//hidden Table
							editTables[i].classList.add('hidden');
							editTables[i].classList.remove('visible');
							//get container by accordion
							var container = editTables[i].parentElement;				
							//take childs in Tbody
							var tablesLanes = editTables[i].children[0].children;
							//take content in field
							for(var j=0;j<tablesLanes.length;j++){
								var line = tablesLanes[j];
								var fieldContent = line.children;
								var fieldtype = j % 2;
								//teke Content and create accordion section
								tableToAccordion.transformTable(fieldContent,fieldtype,container,j)
							}				
						}				
					}
				}
			}else{
				//verify that the item taken is a table and take childs
				for(var i=0;i<editTables.length;i++){
					if(editTables[i].localName == 'table'){
						//hidden Table
						editTables[i].classList.add('visible');
						editTables[i].classList.remove('hidden');	
						editTables[i].classList.add('table-responsive');														
					}
				}				
			}		
		}catch(e){
			console.log(String.format(tableToAccordion.parameters.errorMsg,tableToAccordion.parameters.errorMethod,e));
		}
	},
	transformTable:function(fieldContent,fieldType,container,order){
		try{
			//create accordion section title/content
			var accordionSection = document.createElement('div');
			switch (true){
				case (fieldType == 0 && order == 0):
					accordionSection.setAttribute('class','titleSection collapse in');
					accordionSection.setAttribute('onclick','tableToAccordion.accordionEffect(this)');	
					var icon = document.createElement('i');
					icon.setAttribute('class','iconTitle');
					accordionSection.appendChild(icon);
					var contentTitle = document.createElement('h3');
					contentTitle.setAttribute('id','TitlePrincipal')	
					contentTitle.appendChild(fieldContent[0]);		
					accordionSection.appendChild(contentTitle);				
				break;
				case (fieldType == 0 && order != 0):
					accordionSection.setAttribute('class','titleSection collapse');
					accordionSection.setAttribute('onclick','tableToAccordion.accordionEffect(this)');	
					var icon = document.createElement('i');
					icon.setAttribute('class','iconTitle');
					accordionSection.appendChild(icon);
					var contentTitle = document.createElement('h3');
					contentTitle.setAttribute('id','TitlePrincipal')	
					contentTitle.appendChild(fieldContent[0]);		
					accordionSection.appendChild(contentTitle);				
				break;
				case (fieldType != 0 && order == 1):
					accordionSection.setAttribute('class','contentSection visible');
					accordionSection.appendChild(fieldContent[0]);				
				break;
				case (fieldType != 0 && order != 1):
					accordionSection.setAttribute('class','contentSection hidden');
					accordionSection.appendChild(fieldContent[0]);				
				break;
			}			
			
			var buttonsMedia = accordionSection.getElementsByClassName('mediaPlayerInitialPlayButton');
			for(var k=0;k<buttonsMedia.length;k++){
				buttonsMedia[k].parentElement.removeChild(buttonsMedia[k]);
			}
			var videoElements = accordionSection.getElementsByTagName('VIDEO');
			for(var k=0;k<videoElements.length;k++){
				videoElements[k].setAttribute('controls','controls');
			}
			container.appendChild(accordionSection);
		}catch(e){
			if(tableToAccordion.parameters.errorMethod == 'tableToAccordion.initialize'){
				tableToAccordion.parameters.errorMethod == 'tableToAccordion.transformTable';
			}
			throw e;		
		}
	},
	//call method to insert event click
	accordionEffect:function(element){
		try{
		//select current Area		
			var mainContainer = element.parentElement;
			var contentSections = mainContainer.getElementsByClassName('contentSection');
			var TitleSections = mainContainer.getElementsByClassName('titleSection');
			var currentContainer = element.nextElementSibling;
			var currentTitle = element;
			//var statusCurrentContainer = currentContainer.getAttribute('class');
			
			for(var i= 0;i<TitleSections.length;i++){
				var container = TitleSections[i];
				var statusContainer = container.getAttribute('class');	
				
				if(statusContainer == 'titleSection collapse in' && currentTitle != container){
				container.classList.remove('in');
						}				
								}
			if(currentTitle.getAttribute('class') != 'titleSection collapse in'){
				currentTitle.classList.add('in');
				}
				else{
				currentTitle.classList.remove('in');
				}

								
			for(var i= 0;i<contentSections.length;i++){
				var container = contentSections[i];
				var statusContainer = container.getAttribute('class');				
				//hidden All contentSections in area
				if(statusContainer != 'contentSection hidden' && currentContainer != container){
					container.classList.remove('visible');
		     		container.classList.add('hidden');
		     	}		     				
			}
			//show or hidden current contentSection
			if(currentContainer.getAttribute('class') != 'contentSection hidden'){
				currentContainer.classList.remove('visible');
				currentContainer.classList.add('hidden');
			}else{
				currentContainer.classList.remove('hidden');
				currentContainer.classList.add('visible');
			}
			    		
		}catch(e){
			tableToAccordion.parameters.errorMethod == 'tableToAccordion.accordionEffect';
			throw e
		}
		
	}
}

//tabs site dark//
var tableToTabsDark={
	parameters:{		
		className:'tableTabsDark',
		errorMsg:'Ocurrion un error en la ejecucion del metodo {0} Error:{1}',
		errorMethod:'tableToTabsDark.initialize'			
	},
	initialize:function(){
		try{		
			//get Elements by class
			var editTables = document.getElementsByClassName(tableToTabsDark.parameters.className);
			//identify if page is in edit mode
			var editMode = document.forms[MSOWebPartPageFormName].MSOLayout_InDesignMode.value;
			if (editMode != 1){
				if(editTables.length>0){
					//verify that the item taken is a table and take childs
					for(var i=0;i<editTables.length;i++){
						if(editTables[i].localName == 'table'){
							//hidden Table
							editTables[i].classList.add('hidden');
							editTables[i].classList.remove('visible');	
							//select tabs container
							var tabsContainer = editTables[i].parentElement;
							//take childs in Tbody
							var tablesLanes = editTables[i].children[0].children;
							var headerContainer = document.createElement('div');
							headerContainer.setAttribute('class','row');
							headerContainer.setAttribute('id','headerTabs');
							var bodyContainer = document.createElement('div');
							bodyContainer.setAttribute('class','row');
							bodyContainer.setAttribute('id','bodyTabs');
				
							//take content in field
							for(var j=0;j<tablesLanes.length;j++){
								var line = tablesLanes[j];
								var fieldContent = line.children;
								var fieldtype = j % 2;
								//teke Content and create tabs section
								tableToTabsDark.transformTable(fieldContent,fieldtype,j,headerContainer,bodyContainer)
							}	
							tabsContainer.innerHTML = '';
							tabsContainer.appendChild(headerContainer);
							tabsContainer.appendChild(bodyContainer);
										
						}				
					}
					var firstTab = document.getElementsByName('tab 0');
					firstTab[0].classList.add('activeTab');
				}
			}else{
				//verify that the item taken is a table and take childs
				for(var i=0;i<editTables.length;i++){
					if(editTables[i].localName == 'table'){
						//hidden Table
						editTables[i].classList.add('visible');
						editTables[i].classList.remove('hidden');															
					}
				}				
			}		
		}catch(e){
			console.log(String.format(tableToTabsDark.parameters.errorMsg,tableToTabsDark.parameters.errorMethod,e));
		}
	},
	transformTable:function(fieldContent,fieldType,tabId,headerContainer,bodyContainer){
		try{
				
			//create Tabs sections 
			if(fieldType == 0){
				//create tab 
				var tab = document.createElement('div');
				tab.setAttribute('class','Darktab col-xl-2 col-lg-3 col-md-4 col-sm-6');
				tab.setAttribute('name',String.format('tab {0}',tabId));
				tab.setAttribute('onclick','tableToTabsDark.tabsEffect(this)');
				tab.innerHTML = fieldContent[0].innerHTML;
				headerContainer.appendChild(tab);				
			}else{
				//create container field
				var tabContent = document.createElement('div');
				tabContent.setAttribute('class','tabContent col');
				tabContent.setAttribute('name',String.format('tab {0}',(tabId-1)));
				if(tabId == 1){					
					tabContent.classList.add('visible');	
				}else{
					tabContent.classList.add('hidden');
				}
				tabContent.innerHTML = fieldContent[0].innerHTML;
				//add controls tu video tag
				var buttonsMedia = tabContent.getElementsByClassName('mediaPlayerInitialPlayButton');
				for(var k=0;k<buttonsMedia.length;k++){
					buttonsMedia[k].parentElement.removeChild(buttonsMedia[k]);
				}
				var videoElements = tabContent.getElementsByTagName('VIDEO');
				for(var k=0;k<videoElements.length;k++){
					videoElements[k].setAttribute('controls','controls');
				}
				//appendChild content in tabs body
				bodyContainer.appendChild(tabContent)

			}
		}catch(e){
			if(tableToTabsDark.parameters.errorMethod == 'tableToTabsDark.initialize'){
				tableToTabsDark.parameters.errorMethod = 'tableToTabsDark.transformTable'
			}
			throw e			
		}
	},
	tabsEffect:function(ctrl){	
		var tabs = document.getElementsByClassName('Darktab');		
		var name = ctrl.getAttribute("name");		
		try{
			for (var i=0;i<tabs.length;i++){
				tabs[i].classList.remove('activeTab');
			}	
			ctrl.classList.add('activeTab');
			//show and hide elements
			var content = document.getElementsByClassName('tabContent');
			for (var i=0;i<content.length;i++){
				var item = content[i];
				var tabName = item.getAttribute('name');
				if(tabName == name){
					item.classList.add('visible');
					item.classList.remove('hidden');	
				}else{
					item.classList.add('hidden');
					item.classList.remove('visible');	
				}
			}
						
		}catch(e){
			tableToTabsDark.parameters.errorMethod = 'tableToTabsDark.tabsEffect';
			throw e
		}
	}
}



tableToAccordion.initialize()
tableToLinks.initialize();
tableToHorizontalTabs.initialize();
tableToTabs.initialize();
tableToTabsDark.initialize();