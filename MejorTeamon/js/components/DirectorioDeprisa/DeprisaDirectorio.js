﻿ /***********************/
 //initialize before body load
sputilities.callFunctionOnLoadBody('startGlossary');
 
 function startGlossary(){
	 sputilities.executeDelayspTaxonomy(glossary.initialize)
 } 
 
 var glossary={
	 //create components Parameters
	 parameters:{
		listName:"Deprisa",
		containerId:"glossaryContainer",
		termIdAreas:"643787ae-39e0-4f25-8871-ce0148c1068c",
		person:"<p>Cargo:{0}<br>Base: {1}<br>Telefono: {2}<br></p>",
		glossaryFormatHTML:"<div class='directory-header'><div class='controls-search'><div class='Search-Box'><img src='/sites/Intranet/SiteAssets/Development/images/components/employeeDirectory/ico-directory.png' data-themekey='#'>"
					+"<input type='text' placeholder='Digite el nombre a buscar' class='directory-formname' id='searchBox'/></div><a class='buscar' href='#' onclick='glossary.Search()'>buscar</a></div><div class='searchPaginator'><label>Resultados por pagina:</label><select id='itemsBypage' onchange='glossary.getFilteredItems(\"start\")'>"
					+"<option value='6'>6</option><option value='12'>12</option><option value='18'>18</option></select></div>"
					+"<section id='headerList'>"
					+"<section id='letterFilter'></section></div></section><section id='glossaryList'>"
					+"</section></section></section><section id='glossaryFooter'></section>"		
	 },
	 initialize:function(){
		//insert HTML format in HTML Code
		document.getElementById(glossary.parameters.containerId).innerHTML = glossary.parameters.glossaryFormatHTML;
		//get sections by insert glossary information
		var letterFilterList = document.getElementById('letterFilter');
		var businessList = document.getElementById('businessAreaList');
		var categoryList = document.getElementById('businessCategoryList');
		var glossaryList = document.getElementById('glossaryList');
		//create alphabet filter
		//glossary.createAlphabetFilter(letterFilterList);
		// get terms administered
		glossary.getSelectValues(glossary.parameters.termIdAreas,businessList,categoryList,glossaryList);
		//add envent searchBox
		var searchBox = document.getElementById('searchBox')		
		searchBox.addEventListener('keypress', function (e) {			
			if (e.keyCode == 13) { 
				e.preventDefault();					
				glossary.getFilteredItems(searchBox)
			}			
		});		
	 },
	 getSelectValues:function(termSetId,businessList,categoryList,glossaryList){
glossary.getGlossaryTerms(glossaryList);		 
	
	 },
	getGlossaryTerms:function(glossaryList){
		try{
			//execute function for get Items
			sputilities.getItemsByCaml({
				listName:glossary.parameters.listName,
				success:function(data){
					var items = JSON.parse(data).d.results;
					try{
						for (var i=0;i<items.length;i++){
							var term = items[i];
							//create a term Container
							var termContainer = document.createElement('div');
							termContainer.setAttribute('class','element-person');
							termContainer.setAttribute('data-title',term.NombreDeprisa);
							termContainer.setAttribute('data-word',term.CargoDeprisa);
							termContainer.setAttribute('data-phone',term.TelefonoDeprisa);
							termContainer.setAttribute('data-base',term.Base);
							var termContainerDiv = document.createElement('div');
							termContainerDiv.setAttribute('class','info-person');
							termContainer.appendChild(termContainerDiv)							
							//create Name Person
							var termTitle = document.createElement('h4');
							var titleText = document.createTextNode(term.NombreDeprisa);
							termTitle.appendChild(titleText);
							termContainerDiv.appendChild(termTitle);
							
							var person = String.format(glossary.parameters.person,term.CargoDeprisa,term.Base,term.TelefonoDeprisa);

							//create definition of term
							var definitionContainer = document.createElement('p');
							definitionContainer.setAttribute('id','person'+[i]);
							termContainer.appendChild(definitionContainer);
							var person = String.format(glossary.parameters.person,term.CargoDeprisa,term.Base,term.TelefonoDeprisa);
							//append term in glossary list							
							glossaryList.appendChild(termContainer);
							document.getElementById('person'+[i]).innerHTML = person
						}
						glossary.getFilteredItems('start');
					}catch(e){
						throw e
					}
				}
			});
		}catch(e){
			console.log(String.format("Ocurrió un error en la ejecución del método glossary.getGlossaryTerms() {0}",e)) 
		}
	},
	getFilteredItems:function(ctrl){
		if(ctrl == 'start'){
			var ctrl = document.getElementById('searchBox');			
		}
		var limitPage = Number(document.getElementById('itemsBypage').value);
		var limit = limitPage;
		var counterTerms = 0;
		var page=1;
		var filteredTerms = [];
		var footerFormat="{0}{1}" ;
		//get all Classifieds
		var classifieds = document.getElementsByClassName('element-person');
		// get foilter values
		//var businessArea = document.getElementById('businessAreaList').value;
		//var businessCategory = document.getElementById('businessCategoryList').value;		
		switch (true){
			case (sessionStorage['letterFilter'] != undefined):
				var letterFilter = sessionStorage['letterFilter'];
			break;
			case (sessionStorage['letterFilter'] == undefined):
				var letterFilter = 'Todos';
			break;			
		}
		if(document.getElementById('searchBox').value == ""){
			var searchFilter = 'Todos';
		}else{
			var searchFilter = document.getElementById('searchBox').value;
		}		
		try{			
			for(var i = 0;i<classifieds.length;i++){
					var classifiedValue = ({
					"title":classifieds[i].dataset.title.toLowerCase(),
					"word":classifieds[i].dataset.word.toLowerCase(),
					"phone":classifieds[i].dataset.phone.toLowerCase(),
					"Base":classifieds[i].dataset.base.toLowerCase(),

					//"category":classifieds[i].dataset.category,
					//"letter":classifieds[i].dataset.letter.toLowerCase()
				});
				//filter by data title and word
				if(searchFilter != 'Todos'){
					var data = String.format('{0};{1};{2};{3}',classifiedValue.word,classifiedValue.title,classifiedValue.phone,classifiedValue.Base);
					if(data.indexOf(searchFilter.toLowerCase()) == -1){
						var hiddenBySearch = true;
					}else{
						var hiddenBySearch = false;
					}					
				}else{
					var hiddenBySearch = false;
				}
				//filter by category
				//filter by letter
				//insert style to item
				switch(true){
					case (ctrl.id == 'searchBox' && hiddenBySearch):
						classifieds[i].removeAttribute('data-page');
						classifieds[i].style.display = 'none';						
					break;
					case (ctrl.id != 'searchBox' && (hiddenByCategory || hiddenByLetter)):
						classifieds[i].removeAttribute('data-page');
						classifieds[i].style.display = 'none';
					break;
					default:
						filteredTerms.push(classifieds[i]);
					break;
				}				
			}
			if(filteredTerms.length == 0){
				footerFormat = 'No se encontraron resultados para la búsqueda.'
			}
			//get page
			for (var i=0;i<filteredTerms.length;i++){
				switch (true){
					case i < limit && i != (filteredTerms.length-1):
						filteredTerms[i].setAttribute('data-page',page);
						break;
					case i == limit && i != (filteredTerms.length-1):
						var letterPage = String.format('<label class="paginator" onclick="glossary.goFromPage(this.innerText,this)">{0}</label>',page);
						footerFormat=String.format(footerFormat,letterPage,'{0}{1}');
						page +=1;
						limit +=limitPage;
						filteredTerms[i].setAttribute('data-page',page);
						break;
					case i == limit && i == (filteredTerms.length-1):
					    var letterPage = String.format('<label class="paginator" onclick="glossary.goFromPage(this.innerText,this)">{0}</label>',page);
						footerFormat=String.format(footerFormat,letterPage,'{0}{1}');
						page +=1;
						limit +=limitPage;
						filteredTerms[i].setAttribute('data-page',page);
						var letterPage = String.format('<label class="paginator" onclick="glossary.goFromPage(this.innerText,this)">{0}</label>',page);
						footerFormat=String.format(footerFormat,letterPage,'');
						break;
					case i < limit && i == (filteredTerms.length-1):
						filteredTerms[i].setAttribute('data-page',page);
						var letterPage = String.format('<label class="paginator" onclick="glossary.goFromPage(this.innerText,this)">{0}</label>',page);
						footerFormat=String.format(footerFormat,letterPage,'');
						break;
				}		
			}			
			document.getElementById('glossaryFooter').innerHTML = footerFormat;
			glossary.goFromPage(1);	
					


			
		}catch(e){
			console.log(String.format("Ocurrió un error en la ejecución del método glossary.getFilteredItems() {0}",e)) 
		}
				
	},
	goFromPage:function(page,ctrl){
		if(ctrl == undefined){
			var ctrl = document.getElementsByClassName('paginator')[0];			
		}else{
			var activeControl = document.getElementsByClassName('active-page')[0];
			activeControl.classList.remove('active-page');
			activeControl.classList.add('paginator');
		}
		ctrl.classList.remove('paginator');
		ctrl.classList.add('active-page');
				
		var termsSelected = document.querySelectorAll('[data-page]');
		for(var i=0;i<termsSelected.length;i++){
			if(termsSelected[i].getAttribute('data-page') == page){
				termsSelected[i].style.display='block';
				
				
			}else{
				termsSelected[i].style.display='none';				
			}
		}
	},
	Search:function(){
		var searchBox = document.getElementById('searchBox')					;
		glossary.getFilteredItems(searchBox);
	},
	hideOrShowTerm:function(value,data){
		var hidden = true;
		//search in inventory if category is content
	 	for(var i=0;i<data.length;i++){
	 		if(data[i] == value && hidden){
	 			hidden = false;
	 			return hidden;
			} 		
	 	}
	 	return hidden;		
	},
 }
