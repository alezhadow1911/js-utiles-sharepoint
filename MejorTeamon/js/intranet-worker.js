﻿onmessage = function(oEvent){
	var _spPageContextInfo = JSON.parse(oEvent.data);
	console.log(_spPageContextInfo);
	String.prototype.replaceAll = function(search, replacement) {
	    var target = this;
	    return target.split(search).join(replacement);
	};
	
	var utilities = {
		"http":{
			//default types of the http request
			"types":{
				"jsonOData": "application/json;odata=verbose"
			},
			//default methods of the http request
			"methods":{
				"GET":"GET",
				"POST":"POST",
				"PUT":"PUT"
			},
			//defult states of the http request
			"states":{
				"ready":4,
				"success":200,
				"noContent":204
			},
			//custom function for the generic rest calls
			callRestService: function(options){
				try{
					//creating a request object for a rest call	
					var httpRequest = new XMLHttpRequest();
					//opening the rest client with the url and method specified
					httpRequest.open(options.method, options.url, true );  
					//reading the keys of the header and adding properties
					for(var key in options.headers){
						httpRequest.setRequestHeader(key,options.headers[key]);
					}	
					//validating the method of the rest call and adding the custom data	
					if(options.method != utilities.http.methods.GET){
						httpRequest.send(options.data);
					}
					else{
						httpRequest.send();
					}
					//capturing change of the state in the rest call and calling custom success function
					httpRequest.onreadystatechange = function() {
						if (httpRequest.readyState == utilities.http.states.ready && ( httpRequest.status == utilities.http.states.success || httpRequest.status == utilities.http.states.noContent) ){
							//validating the correct type of the success function
							if(typeof options.success == "function"){
								options.success(httpRequest.responseText);
							}
						}
						else if(httpRequest.readyState == utilities.http.states.ready){
							if(typeof options.error == "function"){
								options.error(httpRequest.responseText);
							}
						}
					}
				}
				catch(e){
					//validating the correct type of the error function
					if(typeof options.error == "function"){
						options.error(e);
					}
				}
			}
		},
		//this function is used to get specific query string parameter
		getQueryString: function(name){
			var qs = decodeURIComponent(window.location.search).replace('?','');
			var params = qs.split('&');
			for( var i = 0 ; i < params.length; i++){
				var param = params[i];
				var vals = param.split('=');
				if(vals[0] == name){
					return vals[1];
				}
			}
			return null;
		},
		//this function can calulate a new GUID in the S4 format
		createGuid: function(){  
	        function S4() {  
	        	return (((1+Math.random())*0x10000)|0).toString(16).substring(1);  
	    	}  
	    	return (S4() + S4() + "-" + S4() + "-4" + S4().substr(0,3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();  
	    } 
	}
	
	var sputilities = {
		//saving current context of the SharePoint Page	
		contextInfo : _spPageContextInfo,	
		//tool for the api call in SharePoint Site
		api:{
			//default formats to url of SharePoint api calls
			urlFormats:{
				getFileByServerRelativeUrl : "{0}/_api/web/getfilebyserverrelativeurl('{1}')/$value",
				getFileByServerRelativeUrlMetaData : "{0}/_api/web/getfilebyserverrelativeurl('{1}')",
				addFileByServerRelativeUrl:"{0}/_api/web/GetFolderByServerRelativeUrl('{1}')/Files/add(url='{2}',overwrite=true)",
				getCurrentUserInfo:"{0}/_api/web/lists/getbytitle('User Information List')/items({1})",
				getItems:"{0}/_api/web/lists/GetByTitle('{1}')/items{2}",
				getItemsByCaml:"{0}/_api/web/lists/getbytitle('{1}')/getitems"
			},
			scripts:{
				layoutsFormat:"{0}/_layouts/15/{1}",
				sp : "sp.js",
				spTaxonomy:"sp.taxonomy.js",
				spRuntime:"sp.runtime.js"
			},
			types:{
				spCaml: 'SP.CamlQuery'
			},
			views:{
				allItems:"<View Scope='Recursive><RowLimit>5000</RowLimit><Query></Query></View>"
			}
		},
		//this tool is required to get all security information  of a SharePoint page
		formDigestInfo:{
			interval: 5,
			request: document.getElementById("__REQUESTDIGEST").value,
			updateFormDigest:function(){
				UpdateFormDigest(sputilities.contextInfo.webServerRelativeUrl,sputilities.formDigestInfo.interval)
			}
		},
		//this function allow call function when a page is completely loaded
		callFunctionOnLoadBody: function(nameFunction){
			_spBodyOnLoadFunctionNames.push(nameFunction);
	
		},
		//this function is required for calls to CSOM or JSOM
		executeDelaysp: function(callBack){
			var firstScriptTag = document.getElementsByTagName('body')[0]
			var scriptRT = document.createElement('script');
			scriptRT.src = String.format(sputilities.api.scripts.layoutsFormat,sputilities.contextInfo.webAbsoluteUrl,sputilities.api.scripts.spRuntime);
			firstScriptTag.parentNode.insertBefore(scriptRT, firstScriptTag)
			var scriptSP = document.createElement('script');
			scriptSP.src = String.format(sputilities.api.scripts.layoutsFormat,sputilities.contextInfo.webAbsoluteUrl,sputilities.api.scripts.sp);
			scriptRT.parentNode.insertBefore(scriptSP, scriptRT)
			SP.SOD.executeOrDelayUntilScriptLoaded(function(){
				SP.SOD.executeOrDelayUntilScriptLoaded(callBack,sputilities.api.scripts.sp);
			},sputilities.api.scripts.spRuntime);
		},
		executeDelayspTaxonomy:function(callBack){
			sputilities.executeDelaysp(function(){
				var firstScriptTag = document.getElementsByTagName('script')[0]
				var scriptTaxonomy = document.createElement('script');
				scriptTaxonomy.src = String.format(sputilities.api.scripts.layoutsFormat,sputilities.contextInfo.webAbsoluteUrl,sputilities.api.scripts.spTaxonomy);
				firstScriptTag.parentNode.insertBefore(scriptTaxonomy,firstScriptTag)
				SP.SOD.executeOrDelayUntilScriptLoaded(callBack,sputilities.api.scripts.spTaxonomy);
			});
		},
		//this function is used to get all content of a library file
		getFileContent: function(fileUrl,success,error){
			var url = String.format(sputilities.api.urlFormats.getFileByServerRelativeUrl, sputilities.contextInfo.webAbsoluteUrl,fileUrl);
			utilities.http.callRestService({
				url:url,
				method:utilities.http.methods.GET,
				headers:{
					"accept":utilities.http.types.jsonOData
				},
				success:success,
				error:error
			});
		},
		//this function is used to update all content of a library file
		updateFileContent:function(fileUrl,data,success,error){
			var url = String.format(sputilities.api.urlFormats.getFileByServerRelativeUrl, sputilities.contextInfo.webAbsoluteUrl,fileUrl);
			sputilities.formDigestInfo.updateFormDigest();
			utilities.http.callRestService({
				url:url,
				method:utilities.http.methods.POST,
				data: data,
				headers:{
					"accept":utilities.http.types.jsonOData,
					"X-RequestDigest":sputilities.formDigestInfo.request,
					"X-HTTP-Method": utilities.http.methods.PUT
				},
				success:success,
				error:error
			});
		},
		//this function is used to add a new file of library
		addFileContent:function(urlLibrary,fileName,data,success,error){
			var url = String.format(sputilities.api.urlFormats.addFileByServerRelativeUrl, sputilities.contextInfo.webAbsoluteUrl,urlLibrary,fileName);
			sputilities.formDigestInfo.updateFormDigest();
			utilities.http.callRestService({
				url:url,
				method:utilities.http.methods.POST,
				data: data,
				headers:{
					"accept":utilities.http.types.jsonOData,
					"X-RequestDigest":sputilities.formDigestInfo.request			
				},
				success:success,
				error:error
			});
		},
		//this function is used to get all current user information
		getCurrentUserInformation: function(success,error){
			var url = String.format(sputilities.api.urlFormats.getCurrentUserInfo, sputilities.contextInfo.webAbsoluteUrl,sputilities.contextInfo.userId);
			utilities.http.callRestService({
				url:url,
				method:utilities.http.methods.GET,
				headers:{
					"accept":utilities.http.types.jsonOData
				},
				success:success,
				error:error
			});
		},
		//this function is used to get items with a specific filter in the list sended
		getItems:function(listName,query,success,error)	{
			var url = String.format(sputilities.api.urlFormats.getItems,sputilities.contextInfo.webAbsoluteUrl,listName,query);
			utilities.http.callRestService({
				url:url,
				method:utilities.http.methods.GET,
				headers:{
					"accept":utilities.http.types.jsonOData
				},
				success:success,
				error:error
			});
	
		},
		getItemsByCaml:function(options){
			var data = { 
			       query:{ 
			           "__metadata": { 'type': sputilities.api.types.spCaml},
			           "ViewXml": options.caml == null ? sputilities.api.views.allItems : options.caml
			        } 
			   }; 
			var url = String.format(sputilities.api.urlFormats.getItemsByCaml, sputilities.contextInfo.webAbsoluteUrl,options.listName);
			sputilities.formDigestInfo.updateFormDigest();
			utilities.http.callRestService({
				url:url,
				method:utilities.http.methods.POST,
				data: JSON.stringify(data),
				headers:{
					"accept":utilities.http.types.jsonOData,
					"content-type": utilities.http.types.jsonOData,
					'odata-version':'3.0',
					"X-RequestDigest":sputilities.formDigestInfo.request			
				},
				success:options.success,
				error:options.error
			});
	
		},
		getTermsInTermSet: function(options){
			if(options.siteUrl == null){
				options["siteUrl"] = sputilities.contextInfo.webAbsoluteUrl;
			}
			var ctx  = new SP.ClientContext(options.siteUrl);
			var ssTaxonomy = SP.Taxonomy.TaxonomySession.getTaxonomySession(ctx);
			var termStore = ssTaxonomy.getDefaultSiteCollectionTermStore();
			if(options.termStoreName != null){
		    	termStore = ssTaxonomy.get_termStores().getByName(options.termStoreName);
			}
			var termSet = termStore.getTermSet(options.termSetId);
			var terms = termSet.getAllTerms();
			ctx.load(terms);
			ctx.executeQueryAsync(function(){
				if(typeof options.success == "function"){
					options.success(terms);
				}
			
			},function(sender,args){
				if(typeof options.error == "function"){
					options.error(args.get_message());
				}
			});
		}	
	}
}