﻿var menu = {
	parameters:{
		propertyLink:"_Sys_Nav_TargetUrl",
		propertyIco:"_sys_ico",
		sessionName:"menuItems",
		termSetId:"96803665-5f5e-4c2d-a796-192bb2b66c3c",
		items:null,
		menuSelector : ".menu"
	},
	initialize:function(){
		if(sessionStorage[menu.parameters.sessionName] != null){
				menu.parameters.items = JSON.parse(sessionStorage[menu.parameters.sessionName]) ;	
				//master.components.push(menu); 
				return;
		}
		sputilities.getTermsInTermSet({
			 	success:function(terms){
			 		var enumerator = terms.getEnumerator();
			 		var objTerms = {};
			 		while(enumerator.moveNext()){
				    	var term  = enumerator.get_current();
				    	var parts = term.get_pathOfTerm().split(';');
				    	var name = 	term.get_name();
				    	var url = term.get_localCustomProperties()[menu.parameters.propertyLink];
				    	var ico = term.get_localCustomProperties()[menu.parameters.propertyIco];
				    	var id = term.get_id().toString();
				    	var orders = term.get_customSortOrder();
						
						if(objTerms[parts.length.toString()] == null){
							objTerms[parts.length.toString()] = {};
						}
				    	objTerms[parts.length.toString()][name] ={
				    		id:id,
				    		url:url,
				    		ico:ico,
				    		orders: orders == null ? orders : orders.split(':'),
				    		parent: parts.length > 1 ? parts[parts.length-2]: null,
				    		root: parts.length > 2 ? parts[parts.length-3]: null
				    	}
				    }
				    var auxTerms = {}
				    for(var level in objTerms){
				    	var items = objTerms[level];
				    	for(var itemName in items){
				    		var item = items[itemName];
				    		if(item.parent == null){
				    			auxTerms[itemName] = {
				    				id:item.id,
				    				url:item.url,
				    				ico:item.ico,
				    				orders:item.orders,
				    				children:{}
				    			}
				    		}
				    		else if(level == "2"){
				    			auxTerms[item.parent].children[itemName]  = {
				    				id:item.id,
				    				url:item.url,
				    				ico:item.ico,
				    				orders:item.orders,
				    				children:{},
				    				order:auxTerms[item.parent].orders != null ? auxTerms[item.parent].orders.indexOf(item.id): null
				    			}
				    		}
				    		else{
				    			auxTerms[item.root].children[item.parent].children[itemName]  = {
				    				id:item.id,
				    				url:item.url,
				    				ico:item.ico,
				    				orders:item.orders,
				    				children:{},
				    				order:auxTerms[item.root].children[item.parent].orders != null ? auxTerms[item.root].children[item.parent].orders.indexOf(item.id): null
				    			}
				    		}
				    	}
				    }
				    
				    objTerms = {};
				    for(var itemName in auxTerms){
				    	var item = auxTerms[itemName];
				  		var childs = new Array(Object.keys(item.children).length);
				    	for(var childName in item.children){
				    		var child = item.children[childName];
				    		var subChilds = new Array(Object.keys(child.children).length);
				    		for(var subChildName in child.children){
				    			var subChild = child.children[subChildName];
					    		subChilds[subChild.order]= {
					    				id:subChild.id,
					    				name: subChildName,
					    				url:subChild.url,
					    				ico:subChild.ico
					    		}
				    		}
				    		childs[child.order]= {
				    				id:child.id,
				    				name: childName,
				    				url:child.url,
				    				ico:child.ico,
				    				childs: subChilds
				    		}
				    	}
				    	objTerms[itemName] ={
				    		url:item.url,
				    		ico:item.ico,
				    		childs : childs
				    	}
				    }
				 	menu.parameters.items = objTerms;	
				 	//master.components.push(menu); 
				 	sessionStorage[menu.parameters.sessionName] = JSON.stringify(objTerms);
				 	menu.insertHTML();
				 },
			 	termSetId:menu.parameters.termSetId
			});

	},
	insertHTML:function(){
		var container = document.querySelector(menu.parameters.menuSelector);
		if(container != null){
			var formatRootUL = "<ul>{0}</ul>";
			var formatFirstLevelLI = "<li class='menu-dropdown-icon'><a href='{0}' target='_blank'>{1}</a>{2}</li>";
			var formatSecondLevelLI = "<li><a id='{3}' data-image='{2}' target='_blank' class='option-parent ico'>{0}</a><ul>{1}</ul></li>"
			var formatSecondLevelLIOption = "<li><a id='{3}' href='{0}' data-image='{2}' target='_blank' class='option-nochild ico'>{1}</a></li>"
			var formatLastLevelLi = "<li><a class='title-suboption' href='{0}' target='_blank'>{1}</a></li>"
			var formatId="node-{0}";
			
			var strMenu = "";
			var ids = [];
			var k = 0;
			for(var itemName in menu.parameters.items){
				var item = menu.parameters.items[itemName];
				var strChilds = "";
				for(var i = 0; i < item.childs.length ; i++){
					var child = item.childs[i];
					var strSubChilds = "";
					for(var j = 0 ; j < child.childs.length ; j++){
						var subChild = child.childs[j];
						strSubChilds += String.format(formatLastLevelLi,subChild.url,subChild.name)
					}
					if(strSubChilds != ""){
						strChilds += String.format(formatSecondLevelLI,child.name,strSubChilds,child.ico,String.format(formatId,k))
					}
					else{
						strChilds += String.format(formatSecondLevelLIOption,child.url,child.name,child.ico,String.format(formatId,k))
					}
					ids.push({id:String.format(formatId,k),data:child.ico});
					k++;
				}
				if(strChilds != ""){
					strChilds = String.format(formatRootUL,strChilds);
				}
				strMenu += String.format(formatFirstLevelLI, item.url,itemName,strChilds);
			}
			for(var  i = 0 ; i < ids.length; i++){
				var css = String.format("<style>#{0}::before{{content:url('{1}');}}</style>",ids[i].id,ids[i].data);
				$('head').append(css);
			}
			strMenu  = String.format(formatRootUL,strMenu);
			container.innerHTML= strMenu;			
			
			$('.menu > ul > li:has( > ul)').addClass('menu-dropdown-icon');
		    //Checks if li has sub (ul) and adds class for toggle icon - just an UI
		
		    
		    //Checks if drodown menu's li elements have anothere level (ul), if not the dropdown is shown as regular dropdown
			$(".menu > ul").before("<a href=\"javascript:void(0);\" class=\"menu-mobile\">Menú</a>");
		
		    //Adds menu-mobile class (for mobile toggle menu) before the normal menu
		    //Mobile menu is hidden if width is more then 959px, but normal menu is displayed
		    //Normal menu is hidden if width is below 959px, and jquery adds mobile menu
		    //Done this way so it can be used with wordpress without any trouble
		
		    $(".menu > ul > li").hover(function (e) {
		        if ($(window).width() > 943) {
		            $(this).children("ul").stop(true, false).fadeToggle(150);
		            e.preventDefault();
		        }
		    });
		    
		    //If width is more than 943px dropdowns are displayed on hover
		
		    $(".menu > ul > li").click(function (e) {
		        if ($(window).width() <= 943) {
		            $(this).children("ul").fadeToggle(150);
		            e.preventDefault();
		        }
		    });
		    //If width is less or equal to 943px dropdowns are displayed on click
		
		    $(".menu-mobile").click(function (e) {
		        $(".menu > ul").toggleClass('show-on-mobile');
		        e.preventDefault();
		    });
		    
		    $(".menu-mobile").click(function (e) {
		        $(".menu-vps > ul").toggleClass('show-on-mobile');
		        e.preventDefault();
		    });
		
		    //when clicked on mobile-menu, normal menu is shown as a list, classic rwd menu story   			
		}
	}
}
sputilities.executeDelayspTaxonomy(function(){
			menu.initialize();
});



