﻿/*global $ */
$(document).ready(function () {
	
    "use strict";
     
     console.log($('a[id$="site_share_button"]');
    $('a[id$="site_share_button"]').attr('href','').attr('onclick','sputilities.displaySharingDialog()');
     
    $('.menu > ul > li:has( > ul)').addClass('menu-dropdown-icon');
    //Checks if li has sub (ul) and adds class for toggle icon - just an UI

	$('.menu-vps > ul > li:has( > ul)').addClass('menu-dropdown-icon');
    $('.menu-vps > ul > li > ul:not(:has(ul))').addClass('normal-sub');
    
    //Checks if drodown menu's li elements have anothere level (ul), if not the dropdown is shown as regular dropdown
	$(".menu > ul").before("<a href=\"javascript:void(0);\" class=\"menu-mobile\">Menú</a>");

    $(".menu-vps > ul").before("<a href=\"javascript:void(0);\" class=\"menu-mobile\">Menú</a>");

    //Adds menu-mobile class (for mobile toggle menu) before the normal menu
    //Mobile menu is hidden if width is more then 959px, but normal menu is displayed
    //Normal menu is hidden if width is below 959px, and jquery adds mobile menu
    //Done this way so it can be used with wordpress without any trouble

    $(".menu > ul > li").hover(function (e) {
        if ($(window).width() > 943) {
            $(this).children("ul").stop(true, false).fadeToggle(150);
            e.preventDefault();
        }
    });
    
    $(".menu-vps > ul > li").hover(function (e) {
        if ($(window).width() > 943) {
            $(this).children("ul").stop(true, false).fadeToggle(150);
            e.preventDefault();
        }
    });

    //If width is more than 943px dropdowns are displayed on hover
   $(".menu > ul > li").click(function () {
   		console.log("Entre a la valicación del click");
		 if ($(window).width() <= 943) {
		 console.log("Entre a la valicación del click");
		  $(this).children("ul").fadeToggle(150); 
		  $(this).siblings("li").find("ul:first").hide();
		   } 
		});
	
     //If width is more than 943px dropdowns are displayed on hover
	$(".menu-vps > ul > li").click(function () {
		 if ($(window).width() <= 943) {
		  $(this).children("ul").fadeToggle(150);
		   $(this).siblings("li").find("ul:first").hide();
		   } 
		});
		
    //If width is less or equal to 943px dropdowns are displayed on click

    $(".menu-mobile").click(function (e) {
        $(".menu > ul").toggleClass('show-on-mobile');
        e.preventDefault();
    });
    
    $(".menu-mobile").click(function (e) {
        $(".menu-vps > ul").toggleClass('show-on-mobile');
        e.preventDefault();
    });

    //when clicked on mobile-menu, normal menu is shown as a list, classic rwd menu story   
	
});

sputilities.callFunctionOnLoadBody("overrideAccountCloseButton");
function overrideAccountCloseButton(){
	//override close button account Bar
	var injectButton = setInterval(function(){ 
			if($('.o365cs-flexpane-settings-title').length > 0){
				$('.o365cs-flexpane-settings-title').append("<label id=\"btn-close-accountBar\" onclick='javascript:hideAccountBar()'>x</label>");
				clearInterval(injectButton);
			}
			
	},1000);
}
function hideAccountBar(){
	$('#O365_MeFlexPane_ButtonID').trigger('click');
}
