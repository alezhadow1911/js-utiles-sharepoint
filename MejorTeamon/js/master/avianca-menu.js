﻿/* Logos Unidades de negocio */
var logosUnits = {
		parameters:{
			listName:"Logos Unidades",
			top:4,
			divID:"logos-menu",
			linkImg:"<a href='{0}' target='_blank'>",
			bodyImg: "<img src='{0}' title='{1}'></a>",
			query:"?$select=Title,Logo,UrlUnidad,Orden&$orderby=Orden&$top={0}",
			resultItems:null
		},
		storage:{
			label:"logosUnits"
		},
		initialize:function(){	
			try
			{
					
					
									//add parameter query for value top
				  var queryOdata = String.format(logosUnits.parameters.query,logosUnits.parameters.top);
				  sputilities.getItems(logosUnits.parameters.listName,queryOdata,function(data){
					 //convert result data json element
					 var resultItems = JSON.parse(data).d.results;
								  //call function show data in hmtl
					  logosUnits.parameters.resultItems = resultItems;
					  masterCp.componentsList.push(logosUnits);
				  },function(xhr){ console.log(xhr)},sputilities.contextInfo().siteAbsoluteUrl);
				}				
				
				
			
			catch(e){
				console.log(e);
			}
		},
		insertHTML: function(){
			var data = logosUnits.parameters.resultItems;
			var html="";
			for (var i=0; i< data.length; i++){
					var current = data[i];
					//retrieving the fields of the results data
					var logo = current.Logo.Url;
					var url = current.UrlUnidad.Url;
				    var title = current.Title;
					
					//adding values of parameters for to make htmml
					var linkNew = String.format(logosUnits.parameters.linkImg,url);
					var bodyNew = String.format(logosUnits.parameters.bodyImg,logo,title);	
					
					//adding values of parameters to the html variable
					html += linkNew;
					html += bodyNew;
						
				}
				//insert the html code
				document.getElementById(logosUnits.parameters.divID).innerHTML = html;
		}
	}
	
/* Footer */	
	var footer ={
		parameters:{
			sessionName:"footerItems",
			termSetId:"200c586f-0fbc-429c-919d-0e71ee601bef",
			idFormat:"footer-items-options{0}",
			propertyLink:"_Sys_Nav_SimpleLinkUrl",
			propertya:"target",
			propertyValTarget:"_blank",
			maxSizeColumn:4,
			minValColumnas:1,
			minValItems:0,
			terms:null,
			loadControlIndex:null
		},
		insertHTML:function(){
			var i = footer.parameters.minValItems;
		 	var j = footer.parameters.minValColumnas;
		 	var ul = document.createElement('ul');
		 	var id = String.format(footer.parameters.idFormat,j);
		 	var divFooter = document.getElementById(id);
			divFooter.innerHTML = "";
		    for(var i = 0; i < footer.parameters.terms.length ; i++){
		    	var term  = footer.parameters.terms[i];
		    	
		        var li = document.createElement('li');
		        var a = document.createElement('a');
		        a.href = term.url;
		        a.setAttribute(footer.parameters.propertya,footer.parameters.propertyValTarget);
		        a.innerHTML = term.name;
		        li.appendChild(a);
		        ul.appendChild(li);			
				if(i == footer.parameters.maxSizeColumn){
			   		document.getElementById(id).innerHTML = "";
		    		document.getElementById(id).appendChild(ul);
		   			var ul = document.createElement('ul');
		    		j++;
		  			id = String.format(footer.parameters.idFormat,j);
		    	}
			}
			document.getElementById(id).innerHTML = "";
			document.getElementById(id).appendChild(ul);
		},
		initialize:function(){
			if(sessionStorage[footer.parameters.sessionName] != null){
				footer.parameters.terms = JSON.parse(sessionStorage[footer.parameters.sessionName]) ;	
				masterCp.componentsList.push(footer); 
				return;
			}
			sputilities.getTermsInTermSet({
			 	success:function(terms){
			 		var enumerator = terms.getEnumerator();
			 		var objTerms = [];
			 		while(enumerator.moveNext()){
				    	var term  = enumerator.get_current();
				    	while(term.get_isDeprecated()){
				    		enumerator.moveNext()
				    		term = enumerator.get_current();
				    	}

				    	var name = 	term.get_name();
				    	var url = term.get_localCustomProperties()[footer.parameters.propertyLink];
				    	objTerms.push({
				    		name:name,
				    		url:url
				    	});
				    }
				 	footer.parameters.terms = objTerms ;	
				 	masterCp.componentsList.push(footer); 
				 	sessionStorage[footer.parameters.sessionName] = JSON.stringify(objTerms);
				 },
			 	termSetId:footer.parameters.termSetId
			});
		}
	}

/* Search Component */	
var search = {
		initialize:function()
		{
			masterCp.componentsList.push(search);
		},
		insertHTML:function(){
			
			var searchContainer = document.getElementById('CajaBusqueda');
			var imagenl = document.getElementById('imagenLupa');
			var boton = '<input id="SearchBox" type="text" name="txt" onkeypress="handle(event)" />'
			var botonn = '<a href="javascript:void(0);" onclick="mySearch()"><img src="/sites/Intranet/SiteAssets/Development/images/master/Lupa.png"></a>'
			searchContainer.innerHTML = boton;
			imagenl.innerHTML = botonn;	
		}
}
/* Menú */
var menu = {
	parameters:{
		propertyLink:"_Sys_Nav_TargetUrl",
		propertySimpleLink : "_Sys_Nav_SimpleLinkUrl",
		propertyIco:"_sys_ico",
		sessionName:"menuItems",
		termSetId:"96803665-5f5e-4c2d-a796-192bb2b66c3c",
		items:null,
		menuSelector : ".menu"
	},
	initialize:function(){	
		if(sessionStorage[menu.parameters.sessionName] != null){
				menu.parameters.items = JSON.parse(sessionStorage[menu.parameters.sessionName]) ;	
				masterCp.componentsList.push(menu); 
				return;
		}
		sputilities.getTermsInTermSet({
			 	success:function(terms){
			 		var enumerator = terms.getEnumerator();
			 		var objTerms = {};
			 		while(enumerator.moveNext()){
				    	var term  = enumerator.get_current();
				    	while(term.get_isDeprecated()){
				    		enumerator.moveNext()
				    		term = enumerator.get_current();
				    	}
				    	var parts = term.get_pathOfTerm().split(';');
				    	var name = 	term.get_name();
				    	var url = term.get_localCustomProperties()[menu.parameters.propertySimpleLink];
				    	url = url == null ? term.get_localCustomProperties()[menu.parameters.propertyLink] : url;				    	
				    	var ico = term.get_localCustomProperties()[menu.parameters.propertyIco];
				    	var id = term.get_id().toString();
				    	var orders = term.get_customSortOrder();
						if(objTerms[parts.length.toString()] == null){
							objTerms[parts.length.toString()] = {};
						}
				    	objTerms[parts.length.toString()][name] ={
				    		id:id,
				    		url:url,
				    		ico:ico,
				    		orders: orders == null ? orders : orders.split(':'),
				    		parent: parts.length > 1 ? parts[parts.length-2]: null,
				    		root: parts.length > 2 ? parts[parts.length-3]: null
				    	}
				    }
				    var auxTerms = {}
				    for(var level in objTerms){
				    	var items = objTerms[level];
				    	for(var itemName in items){
				    		var item = items[itemName];
				    		if(item.parent == null){
				    			auxTerms[itemName] = {
				    				id:item.id,
				    				url:item.url,
				    				ico:item.ico,
				    				orders:item.orders,
				    				children:{}
				    			}
				    		}
				    		else if(level == "2"){
				    			auxTerms[item.parent].children[itemName]  = {
				    				id:item.id,
				    				url:item.url,
				    				ico:item.ico,
				    				orders:item.orders,
				    				children:{},
				    				order:auxTerms[item.parent].orders != null ? auxTerms[item.parent].orders.indexOf(item.id): null
				    			}
				    		}
				    		else{
				    			auxTerms[item.root].children[item.parent].children[itemName]  = {
				    				id:item.id,
				    				url:item.url,
				    				ico:item.ico,
				    				orders:item.orders,
				    				children:{},
				    				order:auxTerms[item.root].children[item.parent].orders != null ? auxTerms[item.root].children[item.parent].orders.indexOf(item.id): null
				    			}
				    		}
				    	}
				    }
				    objTerms = {};
				    for(var itemName in auxTerms){
				    	var item = auxTerms[itemName];
				  		var childs = new Array(Object.keys(item.children).length);
				    	for(var childName in item.children){
				    		var child = item.children[childName];
				    		var subChilds = new Array(Object.keys(child.children).length);
				    		for(var subChildName in child.children){
				    			var subChild = child.children[subChildName];
					    		subChilds[subChild.order]= {
					    				id:subChild.id,
					    				name: subChildName,
					    				url:subChild.url,
					    				ico:subChild.ico
					    		}
				    		}
				    		childs[child.order]= {
				    				id:child.id,
				    				name: childName,
				    				url:child.url,
				    				ico:child.ico,
				    				childs: subChilds
				    		}
				    	}
				    	objTerms[itemName] ={
				    		url:item.url,
				    		ico:item.ico,
				    		childs : childs
				    	}
				    }
				 	menu.parameters.items = objTerms;	
				 	masterCp.componentsList.push(menu); 
				 	sessionStorage[menu.parameters.sessionName] = JSON.stringify(objTerms);
				 },
			 	termSetId:menu.parameters.termSetId
			});

	},
	insertHTML:function(){
		var container = document.querySelector(menu.parameters.menuSelector);
		if(container != null){
			var formatRootUL = "<ul>{0}</ul>";
			var formatFirstLevelLI = "<li class='menu-dropdown-icon'><a href='{0}' target='_self'>{1}</a>{2}</li>";
			var formatSecondLevelLI = "<li><a id='{3}' data-image='{2}' target='_self' class='option-parent ico'>{0}</a><ul>{1}</ul></li>"
			var formatSecondLevelLIOption = "<li><a id='{3}' href='{0}' data-image='{2}' target='_self' class='option-nochild ico'>{1}</a></li>"
			var formatLastLevelLi = "<li><a class='title-suboption' href='{0}' target='_self'>{1}</a></li>"
			var formatId="node-{0}";
			
			var strMenu = "";
			var ids = [];
			var k = 0;
			for(var itemName in menu.parameters.items){
				var item = menu.parameters.items[itemName];
				var strChilds = "";
				for(var i = 0; i < item.childs.length ; i++){
					var child = item.childs[i];
					if(child != null){
						var strSubChilds = "";
						for(var j = 0 ; j < child.childs.length ; j++){
							var subChild = child.childs[j];
							if(subChild != null){
								strSubChilds += String.format(formatLastLevelLi,subChild.url,subChild.name)
							}
						}
						if(strSubChilds != ""){
							strChilds += String.format(formatSecondLevelLI,child.name,strSubChilds,child.ico,String.format(formatId,k))
						}
						else{
							strChilds += String.format(formatSecondLevelLIOption,child.url,child.name,child.ico,String.format(formatId,k))
						}
						ids.push({id:String.format(formatId,k),data:child.ico});
						k++;
					}
				}
				if(strChilds != ""){
					strChilds = String.format(formatRootUL,strChilds);
				}
				strMenu += String.format(formatFirstLevelLI, item.url,itemName,strChilds);
			}
			for(var  i = 0 ; i < ids.length; i++){
				var css = String.format("<style>#{0}::before{{content:url('{1}');}}</style>",ids[i].id,ids[i].data);
				$('head').append(css);
			}
			strMenu  = String.format(formatRootUL,strMenu);
			container.innerHTML= strMenu;			
			
			$('.menu > ul > li:has( > ul)').addClass('menu-dropdown-icon');
		    //Checks if li has sub (ul) and adds class for toggle icon - just an UI
		
		    
		    //Checks if drodown menu's li elements have anothere level (ul), if not the dropdown is shown as regular dropdown
			$(".menu > ul").before("<a href=\"javascript:void(0);\" class=\"menu-mobile\">Menú</a>");
		
		    //Adds menu-mobile class (for mobile toggle menu) before the normal menu
		    //Mobile menu is hidden if width is more then 959px, but normal menu is displayed
		    //Normal menu is hidden if width is below 959px, and jquery adds mobile menu
		    //Done this way so it can be used with wordpress without any trouble
		
		    $(".menu > ul > li").hover(function (e) {
		        if ($(window).width() > 943) {
		            $(this).children("ul").stop(true, false).fadeToggle(150);
		            e.preventDefault();
		        }
		    });
		    
		    //If width is more than 943px dropdowns are displayed on hover
		
		    $(".menu > ul > li").click(function () {
				 if ($(window).width() <= 943) {
				  $(this).children("ul").fadeToggle(150);
				  $(this).siblings("li").find("ul:first").hide();
				   } 
			});
		    //If width is less or equal to 943px dropdowns are displayed on click
		
		    $(".menu-mobile").click(function (e) {
		        $(".menu > ul").toggleClass('show-on-mobile');
		        e.preventDefault();
		    });
		
		    //when clicked on mobile-menu, normal menu is shown as a list, classic rwd menu story   			
		}
	}
}

/*Components: Miga de pan*/
var breadcrumb = {
    parameters :{
        url :String.format('{0}{1}', window.location.origin,window.location.pathname).toLowerCase(),
        siteUrl :sputilities.contextInfo().siteAbsoluteUrl.toLowerCase(),
        siteTitle :sputilities.contextInfo().webTitle,
        webUrl :sputilities.contextInfo().webAbsoluteUrl.toLowerCase(),
        webTitle :sputilities.contextInfo().webTitle,
        listId :sputilities.contextInfo().pageListId.replace('{','').replace('}',''),  
        itemId :sputilities.contextInfo().pageItemId,
        objSites :[],
        objParts :[],
        endSites :false,
        endParts :false,
        sessionName:"principalPage",
        getItemInformationQuery:"('{0}')?$select=Title,FileRef,FileLeafRef,File_x0020_Type,ContentType&$expand=ContentType",
        getInfoListQuery:"?$select=Title,DefaultView&$expand=DefaultView",
        getWebInformationQuery:"?$select=Title,Url",
        basicSlashFormat:"/{0}",
        itemBreadCrumbFormat:"<li><a href='{1}'>{0}</a></li>",
        itemBreadCrumbFormatFinal:"<li>{0}</li>",
        breadcrumbFormat:"<ol class='list-breadcrumb'>{0}<ol>",
        simpleConcat:"{0}{1}",
        dotExtensionFormat:".{0}",
        selectorContainer:'breadcrumb',
		containerNews:'breadcrumb-news'
    },
    initialize:function(){
    	if(window.location.href.toUpperCase() !=  breadcrumb.parameters.webUrl.toUpperCase())
    	{
    		sessionStorage.removeItem(breadcrumb.parameters.sessionName);
    	}
    	else
    	{
    		sessionStorage[breadcrumb.parameters.sessionName] = true;
    	}

        if(breadcrumb.parameters.siteUrl != breadcrumb.parameters.webUrl){
            var partsOfWebUrl = breadcrumb.parameters.webUrl.split('/');
            var strSites = breadcrumb.parameters.url.replace(breadcrumb.parameters.siteUrl,'@').replace(String.format(breadcrumb.parameters.basicSlashFormat,partsOfWebUrl[partsOfWebUrl.length-1]),'@');
            var sites = strSites.split('@');
            if(sites[1] != ""){
                breadcrumb.getAllSitesInPath(sites)
            }else{
                breadcrumb.getCurrentWebAndParentOnly();
            }
        }else{
            breadcrumb.getCurrentWebOnly();
        }
        breadcrumb.getLibraryAndPages();
        if(masterCp  != null){
            masterCp.componentsList.push(breadcrumb);
        }
    },
    insertHTML:function(){
        var validateBreadCrumbInitialize = setInterval(function(){
            if(breadcrumb.parameters.endSites && breadcrumb.parameters.endParts){
                var htmlBreadCrumb = ""
                for(var i = 0; i < breadcrumb.parameters.objSites.length ; i++){
                    var objSite = breadcrumb.parameters.objSites[i];
                    if(i == breadcrumb.parameters.objSites.length - 1 && breadcrumb.parameters.objParts.length == 0)
                    {
                   		 htmlBreadCrumb += String.format(breadcrumb.parameters.itemBreadCrumbFormatFinal,objSite.title)
                   	}
                   	else
                   	{
                   		 htmlBreadCrumb += String.format(breadcrumb.parameters.itemBreadCrumbFormat,objSite.title,objSite.url)

                   	}
                }
                for(var i = 0; i < breadcrumb.parameters.objParts.length ; i++){
                    var objPart = breadcrumb.parameters.objParts[i];
                    if(i == breadcrumb.parameters.objParts.length - 1)
                    {
                   		htmlBreadCrumb += String.format(breadcrumb.parameters.itemBreadCrumbFormatFinal,objPart.title)
                   	}
                   	else
                   	{
                   		htmlBreadCrumb += String.format(breadcrumb.parameters.itemBreadCrumbFormat,objPart.title,objPart.url)

                   	}
                }
                htmlBreadCrumb = String.format(breadcrumb.parameters.breadcrumbFormat,htmlBreadCrumb);
                if(document.getElementById(breadcrumb.parameters.containerNews) != null){
                    document.getElementById(breadcrumb.parameters.containerNews).innerHTML = htmlBreadCrumb;
                }
				else{
					 document.getElementById(breadcrumb.parameters.selectorContainer).innerHTML = htmlBreadCrumb;
				}
                clearInterval(validateBreadCrumbInitialize);
            }
        },500);
    },
    getAllSitesInPath:function(sites){
        var urlSites = "";
        sites = sites[1].split('/');
        var j = 0;
        for(var i = 0;i < sites.length ; i++){
            var site = sites[i];
            urlSites += String.format(breadcrumb.parameters.basicSlashFormat,site);
            var urlSite = String.format(breadcrumb.parameters.simpleConcat,breadcrumb.parameters.siteUrl,urlSites);
            sputilities.getWebInformation(urlSite,breadcrumb.parameters.getWebInformationQuery,function(siteData){
                    var objSiteData = JSON.parse(siteData).d;
                    var title = objSiteData.Title;
                    var auxUrlSite = objSiteData.Url;
                    var auxUrlParts = auxUrlSite.split('/');
                    breadcrumb.parameters.objSites.push({
                        title:title,
                        url:auxUrlSite,
                        order:sites.indexOf(auxUrlParts[auxUrlParts.length-1])
                    })
                    if(i == sites.length && i == breadcrumb.parameters.objSites.length){
                        breadcrumb.parameters.endSites = true;
                        breadcrumb.parameters.objSites.push({
                            title:breadcrumb.parameters.webTitle,
                            url:breadcrumb.parameters.webUrl,
                            order:sites.length
                        })
                        breadcrumb.parameters.objSites.sort(function(siteA,siteB){
                            return siteA.order > siteB.order;
                        });
                    }
                },function(xhr){
                    console.log(xhr);
                }
            );
        }
    },
    getCurrentWebAndParentOnly:function(){
        sputilities.getWebInformation(breadcrumb.parameters.siteUrl,"",function(siteData){
                var title = JSON.parse(siteData).d.Title;
                breadcrumb.parameters.objSites.push({
                    title:title,
                    url:breadcrumb.parameters.siteUrl,
                    order:0
                })
                breadcrumb.parameters.objSites.push( {
                    title:breadcrumb.parameters.webTitle,
                    url:breadcrumb.parameters.webUrl,
                    order:1
                });
                breadcrumb.parameters.endSites = true;
                },function(xhr){
                    console.log(xhr);
                }
        );
    },
    getCurrentWebOnly:function(){
         breadcrumb.parameters.objSites.push( {
            title:breadcrumb.parameters.webTitle,
            url:breadcrumb.parameters.webUrl,
            order:0
        });
        breadcrumb.parameters.endSites = true;
    },
    getLibraryAndPages:function(){
        if(breadcrumb.parameters.listId != null && sessionStorage[breadcrumb.parameters.sessionName] == null){
            sputilities.getInfoListById(breadcrumb.parameters.listId,breadcrumb.parameters.getInfoListQuery,function(listData){
                var objListData = JSON.parse(listData).d;
                var title = objListData.Title;
                var url = objListData.DefaultView.ServerRelativeUrl;
                breadcrumb.parameters.objParts.push({
                    title : title,
                    url:url,
                    order:0
                })
                breadcrumb.getItemInformation(title)
            },function(xhr){
                console.log(xhr);
            })
        }else{
            breadcrumb.parameters.endParts = true;
        }
    },
    getItemInformation:function(title){
        if(breadcrumb.parameters.itemId != -1){
            sputilities.getItems(title,String.format(breadcrumb.parameters.getItemInformationQuery,breadcrumb.parameters.itemId),function(itemsData){
                var objItemData = JSON.parse(itemsData).d;
                var pageTitle = objItemData.Title != "" &&  objItemData.Title != null ? objItemData.Title  : objItemData.FileLeafRef.replace(String.format(breadcrumb.parameters.dotExtensionFormat,objItemData.File_x0020_Type),'') ;
                var pageUrl = objItemData.FileRef;
                breadcrumb.parameters.objParts.push({
                    title : pageTitle,
                    url:pageUrl,
                    order:1
                })
                breadcrumb.parameters.endParts = true;
                },function(xhr){
                    console.log(xhr);
                }
            );
        }else{
            breadcrumb.parameters.endParts = true;
        }
    }
}



sputilities.callFunctionOnLoadBody('masterCp.load');

var masterCp ={
	
	components:{
		//search: search,
		//footer: footer,
		logosUnits: logosUnits,
		menu: menu,
		//breadcrumb: breadcrumb
	},
	load:function(){
	 	masterCp.initComponents();
	 	var loadControl = setInterval(function(){
			
			if(masterCp.componentsList.length  == masterCp.max){
				for(var i = 0; i <  masterCp.componentsList.length ; i++){
					 masterCp.componentsList[i].insertHTML();
				}
				clearInterval(loadControl);	
				document.getElementById('s4-workspace').style.opacity = 1;
				document.getElementById('ms-designer-ribbon').style.opacity = 1;

				setTimeout(function(){	
				},5000);							
			}
		},5000);	


	},	
	initComponents:function(){
		//masterCp.components.search.initialize();
		sputilities.executeDelayspTaxonomy(function(){
			console.log($('a[id$="site_share_button"]'));
    		$('a[id$="site_share_button"]').attr('href','javascript:sputilities.displaySharingDialog()').attr('onclick','');

			//masterCp.components.footer.initialize();
			masterCp.components.menu.initialize();

		});
		masterCp.components.logosUnits.initialize();
		//masterCp.components.breadcrumb.initialize();
	},
	componentsList:[],
	max:2

}